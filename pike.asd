;;;; Copyright (c) 2011-2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)

#-:asdf3.1
(error "Requires ASDF >=3.1")

(in-package #:cl-user)

(asdf:defsystem #:pike
  :name "Pike"
  :version (:read-file-form "version.lisp-expr")
  :description "MERS Toolkit: Pike"
  :author "Steve Levine"
  :maintainer "MIT MERS Group"
  :serial t
  :pathname "src"
  :components ((:file "package")

               (:module
                "utils"
                :serial t
                :components ((:file "utils")
                             (:file "time")))

               (:file "pike-defs")

               (:file "pike-hooks")

               (:file "infinity")

               (:module
                "plant-model"
                :serial t
                :components ((:file "plant-model")
                             (:file "plant-model-noop")
                             (:file "plant-model-rmpl")
                             (:file "pddl-processor")
                             (:file "plant-model-pddl")))

               (:module
                "state-space"
                :serial t
                :components ((:file "state-space")
                             (:file "label-collapsing")
                             ;;(:file "naive-probabilities")
                             ))

               (:module
                "lvs"
                :serial t
                :components ((:file "lvs")
                             (:file "lvs-generic")
                             (:file "lvs-test")))

               (:module
                "labeled-distance-graph"
                :serial t
                :components ((:file "ldg")
                             (:file "tpn-to-ldg-conversion")))

               (:module
                "controllability"
                :serial t
                :components ((:file "ldg-sc")))

               (:module
                "apsp"
                :serial t
                :components ((:file "labeled-apsp")
                             (:file "apsp-to-ldg")))



               (:module
                "constraints"
                :serial t
                :components ((:file "constraints")
                             (:file "constraints-simplify")
                             (:file "constraint-conversions")))

               (:module
                "bayes"
                :serial t
                :components ((:file "bayes-net")
                             (:file "bayes-net-parser")
                             (:file "influence-diagram")
                             (:file "influence-diagram-parser")))

               (:module
                "sat"
                :serial t
                :components ((:file "sat-defs")
                             (:file "constraints-to-sat")
                             (:file "d-dnnf")
                             (:file "bdd")
                             (:file "weighted-model-counting")
                             (:file "bayesian-network-inference")
                             (:file "bayesian-network-inference-bdd")
                             (:file "influence-diagram-inference-bdd")))

               (:module
                "atms"
                :serial t
                :components ((:file "atms")
                             (:file "piatms")
                             (:file "patms")
                             (:file "atms-conversions")
                             (:file "atms-utils")
                             (:file "projected-weighted-label-counter")
                             (:file "atms-test")))

               (:module
                "knowledge-bases"
                :serial t
                :components ((:file "constraint-knowledge-base")
                             (:file "constraint-knowledge-base-atms")
                             (:file "constraint-knowledge-base-patms")
                             (:file "constraint-knowledge-base-sat-solver")
                             (:file "constraint-knowledge-base-bdd")
                             (:file "constraint-knowledge-base-cc")
                             (:file "constraint-knowledge-base-cc-wmc-ddnnf")
                             (:file "constraint-knowledge-base-cc-wmc-bdd")
                             (:file "constraint-knowledge-base-cc-wmc-ab-bdd")
                             (:file "constraint-knowledge-base-memoizer")
                             (:file "constraint-knowledge-base-atms-test")
                             (:file "plan-to-smt")))

               (:module
                "causal-links"
                :serial t
                :components ((:file "causal-link-defs")
                             (:file "causal-link-extraction")))


               (:file "compile-plan")

               (:module
                "online"
                :serial t
                :components ((:file "execution-windows")
                             (:file "causal-link-execution-monitor")
                             (:file "online-execution")))

               (:file "pike")
               (:file "pike-lite")

               (:module
                "sim"
                :serial t
                :components ((:file "sim")))

               (:file "pike-analysis"))

  ;; We set this up so that anyone can run (asdf:test-system ...) to run the
  ;; tests without needing to know which framework is used
  :in-order-to ((asdf:test-op (asdf:load-op "pike/test")))
  :perform (asdf:test-op (op c)  (uiop:symbol-call :fiveam :run! :tests-pike))


  :depends-on ((:version #:mtk-graph "0.1.0")
               (:version #:mtk-graph-algorithms "0.1.0")
               (:version #:tpn "0.1.0")
               (:version #:minisat-cffi "0.1.0")
			   #:bordeaux-threads

			   #:s-xml
               #:asdf
               #:cl-ppcre
               #:cl-json
               #:pileup ;; High-performing binary heap implementation (faster than cl-heap, probably faster in practice than fibonacci heaps due to constants)
               ;;#:cl-bayesnet ;; Bayes net inference engine
               #:inferior-shell
               #:fiveam
               #:split-sequence
               #:parse-number
               #:genhash
               #:cl-store
               #:iterate
               #:safe-queue))

;;; Separate package for testing
(asdf:defsystem #:pike/test
  :version "0.1.0"
  :serial t
  :pathname "src"
  :components
  ((:module "testing"
            :serial t
            :components

            ((:file "package")
             (:file "profiling")
             (:file "test-suite")
             (:file "test-utils")
             (:file "tests-profiler")
             (:file "tests-state-space")
             (:file "tests-causal-link-extraction")
             (:file "tests-causal-link-disturbances")
             (:file "tests-strong-controllability")
             (:file "tests-simplify-constraints")
             (:file "tests-constraints-to-tseitin-sat")
             (:file "tests-cc-wmc")
             (:file "tests-bdd")
             (:file "tests-influence-diagram")
             (:file "tests-k-scalable")
             (:file "tests-pike")
             (:file "tests-riker")
             (:file "tests-unobserved-choices")
             (:file "tests-adaptive-human"))))
  :depends-on (:fiveam :pike))

;;; Separate package for command-line argument
;;; work and testing.
(asdf:defsystem #:pike/cli
  :version "0.1.0"
  :serial t
  :pathname "src"
  :components
  ((:module "cli"
            :serial t
            :components
            ((:file "package")
             (:file "utils")
             (:file "arguments")))))


;; Separate package for benchmarking
(asdf:defsystem #:pike/benchmarking
  :version "0.1.0"
  :serial t
  :pathname "src"
  :components
  ((:module "benchmarking"
            :serial t
            :components
            ((:file "package")
             (:file "benchmarking-utils")
             (:file "benchmarking")
             (:file "benchmark-standalone"))))
  :depends-on (#:pike #:pike/test #:pike/cli  #:parse-number))


;; We include this so that other projects can use specific versions
(defmethod version-satisfies ((component (eql (asdf:find-system "pike"))) version)
  "red-shirt uses semantic versioning"
  (uiop:version-compatible-p (component-version component) version))
