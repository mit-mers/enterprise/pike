# Pike / Riker

![Screenshot](https://upload.wikimedia.org/wikipedia/en/5/58/Enterprise_Forward.jpg)


[![build status](https://git.mers.csail.mit.edu/mars-toolkit/pike/badges/master/build.svg)](https://git.mers.csail.mit.edu/mars-toolkit/pike/commits/master)
[![coverage report](https://git.mers.csail.mit.edu/mars-toolkit/pike/badges/master/coverage.svg)](https://git.mers.csail.mit.edu/mars-toolkit/pike/commits/master)

Execute plans boldy with monitoring.

## Description
Pike is where the rubber meets the pavement, so to speak. It's what actually dispatches plans (in the TPN format) to our robots, and is thus an *executive*.

Pike honors flexible temporal constraints, and performs causal-link based execution monitoring, to anticipate future failures and halt execution when something goes wrong. Additionally, it is capable of deciding controllable choices in the plan, and can thus be used for simultaneous intent recognition and robot adaptation.

Riker extends Pike and uses the same codebase, and additionally takes in probabilistic information about the uncontrollable choices to perform a risk-bounded execution.

## Documentation
[Here](https://git.mers.csail.mit.edu/ros/pike-common) is a link to some documentation on using and interfacing with Pike.

## Getting Start with the Pike / Riker code.

A good way to get started with Pike / Riker is to look at some of its unit tests.

To run hundreds of unit tests:
```lisp
(ql:quickload :pike)           ;; Load core
(ql:quickload :pike/test)      ;; Load tests
(pike/test::run! :tests-pike)  ;; Run all tests
```


## Versioning
This project uses semantic versioning.

## Owners:
- Steve Levine (sjlevine@mit.edu)
