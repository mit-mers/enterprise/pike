;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Setting up Pike's callback hooks!
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar *allowed-pike-hooks* (list :execution-start
                                   :execution-end
                                   :event-executed
                                   :activity-dispatch
                                   :committed-to-choice
                                   :activity-failure
                                   :updated-choices-being-waited-on
                                   :execution-cycle-tick
                                   :apsp-computed
                                   :p-success-update
                                   :ask-question))

(defgeneric pike-register-hook (ps hook-name fn))
(defmethod pike-register-hook ((ps pike-session) hook-name fn)
  "Register the given hook with Pike."
  (with-slots (pike-options hooks) ps
    (with-slots (hook-list) pike-options
      (unless (member hook-name *allowed-pike-hooks*)
        (error "Unknown Pike hook name: ~a" hook-name))

      (unless (functionp fn)
        (error "Non-function specified in Pike's hooks: ~a" fn))

      ;; We're good to go! Register this callback.
      (push fn (gethash hook-name hooks)))))


(defgeneric pike-call-hooks (ps hook-name &rest args))
(defmethod pike-call-hooks ((ps pike-session) (hook-name symbol) &rest args)
  "Call all of the callbacks listed."
  (dolist (hook (gethash hook-name (pike-hooks-table ps)))
    (apply hook args)))


(defgeneric pike-init-hooks (ps))
(defmethod pike-init-hooks ((ps pike-session))
  "Sets up Pike's callbacks based on defaults and any user-specified
   callbacks."

  ;; Set up emtpy hash tables
  (with-slots (pike-options hooks) ps
    (dolist (hook-name *allowed-pike-hooks*)
      (setf (gethash hook-name hooks) nil))

    ;; Set up some defaults
    (pike-register-hook ps :execution-start #'default-execution-start)
    (pike-register-hook ps :execution-end #'default-execution-end)
    (pike-register-hook ps :event-executed #'default-event-executed)
    (pike-register-hook ps :activity-dispatch #'default-activity-dispatch)
    (pike-register-hook ps :committed-to-choice #'default-committed-to-choice)
    (pike-register-hook ps :activity-failure #'default-activity-failed)
    (pike-register-hook ps :updated-choices-being-waited-on #'default-choices-waiting-on-update)
    (pike-register-hook ps :p-success-update #'default-p-success-update)
    (pike-register-hook ps :ask-question #'default-ask-question)

    ;; Register any callbacks provided in pike options.
    (with-slots (hook-list) pike-options
      (unless (evenp (length hook-list))
        (error "Malformed hook / callback list specified!"))

      ;; Loop over the specified callbacks and add them
      (do* ((x hook-list (cddr x))
            (hook-name (car x) (car x))
            (fn (cadr x) (cadr x)))
           ((null x) T)
        ;; Register this hook!
        (pike-register-hook ps hook-name fn)))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Default Pike methods and callbacks
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun default-get-time ()
  "Returns the current time, in seconds."
  (unix-time))


(defun default-execution-start (ps)
  "Called right before execution begins."
  (declare (ignore ps))
  (format t (colorize :green "Execution starting.~%")))

(defun default-execution-end (ps)
  "Called right after execution ends."
  (with-slots (status) (pike-execution-context ps)
    (cond
      ((eql status :complete)
       (format t (colorize :green "Execution complete!~%" :bold T)))

      ((eql status :failed)
       (format t (colorize :red "Execution failed.~%" :bold T))
       (explain-why-execution-failed ps))

      ((eql status :preempted)
       (format t (colorize :yellow "Execution pre-empted.~%" :bold T)))

      (T
       (format t (colorize :red "Unknown execution halt.~%" :bold T))))))


(defun default-event-executed (ps event time)
  "Called when the given event is executed at the given (plan) time."
  (format-time t time "Executing ~a~%" (mention-event ps event)))

(defun default-activity-dispatch (activity lb ub time)
  "Called when the given activity should be dispatched"
  (let ((action-dispatch (action-dispatch (activity-action activity))))
    (format-time t time "~a activity ~a for [~,f, ~,f]~%" (colorize :green "Dispatching") (colorize :purple action-dispatch :bold t) (pretty-number lb) (pretty-number ub))))

(defun default-committed-to-choice (assignment time)
  "Called when a choice is committed to"
  (format-time t time (colorize :green (format nil "Committing to choice ~a~%" assignment))))

(defun default-activity-failed (activity time)
  "Called when the given activity failed (activity dispatcher reported failure)"
  (let ((action-dispatch (action-dispatch (activity-action activity))))
    (format-time t time (colorize :red (format nil "Activity ~a failed~%" action-dispatch)))))

(defun default-choices-waiting-on-update (choices time)
  "Called when the given choices are being waited on."
  (if choices
      (format-time t time "Waiting on choice(s): ~a~%" (colorize :purple (format nil "~{~a~^, ~}" (mapcar #'decision-variable-name choices))))
      (format-time t time "Not waiting on any on choices~%" )))

(defun default-p-success-update (p time)
  "Called when a new update to the current probability of success is available."
  (format-time t time "Estimated P(success | history) = ~a%~%" (float (* 100 p))))

(defun default-ask-question (variable-question time)
  "Called when Pike / Riker wants to ask a question, namely what the outcomes of the given variable will be."
  (format-time t time "Asking a question: what will be outcome to variable ~a?~%" (colorize :purple (decision-variable-name variable-question))))
