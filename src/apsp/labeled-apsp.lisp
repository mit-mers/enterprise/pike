;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)

(in-package #:pike)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  Useful datastructure for storing an APSP.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass apsp-table ()
  ((table
    :type simple-array
    :initarg :table
    :initform (error "Must provide table")
    :accessor apsp-table-table
    :documentation "The actual table, a 2-dimensional array.")
   (vertex->index
    :type hash-table
    :initarg :vertex->index
    :initform (error "Must provide mapping")
    :accessor apsp-table-vertex->index
    :documentation "A mapping from vertices to indices along the
table dimensions."))
  (:documentation "Represents an APSP table, connecting
all pairs of edges."))


(declaim (inline apsp-shortest-path))
(defun apsp-shortest-path (apsp e-from e-to)
  "Returns an LVS containing the shortest distance from e-from to e-end."
  (declare (type apsp-table apsp))
  (with-slots (table vertex->index) apsp
    (declare (type (simple-array lvs) table))
    (aref table
          (gethash e-from vertex->index)
          (gethash e-to vertex->index))))


(defun recast-apsp (apsp)
  (declare (type apsp-table apsp))
  (with-slots (table) apsp
    (let ((N (first (array-dimensions table))))
      (dotimes (i N)
        (dotimes (j N)
          (recast-lvs (aref table i j)))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  Compute a labeled All-pairs shortest path
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric compute-labeled-apsp (ps &key &allow-other-keys))
(defmethod compute-labeled-apsp ((ps pike-session) &key (rationalize t))
  "Implements theh Floyd Warshall APSP algorithm, over a
   labeled directed graph and implements Steve's new completions!"
  (when-debug (ps) (format t "Running APSP..."))
  (with-slots (ldg ss apsp) ps
    (let* ((vertices (mtk-graph:get-vertices ldg))
           (vertex->index (make-hash-table))
           (N (length vertices))
           (d (make-array (list N N) :adjustable nil)))

      (declare (type simple-array d))

      ;; Set up the mapping from vertices to indices
      (let ((i 0))
        (declare (type fixnum i))
        (dolist (v vertices)
          (setf (gethash v vertex->index) i)
          ;; TODO Remove for debugging!
          ;; (format t "~a: ~a~%" i v)
          (incf i)))

      ;; Set up the initial values
      (dotimes (i N)
        (dotimes (j N)
          (let ((lvs (make-lvs)))
            (declare (type fixnum i j)
                     (type lvs lvs))
            (if (= i j)
                (add-pair lvs (create-lv-with-universal-env ss :value 0))
                (add-pair lvs (create-lv-with-universal-env ss :value +inf+)))
            (setf (aref d i j) lvs))))

      ;; Add in the edge weights!
      (dolist (edge (mtk-graph:get-edge-list (mtk-graph:get-edges ldg)))
        (let ((w (mtk-graph:get-weight edge ldg))
              (vi (first (mtk-graph:get-vertices edge)))
              (vj (second (mtk-graph:get-vertices edge))))

          (dolist (pair (lvs-pairs w))
            (add-pair (aref d
                            (gethash vi vertex->index)
                            (gethash vj vertex->index))
                      pair))))

      ;; Feel the rhythm! Feel the rhyme! Get on up, it's APSP time!
      ;; This is the core algorithm right here.
      (let ((C_ij (make-lvs)))
        (declare (type lvs C_ij))
        (dotimes (k N)
          (dotimes (i N)
            (dotimes (j N)
              (declare (type fixnum k i j))

              (setf C_ij (lvs-plus ss
                                   (aref d i k)
                                   (aref d k j)))

              (merge-lvs-with-completions ss
                                          (aref d i j)
                                          C_ij)))))

      ;; All done with the APSP computations! Assemble into a nice
      ;; datastructure for easy access.
      (setf apsp (make-instance 'apsp-table
                                :table d
                                :vertex->index vertex->index))

      ;; Call any desired hooks
      (pike-call-hooks ps :apsp-computed)

      T))
  (when-debug (ps) (format t "done~%")))







;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Retrieving conflict environments from the APSP
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric get-temporal-conflict-environments (apsp))
(defmethod get-temporal-conflict-environments ((apsp apsp-table))
  "Returns a minimal set of environments that are temporal conflicts (i.e.,
   negative cycles in the APSP)."
  (with-slots (table vertex->index) apsp
    (let ((conflicts nil)
          lvs)
      (loop for e-i being the hash-keys of vertex->index do
           (setf lvs (apsp-shortest-path apsp e-i e-i))
           (dolist (pair (lvs-pairs lvs))
             (with-slots (value label ss) pair
               (when (< value 0.0)
                 (setf conflicts (add-minimal conflicts label ss))))))
      conflicts)))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Write the APSP to a file. Useful for debugging
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric write-apsp-table-to-stream (apsp s))
(defmethod write-apsp-table-to-stream ((apsp apsp-table) s)
  (with-slots (vertex->index table) apsp
    (let ((col-size (+ 4 (* 2 (apply #'max (loop for e-i being the hash-keys of vertex->index
                                              collecting (length (format nil "~a" (id e-i)))))))))
      (loop for e-i being the hash-keys of vertex->index do
           (loop for e-j being the hash-keys of vertex->index do
                (format s (format nil "~~~aa: ~~a~~%" col-size)
                        (format nil "~a -> ~a" (id e-i) (id e-j))
                        (print-lvs (apsp-shortest-path apsp e-i e-j) nil nil)))))))
