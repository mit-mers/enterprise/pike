;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)

(in-package #:pike)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Convert an APSP table to an LDG, optionally
;; attempting to remove dominated edges and do minimizations.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(declaim (inline mark!))
(defun mark! (marks edge)
  (declare (type array marks)
           (type list edge))
  (let ((vi (first edge))
        (vj (second edge)))
    (declare (type fixnum vi vj))
    (setf (aref marks vi vj) T)))


(declaim (inline marked?))
(defun marked? (marks edge)
  (declare (type array marks)
           (type list edge))
  (let ((vi (first edge))
        (vj (second edge)))
    (declare (type fixnum vi vj))
    (aref marks vi vj)))


(defgeneric convert-apsp-to-ldg (ps &key &allow-other-keys))
(defmethod convert-apsp-to-ldg ((ps pike-session) &key (minimize nil))
  "Take the apsp stores inside the pike session, turn it
  into an ldg, and overwrite the old ldg in the ps."
  (with-slots (ldg apsp) ps
    (let ((event-initial (event-initial ldg))
          (event-final (event-final ldg))
          (at-event->decision-variable (ldg-at-event->decision-variable ldg))
          (decision-variable->at-event (ldg-decision-variable->at-event ldg)))
      (setf ldg (apsp->ldg apsp :minimize minimize))
      ;; Also set the initial and final events
      (setf (event-initial ldg) event-initial)
      (setf (event-final ldg) event-final)
      (setf (ldg-at-event->decision-variable ldg) at-event->decision-variable)
      (setf (ldg-decision-variable->at-event ldg) decision-variable->at-event))))


(defun apsp->ldg (apsp &key (minimize T))
  (if minimize
      (apsp->minimized-ldg apsp)
      (apsp->non-minimized-ldg apsp)))


(defun apsp->non-minimized-ldg (apsp)
  "Converts an APSP table into an LDG. Note that this LDG
   will hence be in dispatchable form."
  (declare (type apsp-table apsp))
  (with-slots (table vertex->index) apsp
    (let ((ldg (make-instance 'labeled-distance-graph))
          (vertices (loop for e being the hash-keys of vertex->index collect e)))

      ;; Add all vertices to the ldg
      (dolist (v vertices)
        (add-event v ldg :uncontrollable? nil))

      ;; Now add all n^2 edges
      (dolist (v1 vertices)
        (dolist (v2 vertices)
          (let ((w (apsp-shortest-path apsp v1 v2)))
            (add-edge v1 v2 w ldg :contingent? nil))))

      ldg)))


(defun apsp->minimized-ldg (apsp)
  ;; TODO: Can probably make this better by thinking through
  ;; stuff with environemnts and subsumption. For now,
  ;; this only works on simpler case of edges with LVS's like
  ;; {(a, {})} - i.e., effectively "unlabeled" values.
  (declare (type apsp-table apsp))
  (with-slots (table vertex->index) apsp
    (let* ((ldg (make-instance 'labeled-distance-graph))
           (vertices (loop for e being the hash-keys of vertex->index collect e))
           (N (length vertices))
           (vertex-indices (loop for i from 0 to (- N 1) collecting i))
           (marks (make-array (list N N) :element-type (or T nil) :initial-element nil :adjustable nil))
           (intersecting-edges nil))

      ;; Construct the list of all intersecting edges (have
      ;; either the same start or the same end vertices)
      (dolist (v-c vertex-indices)
        (do-pairs-ordered (v-a v-b vertex-indices)
          (when (and (not (= v-a v-c))
                     (not (= v-b v-c))
                     (is-simple-lvs? (aref table v-a v-b))
                     (is-simple-lvs? (aref table v-b v-c))
                     (is-simple-lvs? (aref table v-a v-c))) ;; Ensures "simple"
            (push (list (list v-a v-c) (list v-b v-c))
                  intersecting-edges))))

      (dolist (v-a vertex-indices)
        (do-pairs-ordered (v-b v-c vertex-indices)
          (when (and (not (= v-a v-b))
                     (not (= v-a v-c))
                     (is-simple-lvs? (aref table v-a v-b))
                     (is-simple-lvs? (aref table v-b v-c))
                     (is-simple-lvs? (aref table v-a v-c))) ;; Ensures "simple"
            (push (list (list v-a v-b) (list v-a v-c))
                  intersecting-edges))))


      ;; Now that we've constructed all of the intersecting edges,
      ;; Mark edges for elimination
      (dolist (edges intersecting-edges)
        (format t "On: ~a~%" edges)
        (let* ((e1 (first edges))
               (e2 (second edges))
               (e1-dom-e2? (dominates-edge? e1 e2 apsp))
               (e2-dom-e1? (dominates-edge? e2 e1 apsp)))

          (cond
            ((and e1-dom-e2?
                  e2-dom-e1?)
             (when (and (not (marked? marks e1))
                        (not (marked? marks e2)))
               (format t "    Both!~%")
               ;; Arbitrarily choose one
               (mark! marks e2)))

            (e1-dom-e2?
             (mark! marks e2))

            (e2-dom-e1?
             (mark! marks e1)))))

      ;; By the point, we've finished marking which edges to return. Use that
      ;; information to construct the ldg. Note that this new ldg will
      ;; lose all controllability / contingency information, since that isn't
      ;; stored at all in the APSP.
      (dolist (v vertices)
        (add-event v ldg :uncontrollable? nil))


      (do-pairs-ordered (v-i v-j vertices)
        (let ((vi-i (gethash v-i vertex->index))
              (vi-j (gethash v-j vertex->index)))

          (when (not (marked? marks (list vi-i vi-j)))
            (add-edge v-i v-j (apsp-shortest-path apsp v-i v-j) ldg :contingent? nil))))

      ldg)))


(defun is-simple-lvs? (lvs)
  ;; Checks to see if the LVS is of the form {(a, {})}
  (declare (type lvs lvs))
  (let ((pairs (lvs-pairs lvs)))
    (if (= (length pairs) 1)
        (is-empty-environment? (lv-label (first pairs)) (lv-ss (first pairs)))
        nil)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Domination queries
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun dominates-edge? (edge edge-other apsp)
  (or (dominates-upper? edge edge-other apsp)
      (dominates-lower? edge edge-other apsp)))


;; TODO: inline these?
(defun dominates-upper? (edge edge-other apsp)
  (declare (type list edge edge-other)
           (type apsp-table apsp))
  (with-slots (table) apsp
    (let ((va (first edge-other))
          (vb (first edge))
          (vc (second edge))
          (vc-prime (second edge-other)))
      (declare (type fixnum va vb vc vc-prime))

      ;; Edges must have the same destination
      (when (not (= vc vc-prime))
        (return-from dominates-upper? nil))


      ;; Check dominance!
      ;; TODO: only works for the special case noted above
      (let ((d_a->b (lv-value (first (lvs-pairs (aref table va vb)))))
            (d_b->c (lv-value (first (lvs-pairs (aref table vb vc)))))
            (d_a->c (lv-value (first (lvs-pairs (aref table va vc))))))
        ;; TODO declare type here?

        (if (and (>= d_b->c 0) ;; Both edges must be >= 0
              (>= d_a->c 0)
              (= (+ d_a->b d_b->c) d_a->c))
            (format t "    Upper: ~a dominates ~a~%" edge edge-other))


        (and (>= d_b->c 0) ;; Both edges must be >= 0 ;; TODO - did we find a bug in Muscettola et. al. 1998?? > 0 or >= 0?
             (>= d_a->c 0)
             (= (+ d_a->b d_b->c) d_a->c))))))

(defun dominates-lower? (edge edge-other apsp)
  (declare (type list edge edge-other)
           (type apsp-table apsp))
  (with-slots (table) apsp
    (let ((va (first edge))
          (va-prime (first edge-other))
          (vb (second edge))
          (vc (second edge-other)))
      (declare (type fixnum va vb vc va-prime))

      ;; Edges must have the same destination
      (when (not (= va va-prime))
        (return-from dominates-lower? nil))


      ;; Check dominance!
      ;; TODO: only works for the special case noted above
      (let ((d_a->b (lv-value (first (lvs-pairs (aref table va vb)))))
            (d_b->c (lv-value (first (lvs-pairs (aref table vb vc)))))
            (d_a->c (lv-value (first (lvs-pairs (aref table va vc))))))

        (if (and (< d_a->b 0) ;; Both edges must be >= 0
             (< d_a->c 0)
             (= (+ d_a->b d_b->c) d_a->c))
            (Format t "    Lower: ~a dominates ~a~%" edge edge-other))

        ;; TODO declare type here?
        (and (< d_a->b 0) ;; Both edges must be >= 0
             (< d_a->c 0)
             (= (+ d_a->b d_b->c) d_a->c))))))
