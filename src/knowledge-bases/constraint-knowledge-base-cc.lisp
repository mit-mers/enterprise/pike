;;;; Copyright (c) 2017 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)


;;;; Represents a super class to all of the chance-constrained
;;;; knowledge base types.
(defclass constraint-knowledge-base-cc (constraint-knowledge-base)
  ((bn
    :initform (error "Must supply a Bayesian network to a chance-constrained knowledge base!")
    :accessor bayesian-network
    :initarg :bayesian-network
    :documentation "A Bayesian network representing a probability
distribution over uncontrollable choice variables.")
   (cc
    :initform 0.95
    :accessor chance-constraint
    :initarg :chance-constraint
    :documentation "A chance constraint value, indicating the minimum
acceptable probability of plan execution success.")
   (variable-total-ordering
    :initform nil
    :accessor variable-total-ordering
    :initarg :variable-total-ordering
    :documentation "A total ordering (expressed as a list) of the choice variables
made in the TPN / pTPN.")
   (causal-link-support-variable-orderings
    :type list
    :initform nil
    :initarg :causal-link-support-variable-orderings
    :documentation "A hash table mapping variables (like spec and ordering variables) to sets
of pTPN variables that necessarily succeede them temporarily.")
   (choice-variable-guards
    :initarg :choice-variable-guards
    :documentation "A hash table mapping variables to lists of assignments representing their guards
in the pTPN.")
   (constraints-bn
    :initform nil
    :documentation "A list of constraints encoding the probabilistic inference.")))



(defgeneric compute-conditional-probability-of-correctness (ckb-cc &key assignments-given conflicts)
  (:documentation "Return the conditional probability of plan cocrrectness!"))

(defgeneric compute-conditional-marginal-probability-of-uncontrollable-choice (ckb-cc assignment)
  (:documentation "Returns the marginal probability, conditioned on past observations, of a given assignment to an uncontrollable choice variable."))
