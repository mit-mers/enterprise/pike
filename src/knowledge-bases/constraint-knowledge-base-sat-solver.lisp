;;;; Copyright (c) 2017 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)

(defclass constraint-knowledge-base-sat-solver (constraint-knowledge-base)
  ((ss
    :type state-space-bits
    :initform (error "Must provide state space!")
    :initarg :ss
    :documentation "The state space governing variables")
   (sat-encoding
    :initform nil
    :accessor constraint-knowledge-base-sat-solver-sat-encoding)))


(defmethod initialize-instance :after ((ckb constraint-knowledge-base-sat-solver) &key)
  ;; Attempt to link to the minisat SAT library bindings
  (minisat-cffi:minisat-load-bindings))


(defmethod add-constraint! ((ckb constraint-knowledge-base-sat-solver) constraint)
  "Simply store the constraint for now"
  (push constraint (constraint-knowledge-base-constraints ckb)))


(defmethod update-with-constraint! ((ckb constraint-knowledge-base-sat-solver) constraint)
  (with-slots (sat-encoding) ckb
    (unless sat-encoding
      (error "Must process-constraints before updating with new ones!"))
    ;; Add new constraint to SAT encoding
    (add-more-constraints-to-sat sat-encoding (list constraint))))


(defmethod solution-exists? ((ckb constraint-knowledge-base-sat-solver))
  "Call the SAT solver to see if a solution exists!"
  (with-slots (sat-encoding) ckb
    (call-sat-solver sat-encoding)))


(defun call-sat-solver (sat)
  "Helper function to call a SAT solver from a given SAT encoding."
  (let ((clauses (generate-dimacs-clauses sat)))
    (minisat-cffi:solve-sat-problem clauses)))


(defmethod could-ever-commit-to-environment? ((ckb constraint-knowledge-base-sat-solver) environment)
  "To check if we could ever commit to this environment, create a copy
of the given SAT CNF encoding, and then add additional environment constraint."
  (could-add-constraints-and-commit-to-environment? ckb nil environment))


(defmethod could-add-constraints-and-commit-to-environment? ((ckb constraint-knowledge-base-sat-solver) constraints environment)
  "Can we commit to the given constraints and the environment? Find out by making
a copy of the SAT CNF encoding, adding additional constraints, and checking
satisfiability!"
  (with-slots (sat-encoding ss) ckb
    ;; Make a copy of the SAT problem
    (let ((sat-copy (copy-sat-encoding sat-encoding)))
      ;; Add the given constraints
      (add-more-constraints-to-sat sat-copy constraints)
      ;; Also add the environment conjunction
      (add-more-constraints-to-sat sat-copy (list (environment->conjunction environment ss)))
      ;; Now check satisfiability
      (call-sat-solver sat-copy))))


(defmethod could-add-conflict-environments-and-commit-to-environment? ((ckb constraint-knowledge-base-sat-solver) conflicts environment)
  (with-slots (ss) ckb
    (could-add-constraints-and-commit-to-environment? ckb
                                                    (mapcar #'(lambda (e-c)
                                                                (environment->conflict e-c ss))
                                                            conflicts)
                                                    environment)))

(defmethod can-constraints-hold? ((ckb constraint-knowledge-base-sat-solver) constraints)
  (error "Not implemented yet for SAT solver interface"))

(defmethod process-constraints ((ckb constraint-knowledge-base-sat-solver))
  "Encode the consraints in CNF. Don't need to call the SAT solver just yet."
  (with-slots (ss constraints sat-encoding) ckb
    (setf sat-encoding (encode-constraints-as-sat-cnf constraints (state-space-bits-variables ss)))))


(defmethod print-solutions-to-stream ((ckb constraint-knowledge-base-sat-solver) stream)
  (format stream "The SAT solver constraint back end does not generate solutions beforehand!"))

(defmethod get-solution-size ((ckb constraint-knowledge-base-sat-solver))
  "How big are all the solutions? Not sure! Return -1..."
  -1)
