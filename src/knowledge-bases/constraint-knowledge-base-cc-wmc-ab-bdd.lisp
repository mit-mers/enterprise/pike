;;;; Copyright (c) 2018 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)


;;;; This file implements a chance-constrained solver, effectively solving
;;;; stochastic CSP's. It functions by converting the stochastic CSP constraints
;;;; to a weighted model counting problem, by encoding the Bayesian Network
;;;; as a SAT problem with weights on the literals, and then additionally
;;;; overlaying the other constraints of the CSP. The weighted model count
;;;; will compute the probability that the constraints are satisfiable.

(defclass constraint-knowledge-base-cc-wmc-ab-bdd (constraint-knowledge-base-cc)
  ((ss
    :type state-space-bits
    :initform (error "Must provide state space!")
    :initarg :ss
    :documentation "The state space governing variables")
   (bm
    :type bdd-manager
    :documentation "The BDD manager keeping track of the BDDs")
   (bde
    :type bdd-encoder
    :documentation "The BDD variable encoder using the bm")
   (node-correct
    :type bdd-node
    :documentation "The top-level BDD node representing all constraints (including the BN ones).")
   (idi
    :initform nil
    :accessor get-influence-diagram-inference
    :documentation "A class to do influence diagram inference -- used only for convenient normalization.")
   (node-bn
    :type bdd-node
    :documentation "The top-level BDD Bayesian network node representing just the BN / WMC constraints.")
   (node-constraints
    :type list
    :initform nil
    :documentation "A list of nodes representing the each individual compiled constraint.")
   (key-toplevel
    :type list
    :documentation "The top-level key, which when used to loop up in bm's op-unique-table, yields the current approximation BDD.")
   (interval-check-bounds
    :type fixnum
    :initarg :interval-check-bounds
    :initform 1
    :documentation "The period (in iterations) between which to check against the CC bounds.")
   (compile-to-completion?
    :type boolean
    :initarg :compile-to-completion?
    :initform t
    :documentation "A flag indicating whether we should compile just until we prove or disprove chance constraint, or until completion.")
   (log-iterations?
    :type boolean
    :initarg :log-iterations?
    :initform t
    :documentation "A flag indicating whether any data should be logged at all.")
   (log-graphs?
    :type boolean
    :initarg :log-results?
    :initform nil
    :documentation "A flag indicating whether BDD approximation graphs should be logged regularly.")))


(defmethod initialize-instance :after ((ckb constraint-knowledge-base-cc-wmc-ab-bdd) &key)
  (with-slots (bm bde ss) ckb
    (setf bm (create-bdd-manager))
    (setf bde (make-bdd-encoder :ss ss
                                :bm bm))))


(defmethod add-constraint! ((ckb constraint-knowledge-base-cc-wmc-ab-bdd) constraint)
  "Simply store the constraint for now"
  (with-slots (constraints) ckb
    (push constraint constraints)))


(defmethod update-with-constraint! ((ckb constraint-knowledge-base-cc-wmc-ab-bdd) constraint)
  (with-slots (ss bm bde node-correct idi) ckb
    (let ((bdd-constraint (constraint->bdd bde constraint)))
      ;; Commit to the constraint in node-correct
      (setf node-correct (bdd-and bm
                                  node-correct
                                  bdd-constraint))

      ;; Compute the set of assignments
      (when (or (typep constraint 'assignment-constraint)
                (typep constraint 'conjunction-constraint))
        (let* ((assignment-constraints (cond ((typep constraint 'assignment-constraint) (list constraint))
                                             ((typep constraint 'conjunction-constraint) (conjunction-constraint-conjuncts constraint))))
               (assignments-given (mapcar #'assignment-constraint-assignment assignment-constraints)))

          (setf assignments-given (add-associated-assignments ckb assignments-given))

          ;; Now conjunctively add these assignments to the influence diagram influence subsystem
          (commit-to-assignments idi :assignments (remove-if-not #'(lambda (a)
                                                                     (member (assignment-variable a)
                                                                             (bayesian-network-variables (bayesian-network idi))))
                                                                 assignments-given)))))))


(defmethod solution-exists? ((ckb constraint-knowledge-base-cc-wmc-ab-bdd))
  (with-slots (ss) ckb
    ;; Return if it's possible, under current circumstances, to achieve our chance constraint.
    ;;(could-add-conflict-environments-and-commit-to-environment? ckb nil (make-environment ss))
    (could-ever-commit-to-environment? ckb (make-environment-from-assignments ss nil))))


(defmethod could-ever-commit-to-environment? ((ckb constraint-knowledge-base-cc-wmc-ab-bdd) environment)
  "Checks to see if it would ever even be possible to commit to this environment. In our case with influence diagrams,
this means -- does there exist some assignments to the controllable decision variables, such that the resulting probability
of success would ever be greater than 0?

We can compute this using essentially the same computation as compute-conditional-probability-of-success, except without
normalizing. If the 'numerator' of that computation is > 0, then there exists some assignments of choices (found by the
dynamic programming max-sum-product algorithm) that could work.

Note that this also saves us from some trouble of normalization that is ill-defined, by sometimes trying to
marginalize on an influence diagram (IDI) without having observed all of the decision variables yet."
  (declare (type environment environment))
  (with-slots (ss node-correct bm bde idi) ckb
    (let* ((assignments-given (get-assignments-from-environment ss environment))
           node-correct-assignments-and-given
           p-assignments-and-given)

      ;; Add on any additional associated assignments
      (setf assignments-given (add-associated-assignments ckb assignments-given))

      ;; Condition the entire set of constraints (normal constraints + influence diagram constraints) on the assignments and conflicts given.
      (setf node-correct-assignments-and-given
            (bdd-and bm
                     node-correct
                     (constraint->bdd bde (environment->constraint (make-environment-from-assignments ss assignments-given) ss))))

      ;; Now compute the max sum product!
      (setf p-assignments-and-given (bdd-maxΣ∏ bm node-correct-assignments-and-given))
      ;; See if it's > 0
      (> p-assignments-and-given 0))))



(defmethod could-add-constraints-and-commit-to-environment? ((ckb constraint-knowledge-base-cc-wmc-ab-bdd) constraints environment)
  (error "Not implemented! But could be easily."))


(defmethod could-add-conflict-environments-and-commit-to-environment? ((ckb constraint-knowledge-base-cc-wmc-ab-bdd) conflicts environment)
  "Checks to see if we can commit to the given conflict environments, as well as the given assignments listed in environment, and still succeed. For this CC WMC, this is accomplished by
computing a conditional probability of success given those conflicts and environments, and checking to see if it meets our global chance constraint!"
  (with-slots (cc ss) ckb
    (let ((p-success-posterior (compute-conditional-probability-of-correctness
                                ckb
                                :conflicts conflicts
                                :assignments-given (get-assignments-from-environment ss environment))))
      (>= p-success-posterior cc))))


(defmethod can-constraints-hold? ((ckb constraint-knowledge-base-cc-wmc-ab-bdd) constraints)
  (error "Not implemented yet for WMC CC BDD solver interface"))


(defmethod process-constraints ((ckb constraint-knowledge-base-cc-wmc-ab-bdd))
  (with-slots (ss bn bm bde constraints constraints-bn node-correct node-bn choice-variable-guards idi key-toplevel node-constraints) ckb
    (time (let (all-variables weights variables-bn wmc-var->dependencies)
            ;; During execution, Riker may or may not be able to observe certain outcomes to uncontrollable choice variables.
            ;; This influences Riker's probabilistic reasoning. From the guards of each of the uncontrollable choice variables,
            ;; compute a list of sets of variables that would all be observed together. Mutually exhaustive.
            ;; (setf observation-scenarios (compute-sets-of-observed-choices ckb))
            ;; (format t "Found ~a way(s) to observe Bayesian network: ~a~%" (length observation-scenarios) observation-scenarios)

            ;; Handle the conditional nature of the problem, by compiling to an unconditional form.
            ;; When necessary, add extra values to the domain of some variables -- representing that
            ;; they're inactivated and hence unassigned -- and also get a set of constraints
            ;; associated with this.
            ;; (setf constraints-conditional (process-conditional-variables ckb))

            ;; Encode the bayesian network as a weighted model counting problem, and get the additional constraints
            ;; and variable weights required to do so.
            ;; (multiple-value-setq (weights variables-bn constraints-bn wmc-var->dependencies) (encode-bayesian-network-enc1-for-bdd bn))
            ;; (multiple-value-setq (weights variables-bn constraints-bn wmc-var->dependencies) (encode-phantom-bayesian-networks-enc1-for-bdd bn observation-scenarios choice-variable-guards))
            (multiple-value-setq (weights variables-bn constraints-bn wmc-var->dependencies) (encode-influence-diagram-enc1-for-bdd bn choice-variable-guards :encode-determinism t))

            ;; Set the variable ordering. Note that when taking into account the order
            ;; of choices in the pTPN to estimate risk, the BDD variable ordering
            ;; must match the pTPN choice ordering.

            ;; (setf all-variables (select-variable-ordering-original ckb variables-bn))
            (setf all-variables (select-variable-ordering-redalert ckb variables-bn wmc-var->dependencies))

            ;; Set up BDD encoding
            (bdd-encoder-initialize bde
                                    all-variables
                                    :use-binaries t
                                    :variable-order-heuristic :default)

            ;; Also store the weights
            (bdd-set-weights bm weights)

            ;;  Set the projection / sum product variables variables
            (let (assignments-bn
                  bdd-variables)
              (dolist (v (set-difference variables-bn
                                         (loop for v in all-variables when (controllable? v) collect v)))
                (dolist (val (decision-variable-values v))
                  (push (assignment v val) assignments-bn)))
              (setf bdd-variables (remove-duplicates (mapcar #'first
                                                             (mapcar #'(lambda (a)
                                                                         (bdd-encoder-get-literal bde a))
                                                                     assignments-bn))))

              ;; Set the sum product variables and projection variables to these (still needed for marginalization etc.)
              (bdd-set-Σ∏-variables bm bdd-variables)
              (bdd-set-projection-variables bm bdd-variables))

            ;; Encode the BN constraints
            (setf node-bn (bdd-encoder-get-variable-constraints-bdd bde))
            (dolist (c constraints-bn)
              (setf node-bn (bdd-and bm
                                     node-bn
                                     (constraint->bdd bde c))))

            ;; Compile individual constraints to BDDs
            ;; (format t "Constraints -> BDD Nodes: ~%")
            ;; (push  (pike::bdd-encoder-get-variable-constraints-bdd bde) node-constraints)
            (push node-bn node-constraints)
            (dolist (c constraints)
              (let (n)
                ;; (setf n (pike::bdd-and bm
                ;;                        node-bn ;;(pike::bdd-encoder-get-variable-constraints-bdd bde)
                ;;                        (constraint->bdd bde c)))
                (setf n (pike::constraint->bdd bde c))
                ;; (format t "  ~a  :  ~a~% (size ~a)" n c (bdd-count-children n))
                (push n node-constraints)))

            ;; Do an incremental conjunction!
            (setf key-toplevel (bdd-begin-apply bm
                                                node-constraints))

            ;; Incrementally generate the bounded BDD in an any-time fashion, checking against
            ;; the chance-constraint if desired.
            (incrementally-generate-bounded-bdd ckb key-toplevel)

            ;; Retrieve the lowerbound approximation and set it as node-correct
            (setf node-correct (bdd-compute-underapproximation bm key-toplevel))

            ;; Also set up the separate IDI class, which does inference used for normalization
            ;; and is completely separate from all of the encoding above for simplicity.
            (setf idi (make-instance 'influence-diagram-inference-bdd
                                     :ss ss
                                     :bayesian-network bn
                                     :variable-order (remove-if-not #'(lambda (v) (member v (bayesian-network-variables bn))) all-variables)))
            (initialize-for-inference idi)
            t))))



(defmethod incrementally-generate-bounded-bdd ((ckb constraint-knowledge-base-cc-wmc-ab-bdd) key &key (log-filename "data-graphs.json"))
  "The main while loop that builds up the BDD in an anytime, bounded fashion!"
  (with-slots (bm key-toplevel log-iterations? log-graphs? interval-check-bounds node-bn cc compile-to-completion?) ckb
    (let ((i 0)
          (done? (pileup:heap-empty-p (bdd-manager-queue-ops bm)))
          (ran-final-iteration? nil)
          (early-termination? nil)
          (t_0 (get-internal-real-time))
          (t_offset 0)
          d
          n-result)

      (format t "Beginning generation!~%")
      (loop while (and (not early-termination?)
                       (or (not done?)
                           (not ran-final-iteration?))) do
           (let (step-data
                 n-underapprox n-overapprox
                 wmc-underapprox wmc-overapprox)
             ;; Store the current result.
             ;; (format t "Step ~a~%" i)
             (let () ;; (t-measurement-start (get-internal-real-time))

               (when (= 0 (mod i interval-check-bounds))
                 ;; Also, compute an under and over approximation of the solution set.
                 (setf n-underapprox (bdd-compute-underapproximation bm key))
                 (setf n-overapprox (bdd-compute-overapproximation bm key))
                 ;; Compute WMC on under and over approx
                 (setf wmc-underapprox (bdd-maxΣ∏ bm (bdd-and bm
                                                              n-underapprox
                                                              node-bn)))
                 (setf wmc-overapprox (bdd-maxΣ∏ bm (bdd-and bm
                                                             n-overapprox
                                                             node-bn)))

                 (when log-iterations?
                   (setf step-data (make-hash-table :test #'equal))
                   (setf (gethash "iteration" step-data) i)
                   (setf (gethash "t" step-data) (coerce (/ (- (get-internal-real-time) t_0 t_offset) internal-time-units-per-second) 'float))

                   (setf (gethash "wmc_lowerbound" step-data) wmc-underapprox)
                   (setf (gethash "wmc_upperbound" step-data) wmc-overapprox)

                   (when log-graphs?
                     ;; Get the result
                     (setf n-result (bdd-retrieve-current-approximation bm key))
                     ;; Retrieve the graph (as dot)
                     (setf (gethash "graph" step-data)
                           (with-output-to-string (s) (bdd-dot-illustration ckb n-result s :show-op-node-children nil)))))

                 (format t "Bound: [~a, ~a]~%" (coerce wmc-underapprox 'float) (coerce wmc-overapprox 'float))

                 ;; Check to see if we've proven or disproven the chance constraint.
                 (unless compile-to-completion?
                   (when (< wmc-overapprox cc)
                     ;; Disproven!
                     (format t "Disproven chance constraint!~%")
                     (setf early-termination? t))

                   (when (>= wmc-underapprox cc)
                     ;; Proven!
                     (format t "Proven chance constraint!~%")
                     (setf early-termination? t))))

               ;; NOTE: The following offset calibration isn't perfect! Performing these measurements could
               ;; help warm up some of the BDD projection caches etc, and actually speed up its performance
               ;; a bit. This would probably be a small effect though I think.
               ;; (when account-for-measurement-time-offset
               ;;   (incf t_offset (- (get-internal-real-time) t-measurement-start)))
               t)

             ;; Store this step data
             (when (and log-iterations?
                        (= 0 (mod i interval-check-bounds)))
               (push step-data d))

             (when done?
               (setf ran-final-iteration? t))

             ;; If applicable, run again!
             (unless done?
               ;; Apply!
               (bdd-apply-once bm)
               (incf i))

             ;; Are we done yet?
             (setf done? (pileup:heap-empty-p (bdd-manager-queue-ops bm)))))

      ;; Generate the resulting JSON data
      (when log-iterations?
        (setf d (reverse d))
        (with-open-file (f log-filename :direction :output :if-exists :supersede :if-does-not-exist :create)
          (cl-json:encode-json d f))))))



(defmethod bdd-dot-illustration ((ckb constraint-knowledge-base-cc-wmc-ab-bdd) n-result s &key (show-op-node-children t) (lighten-outer-graph t))
  "Generate a dot illustration of the BDD being built."
  (with-slots (node-constraints) ckb
    (let ((node->vertex-attributes (make-hash-table))
          (node->edge-attributes (make-hash-table))
          (color-light "#e0e0e0")
          (color-transition "#a0a0a0"))

      ;; If desired, lighten the "outer" nodes -- i.e., those parts of the graph that are descendants of op nodes, but not part of the
      ;; new graph that's been constructed so far.
      (when (and lighten-outer-graph
                 show-op-node-children)
        ;; Construct the outer node set
        (let ((nodes-all (bdd-get-all-children n-result :expand-op-node-children t))
              (nodes-inner (bdd-get-all-children n-result :expand-op-node-children nil))
              (nodes-outer))

          (dolist (n nodes-all)
            (unless (member n nodes-inner)
              (push n nodes-outer)))

          ;; Color each of the outer nodes and corresponding edges
          (dolist (n nodes-outer)
            (setf (gethash n node->vertex-attributes) (format nil "color=\"~a\",fontcolor=\"~a\"" color-light color-light))
            (setf (gethash n node->edge-attributes) (format nil "color=\"~a\",fontcolor=\"~a\"" color-light color-light)))

          ;; Color transitions of and nodes to outer nodes lighter too
          (dolist (n nodes-inner)
            (when (typep n 'bdd-op-node)
              (setf (gethash n node->edge-attributes) (format nil "color=\"~a\",fontcolor=\"~a\"" color-transition color-transition))))))

      (bdd-to-dot-stream n-result
                         s
                         :show-op-node-children show-op-node-children
                         :node->vertex-attributes node->vertex-attributes
                         :node->edge-attributes node->edge-attributes))))


(defmethod compute-conditional-probability-of-correctness ((ckb constraint-knowledge-base-cc-wmc-ab-bdd) &key (assignments-given) (conflicts nil))
  "Compute and return the probability of success, given the given assignments and conflicts"
  (with-slots (ss node-correct bm bde idi choice-variable-guards) ckb
    (let* ((conflict-constraints (loop for e-conflict in conflicts collecting (environment->conflict e-conflict ss)))
           node-correct-assignments-and-given
           p-given
           p-assignments-and-given)

      ;; Add on any additional associated assignments
      (setf assignments-given (add-associated-assignments ckb assignments-given))

      ;; Condition the entire set of constraints (normal constraints + influence diagram constraints) on the assignments and conflicts given.
      (setf node-correct-assignments-and-given
            (bdd-and bm
                     node-correct
                     (constraint->bdd bde (environment->constraint (make-environment-from-assignments ss assignments-given) ss))))

      ;; Newer, I believe this might be faster:
      (dolist (conflict conflict-constraints)
        (setf node-correct-assignments-and-given
              (bdd-and bm
                       node-correct-assignments-and-given
                       (constraint->bdd bde conflict))))

      ;; Now compute the max sum product!
      (setf p-assignments-and-given (bdd-maxΣ∏ bm node-correct-assignments-and-given))

      ;; Possible early termination to save time! Don't need to normallize if 0.
      (when (= 0 p-assignments-and-given)
        (return-from compute-conditional-probability-of-correctness 0))

      ;; Compute the denominator from just the influence diagram, assuming no correctness constraints.
      ;; Version 3! Use the IDI for separation from the more complicated phantom BN stuff above.
      (setf p-given (compute-marginal-probability-of-uncontrollable-choices idi (remove-if-not #'(lambda (a)
                                                                                                   (member (assignment-variable a)
                                                                                                           (bayesian-network-variables (bayesian-network idi))))
                                                                                               assignments-given)))

      ;; (format t "Numer: ~a~%" p-assignments-and-given)
      ;; (format t "Denom: ~a~%" p-given)

      ;; Return the quotient!
      ;; Edge case for 0 / 0: return 0 if numerator is 0.
      (/ p-assignments-and-given p-given))))


(defmethod compute-conditional-marginal-probability-of-uncontrollable-choice ((ckb constraint-knowledge-base-cc-wmc-ab-bdd) assignment)
  "Computes the marginal probability, conditioned on the given observed uncontrollable variables, of the given
assignment (which is assumed to be an assignment to an uncontrollable variable). Note that this just uses the Bayesian
network, and none of the correctness constraints."
  (with-slots (idi) ckb
    (compute-conditional-marginal-probability-of-uncontrollable-choice idi assignment)))


(defmethod print-solutions-to-stream ((ckb constraint-knowledge-base-cc-wmc-ab-bdd) stream)
  t)

(defmethod get-solution-size ((ckb constraint-knowledge-base-cc-wmc-ab-bdd))
  (with-slots (node-correct) ckb
    (bdd-count-children node-correct)))
