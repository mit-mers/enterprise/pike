;;;; Copyright (c) 2018 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)

(defclass constraint-knowledge-base-bdd (constraint-knowledge-base)
  ((ss
    :type state-space-bits
    :initform (error "Must provide state space!")
    :initarg :ss
    :documentation "The state space governing variables")
   (bm
    :type bdd-manager
    :documentation "The BDD manager keeping track of the BDDs")
   (bde
    :type bdd-encoder
    :documentation "The BDD variable encoder using the bm")
   (constraint->bdd-node
    :type hash-table
    :initform (make-hash-table :test #'eql)
    :documentation "A hash table mapping each constraint to its associated BDD node")
   (node-toplevel
    :type bdd-node
    :documentation "The top-level BDD representing all constraints.")))


(defmethod initialize-instance :after ((ckb constraint-knowledge-base-bdd) &key)
  (with-slots (bm bde ss) ckb
    (setf bm (create-bdd-manager))
    (setf bde (make-bdd-encoder :ss ss
                                :bm bm))))


(defmethod add-constraint! ((ckb constraint-knowledge-base-bdd) constraint)
  "Simply store the constraint for now"
  (push constraint (constraint-knowledge-base-constraints ckb)))


(defmethod update-with-constraint! ((ckb constraint-knowledge-base-bdd) constraint)
  (with-slots (bde bm node-toplevel) ckb
    (setf node-toplevel
          (bdd-and bm
                   node-toplevel
                   (constraint->bdd bde constraint)))))


(defmethod solution-exists? ((ckb constraint-knowledge-base-bdd))
  "Check if the toplevel BDD node is satisfiable (i.e., not 0)"
  (with-slots (bm node-toplevel) ckb
    (bdd-satisfiable? bm node-toplevel)))


(defmethod could-ever-commit-to-environment? ((ckb constraint-knowledge-base-bdd) environment)
  ;; (with-slots (ss) ckb
  ;;   (format t "Env: ~a~%" (print-environment environment ss nil)))
  ;; (time (could-add-constraints-and-commit-to-environment? ckb nil environment))
  (could-add-constraints-and-commit-to-environment? ckb nil environment))


(defmethod could-add-constraints-and-commit-to-environment? ((ckb constraint-knowledge-base-bdd) constraints environment)
  "Checks to see if the given environment and constraints could be committed to. Builds a BDD
using nice cacheing mechanisms, and checks if it's satisfiable."
  (declare (optimize (speed 3)))
  (with-slots (ss node-toplevel bm bde) ckb
    (let ((n-t node-toplevel)
          bdd-env)
      ;; Add all of the constraints and the environment conjunctively
      ;; First the environment
      (setf bdd-env  (constraint->bdd bde (environment->constraint environment ss)))
      (setf n-t (bdd-and bm
                         n-t
                         bdd-env))

      ;; Then the rest of the constraints
      (dolist (constraint constraints)
        (setf n-t (bdd-and bm
                           n-t
                           (constraint->bdd bde constraint))))
      ;; Return whether the result is satisfiable
      (bdd-satisfiable? bm n-t))))


(defmethod could-add-conflict-environments-and-commit-to-environment? ((ckb constraint-knowledge-base-bdd) conflicts environment)
  (with-slots (ss) ckb
    (could-add-constraints-and-commit-to-environment? ckb
                                                      (mapcar #'(lambda (e-c)
                                                                  (environment->conflict e-c ss))
                                                              conflicts)
                                                      environment)))


(defmethod can-constraints-hold? ((ckb constraint-knowledge-base-bdd) constraints)
  "Precondition: all of the constraints have already been encoded as BDDs!"
  (with-slots (constraint->bdd-node bm bde) ckb
    (let ((n-t (bdd-encoder-get-variable-constraints-bdd bde)))
    (dolist (c constraints)
      (let ((n (gethash c constraint->bdd-node)))
        (setf n-t (bdd-and bm
                           n-t
                           n))))
    ;; Return whether this node is satisfiable.
    (bdd-satisfiable? bm n-t))))


(defmethod process-constraints ((ckb constraint-knowledge-base-bdd))
  "Encode each constraint, one by one, into a BDD. Then, form a top-level conjunction of all constraints,
allong with the variable mutex constraints."
  (with-slots (node-toplevel constraint->bdd-node constraints bm bde ss) ckb

    ;; Now that we have access to all of the constraints (or rather the offline ones at least),
    ;; select a reasonable BDD variable ordering and prepare to encode constraints.
    (bdd-encoder-initialize bde
                            (state-space-bits-variables ss)
                            :use-binaries t
                            :variable-order-heuristic :force
                            :constraints constraints)

    ;; Encode all constraints as BDDs
    (let ((constraint-nodes nil))
      (dolist (constraint constraints)
        (let ((n (constraint->bdd bde constraint)))
          (setf (gethash constraint constraint->bdd-node) n)
          (push n constraint-nodes)))

      ;; Construct a top-level BDD representing the conjunction of all of these constraints
      (setf node-toplevel (bdd-encoder-get-variable-constraints-bdd bde))
      (dolist (n constraint-nodes)
        (setf node-toplevel (bdd-and bm
                                     node-toplevel
                                     n))))

    (format t "BDD manager has ~a nodes~%" (bdd-manager-num-nodes bm))))


(defmethod print-solutions-to-stream ((ckb constraint-knowledge-base-bdd) stream)
  t)


(defmethod get-solution-size ((ckb constraint-knowledge-base-bdd))
  "How big are all the solutions? Not sure! Return -1..."
  (with-slots (node-toplevel) ckb
    (bdd-count-children node-toplevel)))
