;;;; Copyright (c) 2011-2017 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Constraint-based knowledge base
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass constraint-knowledge-base ()
  ((constraints
    :type list
    :initform nil
    :accessor constraint-knowledge-base-constraints
    :documentation "A list of all constraints added
to this knowledge base"))
  (:documentation "Super class of all constraint-based knowledge
base types."))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Public API for all constraint-based knowledge base classes
;; (Also look at the accessors for the above constraint-knowledge-base class)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric add-constraint! (ckb constraint)
  (:documentation "Add the given constraint to the ckb. May not be called after
compilation is performed."))

(defgeneric update-with-constraint! (ckb constraint)
  (:documentation "Add the additional constraint - may be called after compilation is performed."))


(defgeneric solution-exists? (ckb)
  (:documentation "Does any solution exist?"))


(defgeneric could-ever-commit-to-environment? (csb environment)
  (:documentation "Could we ever commit to this environment at any point now or in the future?"))


(defgeneric could-add-constraints-and-commit-to-environment? (csb constraints environment)
  (:documentation "Could we add the given constraints and still commit
to the given environment?"))

(defgeneric could-add-conflict-environments-and-commit-to-environment? (ckb conflicts environment)
  (:documentation "Could we add the given conflict constraints and still
commit to the given environment?"))

(defgeneric can-constraints-hold? (csb constraints)
  (:documentation "Could all of these constraints hold together and be
consistent with things added? Useful for diagnostics."))

(defgeneric process-constraints (csb)
  (:documentation "Tells the constraint knowledge base to actually
process all of the constraints, and do whatever compilation it needs to do, to
answer queries based on the constraints."))

(defgeneric print-solutions-to-stream (ckb stream)
  (:documentation "Prints some representation of the solutions (or comparable)
to the given stream. Useful for debugging and file writing."))

(defgeneric get-solution-size (ckb)
  (:documentation "Prints some representation of how large the solutions is;
for instance, number of environments in a goal label for an ATMS"))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Some helper methods
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric print-solutions (obj))
(defmethod print-solutions ((ckb constraint-knowledge-base))
  "Prints all of the solutions found by the ATMS to stdout"
  (print-solutions-to-stream ckb t))


(defgeneric print-solutions-to-file (obj filename))
(defmethod print-solutions-to-file ((ckb constraint-knowledge-base) filename)
  "Prints all of the solutions found by the ATMS to a file"
  (with-open-file (f filename :direction :output :if-exists :supersede :if-does-not-exist :create)
   (print-solutions-to-stream ckb f)))
