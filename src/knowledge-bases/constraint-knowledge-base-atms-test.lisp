;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)


(defun constraint-knowledge-base-atms-test-1 ()
  (let* ((ss (make-state-space-bits))
         (x (make-instance 'decision-variable
                           :name "x"
                           :domain (list 1 2 3)))
         (y (make-instance 'decision-variable
                           :name "y"
                           :domain (list 1 2 3)))
         (z (make-instance 'decision-variable
                           :name "z"
                           :domain (list 1 2 3)))


         (ckb (make-instance 'constraint-knowledge-base-atms :ss ss)))

    (add-variable! ss x)
    (add-variable! ss y)
    (add-variable! ss z)


    (add-constraint!
     ckb
     (make-instance 'implication-constraint
                    :implicant (make-instance 'assignment-constraint :assignment (assignment x 1))
                    :consequent (make-instance 'conjunction-constraint
                                               :conjuncts (list (make-instance 'assignment-constraint :assignment (assignment y 1))
                                                                (make-instance 'assignment-constraint :assignment (assignment z 1))))))

    ;; (add-constraint!
    ;;  ckb
    ;;  (make-instance 'assignment-constraint :assignment (assignment z 2)))


    (process-constraints ckb)

    ckb))
