;;;; Copyright (c) 2018 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)


;;;; This file implements a chance-constrained solver, effectively solving
;;;; stochastic CSP's. It functions by converting the stochastic CSP constraints
;;;; to a weighted model counting problem, by encoding the Bayesian Network
;;;; as a SAT problem with weights on the literals, and then additionally
;;;; overlaying the other constraints of the CSP. The weighted model count
;;;; will compute the probability that the constraints are satisfiable.

(defclass constraint-knowledge-base-cc-wmc-bdd (constraint-knowledge-base-cc)
  ((ss
    :type state-space-bits
    :initform (error "Must provide state space!")
    :initarg :ss
    :documentation "The state space governing variables")
   (bm
    :type bdd-manager
    :accessor get-bdd-manager
    :documentation "The BDD manager keeping track of the BDDs")
   (bde
    :type bdd-encoder
    :accessor get-bdd-encoder
    :documentation "The BDD variable encoder using the bm")
   (node-correct
    :type bdd-node
    :accessor node-correct
    :documentation "The top-level BDD node representing all constraints (including the BN ones).")
   (idi
    :initform nil
    :accessor get-influence-diagram-inference
    :documentation "A class to do influence diagram inference -- used only for convenient normalization.")
   (node-bn
    :type bdd-node
    :accessor node-bayesian-network
    :documentation "The top-level BDD node representing just the BN / WMC constraints holding (phantom).")))



(defmethod initialize-instance :after ((ckb constraint-knowledge-base-cc-wmc-bdd) &key)
  (with-slots (bm bde ss) ckb
    (setf bm (create-bdd-manager))
    (setf bde (make-bdd-encoder :ss ss
                                :bm bm))))


(defmethod add-constraint! ((ckb constraint-knowledge-base-cc-wmc-bdd) constraint)
  "Simply store the constraint for now"
  (with-slots (constraints) ckb
    (push constraint constraints)))


(defmethod update-with-constraint! ((ckb constraint-knowledge-base-cc-wmc-bdd) constraint)
  (with-slots (ss bm bde node-correct idi) ckb
    (let ((bdd-constraint (constraint->bdd bde constraint)))
      ;; Commit to the constraint in node-correct
      (setf node-correct (bdd-and bm
                                  node-correct
                                  bdd-constraint))

      ;; Compute the set of assignments
      (when (or (typep constraint 'assignment-constraint)
                (typep constraint 'conjunction-constraint))
        (let* ((assignment-constraints (cond ((typep constraint 'assignment-constraint) (list constraint))
                                             ((typep constraint 'conjunction-constraint) (conjunction-constraint-conjuncts constraint))))
               (assignments-given (mapcar #'assignment-constraint-assignment assignment-constraints)))

          (setf assignments-given (add-associated-assignments ckb assignments-given))

          ;; Now conjunctively add these assignments to the influence diagram influence subsystem
          (commit-to-assignments idi :assignments (remove-if-not #'(lambda (a)
                                                                     (member (assignment-variable a)
                                                                             (bayesian-network-variables (bayesian-network idi))))
                                                                 assignments-given)))))))


(defmethod solution-exists? ((ckb constraint-knowledge-base-cc-wmc-bdd))
  (with-slots (ss) ckb
    ;; Return if it's possible, under current circumstances, to achieve our chance constraint.
    ;;(could-add-conflict-environments-and-commit-to-environment? ckb nil (make-environment ss))
    (could-ever-commit-to-environment? ckb (make-environment-from-assignments ss nil))))


(defmethod could-ever-commit-to-environment? ((ckb constraint-knowledge-base-cc-wmc-bdd) environment)
  "Checks to see if it would ever even be possible to commit to this environment. In our case with influence diagrams,
this means -- does there exist some assignments to the controllable decision variables, such that the resulting probability
of success would ever be greater than 0?

We can compute this using essentially the same computation as compute-conditional-probability-of-success, except without
normalizing. If the 'numerator' of that computation is > 0, then there exists some assignments of choices (found by the
dynamic programming max-sum-product algorithm) that could work.

Note that this also saves us from some trouble of normalization that is ill-defined, by sometimes trying to
marginalize on an influence diagram (IDI) without having observed all of the decision variables yet."
  (declare (type environment environment))
  (with-slots (ss node-correct bm bde idi) ckb
    (let* ((assignments-given (get-assignments-from-environment ss environment))
           node-correct-assignments-and-given
           p-assignments-and-given)

      ;; Add on any additional associated assignments
      (setf assignments-given (add-associated-assignments ckb assignments-given))

      ;; Condition the entire set of constraints (normal constraints + influence diagram constraints) on the assignments and conflicts given.
      (setf node-correct-assignments-and-given
            (bdd-and bm
                     node-correct
                     (constraint->bdd bde (environment->constraint (make-environment-from-assignments ss assignments-given) ss))))

      ;; Now compute the max sum product!
      (setf p-assignments-and-given (bdd-maxΣ∏ bm node-correct-assignments-and-given))
      ;; See if it's > 0
      (> p-assignments-and-given 0))))


(defmethod could-add-constraints-and-commit-to-environment? ((ckb constraint-knowledge-base-cc-wmc-bdd) constraints environment)
  (error "Not implemented! But could be easily."))


(defmethod could-add-conflict-environments-and-commit-to-environment? ((ckb constraint-knowledge-base-cc-wmc-bdd) conflicts environment)
  "Checks to see if we can commit to the given conflict environments, as well as the given assignments listed in environment, and still succeed. For this CC WMC, this is accomplished by
computing a conditional probability of success given those conflicts and environments, and checking to see if it meets our global chance constraint!"
  (with-slots (cc ss) ckb
    (let ((p-success-posterior (compute-conditional-probability-of-correctness
                                ckb
                                :conflicts conflicts
                                :assignments-given (get-assignments-from-environment ss environment))))
      (>= p-success-posterior cc))))


(defmethod can-constraints-hold? ((ckb constraint-knowledge-base-cc-wmc-bdd) constraints)
  (error "Not implemented yet for WMC CC BDD solver interface"))


(defmethod select-variable-ordering-original ((ckb constraint-knowledge-base-cc) variables-bn)
  "Select the original variable ordering, where the main variables come in the pTPN-defined
total order, then after come all the spec variables, then after that all the theta variables."
  (with-slots (ss variable-total-ordering) ckb
    (let* (all-variables
           (variables-ptpn-ordered (copy-seq variable-total-ordering))
           (variables-spec-and-ordering (set-difference (set-difference (state-space-bits-variables ss)
                                                                        variables-ptpn-ordered)
                                                        variables-bn))
           (variables-wmc (set-difference variables-bn
                                          variables-ptpn-ordered)))

      (setf all-variables (concatenate 'list
                                       variables-ptpn-ordered
                                       variables-spec-and-ordering
                                       variables-wmc))

      ;; (format t "Computed order: ~{~a~^ ~}~%" (mapcar #'decision-variable-name all-variables))
      (format t "Computed variable ordering for all ~a variables~%" (length all-variables))
      all-variables)))


(defmethod select-variable-ordering-redalert ((ckb constraint-knowledge-base-cc) variables-bn wmc-var->dependencies)
  "Provides a better variable ordering strategy that moves theta and spec variables up in the ordering,
if possible. This can, in some circumstances, allow for a drastically more compact BDD by reusing structure."
  (with-slots (ss variable-total-ordering causal-link-support-variable-orderings) ckb
    (let* (all-variables
           (variables-ptpn-ordered (copy-seq variable-total-ordering))
           (variables-spec-and-ordering (set-difference (set-difference (state-space-bits-variables ss) ;; state space: pTPN variables + spec & ordering variables
                                                                        variables-ptpn-ordered)
                                                        variables-bn))
           (variables-wmc (set-difference variables-bn
                                          variables-ptpn-ordered))) ;; Includes theta variables, obs, "phantom" normalizing variables


      (dolist (var variables-ptpn-ordered)
        ;; Do any spec or ordering variables precede this variable?
        (let (spec-and-ordering-to-add)
          (dolist (vs variables-spec-and-ordering)
            (when (member var (gethash vs causal-link-support-variable-orderings))
              ;; Got one! We should add it.
              (push vs spec-and-ordering-to-add))
            ;; Now actually add them
            (dolist (vs spec-and-ordering-to-add)
              (push vs all-variables)
              (setf variables-spec-and-ordering (remove vs variables-spec-and-ordering)))))

        ;; Add this variable to the list of all variables
        (push var all-variables)

        ;; Search for any theta variables that have not yet been added yet, but all of the
        ;; variables in the associated have. Therefore this variable is functionally defined.
        (let (wmc-vars-to-add)
          (dolist (wmc-var variables-wmc)
            (let (dependencies found?)
              (multiple-value-setq (dependencies found?) (gethash wmc-var wmc-var->dependencies))
              (when found?
                ;; Have all dependency variables already been added to the order?
                ;; If so, this var is functionally defined now -- safe to add it!
                (when (subsetp dependencies all-variables)
                  (push wmc-var wmc-vars-to-add)))))
          ;; Add them all
          (dolist (wmc-var wmc-vars-to-add)
            (push wmc-var all-variables)
            (setf variables-wmc (remove wmc-var variables-wmc)))))

      ;; Add any remaining theta variables that weren't added already... but there shouldn't be any, right?
      (dolist (wmc-var variables-wmc)
        (push wmc-var all-variables))

      ;; Add any remaining spec and ordering variables
      (dolist (var-spec variables-spec-and-ordering)
        (push var-spec all-variables))

      (setf all-variables (reverse all-variables))

      ;; (format t "Computed REDALERT order: ~{~a~^ ~}~%" (mapcar #'decision-variable-name all-variables))
      (format t "Computed REDALERT variable ordering for all ~a variables~%" (length all-variables))

      all-variables)))


(defmethod process-constraints ((ckb constraint-knowledge-base-cc-wmc-bdd))
  (with-slots (ss bn bm bde constraints constraints-bn node-correct node-bn choice-variable-guards idi) ckb
    (time (let (all-variables weights variables-bn wmc-var->dependencies)

            ;; During execution, Riker may or may not be able to observe certain outcomes to uncontrollable choice variables.
            ;; This influences Riker's probabilistic reasoning. From the guards of each of the uncontrollable choice variables,
            ;; compute a list of sets of variables that would all be observed together. Mutually exhaustive.
            ;; (setf observation-scenarios (compute-sets-of-observed-choices ckb))
            ;; (format t "Found ~a way(s) to observe Bayesian network: ~a~%" (length observation-scenarios) observation-scenarios)

            ;; Handle the conditional nature of the problem, by compiling to an unconditional form.
            ;; When necessary, add extra values to the domain of some variables -- representing that
            ;; they're inactivated and hence unassigned -- and also get a set of constraints
            ;; associated with this.
            ;;(setf constraints-conditional (process-conditional-variables ckb))

            ;; Encode the bayesian network as a weighted model counting problem, and get the additional constraints
            ;; and variable weights required to do so.
            ;; (multiple-value-setq (weights variables-bn constraints-bn wmc-var->dependencies) (encode-bayesian-network-enc1-for-bdd bn))
            ;; (multiple-value-setq (weights variables-bn constraints-bn wmc-var->dependencies) (encode-phantom-bayesian-networks-enc1-for-bdd bn observation-scenarios choice-variable-guards))
            (multiple-value-setq (weights variables-bn constraints-bn wmc-var->dependencies) (encode-influence-diagram-enc1-for-bdd bn choice-variable-guards :encode-determinism t))

            ;; Set the variable ordering. Note that when taking into account the order
            ;; of choices in the pTPN to estimate risk, the BDD variable ordering
            ;; must match the pTPN choice ordering.

            ;; (setf all-variables (select-variable-ordering-original ckb variables-bn))
            (setf all-variables (select-variable-ordering-redalert ckb variables-bn wmc-var->dependencies))

            ;; Pepare the BDD encoding
            (bdd-encoder-initialize bde
                                    all-variables
                                    :use-binaries t
                                    :variable-order-heuristic :default)

            ;; Also store the weights
            (bdd-set-weights bm weights)

            ;;  Set the projection / sum product variables variables
            (let (assignments-bn
                  bdd-variables)
              (dolist (v (set-difference variables-bn
                                         (loop for v in all-variables when (controllable? v) collect v)))
                (dolist (val (decision-variable-values v))
                  (push (assignment v val) assignments-bn)))
              (setf bdd-variables (remove-duplicates (mapcar #'first
                                                             (mapcar #'(lambda (a)
                                                                         (bdd-encoder-get-literal bde a))
                                                                     assignments-bn))))

              ;; Set the sum product variables and projection variables to these (still needed for marginalization etc.)
              (bdd-set-Σ∏-variables bm bdd-variables)
              (bdd-set-projection-variables bm bdd-variables))

            ;; Below: possibly better for memory management if we use my recycle function?
            (setf node-bn (bdd-encoder-get-variable-constraints-bdd bde))
            (dolist (c constraints-bn)
              (setf node-bn (bdd-and bm
                                     node-bn
                                     (constraint->bdd bde c))))

            ;; Wow! Even without garbage collection, this is orders of magnitude faster...!
            (setf node-correct node-bn)
            (dolist (c constraints)
              (setf node-correct (bdd-and bm
                                          node-correct
                                          (constraint->bdd bde c))))

            (format t "BDD correct size: ~a~%" (bdd-count-children node-correct))

            ;; Also set up the separate IDI class, which does inference used for normalization
            ;; and is completely separate from all of the encoding above for simplicity.
            (setf idi (make-instance 'influence-diagram-inference-bdd
                                     :ss ss
                                     :bayesian-network bn
                                     :variable-order (remove-if-not #'(lambda (v) (member v (bayesian-network-variables bn))) all-variables)))
            (initialize-for-inference idi)

            ;; All set!
            t))))


(defgeneric compute-sets-of-observed-choices (ckb))
(defmethod compute-sets-of-observed-choices ((ckb constraint-knowledge-base-cc))
  "Computes and returns a set of sets of Bayesian network variables that would all be observed
at the same time, based on their guards in the pTPN. This is important, because Riker is able to do
probabilistic reasoning based on observations. For example, the observation of an uncontrollable
choice variable may influence Riker's reasoning for a later controllable choice. However, Riker may
not get to observe all of the uncontrollable choice variables earlier in the plan, if they're not actiated.
Thus, different candidate subplans incur different sets of uncontrollable choices that could be observed.
This method returns a list of all of such sets. Note that in the worse case, there may be exponentially many
such sets.

It operates by framing a projected model counting problem, using the guards of events associated with uncontrollable
choice variables."
  (with-slots (choice-variable-guards variable-total-ordering bn) ckb
    ;; We'll work in a completely separate state space with a BDD, so to as not change
    ;; the main one we're working in (this is an independent subproblem). We'll keep
    ;; a mapping from the original variables to the new problem variables.
    (let ((ss (make-state-space-bits))
          all-variables
          bm bde n bn-variables
          (var-ht-observed-var (make-hash-table :test #'eql))
          (observed-var-ht-var (make-hash-table :test #'eql))
          (var-ht-controllable-comes-after? (make-hash-table :test #'eql))
          constraints)

      ;; For each variable in the Bayesian network, create a corresponding
      ;; "observed" variable. These variables are binary, and represent whether
      ;; the variable will be observed or not.
      (dolist (var (bayesian-network-variables bn))
        (let ((var-observed (make-instance 'decision-variable
                                           :name (format nil "~a-observed" (decision-variable-name var))
                                           :domain (list "y" "n"))))
          (add-variable! ss var-observed)
          (push var-observed all-variables)
          (setf (gethash var var-ht-observed-var) var-observed)
          (setf (gethash var-observed observed-var-ht-var) var)))

      ;; Do similar work for the problem's variables variables, except preserve same domain.
      (dolist (var (remove-duplicates `(,@(bayesian-network-variables bn) ,@variable-total-ordering)))
        (add-variable! ss var)
        (push var all-variables))
      (setf all-variables (remove-duplicates all-variables))

      ;; Optimization: we only need to do this procedure in cases where a controllable variable comes afterwards!
      ;; It's fine to do it regardless, but we may be more efficeint this way. Mark each controllable variable in the BN
      ;; if there is a controllable variable that comes afterwards.
      (let (found-controllable?)
        (dolist (var (reverse variable-total-ordering)) ;; Any latent BN variables will implicity be a no
          (cond
            ((controllable? var)
             (setf found-controllable? t))
            (t
             ;;(setf (gethash var var-ht-controllable-comes-after?) found-controllable?)
             ;; HACK
             (setf (gethash var var-ht-controllable-comes-after?) t))))) ;; TODO ALWAYS TRUE!


      ;; Now that we have the state space set up, generate constraints using the guards.
      ;; For each uncontrollable variable, we add a constraint that it will be observed
      ;; iff its guard holds.
      (dolist (var (bayesian-network-variables bn))
        (let ((var-observed (gethash var var-ht-observed-var))
              guard found?)
          (multiple-value-setq (guard found?) (gethash var choice-variable-guards))
          (cond
            ;; If this variable has a guard, do the "iff" constraint.
            (found?
             ;; Optimization! Only add the iff constraint if there's a controllable variable that comes later in the
             ;; ordering. Otherwise, we don't need to add it, and make it always observed for efficiency.
             (cond
               ((gethash var var-ht-controllable-comes-after?)
                (push (make-instance 'equivalence-constraint
                                     :lhs (make-instance 'assignment-constraint :assignment (assignment var-observed "y"))
                                     :rhs (make-instance 'conjunction-constraint :conjuncts (loop for a in guard
                                                                                               collecting (make-instance 'assignment-constraint :assignment a))))
                      constraints))
               ;; One doesn't come after! Just make it always observed for efficiency.
               (t
                (push (make-instance 'equivalence-constraint
                                  :lhs (make-instance 'assignment-constraint :assignment (assignment var-observed "y"))
                                  :rhs (make-instance 'constant-constraint :constant t))
                   constraints))))

            ;; If this variable does not have a guard, then it is not part of the pTPN and is always
            ;; a latent variable and will never be observed. Treat it as such by making sure it is never observed in
            ;; this encoding.
            (t
             (push (make-instance 'equivalence-constraint
                                  :lhs (make-instance 'assignment-constraint :assignment (assignment var-observed "y"))
                                  :rhs (make-instance 'constant-constraint :constant nil))
                   constraints)))))

      ;; Set projection variables
      (dolist (var (bayesian-network-variables bn))
        (push (assignment (gethash var var-ht-observed-var) "y") bn-variables)) ;; Don't need the "n" case when using binaries

      ;; Start a BDD manager
      (setf bm (create-bdd-manager))

      ;; Create a BDD encoder (which connects a state space and BDD manager)
      (setf bde (make-bdd-encoder :ss ss
                                  :bm bm))

      ;; Set up the encoder
      (bdd-encoder-initialize bde
                              all-variables
                              :use-binaries t
                              :variable-order-heuristic :default ;; Hopefully doesn't matter too much...
                              :constraints constraints)

      ;; Set sum product / projection variables
      (bdd-set-projection-variables bm bn-variables)

      (setf n (bdd-encoder-get-variable-constraints-bdd bde))
      (dolist (c constraints)
        (setf n (bdd-and bm
                         n
                         (constraint->bdd bde c))))
      ;; Project away
      (setf n (bdd-project bm n))

      ;; (format t "Number of BDD nodes in n: ~a~%" (bdd-count-children n))
      ;; (format t "Projected model count: ~a~%" (bdd-wmc bm n))

      ;; Get the models as lists of assignments
      (let (models models-raw)
        (setf models-raw (bdd-enumerate-all-projected-models bm n))
        (setf models (loop for m in models-raw
                        collect (loop for (a pos?) in m when (and pos? (equal "y" (assignment-value a))) collect (gethash (assignment-variable a) observed-var-ht-var))))
        ;; (format t "Projected models: ~a~%" models)
        models))))


(defmethod compute-conditional-probability-of-correctness ((ckb constraint-knowledge-base-cc-wmc-bdd) &key (assignments-given) (conflicts nil))
  "Compute and return the probability of success, given the given assignments and conflicts"
  (with-slots (ss node-correct bm bde idi choice-variable-guards) ckb
    (let* ((conflict-constraints (loop for e-conflict in conflicts collecting (environment->conflict e-conflict ss)))
           node-correct-assignments-and-given
           p-given
           p-assignments-and-given)

      ;; Add on any additional associated assignments
      (setf assignments-given (add-associated-assignments ckb assignments-given))

      ;; Condition the entire set of constraints (normal constraints + influence diagram constraints) on the assignments and conflicts given.
      (setf node-correct-assignments-and-given
            (bdd-and bm
                     node-correct
                     (constraint->bdd bde (environment->constraint (make-environment-from-assignments ss assignments-given) ss))))

      ;; Newer, I believe this might be faster:
      (dolist (conflict conflict-constraints)
        (setf node-correct-assignments-and-given
              (bdd-and bm
                       node-correct-assignments-and-given
                       (constraint->bdd bde conflict))))

      ;; Now compute the max sum product!
      (setf p-assignments-and-given (bdd-maxΣ∏ bm node-correct-assignments-and-given))

      ;; Possible early termination to save time! Don't need to normallize if 0.
      (when (= 0 p-assignments-and-given)
        (return-from compute-conditional-probability-of-correctness 0))

      ;; Compute the denominator from just the influence diagram, assuming no correctness constraints.
      ;; Version 3! Use the IDI for separation from the more complicated phantom BN stuff above.
      (setf p-given (compute-marginal-probability-of-uncontrollable-choices idi (remove-if-not #'(lambda (a)
                                                                                                   (member (assignment-variable a)
                                                                                                           (bayesian-network-variables (bayesian-network idi))))
                                                                                               assignments-given)))

      ;; (format t "Numer: ~a~%" p-assignments-and-given)
      ;; (format t "Denom: ~a~%" p-given)

      ;; Return the quotient!
      ;; Edge case for 0 / 0: return 0 if numerator is 0.
      (/ p-assignments-and-given p-given))))


(defmethod compute-conditional-marginal-probability-of-uncontrollable-choice ((ckb constraint-knowledge-base-cc-wmc-bdd) assignment)
  "Computes the marginal probability, conditioned on the given observed uncontrollable variables, of the given
assignment (which is assumed to be an assignment to an uncontrollable variable). Note that this just uses the Bayesian
network, and none of the correctness constraints."
  (with-slots (idi) ckb
    (compute-conditional-marginal-probability-of-uncontrollable-choice idi assignment)))


(defmethod add-associated-assignments (ckb assignments-given)
  "Helper function that adds on additional associated assignments that must hold if this assignment holds.
These include:
  - Any guard conditions required for anything in assignments-given to hold
  - Any active_var variables for any assignments in assignments given."
  (with-slots (ss choice-variable-guards) ckb
    (let (assignments-from-guards)
      ;; This trick ensures that all guards must hold for all uncontrollable choices
      ;; that have evidently been observed (in that they are provided in assignments-given)
      (dolist (a assignments-given)
        (let (guard found?)
          (multiple-value-setq (guard found?) (gethash (assignment-variable a) choice-variable-guards))
          (when found?
            (setf assignments-from-guards `(,@assignments-from-guards ,@guard)))))
      (setf assignments-given `(,@assignments-given ,@assignments-from-guards))
      (setf assignments-given (remove-duplicates assignments-given))
      ;; Add in any active_var assignments
      (dolist (a assignments-given)
        (let ((active-var (find (format nil "active_~a" (decision-variable-name (assignment-variable a)))
                                (state-space-bits-variables ss)
                                :key #'decision-variable-name :test #'equal)))
          (when active-var
            (push (assignment active-var "y") assignments-given))))

      assignments-given)))


(defmethod print-solutions-to-stream ((ckb constraint-knowledge-base-cc-wmc-bdd) stream)
  t)


(defmethod get-solution-size ((ckb constraint-knowledge-base-cc-wmc-bdd))
  (with-slots (node-correct) ckb
    (bdd-count-children node-correct)))
