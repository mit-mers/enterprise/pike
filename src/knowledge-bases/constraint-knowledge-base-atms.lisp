;;;; Copyright (c) 2014-2017 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Constraint-based knowledge base
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass constraint-knowledge-base-atms (constraint-knowledge-base)
  ((ss
    :type state-space-bits
    :initform (error "Must provide state space!")
    :initarg :ss
    :documentation "The state space governing variables")
   (atms
    :type atms
    :accessor constraint-knowledge-base-atms-atms
    :documentation "The ATMS that does the reasoning")
   (atms-type
    :type symbol
    :accessor constraint-knowledge-base-atms-atms-type
    :documentation "The type of ATMS that should be used. Must be either :atms, :piatms, or :patms"
    :initarg :atms-type
    :initform :atms)
   (assignment->node
    :type hash-table
    :initform (make-hash-table)
    :documentation "Maps decision variable assignments
to their corresponding assumption nodes.")
   (variable->node
    :type hash-table
    :initform (make-hash-table)
    :documentation "Maps decision variables to the ATMS
node containing all assignments to it (as a label)")
   (constraint->node
    :initform (genhash:make-generic-hash-table :test 'pike-constraints-equal)
    :documentation "A generic hash table mapping constraints to
TMS nodes that represent them.")
   (goal-node
    :type node
    :accessor constraint-knowledge-base-atms-goal-node
    :documentation "The goal node of the ATMS")
   (encode-causal-support-variables
    :initform t
    :initarg :encode-causal-support-variables
    :documentation "A flag for whether to encode the variable mutex / disjunction
constraints for the s_{p, e_c} variables, that encode which causal link is the
supporting causal link. Setting false to ATMS like implementations preserves
correctness, but improves performance I believe."))
  (:documentation "A constraint knowledge base that uses an ATMS
as its underlying constraint solver / knowledge base."))


(defmethod initialize-instance :after ((ckb constraint-knowledge-base-atms) &key)
  "Initialize the ATMS from the SS"
  (with-slots (ss atms atms-type goal-node) ckb
    (cond
      ((eql atms-type :atms)
       (setf atms (create-atms "ATMS" :ss ss))
       (setf goal-node (tms-create-node atms "goal")))
      ((eql atms-type :piatms)
       (setf atms (create-piatms "ATMS" :ss ss))
       (setf goal-node (tms-create-node atms "goal")))
      ((eql atms-type :patms))
      (T
       (error "ATMS type for constraint-knowledge-base-atms must be either :atms or :piatms!")))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Public API calls
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defmethod add-constraint! ((ckb constraint-knowledge-base-atms) (constraint constraint))
  (with-slots (constraints) ckb

    ;; Add the constraint to the list of constraints
    (push constraint constraints)))




(defmethod update-with-constraint! ((ckb constraint-knowledge-base-atms) (constraint constraint))
  ;; Uh oh! This is not one of the specialized constraints! Throw an error.
  (error "Updating with this constraint type isn't supported!"))

(defmethod update-with-constraint! ((ckb constraint-knowledge-base-atms) (constraint conflict-constraint))
  (encode-constraint! ckb constraint))

(defmethod update-with-constraint! ((ckb constraint-knowledge-base-atms) (constraint assignment-constraint))
  (encode-constraint! ckb constraint))




(defmethod solution-exists? ((ckb constraint-knowledge-base-atms))
  "A solution exists iif the ATMS goal node has a non-empty label."
  (with-slots (goal-node) ckb
    (not (not (tms-node-label goal-node)))))






(defmethod print-solutions-to-stream ((ckb constraint-knowledge-base-atms) stream)
  "Prints all of the solutions found by the ATMS to a stream"
  (with-slots (ss goal-node) ckb
    (let ((solutions (node-label goal-node)))
      (format stream "Solutions:~%")
      (dolist (env solutions)
        (print-environment env ss stream)
        (format stream "~%"))
      (format stream "~%Found ~a solution~p." (length solutions) (length solutions)))))


(defmethod get-solution-size ((ckb constraint-knowledge-base-atms))
  "Return the number of environments in the goal label"
  (with-slots (goal-node) ckb
    (length (node-label goal-node))))


(defgeneric print-solutions-for-constraint (ckb constraint))
(defmethod print-solutions-for-constraint ((ckb constraint-knowledge-base-atms) (constraint constraint))
  "Prints the label contents (solutions) corresponding to this given constraint."
  (with-slots (ss constraint->node) ckb
    (let* ((node (genhash:hashref constraint constraint->node))
           (solutions (node-label node)))
      (format t "Solutions:~%")
      (dolist (env solutions)
        (print-environment env ss t)
        (format t "~%"))
      (format t "~%Found ~a solution~p." (length solutions) (length solutions)))))


(defmethod could-ever-commit-to-environment? ((csb constraint-knowledge-base-atms) environment)
  (declare (type environment environment))
  (with-slots (goal-node) csb
    (node-consistent-with? goal-node environment)))


(defmethod could-add-constraints-and-commit-to-environment? ((csb constraint-knowledge-base-atms)
                                                             (constraints list)
                                                             environment)
  (declare (type environment environment))
  ;; TODO
  ;; A less general but faster version (that only works for conflicts represesented as
  ;; environments) is implemented in could-add-conflict-environments-and-commit-to-environment?
  t)



(defmethod could-add-conflict-environments-and-commit-to-environment? ((csb constraint-knowledge-base-atms)
                                                                       (conflicts list)
                                                                       environment)
  (declare (type environment environment))
  (with-slots (goal-node atms-type) csb
    (cond
      ((or (eql atms-type :atms)
           (eql atms-type :patms))
       (node-consistent-with-after-conflicts? goal-node environment conflicts))
      ((eql atms-type :piatms)
       (node-consistent-with-after-conflicts?-piatms goal-node environment conflicts)))))


(defmethod can-constraints-hold? ((csb constraint-knowledge-base-atms) (constraints list))
  "Can all of these constraints hold together at once?
   Precondition: All of these constraints have already been added to the constraint knowledge base,
   and things have been processed."
  (with-slots (atms constraint->node) csb
    (let ((temp-node (tms-create-node atms "diagnostic-node"))
          (constraint-nodes (mapcar #'(lambda (c)
                                        (genhash:hashref c constraint->node))
                                    constraints)))
      (encode-justification csb
                            "diagnostics"
                            temp-node
                            constraint-nodes)

      (> (length (node-label temp-node)) 0))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Stuff
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric encode-justification (ckb reason consequent antecedents))
(defmethod encode-justification ((ckb constraint-knowledge-base-atms) reason consequent antecedents)
  "For a regular ATMS, just call call justify. (Slightly different API for a PATMS due to defstructs),
   and for piATMS."
  (with-slots (atms-type) ckb
    (cond
      ((eql atms-type :atms)
       (justify-node reason
                     consequent
                     (remove-duplicates antecedents)))
      ((eql atms-type :piatms)
       (justify-node-piatms reason
                            consequent
                            (remove-duplicates antecedents)))
      (t
       (error "Invalid ATMS type: don't know how to encode justification!'")))))


(defmethod process-constraints ((ckb constraint-knowledge-base-atms))
  (with-slots (constraints variable->node constraint->node goal-node) ckb
    ;; Set up our hash table
    (setup-constraint-hash-table ckb)

    ;; Add in the variables and mutex constraints
    ;; TODO hack! Put this back!
    (setup-variable-constraints ckb)

    ;; A quick optimization: if there are no constraints, we don't need to do
    ;; any heavy lifting! We know any assignment to variables will be a solution.
    ;;(when (= (length constraints) 0)
    ;;  ;; Set the goal node to be a premise (i.e., always true)
    ;;  (encode-justification ckb
    ;;                        "the-big-one-premise"
    ;;                        goal-node
    ;;                        nil)
    ;;  (return-from process-constraints))

    ;; Remove any duplicate constraints here
    (setf constraints (remove-duplicates constraints :test #'equal-constraints))

    ;; For efficiency, we want to add constraints that will likely
    ;; result in conflicts first. This will make the rest of the ATMS
    ;; encoding process more efficient, since we won't have to propagate
    ;; nogoods as much.

    ;; Sort the constraints in a good order, prioritizing constraints
    ;; that will result in conflicts first.
    (setf constraints (sort constraints
                            #'<
                            :key (lambda (c)
                                   (cond
                                     ((typep c 'assignment-constraint) 0)
                                     ((typep c 'conflict-constraint) 1)
                                     (T 2)))))

    ;; Now, encode each constraint in order.
    (dolist (c constraints)
      (encode-constraint! ckb c))

    ;; Finally, assemble the final goal node.
    (let (goal-antecedents)
      ;; All of the variable nodes
      ;; TODO - is the below line really needed??
      (loop for variable being the hash-keys of variable->node do
           (push (gethash variable variable->node) goal-antecedents))

      ;; Any constraint with non-nil node (i.e., not conflicts):
      (dolist (c constraints)
        (let ((node (genhash:hashref c constraint->node)))
          (when node
            (push node goal-antecedents))))

      ;; Create the big justification
      (encode-justification ckb
                            "the-big-one"
                            goal-node
                            goal-antecedents))))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Encoding constraints in to the ATMS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun setup-constraint-hash-table (ckb)
  "Helper function to set up the generic hash table
   for better constraint hashing -- attempts to find
   duplicate constraints, and reuse TMS nodes to
   reduce label propagation"
  (with-slots (constraints constraint->node) ckb
    (setf constraint->node (genhash:make-generic-hash-table :test 'pike-constraints-equal
                                                            :size (+ 17 (* 3 (length constraints)))))))


(defun setup-variable-constraints (ckb)
  "Adds assumptions to the ATMS corresponding to each variable assignment,
   like x=1 or y=2, and adds corresponding mutex constraints between them
   (i.e., x=1 and x=2 can't both hold). Also adds a node so that at least
   one of the assignments will hold."
  (with-slots (ss atms assignment->node variable->node encode-causal-support-variables) ckb
    (dolist (variable (state-space-bits-variables ss))
      (let (assignment-nodes
            contra-node
            variable-node)
        ;; Create an assumption node for each variable assignment, like x=1
        (dolist (value (decision-variable-values variable))
          (let* ((assignment (assignment variable value))
                 (assignment-node (tms-create-node atms assignment :assumptionp assignment)))
            (push assignment-node assignment-nodes)
            (setf (gethash assignment assignment->node) assignment-node)))


        ;; TODO hack!! Make this cleaner! Don't do this if it's a special s_p_* "flag" variable.
        ;; Helps with normal ATMS encoding. Should generalize and think about when this allowable
        ;; or not. For instance, is this acceptable for binary variables that only appear positively?
        (when (or encode-causal-support-variables
                  (not (search "s_{" (decision-variable-name variable))))
          ;; Now, create mutex constraints between all pairs of assignments.
          ;; But only if there's more than one possible domain value!
          (when (> (length assignment-nodes) 1)
            ;; First, create a contradiction node.
            (setf contra-node (tms-create-node atms (format nil "assignment mutex for ~a" variable) :contradictoryp T))
            ;; Then, iterate over all pairs (order doesn't matter) of the assignment
            ;; assumption nodes, and connect them to the this contradiction node.
            (do-pairs (v1 v2 assignment-nodes)
              (encode-justification ckb
                                    "Variable mutex"
                                    contra-node
                                    (list v1 v2))))

          ;; Finally, create a new variable node that requires
          ;; at least one of the assignments for each variable to be chosen.
          (setf variable-node (tms-create-node atms variable))
          (dolist (assignment-node assignment-nodes)
            (encode-justification ckb
                                  "Variable"
                                  variable-node
                                  (list assignment-node)))
          ;; And keep track of it in our hash table.
          (setf (gethash variable variable->node) variable-node))))))


;; TODO - should we generalize this to something more of a negation? Basically, if there's a top-level
;; negation, turn it into a conflict?
(defgeneric encode-constraint! (ckb c)
  (:documentation "Encodes a constraint in the TMS - the main entry method. It calls constraint->node! recursively.
If the system is already being run online, call update-with-constraint! instead. "))

(defmethod encode-constraint! ((ckb constraint-knowledge-base-atms) (c conflict-constraint))
  "Don't actually create an ATMS node for this conflict; rather, encode it through
   no-goods! Hence, deliberately doesn't call constraint->node! on this conflict."
  (with-slots (atms) ckb
    (let ((contra-node (tms-create-node atms "conflict" :contradictoryp T)))
      (encode-justification ckb
                            "conflict"
                            contra-node
                            (loop for c-i in (conflict-constraint-expressions c)
                               collect (constraint->node! ckb c-i))))))


(defmethod encode-constraint! ((ckb constraint-knowledge-base-atms) (c assignment-constraint))
  "For assignment constraints, we should additionally make all other assignments nogoods."
  (with-slots (atms assignment->node) ckb
    (let* ((assignment (assignment-constraint-assignment c))
           (variable (assignment-variable assignment))
           (value (assignment-value assignment))
           (contra-node (tms-create-node atms "assignment" :contradictoryp T)))
      (loop for val in (decision-variable-values variable)
         when (not (eql val value)) do
           (encode-justification ckb
                                 "assignment"
                                 contra-node
                                 (list (gethash (assignment variable val) assignment->node))))))
  ;; Call the next method to ensure that this assignment
  ;; gets added to the generic hash table
  (call-next-method ckb c))


;; TODO - should we do something similar to the above for conjunctions, if they have any
;; assignment-constraints in their conjuncts? Would probably make things more efficient.

;; TODO - similar to the above - if we have a negation that contains inside of it a conjunction, that
;; contains only assignments - it's effectively a constant. Encode it as such


(defmethod encode-constraint! ((ckb constraint-knowledge-base-atms) (c constraint))
  "For every other type of constraint, represent it with an ATMS node and
   store a reference in our hash table."
  (constraint->node! ckb c))


;; TODO - we can potentially save a lot of encoding time here, by caching
;; and reusing nodes that appear in multiple constraints, so that we don't
;; need to keep recomputing the same (possibly costly) label.



;; The contraint->node! series of function takes in a constraint knowledge base, and a constraint
;; c, and returns a node that corresponds to that constraint.
;;
;; Internally, these use a generic hash table, to map constraints to nodes. Nodes may be reused
;; if queried with an identical constraint. The constraint / node pair is added to the hash table
;; every time this function is called, allowing for this reuse / caching mechanism.
(defgeneric constraint->node! (ckb c))
(defmethod constraint->node! ((ckb constraint-knowledge-base-atms) (c assignment-constraint))
  "For an assignment constraint, simply lookup the assignment assumption node. Also ensure that
   it exists in the constraint->node has table, so that it will properly get encoded until the
   big final constraint later if needed."
  (with-slots (assignment->node constraint->node) ckb
    (let ((node (gethash (assignment-constraint-assignment c) assignment->node)))
      (setf (genhash:hashref c constraint->node) node)
      node)))



(defmethod constraint->node! ((ckb constraint-knowledge-base-atms) (c constant-constraint))
  "For a constant constraint, simply return a node that is always justified for True,
   and a node that is never justified for False."
  (with-slots (atms constraint->node) ckb
    (let (node found?)
      (multiple-value-setq (node found?) (genhash:hashref c constraint->node))
      (when found?
        (return-from constraint->node! node))
      ;; Constraint wasn't found, to set it up
      (setf node (tms-create-node atms c))
      (setf (genhash:hashref c constraint->node) node)
      (when (constant-constraint-constant c)
        (encode-justification ckb
                              "true!"
                              node
                              nil))
      node)))


(defmethod constraint->node! ((ckb constraint-knowledge-base-atms) (c conjunction-constraint))
  "For a conjunction constraint, recursively get the nodes for all conjuncts
   and put them together in a justification.

   This works because the return node will ONLY have this single justification
   going into it - thus it will be true iif the conjunction of nodes holds."
  (with-slots (atms constraint->node) ckb
    (let (node found?)
      (multiple-value-setq (node found?) (genhash:hashref c constraint->node))
      (when found?
        (return-from constraint->node! node))
      ;; Constraint wasn't found, to set it up
      (setf node (tms-create-node atms c))
      (setf (genhash:hashref c constraint->node) node)
      (encode-justification ckb
                            "conjunction"
                            node
                            (loop for c-i in (conjunction-constraint-conjuncts c)
                              collecting (constraint->node! ckb c-i)))
      node)))


(defmethod constraint->node! ((ckb constraint-knowledge-base-atms) (c disjunction-constraint))
  "For a disjunction constraint, recursively get disjuncts and make each one
   a justification."
  (with-slots (atms constraint->node) ckb
    (let (node found?)
      (multiple-value-setq (node found?) (genhash:hashref c constraint->node))
      (when found?
        (return-from constraint->node! node))
      ;; Constraint wasn't found, to set it up
      (setf node (tms-create-node atms c))
      (setf (genhash:hashref c constraint->node) node)
      (dolist (c-i (disjunction-constraint-disjuncts c))
        (encode-justification ckb
                              "disjunction"
                              node
                              (list (constraint->node! ckb c-i))))
      node)))


(defmethod constraint->node! ((ckb constraint-knowledge-base-atms) (c negation-constraint))
  "Negations are the most complicated ones to encode!"
  (with-slots (atms constraint->node) ckb
    (let (node found?)
      (multiple-value-setq (node found?) (genhash:hashref c constraint->node))
      (when found?
        (return-from constraint->node! node))
      ;; Constraint wasn't found, to set it up
      (with-slots (expression) c
        (cond
          ;; Negation of an assignment constraint is the disjunction
          ;; of all the other assignments to that variable.
          ((typep expression 'assignment-constraint)
           (let* ((dva (assignment-constraint-assignment expression))
                  (variable (assignment-variable dva))
                  (value (assignment-value dva)))
             (setf node (constraint->node!
                         ckb
                         (make-instance 'disjunction-constraint
                                        :disjuncts (loop for val in (decision-variable-values variable)
                                                      when (not (eql val value)) collect
                                                        (make-instance 'assignment-constraint
                                                                       :assignment (assignment variable val))))))))

          ;; Apply De Morgan's law to move the negation inwards, closer to the leaves
          ;; of the expression, which will eventually be assignments.
          ((typep expression 'conjunction-constraint)
           (setf node (constraint->node!
                       ckb
                       (make-instance 'disjunction-constraint
                                      :disjuncts (loop for c-i in (conjunction-constraint-conjuncts expression)
                                                    collecting (make-instance 'negation-constraint
                                                                              :expression c-i))))))

          ;; Apply De Morgan's law to move the negation inwards, closer to the leaves
          ;; of the expression, which will eventually be assignments.
          ((typep expression 'disjunction-constraint)
           (setf node (constraint->node!
                       ckb
                       (make-instance 'conjunction-constraint
                                      :conjuncts (loop for c-i in (disjunction-constraint-disjuncts expression)
                                                    collecting (make-instance 'negation-constraint
                                                                              :expression c-i))))))

          ;; A negation of a negation is the thing itself
          ((typep expression 'negation-constraint)
           (setf node (constraint->node! ckb (negation-constraint-expression expression))))

          ;; not(x => y) is logically equivalant to x and not(y)
          ((typep expression 'implication-constraint)
           (setf node (constraint->node!
                       ckb
                       (make-instance 'conjunction-constraint
                                      :conjuncts (list (implication-constraint-implicant expression)
                                                       (make-instance 'negation-constraint
                                                                      :expression (implication-constraint-consequent expression)))))))

          ;; not(x <=> y) is logically equivalant to (x and not(y)) or (y and not(x))
          ((typep expression 'equivalence-constraint)
           (setf node (constraint->node!
                       ckb
                       (make-instance 'disjunction-constraint
                                      :disjunct (list (make-instance 'conjunction-constraint
                                                                     :conjuncts (list (equivalence-constraint-lhs expression)
                                                                                      (make-instance 'negation-constraint
                                                                                                     :expression (equivalence-constraint-rhs expression))))
                                                      (make-instance 'conjunction-constraint
                                                                     :conjuncts (list (make-instance 'negation-constraint
                                                                                                     :expression (equivalence-constraint-lhs expression))
                                                                                      (equivalence-constraint-rhs expression))))))))

          ;; A conflict is the negation of a conjunction, so the negation of a
          ;; conflict is just that conjunction.
          ((typep expression 'conflict-constraint)
           (setf node (constraint->node!
                       ckb
                       (make-instance 'conjunction-constraint
                                      :conjuncts (loop for c-i in (conflict-constraint-expressions expression)
                                                    collecting c-i)))))

          ;; The negation of True is False, and the negation of
          ;; False is True
          ((typep expression 'constant-constraint)
           (setf node (constraint->node!
                       ckb
                       (make-instance 'constant-constraint
                                      :constant (not (constant-constraint-constant expression))))))

          (T
           (error "Unknown type, can't negate it!")))
        ;; Remember this node / constraint pair. The node will now be associated with
        ;; more than one constraint in the hash table, but that's ok
        (setf (genhash:hashref c constraint->node) node)
        node))))


(defmethod constraint->node! ((ckb constraint-knowledge-base-atms) (c conflict-constraint))
  "Encode a conflict as the negation of the conjunction."
  (with-slots (constraint->node) ckb
    (let (node found?)
      (multiple-value-setq (node found?) (genhash:hashref c constraint->node))
      (when found?
        (return-from constraint->node! node))
      ;; Constraint wasn't found, so set it up
      (setf node (constraint->node!
                  ckb
                  (make-instance 'negation-constraint
                                 :expression (make-instance 'conjunction-constraint
                                                            :conjuncts (loop for c-i in (conflict-constraint-expressions c)
                                                                          collecting c-i)))))
      ;; Remember this node / constraint pair. The node will now be associated with
      ;; more than one constraint in the hash table, but that's ok
      (setf (genhash:hashref c constraint->node) node)
      node)))



(defmethod constraint->node! ((ckb constraint-knowledge-base-atms) (c implication-constraint))
  "Encode an implication like x => y as not(x) or y."
  (with-slots (constraint->node) ckb
    (let (node found?)
      (multiple-value-setq (node found?) (genhash:hashref c constraint->node))
      (when found?
        (return-from constraint->node! node))
      ;; Constraint wasn't found, so set it up
      (setf node (constraint->node!
                  ckb
                  (make-instance 'disjunction-constraint
                                 :disjuncts (list (make-instance 'negation-constraint
                                                                 :expression (implication-constraint-implicant c))
                                                  (implication-constraint-consequent c)))))

       ;; Remember this node / constraint pair. The node will now be associated with
      ;; more than one constraint in the hash table, but that's ok
      (setf (genhash:hashref c constraint->node) node)
      node)))



(defmethod constraint->node! ((ckb constraint-knowledge-base-atms) (c equivalence-constraint))
  "Encode an equivalence constraint like x <=> y as (x and y) or (not x and not y)"
  (with-slots (constraint->node) ckb
    (let (node found?)
      (multiple-value-setq (node found?) (genhash:hashref c constraint->node))
      (when found?
        (return-from constraint->node! node))
      ;; Constraint wasn't found, so set it up
      (setf node (constraint->node!
                  ckb
                  (make-instance 'disjunction-constraint
                                 :disjuncts (list (make-instance 'conjunction-constraint
                                                                 :conjuncts (list (equivalence-constraint-lhs c)
                                                                                  (equivalence-constraint-rhs c)))
                                                  (make-instance 'conjunction-constraint
                                                                 :conjuncts (list (make-instance 'negation-constraint
                                                                                                 :expression (equivalence-constraint-lhs c))
                                                                                  (make-instance 'negation-constraint
                                                                                                 :expression (equivalence-constraint-rhs c))))))))

      ;; Remember this node / constraint pair. The node will now be associated with
      ;; more than one constraint in the hash table, but that's ok
      (setf (genhash:hashref c constraint->node) node)
      node)))
