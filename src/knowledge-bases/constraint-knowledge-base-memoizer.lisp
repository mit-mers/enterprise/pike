;;;; Copyright (c) 2017 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Constraint-based knowledge base using a PATMS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass constraint-knowledge-base-memoizer (constraint-knowledge-base)
  ((ckb-internal
    :initform (error "Must provide internal constraint solver!")
    :type constraint-knowledge-base
    :initarg :ckb-internal
    :accessor ckb-internal
    :documentation "The internal constraint knowledge base solver to use!")
   (ss
    :type state-space-bits
    :initform (error "Must provide state space!")
    :initarg :ss
    :documentation "The state space governing variables")
   (L-env-maximal-yes
    :type list
    :initform nil
    :documentation "A *maximal* set of environments (each represents all its subsets)
that can successfully be committed to.")
   (L-env-minimal-no
    :type list
    :initform nil
    :documentation "A *minimal* set of environments (each represents all its supersets)
that can NOT successfully be committed to.")
   (cache-conflicts-with-env
    :type list
    :initform nil
    :documentation "A list of pairs, where each pair contains three things: a set of conflicts, an environment, and a yes/no as to whether this
can be committed to")
   (cache-could-ever-commit-to-env
    :type hash-table
    :initform (make-hash-table :test #'equal))
   (cache-hits
    :type number
    :initform 0
    :documentation "Stats: keeps the number of cache hits")
   (cache-misses
    :type number
    :initform 0
    :documentation "Stats: keeps the number of cache misses (when the internal solver was called)")
   (cache-clears
    :type number
    :initform 0
    :documentation "Stats: the number of times the cache was invalidated"))
  (:documentation "A constraint knowledge base that attempts to cache solutions, or
pass off to an internal solver"))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Public API calls
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod add-constraint! ((ckb constraint-knowledge-base-memoizer) constraint)
  "Pass through. Don't need to invalidate the cache, because preprocess shouldn't have been called yet"
  (with-slots (constraints) ckb
    (push constraint constraints))
  (add-constraint! (ckb-internal ckb) constraint))


(defmethod update-with-constraint! ((ckb constraint-knowledge-base-memoizer) constraint)
  "Pass through and invalidate the cache"
  (invalidate-memoizations ckb)
  (with-slots (constraints) ckb
    (push constraint constraints))
  ;; (format t "~%Updated with constraint: ~a~%~%" constraint)
  (update-with-constraint! (ckb-internal ckb) constraint))


(defmethod solution-exists? ((ckb constraint-knowledge-base-memoizer))
  "Check if we can commit to the empty environment"
  (with-slots (ss) ckb
    ;; NOTE: BELOW DOES NOT WORK WITH CC-WMC type algorithms! TODO FIXME
    ;; (could-add-conflict-environments-and-commit-to-environment? ckb nil (make-environment ss))
    ;; Fixed to below
    (could-ever-commit-to-environment? ckb (make-environment-from-assignments ss nil))))


(defmethod could-ever-commit-to-environment? ((ckb constraint-knowledge-base-memoizer) phi)
  "Checks to see if we can commit to the given environment. Uniquely uses both a *maximal* set of environments, and
a *minimal* set of environments, to bound from both sides compactly. If we we can't prove or disprove, call the internal
method and then cache its result."
  ;; (with-slots (L-env-maximal-yes L-env-minimal-no cache-hits cache-misses ss) ckb
  ;;   ;; Phase I: Optimism! Attempt to prove that the answer is yes. This will be the case if the environment
  ;;   ;; subsumes any of the environments in L-env-maximal-yes
  ;;   (when (some #'(lambda (phi-i)
  ;;                   (declare (type environment phi-i))
  ;;                   (subsumes? phi phi-i ss))
  ;;               L-env-maximal-yes)
  ;;     ;; We've proven yes!
  ;;     (incf cache-hits)
  ;;     (return-from can-commit-to-environment? t))

  ;;   ;; Phase II: We haven't proven yes, so attempt to prove no. If any of the environments in L-env-minimal-no
  ;;   ;; subsume phi, then we've proven no.
  ;;   (when (some #'(lambda (phi-i)
  ;;                   (declare (type environment phi-i))
  ;;                   (subsumes? phi-i phi ss))
  ;;               L-env-minimal-no)
  ;;     (incf cache-hits)
  ;;     (return-from can-commit-to-environment? nil))

  ;;   ;; Phase III: We haven't been able to prove either way! So call the internal solver, and then
  ;;   ;; cache its result.
  ;;   (incf cache-misses)
  ;;   (let ((result (can-commit-to-environment? (ckb-internal ckb) phi)))
  ;;     ;; Update accordingly
  ;;     (if result
  ;;         (setf L-env-maximal-yes (add-maximal L-env-maximal-yes phi ss))
  ;;         (setf L-env-minimal-no  (add-minimal L-env-minimal-no  phi ss)))
  ;;     ;; Return the result
  ;;     result))

  ;; The above logic is great for monotonic reaasoning! But I don't think it
  ;; is correct when we're using Bayesian inference / chance-constrained execution.
  ;; 3/9/2018: The below is also incorrect!
  ;; (could-add-conflict-environments-and-commit-to-environment? ckb nil phi)
  ;; The below should work for non-monotonic reasoning with probabilistic inference:
  ;; By "non-monotonic", we mean that, as execution proceeds, is it possible for this function
  ;; to ever change it's answer from no to yes? Probabilistic settings can in general, because of Bayesian updates.
  ;; Consistency-based approaches cannot -- once they are no, they are always no. If a theory is unsatisfiable, after adding
  ;; additional constraints it will remain unsatisfiable. The only case where a probabilistic setting can guarantee that
  ;; an environment will never be committed to, is if it has probability 0. Then noramlizing can't change anything (0 / anything is still 0).
  (with-slots (cache-could-ever-commit-to-env cache-hits cache-misses) ckb
    (let (result found?)
      (multiple-value-setq (result found?) (gethash phi cache-could-ever-commit-to-env))
      (when found?
        ;; Found it!
        (incf cache-hits)
        (return-from could-ever-commit-to-environment? result))

      ;; Not a cache hit
      (incf cache-misses)
      (setf result (could-ever-commit-to-environment? (ckb-internal ckb) phi))
      (setf (gethash phi cache-could-ever-commit-to-env) result)
      result)))


(defmethod could-add-constraints-and-commit-to-environment? ((ckb constraint-knowledge-base-memoizer) constraints environment)
  "Just pass through -- not worth the effort right now"
  (could-add-constraints-and-commit-to-environment? (ckb-internal ckb) constraints environment))


(defmethod could-add-conflict-environments-and-commit-to-environment? ((ckb constraint-knowledge-base-memoizer) conflicts phi)
  "Check if these arguments precisely match any pairs that we've memoized"
  (with-slots (cache-conflicts-with-env cache-hits cache-misses) ckb
    (loop for (Cis phi-i result) in cache-conflicts-with-env do
         (when (and (sets-equal Cis conflicts :test #'equal)
                    (equal phi phi-i))
           ;; Cache hit! Return the result
           (incf cache-hits)
           (return-from could-add-conflict-environments-and-commit-to-environment? result)))

    ;; Didn't find the result; call the internal solver and cache the result
    (incf cache-misses)
    (let ((result (could-add-conflict-environments-and-commit-to-environment? (ckb-internal ckb) conflicts phi)))
      ;; Cache this result
      (push (list conflicts phi result) cache-conflicts-with-env)
      ;; Return the result
      result)))


(defmethod can-constraints-hold? ((ckb constraint-knowledge-base-memoizer) constraints)
  "Pass through -- this could be cached, if we deem it important enough"
  (can-constraints-hold? (ckb-internal ckb) constraints))


(defmethod process-constraints ((ckb constraint-knowledge-base-memoizer))
   "Pass through."
   (process-constraints (ckb-internal ckb)))


(defmethod print-solutions-to-stream ((ckb constraint-knowledge-base-memoizer) stream)
  "Pass off to internal solver"
  (print-solutions-to-stream (ckb-internal ckb) stream))


(defmethod get-solution-size ((ckb constraint-knowledge-base-memoizer))
  "Pass this off"
  (get-solution-size (ckb-internal ckb)))

(defmethod compute-conditional-probability-of-correctness ((ckb constraint-knowledge-base-memoizer) &key (assignments-given) (conflicts nil))
  "Pass through"
  (compute-conditional-probability-of-correctness (ckb-internal ckb)
                                                  :assignments-given assignments-given
                                                  :conflicts conflicts))

(defmethod compute-conditional-marginal-probability-of-uncontrollable-choice ((ckb constraint-knowledge-base-memoizer) assignment)
  "Pass through"
  (compute-conditional-marginal-probability-of-uncontrollable-choice (ckb-internal ckb) assignment))


(defmethod chance-constraint ((ckb constraint-knowledge-base-memoizer))
  "Pass through"
  (chance-constraint (ckb-internal ckb)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Internal methods
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun invalidate-memoizations (ckb)
  "Helper function to invalidate the cache / memoizations"
  (with-slots (L-env-maximal-yes L-env-minimal-no cache-conflicts-with-env cache-could-ever-commit-to-env cache-clears) ckb
    (setf L-env-maximal-yes nil)
    ;; TODO: We can actually probably cleverly not reset the L-env-minimal-no cache sometimes?
    ;; This is because if we add a new constraint, we can only *remove* possible solutions. So L-env-maximal-yes
    ;; should shrink (and be invalidated), but L-env-minimal-no could only grow, so it should be good?
    (setf L-env-minimal-no nil)
    (setf cache-conflicts-with-env nil)
    (clrhash cache-could-ever-commit-to-env)
    ;; Keep some stats for fun
    (incf cache-clears)))
