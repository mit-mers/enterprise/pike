;;;; Copyright (c) 2017 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)


;;;; This file implements a chance-constrained solver, effectively solving
;;;; stochastic CSP's. It functions by converting the stochastic CSP constraints
;;;; to a weighted model counting problem, by encoding the Bayesian Network
;;;; as a SAT problem with weights on the literals, and then additionally
;;;; overlaying the other constraints of the CSP. The weighted model count
;;;; will compute the probability that the constraints are satisfiable.

(defclass constraint-knowledge-base-cc-wmc-ddnnf (constraint-knowledge-base-cc)
  ((ss
    :type state-space-bits
    :initform (error "Must provide state space!")
    :initarg :ss
    :documentation "The state space governing variables")
   (wmc
    :initform nil
    :accessor constraint-knowledge-base-cc-wmc-dnnf-wmc
    :documentation "A weighted model counter instance")
   (bni
    :initform nil
    :accessor constraint-knowledge-base-cc-wmc-dnnf-bni
    :documentation "A wrapper class for doing inference just
on the Bayesian network (also uses weighted model counting internally)")
   (all-conflicts
    :initform nil
    :type list
    :documentation "A list of possible conflicts that could be committed to online. Each
conflict is represented as an environment.")
   (conflict-environment->assignment
    :initform (make-hash-table :test #'equal)
    :type hash-table
    :documentation "A mapping from conflict environments, to assignments
to auxiliary variables that have been encoded to represent them.")
   (x-correct
    :initform (make-instance 'decision-variable
                             :name "x_correct"
                             :domain `("1" "0")
                             :controllable? t)
    :type decision-variable
    :documentation "A decision variable, which will be true if all
of the supplied constraints hold (doesn't count the auxiliary conflict constraints)")))


(defmethod initialize-instance :after ((ckb constraint-knowledge-base-cc-wmc-ddnnf) &key)

  t)


(defmethod add-constraint! ((ckb constraint-knowledge-base-cc-wmc-ddnnf) constraint)
  "Simply store the constraint for now"
  (with-slots (constraints) ckb
    (push constraint constraints)))



(defmethod update-with-constraint! ((ckb constraint-knowledge-base-cc-wmc-ddnnf) constraint)
  "Will be triggered if we're updating with some type of constraint that isn't
an assignment nor a conflict."
  (error "Can't update online with this type of constraint!"))


(defmethod update-with-constraint! ((ckb constraint-knowledge-base-cc-wmc-ddnnf) (c assignment-constraint))
  (with-slots (wmc bni) ckb
    (let ((a (assignment-constraint-assignment c)))
      (update-cc-wmc-ddnnf-with-assignments ckb `(,a)))))


(defmethod update-with-constraint! ((ckb constraint-knowledge-base-cc-wmc-ddnnf) (c conflict-constraint))
  "Commit to the given conflict."
  (with-slots (wmc bni ss conflict-environment->assignment) ckb
    (let* ((conflict-environment (conflict->environment c ss))
           x-aux-assignment found?)
      ;; Look up the assignment corresponding to this conflict in our mapping
      (multiple-value-setq (x-aux-assignment found?) (gethash conflict-environment conflict-environment->assignment))
      (assert found? nil "Can't update and commit to environment ~a; not initially encoded in WMC!" (print-environment conflict-environment ss nil))
      ;; Commit to this assignment.
      (update-cc-wmc-ddnnf-with-assignments ckb `(,x-aux-assignment)))))


(defmethod update-cc-wmc-ddnnf-with-assignments ((ckb constraint-knowledge-base-cc-wmc-ddnnf) assignments-all)
  (with-slots (wmc bni) ckb
    (let ((assignments-uncontrollable (loop for a in assignments-all
                                         when (not (controllable? (assignment-variable a)))
                                         collect a)))
      (condition-on-assignments wmc :assignments assignments-all)
      (condition-on-assignments bni :assignments assignments-uncontrollable))))


(defmethod solution-exists? ((ckb constraint-knowledge-base-cc-wmc-ddnnf))
  (with-slots (ss) ckb
    ;; Return if it's possible, under current circumstances, to achieve our chance constraint.
    (could-add-conflict-environments-and-commit-to-environment? ckb nil (make-environment ss))))



(defmethod could-ever-commit-to-environment? ((ckb constraint-knowledge-base-cc-wmc-ddnnf) environment)
  "To check if we can commit to this environment, create a copy
of the given SAT CNF encoding, and then add additional "
  (declare (type environment environment))
  ;; (could-add-conflict-environments-and-commit-to-environment? ckb nil environment)
  (with-slots (cc ss) ckb
    (let ((p-success-posterior (compute-conditional-probability-of-correctness
                                ckb
                                :conflicts nil
                                :assignments-given (get-assignments-from-environment ss environment))))
      (> p-success-posterior 0))))



(defmethod could-add-constraints-and-commit-to-environment? ((ckb constraint-knowledge-base-cc-wmc-ddnnf) constraints environment)
  (error "Not implemented!"))


(defmethod could-add-conflict-environments-and-commit-to-environment? ((ckb constraint-knowledge-base-cc-wmc-ddnnf) conflicts environment)
  "Checks to see if we can commit to the given conflict environments, as well as the given assignments listed in environment, and still succeed. For this CC WMC, this is accomplished by
computing a conditional probability of success given those conflicts and environments, and checking to see if it meets our global chance constraint!"
  (with-slots (cc ss) ckb
    (let ((p-success-posterior (compute-conditional-probability-of-correctness
                                ckb
                                :conflicts conflicts
                                :assignments-given (get-assignments-from-environment ss environment))))
      (>= p-success-posterior cc))))


(defmethod can-constraints-hold? ((ckb constraint-knowledge-base-cc-wmc-ddnnf) constraints)
  (error "Not implemented yet for WMC CC solver interface"))

(defmethod process-constraints ((ckb constraint-knowledge-base-cc-wmc-ddnnf))
  (with-slots (ss wmc bni bn constraints all-conflicts conflict-environment->assignment x-correct) ckb
    (let ((i 0)
          (constraints-to-encode nil)
          (conflict-variables nil))

      ;; Add a special overall constraint, which will be true if and only if
      ;; all of the supplied constraints hold. Doesn't count the auxiliary variables /
      ;; conflict constraints -- those are encoded below.
      (push (make-instance 'equivalence-constraint
                           :lhs (make-instance 'assignment-constraint
                                               :assignment (assignment x-correct "1"))
                           :rhs (make-instance 'conjunction-constraint
                                               :conjuncts constraints))
            constraints-to-encode)


      ;; Immediately commit to x-correct = 1 in the WMC -- we'll do normalization
      ;; via the separate Bayesian Network Inference (BNI) class.
      (push (make-instance 'assignment-constraint
                           :assignment (assignment x-correct "1"))
            constraints-to-encode)


      ;; Hand the conflicts. Specifically, we take each conflict environment, create a new
      ;; decision variable called x_aux_i, and create a constraint that x_aux_i=1 => conflict_i.
      ;; All of these constraints will be encoded into the WMC encoding. This is because the WMC
      ;; uses d-DNNF under the hood, and d-DNNF does not support committing to new conflicts online,
      ;; something that is required by Pike's execution. To get over this though, Pike can commit to
      ;; x_aux_i=1 online, thus effectively forcing it (Knowledge compilation will take care of all
      ;; possible combinations of the conflicts therefore.
      (dolist (conflict all-conflicts)
        (let ((x_aux (make-instance 'decision-variable
                                    :name (format nil "x_aux_~a" (incf i))
                                    :domain `("0" "1"))))
          (push x_aux conflict-variables)
          ;; Create the x_aux_i=1 => conflict_i constraint

          ;; TODO FIXME -- Add following back, can be an implication constraint? Equivalence works OK too though
          (push (make-instance 'equivalence-constraint
                               :lhs (make-instance 'assignment-constraint
                                                   :assignment (assignment x_aux "1"))
                               :rhs (environment->conflict conflict ss))
                constraints-to-encode)


          ;; Keep track of the mapping from this conflict environment to the auxiliary variable
          (setf (gethash conflict conflict-environment->assignment) (assignment x_aux "1"))))

      ;; Now that we've gathered all the constraints to encode, encode as a WMC
      ;; with the main constraints we've been provided, and with the Bayesian network.
      (setf wmc (encode-bayesian-network-as-wmc bn
                                                :constraints constraints-to-encode
                                                :all-variables `(,x-correct ,@conflict-variables ,@(state-space-bits-variables ss))
                                                :name "pike-wmc-all"))


      ;; Now that this hard part is done, also construct a Bayesian network inference class, which is
      ;; essentially just a cacheing wrapper around a Bayesian network with no constraints unlike the
      ;; above.
      (setf bni (make-instance 'bayesian-network-inference
                               :bayesian-network bn
                               :ss ss))
      (initialize-for-inference bni :name "pike-wmc-bn")
      t)))


(defmethod compute-conditional-probability-of-correctness ((ckb constraint-knowledge-base-cc-wmc-ddnnf) &key (assignments-given) (conflicts nil))
  "Compute and return the probability of success, given the given assignments and conflicts"
  (with-slots (wmc bni ss x-correct conflict-environment->assignment) ckb
    (let ((conflict-assignments nil))

      ;; Collect the list of assignments to auxiliary variables specified by these conflicts
      (dolist (conflict-environment conflicts)
        (let (assignment found?)
          (multiple-value-setq (assignment found?) (gethash conflict-environment conflict-environment->assignment))
          (assert found? nil "Error! Cannot commit to conflict ~a, because it was not encoded a-priori in weighted model counting problem!" (print-environment conflict-environment ss nil))
          ;; Add the assignment to the list
          (push assignment conflict-assignments)))

      ;; Weighted model counting: solve there.

      ;; ORIGINAL VERSION BELOW, WHERE x-correct IS NOT NECESSARILY COMMITED TO BEING TRUE>
      ;; (compute-conditional-probability wmc
      ;;                                  :assignments `(,(assignment x-correct "1"))
      ;;                                  :given `(,@assignments-given
      ;;                                           ,@conflict-assignments))

      ;; NEW VERSION BELOW
      (let* (p-assignments-and-given
             p-given
             (assignments-uncontrollable (loop for a in assignments-given
                                            when (not (controllable? (assignment-variable a)))
                                            collect a))
             (env-assignments-uncontrollable (make-environment-from-assignments ss assignments-uncontrollable)))
        ;; Numerator: use the WMC with x-correct set to true
        (setf p-assignments-and-given (compute-marginal-probability wmc
                                                                    :assignments `(,@assignments-given
                                                                                   ,@conflict-assignments)))

        ;; Denominator: Use the Bayes Net Inference class, which is a wrapper around a second WMC
        ;; that only encodes the Bayesian network.
        ;; TODO this can be made more efficient I suppose -- an extra projection pass will be
        ;; performed here but isn't really necessary.
        (setf p-given (compute-marginal-probability bni
                                                    :env env-assignments-uncontrollable))
        ;; Return the quotient!
        ;; (format t "Numerator: ~a, Denominator: ~a~%" p-assignments-and-given p-given)
        ;; Edge case for 0 / 0: return 0 if numerator is 0.
        (if (= 0 p-assignments-and-given)
            0
            (/ p-assignments-and-given p-given))))))


(defmethod compute-conditional-marginal-probability-of-uncontrollable-choice ((ckb constraint-knowledge-base-cc-wmc-ddnnf) assignment)
  "Computes the marginal probability, conditioned on the given observed uncontrollable variables, of the given
assignment (which is assumed to be an assignment to an uncontrollable variable). Note that this just usees the Bayesian
network, and none of the correctness constraints."
  (with-slots (ss bni) ckb
    (let ((p-numerator (compute-marginal-probability bni :env (make-environment-from-assignments ss (list assignment))))
          (p-denominator (compute-marginal-probability bni :env (make-environment-from-assignments ss nil))))
      (/ p-numerator p-denominator))))


(defmethod print-solutions-to-stream ((ckb constraint-knowledge-base-cc-wmc-ddnnf) stream)
  t)

(defmethod get-solution-size ((ckb constraint-knowledge-base-cc-wmc-ddnnf))
  (with-slots (wmc) ckb
    (size-of-d-dnnf (weighted-model-counter-d-dnnf wmc))))


(defgeneric register-possible-online-conflicts (ckb conflicts))
(defmethod register-possible-online-conflicts ((ckb constraint-knowledge-base-cc-wmc-ddnnf) conflicts)
  "Register possible conflicts that could be committed to during online execution."
  ;; Just store them for now
  (with-slots (all-conflicts) ckb
    (dolist (c conflicts)
      (push c all-conflicts))))
