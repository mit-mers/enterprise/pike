;;;; Copyright (c) 2017 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)



(defmethod load-problem-pre-smt ((ps pike-session))
   "Load in the given plant model, and write it to an SMTLIB2 file"

  ;; First, load in the plan and plant model, so we have an ldg
  ;; Load the TPN plan file
  (load-plan ps)

  ;; Load an appropriate plant model for this plan
  ;; ex., PDDL, RMPL, NO-OP, etc.
  (load-plant-model ps)

  ;; Check validity of TPN
  (unless (plan-looks-sane? ps)
    (return-from load-problem-pre-smt nil))

  ;; Initializes internal data structures
  ;; from the now-loaded TPN and plant model
  (initialize-from-plan-and-plant-model ps)

  ;; Possibly add strong controllability constraints
  ;; for uncontrollable durations in plan.
  (apply-strong-controllability ps))





(defmethod write-smt-file ((ps pike-session) filename)
  "Construct an SMT file"
  (with-open-file (f filename :direction :output :if-exists :supersede :if-does-not-exist :create)
    (write-smt-to-stream ps f)))


(defmethod generate-smt-causal-link-constraints ((ps pike-session))
  "Constructs a new state space, and returns it as well as a list of constraints"
  (with-slots (ss ldg) ps
    (let ((variables (copy-seq (state-space-bits-variables ss)))
          (constraints-at-least-one-producer nil)
          (producer-notes nil)
          (threat-notes nil)
          (predicates->producer-events (make-hash-table :test #'equal))
          (predicates->threat-events (make-hash-table :test #'equal)))

      ;; Populate the mapping from predicates to potential producers and threats
      (dolist (event (mtk-graph:get-vertices ldg))
        (dolist (predicate (effects event))
          (if (negative? predicate)
              (push event (gethash (about predicate) predicates->threat-events))
              (push event (gethash (about predicate) predicates->producer-events)))))

      ;; Construct causal link constraints!
      (dolist (e-c (mtk-graph:get-vertices ldg))
        (dolist (p (preconditions e-c))
          ;; Define the s_{p, e_c} variable
          (let* ((producer-events (remove e-c (gethash (about p) predicates->producer-events)))
                 (threat-events (remove e-c (gethash (about p) predicates->threat-events)))
                 (spec (make-instance 'decision-variable
                                      :name (format nil "s_{~a,~a}" p (id e-c)) ;; "s_{p, e_C}"
                                      :domain (append (loop for e-p in producer-events
                                                         collecting (string (id e-p))) (list "∘")))))

            (push spec variables)

            ;; Add a constraint that if e-c is activated, there must be at least one supporting causal link
            ;; Set up causal link choice constraint
            (push (simplify-constraint! (make-instance 'implication-constraint
                                                       :implicant (make-instance 'conjunction-constraint
                                                                                 :conjuncts (list (environment->constraint (guard e-c) ss)))
                                                       :consequent (make-instance 'disjunction-constraint
                                                                                  :disjuncts
                                                                                  (loop for e-p in producer-events collecting
                                                                                       (make-instance 'assignment-constraint
                                                                                                      :assignment (assignment spec (string (id e-p))))))))
                  constraints-at-least-one-producer)


            ;; Now, for each of these different producer / threat combinations, we need to producer ordering constraints and
            ;; also add threat resolution constraints.
            ;; We can't actually do that here though, because we dont' have access to the temporal variables! So, just
            ;; make some notes here and pass them on!
            (dolist (e-p producer-events)
              (push (list (assignment spec (string (id e-p)))
                          e-c
                          e-p
                          (simplify-constraint! (make-instance 'conjunction-constraint
                                                               :conjuncts (list (environment->constraint (guard e-p) ss)))))
                    producer-notes)

              ;; For each producer, make some threat notes
              (dolist (e-t threat-events)
                (push (list (simplify-constraint! (make-instance 'conjunction-constraint
                                                                 :conjuncts (list (make-instance 'conjunction-constraint
                                                                                                 :conjuncts (list (environment->constraint (guard e-t) ss)))
                                                                                  (make-instance 'assignment-constraint :assignment (assignment spec (string (id e-p)))))))
                            e-c e-p e-t) threat-notes))))))

      (values (reverse variables) constraints-at-least-one-producer producer-notes threat-notes))))


(defmethod write-smt-to-stream ((ps pike-session) f &key (infinity 1e9))
  "Constructs an SMT representation."
  (with-slots (ldg ss) ps
    (let (variables constraints producer-notes threat-notes)
      ;; Retrieve causal link-related constraints
      (multiple-value-setq (variables constraints producer-notes threat-notes) (generate-smt-causal-link-constraints ps))

      ;; (format t "~a~%" constraints)
      ;; (format t "~a~%" producer-notes)
      ;; (format t "~a~%" threat-notes)

      (format f ";; This file automatically created by Pike from a TPN;; Please do not modify!~%")
      (format f ";; ~%~%")

      ;; Set some header information
      (format f ";; Information about this problem~%")
      ;; Our subtheory is quantifier free over linear reals
      (format f ";; Set subtheory structure for quantifier-free, linear constraints over reals~%")
      (format f "(set-logic QF_LRA)~%")


      ;; Variable declarations
      (format f "~%~%")
      (format f ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;~%")
      (format f ";; Variable declarations~%")
      (format f ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;~%~%")

      ;; Temporal variables
      (format f ";; Events / temporal variables (reals)~%")
      (dolist (event (mtk-graph:get-vertices ldg))
        (format f "(declare-const ~a Real)~%" (event-to-smt-variable-name event)))
      (format f "~%")

      ;; Discrete choice variables with assignments (propositional state logic)
      (format f ";; Discrete choice variables (encoded via assignments as propositional state logic)~%")
      (dolist (var variables)
        (dolist (val (decision-variable-values var))
          (format f "(declare-const ~a Bool)~%" (assignment-to-smt-variable-name (assignment var val)))))


      (format f "~%~%")
      (format f ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;~%")
      (format f ";; Constraints~%")
      (format f ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;~%~%")


      ;; Variable mutex constraints and disjunction constraints
      (format f ";; Discrete variables: \"choose one from each domain\" constraints~%")
      (dolist (var variables)
        ;; Add constraint that exactly one value must be chosen
        (format f "(assert (or ~{~a~^ ~}))~%" (loop for vi in (decision-variable-values var) collecting (assignment-to-smt-variable-name (assignment var vi))))
        ;; Add pairwise mutex constraints between each variable assignment
        (do-pairs (v1 v2 (decision-variable-values var))
          (format f "(assert (or (not ~a) (not ~a)))~%" (assignment-to-smt-variable-name (assignment var v1)) (assignment-to-smt-variable-name (assignment var v2)))))


      ;; Causal link-related constraints
      (format f "~%~%")
      (format f ";; Causal-link constraints: at least one causal link must be active for each consumer~%")
      (dolist (c constraints)
        (format f "(assert ~a)~%" (constraint-to-smt-constraint c)))

      (format f "~%~%")
      (format f ";; Causal link constraints: activated causals must activate producers, which precede consumers~%")
      (dolist (pn producer-notes)
        (let ((spec-assignment (first pn))
              (e-c (second pn))
              (e-p (third pn))
              (phi_e-p (fourth pn)))

          (format f "(assert (=> ~a~%" (assignment-to-smt-variable-name spec-assignment))
          (format f "            (and (< ~a ~a)~%" (event-to-smt-variable-name e-p) (event-to-smt-variable-name e-c))
          (format f "                 ~a)))~%" (constraint-to-smt-constraint phi_e-p))))


      (format f "~%~%")
      (format f ";; Causal link constraints: threats must come before or after activated causal links~%")
      (dolist (pn threat-notes)
        (let ((threat-constraint (first pn))
              (e-c (second pn))
              (e-p (third pn))
              (e-t (fourth pn)))
          (format f "(assert (=> ~a~%" (constraint-to-smt-constraint threat-constraint))
          (format f "            (or (< ~a ~a)~%" (event-to-smt-variable-name e-t) (event-to-smt-variable-name e-p))
          (format f "                (> ~a ~a))))~%" (event-to-smt-variable-name e-t) (event-to-smt-variable-name e-c))))


      ;; Temporal constraints
      (format f "~%;; Temporal constraints: labeled constraints corresponding to the TPN~%")
      ;; Assert initial event is at time 0.0
      (format f "(assert (= |t_ev-initial| 0.0))~%")
      ;; Insert labeled temporal constraints for each edge
      ;; Add in the edge weights!
      (dolist (edge (mtk-graph:get-edge-list (mtk-graph:get-edges ldg)))
        (let ((w (mtk-graph:get-weight edge ldg))
              (vi (first (mtk-graph:get-vertices edge)))
              (vj (second (mtk-graph:get-vertices edge))))


          (dolist (pair (lvs-pairs w))
            (format f "(assert (=> ~a~%" (event-guard-to-smt-constraint (lv-label pair) ss))
            (format f "            (<= (- ~a ~a) ~a)))~%" (event-to-smt-variable-name vj) (event-to-smt-variable-name vi) (num-to-smt-real (lv-value pair) :infinity infinity)))))





      ;; Is it satisfiable? What's the model?
      (format f "~%~%")
      (format f ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;~%")
      (format f ";; Check satisfiability and get the model~%")
      (format f ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;~%~%")
      (format f "(check-sat)~%")
      (format f "(get-model)~%"))))

(defun event-to-smt-variable-name (event)
  (format nil "|t_~a|" (id event)))

(defun assignment-to-smt-variable-name (assignment)
  (format nil "|~a|" assignment))

(defun num-to-smt-real (val &key (infinity 1e9))
  (cond ((= val +inf+) (format nil "~f" infinity))
        ((= val +-inf+) (format nil "~f" (- infinity)))
        (T (format nil "~f" val))))

(defun event-guard-to-smt-constraint (env ss)
  (let ((assignments  (get-assignments-from-environment ss env)))
    (if assignments
        (format nil "(and ~{~a~^ ~})" (mapcar #'assignment-to-smt-variable-name assignments))
        (format nil "true"))))

(defun constraint-to-smt-constraint (c)
  "Converts a constraint to a nice string representation."
  (cond
    ((typep c 'constant-constraint)
     (if (constant-constraint-constant c)
         "true"
         "false"))

    ((typep c 'assignment-constraint)
     (assignment-to-smt-variable-name (assignment-constraint-assignment c)))

    ((typep c 'disjunction-constraint)
     (format nil "(or ~{~a~^ ~})" (mapcar #'constraint-to-smt-constraint (disjunction-constraint-disjuncts c))))

    ((typep c 'conjunction-constraint)
     (format nil "(and ~{~a~^ ~})" (mapcar #'constraint-to-smt-constraint (conjunction-constraint-conjuncts c))))

    ((typep c 'implication-constraint)
     (format nil "(=> ~a ~a)" (constraint-to-smt-constraint (implication-constraint-implicant c)) (constraint-to-smt-constraint (implication-constraint-consequent c))))

    ((typep c 'equivalence-constraint)
     (format nil "(= ~a ~a)" (constraint-to-smt-constraint (equivalence-constraint-lhs c)) (constraint-to-smt-constraint (equivalence-constraint-rhs c))))

    ((typep c 'negation-constraint)
     (format nil "(not ~a)" (constraint-to-smt-constraint (negation-constraint-expression c))))

    ((typep c 'conflict-constraint)
     (format nil "(not (and ~{~a~^ ~}))" (mapcar #'constraint-to-smt-constraint (conflict-constraint-expressions c))))))
