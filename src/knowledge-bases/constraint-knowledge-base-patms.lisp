;;;; Copyright (c) 2015-2017 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Constraint-based knowledge base using a PATMS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass constraint-knowledge-base-patms (constraint-knowledge-base-atms constraint-knowledge-base-cc)
  ()
  (:documentation "A constraint knowledge base that uses a PATMS
as its underlying constraint solver / knowledge base."))


(defmethod initialize-instance :after ((ckb constraint-knowledge-base-patms) &key)
  "Initialize the ATMS from the SS"
  (with-slots (ss atms goal-node) ckb
    (setf atms (create-patms "PATMS" :ss ss))
    ;; Create and register a goal node
    (setf goal-node (tms-create-node atms "goal"))
    (set-patms-goal-node atms goal-node)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Public API calls
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Stuff
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod encode-justification ((ckb constraint-knowledge-base-patms) reason consequent antecedents)
  "For a regular ATMS, just call call justify. (Slightly different API for a PATMS due to defstructs)"
  (justify-node-patms reason
                      consequent
                      (remove-duplicates antecedents)))

(defmethod process-constraints ((ckb constraint-knowledge-base-patms))
  (with-slots ((patms atms) bn ss) ckb
    (let (bni)
     (format t "Using PATMS~%")

     ;; TODO - another hack - setup dummy variables
     ;;(set-dummy-probabilities patms)
     ;;(set-probabilities-from-description patms probability-description)
     (setf bni (make-instance 'bayesian-network-inference
                              :bayesian-network bn
                              :ss ss))
     (initialize-for-inference bni)
     (set-bayesian-network-inference  patms bni)

     ;; Set up a projection mask -- project onto all uncontrollable (i.e., human) variables
     (setf (patms-e-projection-mask patms) (make-controllable-projection-mask (patms-ss patms) :controllable? nil))

     ;; Encode lots of stuff!
     (call-next-method)



     ;; Update, for now until completion...
     ;; TODO - need a better termination condition here
     (loop while (not (= 0 (pileup:heap-count (patms-Q patms)))) do
          (update-step patms)))))


(defmethod compute-conditional-probability-of-correctness ((ckb constraint-knowledge-base-patms) &key (assignments-given) (conflicts nil))


  ;; TODO: Not correct yet; need to take into account conflicts and assignments
  ;; that are passed in!! Just here for testing.

  (with-slots ((patms atms)) ckb
    (with-slots (pwlc) patms
      (pwlc-weight pwlc))))
