;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Useful tools for analyzing Pike's results
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric print-execution-log (ps))
(defmethod print-execution-log ((ps pike-session))
  "Print out Pike's recorded execution trace"
  ;; TODO - doesn't work with choices (nil's)
  (with-slots (execution-context) ps
    (with-slots (event->executed-time) execution-context
     (let (trace)
       (loop for event being the hash-keys of event->executed-time
            using (hash-value time) do
            (push `(,time ,event) trace))

       (format t "Execution trace:~%")
       (dolist (item (sort trace #'< :key #'first))
         (format t (colorize :blue "~8$:") (first item))
         (format t " ~a~%" (second item)))))))


(defgeneric print-constraints (ps))
(defmethod print-constraints ((ps pike-session))
  (with-slots (causal-link-sets temporal-conflict-environments ss) ps
    (format t "Temporal conflicts:~%")
    (dolist (tc temporal-conflict-environments)
      (format t "  ~a~%" (environment->conflict tc ss)))

    (format t "~%Causal Link Constraints:~%")
    (dolist (cls causal-link-sets)
      (dolist (c (constraints cls))
        (format t "  ~a~%" c))
      (format t "~%"))))


(defmethod print-solutions ((ps pike-session))
  (print-solutions (pike-constraint-knowledge-base ps)))


(defgeneric print-conflicts (ps))
(defmethod print-conflicts ((ps pike-session))
  "Helper function to print out all conflicts."
  (print-label (atms-nogoods (constraint-knowledge-base-atms-atms (pike-constraint-knowledge-base ps))) (pike-state-space ps) T))


(defgeneric print-number-of-solutions (ps))
(defmethod print-number-of-solutions ((ps pike-session))
  (let* ((N (get-number-of-solutions ps)))
    (format t "~%Found ~a solution~p." N N)))


(defgeneric get-number-of-solutions (ps))
(defmethod get-number-of-solutions ((ps pike-session))
  (with-slots (ss) ps
    (let ((solutions (node-label (constraint-knowledge-base-atms-goal-node (pike-constraint-knowledge-base ps)))))
      (length solutions))))


(defgeneric print-nogoods (ps))
(defmethod print-nogoods ((ps pike-session))
  (with-slots (ss) ps
    (let ((nogoods (atms-nogoods (constraint-knowledge-base-atms-atms (pike-constraint-knowledge-base ps)))))
      (format t "Nogoods:~%")
      (dolist (env nogoods)
        (print-environment env ss t)
        (format t "~%"))
      (format t "~%Found ~a nogood~p." (length nogoods) (length nogoods)))))


(defgeneric print-variables-and-domains (ps))
(defmethod print-variables-and-domains ((ps pike-session))
  (with-slots (ss) ps
    (dolist (variable (state-space-bits-variables ss))
      (format t "Variable: ~a~%" (decision-variable-name variable))
      (dolist (val (decision-variable-values variable))
        (format t "    ~a~%" val))
      (format t "~%"))))


(defgeneric print-labels-for-each-constraint (ps))
(defmethod print-labels-for-each-constraint ((ps pike-session))
  (with-slots (ss constraint-knowledge-base) ps
    (dolist (constraint (constraint-knowledge-base-constraints constraint-knowledge-base))
      (format t "~a:~%" constraint)
      (with-slots (constraint->node) constraint-knowledge-base
        (print-label (node-label (genhash:hashref constraint constraint->node)) ss T))
      (format t "~%~%"))))


(defgeneric print-apsp-entry (ps e-from-name e-to-name))
(defmethod print-apsp-entry ((ps pike-session) e-from-name e-to-name)
  "Helper function for debugging temporal things"
  (with-slots (apsp) ps
    (apsp-shortest-path apsp
                        (find-event ps e-from-name)
                        (find-event ps e-to-name))))


(defgeneric find-event (ps event-name))
(defmethod find-event ((ps pike-session) (event-name string))
  "Helper function that, when given a string for an event, returns the event object"
  (let ((e (find event-name (mtk-graph:get-vertices (pike-ldg ps)) :test #'equal :key #'(lambda (e)
                                                                                          (string (id e))))))
    (unless e
      (error "Couldn't find event \"~a\"!" event-name))
    e))


(defgeneric find-variable (ps variable-name))
(defmethod find-variable ((ps pike-session) (variable-name string))
  "Helper function that, when given a string for a variable, returns the variable itself."
  (find-variable (pike-state-space ps) variable-name))

(defmethod find-variable ((ss state-space-bits) (variable-name string))
  (find-variable (state-space-bits-variables ss) variable-name))

(defmethod find-variable ((seq sequence) (variable-name string))
   (let ((v (find variable-name seq :test #'equal :key #'decision-variable-name)))
    (unless v
      (error "Couldn't find variable \"~a\"!" variable-name))
    v))


(defgeneric find-variable-assignment (ps variable-name value))
(defmethod find-variable-assignment ((ps pike-session) (variable-name string) (value string))
  "Helper function that, when given strings for a variable's name and value, returns the assignment object."
  (let ((var (find-variable ps variable-name)))
    (assignment var value)))


(defgeneric diagnose-why-no-solutions (ps))
(defmethod diagnose-why-no-solutions ((ps pike-session))
  "Attempt to diagnose why the plan has no solutions."
  (with-slots ((ckb constraint-knowledge-base)
               causal-link-sets) ps

    ;; First check if this is a trick question. Are there solutions?
    (when (not (null (node-label (constraint-knowledge-base-atms-goal-node ckb))))
      (format t "There are solutions!~%")
      (return-from diagnose-why-no-solutions T))

    ;; TODO: Is it temporally inconsistent?

    ;; Check causal links!
    (dolist (cls causal-link-sets)
      (dolist (c (constraints cls))
        (when (typep c 'implication-constraint)
          ;; Check if it can hold
          (unless (can-constraints-hold? ckb (list c))
            ;; Yes!! We found a reason!
            (with-slots (p e-c producer-events threat-events) cls

              (format t "The precondition ~a of ~a can't hold!~%" p (mention-event ps e-c))
              (format t "There are the following possible producers:~%")
              (dolist (e-p producer-events)
                (format t "    ~a~%" (mention-event ps e-p)))
              (format t "~%There are the following possible threats:~%")
              (dolist (e-t threat-events)
                (format t "    ~a~%" (mention-event ps e-t)))



              (return-from diagnose-why-no-solutions T))))))

    ;; Generic catch-all
    (format t "Some combination of causal links and temporal constraints cannot all hold.~%")
    nil))


(defgeneric explain-why-execution-failed (ps))
(defmethod explain-why-execution-failed ((ps pike-session))
  (with-slots (execution-context) ps
    (with-slots (reason-for-last-constraint) execution-context
      (if reason-for-last-constraint
          (format t (colorize :red (format nil "Hint: ~a~%" reason-for-last-constraint)))
          (format t (colorize :red "Unfortunately, no more information is available! Maybe running with -v will help?~%"))))))
