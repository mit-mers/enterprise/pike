;;;; Copyright (c) 2012 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)

(in-package #:cl-user)

(uiop:define-package #:pike
    (:use
     #:cl
     #:cl-user
     #:iterate)

  ;; Import selected symbols from TPN
  (:import-from :tpn
                ;; Useful macros
                :do-events
                :do-constraints
                :do-decision-variables)


  (:export
   ;; API

   ;; Main API (should probably be cleaned up a bit, some of these are exported to make testing easier)
   :pike-session
   :pike-options
   :create-pike-session
   :pike-compile
   :pike-execute
   :pike-execution-context
   :pike-constraint-knowledge-base
   :pike-plant-model
   :pike-register-hook
   :pike-time-fn
   :decision-variable
   :executed-time
   :execution-context
   :execution-context-status
   :pike-causal-link-sets
   :assignment->commit-time
   :event->executed-time
   :assignment
   :make-environment-from-assignments
   :environment-union
   :print-environment
   :find-event
   :find-variable-assignment
   :producer-events
   :parse-state-condition
   :read-file-parsing-parenthesis
   :make-state-space-bits
   :add-variable!
   :negative?
   :run-pike-with-sim
   :run-pike-with-sim-dt

   ;; Lower-level API (exposed mainly for technical work - like benchmarking)
   :compute-labeled-apsp
   :extract-causal-links
   :process-constraints
   :can-execution-succeed?
   :check-if-solutions-exist
   :execute-cycle
   :could-add-conflict-environments-and-commit-to-environment?
   :get-conflict-environments-needed-to-execute-event
   :execute-event
   :commit-to-environment
   :add-conflict-environment
   :prune-events-that-wont-be-executed
   :prune-execution-windows
   :propagate-execution-time
   :compute-conditional-probability-of-correctness
   :compute-conditional-marginal-probability-of-uncontrollable-choice
   :update-with-constraint!

   ;; Disturbances and simulation
   :disturbance
   :trigger
   :delete-predicates
   :commits
   :sim-get-time-fn
   :sim-get-execution-cycle-tick-fn
   :sim-get-dispatch-fn
   :sim-get-event-executed-fn
   :sim-cleanup
   :simulator-dt
   :make-disturbances
   :setup-disturbance-trigger
   :setup-disturbance-conditions

   ;; Constraints
   :constraint
   :assignment-constraint
   :constant-constraint
   :conjunction-constraint
   :disjunction-constraint
   :negation-constraint
   :implication-constraint
   :equivalence-constraint
   :conflict-constraint
   :assignment-constraint-assignment
   :constant-constraint-constant
   :conjunction-constraint-conjuncts
   :disjunction-constraint-disjuncts
   :negation-constraint-expression
   :implication-constraint-implicant
   :implication-constraint-consequent
   :equivalence-constraint-lhs
   :equivalence-constraint-rhs
   :conflict-constraint-expressions
   :simplify-constraint!
   :make-constraint
   :setup-variable-constraints
   :hash-constraint
   :equal-constraints
   :encode-constraint!
   :get-solution-size

   ;; Constraints -> SAT
   :encode-constraints-as-sat-cnf
   :write-dimacs-cnf-file
   :write-dimacs-cnf-to-stream
   :write-dimacs-cnf-variable-mapping-file
   :write-dimacs-cnf-variable-mapping-to-stream

   ;; Analysis
   :print-execution-log
   :print-constraints
   :print-solutions
   :print-conflicts
   :get-number-of-solutions
   :print-number-of-solutions
   :print-nogoods
   :print-variables-and-domains
   :print-labels-for-each-constraint
   :print-apsp-entry
   :find-event
   :find-variable
   :find-variable-assignment
   :diagnose-why-no-solutions))
