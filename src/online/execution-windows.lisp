;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Class for representing an execution window
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass execution-window ()
  ((lb
    :type lvs
    :initform (make-lvs)
    :accessor execution-window-lowerbound
    :documentation "The lower bounds. NOTE! These are actually
represented as negatives of the actual values! For example, 2 is
represented as -2, so that we don't have to define a new type
to make the inequalities work.")
   (ub
    :type lvs
    :initform (make-lvs)
    :accessor execution-window-upperbound
    :documentation "The upper bounds."))
  (:documentation "Represents a window
of possible time values (depending on the
choices made in the plan)"))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Macros for itereating over upper and lowerbounds of execution windows
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmacro do-execution-window-upperbounds (args &rest body)
  "(do-execution-window-upperbounds ((ub env) window)
      ...)"
  (let* ((variables (first args))
         (value-variable (first variables))
         (env-variable (second variables))
         (window (second args))
         (pair-symbol (gensym)))

    `(dolist (,pair-symbol (lvs-pairs (execution-window-upperbound ,window)))
       (let ((,value-variable (lv-value ,pair-symbol))
             (,env-variable (lv-label ,pair-symbol)))
         ,@body))))



(defmacro do-execution-window-lowerbounds (args &rest body)
  "(do-execution-window-upperbounds ((ub env) window)
      ...)"
  (let* ((variables (first args))
         (value-variable (first variables))
         (env-variable (second variables))
         (window (second args))
         (pair-symbol (gensym)))

    `(dolist (,pair-symbol (lvs-pairs (execution-window-lowerbound ,window)))
       (let ((,value-variable (- (lv-value ,pair-symbol)))
             (,env-variable (lv-label ,pair-symbol)))
         ,@body))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Methods for making and editing execution windows
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric update-execution-window-upperbound (window upperbound env ps))
(defmethod update-execution-window-upperbound ((window execution-window) upperbound environment (ps pike-session))
  (with-slots (ub) window
    (add-pair ub (create-lv (pike-state-space ps)
                            upperbound
                            environment))))


(defgeneric update-execution-window-lowerbound (window lowerbound env ps))
(defmethod update-execution-window-lowerbound ((window execution-window) lowerbound environment (ps pike-session))
  (with-slots (lb) window
    (add-pair lb (create-lv (pike-state-space ps)
                            (- lowerbound)
                            environment))))


(defgeneric make-execution-window (ps))
(defmethod make-execution-window ((ps pike-session))
  (with-slots (ss) ps
   (let ((window (make-instance 'execution-window)))
     (update-execution-window-lowerbound window +-inf+ (make-environment ss) ps)
     (update-execution-window-upperbound window +inf+ (make-environment ss) ps)
     window)))


(defgeneric prune-invalid-execution-window-entries (window ps))
(defmethod prune-invalid-execution-window-entries ((window execution-window) (ps pike-session))
  "For each entry in this execution window, remove labeled value pairs that have
   environments that could never be committed to."
  (with-slots ((ckb constraint-knowledge-base)) ps
    (with-slots (lb ub) window
      ;; Define a keep / pruning function
      (flet ((keep-fn (env)
               (could-ever-commit-to-environment? ckb env)))
        (prune-lvs lb #'keep-fn)
        (prune-lvs ub #'keep-fn)))))
