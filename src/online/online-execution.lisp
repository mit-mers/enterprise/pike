;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Online execution class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass execution-context ()
  ((time
    :type number
    :initform 0.0
    :accessor execution-context-time
    :documentation "The current time relative to execute-start-time (so it starts from 0)")
   (execution-start-time
    :type number
    :initform 0.0
    :documentation "The real time of when execution started.")
   (events-remaining-to-execute
    :type list
    :initform nil
    :documentation "Set of events remaining
to be executed.")
   (event->executed-time
    :type hash-table
    :initform (make-hash-table)
    :accessor execution-context-event->executed-time
    :documentation "Mapping from events
to the time they were executed.")
   (event->execution-window
    :type hash-table
    :initform (make-hash-table)
    :accessor execution-context-event->execution-window
    :documentation "Mapping from events
to their allowable execution windows")
   (event->executability
    :type hash-table
    :initform (make-hash-table)
    :accessor execution-context-event->executability
    :documentation "Maps events to a list
of symbols that define whether or not Pike can
choose when to execute this event, or if it is
externally scheduled. Each list may contain
the following symbols:
*  :depends-on-unmade-uncontrollable-choice
*  :activity-end-event
If the list is empty (i.e. nil), that means that
Pike may directly schedule the event now (if it
wants to / its other conditions are met).")
   (assignment->commit-time
    :type hash-table
    :initform (make-hash-table)
    :accessor execution-context-assignment->commit-time
    :documentation "Maps decision variable assignments
to the time at which they were committed to.")
   (committed-conflicts
    :type list
    :initform nil
    :accessor execution-context-committed-conflicts
    :documentation "A minimal list of conflicts that
have been committed to.")
   (event->uncontrollable-choices
    :type hash-table
    :initform (make-hash-table)
    :documentation "Maps events to the uncontrollable choices
on which they depend")
   (uncontrollable-choice->events
    :type hash-table
    :initform (make-hash-table)
    :documentation "Maps uncontrollable choices to the
events that depend on them")
   (choices-being-waited-on
    :type list
    :initform nil
    :documentation "A list of choices currently being waited on")
   (activity-end-mailbox
    :type safe-queue:mailbox
    :initform (safe-queue:make-mailbox)
    :accessor execution-context-activity-end-mailbox
    :documentation "A thread-safe queue for receiving notifications
regarding the completion of activities.")
   (choice-made-mailbox
    :type safe-queue:mailbox
    :initform (safe-queue:make-mailbox)
    :accessor execution-context-choice-made-mailbox
    :documentation "A thread-safe queue for receiving notifications
about choices that have been externally made (usually
uncontrollable ones)")
   (failed-activities
    :type list
    :initform nil
    :accessor execution-context-failed-activities
    :documentation "A list of activities that have failed
during execution.")
   (clex
    :type causal-link-execution-monitor
    :accessor execution-context-clex
    :documentation "A datastructure for keeping track of the
status of causal link monitoring.")
   (clex-fresh-state
    :type (or nil t)
    :initform t
    :accessor execution-context-clex-fresh-state
    :documentation "The flag indicating whether a fresh
state has recently arrived since the last causal link check,
and hence a new check is needed.")
   (state
    :type state
    :accessor execution-context-state
    :documentation "The most current world state.")
   (state-timestamp
    :type float
    :initform -1.0
    :accessor execution-context-state-timestamp
    :documentation "The timestamp (Unix time) of the current
state. Allows us to compare and see if we have an old state.")
   (state-lock
    :accessor execution-context-state-lock
    :initform (bordeaux-threads:make-lock)
    :documentation "A lock allowing thread-safe access to the
current state.")
   (epsilon
    :type float
    :initform 0.0
    :accessor execution-context-epsilon
    :documentation "A small forgiveness factor. If an event
needed to be executed at time t = 2, and the current time
t_now = 2.00001, maybe we should call that OK instead of
aborting due to a temporal inconsistency. This constant
allows the strictness of Pike to be set - a value of 0.0
means that NO temporal inconsistency will be tolerated.")
   (status
    :type symbol
    :initform :offline
    :accessor execution-context-status
    :documentation "Information about the current
progress of execution.")
   (execution-preempted
    :type boolean
    :initform nil
    :accessor execution-context-execution-preempted
    :documentation "A flag indicating whether or not this execution has
been externally cancelled / preempted.")
   (reason-for-last-constraint
    :initform nil
    :accessor execution-context-reason-for-last-constraint
    :documentation "A human-readable hint about why the last constraint
was added; valuable for debugging the final straw causing execution failure")
   (probabilistic-deadlock-due-to-observation?
    :initform nil
    :accessor execution-context-deadlock-due-to-observation?
    :documentation "A flag indicating if probabilistic deadlock has been
detected due to an observation.")
   (probabilistic-deadlock-due-to-upcoming-choice?
    :initform nil
    :accessor execution-context-deadlock-due-to-choice?
    :documentation "nil if there is no deadlock due to a required upcoming choice. Otherwise,
an instance of a decision variable if probabilistic deadlock is present."))
  (:documentation "Represents pertinent information about the current
status of online execution."))



;; TODO: Work on making this more event-based





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Main entry point for online execution
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric online-execute (ps))
(defmethod online-execute ((ps pike-session))
  ;; Actually execute the plan online with execution monitoring!


  ;; Initialize stuff. Time to 0, state to initial state, set up an
  ;; execution context, etc.
  (begin-execution ps)

  (with-slots (execution-context) ps
    (with-slots (time events-remaining-to-execute status) execution-context

      ;; Call the start hook
      (pike-call-hooks ps :execution-start ps)

      ;; Main while loop:
      (do ()
        ((execution-complete? ps) T)

        ;; Update current time
        (update-time ps)

        ;; TODO Perform execution monitor checks, if we've received
        ;; any new state updates
        (causal-link-check ps)

        ;; Process any activities that just completed, by
        ;; executing their end events.
        (process-activities-that-just-finished ps)

        ;; Process any decision variables that have been made
        ;; externally (i.e., generally uncontrollable choices)
        ;; and commit to them
        (process-externally-made-choices ps)

        ;; Process any missed / violated windows
        (process-missed-event-windows ps)

        ;; Find any uncontrollable choices that we're waiting on.
        (are-we-waiting-on-choices ps)

        ;; Check for probabilistic deadlock
        (check-for-deadlock ps)

        ;; After doing the above, check if the plan can still succeed.
        ;; something with (can-execution-succed? ps)
        (unless (can-execution-succeed? ps)
          (setf status :failed)
          (return))


        ;; The main part. Keep calling execute-cycle until it returns nil (it didn't just
        ;; execute any events). This will ensure that time doesn't get incremented
        ;; until we've executed everything there is to execute right now.
        (do ()
          ((not (execute-cycle ps)) T))

        ;; Call any hooks that are registered
        ;; each time an "execution loop" completes
        ;; (such hooks usually off, mainly used for
        ;; debugging and testing)
        (pike-call-hooks ps :execution-cycle-tick)

        ;; Politely yield the thread
        ;; (bordeaux-threads:thread-yield)
        ;; Sleep for 0.01 seconds to avoid pegging the CPU.
        ;;
        ;; TODO: Update 0.01 to something more appropriate to Pike's maximum
        ;; resolution?
        ;;
        ;; TODO: This is a stopgap measure to avoid CPU pegging. But it does
        ;; still use quite a bit of CPU. See the Issue at
        ;; https://git.mers.csail.mit.edu/mars-toolkit/pike/issues/3 for a
        ;; discussion of what this should be replaced with.
        (sleep 0.0))

      ;; Update the status to mark successful completion.
      (when (eql status :executing)
        (setf status :complete))

      ;; Call the execution end hook
      (pike-call-hooks ps :execution-end ps))))



(defgeneric initialize-for-execution (ps))
(defmethod initialize-for-execution ((ps pike-session))
  "Called to do some heavy lifting before execution starts."
  (with-slots (execution-context ldg ss plant-model pike-options) ps
    (setf execution-context (make-instance 'execution-context))
    (with-slots (time S events-remaining-to-execute
                      event->executed-time event->execution-window event->executability
                      epsilon state state-lock status) execution-context

      ;; We're now about to execute
      (setf status :executing)

      ;; Set up the executed sets
      ;; TODO - just setf, will stop reversing.
      (dolist (e (mtk-graph:get-vertices ldg))
        (push e events-remaining-to-execute))

      ;; Set up execution time windows [-infinity, infinity]
      ;; for all events
      (dolist (e (mtk-graph:get-vertices ldg))
        (setf (gethash e event->execution-window)
              (make-execution-window ps)))

      ;; Update the schedulability of all events
      (dolist (e (mtk-graph:get-vertices ldg))
        (setup-executability-flags ps e))


      ;; Set up causal-link execution monitoring
      (initialize-causal-link-monitor ps)

      ;; Set up an empty initial state.
      ;; The real state will be asserted when event e_initial
      ;; is executed.
      (bordeaux-threads:with-recursive-lock-held (state-lock)
        (setf state (get-empty-state ps plant-model)))



      ;; Assert any decision variable outcomes
      ;; that have been specified a-priori.
      ;; TODO - we could move these to compile time?
      (let ((variable-commits (pike-options-initial-choice-assignments pike-options)))
        (with-slots (ss) ps
          (dolist (vc variable-commits)
            (commit-to-choice-by-name ps (first vc) (second vc) :time 0.0 :reason "initially-made choices"))))

      ;; Set the forgiveness factor, epsilon
      (setf epsilon (pike-options-epsilon pike-options))

      ;; In case any events or environments can be pruned out immediately.
      (prune-events-that-wont-be-executed ps) ;; TODO - not needed since we execute initial event below
      (prune-execution-windows ps)
      ;; Optional slow step now, but improves performance later -- execution windows should be smaller.
      ;; NOTE - This will also change the APSP, since they both share the same "copy" of the lvs weights.
      ;; NOTE - This may potentially make debugging a bit tricker! Since the conflicts will be removed,
      ;; you won't be able to see them. Hence print a warning message
      (when-debug (ps)
                  (format t "Heads up: Pruning LDG and APSP conflicts from weights (conflicts will no longer show up directly in these)..."))
      (prune-ldg-weights ps)
      (when-debug (ps)
                  (format t "done~%"))

      ;; Set the initial execution time
      (setf time 0.0)

      ;; Execute the initial event
      (let (conflicts-needed-to-execute
            reasons
            (e-initial (event-initial (pike-ldg ps))))

        ;; Get all of the conflicts that would be required to execute this event now.
        (multiple-value-setq (conflicts-needed-to-execute reasons) (get-conflict-environments-needed-to-execute-event ps e-initial))

        ;; TODO - deal with conflicts for *unexecuted* things, like in main method below?

        ;; If we can, execute this event now!
        (execute-event ps e-initial time :conflicts conflicts-needed-to-execute :conflict-reasons reasons)))))


(defgeneric begin-execution (ps))
(defmethod begin-execution ((ps pike-session))
  "Called immediately before execution begins."
  (with-slots (execution-start-time state-timestamp state-lock) (pike-execution-context ps)
    ;; Set up execution start time to the current actual time,
    ;; and then set up an "execution time" relative to it
    (setf execution-start-time (get-time ps))
    (bordeaux-threads:with-recursive-lock-held (state-lock)
      (setf state-timestamp 0.0))))


(defgeneric setup-executability-flags (ps e))
(defmethod setup-executability-flags ((ps pike-session) (e event))
  "Helper function to set up the executabilty flags for this event"
  (with-slots (execution-context ss ldg) ps
    (with-slots (event->executability
                 event->uncontrollable-choices
                 uncontrollable-choice->events) execution-context
      (let ((executability-list nil))

        ;; If this is the end event of some activity, note it.
        (when (slot-boundp e 'activity-end)
          (push :activity-end-event executability-list))


        ;; If this event depends on uncontrollable choices, note it.
        ;; Also set up the hash tables that will be used to maintain
        ;; this property.
        (unless (pike-options-make-uncontrollable-choices? (pike-options ps))
          (let (found-uncontrollable-variable?)
            (dolist (variable (mapcar #'assignment-variable (get-assignments-from-environment ss (guard e))))
              (when (uncontrollable? variable)
                ;; Found one!
                (setf found-uncontrollable-variable? T)
                ;; Update the hash tables appropriately
                (push e (gethash variable uncontrollable-choice->events))
                (push variable (gethash e event->uncontrollable-choices))))
            ;; Also consider the event depending on an uncontrollable choice variable if
            ;; it's an at-event of an uncontrollable decision variable
            (let ((variable (gethash e (ldg-at-event->decision-variable (pike-ldg ps)))))
              (when (and variable
                         (not (controllable? variable)))
                (setf found-uncontrollable-variable? T)
                (push e (gethash variable uncontrollable-choice->events))
                (push variable (gethash e event->uncontrollable-choices))))

            (when found-uncontrollable-variable?
              (push :depends-on-unmade-uncontrollable-choice executability-list))))

        ;; Set the final list
        (setf (gethash e event->executability) executability-list)))))


(defgeneric execution-complete? (ps))
(defmethod execution-complete? ((ps pike-session))
  (with-slots (execution-context) ps
    (with-slots (events-remaining-to-execute execution-preempted) execution-context
      ;; TODO: Also check status flags
      (or (eq events-remaining-to-execute nil)
          execution-preempted))))


(defgeneric preempt-execution (ps))
(defmethod preempt-execution ((ps pike-session))
  "Mark the current execution as canceled / preempted!"
  (with-slots (execution-context) ps
    (with-slots (execution-preempted status) execution-context
      (setf execution-preempted T)
      (setf status :preempted))))


(defgeneric update-time (ps))
(defmethod update-time ((ps pike-session))
  "Updates Pike current working time to the execution-relative time.
  This working time stays frozen while Pike computes stuff for this cycle."
  (with-slots (execution-context) ps
    (with-slots (time execution-start-time) execution-context
      (setf time (raw-time->execution-time ps (get-time ps))))))


(defgeneric raw-time->execution-time (ps t_raw))
(defmethod raw-time->execution-time ((ps pike-session) t_raw)
  "Converts this raw time into seconds relative to the start of execution."
  (with-slots (execution-context) ps
    (with-slots (execution-start-time) execution-context
      (coerce (- t_raw execution-start-time) 'single-float)))) ;; NOTE - must match LVS type for numerical stability


(defgeneric execution-time->raw-time (ps t_exec))
(defmethod execution-time->raw-time ((ps pike-session) t_exec)
  "Converts execution time to raw time, using the execution start time (seconds)."
  (with-slots (execution-context) ps
    (with-slots (execution-start-time) execution-context
      (+ t_exec execution-start-time))))


(defgeneric get-time (ps))
(defmethod get-time ((ps pike-session))
  "Get the current raw time (not relative to execution start)"
  (funcall (pike-time-fn (pike-options ps))))


(defgeneric update-state (ps s &key &allow-other-keys))
(defmethod update-state ((ps pike-session) (s state) &key (time :now))
  "Helper method to update the current state with the given one (with the
   given timestamp)"
  (when (slot-boundp ps 'execution-context)
    (with-slots (execution-context) ps
      (with-slots (state state-timestamp state-lock clex-fresh-state) execution-context
        ;; Make sure the time is a current Unix time stamp.
        (when (eql time :now)
          (setf time (raw-time->execution-time ps (get-time ps))))

        ;; Update the state
        (bordeaux-threads:with-recursive-lock-held (state-lock)
          ;; Update the state only if it is newer than the current one.
          ;; This is super important! Pike updates the internal state sometimes
          ;; to simulate the effects of activities. We don't want to overwrite it with
          ;; an old rusty state.
          (when (>= time state-timestamp)
            (setf state s)
            ;; Update the timestamp
            (setf state-timestamp time)
            ;; Mark as a fresh state for causal link checking
            (setf clex-fresh-state t)))))))


(defgeneric execute-cycle (ps))
(defmethod execute-cycle ((ps pike-session))
  "Runs one execution cycle. Returns T if it executes an event, and
   nil otherwise if nothing was executed."
  (with-slots (execution-context (ckb constraint-knowledge-base)) ps
    (with-slots (time events-remaining-to-execute) execution-context
      ;; Loop over all events, trying to find ones that can be
      ;; executed. In other words, find the first event that can
      ;; be executed and execute it. We can execute it if we can
      ;; commit to any conflicts it would entail to execute it now,
      ;; as well of course to the event's environment.
      (dolist (e events-remaining-to-execute)
        (let (conflicts-needed-to-execute reasons can-accept?)

          ;; Get all of the conflicts that would be required to execute this event now.
          (multiple-value-setq (conflicts-needed-to-execute reasons) (get-conflict-environments-needed-to-execute-event ps e))

          ;; Could we accept these conflicts and also commit to the event's environment?
          (setf can-accept? (could-add-conflict-environments-and-commit-to-environment? ckb
                                                                                        conflicts-needed-to-execute
                                                                                        (guard e)))

          ;; TODO Do we need to deal with events that need to add conflicts if *not* executed,
          ;; for example, as part of a zero group etc.?

          ;; If we can, execute this event now!
          (when can-accept?
            (execute-event ps e time :conflicts conflicts-needed-to-execute :conflict-reasons reasons)
            (return-from execute-cycle T))))

      nil)))



(defgeneric get-conflict-environments-needed-to-execute-event (ps event &key &allow-other-keys))
(defmethod get-conflict-environments-needed-to-execute-event ((ps pike-session) event &key (ignore-executability nil))
  " Collect conflicts for:
  * If this event is externally scheduled, execute it under no circumstances.
    Therefore, return a list containing {}.

  * Unexecuted predecessors ( < 0, not <= 0).
    We do < 0 and not <= 0 because we may actually execute other events that are <=
    later on in this cycle. I think it keeps things simpler.

  * If the event has a <= to an externally scheduled (i.e., non-executable) event.
    This is because we know this non-executed event will *not* be scheduled
    now (during this execution cycle) or in the past.

  * If this event would be scheduled outside of some execution windows.

  * TODO: Is it necessary to add conflicts for predicted causal link violations?
    For example, if we know that executing an event will necessarily violate some
    causal link (i.e., it asserts \neg p), then perhaps we should add the
    corresponding monitor conditions conflict constraint. Not sure if it's necessary.
    May help in scenarios where two conflicting events could be executed in the
    execute cycle."
  (with-slots (execution-context ss ldg) ps
    (with-slots (time event->execution-window) execution-context
      (let ((conflicts nil)
            (reasons (make-hash-table))
            (window (gethash event event->execution-window)))

        ;; Under no circumstances should we execute this event
        (unless ignore-executability
          (unless (is-event-executable? ps event)
            ;; Return a list with, which implies necessarily failing.
            (return-from get-conflict-environments-needed-to-execute-event (list (make-environment ss)))))

        ;; Violated execution windows (lowerbounds)
        (do-execution-window-lowerbounds ((lb env) window)
          (when (< time lb)
            (setf (gethash env reasons) (format nil "~a would have had to happen after ~a (currently ~a) with it" (id event) lb time))
            (setf conflicts (add-minimal conflicts env ss))))

        ;; Violated execution windows (upperbounds)
        ;; TODO FIXME: Don't hardcode fudge factor below; use Pike's epsilon
        (do-execution-window-upperbounds ((ub env) window)
          (when (> (- time ub) 0.01)
            (setf (gethash env reasons) (format nil "violated exec window upper bound for event ~a" (id event)))
            (setf conflicts (add-minimal conflicts env ss))))

        ;; Violated, un-executed predecessors ( < 0)
        (dolist (edge (mtk-graph:find-edges-with-source-vertex event ldg))
          (let ((e (mtk-graph:get-target-vertex edge))
                (w-lvs (mtk-graph:get-weight edge ldg)))
            (when (not (event-has-been-executed? ps e))
              (dolist (pair (lvs-pairs w-lvs))
                (let ((w (lv-value pair))
                      (env (lv-label pair)))
                  (when (< w 0)
                    (setf (gethash env reasons) (format nil "violated un-executed predecessor [~a]" (id e)))
                    (setf conflicts (add-minimal conflicts env ss))))))))

        ;; Violating earlier-or-now, unexecuted yet externally scheduled events ( <= 0 )
        (dolist (edge (mtk-graph:find-edges-with-source-vertex event ldg))
          (let ((e (mtk-graph:get-target-vertex edge))
                (w-lvs (mtk-graph:get-weight edge ldg)))
            (when (and (not (is-event-executable? ps e))
                       (not (event-has-been-executed? ps e))
                       (not (eql event e)))
              (dolist (pair (lvs-pairs w-lvs))
                (let ((w (lv-value pair))
                      (env (lv-label pair)))
                  (when (<= w 0)
                    (setf (gethash env reasons) (format nil "~a must happen at-or-after externally-scheduled ~a, but is happening now" (id event) (id e)))
                    (setf conflicts (add-minimal conflicts env ss))))))))

        ;; Return a list of these conflicts
        (values conflicts reasons)))))


(defgeneric process-missed-event-windows (ps))
(defmethod process-missed-event-windows ((ps pike-session))
  (with-slots (execution-context ss ldg) ps
    (with-slots (time event->execution-window events-remaining-to-execute) execution-context
      (let (conflicts window events-missed)
        (dolist (event events-remaining-to-execute)
          (setf window (gethash event event->execution-window))

          ;; Violated execution windows (upperbounds). We
          ;; can't go back in time, so these must definitely
          ;; be added as conflicts.
          ;; TODO: Don't hardcode this constant!! Use Pike's epsilon value?
          (do-execution-window-upperbounds ((ub env) window)
            (when (> (- time ub) 0.01)
              ;; TODO Below commented out for debug legibility...
              ;; (when-debug (ps) (format-time t time (format nil
              ;;                                              "~a ~a ~a of event ~a~%"
              ;;                                              (colorize :red "Missed upperbound window")
              ;;                                              ub
              ;;                                              (print-environment env ss nil)
              ;;                                              event)))
              (push event events-missed)
              (setf conflicts (add-minimal conflicts env ss)))))

        ;; Add any conflicts we found
        (dolist (conflict conflicts)
          (add-conflict-environment ps conflict :reason (format nil "missed temporal window for event(s) ~{~a~^, ~}" (mapcar #'id (remove-duplicates events-missed)))))

        ;; Prune any other events that will never be executed.
        (prune-events-that-wont-be-executed ps)
        (prune-execution-windows ps)  ;; # TODO - BENCHMARK HACK REMOVE
        t))))


(defgeneric is-event-executable? (ps event))
(defmethod is-event-executable? ((ps pike-session) (event event))
  "Returns T if the event is executable, nil otherwise. Doesn't
   check conflicts / environments or anything - simply if Pike is even
   *allowed* to schedule this event. For example, if it's the end of an
   activity or depends on an uncontrollabl choice, we can't."
  (with-slots (event->executability) (pike-execution-context ps)
    (eq (gethash event event->executability) nil)))


(defgeneric event-has-been-executed? (ps event))
(defmethod event-has-been-executed? ((ps pike-session) (event event))
  (with-slots (event->executed-time) (pike-execution-context ps)
    (let (time found?)
      (multiple-value-setq (time found?) (gethash event event->executed-time))
      found?)))


(defgeneric has-committed-to-choice? (ps assignment))
(defmethod has-committed-to-choice? ((ps pike-session) (assignment decision-variable-assignment))
  (with-slots (assignment->commit-time) (pike-execution-context ps)
    (let (time found?)
      (multiple-value-setq (time found?) (gethash assignment assignment->commit-time))
      found?)))


(defgeneric get-committed-choices (ps))
(defmethod get-committed-choices ((ps pike-session))
  "Return all of the choices that that have been
   committed to so far during execution."
  (with-slots (assignment->commit-time) (pike-execution-context ps)
    (loop for a being the hash-keys
       of assignment->commit-time
       collecting a)))


(defgeneric execute-event (ps event t_reported &key &allow-other-keys))
(defmethod execute-event ((ps pike-session) event t_reported &key (conflicts nil) (conflict-reasons nil))
  "Schedule and execute this event now! Assume it was reported at t_reported (in executin time)"
  (with-slots (time events-remaining-to-execute event->executed-time) (pike-execution-context ps)
    ;; Mark the event as executed (with the actual time relative to start)
    (setf (gethash event event->executed-time) (raw-time->execution-time ps (get-time ps)))

    ;; Remove it from the list of events remaining to be executed
    (setf events-remaining-to-execute (delete event events-remaining-to-execute))

    ;; Commit to this event's environment
    (commit-to-environment ps (guard event) time :reason (format nil "committing to guard of executed event ~a" (id event)))

    ;; Commit to these conflicts required to execute the event now.
    (dolist (conflict conflicts)
      (add-conflict-environment ps conflict :reason (gethash conflict conflict-reasons)))

    ;; If this is the at-event for a controllable choice variable,
    ;; commit to one of it's values.
    (make-any-required-choices ps event)

    ;; Prune any other events that will never be executed.
    (prune-events-that-wont-be-executed ps)
    (prune-execution-windows ps) ;; # TODO - BENCHMARK HACK REMOVE

    ;; Propagate time to immediate neighbors and update their execution bounds
    (propagate-execution-time ps event time)

    ;; Trigger hooks (but ignore for initial and final events, since we added
    ;; those, not the callee)
    (unless (is-special-event? ps event)
      (pike-call-hooks ps :event-executed ps event time))
    (when (member (pike-options-constraint-knowledge-base-type (pike-options ps)) `(:cc-wmc-ddnnf :cc-wmc-bdd :cc-wmc-ab-bdd :patms))
      (pike-call-hooks ps :p-success-update (compute-conditional-probability-of-correctness (pike-constraint-knowledge-base ps)) time))

    ;; Dispatch or end any appropriate activities
    (when (slot-boundp event 'activity-start)
      (dispatch-activity ps (event-activity-start event)))

    ;; Update the current state.
    (update-state-from-event ps event t_reported)

    ;; Update the currently-active monitor conditions
    (causal-link-update-event-executed ps event)

    T))


(defgeneric update-state-from-event (ps event t_exec))
(defmethod update-state-from-event ((ps pike-session) event t_exec)
  "Given that this event just executed at t_exec (execution time), apply its effects to the
   current state. Note! This assumes the effects happened successfully.
   It there is a problem, we should detect it shortly via a state update, which
   we should be receiving frequently. This follows the \"innocent until proven
   guilty\" philosophy -- execution is believed to be successful until we sense
   that it's not."
  (with-slots (execution-context) ps
    (with-slots (state state-lock state-timestamp clex-fresh-state) execution-context
      (bordeaux-threads:with-recursive-lock-held (state-lock)
        ;; Apply the effects of the given event to the current state, but only if
        ;; we don't already have a newer state.
        (when (>= t_exec state-timestamp)
          (apply-effects! state (effects event))
          ;; Update the last state update timestamp to the reported time
          ;; (which could be a little bit in the past)
          (setf state-timestamp t_exec)
          ;; Mark as a fresh state for causal link checking
          (setf clex-fresh-state t))))))


(defgeneric is-special-event? (ps event))
(defmethod is-special-event? ((ps pike-session) (event event))
  (with-slots (ldg) ps
    (or (eql event (event-initial ldg))
        (eql event (event-final ldg)))))


(defgeneric prune-event (ps event))
(defmethod prune-event ((ps pike-session) event)
  "Mark this event as pruned - meaning that we know we will
   never execute it. For instance, it's guard will never be
   active due to choices we've made."
  (with-slots (events-remaining-to-execute event->executed-time time clex) (pike-execution-context ps)
    (when-debug (ps) (format-time t time "Pruning event ~a~%" (id event)))
    ;; Mark it's execution time as nil
    (setf (gethash event event->executed-time) nil)
    ;; Remove it from the remaining events
    (setf events-remaining-to-execute (delete event events-remaining-to-execute))
    ;; Remove any monitor conditions ending in this event.
    (prune-monitor-conditions-ending-at-event clex event)))


(defgeneric propagate-execution-time (ps event time))
(defmethod propagate-execution-time ((ps pike-session) (event event) time)
  "Propagates the execution time of this event to it's immediate neighbors.
   Updates the execution windows of those neighboring events. Upperbounds
   propagate along edges in one direction, and lowerbounds in the other."
  (with-slots (execution-context ss ldg) ps
    (with-slots (event->execution-window) execution-context

      ;; Upperbounds propagate along (positive) edges from the event to its neighbors.
      (dolist (edge (mtk-graph:find-edges-with-source-vertex event ldg))
        (let ((e (mtk-graph:get-target-vertex edge))
              (w-lvs (mtk-graph:get-weight edge ldg))
              (t-now-lvs (make-lvs))
              (t-bound-lvs nil))
          ;; Set up the current time LVS, {(t_now, ())}
          (add-pair t-now-lvs (create-lv-with-universal-env ss :value time))
          ;; Create an LVS containing new upperbounds for e, by
          ;; adding the t-now LVS with the weight along the edge.
          (setf t-bound-lvs (lvs-plus ss w-lvs t-now-lvs))
          ;; Now, merge this new bound in with the original one.
          (merge-lvs-with-completions ss
                                      (execution-window-upperbound (gethash e event->execution-window))
                                      t-bound-lvs)))

      ;; Lowerbounds propagate along (negative) edges from the event's neighbors back to the event.
      (dolist (edge (mtk-graph:find-edges-with-target-vertex event ldg))
        (let ((e (mtk-graph:get-source-vertex edge))
              (w-lvs (mtk-graph:get-weight edge ldg))
              (t-now-lvs (make-lvs))
              (t-bound-lvs nil))
          ;; Set up the current time LVS, {(t_now, ())}
          (add-pair t-now-lvs (create-lv-with-universal-env ss :value time))
          ;; Create an LVS containing new upperbounds for e, by
          ;; adding the t-now LVS with the weight along the edge.
          (setf t-bound-lvs (lvs-minus ss w-lvs t-now-lvs)) ;; Really t-lb, but since we represent lb's backwards
          ;; Now, merge this new bound in with the original one.
          (merge-lvs-with-completions ss
                                      (execution-window-lowerbound (gethash e event->execution-window))
                                      t-bound-lvs))))))



(defgeneric make-any-required-choices (ps event))
(defmethod make-any-required-choices ((ps pike-session) event)
  "Make any required choices that need to be made at this event.
   For instance, if this event is a controllable choic event,
   we need to make an event for that choice here."
  (with-slots (execution-context ldg ss) ps
    (with-slots (time) execution-context
      (let ((ckb-type (pike-options-constraint-knowledge-base-type (pike-options ps)))
            (made-choice? nil)
            (dv (gethash event (ldg-at-event->decision-variable ldg))))
        (when dv
          ;; This event is an at-event! Is it for a controllable variable?
          (when (controllable? dv)
            ;; Yes it is! We need to make the choice there.
            ;; We may pick any value of the variable that can be committed to
            ;; according to the kb. We prioritize based on the local utility
            ;; of this choice (note that this is NOT optimizing global utility).
            (let (assignments-sorted)

              ;; Sort the assignments appropriately.
              ;; If using consistency based Pike: sort by local assignment utility (still not globally optimal).
              ;; If using Riker, sort first by local assignment utility (hence greedy), then if there are ties,
              ;; by the choice with the highest posterior of plan execution success.
              (cond
                ;; Plan by posterior of plan success
                ((or (eql :cc-wmc-ddnnf ckb-type)
                     (eql :cc-wmc-bdd ckb-type)
                     (eql :cc-wmc-anytime-bdd ckb-type)
                     (eql :patms ckb-type))

                 (setf assignments-sorted (sort (loop for val in (decision-variable-values dv)
                                                   collecting (assignment dv val))

                                                ;; Sort by first field, then by second field
                                                #'(lambda (x y)
                                                    (cond
                                                      ((> (first x) (first y)) t)
                                                      ((= (first x) (first y)) (> (second x) (second y)))
                                                      (t nil)))

                                                :key #'(lambda (a)
                                                         (list
                                                          (assignment-utility a)
                                                          (compute-conditional-probability-of-correctness (pike-constraint-knowledge-base ps)
                                                                                                          :conflicts nil
                                                                                                          :assignments-given `(,a)))))))
                ;; Plan by local utility
                (t
                 (setf assignments-sorted (sort (loop for val in (decision-variable-values dv)
                                                   collecting (assignment dv val))
                                                #'>
                                                :key #'assignment-utility))))


              (dolist (a assignments-sorted)
                (when (could-add-conflict-environments-and-commit-to-environment? (pike-constraint-knowledge-base ps)
                                                                                  nil
                                                                                  (make-environment-from-assignments ss `(,a)))
                  ;; Great! This is the highest local utility assignment that
                  ;; we can commit to.
                  (commit-to-environment ps (make-environment-from-assignments ss `(,a)) time :reason (format nil "making required choice ~a at event" a))
                  (setf made-choice? t)
                  (return)))

              (unless made-choice?
                (let (question-best)
                  (cond
                    ((or (eql :cc-wmc-ddnnf ckb-type)
                         (eql :cc-wmc-bdd ckb-type)
                         (eql :cc-wmc-anytime-bdd ckb-type)
                         (eql :patms ckb-type))
                     (format-time t time (format nil
                                                 "~a probabilistic deadlock. No acceptable choice for variable ~a.~%"
                                                 (colorize :red "Warning:")
                                                 (decision-variable-name dv)))
                     (dolist (a assignments-sorted)
                       (format-time t time (format nil
                                                   "    P(success | ~a, history) = ~a~%"
                                                   a
                                                   (compute-conditional-probability-of-correctness (pike-constraint-knowledge-base ps)
                                                                                                   :conflicts nil
                                                                                                   :assignments-given `(,a)))))
                     (setf (execution-context-deadlock-due-to-choice? execution-context) dv)
                     ;; Select the best question to ask
                     (setf question-best (select-question-to-resolve-probabilistic-deadlock-due-to-upcoming-choice ps))
                     ;; Ask the question! Call the hook.
                     (pike-call-hooks ps :ask-question question-best time))

                    (t
                     (format-time t time (format nil
                                                 "~a deadlock. No acceptable choice for variable ~a.~%"
                                                 (colorize :red "Warning:")
                                                 (decision-variable-name dv))))))))))))))



(defgeneric commit-to-environment (ps environment time &key &allow-other-keys))
(defmethod commit-to-environment ((ps pike-session) environment time &key (reason nil))
  (with-slots (ss) ps
    (let ((conjunction (environment->conjunction environment ss)))
      (dolist (assignment (conjunction-constraint-conjuncts conjunction))
        ;; The type will be an assignment-constraint, out of which we
        ;; can get just the assignment and commit to it!
        (commit-to-choice ps assignment time :reason reason)))))


(defgeneric commit-to-choice (ps assignment-constraint time &key &allow-other-keys))
(defmethod commit-to-choice ((ps pike-session) (assignment-constraint assignment-constraint) time &key (reason nil))
  ;; If we have not already committed to this choice, do it.
  (with-slots (assignment) assignment-constraint
    (unless (has-committed-to-choice? ps assignment)
      (with-slots (assignment->commit-time reason-for-last-constraint) (pike-execution-context ps)
        ;; Mark as committed
        (setf (gethash assignment assignment->commit-time) time)

        ;; Update the constraint knowledge base
        (update-with-constraint! (pike-constraint-knowledge-base ps) assignment-constraint)
        (setf reason-for-last-constraint reason)

        ;; Update the executability flags if this was an uncontrollable choice
        (when (uncontrollable? (assignment-variable assignment))
          (update-executablity-from-uncontrollable-choice ps (assignment-variable assignment)))

        ;; Call hooks
        (pike-call-hooks ps :committed-to-choice assignment time)
        (when (member (pike-options-constraint-knowledge-base-type (pike-options ps)) `(:cc-wmc-ddnnf :cc-wmc-bdd :cc-wmc-ab-bdd :patms))
          (pike-call-hooks ps :p-success-update (compute-conditional-probability-of-correctness (pike-constraint-knowledge-base ps)) time))))))


(defgeneric commit-to-choice-by-name (ps variable-name value &key &allow-other-keys))
(defmethod commit-to-choice-by-name ((ps pike-session) (variable-name string) value &key (time nil) (reason nil))
  "Alows us to commit to a choice by the decision variable's name."
  (with-slots (ss) ps
    (let ((variable (find variable-name
                          (state-space-bits-variables ss)
                          :key #'decision-variable-name
                          :test #'equal)))
      (unless variable
        (error "Could not commit to ~a = ~a; decision variable \"~a\" couldn't be found!"
               variable-name
               value
               variable-name))
      (commit-to-choice ps
                        (make-instance 'assignment-constraint
                                       :assignment (assignment variable value))
                        (if time
                            time
                            (raw-time->execution-time ps (get-time ps)))
                        :reason reason))))


(defgeneric update-executablity-from-uncontrollable-choice (ps variable))
(defmethod update-executablity-from-uncontrollable-choice ((ps pike-session) (variable decision-variable))
  "Given that the given decision variable was just chosen, update the executability flag"
  (assert (uncontrollable? variable))
  (with-slots (execution-context) ps
    (with-slots (event->uncontrollable-choices uncontrollable-choice->events) execution-context
      (dolist (e (gethash variable uncontrollable-choice->events))
        ;; Remove this variable from the list of uncontrollable choices on which this
        ;; event depends
        (setf (gethash e event->uncontrollable-choices)
              (remove variable (gethash e event->uncontrollable-choices)))
        ;; If it's empty now, we can remove the entire flag!
        (when (= 0 (length (gethash e event->uncontrollable-choices)))
          (remove-event-executability-flag ps e :depends-on-unmade-uncontrollable-choice))))))


(defgeneric add-conflict-environment (ps env &key &allow-other-keys))
(defmethod add-conflict-environment ((ps pike-session) environment &key (reason nil))
  "Adds the given conflict environment, if we haven't already added it (or one that subsumes it)"
  (with-slots (ss (ckb constraint-knowledge-base) execution-context) ps
    (with-slots (committed-conflicts reason-for-last-constraint) execution-context
      ;; First, check if we've already added any conflict that subsumes it.
      ;; if we have, then simply return.
      (when (some #'(lambda (e-c)
                      (declare (type environment e-c environment))
                      (subsumes? e-c environment ss))
                  committed-conflicts)
        ;; Found one! We've already added a conflict that subsumes this one, so
        ;; adding this one would be pointless. Return.
        (return-from add-conflict-environment))

      (when-debug (ps)
                  (format-time t
                               (execution-context-time (pike-execution-context ps))
                               (if reason
                                   (format nil "Adding conflict ~a because ~a~%" (print-environment environment ss nil) reason)
                                   (format nil "Adding conflict ~a~%" (print-environment environment ss nil)))))

      (update-with-constraint! ckb (environment->conflict environment ss))
      (setf reason-for-last-constraint reason)
      ;; Also update the list of added conflicts minimally
      (setf committed-conflicts (add-minimal committed-conflicts environment ss)))))


(defgeneric prune-events-that-wont-be-executed (ps))
(defmethod prune-events-that-wont-be-executed ((ps pike-session))
  "Prunes events that will never be executed - i.e., whose guard
   conditions will never possibly be activated. "
  (with-slots (execution-context (ckb constraint-knowledge-base)) ps
    (with-slots (events-remaining-to-execute) execution-context
      (dolist (e events-remaining-to-execute)
        (unless (could-ever-commit-to-environment? ckb (guard e))
          (prune-event ps e))))))


(defgeneric prune-execution-windows (ps))
(defmethod prune-execution-windows ((ps pike-session))
  "Prunes entries out of execution windows that can never hold -
   i.e., those whose labels could never possibly be activated."
  (with-slots (event->execution-window) (pike-execution-context ps)
    (loop for window being the hash-values of event->execution-window do
         (prune-invalid-execution-window-entries window ps))))


(defgeneric prune-ldg-weights (ps))
(defmethod prune-ldg-weights ((ps pike-session))
  "Prunes entries out of the weights in the ldg that can never hold -- i.e.,
prunes the LVS labeled values out of the weight edges whose environments could
never possibly be activated."
  (with-slots ((ckb constraint-knowledge-base) ldg) ps
    ;; Define a keep / pruning function
    (flet ((keep-fn (env)
             (could-ever-commit-to-environment? ckb env)))
      (loop for edge being the hash-keys of (mtk-graph:get-weight-ht-edge ldg)
         using (hash-value weight) do
           (prune-lvs weight #'keep-fn)))))


(defgeneric are-we-waiting-on-choices (ps))
(defmethod are-we-waiting-on-choices ((ps pike-session))
  ;; Updates the set of decision variables we're waiting on
  (with-slots (execution-context ldg) ps
    (with-slots (choices-being-waited-on event->uncontrollable-choices time) execution-context
      (let* ((choices-currently-waiting-on nil))
        ;; Compute the current set of choices that are being waited on.
        ;; Check all uncontrollable decision variables that occur at a yet-unexecuted event
        (maphash #'(lambda (e-at dv)
                     (when (and (uncontrollable? dv)
                                (not (decision-variable-assigned? ps dv)) ;; NOTE Remove if we want to re-observe choices possibly observed via questions
                                (not (event-has-been-executed? ps e-at)))
                       ;; Check and see if we could have committed to this candidate, ignoring the
                       ;; other executability conditions (which are probably just waiting on this event anyway)
                       (let (conflicts-needed-to-execute reasons)
                         (multiple-value-setq (conflicts-needed-to-execute reasons)
                           (get-conflict-environments-needed-to-execute-event ps e-at :ignore-executability t))
                         (when (could-add-conflict-environments-and-commit-to-environment? (pike-constraint-knowledge-base ps)
                                                                                           conflicts-needed-to-execute
                                                                                           (guard e-at))
                           ;; It appears that, if it weren't for the fact that we're waiting on this uncontrollable choice,
                           ;; we could just execute this event now! So add the choice to the set of events we're waiting on
                           (push dv choices-currently-waiting-on)))))
                 (ldg-at-event->decision-variable ldg))

        ;; Now, compare against the current set. If it's different, than issue
        ;; an update!
        (when (set-exclusive-or choices-being-waited-on choices-currently-waiting-on)
          (setf choices-being-waited-on choices-currently-waiting-on)
          (pike-call-hooks ps :updated-choices-being-waited-on choices-being-waited-on time))))))

(defgeneric check-for-deadlock (ps))
(defmethod check-for-deadlock ((ps pike-session))
  "Check for probabilistic deadlock and take appropriate measures."

  (with-slots (probabilistic-deadlock-due-to-observation? time) (pike-execution-context ps)
    ;; If deadlock has already been detected, skip.
    (when probabilistic-deadlock-due-to-observation?
      (return-from check-for-deadlock))


    (let ((ckb-type (pike-options-constraint-knowledge-base-type (pike-options ps)))
          (ckb (pike-constraint-knowledge-base ps))
          p-success)
      ;; Only possible with Riker / risk-bounded Pike
      (when (or (eql :cc-wmc-ddnnf ckb-type)
                (eql :cc-wmc-bdd ckb-type)
                (eql :cc-wmc-anytime-bdd ckb-type)
                (eql :patms ckb-type))

        ;; Check if we've entered into probabilistic deadlock due to a recent observation.
        ;; This will manifest itself by the current probability of success being less than chance constraint.
        (setf p-success (compute-conditional-probability-of-correctness ckb
                                                                        :conflicts nil
                                                                        :assignments-given nil))
        (when (< p-success (chance-constraint ckb))
          ;; Deadlock detected!
          (let (question-best)
            (setf probabilistic-deadlock-due-to-observation? t)

            (format-time t time (format nil
                                        "~a probabilistic deadlock detected due to a recent uncontrollable observation.~%"
                                        (colorize :red "Warning:")))

            (setf question-best (select-question-to-resolve-probabilistic-deadlock-due-to-observation ps))
            ;; Ask the question! Call the hook.
            (pike-call-hooks ps :ask-question question-best time)))



        ;; TODO: Detect probabilistic deadlock due to an upcoming controllable choice.
        ;; Note: this is actually implemented in another function, make-any-required-choices.


        t))))


(defgeneric clear-probabilistic-deadlock-flags (ps))
(defmethod clear-probabilistic-deadlock-flags ((ps pike-session))
  (with-slots (probabilistic-deadlock-due-to-observation? probabilistic-deadlock-due-to-upcoming-choice?) (pike-execution-context ps)
    (setf probabilistic-deadlock-due-to-observation? nil)
    (setf probabilistic-deadlock-due-to-upcoming-choice? nil)))


(defgeneric select-question-to-resolve-probabilistic-deadlock-due-to-observation (ps))
(defmethod select-question-to-resolve-probabilistic-deadlock-due-to-observation ((ps pike-session))
  "Select the best question to ask. A question here is an uncommitted, uncontrollable decision variable.
Choose the question to ask that -- if the answer / assignment were to be provided by a human oracle, would
be the most likely to resolve the probabilistic deadlock."
  (with-slots (bayesian-network execution-context (ckb constraint-knowledge-base)) ps
    (with-slots (time) execution-context
      ;; Collect the set of unassigned, uncontrollable decision variables
      (let* ((candidate-variables (loop for dv in (bayesian-network-variables bayesian-network)
                                     when (not (decision-variable-assigned? ps dv))
                                     collect dv))
             (cc (chance-constraint ckb))
             (expected-values nil)
             question-best)
        (when-debug (ps)
                    (format-time t time "Candidate questions: ~a~%" candidate-variables))


        ;; For each question, compute the probability that asking it will resolve deadlock given current belief state
        (dolist (var-question candidate-variables)
          ;; Compute the expected value of the indicator function of whether this choice will resolve the deadlock.
          ;; This expected value is hence the probability that asking about the given variable will resolve deadlock.
          (let ((expected-value 0))
            (dolist (val (remove "∘" (decision-variable-values var-question) :test #'equal))
              ;; Would this resolve the probabilistic deadlock?
              (let* ((p-success  (compute-conditional-probability-of-correctness ckb
                                                                                 :conflicts nil
                                                                                 :assignments-given (list (assignment var-question val))))
                     (indicator-resolved (if (or (>= p-success cc)
                                                 (= p-success 0))
                                             1
                                             0))
                     (p-assignment (compute-conditional-marginal-probability-of-uncontrollable-choice ckb (assignment var-question val))))
                ;; Compute expected value, of indicator variable
                (incf expected-value (* indicator-resolved p-assignment))
                (when-debug (ps)
                            (format-time t time "~a=~a: ~a  (Prob: ~a)~%" (decision-variable-name var-question) val indicator-resolved p-assignment))))

            ;; Keep track of the expected values
            (push `(,expected-value ,var-question) expected-values)))

        (when-debug (ps)
                    (format-time t time "Expected values: ~a~%" expected-values))

        ;; Select the best option -- the question which maximizes the chances of resolving deadlock
        (setf question-best (second (argmin expected-values #'> #'first)))

        ;; (format t "Best question: ~a~%" question-best)
        question-best))))



(defgeneric select-question-to-resolve-probabilistic-deadlock-due-to-upcoming-choice (ps))
(defmethod select-question-to-resolve-probabilistic-deadlock-due-to-upcoming-choice ((ps pike-session))
  "Select the best question to ask. A question here is an uncommitted, uncontrollable decision variable.
Choose the question to ask that -- if the answer / assignment were to be provided by a human oracle, would
be the most likely to resolve the probabilistic deadlock due to the variable decision that must be made."
  (with-slots (bayesian-network execution-context (ckb constraint-knowledge-base)) ps
    (with-slots (time) execution-context
      ;; Collect the set of unassigned, uncontrollable decision variables
      (let* ((candidate-variables (loop for dv in (bayesian-network-variables bayesian-network)
                                     when (not (decision-variable-assigned? ps dv))
                                     collect dv))
             (cc (chance-constraint ckb))
             (expected-values nil)
             (var-deadlock (execution-context-deadlock-due-to-choice? execution-context))
             question-best)
        (when-debug (ps)
                    (format-time t time "Candidate questions: ~a~%" candidate-variables))


        ;; For each question, compute the probability that asking it will resolve deadlock given current belief state
        (dolist (var-question candidate-variables)
          ;; Compute the expected value of the indicator function of whether this choice will resolve the deadlock.
          ;; This expected value is hence the probability that asking about the given variable will resolve deadlock.
          (let ((expected-value 0))
            (dolist (val (remove "∘" (decision-variable-values var-question) :test #'equal))
              ;; Would this resolve the probabilistic deadlock? It will if at least one value of var-deadlock,
              ;; along with the response to this question, resolves the probabilistic deadlock.
              (let* ((indicator-resolved (if (some #'(lambda (val-deadlock)
                                                       (>= (compute-conditional-probability-of-correctness ckb
                                                                                                           :conflicts nil
                                                                                                           :assignments-given (list (assignment var-question val)
                                                                                                                                    (assignment var-deadlock val-deadlock)))
                                                           cc))
                                                   (remove "∘" (decision-variable-values var-deadlock) :test #'equal))
                                             1
                                             0))
                     (p-assignment (compute-conditional-marginal-probability-of-uncontrollable-choice ckb (assignment var-question val))))
                ;; Compute expected value, of indicator variable
                (incf expected-value (* indicator-resolved p-assignment))
                (when-debug (ps)
                            (format-time t time "~a=~a: ~a  (Prob: ~a)~%" (decision-variable-name var-question) val indicator-resolved p-assignment))))

            ;; Keep track of the expected values
            (push `(,expected-value ,var-question) expected-values)))

        (when-debug (ps)
                    (format-time t time "Expected values: ~a~%" expected-values))

        ;; Select the best option -- the question which maximizes the chances of resolving deadlock
        (setf question-best (second (argmin expected-values #'> #'first)))

        ;; (format t "Best question: ~a~%" question-best)
        question-best))))




(defgeneric decision-variable-assigned? (ps variable))
(defmethod decision-variable-assigned? ((ps pike-session) (variable decision-variable))
  "Returns T if the given decision variable has already been assigned -- and nil otherwise."
  (with-slots (assignment->commit-time) (pike-execution-context ps)
    (let ((variables-already-assigned (loop for key being the hash-keys of assignment->commit-time collect (assignment-variable key))))
      (member variable variables-already-assigned))))


(defgeneric dispatch-activity (ps activity))
(defmethod dispatch-activity ((ps pike-session) (activity activity))
  "Dispatches the given event."
  (with-slots (execution-context ss) ps
    (with-slots (time event->execution-window assignment->commit-time) execution-context

      ;; Computer a reasonable lower / upperbound to give the
      ;; activity dispatcher some flexbility.

      ;; Current method: Query the execution windows for the end event,
      ;; and choose the lb and ub based on the result.

      ;; TODO This method works a lot of the time, but is technically
      ;; flawed. Possible fixes include:
      ;;   1.) Choosing one valid continguous duration, and possibly
      ;;       committing to some choices as a result, or
      ;;
      ;;   2.) Extending the interface to the activity dispatcher to
      ;;       support disjunctive durations. Probably a bad idea --
      ;;       adds a lot of complexity everywhere.

      (let* ((env-commits (make-environment-from-assignments ss (get-committed-choices ps)))
             (window (gethash (activity-end-event activity) event->execution-window))
             ;; TODO - should we also check conflicts in below query? Use a separate function?
             (lb (round-to-resolution (- (- (query-lvs ss (execution-window-lowerbound window) env-commits)) time)))
             (ub (round-to-resolution (- (query-lvs ss (execution-window-upperbound window) env-commits) time))))

        ;; Sanity check: Make sure lb, ub are both >= 0, and that ub >= lb.
        ;; TODO - fix up!!
        (when (< lb 0.0)
          (setf lb 0.0))

        (when (< ub lb)
          (setf ub lb))


        ;; Call callbacks / hooks
        (pike-call-hooks ps :activity-dispatch activity lb ub time)))))


(defgeneric causal-link-check (ps))
(defmethod causal-link-check ((ps pike-session))
  "Performs a causal link check, if one is necessary!
   If we haven't performed a causal link check since the
   last state update, perform one now.

   Gather a list of constraints due to violated causal links, and
   commit to them."
  (with-slots (execution-context constraint-knowledge-base) ps
    (with-slots (state state-lock state-timestamp clex clex-fresh-state reason-for-last-constraint) execution-context
      (bordeaux-threads:with-recursive-lock-held (state-lock)
        ;; Only perform a causal link check if we haven't since the last state update.
        (when clex-fresh-state
          ;; Time to check!
          ;; (format t "Checking causal links~%")
          ;; (print state)
          (let (vio-constraints)
            (setf vio-constraints (check-causal-links ps state))
            ;; Commit to each violation constraint
            (dolist (vio-c vio-constraints)
              (format t "  --> Adding: ~a~%" vio-c)
              (update-with-constraint! constraint-knowledge-base vio-c)
              (setf reason-for-last-constraint "violated causal link"))
            ;; Update the timestamp of last causal link check to current time
            (setf clex-fresh-state nil)))))))




(defun round-to-resolution (x &key (resolution 1000))
  "Rounds the given value to the given resolution (i.e., 10's, 100's, 1000's, etc.)"
  ;; If infinity, SBCL can't round that.
  #+sbcl
  (if (or (eql sb-ext:single-float-positive-infinity x)
          (eql sb-ext:single-float-negative-infinity x))
      x
      (float (/ (round (* x resolution)) resolution))))


(defgeneric can-execution-succeed? (ps))
(defmethod can-execution-succeed? ((ps pike-session))
  "Returns T if execution could still proceed correctly, and
   nil otherwise."
  ;; Check the constraint knowledge base.
  (with-slots (execution-context) ps
    (and (solution-exists? (pike-constraint-knowledge-base ps))
         (eql (execution-context-failed-activities execution-context) nil))))


(defgeneric process-activities-that-just-finished (ps))
(defmethod process-activities-that-just-finished ((ps pike-session))
  ;; TODO - use the reported completion time, not the current
  ;; execution time, for the end events.
  ;;
  (with-slots (activity-end-mailbox failed-activities time) (pike-execution-context ps)
    (let (entry found?)
      ;; Get the first entry
      (multiple-value-setq (entry found?) (safe-queue:mailbox-receive-message-no-hang activity-end-mailbox))
      ;; This is basically a do-while loop.
      (do ()
          ((not found?))

        (let* ((activity (first entry))
               (end-event (activity-end-event activity))
               (time-completion (second entry))
               (success? (third entry))
               (conflicts nil)
               reasons)

          ;; (format t "activity end time: ~a~%" time-completion)

          ;; Did this activity fail? If so, add it to the list of failed activities
          (unless success?
            (push activity failed-activities)
            ;; Call the appropriate callback
            (pike-call-hooks ps :activity-failure activity time))

          ;; Remove the :activity-end-event flag since we can now safely
          ;; execute this event (prevents false conflicts)
          (remove-event-executability-flag ps end-event :activity-end-event)

          ;; Execute the end event and add any appropriate conflicts!
          (multiple-value-setq (conflicts reasons) (get-conflict-environments-needed-to-execute-event ps end-event))
          (execute-event ps end-event time-completion :conflicts conflicts :conflict-reasons reasons))


        ;; Try and get more entries
        (multiple-value-setq (entry found?) (safe-queue:mailbox-receive-message-no-hang activity-end-mailbox))))))


(defgeneric process-externally-made-choices (ps))
(defmethod process-externally-made-choices ((ps pike-session))
  "Commit to any choices that have been made externally"
  (with-slots (choice-made-mailbox time) (pike-execution-context ps)
    (let (entry found?)

      ;; Implement a do-while loop: Keep doing this until there are no more entries to read
      (multiple-value-setq (entry found?) (safe-queue:mailbox-receive-message-no-hang choice-made-mailbox))
      (do ()
          ((not found?))
        (let ((variable-name (first entry))
              (variable-value (second entry)))
          (commit-to-choice-by-name ps variable-name variable-value :reason (format nil "observed choice ~a=~a" variable-name variable-value))
          (clear-probabilistic-deadlock-flags ps))
        (multiple-value-setq (entry found?) (safe-queue:mailbox-receive-message-no-hang choice-made-mailbox))))))


(defgeneric pike-queue-up-finished-activity (ps activity time success?))
(defmethod pike-queue-up-finished-activity ((ps pike-session) (activity activity) time success?)
  (safe-queue:mailbox-send-message (execution-context-activity-end-mailbox (pike-execution-context ps))
                                    (list activity
                                          time
                                          success?)))


(defgeneric pike-queue-up-choice-made (ps variable-name value))
(defmethod pike-queue-up-choice-made ((ps pike-session) (variable-name string) value)
  (safe-queue:mailbox-send-message (execution-context-choice-made-mailbox (pike-execution-context ps))
                                    (list variable-name
                                          value)))


(defgeneric remove-event-executability-flag (ps event flag))
(defmethod remove-event-executability-flag ((ps pike-session) (event event) flag)
  "Removes the given flag from the given event's executability list (if it exists)."
  (with-slots (execution-context) ps
    (with-slots (event->executability) execution-context
      (setf (gethash event event->executability)
            (delete flag (gethash event event->executability))))))


(defgeneric time-until-next-execute-cycle (ps))
(defmethod time-until-next-execute-cycle ((ps pike-session))
  0.0)
