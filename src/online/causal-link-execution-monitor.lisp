;;;; Copyright (c) 2015 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Class definitions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass monitor-condition ()
  ((event-start
    :type event
    :initarg :event-start
    :initform (error "Start event  must be specified")
    :accessor event-start
    :documentation "The start event of this monitor condition.")
   (event-end
    :type event
    :initarg :event-end
    :initform (error "End event must be specified")
    :accessor event-end
    :documentation "The end event of this monitor condition.")
   (state-condition
    :type state-condition
    :initarg :state-condition
    :accessor state-condition
    :initform (error "The state condition to be monitored must be specified")
    :documentation "The state condition to be monitored between event-start and
event-end (if both are scheduled / executed)")
   (violation-constraint
    :type constraint
    :initarg :violation-constraint
    :initform (error "The violation constraint must be specified")
    :accessor violation-constraint
    :documentation "A constraint that must hold true, should this monitor
condition be violated online.")
   (description
    :type string
    :initarg :description
    :initform ""
    :accessor description
    :documentation "A textual, human-readable description of this monitor
condition."))
  (:documentation "Represents a single condition that should be monitored."))

(defmethod print-object ((mc monitor-condition) s)
  "Pretty print"
  (with-slots (event-start event-end state-condition) mc
    (format s "<MONITOR-CONDITION ~a -> ~a  ~a>" event-start event-end state-condition)))



(defclass causal-link-execution-monitor ()
  ((active-monitor-conditions
    :type hash-table
    :initform (make-hash-table)
    :accessor active-monitor-conditions
    :documentation "A hash set of currently-active monitor conditions.")
   (monitor-condition->status
    :type hash-table
    :initform (make-hash-table)
    :documentation "A hash map mapping monitor conditions to a status flag,
primarily useful for debugging purposes. Flags may include :active (for currently
active), :waiting (for never activated yet), :violated (for ones that have been
violated), :completed (for ones that finished normally), and :pruned for ones that
will never be activated.")
   (event-start->waiting-monitor-conditions
    :type hash-table
    :initform (make-hash-table)
    :documentation "Maps an event to a hash set of monitor conditions
waiting to be activated upon the given start event. Will not include
monitor conditions that have been pruned out, or ones that have ever been
activated.")
   (event-end->active-monitor-conditions
    :type hash-table
    :initform (make-hash-table)
    :documentation "Maps an event to a hash set containing currently
active monitor conditions whose end event is that event.")
   (event-end->all-monitor-conditions
    :type hash-table
    :initform (make-hash-table)
    :documentation "Maps an event to a hash set containing all
monitor conditions (active or inactive, etc.) whose end event
is that event."))

  (:documentation "Keeps track of relevant information and provides
methods for monitoring causal links online"))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Offline setup methods - API to be called
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric add-monitor-condition (clex mc))
(defmethod add-monitor-condition ((clex causal-link-execution-monitor) (mc monitor-condition))
  "Adds the given monitor condition to the list of possible conditions to be monitored."
  (with-slots (event-start->waiting-monitor-conditions
               event-end->all-monitor-conditions
               monitor-condition->status) clex
    (with-slots (event-start event-end) mc
      ;; Set the status to :waiting
      (setf (gethash mc monitor-condition->status) :waiting)
      ;; Add it to the hash set mc's of monitor conditions waiting to start at this event
      (let ((mcs (gethash event-start event-start->waiting-monitor-conditions)))
        (setf (gethash mc mcs) T))
      ;; Add it to the hash set of all mc's ending with this event
      (setf (gethash mc (gethash event-end event-end->all-monitor-conditions)) T))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Online monitoring related methods - API to be called
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric initialize-causal-link-monitor (ps))
(defmethod initialize-causal-link-monitor ((ps pike-session))
  "Given the Pike session ps, create a causal link execution monitor and initialize
  it with with the plan and given causal link sets."
  (with-slots (clex) (pike-execution-context ps)
    (setf clex (make-instance 'causal-link-execution-monitor))
    (setup-causal-link-monitor ps (pike-ldg ps) (pike-causal-link-sets ps))))


(defgeneric check-causal-links (ps state))
(defmethod check-causal-links ((ps pike-session) (state state))
  "Checks the given state against the list of predicates currently being monitored.
   Returns a list of violation constraints, or namely, a list of constraints that should
   hold true because of any violations that have been found."
  (with-slots (clex time) (pike-execution-context ps)
    (let (mc-vio violation-constraints)
      (with-slots (active-monitor-conditions) clex
        ;; For each active monitor condition, check and see if it holds in the given state.
        ;; If not, add it to a list to be returned.
        (loop for mc being the hash-keys of active-monitor-conditions do
             (unless (state-condition-in-state? (state-condition mc) state)
               (format-time t time "~a ~a~%" (colorize :red "Violated:") (description mc))
               (push mc mc-vio)))
        ;; Remove all of the violated mc's
        (dolist (mc mc-vio)
          (deactivate-monitor-condition clex mc :status :violated))
        ;; Collect and return corresponding violation constraints that must
        ;; now be made to hold true.
        (dolist (mc mc-vio)
          (push (violation-constraint mc) violation-constraints))
        violation-constraints))))


(defgeneric causal-link-update-event-executed (ps event))
(defmethod causal-link-update-event-executed ((ps pike-session) (event event))
  "Helper method that is called whenever an event is executed / dispatched / scheduled."
  (with-slots (clex) (pike-execution-context ps)
   (deactivate-monitor-conditions-ending-at-event clex event :status :completed)
   (activate-monitor-conditions-starting-at-event clex event)))


(defgeneric activate-monitor-conditions-starting-at-event (clex e))
(defmethod activate-monitor-conditions-starting-at-event ((clex causal-link-execution-monitor) (e event))
  (with-slots (event-start->waiting-monitor-conditions) clex
    ;; For each monitor condition waiting to be activated, activate it.
    (let ((mcs-waiting (gethash e event-start->waiting-monitor-conditions)))
      ;; There are mc's waiting to be activated!
      (loop for mc being the hash-keys of mcs-waiting do
           (activate-monitor-condition clex mc)))))


(defgeneric deactivate-monitor-conditions-ending-at-event (clex e &key &allow-other-keys))
(defmethod deactivate-monitor-conditions-ending-at-event ((clex causal-link-execution-monitor) (e event) &key (status :completed))
  (with-slots (event-end->active-monitor-conditions) clex
    ;; First agregate all mc that have end-event e into a list, and subsequently deactive them. (Need to
    ;; do this like this so as to not iterate and delete from a hash table at the same time)
    (dolist (mc (loop for mc being the hash-keys of (gethash e event-end->active-monitor-conditions) collect mc))
      (deactivate-monitor-condition clex mc :status status))))


(defgeneric prune-monitor-conditions-ending-at-event (clex e))
(defmethod prune-monitor-conditions-ending-at-event ((clex causal-link-execution-monitor) (e event))
  "Prunes out any active or waiting "
  (with-slots (event-end->all-monitor-conditions) clex
    (loop for mc being the hash-keys of (gethash e event-end->all-monitor-conditions) do
         (prune-monitor-condition clex mc))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Offline and online helper methods (not part of API, and not meant to be
;; called directly
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric setup-causal-link-monitor (ps ldg cls-sets))
(defmethod setup-causal-link-monitor ((ps pike-session) (ldg labeled-distance-graph) (cls-sets list))
  ;; Initialize mappings from event starts to event ends to empty has sets.
  (with-slots (clex) (pike-execution-context ps)
   (with-slots (event-start->waiting-monitor-conditions
                event-end->active-monitor-conditions
                event-end->all-monitor-conditions) clex
     (dolist (e (append (mtk-graph:get-vertices ldg) (list (event-initial ldg) (event-final ldg))))
       (setf (gethash e event-start->waiting-monitor-conditions) (make-hash-table))
       (setf (gethash e event-end->active-monitor-conditions) (make-hash-table))
       (setf (gethash e event-end->all-monitor-conditions) (make-hash-table))))

   ;; Convert causal link sets into monitor conditions
   (let (mcs)
     (dolist (cls cls-sets)
       (with-slots (e-c producer-events p causal-link-choice-variable) cls
         ;; Create a monitor condition for each producer event to each consumer event
         (dolist (e-p producer-events)
           (push (make-instance 'monitor-condition
                                :event-start e-p
                                :event-end e-c
                                :state-condition p
                                :violation-constraint (make-instance 'conflict-constraint
                                                                     :expressions (list (make-instance 'assignment-constraint
                                                                                                       :assignment (assignment causal-link-choice-variable (string (id e-p))))))
                                :description (format nil "causal link of ~a from ~a to ~a" p (mention-event ps e-p) (mention-event ps e-c)))
                 mcs))
         ;; Add all of the monitor conditions.
         (dolist (mc mcs)
           (add-monitor-condition clex mc)))))))


(defgeneric activate-monitor-condition (clex mc))
(defmethod activate-monitor-condition ((clex causal-link-execution-monitor) (mc monitor-condition))
  "Helper method to activate the given monitor condition"
  (with-slots (active-monitor-conditions
               monitor-condition->status
               event-start->waiting-monitor-conditions
               event-end->active-monitor-conditions) clex
    ;; Set the status to :active
    (setf (gethash mc monitor-condition->status) :active)
    ;; Add it to the active list
    (setf (gethash mc active-monitor-conditions) T)
    ;; Remove it from the list of monitor conditions waiting to be activated
    (remhash mc (gethash (event-start mc) event-start->waiting-monitor-conditions))
    ;; Add mc to the hash set of active monitor conditions ending at it's end event
    (setf (gethash mc (gethash (event-end mc) event-end->active-monitor-conditions)) T)))


(defgeneric deactivate-monitor-condition (clex mc &key &allow-other-keys))
(defmethod deactivate-monitor-condition ((clex causal-link-execution-monitor) (mc monitor-condition) &key (status :completed))
  "Helper method to deactive a currently-active monitor condition"
  (with-slots (active-monitor-conditions
               monitor-condition->status
               event-end->active-monitor-conditions) clex
    ;; Update the status
    (setf (gethash mc monitor-condition->status) status)
    ;; Remove it from active hash set
    (remhash mc active-monitor-conditions)
    ;; Also remove it from the mapping from end events.
    (remhash mc (gethash (event-end mc) event-end->active-monitor-conditions))))


(defgeneric prune-monitor-condition (clex mc))
(defmethod prune-monitor-condition ((clex causal-link-execution-monitor) (mc monitor-condition))
  "Helper method to prune the given monitor condition. If it's active, then just deactivate it.
   Otherwise, remove it from the monitor conditions waiting to be activated."
  (with-slots (monitor-condition->status
               event-start->waiting-monitor-conditions) clex
    (let ((status (gethash mc monitor-condition->status)))
      (cond
        ;; If it's active, just deactivate it.
        ((eql status :active)
         (deactivate-monitor-condition clex mc :status :pruned))

        ;; If it's inactive
        ((eql status :waiting)
         ;; Mark it as pruned, remove it from waiting mc's by start
         (setf (gethash mc monitor-condition->status) :pruned)
         (remhash mc (gethash (event-start mc) event-start->waiting-monitor-conditions)))

        ;; If already :pruned, :completed, or :violated, don't need to do anything.
        ((or (eql status :pruned)
             (eql status :completed)
             (eql status :violated)))))))


(defgeneric state-condition-in-state? (sc state))
(defmethod state-condition-in-state? ((sc state-condition) (state state))
  (not (not (member sc (state-conditions state) :test 'state-conditions-equal))))
