;;;; Copyright (c) 2014-2016 Massachusetts Institute of Technology
;;;;
;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:
;;;;
;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.
;;;;
;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.
;;;;
;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.
;;;;
;;;; Authors:
;;;;   Steve Levine

(in-package #:pike/cli)

(defclass parse-options ()
  ((program-name
    :type string
    :initform ""
    :initarg :program-name
    :accessor program-name
    :documentation "The name of the program")
   (banner
    :initform nil
    :initarg :banner
    :accessor banner
    :documentation "An optional banner to be printed
at the top during help.")
   (description
    :type string
    :initform ""
    :accessor description
    :initarg :description)
   (quit-on-help
    :type (or nil T)
    :initform T
    :accessor quit-on-help
    :initarg :quit-on-help)
   (arguments
    :type list
    :initform nil
    :accessor arguments
    :initarg :arguments))
  (:documentation "Options for parsing"))

(defclass argument ()
  ((key
    :type symbol
    :accessor key
    :initarg :key
    :documentation "The key returned in the hash map")
   (name
    :type string
    :accessor name
    :initform ""
    :initarg :name
    :documentation "A name")
   (flags
    :type list
    :accessor flags
    :initform nil
    :initarg :flags
    :documentation "A list of flags, like (list \"--plan\" \"-p\").
Positional argument (not flag-based) if nil.")
   (description
    :type string
    :accessor description
    :initform ""
    :initarg :description
    :documentation "Textual description of this field")
   (default-value
     :initarg :default-value
     :accessor default-value
     :initform nil
     :documentation "The default value of this argument")
   (has-parameter?
    :type (or nil T)
    :initform T
    :accessor has-parameter?
    :initarg :has-parameter?))
  (:documentation "Represents a command line argument"))



(defmethod parse-arguments ((po parse-options) (args list))
  "Parse the given arguments! Returns a hash table
mapping keys to their values."
  (with-slots (arguments quit-on-help) po
    ;; First see if help - if it is, show help and quit!
    (when (and quit-on-help
               (member (first args) (list "--help" "-h") :test #'equalp))
      (print-help-text po)
      (quit))

    (let ((mapping (make-hash-table))
          (state :new)
          (description nil)
          (a-list (copy-seq args))
          positional-descriptions)

      ;; Collect the list of positional arguments (map them in
      ;; the same order they are provided)
      (dolist (description arguments)
        (when (null (flags description))
          (push description positional-descriptions)))
      (setf positional-descriptions (nreverse positional-descriptions))

      ;; Now, keep consuming arguments (keeping track of parser state),
      ;; and mapping accordingly.

      (loop while a-list do
           (let ((term (pop a-list)))
             ;; Switch on state
             (cond ((eql state :new)
                    ;; It's a new tag (not a parameter of the last argument).
                    (cond ((is-flag? term)
                           ;; Keyword argument.
                           ;; Look it up.
                           (setf description (find-matching-description po term))
                           (unless description
                             (error "Unknown flag \"~a\"" term))
                           ;; If it takes a second parameter, note that in the state. Otherwise,
                           ;; continue on.
                           (if (has-parameter? description)
                               (setf state :param-of-last)
                               ;; It's a single flag argument so set it.
                               (setf (gethash (key description) mapping) T)))

                          (T
                           ;; Positional argument seems to have been supplied as a new argument.
                           ;; Apply positional arguments in order.
                           (when (not (should-ignore? term))
                             (when (null positional-descriptions)
                               (error "Invalid number of arguments!"))
                             (setf description (pop positional-descriptions))
                             (setf (gethash (key description) mapping) term)))))

                   ((eql state :param-of-last)
                    ;; If the last state requested a parameter, make sure we don't have another flag on our hands.
                    (when (is-flag? term)
                      (error "Must supply a parameter with argument \"~a\"" (name description)))
                    (setf (gethash (key description) mapping) term)
                    ;; Reset the parser state
                    (setf state :new)))))

      ;; Make sure we don't have any dangling!
      (when (eql state :param-of-last)
        (error "Must specify a parameter with argument \"~a\"" (name description)))

      ;; Now, fill in the default for any arguments not found!
      (dolist (d arguments)
        (let (entry found?)
          (multiple-value-setq (entry found?) (gethash (key d) mapping))
          (unless found?
            ;; This option wasn't specified! Use the default value.
            (setf (gethash (key d) mapping) (default-value d)))))

      ;; Return the mapping we've produced!
      mapping)))


(defmethod is-flag? ((word string))
  "Helper function to check if the given argument is a flag"
  (eql (char word 0) #\-))


(defmethod should-ignore? ((word string))
  "Helper function to check if the given argument begins with _, in which
  case we should ignore it (this is somewhat of a hack to roslaunch support
  working)"
  (eql (char word 0) #\_))


(defmethod print-help-text ((po parse-options))
  (let ((flag-arguments (get-flag-arguments po))
        (positional-arguments (get-positional-arguments po)))

    (when (banner po)
      ;; Print a banner if one was specified.
      (format t "~a~%" (banner po)))

    (format t
            "Usage:  ~a ~{[~a]~^ ~}  ~a~%"
            (program-name po)
            (mapcar #'name positional-arguments)
            (if (> (length flag-arguments) 0)
                "[Options...]"))
    (format t "  ~a~%~%" (description po))

    (dolist (a positional-arguments)
      (format t
              "  ~a~%    ~a~%~%"
              (name a)
              (description a)))

    (when (> (length flag-arguments) 0)
      (format t "Options:~%~%")
      (dolist (a flag-arguments)
        (format t
                "  ~{~a~^, ~} ~a~%    ~a~%~%"
                (flags a)
                (if (has-parameter? a)
                    (format nil "[~a]" (name a))
                    "")
                (description a))))
    (when (quit-on-help po)
      (format t "  --help, -h~%    Prints this help message and exits.~%~%"))))



(defmethod get-positional-arguments ((po parse-options))
  (loop for d in (arguments po)
       when (null (flags d)) collect d))


(defmethod get-flag-arguments ((po parse-options))
  (loop for d in (arguments po)
       when (flags d) collect d))


(defmethod find-matching-description ((po parse-options) (flag string))
  "Helper function to find a matching description"
  (with-slots (arguments) po
    (find-if #'(lambda (d)
                 (member flag (flags d) :test #'equalp))
             arguments)))


(defmethod parse-command-line-arguments ((po parse-options))
  (let ((command-line-args (rest (get-command-line-arguments))))
    (parse-arguments po command-line-args)))


;; TODO - move to a separate file
(defmethod test-arguments (args)
  (let ((opts (make-instance 'parse-options
                             :program-name "pike"
                             :description "Execute plans with monitoring."
                             :arguments (list
                                         (make-instance 'argument
                                                        :key :plan-filename
                                                        :flags nil
                                                        :name "plan"
                                                        :has-parameter? T
                                                        :description "The plan filename (ends in .tpn)"
                                                        :default-value nil)

                                         (make-instance 'argument
                                                        :key :domain-filename
                                                        :flags (list "--domain" "-d")
                                                        :name "domain"
                                                        :has-parameter? T
                                                        :description "The domain filename (ends in .xml)"
                                                        :default-value nil)

                                         (make-instance 'argument
                                                        :key :simulation-mode
                                                        :flags (list "--sim" "--simulation" "-s")
                                                        :name "simulation"
                                                        :has-parameter? nil
                                                        :description "Runs in simulation mode, without interacting in ROS"
                                                        :default-value nil)))))

    (parse-arguments opts args)))
