;;;; Copyright (c) 2014-2015 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)


(defclass simulator ()
  ((ps
    :type pike-session
    :initarg :pike-session
    :accessor simulator-pike-session
    :documentation "A pike session."))
  (:documentation "A simulator"))


(defgeneric sim-get-time-fn (sim))
(defgeneric sim-get-dispatch-fn (sim))
(defgeneric sim-get-event-executed-fn (sim))
(defgeneric sim-get-execution-cycle-tick-fn (sim))
(defgeneric sim-cleanup (sim))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Realtime simulation of online execution
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass simulator-realtime (simulator)
  ((threads
    :type list
    :initform nil
    :accessor simulator-realtime-threads
    :documentation "A list of all active threads.")
   (threads-list-lock
    :accessor simulator-realtime-threads-list-lock
    :initform (bordeaux-threads:make-recursive-lock)
    :documentation "A lock for accessing the threads field."))
  (:documentation "Represents a realtime simulation."))


(defmethod sim-get-time-fn ((sim simulator-realtime))
  #'default-get-time)

(defmethod sim-get-dispatch-fn ((sim simulator-realtime))
  "Return a function that spins up a new thread to dispatch & wait.
    This function will be called whenever an activity is dispatched."
  (with-slots (ps threads) sim
    (lambda (activity lb ub time)
      (declare (ignore ub time))
      ;; Create & start a new thread that will sleep and then notify
      ;; that the activity completed.
      (let ((thread (bordeaux-threads:make-thread
                     #'(lambda ()
                         (sleep lb)
                         (pike-notify-activity-finished ps activity (default-get-time) T)
                         ;; Remove this thread from the threads list
                         (sim-delete-thread sim (bordeaux-threads:current-thread))))))
        ;; Push this to the list of threads
        (sim-add-thread sim thread)))))


(defmethod sim-cleanup ((sim simulator-realtime))
  ;; Destroy all threads.
  (with-slots (threads threads-list-lock) sim
    (bordeaux-threads:with-recursive-lock-held (threads-list-lock)
      (dolist (thread threads)
        ;; Destroy this thread if it is still alive
        (when (bordeaux-threads:thread-alive-p thread)
          (bordeaux-threads:destroy-thread thread))))))


(defgeneric sim-add-thread (sim thread))
(defmethod sim-add-thread ((sim simulator-realtime) thread)
  (with-slots (threads threads-list-lock) sim
    (bordeaux-threads:with-recursive-lock-held (threads-list-lock)
      ;; Add this thread to the list
      (push thread threads))))


(defgeneric sim-delete-thread (sim thread))
(defmethod sim-delete-thread ((sim simulator-realtime) thread)
  (with-slots (threads threads-list-lock) sim
    (bordeaux-threads:with-recursive-lock-held (threads-list-lock)
      ;; Delete this thread from the list
      (setf threads (delete thread threads)))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Discrete time simulation (better for automated testing)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass simulator-dt (simulator)
  ((dt
    :type float
    :initform 0.01
    :initarg :dt
    :documentation "The time step to increment, per execution
cycle")
   (time
    :type float
    :initform 0.0
    :initarg :t0
    :documentation "The current simulation time")
   (actions-pending
    :type list
    :initform nil
    :documentation "A list of activities pending.")
   (disturbances
    :type list
    :initform nil
    :initarg :disturbances
    :documentation "A list of pending disturbances")
   (events-just-executed
    :type list
    :initform nil
    :documentation "A list of events executed since the last :execution-cycle-tick callback"))
  (:documentation "Simulates things offline with discrete time"))


(defclass disturbance ()
  ((trigger
    :initform `(:never-triggered)
    :initarg :trigger
    :accessor trigger
    :type list
    :documentation "The trigger condition for when this
disturbance applies")
   (delete-predicates
    :type list
    :initform nil
    :accessor delete-predicates
    :initarg :delete
    :documentation "A list of string specifying predicates
that will be deleted")
   (commits
    :type list
    :initform nil
    :accessor commits
    :initarg :commits
    :documentation "A list of commits to make")
   (sampling-operation
    :type list
    :initform nil
    :accessor sampling-operation
    :initarg :sampling-operation
    :documentation "A tuple specifiying a sampling operation, if desired."))
  (:documentation "Represents a simulated disturbance"))


(defmethod sim-get-time-fn ((sim simulator-dt))
  "Return the current simulation time"
  (with-slots (time) sim
    (lambda ()
      time)))


(defmethod sim-get-dispatch-fn ((sim simulator-dt))
  "Returns a function that, when called from Pike, queues
   up an action."
  (with-slots (actions-pending time) sim
    (lambda (activity lb ub time-dispatch)
      (declare (ignore time-dispatch))
      ;; Sanity check: lb <= ub
      (unless (<= lb ub)
        (error "Simulaton dispatch error: lb must be <= ub!"))
      (push (list activity (+ time lb) (+ time ub)) actions-pending))))


(defmethod sim-get-event-executed-fn ((sim simulator-dt))
  "Returns a function that, when called from Pike, queues up
   an executed event."
  (with-slots (events-just-executed) sim
    (lambda (ps event time)
      (declare (ignore ps time))
      (push event events-just-executed))))


(defmethod sim-get-execution-cycle-tick-fn ((sim simulator-dt))
  "Return a function of no arguments that does various book-keeping
   things with this simulation."
  (with-slots (ps time dt actions-pending events-just-executed disturbances) sim
    (lambda ()
      ;; Increment time
      (incf time dt)

      ;; Are any actions finishing? If so, tell Pike that they're finished.
      (let (actions-finishing)
        (dolist (a actions-pending)
          (when (>= time (second a))
            ;; Current time after lb!
            (unless (<= time (third a))
              ;; Uh oh, we missed the window! The current time is after ub.
              (format t "Warning: Simulation dt missed window!~%"))
            ;; Tell Pike it's done successfully
            (pike-notify-activity-finished ps (first a) time T)
            (push a actions-finishing)))

        ;; Remove from pending
        (dolist (a actions-finishing)
          (setf actions-pending (delete a actions-pending))))

      ;; Are any choices being made?

      ;; Are any disturbances happening?
      (let (disturbances-fired)
        ;; Find any disturbances that fire now
        (dolist (d disturbances)
          (cond
            ((eql :on-event (first (trigger d)))
             (when (member (second (trigger d)) events-just-executed)
               (push d disturbances-fired)))

            ((eql :at-time (first (trigger d)))
             (when (>= time (second (trigger d)))
               (push d disturbances-fired)))))

        ;; Apply their effects
        (dolist (d disturbances-fired)
          (format-time t time "Firing disturbance: ~a~%" d)
          (when (delete-predicates d)
            (apply-effects! (execution-context-state (pike-execution-context ps)) (delete-predicates d)))
          (when (commits d)
            (dolist (c (commits d))
              (commit-to-choice-by-name ps (decision-variable-name (assignment-variable c)) (assignment-value c))))
          (when (sampling-operation d)
            (cond
              ;; Sample probabilty and call user-specified function
              ((eql :sample-probability (first (sampling-operation d)))
               (funcall (second (sampling-operation d)) (compute-conditional-probability-of-correctness (pike-constraint-knowledge-base ps)) ))
              (t (error "Invalid sampling operation")))))

        ;; Remove any disturbances that just fired
        (dolist (d disturbances-fired)
          (setf disturbances (delete d disturbances))))

      ;; Reset the events just executed since the last callback
      (setf events-just-executed nil))))


(defmethod sim-cleanup ((sim simulator-dt))
  "Nothing to cleanup.")


(defgeneric run-pike-with-sim (ps))
(defmethod run-pike-with-sim ((ps pike-session))
  "Run an already-compiled Pike session in a realtime simulation with multi-threading."
  (let* ((sim (make-instance 'simulator-realtime :pike-session ps))
         (po (pike-options ps)))
    (unwind-protect
         (progn
           ;; Override the time function
           (setf (pike-time-fn po) (sim-get-time-fn sim))
           ;; Connect simulated activity dispatch
           (pike-register-hook ps :activity-dispatch (sim-get-dispatch-fn sim))
           ;; Go!
           (pike-execute ps))

      ;; Cleanup the simulator.
      (sim-cleanup sim))
    ps))


(defgeneric run-pike-with-sim-dt (ps &key &allow-other-keys))
(defmethod run-pike-with-sim-dt ((ps pike-session) &key
                                                     (dt 0.01)
                                                     (disturbances nil))
  "Run an already-compiled Pike session in a discrete time simulation. Probably
   better for quick
   and automated testing."
  (let* ((sim (make-instance 'simulator-dt
                             :pike-session ps
                             :dt dt
                             :disturbances (make-disturbances ps disturbances)))
         (po (pike-options ps)))
    (unwind-protect
         (progn
           ;; Override default time function
           (setf (pike-time-fn po) (sim-get-time-fn sim))
           ;; Connect simulated activity dispatch
           (pike-register-hook ps :activity-dispatch (sim-get-dispatch-fn sim))
           ;; Set up a hook to increment time, call Pike dispatch results, and
           ;; do other book keeping -- each time Pike finishes an execution cycle
           (pike-register-hook ps :execution-cycle-tick (sim-get-execution-cycle-tick-fn sim))
           ;; Another hook each time an event is executed
           (pike-register-hook ps :event-executed (sim-get-event-executed-fn sim))
           ;; Go!
           (pike-execute ps))

      ;; Cleanup the simulator.
      (sim-cleanup sim))
    ps))


(defgeneric make-disturbances (ps disturbance-templates))
(defmethod make-disturbances ((ps pike-session) disturbance-templates)
  "Helper method that, given a list of disturbance descriptions, creates and returns
   disturbance objects"
  (mapcar #'(lambda (d-template)
              (let ((d (make-instance 'disturbance)))
                (setup-disturbance-trigger ps d d-template)
                (setup-disturbance-conditions ps d d-template)
                d))
          disturbance-templates))


(defgeneric setup-disturbance-trigger (ps d d-template))
(defmethod setup-disturbance-trigger ((ps pike-session) (d disturbance) d-template)
  "Helper method to set up the trigger of a disturbance."
  (cond
    ((eql :on-event (first d-template))
     (setf (trigger d) `(:on-event ,(find-event ps (second d-template)))))

    ((eql :at-time (first d-template))
     (setf (trigger d) `(:at-time ,(second d-template))))

    (T (error "Invalid disturbance trigger syntax"))))


(defgeneric setup-disturbance-conditions (ps d d-template))
(defmethod setup-disturbance-conditions ((ps pike-session) (d disturbance) d-template)
  "Helper method to set up the trigger of a disturbance."
  (flet ((parse-negative-state-condition (scs)
           (let ((sc (parse-state-condition scs (pike-plant-model ps))))
             (setf (negative? sc) T)
             sc))
         (parse-assignment (a)
           (find-variable-assignment ps (first a) (second a))))
    (cond
      ((eql :delete (third d-template))
       (setf (delete-predicates d) (mapcar #'parse-negative-state-condition (fourth d-template))))

      ((eql :commit (third d-template))
       (setf (commits d) (mapcar #'parse-assignment (fourth d-template))))

      ((eql :sample-probability (third d-template))
       (setf (sampling-operation d) `(:sample-probability ,(fourth d-template))))

      (T (error "Invalid disturbance modifier syntax")))))
