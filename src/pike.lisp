;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; High level Pike (useful as part of main public API)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric create-pike-session (po))
(defmethod create-pike-session ((po pike-options))
  "Creates and returns an instance of a pike session."
  (let ((ps (make-instance 'pike-session :options po)))
    ;; Set up callbacks
    (pike-init-hooks ps)
    ps))




(defgeneric pike-compile (ps))
(defmethod pike-compile ((ps pike-session))
  "Causes Pike to completely compile the plan and prepare for
   iminent execution."
  (compile-plan ps))


(defgeneric pike-execute (ps))
(defmethod pike-execute ((ps pike-session))
  "Causes Pike to begin execution."
  (online-execute ps))


(defgeneric pike-notify-activity-finished (ps activity time success?))
(defmethod pike-notify-activity-finished ((ps pike-session) (activity activity) time success?)
  "Called to notify Pike that an activity has been finished."
  (pike-queue-up-finished-activity ps activity time success?))


(defgeneric pike-notify-choice-made (ps variable-name value))
(defmethod pike-notify-choice-made ((ps pike-session) (variable-name string) value)
  "Called to notify Pike that a choice has been made."
  (pike-queue-up-choice-made ps variable-name value))

(defgeneric pike-update-state (ps state &key &allow-other-keys))
(defmethod pike-update-state ((ps pike-session) (state state) &key (time :now))
  "Called to update Pike with the given state (at the given time, which may be either
   a unix-time style number, or :now)"
  (update-state ps state :time time))


(defgeneric pike-serialize (ps file))
(defmethod pike-serialize ((ps pike-session) filename)
  (with-open-file (f filename :Direction :output :if-exists :supersede :if-does-not-exist :create :element-type '(unsigned-byte 8))
    (cl-store:store ps f)))


(defgeneric pike-deserialize (file))
(defmethod pike-deserialize (filename)
  (cl-store:restore filename))




;; pike-register-hook is also public API but in another file.
