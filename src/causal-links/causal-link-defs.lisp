;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Causal link definitions!
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass causal-link ()
  ((consumer-event
    :type event
    :initarg :consumer-event
    :accessor consumer-event
    :documentation "The producer event for this
causal link.")
   (producer-event
    :type event
    :initarg :producer-event
    :accessor producer-event
    :documentation "The consumer event for this
causal link.")
   (predicate
    :type state-condition
    :initarg :predicate
    :accessor predicate
    :documentation "The state condition / predicate
for this causal link.")
   (label
    :type environment
    :initarg :label
    :accessor label
    :documentation "The label for this causal link."))
  (:documentation "Represents a causal link!"))


(defclass causal-link-set ()
  ((p
    :type state-condition
    :accessor predicate
    :initarg :p
    :documentation "The state condition")
   (e-c
    :type event
    :accessor consumer-event
    :initarg :e-c
    :documentation "The consumer event")
   (producer-events
    :type list
    :accessor producer-events
    :initarg :producer-events
    :initform nil
    :documentation "A set of possible
producers - (events that have p as a positive effect)")
   (threat-events
    :type list
    :accessor threat-events
    :initarg :threat-events
    :initform nil
    :documentation "A set of events that
all have p as a negative effect")
   (causal-links
    :type list
    :accessor causal-links
    :initarg :causal-links
    :initform nil
    :documentation "The list of causal links")
   (causal-link-choice-variable
    :type decision-variable
    :accessor causal-link-choice-variable
    :documentation "A decision variable whose domain
is the size of the  number or producer events")
   (threat-resolver-choice-variables-by-events
    :type hash-table
    :accessor threat-resolver-choice-variables-by-events
    :initform (make-hash-table :test #'equal)
    :documentation "A hash table that maps list tupes
of the form (producer-event threat-event) to decision
variables. Each of these decision variables has two
domain values, indicating if the threat must come
after the consumer or before the producer (if everything
is activated).")
   (constraints
    :type list
    :accessor constraints
    :initarg :constraints
    :initform nil
    :documentation "A list of constraints
corresponding to this set of causal links"))
  (:documentation "Represents a set of candidate
causal links for a particular precondition of a
consuming event. Contains the actual causal links,
as well as auxiliary information and constraints."))

(defmethod print-object ((cls causal-link-set) s)
  (with-slots (p e-c) cls
    (format s "<CL-SET for ~a of ~a>" p e-c)))
