;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Algorithms for causal link extraction!
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric extract-causal-links (ps))
(defmethod extract-causal-links ((ps pike-session))
  "Extracts causal links."

  ;; (Define precedence and succession operations (see notes) - use an flet?)

  ;; Iterate over each event in the plan
  ;; For each, get its preconditions.

  ;; Lookup all other events that affect this condition (using a hashtable?)

  ;; Create a generic LVS using succession relation above

  ;; Add all of these events to a new

  ;; Separate these events into two groups: one for and one for neg-p

  (with-slots (ldg apsp ss causal-link-sets) ps
    ;; First, print out some debug information if desired.
    (when-debug (ps)
                (format t "Using causal link domination: ~a~%" (if (pike-options-use-causal-link-dominance? (pike-options ps))
                                                                   "yes"
                                                                   "no")))

    ;; Create a hash table mapping what events affect back to the events.
    ;; (about predicate) returns an equal-comparable summary of the predicate
    ;; that ignores negation.
    (let ((producers-by-about (make-hash-table :test #'equal)))
      (dolist (e-p (mtk-graph:get-vertices ldg))
        (dolist (p (effects e-p))
          (push e-p (gethash (about p) producers-by-about))))


      (dolist (e-c (mtk-graph:get-vertices ldg))
        (dolist (p (preconditions e-c))
          ;; Extract causal links for the precondition p of this consumer event, e-c.
          ;; Create an LVS using the succession relation
          (let (lvs
                (cl-set (make-instance 'causal-link-set :p p :e-c e-c))
                reduced-events)

            ;; TODO: 8/12/2018 the below comment isn't quite right I think!
            ;; Do we want to consider causal link domination? Under normal circumstances, the
            ;; answer is probably yes. However, for the richest possible robustness to online
            ;; execution, we may choose not to consider domination. To implement this, we can
            ;; modify our LVS to not actually perform any domination (essentially making it
            ;; just a normal list)
            (cond ((pike-options-use-causal-link-dominance? (pike-options ps))
                   (setf lvs (make-instance 'lvs-generic
                                            ;; The below is the <_CL relation. Note that the normal succedes relation
                                            ;; is not good enough (see journal paper)
                                            :relation #'(lambda (e-i e-j)
                                                          (and (precedes? ps e-j e-i :env-context (guard e-c))
                                                               (precedes? ps e-i e-c :env-context (guard e-j)))))))
                  ;; Don't use causal link domination. By making the LVS relation false, domination will never occur.
                  (t
                   (setf lvs (make-instance 'lvs-generic :relation #'(lambda (e-i e-j) (declare (ignore e-i e-j)) nil)))))


            (with-slots (producer-events threat-events causal-links causal-link-choice-variable
                                         threat-resolver-choice-variables-by-events constraints) cl-set

              ;; Add each event that affects p to this LVS (so long as e-c does not precede it
              ;; the two events can actually occur together, and e-p and e-c aren't the same).
              ;; This effectively creates a list of the latest-occuring events (dominating
              ;; earlier events if possible) that affect p.
              (dolist (e-p (gethash (about p) producers-by-about))
                (when (and (not (eql e-p e-c))
                           (not (precedes? ps e-c e-p))
                           (environment-feasible? ps (environment-union (guard e-c) (guard e-p) ss)))
                  (add-pair-generic lvs
                                    (make-instance 'lv-generic
                                                   :ss ss
                                                   :value e-p
                                                   :label (guard e-p)))))

              ;; TODO Reduce the set using dominating sets. Note that this is more powerful than
              ;; the original formulation! In fact, this could be used instead of the LVS stuff above I believe.
              (if (pike-options-use-causal-link-dominance? (pike-options ps))
                  (setf reduced-events (reduce-via-dominance-sets ps (mapcar #'value (pairs lvs)) e-c))
                  (setf reduced-events (mapcar #'value (pairs lvs))))


              ;; Now, divide every event in the LVS into positive ones
              ;; (producer candidates) and negative ones (threats)
              (dolist (e reduced-events)
                (if (affects-negatively? e (about p))
                    (push e threat-events)
                    (push e producer-events)))

              ;; Next, figure out which threat resolver decision variable's we'll
              ;; need to add. (They'll be added all at once later)
              (dolist (e-p producer-events)
                (dolist (e-t threat-events)
                  ;; We need to add decision variables if all three events could happen
                  ;; at once, and if the threat is incomparable to both the producer
                  ;; and the consumer. Other cases may still require thread resolution,
                  ;; but we can get away without adding an additional decision variable
                  ;; so we don't.
                  (let ((env (environment-union-multiple ss
                                                         (guard e-c)
                                                         (guard e-p)
                                                         (guard e-t))))
                    (when (and (environment-feasible? ps env)
                               (incomparable? ps e-t e-p :env-context env)
                               (incomparable? ps e-t e-c :env-context env))
                      ;; We need to add a threat resolver decision variable
                      ;; For now, just mark T in the hash table. It will be filled in later
                      ;; with an actual decision variable object.
                      (setf (gethash `(,e-p ,e-t) threat-resolver-choice-variables-by-events) T)))))

              (push cl-set causal-link-sets)))))


      ;; Now, actually allocate all of the new variables.
      (dolist (cl-set (pike-causal-link-sets ps))
        (with-slots (p e-c producer-events threat-events causal-links causal-link-choice-variable
                       threat-resolver-choice-variables-by-events constraints) cl-set
          ;; Allocate the decision variable that chooses which of the
          ;; candidate causal links to enforce
          (setf causal-link-choice-variable
                (make-instance 'decision-variable
                               :name (format nil "s_{~a,~a}" p (id e-c)) ;; "s_{p, e_C}"
                               :domain (append (loop for e-p in producer-events collecting (string (id e-p))) (list "∘")))) ;; Do we need this too? (list "⟂"). Answer: YES WE DO! If we don't have, we could have a problem with causal link monitoring. Consider case 6 in the disturbance-3 test. If both causal links get violated, then conflicts for both s_{p, e_c} values are added - resulting in a conflict of {}, resulting in failure. However, the plan can still succeed, by choosing dv3 = c1. In other words, even if all candidate causal links for some activity are violated, the plan can still succeed if it can choose not execute that activity. In that case, we'd still need s_{p, e_c} to not have an empty domain.
          (add-variable! ss causal-link-choice-variable)


          ;; Allocate threat resolver variables if needed
          (dolist (entry (loop for entry being the hash-keys of threat-resolver-choice-variables-by-events collecting entry))
            (let (variable)
              (setf variable (make-instance 'decision-variable
                                            :name (format nil "o_{~a,~a,~a,~a}" (predicate cl-set) (id e-c) (id (first entry)) (id (second entry)))
                                            :domain (list "-1" "+1")))
              (setf (gethash entry threat-resolver-choice-variables-by-events) variable)
              (add-variable! ss variable)))))


      ;; Now that we've added all of the decision variables, recast everything
      (when-debug (ps) (format t "Recasting~%"))
      (recast-environments ps)


      ;; Convert causal links to constraints and temporal constraints
      (dolist (cl-set (pike-causal-link-sets ps))
        (with-slots (p e-c producer-events threat-events causal-links causal-link-choice-variable
                       threat-resolver-choice-variables-by-events constraints) cl-set

          ;; Set up causal link choice constraint
          (push (simplify-constraint! (make-instance 'implication-constraint
                                                     :implicant (make-instance 'conjunction-constraint
                                                                               :conjuncts (list (environment->constraint (guard e-c) ss)))
                                                     :consequent (make-instance 'disjunction-constraint
                                                                                :disjuncts
                                                                                (loop for e-p in producer-events collecting
                                                                                     (make-instance 'conjunction-constraint
                                                                                                    :conjuncts
                                                                                                    (list
                                                                                                     (make-instance 'assignment-constraint
                                                                                                                    :assignment (assignment causal-link-choice-variable (string  (id e-p))))
                                                                                                     (environment->constraint (guard e-p) ss)))))))
                constraints)


          ;; TODO add ordering constraints to producers if necessary
          (dolist (e-p producer-events)
            (unless (precedes? ps e-p e-c)
              (let ((env (environment-union-multiple ss
                                                     (make-environment-from-assignments ss (list (assignment causal-link-choice-variable (string (id e-p)))))
                                                     (guard e-p)
                                                     (guard e-c))))
                ;; TODO - don't hardcode 0.001!
                (add-temporal-constraint ps e-p e-c 0.001 +inf+ env))))



          ;; Now, set up the threat resolution constraints - both propositional constraints, as well
          ;; as additional temporal constraints that must be added.
          (dolist (e-p producer-events)
            (dolist (e-t threat-events)
              (let ((env (environment-union-multiple ss
                                                     (make-environment-from-assignments ss (list (assignment causal-link-choice-variable (string (id e-p)))))
                                                     (guard e-c)
                                                     (guard e-p)
                                                     (guard e-t))))
                (when (environment-feasible? ps env)
                  ;; Compute possible ordering constraints
                  (let* ((e-t<e-p (precedes? ps e-t e-p :env-context env))
                         (e-p<e-t (precedes? ps e-p e-t :env-context env))
                         (e-p<>e-t (and (not e-t<e-p)
                                        (not e-p<e-t)))
                         (e-t<e-c (precedes? ps e-t e-c :env-context env))
                         (e-c<e-t (precedes? ps e-c e-t :env-context env))
                         (e-c<>e-t (and (not e-t<e-c)
                                        (not e-c<e-t))))

                    ;; Sanity check: Certain things should never happen if all of these
                    ;; can feasibly happen together!
                    (assert (not (and e-t<e-p e-p<e-t)))
                    (assert (not (and e-t<e-c e-c<e-t)))

                    ;; There should be nine combinations: three for each pair of events.
                    ;; The above check assertion check eliminates from 16 down to 9.

                    ;; Handle each possibility on a case-by-case basis
                    (cond
                      ((or e-t<e-p
                           e-c<e-t)
                       ;; 5 cases. None of them are actually threats
                       ;; Do nothing!
                       t)


                      ((and e-p<e-t
                            e-t<e-c)
                       ;; 1 case. Can't move the threat, so add a conflict.
                       (let ((env (environment-union-multiple ss
                                                              (make-environment-from-assignments ss (list (assignment causal-link-choice-variable (string (id e-p)))))
                                                              (guard e-c)
                                                              (guard e-p)
                                                              (guard e-t))))
                         (push (environment->conflict env ss) constraints)))


                      ((and e-p<>e-t
                            e-t<e-c)
                       ;; 1 case. Add a TC to force threat before e-p
                       (let ((env (environment-union-multiple ss
                                                              (make-environment-from-assignments ss (list (assignment causal-link-choice-variable (string (id e-p)))))
                                                              (guard e-c)
                                                              (guard e-p)
                                                              (guard e-t))))
                         (add-temporal-constraint ps e-t e-p 0.001 +inf+ env)))


                      ((and e-p<e-t
                            e-c<>e-t)
                       ;; 1 case. Add a TC to force threat after e-c
                       (let ((env (environment-union-multiple ss
                                                              (make-environment-from-assignments ss (list (assignment causal-link-choice-variable (string (id e-p)))))
                                                              (guard e-c)
                                                              (guard e-p)
                                                              (guard e-t))))
                         (add-temporal-constraint ps e-c e-t 0.001 +inf+ env)))


                      ((and e-p<>e-t
                            e-c<>e-t)
                       ;; 1 case. Use a decision variable to choose whether
                       ;; we go before or after, and also add two
                       ;; corresponding TC's.

                       ;; Look up the decision variable
                       (let ((b (gethash `(,e-p ,e-t) threat-resolver-choice-variables-by-events))
                             env-before env-after)
                         ;; Make sure we have the variable
                         (assert b)
                         ;; This decision variable has already been added to the state space
                         ;; so we just need to add the temporal constraints now.
                         (setf env-before (environment-union-multiple ss
                                                                      (make-environment-from-assignments ss (list (assignment b "-1")))
                                                                      (make-environment-from-assignments ss (list (assignment causal-link-choice-variable (string (id e-p)))))
                                                                      (guard e-c)
                                                                      (guard e-p)
                                                                      (guard e-t)))
                         (setf env-after (environment-union-multiple ss
                                                                     (make-environment-from-assignments ss (list (assignment b "+1")))
                                                                     (make-environment-from-assignments ss (list (assignment causal-link-choice-variable (string (id e-p)))))
                                                                     (guard e-c)
                                                                     (guard e-p)
                                                                     (guard e-t)))
                         (add-temporal-constraint ps e-t e-p 0.001 +inf+ env-before)
                         (add-temporal-constraint ps e-c e-t 0.001 +inf+ env-after))))))))))))))


(defgeneric reduce-via-dominance-sets (ps events e-c))
(defmethod reduce-via-dominance-sets ((ps pike-session) (events list) (e-c event))
  "Helper function to reduce events from sequence that are dominated by *sets* of other
events.
  Precondition: events may be partially ordered, but must all affect some predicate (either
positively or negatively) that is a precondition of e-c."
  ;; (format t "*************************~%")
  ;; (format t "  e-c: ~a~%  events: ~a~%~%" e-c events)
  (with-slots (ss) ps
    (let ((events-reduced nil))
      (dolist (e events)
        ;; For each event, construct the list of all other events that are guaranteed to succeed
        ;; it yet still definitely come before e-c.
        (let (successors
              L-successors
              (dominated? nil))
          (dolist (e-s events)
            (when (and (not (eql e e-s))
                       (precedes? ps e e-s :env-context (guard e-c))
                       (precedes? ps e-s e-c :env-context (guard e)))
              (push e-s successors)))

          ;; (format t "  Considering ~a~%" e)
          ;; (format t "    Successors: ~a~%" successors)

          ;; Add guards of the successors to a label and compactify -- thus
          ;; computing a simplified label. This allows us to easily check
          ;; entailment later.
          (dolist (e-s successors)
            (setf L-successors (add-minimal L-successors (guard e-s) ss)))
          (setf L-successors (fully-compactify L-successors ss))

          ;; (format t "    L-successors: ")
          ;; (print-label L-successors ss t)
          ;; (format t "~%")

          ;; Check if \varphi_e |= \bigvee_{i}{\varphi_e_s_i}.
          ;; This will happen if any environment in L-successors subsumes
          ;; e-s.
          (dolist (varphi-s L-successors)
            (when (subsumes? varphi-s (guard e) ss)
              ;; Aha! Got one! This event can be removed.
              ;; (format t "    Dominated: ~a~%" e)
              (setf dominated? t)
              (return)))
          ;; If it's not dominated, add to events-reduced.
          (unless dominated?
            (push e events-reduced))))
      ;; (format t "~%  Returning: ~a~%" events-reduced)
      events-reduced)))



(defgeneric add-temporal-constraint (ps event-from event-to lb ub label))
(defmethod add-temporal-constraint ((ps pike-session) (event-from event) (event-to event) lb ub label)
  (with-slots (ldg ss causal-link-temporal-constraints) ps
    (let ((lvs-lb (make-lvs))
          (lvs-ub (make-lvs))
          edge-ub edge-lb)

      (add-pair lvs-lb (create-lv ss (- lb) label))
      (add-pair lvs-ub (create-lv ss ub label))

      (when-debug (ps)
                  (format t "Adding [~a, ~a] " (pretty-number lb) (pretty-number ub))
                  (print-environment label ss t)
                  (format t " from ~a to ~a~%" event-from event-to))

      ;; Add the edges with these lvs's
      (setf edge-ub (add-edge event-from event-to lvs-ub ldg))
      (setf edge-lb (add-edge event-to event-from lvs-lb ldg))

      ;; Push these constraints to the list of causal link added temporal constraints
      (push edge-ub causal-link-temporal-constraints)
      (push edge-lb causal-link-temporal-constraints))))


(defgeneric environment-feasible? (ps env))
(defmethod environment-feasible? ((ps pike-session) environment)
  "Returns T if both this environment is valid (doesn't assign
   conflciting values to variables, like x=1 and x=2), and isn't
   part of a temporal inconsistency."
  (with-slots (ss temporal-conflict-environments) ps
    (and (env-valid? ss environment)
         (notany #'(lambda (c)
                     (subsumes? c environment ss))
                 temporal-conflict-environments))))


(defgeneric precedes? (ps e-A e-B &key &allow-other-keys))
(defmethod precedes? ((ps pike-session) (e-A event) (e-B event) &key (env-context nil))
  "Returns T if e-A is guaranteed to precede e-B in ALL possible
   schedules where both e-A and e-B are executed, and nil otherwise.
   env-context contains any other assignments that may also be known
   to be true.

   Note that all of the three cases below are possible:
   1.) e-A precedes e-B
   2.) e-B precedes e-A
   3.) Neither e-A nor e-B precede each other

   It is not possible (in a consistent plan) however for BOTH
   to precede each other. If the plan is inconsistent, this
   could happen though."
  (with-slots (apsp ss) ps
    (let ((env (if env-context
                   (environment-union-multiple ss (guard e-A) (guard e-B) env-context)
                   (environment-union (guard e-A) (guard e-B) ss))))
      ;; Query the LVS with this environment.
      (< (query-lvs ss
                    (apsp-shortest-path apsp e-B e-A)
                    env)
         0))))


(defgeneric succedes? (ps e-A e-B &key &allow-other-keys))
(defmethod succedes? ((ps pike-session) (e-A event) (e-B event) &key (env-context nil))
  "Returns T if e-A is guaranteed to succede e-B in ALL possible
   schedules where both e-A and e-B are executed, and nil otherwise."
  (precedes? ps e-B e-A :env-context env-context))


(defgeneric incomparable? (ps e-A e-B &key &allow-other-keys))
(defmethod incomparable? ((ps pike-session) (e-A event) (e-B event) &key (env-context nil))
  "Returns true if neither e-A precedes e-B nor e-B precedes e-A,
   and nil otherwise."
  (and (not (precedes? ps e-A e-B :env-context env-context))
       (not (succedes? ps e-A e-B :env-context env-context))))


(defgeneric affects-negatively? (e p-about))
(defmethod affects-negatively? ((e event) p-about)
  "Returns T if e has an effect equal to p-about, and that
   effect is negative."
  (let ((p (find p-about (effects e) :key #'about :test #'equal)))
    (unless p
      (error "Huh? Can't find the effect for an event anymore!"))
    (negative? p)))
