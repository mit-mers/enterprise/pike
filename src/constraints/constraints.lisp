;;;; Copyright (c) 2011-2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)


(defclass constraint ()
  ()
  (:documentation "A super class for all constraint types."))

;; TODO cleanup the accessor names in this file?

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Assignment
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass assignment-constraint (constraint)
  ((assignment
    :type decision-variable-assignment
    :initarg :assignment
    :accessor assignment-constraint-assignment))
  (:documentation "Represents an assignment to a decision variable,
like x=a, as a type of constraint."))

(defmethod print-object ((ac assignment-constraint) s)
  (with-slots (assignment) ac
    (format s "~a" assignment)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Constant
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass constant-constraint (constraint)
  ((constant
    :type (or nil T)
    :initarg :constant
    :accessor constant-constraint-constant))
  (:documentation "Represents either True or False"))

(defmethod print-object ((cc constant-constraint) s)
  (with-slots (constant) cc
    (if constant
        (format s "True")
        (format s "False"))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Conjunction
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass conjunction-constraint (constraint)
  ((conjuncts
    :type list
    :initarg :conjuncts
    :accessor conjunction-constraint-conjuncts))
  (:documentation "Represents a conjuntion of other constraints."))


(defmethod print-object ((cc conjunction-constraint) s)
  (with-slots (conjuncts) cc
    (format s "(~{~a~^ ∧ ~})" conjuncts)))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Disjunction
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass disjunction-constraint (constraint)
  ((disjuncts
    :type list
    :initarg :disjuncts
    :accessor disjunction-constraint-disjuncts))
  (:documentation "Represents a disjunction of other constraints"))


(defmethod print-object ((dc disjunction-constraint) s)
  (with-slots (disjuncts) dc
    (format s "(~{~a~^ ∨ ~})" disjuncts)))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Negation
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass negation-constraint (constraint)
  ((expression
    :type constraint
    :initarg :expression
    :accessor negation-constraint-expression))
  (:documentation "A negation of another constraint"))

(defmethod print-object ((nc negation-constraint) s)
  (with-slots (expression) nc
    (format s "¬(~a)" expression)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Implication (can also be implemented as disjunction with a negation)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass implication-constraint (constraint)
  ((implicant
    :type constraint
    :initarg :implicant
    :accessor implication-constraint-implicant)
   (consequent
    :type constraint
    :initarg :consequent
    :accessor implication-constraint-consequent)))

(defmethod print-object ((ic implication-constraint) s)
  (with-slots (implicant consequent) ic
    (format s "(~a ⇒ ~a)" implicant consequent)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Equivalance (can also be implemented as two implications)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass equivalence-constraint (constraint)
  ((lhs
    :type constraint
    :initarg :lhs
    :accessor equivalence-constraint-lhs
    :documentation "Left hand side of equivalence")
   (rhs
    :type constraint
    :initarg :rhs
    :accessor equivalence-constraint-rhs
    :documentation "Right hand side of equivalence")))

(defmethod print-object ((eqc equivalence-constraint) s)
  (with-slots (lhs rhs) eqc
    (format s "(~a ⇔ ~a)" lhs rhs)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Conflict (can also be implemented as a negation of a conjunction)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass conflict-constraint (constraint)
  ((expressions
    :type list
    :initarg :expressions
    :accessor conflict-constraint-expressions)))


(defmethod print-object ((cc conflict-constraint) s)
  (with-slots (expressions) cc
    (format s "Conflict(~{~a~^, ~})" expressions)))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Convenience tools
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun make-constraint (s)
  "A purely concenience helper function that generates a constraint object
   from an infix version of logical notation, using the operators:
      * &, &&, and, ∧
      * |, ||, ∨, or
      * not, ~, ¬
      * =>, ->, implies, ⇒
      * <=>, <->, equivalent, equivalence, iff, ⇔
      * True, T, 1, False, F, 0
      * = (for assignments)

   Input: An s-expression containing these symbols and others
   Output: A constraint object representation of it."
  (when (or (typep s 'symbol)
            (typep s 'string))
    (when (member (string s) `("True" "T" "1") :test #'equalp)
      (return-from make-constraint (make-instance 'constant-constraint :constant t)))
    (when (member (string s) `("False" "F" "0") :test #'equalp)
      (return-from make-constraint (make-instance 'constant-constraint :constant nil))))
  (unless (typep s 'list)
    (error "Malformed constraint s-expression!"))
  (when (eql s nil)
    (make-instance 'constant-constraint :constant nil))
  (let* ((op (string  (first s))))
    (cond
      ((member op '("&" "&&" "and" "∧") :test #'equalp)
       (make-instance 'conjunction-constraint
                      :conjuncts (mapcar #'make-constraint (rest s))))

      ((member op '("|" "||" "or" "∨") :test #'equalp)
       (make-instance 'disjunction-constraint
                      :disjuncts (mapcar #'make-constraint (rest s))))

      ((member op '("not" "~" "¬") :test #'equalp)
       (unless (eql 2 (length s))
         (error "Invalid number of arguments for <=> constraint!"))
       (make-instance 'negation-constraint
                      :expression (make-constraint (second s))))

      ((member op '("=>" "->" "implies" "⇒") :test #'equalp)
       (unless (eql 3 (length s))
         (error "Invalid number of arguments for => constraint!"))
       (make-instance 'implication-constraint
                      :implicant (make-constraint (second s))
                      :consequent (make-constraint (third s))))

      ((member op '("<=>" "<->" "equivalent" "equivalence" "iff" "⇔") :test #'equalp)
       (unless (eql 3 (length s))
         (error "Invalid number of arguments for <=> constraint!"))
       (make-instance 'equivalence-constraint
                      :lhs (make-constraint (second s))
                      :rhs (make-constraint (third s))))

      ((member op '("True" "T" "1") :test #'equalp)
       (make-instance 'constant-constraint :constant t))

      ((member op '("False" "F" "nil" "0") :test #'equalp)
       (make-instance 'constant-constraint :constant nil))

      ((member op '("=") :test #'equalp)
       (unless (eql 3 (length s))
         (error "Invalid number of arguments for = constraint!"))
       (make-instance 'assignment-constraint :assignment (assignment (second s) (third s)))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Tools to facilitate constraint hashing and comparisons
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; TODO: Apply this to constraint->node methods in label propagation!

(defun equal-constraints (c1 c2)
  "Checks if two constraints, c1 and c2, are
  syntactically equal. Note that two contraints may
  both be effectively equivalent (in terms of solutions)
  but may not be considered equal by this method."

  ;; If the constraint types are not equal, then they're certainly not equal
  (unless (eql (type-of c1) (type-of c2))
    (return-from equal-constraints nil))

  ;; Now, split on type.
  ;; Assignments: whether the assignments are the same.
  (cond ((typep c1 'assignment-constraint)
         (eql (assignment-constraint-assignment c1)
              (assignment-constraint-assignment c2)))

        ;; Constants: See if the constants are equal
        ((typep c1 'constant-constraint)
         (eql (constant-constraint-constant c1)
              (constant-constraint-constant c2)))

        ;; Conjunction: Compare set of conjuncts
        ((typep c1 'conjunction-constraint)
         (equal-sets-of-constraints (conjunction-constraint-conjuncts c1)
                                    (conjunction-constraint-conjuncts c2)))

        ;; Disjunction: Compare set of disjuncts
        ((typep c1 'disjunction-constraint)
         (equal-sets-of-constraints (disjunction-constraint-disjuncts c1)
                                    (disjunction-constraint-disjuncts c2)))

        ;; Negation: recurse on subexpressions
        ((typep c1 'negation-constraint)
         (equal-constraints (negation-constraint-expression c1)
                            (negation-constraint-expression c2)))


        ;; Implication: Recurse on antecedent and consequent
        ((typep c1 'implication-constraint)
         (and (equal-constraints (implication-constraint-implicant c1)
                                 (implication-constraint-implicant c2))
              (equal-constraints (implication-constraint-consequent c1)
                                 (implication-constraint-consequent c2))))

        ;; Equivalence: Recurse on lhs and rhs
        ((typep c1 'equivalence-constraint)
         (and (equal-constraints (equivalence-constraint-lhs c1)
                                 (equivalence-constraint-lhs c2))
              (equal-constraints (equivalence-constraint-rhs c1)
                                 (equivalence-constraint-rhs c2))))

        ;; Conflict: Check that the sets are matching
        ((typep c1 'conflict-constraint)
         (equal-sets-of-constraints (conflict-constraint-expressions c1)
                                    (conflict-constraint-expressions c2)))

        (T
         (error (format nil "Unsupported type of constraints: ~a, ~a" (type-of c1) (type-of c2))))))


(defun hash-constraint (c)
  "Returns a measure of the has of this constraint.
   Has the key property required of any hash function:

   (a = b) => (hash(a) == hash(b))

  The converse of the above is often true, but is not
  guaranteed to be true."
  ;; Split on the type.
  ;; Assignment constraint: sxhash of the assignment object (which are unique)
  (cond ((typep c 'assignment-constraint)
         (+ (sxhash (decision-variable-name (assignment-variable (assignment-constraint-assignment c))))
            (sxhash (assignment-value (assignment-constraint-assignment c)))))

        ;; Constant constraint: sxhash the constant
        ((typep c 'constant-constraint)
         (sxhash (constant-constraint-constant c)))

        ;; Conjunction: sum of recursive hashes of "and" and all elements
        ;; Relies that addition is associative.
        ((typep c 'conjunction-constraint)
         (+ (sxhash "and")
            (apply #'+ (mapcar #'hash-constraint (conjunction-constraint-conjuncts c)))))

        ;; Disjunction: sum of recrusive hashes of "or" and all elements
        ;; Relies that addition is associative.
        ((typep c 'disjunction-constraint)
         (+ (sxhash "or")
            (apply #'+ (mapcar #'hash-constraint (disjunction-constraint-disjuncts c)))))

        ;; Negation: negate the expression and add sxhash of "not"
        ((typep c 'negation-constraint)
         (+ (sxhash "not")
            (hash-constraint (negation-constraint-expression c))))

        ;; Implication: Sum of implies, and recursive applied to both parts
        ((typep c 'implication-constraint)
         (+ (sxhash "implies")
            (hash-constraint (implication-constraint-implicant c))
            (hash-constraint (implication-constraint-consequent c))))

        ;; Equivalence: Sum of equivalence, and recursive applied to both parts
        ((typep c 'equivalence-constraint)
         (+ (sxhash "equivalence")
            (hash-constraint (equivalence-constraint-lhs c))
            (hash-constraint (equivalence-constraint-rhs c))))

        ;; Conflict: Sum of sxhash of "conflict", and recursive applied to all parts
        ((typep c 'conflict-constraint)
         (+ (sxhash "conflict")
            (apply #'+ (mapcar #'hash-constraint (conflict-constraint-expressions c)))))

        (T
         (error "Invalid constraint type -- can't hash!"))))


(defun equal-sets-of-constraints (l1 l2)
  "Helper method that compares to lists of constraints -- l1 and l2 --
  and tries to see if they're equal, barring the ordering of the constraints
  within those sets.

  This is accomplished by attempting to match each term of l1 to l2 (invariant of order).
  Namely, create a copy of l2 -- and then remote from it every element from l1, using
  our constraint-equal function above. We should end with an empty list at the end --
  if we do, then we're guaranteed the sets are equal since there is a bijection between
  elements."
  ;; Optimization: Return false immediately if l1 and l2 are different lengths
  (unless (= (length l1) (length l2))
    (return-from equal-sets-of-constraints nil))
  ;; Attempt to find a bijection by removing elements from a copy of l2
  (let ((l (sequence:copy-seq l2))
        deleted?)
    (dolist (exp l1)
      (multiple-value-setq (l deleted?) (delete-one-from-list-and-tell! exp l :test 'equal-constraints))
      ;; If nothing was deleted, we can terminate early.
      (unless deleted?
        (return-from equal-sets-of-constraints nil)))
    ;; Return T if we get here -- we were able to match everything.
    T))


;; Register the two for use in the genhash module
(when (not (gethash 'pike-constraints-equal genhash::*hash-test-designator-map*))
  (genhash:register-test-designator 'pike-constraints-equal
                                    #'hash-constraint
                                    #'equal-constraints))
