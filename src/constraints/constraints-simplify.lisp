;;;; Copyright (c) 2011-2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.


;;;; This file contains some code to do some basic simplifications over constraints.
;;;; Note that it is not guaranteed to put anything in a minimal form, and it's
;;;; not guaranteed to do any very deep / complex simplifications. Rather, it's
;;;; easy stuff to clean things up a bit.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)

(defgeneric simplify-constraint! (c)
  (:documentation "Perform some basic constraint simplification. Reserves
the right to modify constraints (hence the !)."))


(defmethod simplify-constraint! ((c conjunction-constraint))
  "Conjunction constraints: Try to recursively simplify all of the conjuncts.
  If any of the resulting conjuncts evaluate to True (i.e., are valid), then
  remove from the conjunction.
  If any of the conjuncts are themselves conjuncts, merge into this conjunction.
  If any of the conjuncts evaluate to False, then return False as well (since
  the conjunction is necessarily false).
  If the conjunction contains just one conjunct, return it.
  If the conjunction is empty, return True."
  (with-slots (conjuncts) c
    ;; Recursively simplify each of the conjuncts
    (setf conjuncts (loop for ci in conjuncts collecting (simplify-constraint! ci)))
    ;; Remove any that are True
    (setf conjuncts (loop for ci in conjuncts when (not (is-true-constraint? ci)) collect ci))
    ;; If any of the conjuncts are themselves conjunctions, merge into this one
    (let ((subconjuncts (loop for ci in conjuncts when (typep ci 'conjunction-constraint) collect ci)))
      (dolist (subconjunct subconjuncts)
        (setf conjuncts (delete subconjunct conjuncts))
        (dolist (ci-subconjunct (conjunction-constraint-conjuncts subconjunct))
          (push ci-subconjunct conjuncts))))
    ;; If any of the conjuncts are False, return False.
    (when (some #'is-false-constraint? conjuncts)
      (return-from simplify-constraint! (make-instance 'constant-constraint :constant nil)))
    ;; Remove any duplicates expressions, since x & x is equivalent to x.
    (setf conjuncts (remove-duplicates conjuncts :test #'equal-constraints))
    ;; If there is just one conjunct, return it.
    (when (= 1 (length conjuncts))
      (return-from simplify-constraint! (first conjuncts)))
    ;; If there are no conjuncts, return True.
    (when (= 0 (length conjuncts))
      (return-from simplify-constraint! (make-instance 'constant-constraint :constant t)))
    ;; Otherwise, return the conjunction.
    c))


(defmethod simplify-constraint! ((d disjunction-constraint))
  "Disjunction constraints: Try to recursively simplify all of the disjuncts.
  If any of them result in False (i.e., invalid), then remove from the disjunction.
  If any of the disjuncts evaluate to True, then return True as well (since
  the disjunction is then valid).
  If any of the disjuncts are themselves disjuncts, merge into this disjunction.
  If the disjunction contains just one disjunct, return it.
  If the disjunction is empty, return False."
  (with-slots (disjuncts) d
    ;; Recursively simplify each of the conjuncts
    (setf disjuncts (loop for di in disjuncts collecting (simplify-constraint! di)))
    ;; Remove any that are False
    (setf disjuncts (loop for di in disjuncts when (not (is-false-constraint? di)) collect di))
    ;; If any of the disjuncts are themselves disjunctions, merge into this one
    (let ((subdisjuncts (loop for di in disjuncts when (typep di 'disjunction-constraint) collect di)))
      (dolist (subdisjunct subdisjuncts)
        (setf disjuncts (delete subdisjunct disjuncts))
        (dolist (di-subdisjunct (disjunction-constraint-disjuncts subdisjunct))
          (push di-subdisjunct disjuncts))))
    ;; If any of the disjuncts are True, return True.
    (when (some #'is-true-constraint? disjuncts)
      (return-from simplify-constraint! (make-instance 'constant-constraint :constant t)))
    ;; Remove any duplicates expressions, since x or x is equivalent to x.
    (setf disjuncts (remove-duplicates disjuncts :test #'equal-constraints))
    ;; If there is just one disjunct, return it.
    (when (= 1 (length disjuncts))
      (return-from simplify-constraint! (first disjuncts)))
    ;; If there are no disjuncts, return False.
    (when (= 0 (length disjuncts))
      (return-from simplify-constraint! (make-instance 'constant-constraint :constant nil)))
    ;; Otherwise, return the disjunction.
    d))


(defmethod simplify-constraint! ((im implication-constraint))
  "Implication constraints of the form X => Y.
   If X is True, then return Y.
   If X is False, then return True.
   If Y is True, return True.
   If Y is False, return not(X)."
  (with-slots (implicant consequent) im
    ;; Try to simplify both
    (setf implicant (simplify-constraint! implicant))
    (setf consequent (simplify-constraint! consequent))
    ;; Run some special cases
    (cond
      ((is-true-constraint? implicant)
       consequent)
      ((is-false-constraint? implicant)
       (make-instance 'constant-constraint :constant t))
      ((is-true-constraint? consequent)
       (make-instance 'constant-constraint :constant t))
      ((is-false-constraint? consequent)
       (simplify-constraint! (make-instance 'negation-constraint :expression implicant)))
      (t im))))

(defmethod simplify-constraint! ((eqc equivalence-constraint))
  "Equivalence constraints of the form X <=> Y.
   If Y is True, then return X.
   If X is True, then return Y.
   If X is False, then return not(Y)
   If Y is False, then return not(X)
   If X equals Y, return True
   If X equals not(Y), return False"
  (with-slots (lhs rhs) eqc
    ;; Try to simplify both
    (setf lhs (simplify-constraint! lhs))
    (setf rhs (simplify-constraint! rhs))
    ;; Run some special cases
    (cond
      ((is-true-constraint? rhs)
       lhs)
      ((is-true-constraint? lhs)
       rhs)
      ((is-false-constraint? lhs)
       (simplify-constraint! (make-instance 'negation-constraint :expression rhs)))
      ((is-false-constraint? rhs)
       (simplify-constraint! (make-instance 'negation-constraint :expression lhs)))
      ((equal-constraints lhs rhs)
       (make-instance 'constant-constraint :constant t))
      ((and (typep lhs 'negation-constraint)
            (equal-constraints (negation-constraint-expression lhs) rhs))
       (make-instance 'constant-constraint :constant nil))
      ((and (typep rhs 'negation-constraint)
            (equal-constraints (negation-constraint-expression rhs) lhs))
       (make-instance 'constant-constraint :constant nil))
      (t eqc))))


(defmethod simplify-constraint! ((nc negation-constraint))
  "Negation constraint: If the expression is also a negation, cancel them out.
   If the expression is True, return False. If it's False, return True.
   Otherwise, return unchanged."
  (with-slots (expression) nc
    ;; Recursively simplify
    (setf expression (simplify-constraint! expression))
    (cond
      ;; Two negations cancel
      ((typep expression 'negation-constraint)
       (negation-constraint-expression expression)) ;; It's already been simplified
      ;; Boolean constraint - reverse it
      ((typep expression 'constant-constraint)
       (with-slots (constant) expression
         (setf constant (not constant))
         expression))
      ;; Anything else - just return
      (t nc))))


(defmethod simplify-constraint! (c)
  "For all other constraints: Return unmodified."
  c)


(defun is-true-constraint? (c)
  "Helper method to return whether this constraint is True."
  (and (typep c 'constant-constraint)
       (constant-constraint-constant c)))


(defun is-false-constraint? (c)
  "Helper method to return whether this constraint is False."
  (and (typep c 'constant-constraint)
       (not (constant-constraint-constant c))))
