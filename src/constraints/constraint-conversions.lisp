;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Some useful methods for converting between constraint types, and
;; between environments.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun environment->conflict (environment ss)
  "Takes an environment (which represents a conjunction of assignments), and
   interprets it as a conflict that is then returned."
  (declare (type environment environment)
           (type state-space-bits ss))
  (make-instance 'conflict-constraint
                 :expressions (loop for assignment in (get-assignments-from-environment ss environment)
                                   collecting (make-instance 'assignment-constraint :assignment assignment))))

(defun conflict->environment (conflict ss)
  "Does the opposite of the above."
  (declare (type conflict-constraint conflict)
           (type state-space-bits ss))
  (make-environment-from-assignments ss (mapcar #'assignment-constraint-assignment (conflict-constraint-expressions conflict))))



(defun environment->conjunction (environment ss)
  "Takes an environment (which represents a conjunction of assignments),
   and interprets as a conjunction that is then returned."
  (declare (type environment environment)
           (type state-space-bits ss))
  (make-instance 'conjunction-constraint
                 :conjuncts (loop for assignment in (get-assignments-from-environment ss environment)
                                 collecting (make-instance 'assignment-constraint :assignment assignment))))

(defun environment->constraint (environment ss)
  "Takes an environment (which represents a conjunction of assignments),
   and interprets as a conjunction that is then returned. If it's empty
   however, return the True constraint (equivalent, but prettier sometimes)."
  (declare (type environment environment)
           (type state-space-bits ss))
  (if (is-empty-environment? environment ss)
      ;; If it's an empty environment, just return True.
      (make-instance 'constant-constraint
                     :constant T)

      ;; Otherwise, return the conjunction.
      (make-instance 'conjunction-constraint
                  :conjuncts (loop for assignment in (get-assignments-from-environment ss environment)
                                collecting (make-instance 'assignment-constraint :assignment assignment)))))

(defun conjunction->environment (c ss)
  "Does the opposite of the above."
  (declare (type conjunction-constraint c)
           (type state-space-bits ss))
  (make-environment-from-assignments ss (mapcar #'assignment-constraint-assignment (conjunction-constraint-conjuncts c))))
