;;;; Copyright (c) 2016 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; A lightweight, less feature-ful version of Pike.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass pike-session-lite (pike-session)
  ((event-ht-preceders
    :initform (make-hash-table :test 'eql)
    :documentation "Maps events to a list of immediately-preceding events")
   (choices-being-waited-on
    :initform nil
    :documentation "A list of choices currently being waited on")))

(defgeneric create-pike-session-lite (po))
(defmethod create-pike-session-lite ((po pike-options))
  "Creates and returns an instance of a pike session."
  (let ((ps (make-instance 'pike-session-lite :options po)))
    ;; Set up callbacks
    (pike-init-hooks ps)
    ps))



(defmethod pike-compile ((ps pike-session-lite))
  "Causes Pike to completely compile the plan and prepare for
   iminent execution."
  (compile-plan ps))


(defmethod pike-execute ((ps pike-session-lite))
  "Causes Pike to begin execution."
  (online-execute ps))



(defmethod compile-plan ((ps pike-session-lite))
  "Top-level compilation function. This prepares
   Pike for execution, and may throw errors if problems
   are detected."

  ;; TODO - check success for each for each of these!
  ;; Use signals, etc. Make sure each of these throws
  ;; properly.

  ;; Load the TPN plan file
  (load-plan ps)

  ;; Load an appropriate plant model for this plan
  ;; ex., PDDL, RMPL, NO-OP, etc.
  (load-plant-model ps)

  ;; Check validity of TPN
  (unless (plan-looks-sane? ps)
    (return-from compile-plan nil))

  ;; Initializes internal data structures
  ;; from the now-loaded TPN and plant model
  (initialize-from-plan-and-plant-model ps)

  ;; Setup datastructures and do a bit more work to get ready to execute.
  (initialize-for-execution ps)

  ;; Sets up a hash table mapping each event to all immediate local
  ;; predecessors
  (setup-event-predecessors ps)

  T)


(defmethod initialize-for-execution ((ps pike-session-lite))
  "Called to do some heavy lifting before execution starts."
  (with-slots (execution-context ldg ss plant-model pike-options) ps
    (setf execution-context (make-instance 'execution-context))
    (with-slots (time S events-remaining-to-execute
                      event->executed-time event->execution-window event->executability
                      epsilon state state-lock status) execution-context

      ;; We're now about to execute
      (setf status :executing)

      ;; Set up the executed sets
      ;; TODO - just setf, will stop reversing.
      (dolist (e (mtk-graph:get-vertices ldg))
              (push e events-remaining-to-execute))


      ;; Update the schedulability of all events
      (dolist (e (mtk-graph:get-vertices ldg))
              (setup-executability-flags ps e))


      ;; Set up causal-link execution monitoring
      (initialize-causal-link-monitor ps)

      ;; Set up an empty initial state.
      ;; The real state will be asserted when event e_initial
      ;; is executed.
      (bordeaux-threads:with-recursive-lock-held (state-lock)
                                                 (setf state (get-empty-state ps plant-model)))


      ;; Set the forgiveness factor, epsilon
      (setf epsilon (pike-options-epsilon pike-options))


      ;; Set the initial execution time
      (setf time 0.0)

      (execute-event ps (event-initial (pike-ldg ps)) time))))

(defgeneric setup-event-predecessors (ps))
(defmethod setup-event-predecessors ((ps pike-session-lite))
  "Sets up the magical hash table mapping each event to its predecessors."
  (with-slots (event-ht-preceders execution-context) ps
    (with-slots (events-remaining-to-execute) execution-context
      (dolist (e events-remaining-to-execute)
        (setf (gethash e event-ht-preceders)
              (get-preceding-events (pike-ldg ps) e))))))


(defgeneric get-preceding-events (ldg e))
(defmethod get-preceding-events ((ldg labeled-distance-graph) (e event))
  "Helper method that returns the events preceding this one, based purely on the
   local temporal constraints."
  (let* ((edges (mtk-graph:find-edges-with-source-vertex e ldg)))
    (loop for edge in edges
       when (and (not (equal (second (mtk-graph:get-vertices edge)) e))
                 (some #'(lambda (pair) (<= (lv-value pair) 0)) (lvs-pairs (mtk-graph:get-weight edge ldg))))
       collect (second (mtk-graph:get-vertices edge)))))





(defmethod dispatch-activity ((ps pike-session-lite) (activity activity))
  "Dispatches the given event."
  (with-slots (execution-context ss) ps
    (with-slots (time) execution-context
      (pike-call-hooks ps :activity-dispatch activity 0.05 100.0 time))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Main entry point for online execution
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defmethod online-execute ((ps pike-session-lite))
  ;; Actually execute the plan online with execution monitoring!


  ;; Initialize stuff. Time to 0, state to initial state, set up an
  ;; execution context, etc.
  (begin-execution ps)

  (with-slots (execution-context) ps
    (with-slots (time events-remaining-to-execute status) execution-context

      ;; Call the start hook
      (pike-call-hooks ps :execution-start ps)


      ;; Main while loop:
      (do ()
          ((execution-complete? ps) T)

        ;; Update current time
        (update-time ps)

        ;; TODO Perform execution monitor checks, if we've received
        ;; any new state updates
        ;;(causal-link-check ps)

        ;; Process any activities that just completed, by
        ;; executing their end events.
        (process-activities-that-just-finished ps)

        ;; Process any decision variables that have been made
        ;; externally (i.e., generally uncontrollable choices)
        ;; and commit to them
        (process-externally-made-choices ps)

        ;; Check and see which choices (if any) we're waiting on.
        (are-we-waiting-on-choices ps)

        ;; Process any missed / violated windows
        ;; (process-missed-event-windows ps)

        ;; After doing the above, check if the plan can still succeed.
        ;; something with (can-execution-succed? ps)
        ; (unless (can-execution-succeed? ps)
        ;   (setf status :failed)
        ;   (return))


        ;; The main part. Keep calling execute-cycle until it returns nil (it didn't just
        ;; execute any events). This will ensure that time doesn't get incremented
        ;; until we've executed everything there is to execute right now.
        (do ()
            ((not (execute-cycle ps)) T))

        ;; Call any hooks that are registered
        ;; each time an "execution loop" completes
        ;; (such hooks usually off, mainly used for
        ;; debugging and testing)
        (pike-call-hooks ps :execution-cycle-tick)

        ;; Politely yield the thread
        (bordeaux-threads:thread-yield)
        ;; Sleep for 0.01 seconds to avoid pegging the CPU.
        ;;
        ;; TODO: Update 0.01 to something more appropriate to Pike's maximum
        ;; resolution?
        ;;
        ;; TODO: This is a stopgap measure to avoid CPU pegging. But it does
        ;; still use quite a bit of CPU. See the Issue at
        ;; https://git.mers.csail.mit.edu/mars-toolkit/pike/issues/3 for a
        ;; discussion of what this should be replaced with.
        (sleep 0.01))

      ;; Update the status to mark successful completion.
      (when (eql status :executing)
        (setf status :complete))

      ;; Call the execution end hook
      (pike-call-hooks ps :execution-end ps))))


(defmethod execute-cycle ((ps pike-session-lite))
  "Runs one execution cycle. Returns T if it executes an event, and
   nil otherwise if nothing was executed."
  (with-slots (execution-context) ps
    (with-slots (time events-remaining-to-execute) execution-context
      ;; Loop over all events, trying to find ones that can be
      ;; executed. In other words, find the first event that can
      ;; be executed and execute it. We can execute it if we can
      ;; commit to any conflicts it would entail to execute it now,
      ;; as well of course to the event's environment.
      (dolist (e events-remaining-to-execute)
              (when (can-execute-event-now? ps e)
                (execute-event ps e time)
                (return-from execute-cycle T))
      nil))))


(defmethod execute-event ((ps pike-session-lite) event t_reported &key &allow-other-keys)
  (with-slots (time events-remaining-to-execute event->executed-time) (pike-execution-context ps)
    ;; Mark the event as executed (with the actual time relative to start)
    (setf (gethash event event->executed-time) (raw-time->execution-time ps (get-time ps)))

    ;; Remove it from the list of events remaining to be executed
    (setf events-remaining-to-execute (delete event events-remaining-to-execute))

    ;; Commit to this event's environment
    (commit-to-environment ps (guard event) time)

    ;; Commit to these conflicts required to execute the event now.
    ;; (dolist (conflict conflicts)
    ;;   (add-conflict-environment ps conflict :reason (gethash conflict conflict-reasons)))

    ;; Prune any other events that will never be executed.
    (prune-events-that-wont-be-executed ps)
    ;; (prune-execution-windows ps) # TODO - BENCHMARK HACK REMOVE

    ;; Propagate time to immediate neighbors and update their execution bounds
    ;; (propagate-execution-time ps event time)

    ;; Trigger hooks (but ignore for initial and final events, since we added
    ;; those, not the callee)
    (unless (is-special-event? ps event)
      (pike-call-hooks ps :event-executed ps event time))

    ;; Dispatch or end any appropriate activities
    (when (slot-boundp event 'activity-start)
      (dispatch-activity ps (event-activity-start event)))

    ;; Update the current state.
    (update-state-from-event ps event t_reported)

    ;; Update the currently-active monitor conditions
    (causal-link-update-event-executed ps event)

    T))


(defmethod process-activities-that-just-finished ((ps pike-session-lite))
  (with-slots (activity-end-mailbox failed-activities time) (pike-execution-context ps)
    (let (entry found?)
      ;; Get the first entry
      (multiple-value-setq (entry found?) (safe-queue:mailbox-receive-message-no-hang activity-end-mailbox))
      ;; This is basically a do-while loop.
      (do ()
          ((not found?))

        (let* ((activity (first entry))
               (end-event (activity-end-event activity))
               ;; (time-completion (second entry))
               (success? (third entry)))
          ;; (conflicts nil)
          ;; reasons

          ;; (format t "activity end time: ~a~%" time-completion)

          ;; Did this activity fail? If so, add it to the list of failed activities
          (unless success?
            (push activity failed-activities)
            ;; Call the appropriate callback
            (pike-call-hooks ps :activity-failure activity time))

          ;; Remove the :activity-end-event flag since we can now safely
          ;; execute this event (prevents false conflicts)

          ;; Execute the end event and add any appropriate conflicts!
          ;;(multiple-value-setq (conflicts reasons) (get-conflict-environments-needed-to-execute-event ps end-event))
          ;;(execute-event-lite ps end-event time-completion)
          (remove-event-executability-flag ps end-event :activity-end-event))

        ;; Try and get more entries
        (multiple-value-setq (entry found?) (safe-queue:mailbox-receive-message-no-hang activity-end-mailbox))))))


(defmethod are-we-waiting-on-choices ((ps pike-session-lite))
  ;; Updates the set of decision variables we're waiting on
  (with-slots (execution-context event-ht-preceders choices-being-waited-on) ps
    (with-slots (events-remaining-to-execute event->executability event->uncontrollable-choices time) execution-context
      (let* ((choices-currently-waiting-on nil))
        (dolist (e events-remaining-to-execute)
          (let* ((executability-flags-minus-choices (remove :depends-on-unmade-uncontrollable-choice (gethash e event->executability))))
            ;; Check if this event is ready to be executed, barring any unmade uncontrollable choices
            (when (and (eql nil executability-flags-minus-choices)
                       (eql nil (intersection events-remaining-to-execute
                                              (gethash e event-ht-preceders))))
              ;; This event would otherwise be executable!
              (let ((choices-waiting-on (gethash e event->uncontrollable-choices)))
                (when choices-waiting-on
                  (dolist (dv choices-waiting-on)
                    (when (not (member dv choices-currently-waiting-on))
                      (push dv choices-currently-waiting-on))))))))
        (unless (equal choices-being-waited-on choices-currently-waiting-on)
          (setf choices-being-waited-on choices-currently-waiting-on)
          (pike-call-hooks ps :updated-choices-being-waited-on choices-being-waited-on time))))))


(defgeneric can-execute-event-now? (ps e))
(defmethod can-execute-event-now? ((ps pike-session-lite) (e event))
  "A lite method, to determine if the given event can be executed right now.
   Returns T iif the following hold true, and nil otherwise:
     1. The event isn't waiting on any executability flags (i.e., waiting on uncontrollable choice, activity end event, etc.)
     2. None of the predecessor events are still in events-remaining-to-execute
   "
  (with-slots (execution-context event-ht-preceders) ps
    (with-slots (event->executability events-remaining-to-execute) execution-context
      (let* ((executability-flags (gethash e event->executability)))
        (and (eql executability-flags nil)
             (eql nil (intersection events-remaining-to-execute
                                    (gethash e event-ht-preceders))))))))


(defgeneric environment-consistent-with-committed-choices? (ps env))
(defmethod environment-consistent-with-committed-choices? ((ps pike-session-lite) environment)
  "Helper function that returns T iif the given environment does not contain any choice values that
   directly contradict any choices already made, and nil otherwise."
  (with-slots (execution-context ss) ps
    (with-slots (assignment->commit-time) execution-context ()
      (let* ((assignments-committed (loop for a being the hash-keys in assignment->commit-time collecting a))
             (e-committed (make-environment-from-assignments ss assignments-committed)))
        (env-valid? ss (environment-union e-committed environment ss))))))


(defmethod prune-events-that-wont-be-executed ((ps pike-session-lite))
  "Prunes events that will never be executed - i.e., whose guard
   conditions will never possibly be activated. "
  (with-slots (execution-context) ps
    (with-slots (events-remaining-to-execute) execution-context
      (dolist (e events-remaining-to-execute)
        (unless (environment-consistent-with-committed-choices? ps (guard e))
          (prune-event ps e))))))




(defmethod commit-to-environment ((ps pike-session-lite) environment time &key (reason nil))
  (with-slots (ss) ps
    (let ((conjunction (environment->conjunction environment ss)))
      (dolist (assignment (conjunction-constraint-conjuncts conjunction))
        ;; The type will be an assignment-constraint, out of which we
        ;; can get just the assignment and commit to it!
        (commit-to-choice ps assignment time)))))


(defmethod commit-to-choice ((ps pike-session-lite) (assignment-constraint assignment-constraint) time &key (reason nil))
  ;; If we have not already committed to this choice, do it.
  (with-slots (assignment) assignment-constraint
    (unless (has-committed-to-choice? ps assignment)
      (with-slots (assignment->commit-time) (pike-execution-context ps)
        ;; Mark as committed
        (setf (gethash assignment assignment->commit-time) time)

        ;; Update the constraint knowledge base
        ;; (update-with-constraint! (pike-constraint-knowledge-base ps) assignment-constraint)

        ;; Update the executability flags if this was an uncontrollable choicesi
        (when (uncontrollable? (assignment-variable assignment))
          (update-executablity-from-uncontrollable-choice ps (assignment-variable assignment)))

        ;; Call hooks
        (pike-call-hooks ps :committed-to-choice assignment time)))))


(defmethod commit-to-choice-by-name ((ps pike-session-lite) (variable-name string) value &key (time nil))
  "Alows us to commit to a choice by the decision variable's name."
  (with-slots (ss) ps
    (let ((variable (find variable-name
                          (state-space-bits-variables ss)
                          :key #'decision-variable-name
                          :test #'equal)))
      (unless variable
        (error "Could not commit to ~a = ~a; decision variable \"~a\" couldn't be found!"
               variable-name
               value
               variable-name))
      (commit-to-choice ps
                        (make-instance 'assignment-constraint
                                       :assignment (assignment variable value))
                        (if time
                            time
                            (raw-time->execution-time ps (get-time ps)))))))


(defmethod process-externally-made-choices ((ps pike-session-lite))
  "Commit to any choices that have been made externally"
  (with-slots (choice-made-mailbox time) (pike-execution-context ps)
    (let (entry found?)

      ;; Implement a do-while loop: Keep doing this until there are no more entries to read
      (multiple-value-setq (entry found?) (safe-queue:mailbox-receive-message-no-hang choice-made-mailbox))
      (do ()
          ((not found?))
        (let ((variable-name (first entry))
              (variable-value (second entry)))
          (commit-to-choice-by-name ps variable-name variable-value))
        (multiple-value-setq (entry found?) (safe-queue:mailbox-receive-message-no-hang choice-made-mailbox))))))
