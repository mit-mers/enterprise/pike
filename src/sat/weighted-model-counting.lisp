;;;; Copyright (c) 2017 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
;;;;
(in-package #:pike)

(defclass weighted-model-counter ()
  ((bayesian-network
    :initarg :bayesian-network
    :accessor weighted-model-counter-bayesian-network)
   (sat
    :type sat-encoding
    :initarg :sat
    :accessor weighted-model-counter-sat)
   (weights
    :type hash-table
    :initarg :weights
    :accessor weighted-model-counter-weights)
   (d-dnnf
    :type d-dnnf
    :initarg :d-dnnf
    :accessor weighted-model-counter-d-dnnf)
   (all-variables
    :type list
    :initarg :all-variables
    :accessor weighted-model-counter-all-variables)
   (priority-variables
    :type list
    :initarg :priority-variables
    :accessor weighted-model-counter-priority-variables)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Main API
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric encode-bayesian-network-as-wmc (bn &key &allow-other-keys))
(defmethod encode-bayesian-network-as-wmc ((bn bayesian-network) &key (all-variables nil) (constraints nil) (name "pike-wmc"))
  "Encodes a Bayesian Network as a weighted model counting problem."
  ;; Get the definitive list of all variables to encode. The union of the BN variables and all-variables-to-encode
  (setf all-variables (concatenate 'list (bayesian-network-variables bn) all-variables))
  (remove-duplicates all-variables)
  ;; Encode the SAT variables, and the current constraints
  (let* ((sat (encode-constraints-as-sat-cnf constraints all-variables))
         weights
         d-dnnf
         priority-variables)

    ;; Optional: initial pass of unit propagation
    ;; (time (unit-propagation sat)) ;; Makes it slower? My unit propgation methods aren't optimized. Also the d-DNNF becomes bigger? Maybe heuristics don't work as well??

    ;; Now encode the probability distribution
    (setf weights (encode-bayesian-network-enc1 bn sat :encode-determinism t))

    ;; Add any additional constraints to the SAT problem.
    ;;(add-more-constraints-to-sat sat constraints)

    ;; Collect all of the priority variables that we will project to
    (maphash #'(lambda (var w)
                 (declare (ignore w))
                 (push var priority-variables))
             weights)

    (dolist (var (bayesian-network-variables bn))
      (dolist (val (decision-variable-values var))
        (push (retrieve-sat-variable-for-assignment sat (assignment var val)) priority-variables)))
    (setf priority-variables (remove-duplicates priority-variables))

    ;; Now that the SAT problem is fully defined, compile to d-DNNF to enable
    ;; fast weighted model counting.
    (setf d-dnnf (compile-sat-to-d-dnnf sat
                                        :priority-variables priority-variables
                                        :filename-prefix name))
    ;; Return a weighted model counter object
    (make-instance 'weighted-model-counter
                   :bayesian-network bn
                   :sat sat
                   :weights weights
                   :d-dnnf d-dnnf
                   :all-variables all-variables
                   :priority-variables priority-variables)))


(defgeneric compute-marginal-probability (o &key &allow-other-keys))
(defmethod compute-marginal-probability ((wmc weighted-model-counter) &key (assignments nil) (d-dnnf-initial nil))
  "Helper method to compute either P(assignments) or P(correct & assignments) depending on
if commit-to-x-correct is T or nil.
Assignments should be a list of assignments, and commit-to-x-correct should be T or nil.
The optional d-dnnf-initial will be used in place of the stored d-DNNF if desired -- useful
to save computation sometimes."
  (with-slots (d-dnnf weights sat priority-variables x-correct all-variables) wmc
    (let (d
          d-proj
          (p-assignments 1))
      ;; Check if everything in assignments are in the all-variables of the WMC
      (assert (subsetp (mapcar #'assignment-variable assignments) all-variables)
              nil "Some variables in these assignments are not modeled in the WMC! ~a" assignments)

      ;; Set d to be either the stored d-dnnf, or the desired initial, if specified.
      (setf d (if d-dnnf-initial
                  d-dnnf-initial
                  d-dnnf))
      ;; Condition on the given assignments
      (setf d (condition-d-dnnf d (loop for xi=vi in assignments collecting
                                       (make-instance 'sat-literal
                                                      :variable (retrieve-sat-variable-for-assignment sat xi=vi)
                                                      :positive t))))
      ;; Now, project onto the priority variables
      (setf d-proj (project-d-dnnf d priority-variables))
      ;; Now that we've projected, perform a weighted model count
      (setf p-assignments (weighted-model-count d-proj weights))
      ;; All done! Return the probabilty p-assignments, and also
      ;; the d-DNNF after conditioning it (but before projecting)
      (values p-assignments d))))



(defmethod compute-conditional-probability ((wmc weighted-model-counter) &key (assignments nil) (given nil))
  "Computes the probability P(correct & assignments | given), where correct is true iff
  all of the given constraints hold. Note that if no constraints were provided, this effectively
  equals P(assignments | given), which is useful for doing pure Bayesian inference.

  Performs this using Bayes Rule with two steps: P(correct & assignments & given) / P(given)"
  (let (d
        (p-given 1)
        (p-assignments-and-given 1))

    ;; First, compute P(given)
    (multiple-value-setq (p-given d)
      (compute-marginal-probability wmc
                                    :assignments given))

    ;; Edge case: if p-given is 0, then p-assignments-and-given will also be 0.
    ;; We'd end up with a division by zero error. Resolve this edge case by simply
    ;; returning 0.
    (when (= 0 p-given)
      (return-from compute-conditional-probability 0))

    ;; Next, compute P(correct & assignments & given). We can reuse the conditioning
    ;; on given done in the last step to save some computation.
    (multiple-value-setq (p-assignments-and-given d)
      (compute-marginal-probability wmc
                                    :d-dnnf-initial d
                                    :assignments assignments))

    ;; All done! Return the quotient.
    (/ p-assignments-and-given p-given)))



(defmethod condition-on-assignments ((wmc weighted-model-counter) &key (assignments nil))
  "Permanently condition the existing d-DNNF on the given assignments! Updates the
d-DNNF stored with the new (likely smaller) d-DNNF."
  (with-slots (d-dnnf sat all-variables) wmc
    ;; Check if everything in assignments are in the all-variables of the WMC
    (assert (subsetp (mapcar #'assignment-variable assignments) all-variables)
            nil "Some variables in these assignments are not modeled in the WMC!! ~a" assignments)
    (setf d-dnnf (condition-d-dnnf d-dnnf (loop for xi=vi in assignments collecting
                                               (make-instance 'sat-literal
                                                              :variable (retrieve-sat-variable-for-assignment sat xi=vi)
                                                              :positive t))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Helper functions
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun encode-bayesian-network-enc1 (bn sat &key (encode-determinism t))
  "This algorithm refers to ENC1 in (Chavira 2008). When encode-determinism is t,
  if any of the cpt entries are 0, a conflict is added preventing it from ever running.
   I think it's similar to do something for 1 too."
  (let ((weights (make-hash-table)))
    (dolist (cpt (conditional-probability-tables bn))
      ;; For each CPT, add new variables and the constraints theta <=>
      (do-cpt-assignments (var-assignment parents-assignments p) cpt
                          (cond
                            ;; If determinims is enabled and this entry is 0, essentially add a conflict
                            ((and (= 0 p)
                                  encode-determinism)
                             (add-more-constraints-to-sat sat
                                                          (list
                                                           (make-instance 'disjunction-constraint
                                                                          :disjuncts (loop for a in `(,var-assignment ,@parents-assignments)
                                                                                        collecting (make-instance 'negation-constraint
                                                                                                                  :expression
                                                                                                                  (make-instance 'assignment-constraint :assignment a)))))))

                            ;; Default case
                            (t
                             (let (x-conjunction x-theta)
                               ;; Create a new SAT variable <=> to the conjunction of variables.
                               (setf x-conjunction (tseitin-encoding sat (make-instance 'conjunction-constraint
                                                                                        :conjuncts (loop for a in `(,var-assignment ,@parents-assignments)
                                                                                                      collecting (make-instance 'assignment-constraint :assignment a)))))
                               ;; Create a new theta variable, whose literals will be weighted.
                               (setf x-theta (add-new-sat-variable sat
                                                                   :tag (format nil "θ_~a|~a" var-assignment (format nil "~{~a~^,~}" parents-assignments))))
                               ;; Add two clauses enforcing x-theta <=> x-conjunction
                               (add-clause sat `(,x-theta (:not ,x-conjunction)))
                               (add-clause sat `((:not ,x-theta) ,x-conjunction))
                               ;; Record a proper weight for theta equal to p
                               (setf (gethash x-theta weights) p))))))
    weights))


(defun compile-sat-to-d-dnnf (sat &key (priority-variables nil) (filename-prefix "pike-wmc"))
  "A helper function that compiles the SAT formulation to d-DNNF,
using the external tool DSharp. State of the art model counting
techniques view the d-DNNF as an arithmetic circuit, to perform
weighted model counting in time linear to the size of the
d-DNNF (which worst case could be exponential in the theory of course,
but in practice is usually much more reasonable)"
  (let ((cnf-filename (format nil "~a.cnf" filename-prefix))
        (nnf-filename (format nil "~a.nnf" filename-prefix))
        (varmap-filename (format nil "~a-var-map.txt" filename-prefix))
        d-dnnf
        cmd)
    ;; Write the SAT encoding to a file
    (write-dimacs-cnf-file sat cnf-filename)
    (write-dimacs-cnf-variable-mapping-file sat varmap-filename)
    ;; Delete any previous results, if they're lying around
    (when (probe-file nnf-filename)
      (delete-file nnf-filename))
    ;; Call d-sharp
    (setf cmd `("dsharp" "-smoothNNF"
                         "-Fnnf" ,nnf-filename
                         ,@(if priority-variables
                               `("-priority" ,(format nil "~{~a~^,~}" (mapcar #'id priority-variables)))
                               nil)
                         ,cnf-filename))

    (inferior-shell:run/nil cmd)
    ;; If no d-DNNF file was produced, then the theory is unsatisfiable. d-sharp skips file
    ;; output sometimes in this case.
    (unless (probe-file nnf-filename)
      (return-from compile-sat-to-d-dnnf (create-boolean-d-dnnf nil)))

    ;; Wonderful! Now, read in the d-DNNF file that was produced, cross referencing
    ;; to the appropriate SAT variables.
    (setf d-dnnf (read-d-dnnf-from-file nnf-filename :sat-encoding sat))
    ;; Perform a simplification pass and return
    (simplify-d-dnnf d-dnnf)))
