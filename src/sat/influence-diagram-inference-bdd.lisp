;;;; Copyright (c) 2018 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
;;;;
(in-package #:pike)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; This class implements a simple Bayesian network inference engine,
;;; that can quickly and easily compute marginal probabilities. It's
;;; a (probably faster) BDD version of the d-DNNF version in the other file.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass influence-diagram-inference-bdd (bayesian-network-inference-bdd)
  ((variable-order
    :type list
    :initform (error "Must specify a variable order!")
    :initarg :variable-order
    :documentation "A variable order of decisions / observations for the influence diagram.")
   (committed-choices
    :type list
    :initform nil
    :documentation "A list of choice variables that have been committed to.")
   (infdiag-dependencies
    :initform nil
    :documentation "A hash table mapping tuples of nodes to T iff there is a directed path."))
  (:documentation "Performs Bayesian inference on the influence diagram."))


(defmethod initialize-for-inference ((idi influence-diagram-inference-bdd) &key (extra-constraints nil) &allow-other-keys)
  "Double check the ordering"
  (with-slots (bn variable-order infdiag-dependencies) idi
    ;; (influence-diagram-verify-variable-order bn variable-order)
    (setf infdiag-dependencies (influence-diagram-compute-node-dependencies bn)))
  (call-next-method idi :extra-constraints extra-constraints))


(defmethod compute-marginal-probability-of-uncontrollable-choices ((idi influence-diagram-inference-bdd) assignments)
  "Computes the current marginal probability (conditioned on anything committed to already) of the given assignment.
NOTE: for influence diagrams, it is *required* that any preceding decision variables have already been committed to,
or are present in assignments!"
  (with-slots (node-bn bm bde variable-order committed-choices infdiag-dependencies) idi
    (let* ((assignment-variables (mapcar #'assignment-variable assignments))
           (future-decision-variables (set-difference (loop for vo in variable-order when (controllable? vo) collect vo)
                                                      assignment-variables))
           (node node-bn))

      ;; ;; Error check: ensure that for any assignments, all decision variables prior have been committed to.
      ;; (dolist (v assignment-variables)
      ;;   ;; Iterate over the variable order until we reach this variable
      ;;   (dolist (vo variable-order)
      ;;     (when (eql vo v)
      ;;       (return))
      ;;     ;; If this is a controllable variable, make sure we have an assignment for it
      ;;     (when (controllable? vo)
      ;;       (when (not (or (member vo committed-choices)
      ;;                      (member vo assignment-variables)))
      ;;         (error "Cannot evaluate influence diagram marginal probability for assigmnent ~a! Preceding decision variable ~a not committed to yet." (decision-variable-name v) (decision-variable-name vo))))))
                                        ;
      ;; ;; Pass those checks. Now, commit to any arbitrary value for all later decision variables that have yet to be observed, to "lock them down"
      ;; ;; and not artificially increase the WMC.
      ;; (dolist (a assignment-variables)
      ;;   (let ((variable-order-remaining (copy-seq variable-order)))

      ;;     ;; Pop off the beginning of the variable order before and including vo
      ;;     (iter (while (not (eql (pop variable-order-remaining) a))))

      ;;     ;; (format t "Variable order remaining for ~a: ~a~%" a variable-order-remaining)

      ;;     ;; Take the intersection (so that way we find the latest decision variables over all assignments...)
      ;;     (setf future-decision-variables (intersection future-decision-variables variable-order-remaining))))

      ;; ;; Remove any lock down variables that are in assignments
      ;; (setf future-decision-variables (loop for vo in future-decision-variables when (not (member vo committed-choices)) collect vo))

      ;; Error check: ensure that for any uncontrollable assignments, all decision variables with a direct edge into it
      ;; have been committed to are given here.
      ;;(break)
      (dolist (v-u assignment-variables)
        ;; Iterate over the variable order until we reach this variable
        (when (not (controllable? v-u))
          (let ((dependency-decisions (loop for v in variable-order when (and (controllable? v) (gethash `(,v ,v-u) infdiag-dependencies)) collect v)))
            (dolist (v-dep dependency-decisions)
              (cond
                ;; This dependency is met!
                ((or (member v-dep committed-choices)
                     (member v-dep assignment-variables))
                 (setf future-decision-variables (remove v-dep future-decision-variables)))
                ;; Dependency is not met!
                (t
                 (error "Cannot evaluate influence diagram marginal probability for assigmnent ~a! Preceding decision variable ~a not committed to yet." (decision-variable-name v-u) (decision-variable-name v-dep))))))))



      ;; For those future decision variables, lock down to an arbitrary value
      ;; (format t "Lock down: ~a~%" future-decision-variables)
      (dolist (vo future-decision-variables)
        (setf node (bdd-and bm
                            node
                            (constraint->bdd bde (make-instance 'assignment-constraint
                                                                :assignment (assignment vo (first (decision-variable-values vo))))))))

      ;; Now, and-in all of the given assignments.
      (dolist (a assignments)
        (setf node (bdd-and bm
                            node
                            (constraint->bdd bde (make-instance 'assignment-constraint :assignment a)))))

      ;; That's it -- return the WMC
      (let ((prob (bdd-wmc bm node)))
        prob))))


(defmethod compute-conditional-marginal-probability-of-uncontrollable-choice ((idi influence-diagram-inference-bdd) assignment)
  "Computes the current marginal probability (conditioned on anything committed to already) of the given assignment."
  (with-slots (node-bn bm bde) idi
    (let ((p-numerator (bdd-wmc bm
                                (bdd-and bm
                                         node-bn
                                         (constraint->bdd bde (make-instance 'assignment-constraint :assignment assignment)))))
          (p-denominator (bdd-wmc bm node-bn)))
      (/ p-numerator p-denominator))))



(defmethod commit-to-assignments ((idi influence-diagram-inference-bdd) &key (assignments nil))
  "Permanently commot to the given assignments."
  (with-slots (node-bn bm bde committed-choices) idi
    (dolist (a assignments)
      (push (assignment-variable a) committed-choices))
    (call-next-method)))
