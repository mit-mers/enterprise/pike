;;;; Copyright (c) 2018 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
;;;;
(in-package #:pike)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; This class implements a simple Bayesian network inference engine,
;;; that can quickly and easily compute marginal probabilities. It's
;;; a (probably faster) BDD version of the d-DNNF version in the other file.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass bayesian-network-inference-bdd ()
  ((bn
    :type bayesian-network
    :initform (error "Must provide a Bayesian network!")
    :accessor bayesian-network
    :initarg :bayesian-network
    :documentation "The Bayesian network to do inference over.")
   (ss
    :type state-space-bits
    :initform (error "Must provide a state space!")
    :initarg :ss
    :documentation "A state space")
   (bm
    :type bdd-manager
    :accessor get-bdd-manager
    :documentation "The BDD manager keeping track of the BDDs")
   (bde
    :type bdd-encoder
    :accessor get-bdd-encoder
    :documentation "The BDD variable encoder using the bm")
   (node-bn
    :type bdd-node
    :accessor node-bayesian-network
    :documentation "The top-level BDD node representing just the BN / WMC constraints holding."))
  (:documentation "Performs Bayesian inference"))


(defmethod initialize-for-inference ((bni bayesian-network-inference-bdd) &key (extra-constraints nil) &allow-other-keys)
  "Prepare for inference!"
  (with-slots (bn ss bm bde node-bn) bni
    (let (weights variables-bn constraints-bn theta->cpt)
      ;; Set up for the BDD
      (setf bm (create-bdd-manager))
      (setf bde (make-bdd-encoder :ss ss
                                  :bm bm))

      ;; Frame a weighted model counting problem via a BDD
      (multiple-value-setq (weights variables-bn constraints-bn theta->cpt) (encode-bayesian-network-enc1-for-bdd bn))

      ;; Prepare to solve
      (bdd-encoder-initialize bde
                              variables-bn
                              :use-binaries t
                              :variable-order-heuristic :default) ;; Hopefully default is good enough here! Could maybe use FORCE

      ;; Also store the weights
      (bdd-set-weights bm weights)

      ;;  Set the projection / sum product variables variables
      (let (assignments-bn
            bdd-variables)
        (dolist (v variables-bn)
          (dolist (val (decision-variable-values v))
            (push (assignment v val) assignments-bn)))
        (setf bdd-variables (remove-duplicates (mapcar #'first
                                                       (mapcar #'(lambda (a)
                                                                   (bdd-encoder-get-literal bde a))
                                                               assignments-bn))))

        ;; Set the sum product variables and projection variables to these (still needed for marginalization etc.)
        (bdd-set-projection-variables bm bdd-variables))

      ;; Encode as BDD
      (setf node-bn (bdd-encoder-get-variable-constraints-bdd bde))
      (dolist (c `(,@extra-constraints ,@constraints-bn))
        (setf node-bn (bdd-and bm
                               node-bn
                               (constraint->bdd bde c))))

      ;; Normally would project here, but we shouldn't need to actually -- since
      ;; we've only encoded projection variables essentially.
      t)))


(defmethod compute-marginal-probability-of-uncontrollable-choices ((bni bayesian-network-inference-bdd) assignments)
  "Computes the current marginal probability (conditioned on anything committed to already) of the given assignment."
  (with-slots (node-bn bm bde) bni
    (let ((prob (bdd-wmc bm
                         (bdd-and bm
                                  node-bn
                                  (constraint->bdd bde (make-instance 'conjunction-constraint
                                                                      :conjuncts (loop for a in assignments collecting
                                                                                      (make-instance 'assignment-constraint :assignment a))))))))
      prob)))


(defmethod compute-conditional-marginal-probability-of-uncontrollable-choice ((bni bayesian-network-inference-bdd) assignment)
  "Computes the current marginal probability (conditioned on anything committed to already) of the given assignment."
  (with-slots (node-bn bm bde) bni
    (let ((p-numerator (bdd-wmc bm
                                (bdd-and bm
                                         node-bn
                                         (constraint->bdd bde (make-instance 'assignment-constraint :assignment assignment)))))
          (p-denominator (bdd-wmc bm node-bn)))
      (/ p-numerator p-denominator))))



(defmethod commit-to-assignments ((bni bayesian-network-inference-bdd) &key (assignments nil))
  "Permanently commot to the given assignments."
  (with-slots (node-bn bm bde) bni
    (setf node-bn (bdd-and bm
                           node-bn
                           (constraint->bdd bde (make-instance 'conjunction-constraint
                                                               :conjuncts (loop for a in assignments
                                                                             collecting (make-instance 'assignment-constraint :assignment a))))))))
