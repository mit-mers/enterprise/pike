;;;; Copyright (c) 2017 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
;;;;
;;;; Thanks to Christian Muise for work on the krtoolkit -- much of the outline
;;;; of this code takes inspiration from that package.
(in-package #:pike)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; d-DNNF Class Definitions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass d-dnnf ()
  ((root
    :initarg :root
    :initform (error "Must specify root node!")
    :accessor root)))

(defmethod print-object ((d-dnnf d-dnnf) s)
  ;; (let ((size (size-of-d-dnnf d-dnnf)))
  ;;   (format s "#<D-DNNF with ~a node~p>" size size))
  (format s "#<D-DNNF>"))

(defclass nnf-node ()
  ((children
    :type list
    :initform nil
    :initarg :children
    :accessor children)
   (weighted-count
    :type real
    :initform 0
    :accessor weighted-count
    :documentation "A slot to hold the weighted count
of this node, when computed.")
   (replacement
    :initform nil
    :documentation "A slot to hold a cached version of
the substituted replacement for certain operations.")
   (hit?
    :initform nil
    :accessor hit?
    :documentation "A flag indicating whether computation has hit
this node or not. Useful for preventing redundant computation by
not repeating nodes already traversed down the d-DNNF.")))

(defclass and-nnf-node (nnf-node) ())
(defclass or-nnf-node (nnf-node) ())
(defclass constant-nnf-node (nnf-node)
  ((constant
    :initform t
    :initarg :constant
    :accessor constant-node-constant)))
(defclass literal-nnf-node (nnf-node)
  ((literal
    :type sat-literal
    :initarg :literal
    :initform (error "Must specify literal!")
    :accessor literal-node-literal)))

(defmethod print-object ((ln literal-nnf-node) s)
  (format s "#<LITERAL-NODE ~a>" (literal-node-literal ln)))

(defmethod print-object ((cn constant-nnf-node) s)
  (format s "#<CONSTANT-NODE ~a>" (if (constant-node-constant cn) "true" "false")))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; d-DNNF queries
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Weighted model counting
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric weighted-model-count (obj weights))
(defmethod weighted-model-count ((d-dnnf d-dnnf) weights)
  "Perform weighted model counting on this d-DNNF. Weights
is a hashtable mapping variables to their real weights. Any
variable, as well as all negative literals, not in weights are
assumed to have a weight of 1.0. We could in theory easily
support weights on negative literals, but they're not needed
here and so we don't go through the extra hashing effort.

Precondition: the d-DNNF is simplified (i.e., no true or false constant
nodes in it, except for a single one possibly as the root) "
  (declare (optimize (speed 3)))
  (reset-d-dnnf d-dnnf)
  (cond
    ;; If the root node is false, return 0
    ((typep (root d-dnnf) 'constant-nnf-node)
     (if (constant-node-constant (root d-dnnf))
         (error "Can't compute weighted model count of true, not implemented!")
         0))
    ;; Otherwise, if a non constant root node, traverse the tree.
    ;; Assume it is simplified first, so no other trues / falses
    ;; should appear down the tree.
    (t
     (weighted-model-count (root d-dnnf) weights))))

(defmethod weighted-model-count ((node and-nnf-node) weights)
  "And node: multiply the weighted count of children"
  (declare (optimize (speed 3)))
  (with-slots (weighted-count hit?) node
    ;; If we've already computed this node, don't recurse again;
    ;; use the cached value
    (when hit?
      (return-from weighted-model-count weighted-count))
    ;; Otherwise, compute recursively
    (setf hit? t)
    (setf weighted-count 1)
    (dolist (c (children node))
      (setf weighted-count (* weighted-count (weighted-model-count c weights)))
      ;; Early termination condition: if 0, will always be 0
      (when (= weighted-count 0)
        (return-from weighted-model-count weighted-count)))
    weighted-count))


(defmethod weighted-model-count ((node or-nnf-node) weights)
  "Or node: add the weighted counts of all children"
  (declare (optimize (speed 3)))
  (with-slots (weighted-count hit?) node
    ;; If we've already computed this node, don't recurse again;
    ;; use the cached value
    (when hit?
      (return-from weighted-model-count weighted-count))
    ;; Otherwise, compute recursively
    (setf hit? t)
    (setf weighted-count 0)
    (dolist (c (children node))
      (incf weighted-count (weighted-model-count c weights)))
    weighted-count))

(defmethod weighted-model-count ((lit-node literal-nnf-node) weights)
  "Base case: return the weight if found in weights, else 1.0."
  (declare (optimize (speed 3)))
  (with-slots (weighted-count hit?) lit-node
    ;; If we've already computed this node, don't recurse again;
    ;; use the cached value
    (when hit?
      (return-from weighted-model-count weighted-count))
    ;; Otherwise, compute recursively
    (setf hit? t)
    (let (w found?)
      (multiple-value-setq (w found?) (gethash (sat-literal-variable (literal-node-literal lit-node)) weights))
      (setf weighted-count (if (and found?
                                    (positive? (literal-node-literal lit-node)))
                               w
                               1)))
    weighted-count))

(defmethod weighted-model-count ((constant-node constant-nnf-node) weights)
  "Base case: return the weight if found in weights, else 1.0."
  (error "Getting the weighted model count of a constant node isn't supported yet! Simplify the d-DNNF first."))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Measure the size of the d-DNNF
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric size-of-d-dnnf (obj))
(defmethod size-of-d-dnnf ((d-dnnf d-dnnf))
  "Measure the sizeof the d-dnnf, as defined
as the number of nodes."
  (declare (optimize (speed 3)))
  (reset-d-dnnf d-dnnf)
  ;;(format t "Beginning size computation~%")
  (size-of-d-dnnf (root d-dnnf)))


(defmethod size-of-d-dnnf ((node nnf-node))
  (declare (optimize (speed 3)))
  (with-slots (hit?) node
    ;; If we've already hit this node, don't double count anything underneath.
    (when hit?
      ;;(format t "Size: ~a already hit, returning 0~%" node)
      (return-from size-of-d-dnnf 0))
    (setf hit? t)
    ;;(format t "Size: ~a is new~%" node)
    (+ 1
       (apply #'+ (mapcar #'size-of-d-dnnf (children node))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Simplify d-DNNF
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric simplify-d-dnnf (obj))
(defmethod simplify-d-dnnf ((d-dnnf d-dnnf))
  (declare (optimize (speed 3)))
  (reset-d-dnnf d-dnnf)
  (make-instance 'd-dnnf :root (simplify-d-dnnf (root d-dnnf))))


(defmethod simplify-d-dnnf ((node nnf-node))
  "Simplify this node by propagating any true's / false's etc."
  ;; Have we already simplified this node and determined a suitable replacement?
  ;; If so, don't re-simplify; return the cached computation.
  (declare (optimize (speed 3)))
  (with-slots (hit? replacement) node
    (when hit?
      (return-from simplify-d-dnnf replacement))
    ;; Not a hit -- compute and cache for next time
    (setf hit? t)
    (let ((children-new (mapcar #'simplify-d-dnnf (children node))))
      (setf replacement (compress node children-new)))
    replacement))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Conditioning d-DNNF on some literals
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric condition-d-dnnf (obj literals))
(defmethod condition-d-dnnf ((d-dnnf d-dnnf) literals)
  (declare (optimize (speed 3)))
  (reset-d-dnnf d-dnnf)
  (make-instance 'd-dnnf :root (condition-d-dnnf (root d-dnnf) literals)))


(defmethod condition-d-dnnf ((node nnf-node) literals)
  "Set any literals in this sub graph to true
as appropriate"
  (declare (optimize (speed 3)))
  (with-slots (hit? replacement) node
    (when hit?
      (return-from condition-d-dnnf replacement))
    ;; Not a hit -- compute and cache for next time
    (setf hit? t)
    (let ((children-new (mapcar #'(lambda (n) (condition-d-dnnf n literals)) (children node))))
      (setf replacement (compress node children-new)))
    replacement))


(defmethod condition-d-dnnf ((ln literal-nnf-node) literals)
  "Base case in the recursion: return True if the literal is
in literals, False if it's negation is in literals, and a copy
unchanged otherwise."
  (declare (optimize (speed 3)))
  (with-slots (hit? replacement literal) ln
    (when hit?
      (return-from condition-d-dnnf replacement))
    ;; Not a hit -- compute and cache for next time
    (setf hit? t)
    (cond ((member literal literals :test 'literals-equal?)
           (setf replacement (make-instance 'constant-nnf-node :constant t)))

          ((member (negate-literal literal) literals :test 'literals-equal?)
           (setf replacement (make-instance 'constant-nnf-node :constant nil)))

          (t
           (setf replacement (make-instance 'literal-nnf-node :literal literal))))
    replacement))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Project this d-DNNF by removing any reference to
;; variables not in this projection list.
;;
;; Please note that, depending on the d-DNNF structure,
;; the result may no longer be d-DNNF! Specifically, it
;; may lose determinism if you're not careful. However,
;; if you ensure that all of the projected variables
;; appear first ("priority variables" in DSharp), then
;; you still do retain the determinism property and have
;; d-DNNF.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric project-d-dnnf (obj variables))
(defmethod project-d-dnnf ((d-dnnf d-dnnf) variables)
  (declare (optimize (speed 3)))
  (reset-d-dnnf d-dnnf)
  (make-instance 'd-dnnf :root (project-d-dnnf (root d-dnnf) variables)))


(defmethod project-d-dnnf ((node nnf-node) variables)
  "Set any literals mentioning a variable in this sub graph to true
as appropriate"
  (declare (optimize (speed 3)))
  (with-slots (hit? replacement) node
    (when hit?
      (return-from project-d-dnnf replacement))
    ;; Not a hit -- compute and cache for next time
    (setf hit? t)
    (let ((children-new (mapcar #'(lambda (n) (project-d-dnnf n variables)) (children node))))
      (setf replacement (compress node children-new)))
    replacement))


(defmethod project-d-dnnf ((ln literal-nnf-node) variables)
  "Base case in the recursion: return True if the literal is
in literals, False if it's negation is in literals, and a copy
unchanged otherwise."
  (declare (optimize (speed 3)))
  (with-slots (hit? replacement literal) ln
    (when hit?
      (return-from project-d-dnnf replacement))
    ;; Not a hit -- compute and cache for next time
    (setf hit? t)
    (cond
      ;; Regardless of whether this literal is positive or negative,
      ;; project it away by setting it to true if it does not mention
      ;; one of our projection variables.
      ((not (member (sat-literal-variable literal) variables))
       (setf replacement (make-instance 'constant-nnf-node :constant t)))
      ;; Otherwise, return a copy of this literal.
      (t
       (setf replacement (make-instance 'literal-nnf-node :literal literal))))
    replacement))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Simplify this d-DNNF by removing true / false
;; nodes up the tree, using caching as appropriate
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defgeneric compress (nnf-node children-new))
(defmethod compress ((node and-nnf-node) children-new)
  "Return a new and node that is locally
simplified"
  (declare (optimize (speed 3)))
  (let (children-compressed)
    (dolist (c children-new)
      ;; Process constant nodes specially
      (cond ((typep c 'constant-nnf-node)
             ;; If it's true, don't add it to children compressed.
             ;; If it's false, return a False node
             (unless (constant-node-constant c)
               (return-from compress (make-instance 'constant-nnf-node :constant nil))))
            ;; If it's not a constant, add it.
            (t
             (push c children-compressed))))
    ;; If there are no nodes left, this is a false node
    (when (= 0 (length children-compressed))
      (return-from compress (make-instance 'constant-nnf-node :constant t)))
    ;; If there's just a single child, return it (unpack the and)
    (when (= 1 (length children-compressed))
      (return-from compress (first children-compressed)))
    ;; Otherwise, return a new and node with the compressed list of children
    (make-instance 'and-nnf-node :children children-compressed)))


(defmethod compress ((node or-nnf-node) children-new)
  "Return a new and node that is locally
simplified"
  (declare (optimize (speed 3)))
  (let (children-compressed)
    (dolist (c children-new)
      ;; Process constant nodes specially
      (cond ((typep c 'constant-nnf-node)
             ;; If it's false, don't add it to children compressed.
             ;; If it's true, return a true node
             (when (constant-node-constant c)
               (return-from compress (make-instance 'constant-nnf-node :constant t))))
            ;; If it's not a constant, add it.
            (t
             (push c children-compressed))))
    ;; If there are no nodes left, this is a false node
    (when (= 0 (length children-compressed))
      (return-from compress (make-instance 'constant-nnf-node :constant nil)))
    ;; If there's just a single child, return it (unpack the or)
    (when (= 1 (length children-compressed))
      (return-from compress (first children-compressed)))
    ;; Otherwise, return a new or node with the compressed list of children
    (make-instance 'or-nnf-node :children children-compressed)))




(defmethod compress ((node literal-nnf-node) children-new)
  "No compression possible for literals -- return as is"
  (declare (optimize (speed 3)))
  (make-instance 'literal-nnf-node :literal (literal-node-literal node)))

(defmethod compress ((node constant-nnf-node) children-new)
  "No compression for constants -- return as is"
  (declare (optimize (speed 3)))
  (make-instance 'constant-nnf-node :constant (constant-node-constant node)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Resetting the recursive computations between runs
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric reset-d-dnnf (obj))
(defmethod reset-d-dnnf ((d-dnnf d-dnnf))
  "Traverse the d-DNNF, and if anything has been
hit, unmark and traverse children. This allows us
to use a cacheing scheme to not re-compute things
redundantly."
  (declare (optimize (speed 3)))
  (reset-d-dnnf (root d-dnnf)))

(defmethod reset-d-dnnf ((node nnf-node))
  "If this node has been hit, reset and
  travel down to children."
  (declare (optimize (speed 3)))
  (with-slots (hit? weighted-count replacement) node
    (when hit?
      ;;(format t "Reset: Clearing ~a~%" node)
      (setf hit? nil)
      (setf weighted-count 0)
      (setf replacement nil)
      (dolist (c (children node))
        (reset-d-dnnf c)))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Input / output functions for d-DNNF
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun read-d-dnnf-from-file (filename &key (sat-encoding nil))
  "This file reads a .nnf file that represents a
d-DNNF DAG. For details on this file format, please see
the documentation from the c2d compiler, or also this
webpage:
http://www.cril.univ-artois.fr/KC/d-DNNF-reasoner.html

This file also takes in an optional parameter, sat-encoding,
which is a datastructure that keeps track of literals and
variable ID's. If provided, then literals in the d-DNNF
datastructure producted are eql to the variables in the
sat-encoding. Otherwise, new variables are created."
  (let (lines)
    ;; First, read the file line-by-line
    (with-open-file (f filename :direction :input)
      (let (line)
        (loop while (not (eql line :eof)) do
             (setf line (read-line f nil :eof))
             (when (not (eql line :eof))
               (push line lines)))))
    (setf lines (reverse lines))

    ;; Now, process each line -- starting with the header
    (let (n-nodes n-edges n-vars line terms nodes-topological-ordering sat-variables)
      ;; Parse the header line
      (setf line (pop lines))
      (setf terms (cl-ppcre:split "[\\s]+" (string-trim " " line)))
      (assert (equal "nnf" (nth 0 terms)))
      (assert (= 4 (length terms)))
      (setf n-nodes (parse-integer (nth 1 terms)))
      (setf n-edges (parse-integer (nth 2 terms)))
      (setf n-vars (parse-integer (nth 3 terms)))
      ;; Check that the number of remaining lines in the file matches the number of nodes
      (assert (= n-nodes (length lines)))

      ;; Cool! Now that we've processed the header line, parse each line specifically.
      ;; First set up an array mapping 0-based indicies to nodes. This is the order
      ;; of nodes in the d-DNNF file, and the file format also requires that this is a
      ;; topological ordering over nodes.
      (setf nodes-topological-ordering (make-array n-nodes))
      ;; Similarly create a list of all variables, or cross reference to the sat-encoding
      (cond ((not sat-encoding)
             (setf sat-variables (make-array (1+ n-vars)))
             (dotimes (j n-vars)
               (setf (aref sat-variables (1+ j))
                     (make-instance 'sat-variable
                                    :id (1+ j)))))
            ;; If we were provided with a sat encoding, cross reference with that one.
            (t
             (setf sat-variables (copy-seq (sat-encoding-id->sat-variable sat-encoding)))
             (assert (= (1+ n-vars) (length sat-variables)))))

      ;; Great! Now we're ready to iterate over each line.
      (dotimes (i n-nodes)
        (setf line (pop lines))
        (setf terms (cl-ppcre:split "[\\s]+" (string-trim " " line)))
        ;; Do different things based on which kind of node we have.
        (let ((node-type (pop terms)))
          (cond ((equal "L" node-type)
                 (assert (= 1 (length terms)))
                 (let* ((literal-int (parse-integer (pop terms)))
                        (variable-index (abs literal-int))
                        (positive? (> literal-int 0)))
                   (setf (aref nodes-topological-ordering i)
                         (make-instance 'literal-nnf-node
                                        :literal (make-instance 'sat-literal
                                                                :variable (aref sat-variables variable-index)
                                                                :positive positive?)))))

                ((equal "A" node-type)
                 (let ((n-children (parse-integer (pop terms))))
                   (assert (= n-children (length terms)))
                   ;; If there are no children, then this is really a "True" node.
                   (cond ((= 0 n-children)
                          (setf (aref nodes-topological-ordering i)
                                (make-instance 'constant-nnf-node :constant t)))

                         ;; Otherwise, it's not a true node, but a normal and node.
                         (t
                          (let ((children-indices (mapcar #'parse-integer terms)))
                            (setf (aref nodes-topological-ordering i)
                                  (make-instance 'and-nnf-node
                                                 :children (mapcar #'(lambda (j)
                                                                       (aref nodes-topological-ordering j))
                                                                   children-indices))))))))

                ((equal "O" node-type)
                 (let ((split-var-index (parse-integer (pop terms)))
                       (n-children (parse-integer (pop terms))))
                   (declare (ignore split-var-index))
                   (assert (= n-children (length terms)))
                   ;; If there are no children, then this is really a "False" node.
                   (cond ((= 0 n-children)
                          (setf (aref nodes-topological-ordering i)
                                (make-instance 'constant-nnf-node :constant nil)))

                         ;; Otherwise, it's not a false node, but a normal or node.
                         (t
                          (let ((children-indices (mapcar #'parse-integer terms)))
                            (setf (aref nodes-topological-ordering i)
                                  (make-instance 'or-nnf-node
                                                 :children (mapcar #'(lambda (j)
                                                                       (aref nodes-topological-ordering j))
                                                                   children-indices))))))))

                (t
                 (error "Malformed .nnf file! Unknown node type ~a" node-type)))))

      ;; All done constructing the d-DNNF! Return the final datastructure.
      (make-instance 'd-dnnf
                     :root (aref nodes-topological-ordering (1- n-nodes))))))


(defun create-boolean-d-dnnf (constant)
  "Create a d-DNNF that is either true or false."
  (make-instance 'd-dnnf
                 :root (make-instance 'constant-nnf-node
                                      :constant constant)))
