;;;; Copyright (c) 2017 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
;;;;
(in-package #:pike)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; This class implements a simple Bayesian network inference engine,
;;; that can quickly and easily compute marginal probabilities. It
;;; employs cacheing / memoization, so that it doesn't have to compute
;;; the same thing twice.
;;;
;;; This class is basically a simple wrapper around the weighted model
;;; counter, which is where all of the heavy lifting actually happens!
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass bayesian-network-inference ()
  ((bn
    :type bayesian-network
    :initform (error "Must provide a Bayesian network!")
    :accessor bayesian-network
    :initarg :bayesian-network
    :documentation "The Bayesian network to do inference over.")
   (ss
    :type state-space-bits
    :initform (error "Must provide a state space!")
    :initarg :ss
    :documentation "A state space")
   (wmc
    :documentation "A WMC instance allowing for inference.")
   (env->conditional-prob
    :initform (make-hash-table :test #'equal)
    :documentation "A table cacheing solutions to
conditional probability results."))
  (:documentation "A wrapper around the weighted model counter,
for doing Bayesian inference."))


(defmethod initialize-for-inference ((bni bayesian-network-inference) &key (name "pike-bn-wmc"))
  "Prepare for inference!"
  (with-slots (wmc bn) bni
    ;; Encode a weighted model counting problem of the bayesian network
    (setf wmc (encode-bayesian-network-as-wmc bn
                                              :constraints nil
                                              :name name))))


(defmethod compute-marginal-probability ((bni bayesian-network-inference) &key (env nil) &allow-other-keys)
  (with-slots (env->conditional-prob ss wmc) bni
    ;; Check the cache: have we seen this environment before?
    (let (prob found?)
      (multiple-value-setq (prob found?) (gethash env env->conditional-prob))
      (when found?
        ;; Cache hit! Return it
        (return-from compute-marginal-probability prob))
      ;; If we get here, no cache hit. We must use the WMC.
      (let ((assignments (get-assignments-from-environment ss env))
            prob)
        ;; Compute it
        (setf prob (compute-marginal-probability wmc :assignments assignments))
        ;; Store the value in the cache for next time
        (setf (gethash env env->conditional-prob) prob)))))


(defmethod condition-on-assignments ((bni bayesian-network-inference) &key (assignments nil))
  "Permanently condition the existing d-DNNF on the given assignments! Updates the
d-DNNF stored with the new (likely smaller) d-DNNF."
  (with-slots (wmc) bni
    (condition-on-assignments wmc :assignments assignments)
    (clear-cache bni)))


(defmethod clear-cache ((bni bayesian-network-inference))
  "Helper function to clear the cache (for instance, if our
probability distribution changes)"
  (with-slots (env->conditional-prob) bni
    (clrhash env->conditional-prob)))
