;;;; Copyright (c) 2014-2017 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Various classes to represent SAT problems.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass sat-variable ()
  ((id
    :initform (error "A unique ID must be assigned")
    :initarg :id
    :accessor id
    :documentation "A unique integer ID")
   (tag
    :initform nil
    :initarg :tag
    :accessor sat-variable-tag
    :documentation "A tag for this variable, such as a constraint, string, etc."))
  (:documentation "Represents a SAT variable."))

(defmethod print-object ((sv sat-variable) s)
  "Pretty-prints the sat variable"
  (cond ((or (typep (sat-variable-tag sv) 'assignment-constraint)
             (typep (sat-variable-tag sv) 'string))
         (format s "x_{~a}" (sat-variable-tag sv)))

        (t
         (format s "x_{~a}" (id sv)))))


(defclass sat-literal ()
  ((variable
    :initform (error "Must specify variable")
    :initarg :variable
    :accessor sat-literal-variable
    :documentation "The variable associated with this literal")
   (positive?
    :initform T
    :initarg :positive
    :accessor positive?
    :documentation "Either T or nil, if this literal is positive or negative")))

(defmethod print-object ((sl sat-literal) stream)
  "Pretty-prints the sat variable"
  (when (not (positive? sl))
    (format stream "¬"))
  (format stream "~a" (sat-literal-variable sl)))

(defun literals-equal? (l1 l2)
  "Return T if these literals represent the same variable (by eql); nil otherwise"
  (and (typep l1 'sat-literal)
       (typep l2 'sat-literal)
       (eql (sat-literal-variable l1) (sat-literal-variable l2))
       (eql (positive? l1) (positive? l2))))

(defun negate-literal (l)
  (make-instance 'sat-literal
                 :variable (sat-literal-variable l)
                 :positive (not (positive? l))))
