;;;; Copyright (c) 2014-2017 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; This file contains method and classes to convert
;; our (finite-domain variable) constraint representaiton
;; and convert it to a CNF representation suitable
;; for use by external SAT solvers.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass sat-encoding ()
  ((id->sat-variable
    :initform (make-array 10 :initial-element nil :adjustable T :fill-pointer 1)
    :initarg :id->sat-variable
    :accessor sat-encoding-id->sat-variable
    :documentation "An in-order array of SAT-related variables. The ID of the variable is its index; starts at 1 (cf of dimacs format)")
   (constraint->sat-variable
    :initform nil
    :initarg :constraint->sat-variable
    :accessor sat-encoding-constraint->sat-variable
    :documentation "A mapping from constraint objects to their representative variables
they represent.")
   (clauses
    :initform nil
    :initarg :clauses
    :accessor clauses
    :documentation "A list of clauses. Each clause is itself a list, of sat-literal's."))
  (:documentation "Represents a SAT formulation of the constraints."))

(defun copy-sat-encoding (sat)
  "Helper function to make a (shallow) copy of a SAT encoding, where it is
   safe to add additional constraints without affecting the original
   SAT formulation."
  (let ((constraint->sat-variable (genhash:make-generic-hash-table :test 'pike-constraints-equal
                                                                   :size (genhash:generic-hash-table-size (sat-encoding-constraint->sat-variable sat)))))
    ;; Copy each hashed constraint to the new hash table.
    (genhash:hashmap #'(lambda (key val)
                         (setf (genhash:hashref key constraint->sat-variable) val))
                     (sat-encoding-constraint->sat-variable sat))
    ;; Now return a copy.
    (make-instance 'sat-encoding
                   :id->sat-variable (make-array (length (sat-encoding-id->sat-variable sat))
                                                 :initial-contents (sat-encoding-id->sat-variable sat)
                                                 :adjustable T
                                                 :fill-pointer (length (sat-encoding-id->sat-variable sat)))
                   :constraint->sat-variable constraint->sat-variable
                   :clauses (copy-seq (clauses sat)))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Main API
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defmethod encode-constraints-as-sat-cnf (constraints all-variables &key (likely-number-of-constraints nil))
  "Given a list of constraints and a state space, generate a
SAT encoding in CNF of the constraints. Return the sat encoding object.
Uses the popular Tseitin transformation, which introduces new intermediate
variables, but doesn't blow up when applying De Morgan's law. Each solution"
  ;; Create a new SAT encoding object. As part of it, set up a special
  ;; hash table that uses our customized hashing / equality checking
  ;; for constraint objects.
  (let ((sat (make-instance 'sat-encoding
                            :constraint->sat-variable
                            (genhash:make-generic-hash-table :test 'pike-constraints-equal
                                                             :size (+ 17 (* 5 (if likely-number-of-constraints
                                                                                  likely-number-of-constraints
                                                                                  (length constraints)))))))
        constraints-mutex)

    ;; Create variable mutex and selection constraints, to enforce the propositional state logi
    ;; (can't chose x=1 and x=2 for instance)
    (dolist (variable all-variables)
      ;; Add constraint that exactly one value must be chosen
      (push (make-constraint `(or ,@(loop for vi in (decision-variable-values variable) collecting `(= ,variable ,vi))))
            constraints-mutex)
      ;; Add pairwise mutex constraints between each variable assignment
      (do-pairs (v1 v2 (decision-variable-values variable))
        (push (make-constraint `(or (not (= ,variable ,v1))
                                    (not (= ,variable ,v2))))
              constraints-mutex)))

    ;; Associate all variable values with the first indices. This isn't strictly necessary, but makes
    ;; debugging the output CNF easier!
    (dolist (variable (reverse all-variables))
      (dolist (v (decision-variable-values variable))
        (get-sat-variable sat (make-instance 'assignment-constraint :assignment (assignment variable v)))))

    ;; Encode each of the constraints using the Tseitin transformation
    (add-more-constraints-to-sat sat (concatenate 'list constraints constraints-mutex))

    sat))


(defmethod add-more-constraints-to-sat ((sat sat-encoding) constraints)
  "Helper function to add additional constraints to the SAT encoding!
   Doesn't re-encode or modify any previous constraints, just adds
   new additional constraints to the problem. Will make use of previously
   found caching / memoization, variables, etc. Will modify the SAT problem."
  (dolist (constraint constraints)
    (let ((x_overall (tseitin-encoding sat constraint)))
      ;; By this point, additional clauses have been added to the SAT theory
      ;; to guarantee that x_overall <=> constraint. Will have made use of any
      ;; previous caching. We must now require that this x_overall holds, so add
      ;; a unitary clause for this.
      (add-clause sat `(,x_overall))))
  t)


(defmethod write-dimacs-cnf-file (sat filename)
  (with-open-file (f filename :direction :output :if-exists :supersede :if-does-not-exist :create)
    (write-dimacs-cnf-to-stream sat f)))

(defmethod write-dimacs-cnf-to-stream (sat stream)
  (with-slots (id->sat-variable clauses) sat
    (format stream "c  This CNF auto-generated from Pike.~%c~%")
    (format stream "p cnf ~a ~a~%"
            (- (length id->sat-variable) 1)
            (length clauses))
    ;; Now, write each clause
    (dolist (clause clauses)
      (dolist (sl clause)
        (if (not (positive? sl))
            (format stream "-"))
        (format stream "~a " (id (sat-literal-variable sl))))
      ;; Terminate the clause with a 0
      (format stream "0~%"))))

(defmethod write-dimacs-cnf-variable-mapping-file (sat filename)
  (with-open-file (f filename :direction :output :if-exists :supersede :if-does-not-exist :create)
    (write-dimacs-cnf-variable-mapping-to-stream sat f)))

(defmethod write-dimacs-cnf-variable-mapping-to-stream (sat stream)
  "Helper function that outputs a key file, mapping integer SAT variable ID's in the
CNF to the constraints they represents."
  (with-slots (id->sat-variable) sat
    (loop for i from 1 to (- (length id->sat-variable) 1) do
         (format stream "~a: ~a~%" i (sat-variable-tag (aref id->sat-variable i))))))





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Internal methods
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defmethod tseitin-encoding (sat constraint)
  "Implements the Tseitin encoding. Adds new sat variables and clauses
to sat, and returns the new sat variable corresponding to the constraint --
i.e., the one that is true iff the constraint is true"
  ;; Get (or re-use) a SAT variable to represent this constraint. It will
  ;; be true iif the constraint is true.
  (let (x_constraint already-exists?)
    (multiple-value-setq (x_constraint already-exists?) (get-sat-variable sat constraint))
    ;; If the variable already exists, we don't need to encode any additional
    ;; constraints, as they already have been encoded. Simply return the variable
    ;; for reuse. This is an optimization that avoids duplicate clauses in the
    ;; final encoding.
    (when already-exists?
      (return-from tseitin-encoding x_constraint))

    ;; If we get here, then this is a new constraint, that has not been
    ;; encoded before. Do different things depending on the constraint type:
    (cond
      ;; Assignment: No further clauses are needed!
      ((typep constraint 'assignment-constraint))

      ;; Constant: Depending on the positivity of the constant,
      ;; add a new clause that will force x_constraint
      ;; to be either True or False.
      ((typep constraint 'constant-constraint)
       (if (constant-constraint-constant constraint)
           ;; Positive:
           (add-clause sat `(,x_constraint))
           ;; Negative:
           (add-clause sat `((:not ,x_constraint)))))

      ;; Negation: Apply the recursively, then add
      ;; additional clauses
      ((typep constraint 'negation-constraint)
       ;; Recursively add clauses reprsesenting the subexpression
       (let ((x_exp (tseitin-encoding sat (negation-constraint-expression constraint))))
         ;; Additionally add clauses enforcing the negation
         (add-clause sat `(,x_constraint ,x_exp))
         (add-clause sat `((:not ,x_constraint) (:not ,x_exp)))))


      ;; Conjunction: Apply the recursively, then add
      ;; additional clauses
      ((typep constraint 'conjunction-constraint)
       ;; Apply the Tseitin transform recursively to all sub expressions
       (let ((x_is (mapcar #'(lambda (ci)
                               (tseitin-encoding sat ci))
                           (conjunction-constraint-conjuncts constraint))))
         ;; Now, add additional clauses
         (add-clause sat (concatenate 'list
                                      `(,x_constraint)
                                      (loop for x_i in x_is collecting `(:not ,x_i))))
         (dolist (x_i x_is)
           (add-clause sat `((:not ,x_constraint) ,x_i)))))


      ;; Disjunction: Apply the recursively, then add
      ;; additional clauses
      ((typep constraint 'disjunction-constraint)
       ;; Apply the Tseitin transform recursively to all sub expressions
       (let ((x_is (mapcar #'(lambda (di)
                               (tseitin-encoding sat di))
                           (disjunction-constraint-disjuncts constraint))))
         ;; Now, add additional clauses
         (add-clause sat (concatenate 'list `((:not ,x_constraint)) x_is))
         (dolist (x_i x_is)
           (add-clause sat `((:not ,x_i) ,x_constraint)))))


      ;; Implication: Apply the recursively, then add
      ;; additional clauses
      ((typep constraint 'implication-constraint)
       ;; Apply the tseitin transform to the implicant and consequent parts
       (let ((x_A (tseitin-encoding sat (implication-constraint-implicant constraint)))
             (x_B (tseitin-encoding sat (implication-constraint-consequent constraint))))
         ;; Add additional clauses
         (add-clause sat `((:not ,x_constraint) (:not ,x_A) ,x_B))
         (add-clause sat `(,x_A ,x_constraint))
         (add-clause sat `((:not ,x_B) ,x_constraint))))


      ;; Equivalence: Apply the recursively, then add
      ;; additional clauses
      ((typep constraint 'equivalence-constraint)
       ;; Apply the tseitin transform to the implicant and consequent parts
       (let ((x_A (tseitin-encoding sat (equivalence-constraint-lhs constraint)))
             (x_B (tseitin-encoding sat (equivalence-constraint-rhs constraint))))
         ;; Add additional clauses
         (add-clause sat `(,x_A ,x_B ,x_constraint))
         (add-clause sat `((:not ,x_A) (:not ,x_B) ,x_constraint))
         (add-clause sat `((:not ,x_A) ,x_B (:not ,x_constraint)))
         (add-clause sat `(,x_A (:not ,x_B) (:not ,x_constraint)))))


      ;; Conflict: Apply the recursively, then add
      ;; additional clauses
      ((typep constraint 'conflict-constraint)
       ;; Apply the Tseitin transform recursively to all sub expressions
       (let ((x_is (mapcar #'(lambda (ci)
                               (tseitin-encoding sat ci))
                           (conflict-constraint-expressions constraint))))
         ;; Now, add additional clauses
         (add-clause sat (concatenate 'list
                                      `((:not ,x_constraint))
                                      (loop for x_i in x_is collecting `(:not ,x_i))))
         (dolist (x_i x_is)
           (add-clause sat `(,x_constraint ,x_i)))))

      (T
       (error "Invalid constraint type: can't perform Tseitin transformation!")))

    x_constraint))


(defmethod get-sat-variable (sat constraint)
  "Returns a SAT variable for representing the given constraint. If one already
has been created, returns it! Otherwise adds a new variable.

This also returns a secondary value -- for whether or not the variable
was found (t), or if it wasn't found but had to be created (nil)"
  (with-slots (constraint->sat-variable) sat
    (let (variable found?)
      (multiple-value-setq (variable found?) (genhash:hashref constraint constraint->sat-variable))
      (when found?
        ;; Simply return the existing variable for re-use
        (return-from get-sat-variable (values variable t)))
      ;; If we get here, no such variable for representing this constraint has been
      ;; generated yet -- so make one.
      (let ((sat-variable (add-new-sat-variable sat :tag constraint)))
        (setf (genhash:hashref constraint constraint->sat-variable) sat-variable)
        (values sat-variable nil)))))


(defmethod add-new-sat-variable (sat &key (tag nil))
  "Helper function that creates a new SAT variable and adds it to our encoding.
Marks it with the given tag."
  (with-slots (id->sat-variable) sat
    (let ((sat-variable (make-instance 'sat-variable
                                       :id (length id->sat-variable)
                                       :tag tag)))
      (vector-push-extend sat-variable id->sat-variable)
      sat-variable)))


(defmethod add-clause (sat clause-description)
  "Helper method for adding clauses via a simple, succint syntax"
  (with-slots (clauses) sat
    (let ((clause nil))
      (dolist (lit-desc clause-description)
        (cond
          ;; If it's in the form <variable>
          ((typep lit-desc 'sat-variable)
           (push (make-instance 'sat-literal
                                :variable lit-desc
                                :positive t)
                 clause))
          ;; If it's in the form (:not <variable>)
          ((typep lit-desc 'list)
           (assert (= 2 (length lit-desc)))
           (assert (eql :not (elt lit-desc 0)))
           (assert (typep (elt lit-desc 1) 'sat-variable))
           (push (make-instance 'sat-literal
                                :variable (elt lit-desc 1)
                                :positive nil)
                 clause))

          (T
           (error "Invalid clause description to add to SAT formulation!"))))
      ;; Add this clause!
      (push (reverse clause) clauses))))


(defmethod generate-dimacs-clauses (sat)
  "Helper function that generates a list of clauses, where each clause is itself
a list of positive or negative integers, based from 1."
  (with-slots (clauses) sat
    (let (cs)
      (dolist (clause clauses)
        (let (c)
          (dolist (sl clause)
            (push (if (positive? sl)
                      (id (sat-literal-variable sl))
                      (- (id (sat-literal-variable sl))))
                  c))
          (push c cs)))
      cs)))


(defmethod retrieve-sat-variable-for-assignment (sat assignment)
  "Helper function to retrieve the SAT variable corresponding to an assignment."
  (genhash:hashref (make-instance 'assignment-constraint :assignment assignment) (sat-encoding-constraint->sat-variable sat)))



(defmethod simplify-clauses (sat)
  "Performs basic clause simplifications."
  (with-slots (clauses id->sat-variable) sat

    ;; Remove any duplicate literals from each of the clauses
    (setf clauses (loop for clause in clauses
                     collecting (remove-duplicates clause :test #'literals-equal?)))


    ;; Remove any clauses that have both x and not(x). Automatically satisfied.
    (setf clauses (loop for clause in clauses
                     when (not (clause-contains-opposing-literals? clause))
                       collect clause))

    ;; Perform unit propagation
    ;;(unit-propagation sat)
    t))


(defmethod clause-contains-opposing-literals? (clause)
  "Helper function that returns true if this clause contains opposint literals,
   (i.e., x and not(x) )and nil otherwise."
  (declare (optimize (debug 3)))
  (let ((h (make-hash-table :test #'eql)))
    (dolist (lit clause)
      (let (pos? found?)
        (multiple-value-setq (pos? found?) (gethash (sat-literal-variable lit) h))
        (cond
          (found?
           (when (not (eql pos? (positive? lit)))
             (return-from clause-contains-opposing-literals? t)))
          (t
           (setf (gethash (sat-literal-variable lit) h) (positive? lit)))))))
  nil)


(defmethod substitute-literal (sat literal-find literal-replace)
  "Helper method that substitutes all instances of some literal, with
another literal. Keeps track of positivity / negativity correctly. Can
be used, for example, if one literal is defined to be equivalent to another
literal, all instances of the first variable from the clauses."
  (let ((var-find (sat-literal-variable literal-find))
        (pos-find (positive? literal-find))
        (var-replace (sat-literal-variable literal-replace))
        (pos-replace (positive? literal-replace)))
    (with-slots (clauses) sat
      (setf clauses (loop for clause in clauses
                       collecting (mapcar #'(lambda (lit)
                                              (cond
                                                ((eql var-find (sat-literal-variable lit))
                                                 (make-instance 'sat-literal
                                                                :variable var-replace
                                                                :positive (if (eql pos-find (positive? lit))
                                                                              pos-replace
                                                                              (not pos-replace))))
                                                (t
                                                 lit)))
                                          clause))))))



(defmethod unit-propagation (sat)
  "Run unit propagation to simplify the given SAT clauses. Modifies in place (i.e., does not return a copy).
   Unit propagation looks for unit clauses, which clearly must hold for the theory to be satisfiable, and then
   substitutes True / False accordingly in later clauses to simplify them. If any other clauses now become unit,
   the process continues until a fixed point is reached. Does not change the satisfiability of the theory."
  ;; TODO This implementation is very slow and inefficient!
  ;; Making faster isn't my focus; and watch literals are evidently patented?
  (declare (optimize (speed 3)))
  (with-slots (clauses) sat
    ;; Keep looping until no clauses change
    (let ((n-propagations 0))
      (loop with run-again?
         do
           (block clause-propagation
             (setf run-again? nil)
             ;; Find a unit clause
             (dolist (clause clauses)
               (when (= 1 (length clause))
                 ;; Found a unit clause! Now simplify all other clauses.
                 (let ((lit (first clause))
                       clauses-new)
                   ;; (format t "Unit propagating: ~a~%" lit)
                   (incf n-propagations)
                   (dolist (c clauses)
                     ;; Only add the clause if it does not contain this literal
                     (unless (member lit c :test #'literals-equal?)
                       ;; Stip off any negations of this literal from clauses
                       (push (remove-if #'(lambda (l)
                                            (literals-equal? l (negate-literal lit)))
                                        c)
                             clauses-new)))
                   (setf clauses (reverse clauses-new))
                   (setf run-again? t)
                   (return-from clause-propagation)))))
         while run-again?)
      (format t "Unit propagated ~a time(s).~%" n-propagations))))
