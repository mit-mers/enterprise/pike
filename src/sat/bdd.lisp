;;;; Copyright (c) 2018 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)

;;; Implementation of Reduced Ordered Binary Decision Diagrams (ROBDD's)
;;; We use bdd-nodes to represent functions.
;;;
;;; Some optimizations made by this implementation:
;;;   - computed table for bdd-apply-like operations
;;;   - Uses structs to be faste
;;;   - Multi-rooted, shared BDD's (via the manager)

;;; NOTE: there are other optimizations that could be made to this library!
;;; One includes complement arcs.
;;;
;;; NOTE: Should I use weak hash tables here? Right now nodes are never
;;;       actually removed from the manager etc.

(in-package #:pike)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; BDD Manager struct
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Highly optimized hash table functions
(defun eql-list-3 (m1 m2)
  (declare (optimize (speed 3) (debug 0) (safety 0) (debug 0)))
  (and (eq (first m1) (first m2))
       (eq (second m1) (second m2))
       (eq (third m1) (third m2))))

(defun eql-list-2 (m1 m2)
  (declare (optimize (speed 3) (debug 0) (safety 0) (debug 0)))
  (and (eq (first m1) (first m2))
       (eq (second m1) (second m2))))

(defun eql-list-n (m1 m2)
  (declare (optimize (speed 3) (debug 0) (safety 0) (debug 0))
           (type list m1 m2))

  (do ((p1 m1 (cdr p1))
       (p2 m2 (cdr p2)))
      (nil t)

    ;; If both p1 and p2 have reached the end, return t
    (when (not (or p1 p2))
      (return-from eql-list-n t))

    ;; Otherwise if one has reached the end but the other
    ;; hasn't, return nil
    (when (or (not p1)
              (not p2))
      (return-from eql-list-n nil))

    ;; Check the elements. Return nil
    ;; if not eq.
    (unless (eq (car p1) (car p2))
      (return-from eql-list-n nil))))


(sb-ext:define-hash-table-test eql-list-n sxhash)
(sb-ext:define-hash-table-test eql-list-3 sxhash)
(sb-ext:define-hash-table-test eql-list-2 sxhash)

(defstruct (bdd-manager (:print-function print-bdd-manager))
  ;; The list of nodes managed here
  ;; (nodes nil :type list)
  ;; The number of nodes here
  (num-nodes 0 :type fixnum)
  ;; Define a hash table mapping variables (via #'eql)
  ;; to variable orderings
  (variable->index (make-hash-table :test #'eql :rehash-size 2.0) :type hash-table) ;; var --> i
  (index->variable (make-array 0 :adjustable nil) :type simple-vector)
  ;; Cache to re-use existing BDD nodes, thereby ensuring
  ;; canonicity and efficiency. Maps lists (variable node_low node_high)
  ;; to nodes that represent that if-then-else function.
  (unique-table (make-hash-table :test #'eql-list-3 :rehash-size 2.0) :type hash-table) ;; (var, child_low, child_high) -> n
  ;; Operation-specific hash-tables for AND, OR, and NOT
  (computed-table-and (make-hash-table :test #'eql-list-2 :rehash-size 2.0) :type hash-table) ;; (id(f), id(g)) -> n
  (computed-table-or (make-hash-table :test #'eql-list-2 :rehash-size 2.0) :type hash-table) ;; (id(f), id(g)) -> n
  (computed-table-not (make-hash-table :test #'eql :rehash-size 2.0) :type hash-table) ;; id(f) -> n
  ;; Store past projections
  (computed-table-projection (make-hash-table :test #'eql :rehash-size 2.0) :type hash-table) ;; id(f) -> n
  (index->projected-onto? (make-array 0 :adjustable nil :element-type 'boolean :initial-element t) :type simple-vector) ;; i -> t/nil
  ;; Literal weights and computed weighted model counts
  (literal->weight (make-hash-table :test #'eql-list-2 :rehash-size 2.0) :type hash-table) ;; (var, t/nil) --> weight
  (computed-table-alpha (make-hash-table :test #'eql :rehash-size 2.0)) ;; id(f) -> alpha
  ;; Datastructures for the incremental, queue-based version.
  (op-unique-table (make-hash-table :test #'eql-list-n :rehash-size 2.0) :type hash-table) ;; (:op sorted-operand-ids ...) -> n
  (queue-ops nil) ;; Represents set of queued ops
  (node-ops (make-hash-table :test #'eql :rehash-size 2.0) :type hash-table) ;; id(f) --> sorted list of op tuples.
  (parents (make-hash-table :test #'eql :rehash-size 2.0) :type hash-table) ;; id(f) --> list of parent nodes (only used during apply-once)
  (node->heuristic (make-hash-table :test #'eql :rehash-size 2.0) :type hash-table) ;; id(f) --> number
  (n-iterations 0 :type fixnum)
  ;; (bdd-op-heuristic-fn #'(lambda (n) (declare (ignore n)) 0) :type function) ;; function: node -> real
  (computed-table-approx (make-hash-table :test #'eql-list-2 :rehash-size 2.0) :type hash-table) ;; (id(f) id(r)) -> node
  ;; Datastructures for MaxΣ∏
  (computed-table-beta (make-hash-table :test #'eql)) ;; if(f) -> beta
  (index->Σ∏-variable? (make-array 0 :adjustable nil :element-type 'boolean :initial-element t) :type simple-vector)) ;; i -> t/nil





(defun print-bdd-manager (bdd-manager stream ignore)
  "Pretty print."
  (declare (ignore ignore))
  (with-slots (num-nodes) bdd-manager
    (format stream "#<BDD-MANAGER with ~a nodes>" num-nodes)))


(defun create-bdd-manager ()
  (let ((bm (make-bdd-manager)))
    ;; Add two nodes corresponding to the terminals 0 and 1, respectively
    (get-bdd-node bm 0 nil nil)
    (get-bdd-node bm 1 nil nil)
    ;; Return it
    bm))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; BDD node struct
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defstruct (bdd-node (:print-function print-bdd-node))
  ;; A unique ID for this node
  (id 0 :type fixnum)
  ;; The variable represented by this if-then-else BDD node.
  ;; Note that this variable can be any class. Note: you probably
  ;; don't want to directly instantiate this class; use the
  ;; bdd-manager instead.
  (variable)
  ;; The high (1) child
  (child-high (error "Must specify high child of BDD!"))
  ;; The low (0) child
  (child-low (error "Must specify low child of BDD!")))


(defun print-bdd-node (bdd-node stream ignore)
  "Pretty print."
  (declare (ignore ignore))
  (format stream "#<BDD-NODE n~a>" (bdd-node-id bdd-node)))


(defun bdd-node-terminal? (node &key (value -1))
  "Returns t if this node is a terminal; nil otherwise. If an optional value
is specified, returns t if it's that terminal value. Ex., if value is 0,
will return t only if the node represents 0."
  (declare (optimize (speed 3)))
  (declare (type bdd-node node)
           (type fixnum value))
  (assert (or (= value -1) (= value 0) (= value 1)))
  (cond ((= value -1)
         (or (= (bdd-node-id node) 0)
             (= (bdd-node-id node) 1)))
        (t
         (= (bdd-node-id node) value))))


(defun bdd-node-terminal-boolean-value (node)
  "Helper function to return the terminal value
as a boolean (t or nil) for this terminal node."
  (declare (optimize (speed 3)))
  (declare (type bdd-node node))
  (assert (bdd-node-terminal? node))
  (if (= (bdd-node-id node) 0)
      nil
      t))


;; A special op node for use with the incremental version.
(defstruct (bdd-op-node (:include bdd-node)
                        (:print-function print-bdd-op-node))

  ;; Name of operation for this node
  (operation :noop)
  ;; A list of operands for this node.
  (operands nil))


(defun print-bdd-op-node (bdd-op-node stream ignore)
  "Pretty print."
  (declare (ignore ignore))
  (format stream "#<BDD-OP-NODE ~a n~a>" (bdd-op-node-operation bdd-op-node) (bdd-node-id bdd-op-node)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; BDD API
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun register-variable-ordering (bm vars)
  "Register a given ordering of the variables. vars should be an
in-order list of variables. Variables earlier in the list will
come higher up in the BDD's."
  ;; TODO: This should only be called at the beginning. Throw
  ;; an error if it's not.
  (declare (optimize (speed 3))
           (type list vars)
           (type bdd-manager bm))
  (with-slots (variable->index index->variable index->projected-onto? index->Σ∏-variable?) bm
    (let ((n (length vars)))
      (clrhash variable->index)
      (setf index->variable (make-array (+ n 2) :initial-element nil))
      (let ((i 0))
        (declare (type fixnum i))
        (dolist (v vars)
          (setf (gethash v variable->index) i)
          (setf (aref index->variable i) v)
          (incf i)))
      ;; Register the terminal's variables as the last indices This trick helps with
      ;; weighted model counting
      (setf (gethash 0 variable->index) n)
      (setf (gethash 1 variable->index) n)
      (setf (aref index->variable n) 0)
      (setf (aref index->variable (1+ n)) 1) ;; A bit weird but OK
      ;; Default to projecting onto every variable: i.e., no projection
      (setf index->projected-onto? (make-array n :element-type 'boolean :initial-element t))
      (setf index->Σ∏-variable? (make-array n :element-type 'boolean :initial-element t)))))


(defun get-bdd-terminal (bm value)
  "Return either the 0 or the 1 terminal node. Value
should be tierh 0 or 1."
  (declare (optimize (speed 3)))
  (declare (type bdd-manager bm)
           (type fixnum value))
  (assert (or (= value 0) (= value 1)))
  (get-bdd-node bm value nil nil))



(defun get-bdd-for-single-variable (bm variable &key (positive? t))
  "Return a BDD representing the single variable, positively or
negatively."
  (declare (optimize (speed 3)))
  (declare (type bdd-manager bm)
           (type boolean positive?))
  (get-bdd-node bm
                variable
                (if positive?
                    (get-bdd-terminal bm 0)
                    (get-bdd-terminal bm 1))
                (if positive?
                    (get-bdd-terminal bm 1)
                    (get-bdd-terminal bm 0))))



(defun bdd-satisfiable? (bm n)
  "Returns t if and only if the boolean function
represented by the bdd-node n is satisfiable, nil otherwise."
  (declare (optimize (speed 3)))
  (declare (type bdd-manager bm)
           (type bdd-node n))
  (not (eql (get-bdd-terminal bm 0) n)))


(defun bdd-valid? (bm n)
  "Returns t if and only if the boolean function
represented by the bdd-node n is valid (i.e., always true), nil otherwise."
  (declare (optimize (speed 3)))
  (declare (type bdd-manager bm)
           (type bdd-node n))
  (eql (get-bdd-terminal bm 1) n))


(declaim (inline get-bdd-variable-index))
(defun get-bdd-variable-index (bm n)
  "Helper function to look up the variable index associated
with a given node's variable."
  (declare (optimize (speed 3)))
  (declare (type bdd-manager bm)
           (type bdd-node n))
  (with-slots (variable->index) bm
    (gethash (bdd-node-variable n) variable->index)))


(defun bdd-and (bm f g &key (use-cache t))
  "Take the conjunction of two BDD's. Returns a
BDD-node representing this conjunction.

   Note that this is a little faster than bdd-apply,
as it will do pruning earlier."
  (declare (optimize (speed 3)))
  (declare (type bdd-manager bm)
           (type bdd-node f g))

  ;; (format t "bdd-and: ~a ~a~%" f g)
  ;; First check if this is one of the terminal cases (no recursion required!)

  ;; Is f == g? If so just return one
  (when (eql f g)
    (return-from bdd-and f))

  ;; If either f or g are 0, then return 0.
  (when (or (bdd-node-terminal? f :value 0)
            (bdd-node-terminal? g :value 0))
    (return-from bdd-and (get-bdd-terminal bm 0)))

  ;; If f is 1, return g.
  (when (bdd-node-terminal? f :value 1)
    (return-from bdd-and g))

  ;; If g is 1, return f.
  (when (bdd-node-terminal? g :value 1)
    (return-from bdd-and f))

  (with-slots (computed-table-and) bm
    ;; Not a base case. But check and see if we've already cached
    ;; the value of f & g; if so, just return that node.
    (let* (n found? key
             ;; First, we'll try and order f and g. Put the smaller f
             ;; first. More likely to result in cache hits.
             (f-i (get-bdd-variable-index bm f))
             (g-i (get-bdd-variable-index bm g)))
      (declare (type fixnum f-i g-i))
      (when (< g-i f-i)
        ;; Swap them -- now f will be the minimum index (highest in BDD)
        (multiple-value-setq (f g) (values g f)))

      ;; Believe it or not, sometimes this cache *significantly slows us down!
      ;; Like, by several orders of magnigude slow down...! Should investigate more why.
      ;; Are common lisp hash tables really that much slower than re-building every time?
      (when use-cache
        ;; Set the key
        (setf key `(,(bdd-node-id f) ,(bdd-node-id g)))
        ;; Have we already computed f & g? If so, use that one.
        (multiple-value-setq (n found?) (gethash key computed-table-and))
        (when found?
          ;; (format t "Cache hit!~%")
          ;; Return the already-computed one!
          (let ()
            (declare (type bdd-node n))
            (return-from bdd-and n))))

      ;; If we get here, not a terminal case and not a pre-computed
      ;; cached value. Recurse.
      ;; We know from the re-ordering above that f has a variable index
      ;; that is <= that of g. Two cases: less, and equal.
      (let (n-child-low n-child-high)
        (cond
          ;; Case I: f-i == g-i
          ((= f-i g-i)
           (setf n-child-low (bdd-and bm
                                      (bdd-node-child-low f)
                                      (bdd-node-child-low g)
                                      :use-cache use-cache))
           (setf n-child-high (bdd-and bm
                                       (bdd-node-child-high f)
                                       (bdd-node-child-high g)
                                       :use-cache use-cache)))
          ;; Case II: f-i < g-i
          (t
           (setf n-child-low (bdd-and bm
                                      (bdd-node-child-low f)
                                      g
                                      :use-cache use-cache))
           (setf n-child-high (bdd-and bm
                                       (bdd-node-child-high f)
                                       g
                                       :use-cache use-cache))))

        (let ()
          (declare (type bdd-node n-child-low n-child-high))
          ;; If the low and high childs are the same, don't make
          ;; a new node -- just return this one. Otherwise,
          ;; create (or look up an existing) node for this
          ;; if-then-else
          (setf n (if (eql n-child-low n-child-high)
                      n-child-low
                      (get-bdd-node bm (bdd-node-variable f) n-child-low n-child-high)))
          ;; Cool! Now that we've computed the result, add it to our
          ;; computed table.
          (when use-cache
            (setf (gethash key computed-table-and) n))
          n)))))


(defun bdd-or (bm f g)
  "Take the disjunction of two BDD's. Returns a
BDD-node representing this disjunction.

   Note that this is a little faster than bdd-apply,
as it will do pruning earlier."
  (declare (optimize (speed 3)))
  (declare (type bdd-manager bm)
           (type bdd-node f g))

  ;; (format t "bdd-or: ~a ~a~%" f g)
  ;; First check if this is one of the terminal cases (no recursion required!)

  ;; Is f == g? If so just return one
  (when (eql f g)
    (return-from bdd-or f))

  ;; If either f or g are 1, then return 1.
  (when (or (bdd-node-terminal? f :value 1)
            (bdd-node-terminal? g :value 1))
    (return-from bdd-or (get-bdd-terminal bm 1)))

  ;; If f is 0, return g.
  (when (bdd-node-terminal? f :value 0)
    (return-from bdd-or g))

  ;; If g is 0, return f.
  (when (bdd-node-terminal? g :value 0)
    (return-from bdd-or f))

  (with-slots (computed-table-or) bm
    ;; Not a base case. But check and see if we've already cached
    ;; the value of f & g; if so, just return that node.
    (let* (n found? key
             ;; First, we'll try and order f and g. Put the smaller f
             ;; first. More likely to result in cache hits.
             (f-i (get-bdd-variable-index bm f))
             (g-i (get-bdd-variable-index bm g)))
      (declare (type fixnum f-i g-i))
      (when (< g-i f-i)
        ;; Swap them -- now f will be the minimum index (highest in BDD)
        (multiple-value-setq (f g) (values g f)))
      ;; Set the key
      (setf key `(,(bdd-node-id f) ,(bdd-node-id g)))
      ;; Have we already computed f & g? If so, use that one.
      (multiple-value-setq (n found?) (gethash key computed-table-or))
      (when found?
        ;; Return the already-computed one!
        (let ()
          (declare (type bdd-node n))
          (return-from bdd-or n)))
      ;; If we get here, not a terminal case and not a pre-computed
      ;; cached value. Recurse.
      ;; We know from the re-ordering above that f has a variable index
      ;; that is <= that of g. Two cases: less, and equal.
      (let (n-child-low n-child-high)
        (cond
          ;; Case I: f-i == g-i
          ((= f-i g-i)
           (setf n-child-low (bdd-or bm
                                      (bdd-node-child-low f)
                                      (bdd-node-child-low g)))
           (setf n-child-high (bdd-or bm
                                       (bdd-node-child-high f)
                                       (bdd-node-child-high g))))
          ;; Case II: f-i < g-i
          (t
           (setf n-child-low (bdd-or bm
                                      (bdd-node-child-low f)
                                      g))
           (setf n-child-high (bdd-or bm
                                       (bdd-node-child-high f)
                                       g))))

        (let ()
          (declare (type bdd-node n-child-low n-child-high))
          ;; If the low and high childs are the same, don't make
          ;; a new node -- just return this one. Otherwise,
          ;; create (or look up an existing) node for this
          ;; if-then-else
          (setf n (if (eql n-child-low n-child-high)
                      n-child-low
                      (get-bdd-node bm (bdd-node-variable f) n-child-low n-child-high)))
          ;; Cool! Now that we've computed the result, add it to our
          ;; computed table.
          (setf (gethash key computed-table-or) n)
          n)))))


(defun bdd-not (bm f)
  "Return a new BDD node representing the negation of the binary function
f (represented as a BDD node)."
  (declare (optimize (speed 3)))
  (declare (type bdd-manager bm)
           (type bdd-node f))

  ;; (format t "bdd-not: ~a~%" f)

  ;; First, check if it's a terminal case. This means that
  ;; f is either 0 or 1. Invert it.
  (when (bdd-node-terminal? f :value 0)
    (return-from bdd-not (get-bdd-terminal bm 1)))
  (when (bdd-node-terminal? f :value 1)
    (return-from bdd-not (get-bdd-terminal bm 0)))

  ;; Not a terminal.
  (with-slots (computed-table-not) bm
    ;; Check and see if we've already cached the negation of this
    ;; node.
    (let (n found?
            (key (bdd-node-id f)))
      (multiple-value-setq (n found?) (gethash key computed-table-not))
      (when found?
        ;; Return the already-computed one!
        (let ()
          (declare (type bdd-node n))
          (return-from bdd-not n)))
      ;; If we get here, not a terminal case and not a pre-computed
      ;; cached value. Recurse downwards.
      (let ((n-child-low (bdd-not bm (bdd-node-child-low f)))
            (n-child-high (bdd-not bm (bdd-node-child-high f))))
        (declare (type bdd-node n-child-low n-child-high))
        ;; If the low and high childs are the same, don't make
        ;; a new node -- just return this one. Otherwise,
        ;; create (or look up an existing) node for this
        ;; if-then-else
        (setf n (if (eql n-child-low n-child-high)
                    n-child-low
                    (get-bdd-node bm (bdd-node-variable f) n-child-low n-child-high)))
        ;; Cool! Now that we've computed the result, add it to our
        ;; computed table.
        (setf (gethash key computed-table-not) n)
        n))))



(defun bdd-apply (bm f g op)
  ;; TODO! May not strictly be necessary, since I have
  ;; optimized, specialized implementatios of AND, OR, and NOT above.
  (error "Not implemented!"))




(defun bdd-condition (bm f values &key (computed-conditions nil))
  "Return a new BDD, with the values of the given literals committed to.
In d-DNNF, this is known as conditioning; in the BDD literature, this is
often referred to as restricting.

bm - a BDD manager
f - a BDD node representing a function to condition
values - a hash table mapping variables of BDD nodes to 0 / 1 values.
         Any variable as a key there will be conditioned. Those not
         will be left alone.

Returns: A bdd node representing the conditioning."
  (declare (optimize (speed 3))
           (type bdd-manager bm)
           (type bdd-node f)
           (type hash-table values))

  ;; Keep track of computed-conditions, a hash table mapping nodes
  ;; to conditioned results. We don't store this in bm, like the other
  ;; computed tables, because there could potentially be a lot of different
  ;; values the ''values'' hash table (and results would need to be stored for each).
  ;; TODO: Is the above right? Actually maybe I can be clever about this, and save lots of space!
  (when (eql nil computed-conditions)
    (setf computed-conditions (make-hash-table :test #'eql)))

  ;; If f is a terminal node, simply return it
  (when (bdd-node-terminal? f)
    (return-from bdd-condition f))

  ;; Check if we've already cached the result
  (let ((key (bdd-node-id f)))
    (let (f-conditioned
          found?)
      (multiple-value-setq (f-conditioned found?) (gethash key computed-conditions))
      (when found?
        (return-from bdd-condition f-conditioned)))

    ;; Is the variable at this node one that we're conditioning on?
    (let ((val -1)
          (found? nil)
          n)
      (multiple-value-setq (val found?) (gethash (bdd-node-variable f) values))
      (when found?
        ;; Yes, it is! Condition by selecting the proper child.
        (cond
          ((eql 0 val) (setf n (bdd-condition bm (bdd-node-child-low f) values :computed-conditions computed-conditions)))
          ((eql 1 val) (setf n (bdd-condition bm (bdd-node-child-high f) values :computed-conditions computed-conditions)))
          (t (error "Invalid conditioning value: must be 0 or 1!")))
        (setf (gethash f computed-conditions) n)
        (return-from bdd-condition n))

      ;; Nope, not found. So simply recurse downward on the two children.
      (let ((n-child-low (bdd-condition bm (bdd-node-child-low f) values :computed-conditions computed-conditions))
            (n-child-high (bdd-condition bm (bdd-node-child-high f) values :computed-conditions computed-conditions)))

        (declare (type bdd-node n-child-low n-child-high))
        ;; If the low and high childs are the same, don't make
        ;; a new node -- just return this one. Otherwise,
        ;; create (or look up an existing) node for this
        ;; if-then-else
        (setf n (if (eql n-child-low n-child-high)
                    n-child-low
                    (get-bdd-node bm (bdd-node-variable f) n-child-low n-child-high)))
        ;; Cool! Now that we've computed the result, add it to our
        ;; computed table.
        (setf (gethash key computed-conditions) n)
        (return-from bdd-condition n)))))


(defun bdd-set-projection-variables (bm vars-proj)
  "Set the list of variables that are being projected onto.
This is useful for "
  (with-slots (index->projected-onto? index->variable variable->index) bm
    ;; Sanity check the variables
    (dolist (v vars-proj)
      (unless (gethash v variable->index)
        (error "Attempting to project onto variable ~a that does not exist in BDD! (binaries?)" v)))
    ;; Set the mask properly
    (dotimes (i (length index->projected-onto?))
      (if (member (aref index->variable i) vars-proj)
          (setf (aref index->projected-onto? i) t)
          (setf (aref index->projected-onto? i) nil))))
  ;; Reset the cache
  (reset-wmc-and-projection-cache bm))


(defun bdd-set-Σ∏-variables (bm vars-proj)
  "Set the list of variables that are being projected onto.
This is useful for "
  (with-slots (index->Σ∏-variable? index->variable variable->index) bm
    ;; Sanity check the variables
    (dolist (v vars-proj)
      (unless (gethash v variable->index)
        (error "Attempting to set Σ∏ variable ~a that does not exist in BDD! (binaries?)" v)))
    ;; Set the mask properly
    (dotimes (i (length index->Σ∏-variable?))
      (if (member (aref index->variable i) vars-proj)
          (setf (aref index->Σ∏-variable? i) t)
          (setf (aref index->Σ∏-variable? i) nil))))
  ;; Reset the cache
  (reset-max-Σ∏-cache bm))


(defun bdd-project (bm f)
  "Projection. Projects onto the variables previously set by
(bdd-set-projection-variables ...). Specifically, leaves all
assignments to those variables, such that there exists values
to the other variables satisfiying f. Works with the weighted
model counting.

Precondition: the projection variables have already been set."
  (declare (optimize (speed 3))
           (type bdd-manager bm)
           (type bdd-node f))
  (with-slots (computed-table-projection index->projected-onto? variable->index) bm
    (let ((key (bdd-node-id f)))
      ;; Check to see if we have previously computed the projection of f
      ;; under these same variables. If so, return the result.
      (let (result found?)
        (multiple-value-setq (result found?) (gethash key computed-table-projection))
        (when found?
          (return-from bdd-project (the bdd-node result))))

      ;; If f is terminal, return it.
      (when (bdd-node-terminal? f)
        (return-from bdd-project f))

      ;; f is not terminal, so recurse on the children
      (let ((n-low-projected (bdd-project bm (bdd-node-child-low f)))
            (n-high-projected (bdd-project bm (bdd-node-child-high f)))
            n)
        (declare (type bdd-node n-low-projected n-high-projected))
        (cond
          ;; var(f): projecting onto this variable
          ((aref index->projected-onto? (gethash (bdd-node-variable f) variable->index))
           (setf n (if (eql n-low-projected n-high-projected)
                       n-low-projected
                       (get-bdd-node bm (bdd-node-variable f) n-low-projected n-high-projected))))

          ;; var(f): projecting away this variable
          (t
           (setf n (bdd-or bm n-low-projected n-high-projected))))

        ;; Cool! Now that we've computed the result, add it to our
        ;; computed table and return the result.
        (setf (gethash key computed-table-projection) n)
        n))))



(defun bdd-existential-quantify (bm f var &key (computed-table nil))
  (declare (optimize (speed 3))
           (type bdd-manager bm)
           (type bdd-node f))

  ;; TODO: Optimization: remember the cacheing
  (unless computed-table
    (setf computed-table (make-hash-table :test #'eql)))

  (let ((key (bdd-node-id f)))
    ;; Check to see if we have previously computed the projection of f
    ;; under these same variables. If so, return the result.
    (let (result found?)
      (multiple-value-setq (result found?) (gethash key computed-table))
      (when found?
        (return-from bdd-existential-quantify (the bdd-node result))))

    ;; If f is terminal, return it.
    (when (bdd-node-terminal? f)
      (return-from bdd-existential-quantify f))

    ;; TODO Optimization: if the variable in question is "higher" up than the root
    ;; of this BDD, just return this BDD (no need to recurse)

    ;; f is not terminal, so recurse on the children
    (let ((n-low (bdd-existential-quantify bm (bdd-node-child-low f) var :computed-table computed-table))
          (n-high (bdd-existential-quantify bm (bdd-node-child-high f) var :computed-table computed-table))
          n)
      (declare (type bdd-node n-low n-high))
      (cond
        ;; var(f): projecting onto this variable
        ((not (eql (bdd-node-variable f) var))
         (setf n (if (eql n-low n-high)
                     n-low
                     (get-bdd-node bm (bdd-node-variable f) n-low n-high))))

        ;; var(f): projecting away this variable
        (t
         (setf n (bdd-or bm n-low n-high))))

      ;; Cool! Now that we've computed the result, add it to our
      ;; computed table and return the result.
      (setf (gethash key computed-table) n)
      n)))


(defun bdd-universal-quantify (bm f var &key (computed-table nil))
  (declare (optimize (speed 3))
           (type bdd-manager bm)
           (type bdd-node f))

  (unless computed-table
    (setf computed-table (make-hash-table :test #'eql)))

  (let ((key (bdd-node-id f)))
    ;; Check to see if we have previously computed the projection of f
    ;; under these same variables. If so, return the result.
    (let (result found?)
      (multiple-value-setq (result found?) (gethash key computed-table))
      (when found?
        (return-from bdd-universal-quantify (the bdd-node result))))

    ;; If f is terminal, return it.
    (when (bdd-node-terminal? f)
      (return-from bdd-universal-quantify f))

    ;; f is not terminal, so recurse on the children
    (let ((n-low (bdd-universal-quantify bm (bdd-node-child-low f) var :computed-table computed-table))
          (n-high (bdd-universal-quantify bm (bdd-node-child-high f) var :computed-table computed-table))
          n)
      (declare (type bdd-node n-low n-high))
      (cond
        ;; var(f): projecting away this variable
        ((not (eql (bdd-node-variable f) var))
         (setf n (if (eql n-low n-high)
                     n-low
                     (get-bdd-node bm (bdd-node-variable f) n-low n-high))))

        ;; var(f): projecting away this variable
        (t
         (setf n (bdd-and bm n-low n-high))))

      ;; Cool! Now that we've computed the result, add it to our
      ;; computed table and return the result.
      (setf (gethash key computed-table) n)
      n)))


(defun bdd-enumerate-all-projected-models (bm f)
  "Enumerates all projected models (i.e., paths to a 1 only including
projected variables). Recursive. Returns a list of models, where each model
 is itself a list of assignments, where each assignment is a list like (variable t)
or (variable nil)."
  (with-slots (index->projected-onto? variable->index index->variable) bm
    (let ((projected-models (bdd-enumerate-all-projected-models-helper bm f))
          (i-var (the fixnum (gethash (bdd-node-variable f) variable->index))))
      ;; Now, take into account variables from the start!
      (loop for i from 0 to (1- i-var) do
           (when (aref index->projected-onto? i)
             (setf projected-models (loop for m in projected-models
                                       collect `(,@m (,(aref index->variable i) nil))
                                       collect `(,@m (,(aref index->variable i) t))))))
      projected-models)))




(defun bdd-enumerate-all-projected-models-helper (bm f)
  "Enumerates all projected models (i.e., paths to a 1 only including
projected variables). Recursive. Returns a list of models, where each model
 is itself a list of assignments, where each assignment is a list like (variable t)
or (variable nil)."
  (with-slots (index->projected-onto? variable->index index->variable) bm
    ;; Base cases!
    ;; If f is terminal 1, then return a single empty model.
    ;; If f is terminal 0, return no models.
    (cond
      ((bdd-node-terminal? f :value 1)
       (return-from bdd-enumerate-all-projected-models-helper (list nil)))
      ((bdd-node-terminal? f :value 0)
       (return-from bdd-enumerate-all-projected-models-helper nil)))

    ;; Not a base case; recurse.
    (let ((projected-models-low  (bdd-enumerate-all-projected-models-helper bm (bdd-node-child-low f)))
          (projected-models-high (bdd-enumerate-all-projected-models-helper bm (bdd-node-child-high f)))
          (i-var (the fixnum (gethash (bdd-node-variable f) variable->index)))
          (i-var-child-low (the fixnum (gethash (bdd-node-variable (bdd-node-child-low f)) variable->index)))
          (i-var-child-high (the fixnum (gethash (bdd-node-variable (bdd-node-child-high f)) variable->index))))

      ;; Error check
      (unless (aref index->projected-onto? i-var)
        (error "Invalid call to BDD WMC: current variable is being projected away!"))

      ;; Low branch. First, add this assignment on to every low model
      ;; First, add this assignment on.
      (setf projected-models-low (loop for m in projected-models-low
                                    collect `(,@m (,(bdd-node-variable f) nil))))
      ;; For each "in between" variable being projected into, cross and add on both
      ;; low and high combinations
      (loop for i from (1+ i-var) to (1- i-var-child-low) do
           (when (aref index->projected-onto? i)
             (setf projected-models-low (loop for m in projected-models-low
                                           collect `(,@m (,(aref index->variable i) nil))
                                           collect `(,@m (,(aref index->variable i) t))))))

      ;; Same procedure with high
      ;; First, add this assignment on.
      (setf projected-models-high (loop for m in projected-models-high
                                     collect `(,@m (,(bdd-node-variable f) t))))
      ;; For each "in between" variable being projected into, cross and add on both
      ;; low and high combinations
      (loop for i from (1+ i-var) to (1- i-var-child-high) do
           (when (aref index->projected-onto? i)
             ;;(format t "Yeah!!!! ~a~%" (aref index->variable i))
             (setf projected-models-high (loop for m in projected-models-high
                                            collect `(,@m (,(aref index->variable i) nil))
                                            collect `(,@m (,(aref index->variable i) t))))))

      `(,@projected-models-low ,@projected-models-high))))



(defun bdd-project-priority (bm f)
  "Optimized version if we have priority variables"
  (error "Not implemented!"))


(defun bdd-set-weights (bm weights)
  "Set the weights of literals to perform weighted model counting.
Weights should be ..."
  (with-slots (literal->weight) bm
    (setf literal->weight weights)
    (reset-wmc-and-projection-cache bm)))



(defun reset-wmc-and-projection-cache (bm)
  (with-slots (computed-table-alpha computed-table-projection) bm
    (clrhash computed-table-alpha)
    (clrhash computed-table-projection)))

(defun reset-max-Σ∏-cache (bm)
  (with-slots (computed-table-beta) bm
    (clrhash computed-table-beta)))


(defun bdd-wmc (bm f)
  "Perform weighted model counting on the BDD! Takes into account the projection variables too.

   Precondition: assume weights have been set if desired. The weights maps
literals in the form of (assignment t/nil) -> number. Any literal value
not found there is assumed to have a weight of 1. So if no weights have been
set, this will also just return the straight up model count (i.e., number of solutions)."
  (declare (optimize (speed 3))
           (type bdd-manager bm)
           (type bdd-node f))
  (with-slots (variable->index index->variable index->projected-onto?) bm
    (let ((alpha (bdd-wmc-compute-alpha bm f))
          (i-var-f (the fixnum (gethash (bdd-node-variable f) variable->index)))
          (prod 1))
      (loop for i from 0 to (1- i-var-f) do
           (when (aref index->projected-onto? i)
             (setf prod (* prod
                           (+ (bdd-wmc-get-weight bm (aref index->variable i) t)
                              (bdd-wmc-get-weight bm (aref index->variable i) nil))))))
      (* alpha prod))))


(defun bdd-wmc-compute-alpha (bm f)
  "A helper function to compute the alpha for a given BDD node.
Alpha is the weighted model count of f, for variables from Var(f) downward to literals (i.e. of subtheory over subvariables)"
  (declare (optimize (speed 3))
           (type bdd-manager bm)
           (type bdd-node f))
  (with-slots (computed-table-alpha literal->weight variable->index index->variable index->projected-onto?) bm
    (let ((alpha 0)
          (alpha-low 0)
          (alpha-high 0)
          (key (bdd-node-id f)))

      ;; Check to see if alpha for f has already been cached
      (let (result found?)
        (multiple-value-setq (result found?) (gethash key computed-table-alpha))
        (when found?
          ;; (format t "WMC cache hit! on ~a~%" f)
          (return-from bdd-wmc-compute-alpha (the real result))))

      (cond
        ;; f is terminal
        ((bdd-node-terminal? f)
         (setf alpha (if (bdd-node-terminal? f :value 0)
                         0
                         1)))

        ;; f not terminal
        (t

         ;; Use those values to compute alpha for this node.
         (let ((i-var (the fixnum (gethash (bdd-node-variable f) variable->index)))
               (i-var-child-low (the fixnum (gethash (bdd-node-variable (bdd-node-child-low f)) variable->index)))
               (i-var-child-high (the fixnum (gethash (bdd-node-variable (bdd-node-child-high f)) variable->index)))
               (prod-low 1)
               (prod-high 1))

           ;; Error check: var(f) should never be a variable that is being projected away.
           ;; If it is, signal an error. To do a WMC over all variables, set the projection
           ;; variables to everything
           (unless (aref index->projected-onto? i-var)
             (error "Invalid call to BDD WMC: current variable is being projected away!"))

           ;; Recursively compute alpha for the low and high children
           (setf alpha-low (bdd-wmc-compute-alpha bm (bdd-node-child-low f)))
           (setf alpha-high (bdd-wmc-compute-alpha bm (bdd-node-child-high f)))

           ;; Multiply the products weights along the low branch
           (loop for i from (1+ i-var) to (1- i-var-child-low) do
                (when (aref index->projected-onto? i)
                  (setf prod-low (* prod-low
                                    (+ (bdd-wmc-get-weight bm (aref index->variable i) t)
                                       (bdd-wmc-get-weight bm (aref index->variable i) nil))))))
           ;; Multiply the products weights along the high branch
           (loop for i from (1+ i-var) to (1- i-var-child-high) do
                (when (aref index->projected-onto? i)
                  (setf prod-high (* prod-high
                                     (+ (bdd-wmc-get-weight bm (aref index->variable i) t)
                                        (bdd-wmc-get-weight bm (aref index->variable i) nil))))))
           ;; Compute alpha for this node
           (setf alpha (+ (* alpha-low
                             (bdd-wmc-get-weight bm (bdd-node-variable f) nil)
                             prod-low)
                          (* alpha-high
                             (bdd-wmc-get-weight bm (bdd-node-variable f) t)
                             prod-high))))))

      ;; Store this new alpha in the cache and return it
      ;; (format t "Storing WMC cache ~a -> ~a~%" key alpha)
      (setf (gethash key computed-table-alpha) alpha)
      (the real alpha))))



(defun bdd-maxΣ∏ (bm f)
  "Perform weighted model counting on the BDD! Takes into account the projection variables too.

   Precondition: assume weights have been set if desired. The weights maps
literals in the form of (assignment t/nil) -> number. Any literal value
not found there is assumed to have a weight of 1. So if no weights have been
set, this will also just return the straight up model count (i.e., number of solutions)."
  (declare (optimize (speed 3))
           (type bdd-manager bm)
           (type bdd-node f))
  (with-slots (variable->index index->variable index->Σ∏-variable?) bm
    (let ((beta (bdd-wmc-compute-beta bm f))
          (i-var-f (the fixnum (gethash (bdd-node-variable f) variable->index)))
          (prod 1))
      (loop for i from 0 to (1- i-var-f) do
           (when (aref index->Σ∏-variable? i)
             (setf prod (* prod
                           (+ (bdd-wmc-get-weight bm (aref index->variable i) t)
                              (bdd-wmc-get-weight bm (aref index->variable i) nil))))))
      (* beta prod))))


(defun bdd-wmc-compute-beta (bm f)
  "A helper function to compute the beta for a given BDD node.
Beta is the max over controllable choices (in order) of the weighted model count of f (i.e. of subtheory over subvariables)"
  (declare (optimize (speed 3))
           (type bdd-manager bm)
           (type bdd-node f))

  (with-slots (computed-table-beta literal->weight variable->index index->variable index->Σ∏-variable?) bm
    (let ((beta 0)
          (beta-low 0)
          (beta-high 0)
          (key (bdd-node-id f)))

      ;; Check to see if beta for f has already been cached
      (let (result found?)
        (multiple-value-setq (result found?) (gethash key computed-table-beta))
        (when found?
          ;; (format t "MaxΣ∏ cache hit! on ~a~%" f)
          (return-from bdd-wmc-compute-beta (the real result))))

      (cond
        ;; f is terminal
        ((bdd-node-terminal? f)
         (setf beta (if (bdd-node-terminal? f :value 0)
                        0
                        1)))

        ;; f not terminal
        (t
         (let ((i-var (the fixnum (gethash (bdd-node-variable f) variable->index)))
               (i-var-child-low (the fixnum (gethash (bdd-node-variable (bdd-node-child-low f)) variable->index)))
               (i-var-child-high (the fixnum (gethash (bdd-node-variable (bdd-node-child-high f)) variable->index)))
               (prod-low 1)
               (prod-high 1))

           ;; Recursively compute beta for the low and high children
           (setf beta-low (bdd-wmc-compute-beta bm (bdd-node-child-low f)))
           (setf beta-high (bdd-wmc-compute-beta bm (bdd-node-child-high f)))

           ;; Multiply the products weights along the low branch (takes into account skipped variables)
           (loop for i from (1+ i-var) to (1- i-var-child-low) do
                (when (aref index->Σ∏-variable? i)
                  (setf prod-low (* prod-low
                                    (+ (bdd-wmc-get-weight bm (aref index->variable i) t)
                                       (bdd-wmc-get-weight bm (aref index->variable i) nil))))))

           ;; Multiply the products weights along the high branch (takes into account skipped variables)
           (loop for i from (1+ i-var) to (1- i-var-child-high) do
                (when (aref index->Σ∏-variable? i)
                  (setf prod-high (* prod-high
                                     (+ (bdd-wmc-get-weight bm (aref index->variable i) t)
                                        (bdd-wmc-get-weight bm (aref index->variable i) nil))))))

           (cond
             ;; A Σ∏ variable!
             ((aref index->Σ∏-variable? i-var)
              ;; (format t "Taking Σ∏ on ~a~%" (bdd-node-id f))
              (setf beta (+ (* beta-low
                               (bdd-wmc-get-weight bm (bdd-node-variable f) nil)
                               prod-low)
                            (* beta-high
                               (bdd-wmc-get-weight bm (bdd-node-variable f) t)
                               prod-high))))

             ;; A max variable!
             (t
              ;; (format t "Taking max on ~a~%" (bdd-node-id f))
              (setf beta (max (* beta-low
                                 ;; (bdd-wmc-get-weight bm (bdd-node-variable f) nil) ;; Max vars should cleanly have weight "1"
                                 prod-low)
                              (* beta-high
                                 ;; (bdd-wmc-get-weight bm (bdd-node-variable f) t)
                                 prod-high))))))))

      ;; Store this new alpha in the cache and return it
      ;; (format t "Storing WMC cache ~a -> ~a~%" key alpha)
      (setf (gethash key computed-table-beta) beta)
      (the real beta))))


(defun bdd-extract-max-policy (bm f)
  "Extracts the best policy (as a BDD) from the BDD representation
of the explicit graph. This procedure closely follows the compute-beta
procedure, but is analogous to argmax vs max."
  (declare (optimize (speed 3))
           (type bdd-manager bm)
           (type bdd-node f))

  ;; First, compute beta for the subgraph.
  (bdd-wmc-compute-beta bm f)

  ;; Go!
  (bdd-extract-max-policy-helper bm f))


(defun bdd-extract-max-policy-helper (bm f)
  "Helper to extract the best policy (as a BDD) from the BDD representation
of the explicit graph. This procedure closely follows the compute-beta
procedure, but is analogous to argmax vs max.

  Precondition: beta is computed for f and all cofactors."
  (declare (optimize (speed 3))
           (type bdd-manager bm)
           (type bdd-node f))

  ;; Next, we will recursively
  (with-slots (computed-table-beta literal->weight variable->index index->variable index->Σ∏-variable?) bm
    (let (n-low
          n-high
          n-result
          (beta-low 0)
          (beta-high 0))

      ;; (format t "Node ~a splitting on ~a~%" f (bdd-node-variable f))
      ;; If we've reached a terminal node, return it.
      (when (bdd-node-terminal? f)
        (return-from bdd-extract-max-policy-helper f))

      (let ((i-var (the fixnum (gethash (bdd-node-variable f) variable->index)))
            (i-var-child-low (the fixnum (gethash (bdd-node-variable (bdd-node-child-low f)) variable->index)))
            (i-var-child-high (the fixnum (gethash (bdd-node-variable (bdd-node-child-high f)) variable->index)))
            (prod-low 1)
            (prod-high 1))

        ;; If this is a sub product node, recurse on both children and return both.
        (cond
          ;; A Σ∏ variable!
          ((aref index->Σ∏-variable? i-var)
           ;; Recurse on low and high
           ;; Keep both children and return the result (after reducing)
           (setf n-low (bdd-extract-max-policy-helper bm (bdd-node-child-low f)))
           (setf n-high (bdd-extract-max-policy-helper bm (bdd-node-child-high f))))


          ;; A max variable!
          (t
           ;; Recursively compute beta for the low and high children
           (setf beta-low  (gethash (bdd-node-id (bdd-node-child-low f)) computed-table-beta))
           (setf beta-high  (gethash (bdd-node-id (bdd-node-child-high f)) computed-table-beta))

           ;; Multiply the products weights along the low branch (takes into account skipped variables)
           (loop for i from (1+ i-var) to (1- i-var-child-low) do
                (when (aref index->Σ∏-variable? i)
                  (setf prod-low (* prod-low
                                    (+ (bdd-wmc-get-weight bm (aref index->variable i) t)
                                       (bdd-wmc-get-weight bm (aref index->variable i) nil))))))

           ;; Multiply the products weights along the high branch (takes into account skipped variables)
           (loop for i from (1+ i-var) to (1- i-var-child-high) do
                (when (aref index->Σ∏-variable? i)
                  (setf prod-high (* prod-high
                                     (+ (bdd-wmc-get-weight bm (aref index->variable i) t)
                                        (bdd-wmc-get-weight bm (aref index->variable i) nil))))))

           ;; (format t "Low: ~a, High: ~a~%" (* beta-low prod-low) (* beta-high prod-high))

           ;; Pick the best one
           (cond
             ;; Recurse, take the low branch and set other to 0
             ((> (* beta-low prod-low)
                 (* beta-high prod-high))
              (setf n-low (bdd-extract-max-policy-helper bm (bdd-node-child-low f)))
              (setf n-high (get-bdd-terminal bm 0)))

             ;; Take the high branch and set other to 0
             (t
              (setf n-low (get-bdd-terminal bm 0) )
              (setf n-high (bdd-extract-max-policy-helper bm (bdd-node-child-high f)))))))

        (setf n-result (if (eql n-low n-high)
                           n-low
                           (get-bdd-node bm
                                         (bdd-node-variable f)
                                         n-low
                                         n-high)))

        ;; (format t "Returning: ~a~%" n-result)
        n-result))))



(declaim (inline bdd-wmc-get-weight))
(defun bdd-wmc-get-weight (bm var pos)
  "Helper function to retrieve the weight of a given literal. Returns 1
if its weight hasn't been explicitly set."
  (declare (optimize (speed 3)))
  (with-slots (literal->weight) bm
    (let (w found?)
      (multiple-value-setq (w found?) (gethash `(,var ,pos) literal->weight))
      (if found?
          (the real w)
          1))))





(defun encode-bayesian-network-enc1-for-bdd (bn &key (encode-determinism t))
  "This algorithm refers to ENC1 in (Chavira 2008). When encode-determinism is t,
  if any of the cpt entries are 0, a conflict is added preventing it from ever running.
   I think it's similar to do something for 1 too.

   Note that this method is different than the d-DNNF one for the weighted-model-counter class,
   in that it does not operate directly on a SAT instance. Instead, it returns a set of constraints
   and weights that define the weighted model counting problem."
  (let ((weights (make-hash-table :test #'eql-list-2))
        (all-variables (copy-seq (bayesian-network-variables bn)))
        (constraints nil)
        (wmc-var->dependencies (make-hash-table :test #'eql)))

    (dolist (cpt (conditional-probability-tables bn))
      ;; For each CPT, add new variables and the constraints theta <=>
      (do-cpt-assignments (var-assignment parents-assignments p) cpt
                          (cond
                            ;; If determinims is enabled and this entry is 0, essentially add a conflict
                            ((and (= 0 p)
                                  encode-determinism)
                             (push (make-instance 'disjunction-constraint
                                                  :disjuncts (loop for a in `(,var-assignment ,@parents-assignments)
                                                                collecting (make-instance 'negation-constraint
                                                                                          :expression
                                                                                          (make-instance 'assignment-constraint :assignment a))))
                                   constraints))

                            ((and (= 1 p)
                                  encode-determinism)
                             ;; We don't need to encode a theta variable with weight 1! Won't change the WMC, so ignore.
                             t)

                            ;; Default case
                            (t
                             ;; Create a new binary variable <=> to the conjunction of variables.
                             (let ((x-theta (make-instance 'decision-variable
                                                           :name (format nil "θ_{~a|~a}" var-assignment (format nil "~{~a~^,~}" parents-assignments))
                                                           :domain `("1" "0")
                                                           :controllable? nil)))
                               (push x-theta all-variables)
                               (push (make-instance 'equivalence-constraint
                                                    :lhs (make-instance 'assignment-constraint :assignment (assignment x-theta "1"))
                                                    :rhs (make-instance 'conjunction-constraint
                                                                        :conjuncts (loop for a in `(,var-assignment ,@parents-assignments)
                                                                                      collecting (make-instance 'assignment-constraint :assignment a))))
                                     constraints)
                               ;; Record a proper weight for theta equal to p
                               (setf (gethash `(,(assignment x-theta "1") t) weights) p)
                               ;; Associate this theta with this cpt, for future reference
                               (setf (gethash x-theta wmc-var->dependencies) `(,(cpt-variable cpt) ,@(cpt-parents cpt))))))))

    (values weights all-variables constraints wmc-var->dependencies)))



(defun encode-influence-diagram-enc1-for-bdd (id choice-variable-guards &key (encode-determinism t))
  "A modification of the ENC1 encoding but for influence diagrams. The actual encoding is exactly
  the same as ENC1; the only difference is that we keep track of some additional variable dependencies
  to account for the functionally-defined active_H type variables."
  (let (weights all-variables constraints wmc-var->dependencies)
    ;; Start with the stock ENC1 encoding
    (multiple-value-setq
        (weights all-variables constraints wmc-var->dependencies)
      (encode-bayesian-network-enc1-for-bdd id
                                            :encode-determinism encode-determinism))

    ;; Record that each active_H type variable is functionally defined by the guard of H.
    ;; We can thus add a dependency from each active_H to each variable in the guard.
    ;; The actual constraints of the form active_H <=> guard , and active_H <=> H=c_1 or H=c_2 ...
    ;; have been added elsewhere, when the conditionalness was handled.
    (dolist (var (bayesian-network-variables id))


      (let (guard found? active-var dependencies)
        ;; Get a guard for this variable
        (multiple-value-setq (guard found?) (gethash var choice-variable-guards))
        (when (and found? guard)
          (setf dependencies (mapcar #'assignment-variable guard))
          ;; Find the associated active variable
          (setf active-var (find-variable all-variables (format nil "active_~a" (decision-variable-name var))))
          (setf (gethash active-var wmc-var->dependencies) dependencies))))



    ;; Return values
    (values weights all-variables constraints wmc-var->dependencies)))




(defun encode-phantom-bayesian-networks-enc1-for-bdd (bn observation-scenarios choice-variable-guards &key (encode-determinism t))
  "This algorithm refers to ENC1 in (Chavira 2008). When encode-determinism is t,
  if any of the cpt entries are 0, a conflict is added preventing it from ever running.
   I think it's similar to do something for 1 too.

   Note that this method is different than the d-DNNF one for the weighted-model-counter class,
   in that it does not operate directly on a SAT instance. Instead, it returns a set of constraints
   and weights that define the weighted model counting problem."
  (let ((weights (make-hash-table :test #'eql-list-2))
        (all-variables nil)
        (constraints nil)
        (wmc-var->dependencies (make-hash-table :test #'eql))
        obs
        (obs->bn-variables (make-hash-table :test #'eql))
        obs-dependencies
        (i 0))

    ;; Create a special "obs" decision variable that will be set based on the observation scenario.
    (setf obs (make-instance 'decision-variable
                             :name "obs"
                             :controllable? nil
                             :domain (loop for i from 1 to (length observation-scenarios) collecting (format nil "c~a" i))))
    (push obs all-variables)

    ;; Now, iterate for each ``phantom'' Bayesian network.
    (dolist (observation-scenario observation-scenarios)
      (let ((obs-val (format nil "c~a" (incf i)))
            (assignment->new-assignment (make-hash-table :test #'eql))
            (variable->new-variable (make-hash-table :test #'eql))
            variables-observed variables-unobserved)

        ;; Compute which variables are observed, and which ones are not observed.
        (setf variables-observed observation-scenario)
        (setf variables-unobserved (set-difference (bayesian-network-variables bn) observation-scenario))
        ;; For each unobserved variable, create a new ``phantom'' variable whose job is to aid in normalization,
        ;; and won't be connected to the real version of that variable. For each variable, map its assignments
        ;; to either its real variables (if it's observed) or its phantom values (if it's unobserved)
        (dolist (var (bayesian-network-variables bn))
          (cond
            ;; Observed variable
            ((member var variables-observed)
             (push var all-variables)
             (push var (gethash (assignment obs obs-val) obs->bn-variables))
             (setf (gethash var variable->new-variable) var)
             (dolist (val (decision-variable-values var))
               (setf (gethash (assignment var val) assignment->new-assignment)
                     (assignment var val))))

            ;; Unobserved variable -- create a phantom and map assignments
            (t
             (let ((var-phantom (make-instance 'decision-variable
                                               :name (format nil "~a-~a" (decision-variable-name var) i)
                                               :domain (decision-variable-values var)
                                               :controllable? nil)))
               (push var-phantom all-variables)
               (push var-phantom (gethash (assignment obs obs-val) obs->bn-variables))
               (setf (gethash var variable->new-variable) var-phantom)
               (dolist (val (decision-variable-values var))
                 (setf (gethash (assignment var val) assignment->new-assignment)
                       (assignment var-phantom val)))))))

        ;; Add a constraint enforcing this phantom BN applies when this obs=ci holds.
        (let (guard-constraints)
          ;; Compute a set of ``guard constraints'', which intuitively the conjunction of which will hold iff
          ;; this observation scenario happens. It's the conjunction of the guards of all observed variables, or
          ;; the negation of the guards of all unobserved variables.
          (dolist (var (bayesian-network-variables bn))
            (let (guard-constraint
                  guard found?)
              ;; Get a guard for this variable
              (multiple-value-setq (guard found?) (gethash var choice-variable-guards))
              (cond
                ;; Variable has a guard -- so we know when it will be observed.
                (found?
                 (setf guard-constraint (make-instance 'conjunction-constraint :conjuncts (loop for a in guard
                                                                                             collecting (make-instance 'assignment-constraint :assignment a))))
                 (setf obs-dependencies `(,@obs-dependencies ,@(mapcar #'assignment-variable guard))))

                ;; Variable has no guard -- means it is always a latent variable and never observed.
                ((not found?)
                 (setf guard-constraint (make-instance 'constant-constraint :constant nil))))

              (cond
                ;; Observed variable
                ((member var variables-observed)
                 (push guard-constraint guard-constraints))

                ;; Unobserved variable
                (t (push (make-instance 'negation-constraint :expression guard-constraint) guard-constraints)))))

          ;; Add the constraint
          (push (make-instance 'implication-constraint
                               :implicant (make-instance 'conjunction-constraint :conjuncts guard-constraints)
                               :consequent (make-instance 'assignment-constraint :assignment (assignment obs obs-val)))
                constraints))


        ;; Add some lockdown constraints
        (dolist (var-unobserved variables-unobserved)
          (push (make-instance 'equivalence-constraint
                               :lhs (make-instance 'assignment-constraint :assignment (assignment obs obs-val))
                               :rhs (make-instance 'negation-constraint
                                                   :expression (make-instance 'assignment-constraint
                                                                              :assignment (assignment (gethash var-unobserved variable->new-variable) "∘"))))
                constraints))


        (dolist (cpt (conditional-probability-tables bn))
          ;; For each CPT, add new variables and the constraints theta <=>
          (let ((var-mapped (gethash (cpt-variable cpt) variable->new-variable))
                (parents-mapped (loop for parent in (cpt-parents cpt) collecting (gethash parent variable->new-variable))))
            (do-cpt-assignments (var-assignment parents-assignments p) cpt
                                (let ((var-assignment-mapped (gethash var-assignment assignment->new-assignment))
                                      (parents-assignments-mapped (loop for a in parents-assignments collecting (gethash a assignment->new-assignment))))
                                  (cond
                                    ;; If determinism is enabled and this entry is 0, essentially add a conflict
                                    ((and (= 0 p)
                                          encode-determinism)
                                     (push (make-instance 'implication-constraint
                                                          :implicant (make-instance 'assignment-constraint :assignment (assignment obs obs-val))
                                                          :consequent (make-instance 'disjunction-constraint
                                                                                     :disjuncts (loop for a in `(,var-assignment-mapped ,@parents-assignments-mapped)
                                                                                                   collecting (make-instance 'negation-constraint
                                                                                                                             :expression
                                                                                                                             (make-instance 'assignment-constraint :assignment a)))))
                                           constraints))

                                    ;; Default case
                                    (t
                                     ;; Create a new binary variable <=> to the conjunction of variables.
                                     (let ((x-theta (make-instance 'decision-variable
                                                                   :name (format nil "θ_~a|~a-~a" var-assignment-mapped (format nil "~{~a~^,~}" parents-assignments-mapped) i)
                                                                   :domain `("1" "0")
                                                                   :controllable? t)))
                                       (push x-theta all-variables)
                                       (push (make-instance 'equivalence-constraint
                                                            :lhs (make-instance 'assignment-constraint :assignment (assignment x-theta "1"))
                                                            :rhs (make-instance 'conjunction-constraint
                                                                                :conjuncts `(,@(loop for a in `(,var-assignment-mapped ,@parents-assignments-mapped)
                                                                                                  collecting (make-instance 'assignment-constraint :assignment a))
                                                                                               ,(make-instance 'assignment-constraint :assignment (assignment obs obs-val)))))
                                             constraints)


                                       ;; Record a proper weight for theta equal to p
                                       (setf (gethash `(,(assignment x-theta "1") t) weights) p)

                                       ;; Associate this theta with this cpt, for future reference
                                       (setf (gethash x-theta wmc-var->dependencies) `(,var-mapped ,@parents-mapped ,obs)))))))))))

    ;; Remove duplicates from all-variables.
    ;; all variables contains the random variables, plus the theta variables too.
    (setf all-variables (remove-duplicates all-variables))

    ;; Set the dependencies of obs -- it will depend on the union of all of the variables aboved in the above
    ;; constraint.
    (setf obs-dependencies (remove-duplicates obs-dependencies))
    (setf (gethash obs wmc-var->dependencies) obs-dependencies)

    (format t "Constraints:~%")
    (dolist (c constraints)
      (format t "    ~a~%" c))

    (values weights all-variables constraints wmc-var->dependencies)))



(defun bdd-recycle (bm &key (bdds-to-keep nil))
  "A first attempt at memory management / recycling.

  Recursively traverses all of the important BDDs in bdds-to-keep,
setting storing the set of node id's that should be kept around / are
part of BDDs that will be used.

   Then, iterates over the unique table and removes any key/value pair where
the value is a BDD node that we're not keeping. Does similar computations
for the computed-and, computed-or, and computed-not tables.

Doing the above should release lots of references to BDDs we (may) not
ever need, and allow a full garbage collect to reclaim them."
  ;; (let ((node-id->keep? (make-hash-table :test #'eql :rehash-size 2.0)))
  ;;   (dolist (node-keep bdds-to-keep)
  ;;     (bdd-bfs node-keep #'(lambda (n)
  ;;                            (setf (gethash (bdd-node-id n) node-id->keep?) t))))
  ;;   )
  (format t "BDD recycling~%"))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; BDD internal methods
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun get-bdd-node (bm variable child-low child-high &key (record-parents nil))
  "Returns a BDD node splitting on variable, with the given
child-low and child-high (both BDD nodes). If no such node
exists yet, creates and returns a new one."
  (declare (optimize (speed 3)))
  (declare (type bdd-manager bm))
  (with-slots (num-nodes unique-table parents) bm
    ;; Set up a key of the form <variable, child-low, child-high> for the unique table
    (let ((key (if (and (typep child-low 'bdd-node) (typep child-high 'bdd-node))
                   `(,variable ,(bdd-node-id child-low) ,(bdd-node-id child-high))
                   `(,variable ,child-low ,child-high)))
          n found?)
      (multiple-value-setq (n found?) (gethash key unique-table))
      (when found?
        (return-from get-bdd-node (the bdd-node n)))
      ;; An existing node wasn't found; so therefore create a new one
      (setf n (make-bdd-node
               :id num-nodes
               :variable variable
               :child-low child-low
               :child-high child-high))
      (setf num-nodes (+ num-nodes 1))

      (when (= 0 (mod num-nodes 1000000))
        (format t "BDD nodes: ~a~%" num-nodes)
        ;; (when (= 0 (mod num-nodes 10000000))
        ;;   (format t "Garbage collecting...")
        ;;   (sb-ext:gc :full t)
        ;;   (format t "done~%"))
        t)

      ;; (push n nodes)
      ;; Record parents, if desired
      (when record-parents
        (push n (gethash (bdd-node-id child-low) parents))
        (push n (gethash (bdd-node-id child-high) parents)))
      ;; Store this new node in the unique-table
      (setf (gethash key unique-table) n)
      ;; And return it
      (the bdd-node n))))


(defun bdd-get-all-children (node &key (expand-op-node-children nil))
  "Gets all of the children descendants
of a node."
  (let (children)
    (bdd-bfs node #'(lambda (n)
                      (push n children))
             :expand-op-node-children expand-op-node-children)
    children))


(defun bdd-get-all-ancestors (bm nodes)
  "Gets all of the children descendants
of the set of nodes."
  (declare (optimize (speed 3))
           (type bdd-manager bm)
           (type list nodes))
  (let (ancestors)
    (bdd-upwards-bfs bm nodes #'(lambda (n)
                                  (declare (type bdd-node n))
                                  (unless (member n nodes)
                                    (push n ancestors))))
    ancestors))


(defun bdd-count-children (node)
  "Counts all of the children descendants
of a node."
  (let ((count 0))
    (bdd-bfs node #'(lambda (n)
                      (declare (ignore n))
                      (incf count)))
    count))


(defun bdd-bfs (node fn &key (expand-op-node-children nil))
  "Traverse all of the nodes reachable from node in breadth-first order.
  Maintains a visited list so as to not re-visit already-visited nodes"
  (declare (optimize (speed 3)))
  (declare (type bdd-node node))
  (declare (type function fn))
  (let ((Q `(,node))
        (visited (make-hash-table))
        n)
    ;;(declare (type bdd-node n))
    (setf (gethash node visited) t)
    (loop while Q do
       ;; Pop off the queue
         (setf n (pop Q))
       ;; Call the callback on this node
         (funcall fn n)
       ;; Add any unvisited children to the Q
         (let ((children (cond ((bdd-node-terminal? n) nil)
                               ((typep n 'bdd-op-node)
                                (if expand-op-node-children
                                    (bdd-op-node-operands n)
                                    nil))
                               (t `(,(bdd-node-child-high n) ,(bdd-node-child-low n))))))
           (dolist (child children)
             (when (not (gethash child visited))
               ;; Mark this child as visited and add to the end of Q
               (setf (gethash child visited) t)
               (setf Q `(,@Q ,child))))))))


(defun bdd-upwards-bfs (bm nodes fn)
  "Traverse all of the nodes ancestors in breadth-first order.
  Maintains a visited list so as to not re-visit already-visited nodes"
  (declare (optimize (speed 3)))
  (declare (type list nodes))
  (declare (type function fn))
  (declare (Type bdd-manager bm))

  (with-slots (parents) bm
    (let ((Q `(,@nodes))
          (visited (make-hash-table))
          n)
      ;;(declare (type bdd-node n))
      (dolist (node nodes)
        (setf (gethash node visited) t))
      (loop while Q do
         ;; Pop off the queue
           (setf n (pop Q))
         ;; Call the callback on this node
           (funcall fn n)
         ;; Add any unvisited children to the Q
           (unless (or (bdd-node-terminal? n)
                       (typep n 'bdd-op-node))
             (dolist (parent (gethash (bdd-node-id n) parents))
               (when (not (gethash parent visited))
                 ;; Mark this child as visited and add to the end of Q
                 (setf (gethash parent visited) t)
                 (setf Q `(,@Q ,parent)))))))))


(defun bdd-to-dot-stream (node stream &key (show-op-node-children nil) (node->vertex-attributes (make-hash-table)) (node->edge-attributes (make-hash-table)) (omit-edges-to-zero nil))
  (let ((nodes (bdd-get-all-children node :expand-op-node-children show-op-node-children)))
    (bdd-nodes-to-dot-stream nodes
                             stream
                             :show-op-node-children show-op-node-children
                             :node->vertex-attributes node->vertex-attributes
                             :node->edge-attributes node->edge-attributes
                             :omit-edges-to-zero omit-edges-to-zero)))


(defun bdds-to-dot-stream (nodes stream &key (show-op-node-children nil))
  (let (nodes-all)
    (dolist (n nodes)
      (setf nodes-all `(,@(bdd-get-all-children n :expand-op-node-children show-op-node-children) ,@nodes-all)))
    (setf nodes-all (remove-duplicates nodes-all))
    (bdd-nodes-to-dot-stream nodes-all stream :show-op-node-children show-op-node-children)))


(defun bdd-manager-to-dot-stream (bm stream &key (show-op-node-children nil))
  "Illustrate the entire multi-rooted shared structure."
  (with-slots (unique-table) bm
    (let ((nodes (alexandria:hash-table-values unique-table)))
      (bdd-nodes-to-dot-stream nodes stream :show-op-node-children show-op-node-children))))


(defun bdd-nodes-to-dot-stream (nodes stream &key (show-op-node-children nil) (node->vertex-attributes (make-hash-table)) (node->edge-attributes (make-hash-table)) (omit-edges-to-zero nil))
  "Helper function that, when given all of the nodes,
generates dot output."

  (flet ((get-extra-vertex-attributes (n)
           (let ((result (gethash n node->vertex-attributes)))
             (cond
               (result (format nil ",~a" result))
               (t ""))))
         (get-extra-edge-attributes (n)
           (let ((result (gethash n node->edge-attributes)))
             (cond
               (result (format nil ",~a" result))
               (t "")))))

    (format stream "graph BDD{~%")
    ;; Generate all nodes
    (dolist (n nodes)
      (cond
        ;; Terminal node
        ((bdd-node-terminal? n)
         (format stream
                 "  n~a [label=~a,shape=box~a];~%"
                 (bdd-node-id n)
                 (bdd-node-id n)
                 (get-extra-vertex-attributes n)))
        ;; Op node
        ((typep n 'bdd-op-node)
         (format stream
                 "  n~a [label=\"~a\",shape=circle,fixedsize=true,tooltip=\"n~a: ~a operands\",fillcolor=\"black\",fontcolor=\"white\",style=\"filled\"~a];~%"
                 (bdd-node-id n)
                 (bdd-op-node-operation n)
                 (bdd-node-id n)
                 (length (bdd-op-node-operands n))
                 (get-extra-vertex-attributes n)))
        ;; Normal node
        (t
         (format stream
                 "  n~a [label=\"~a\",shape=circle,fixedsize=true~a];~%"
                 (bdd-node-id n)
                 (bdd-node-variable n)
                 (get-extra-vertex-attributes n)))))
    ;; Now, generate all edges
    (dolist (n nodes)
      (cond
        ;; Terminals have no children
        ((bdd-node-terminal? n) t)
        ;; Only show children if desired
        ((typep n 'bdd-op-node)
         (when show-op-node-children
           (dolist (n-operand (bdd-op-node-operands n))
             (format stream
                     "  n~a -- n~a [label=\"\"~a];~%"
                     (bdd-node-id n)
                     (bdd-node-id n-operand)
                     (get-extra-edge-attributes n)))))
        ;; Normal non-terminal BDD node
        (t
         ;; Add it's edges
         (cond
           ;; Normal
           ((not omit-edges-to-zero)
            (format stream
                    "  n~a -- n~a [label=0,style=dashed~a];~%"
                    (bdd-node-id n)
                    (bdd-node-id (bdd-node-child-low n))
                    (get-extra-edge-attributes n))
            (format stream
                    "  n~a -- n~a [label=1~a];~%"
                    (bdd-node-id n)
                    (bdd-node-id (bdd-node-child-high n))
                    (get-extra-edge-attributes n)))
           ;; If requested, suppress edges to 0
           (t
            (when (not (eql 0 (bdd-node-id (bdd-node-child-low n))))
              (format stream
                      "  n~a -- n~a [label=0,style=dashed~a];~%"
                      (bdd-node-id n)
                      (bdd-node-id (bdd-node-child-low n))
                      (get-extra-edge-attributes n)))

            (when (not (eql 0 (bdd-node-id (bdd-node-child-high n))))
              (format stream
                      "  n~a -- n~a [label=1~a];~%"
                      (bdd-node-id n)
                      (bdd-node-id (bdd-node-child-high n))
                      (get-extra-edge-attributes n))))))))
    ;; Close off the graph
    (format stream "}~%")))


(defun bdd-to-dot-file (node filename)
  "Generate a dot file of the given node"
  (with-open-file (stream filename :direction :output :if-exists :supersede)
    (bdd-to-dot-stream node stream)))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; BDD encoder
;;
;; A wrapper around a BDD manager, for handling propositional
;; state logic. Namely, maps assignments to finite-domain variables
;; to representative BDD's.
;;
;; Also allows specification of the BDD lower level variable ordering
;; using clustering (see paper).
;;
;; TODO: Move to a separate file probably.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defstruct (bdd-encoder (:print-function print-bdd-encoder))
  (bm (error "Must specify BDD manager!")
      :type bdd-manager)
  (ss (error "Must specify state space!")
      :type state-space-bits)
  (assignment->bdd (make-hash-table :test #'eql)
                   :type hash-table)
  (assignment->literal (make-hash-table :test #'eql)
                       :type hash-table)
  (variable-constraints-bdd nil))

(defun print-bdd-encoder (bdd-encoder stream ignore)
  "Pretty print."
  (declare (ignore ignore)
           (ignore bdd-encoder))
  (format stream "#<BDD-ENCODER>"))


(defun bdd-encoder-register-variable-ordering (bde variables)
  "Register a variable ordering using the clustered method,
  starting from the highest in the tree (first in list)."
  (with-slots (variable-ordering) bde
    (setf variable-ordering variables)))


(defun bdd-encoder-initialize (bde all-variables &key (use-binaries t) (constraints nil) (variable-order-heuristic :force))
  "Set up the encoding, using the desired encoding scheme.

  Precondition: A variable ordering should have been already provided.

  If use-binaries is t, any finite domain variable with two domain values
  will be guaranteed to use a single boolean/binary variable, rather than
  2 in a one-hot encoding.

  We could potentialily use a log-encoding, but we don't. We use a
  direct (a.k.a one-hot) encoding as outlined in (Walsh 2000).

  Variable order heuristic: can be :default (clustered in the order given),
:random (a random shuffle -- expected to be bad!), or :force (Aloul et al)."
  (declare (type bdd-encoder bde))
  (with-slots (bm ss assignment->bdd variable-constraints-bdd assignment->literal) bde
    (let ((bdd-variable-ordering nil))
      ;; Use the direct encoding outlined in (Walsh 2000); i.e., we use
      ;; a new binary variable for each assignment to a finite-domain variable that
      ;; is 1 iff that assignment holds.
      ;;
      ;; The exception is if use-binaries is true, which makes an optimization that,
      ;; for finite-domain variables with 2 (or less) domain variables, use
      ;; a single boolean variable (more efficient).


      ;; Select a variable ordering
      (cond
        ((eql :force variable-order-heuristic)
         (setf bdd-variable-ordering (bdd-variable-order-heuristic-force all-variables constraints
                                                                         :use-binaries t)))
        ((eql :force-clustered variable-order-heuristic)
         (setf bdd-variable-ordering (bdd-variable-order-heuristic-force-clustered all-variables constraints
                                                                                   :use-binaries t)))
        ((eql :default variable-order-heuristic)
         (setf bdd-variable-ordering (bdd-variable-order-default all-variables
                                                                 :use-binaries use-binaries)))
        ((eql :random variable-order-heuristic)
         (setf bdd-variable-ordering (bdd-variable-order-random all-variables
                                                                :use-binaries use-binaries)))
        (t
         (error "Unrecognized BDD variable order heuristic!")))

      ;; Register with the bm
      (register-variable-ordering bm bdd-variable-ordering)


      ;; Set up a hash table mapping assignments to BDDs that hold iff those
      ;; assignments hold.
      (dolist (v all-variables)
        (let ((values (decision-variable-values v)))
          (cond
            ;; Two domain variables and we're using binary variables
            ((and use-binaries
                  (= (length values) 2))
             ;; First assignment (positive literal)
             (setf (gethash (assignment v (first values)) assignment->bdd)
                   (get-bdd-for-single-variable bm
                                                (assignment v (first values))
                                                :positive? t))
             (setf (gethash (assignment v (first values)) assignment->literal)
                   `(,(assignment v (first values)) t))
             ;; Second assignment (negative literal)
             (setf (gethash (assignment v (second values)) assignment->bdd)
                   (get-bdd-for-single-variable bm
                                                (assignment v (first values))
                                                :positive? nil))
             (setf (gethash (assignment v (second values)) assignment->literal)
                   `(,(assignment v (first values)) nil)))

            ;; Otherwise if domain has 0, 1, or 2+ values
            (t
             (dolist (val values)
               (setf (gethash (assignment v val) assignment->bdd)
                     (get-bdd-for-single-variable bm
                                                  (assignment v val)
                                                  :positive? t))
               (setf (gethash (assignment v val) assignment->literal)
                     `(,(assignment v val) t)))))))

      ;; Create a BDD function that is true iff all of the variables are assigned
      ;; valid values. Encodes the mutex and disjunction variable constraints
      ;; Note that in the best case, if all variables are boolean, this expression
      ;; will be very simply -- simply true / terminal 1!
      (setf variable-constraints-bdd (get-bdd-terminal bm 1))
      (dolist (v all-variables)
        ;; Add a disjunction amongst all assignments
        (setf variable-constraints-bdd
              (bdd-and bm
                       variable-constraints-bdd
                       (constraint->bdd bde
                                        (make-instance 'disjunction-constraint
                                                       :disjuncts (loop for val in (decision-variable-values v)
                                                                     collecting (make-instance 'assignment-constraint
                                                                                               :assignment (assignment v val)))))))

        ;; Add mutex constraints amongst all pairs
        (do-pairs (v1 v2 (decision-variable-values v))
          (setf variable-constraints-bdd
                (bdd-and bm
                         variable-constraints-bdd
                         (constraint->bdd bde
                                          (make-instance 'conflict-constraint
                                                         :expressions (list (make-instance 'assignment-constraint
                                                                                           :assignment (assignment v v1))
                                                                            (make-instance 'assignment-constraint
                                                                                           :assignment (assignment v v2))))))))))))


(defun bdd-encoder-get-assignment-bdd (bde assignment)
  "Retrieve the BDD corresponding to the assignment holding true."
  (declare (optimize (speed 3))
           (type bdd-encoder bde)
           (type decision-variable-assignment assignment))
  (with-slots (assignment->bdd) bde
    (gethash assignment assignment->bdd)))


(defun bdd-encoder-get-literal (bde assignment)
  "Retrieve the literal -- a tuple `(<variable> t/nil) for the
given assignment. Note that this variable is itself an assignment, since
the BDD encoder implements propositional state logic -- assignments to finite
domain variables maps to binary variables."
  (declare (optimize (speed 3))
           (type bdd-encoder bde)
           (type decision-variable-assignment assignment))
  (with-slots (assignment->literal) bde
    (gethash assignment assignment->literal)))


(defun bdd-encoder-get-variable-constraints-bdd (bde)
  "Retrieve a BDD representing the variable constraints (such as
                                                              mutex or disjunction constraints) that must hold."
  (with-slots (variable-constraints-bdd) bde
    variable-constraints-bdd))


(defun constraint->bdd (bde constraint)
  "Convert the constraint fully to an equivalent bdd, given the bdd
encoder."
  (declare (optimize (speed 3))
           (type bdd-encoder bde)
           (type constraint constraint))

  (with-slots (bm) bde
    (cond
      ;; constant
      ((typep constraint 'constant-constraint)
       (get-bdd-terminal bm (if (constant-constraint-constant constraint)
                                1
                                0)))
      ;; assignment
      ((typep constraint 'assignment-constraint)
       (bdd-encoder-get-assignment-bdd bde (assignment-constraint-assignment constraint)))

      ;; and
      ((typep constraint 'conjunction-constraint)
       (let ((n (get-bdd-terminal bm 1)))
         (dolist (conjunct (conjunction-constraint-conjuncts constraint))
           (let ((n-conjunct (constraint->bdd bde conjunct)))
             (setf n (bdd-and bm n n-conjunct))))
         n))

      ;; or
      ((typep constraint 'disjunction-constraint)
       (let ((n (get-bdd-terminal bm 0)))
         (dolist (disjunct (disjunction-constraint-disjuncts constraint))
           (let ((n-disjunct (constraint->bdd bde disjunct)))
             (setf n (bdd-or bm n n-disjunct))))
         n))

      ;; not
      ((typep constraint 'negation-constraint)
       (bdd-not bm (constraint->bdd bde (negation-constraint-expression constraint))))

      ;; =>
      ((typep constraint 'implication-constraint)
       (constraint->bdd bde (make-instance 'disjunction-constraint
                                           :disjuncts (list (make-instance 'negation-constraint
                                                                           :expression (implication-constraint-implicant
                                                                                        constraint))
                                                            (implication-constraint-consequent constraint)))))

      ;; <=>
      ((typep constraint 'equivalence-constraint)
       (constraint->bdd bde (make-instance 'conjunction-constraint
                                           :conjuncts (list (make-instance 'implication-constraint
                                                                           :implicant (equivalence-constraint-lhs constraint)
                                                                           :consequent (equivalence-constraint-rhs constraint))
                                                            (make-instance 'implication-constraint
                                                                           :implicant (equivalence-constraint-rhs constraint)
                                                                           :consequent (equivalence-constraint-lhs constraint))))))

      ;; conflict
      ((typep constraint 'conflict-constraint)
       (constraint->bdd bde (make-instance 'negation-constraint
                                           :expression (make-instance 'conjunction-constraint
                                                                      :conjuncts (conflict-constraint-expressions constraint)))))

      (t
       (error "Invalid constraint type -- can't construct BDD!")))))


(defun bdd-encoder-condition-on-assignment (bde f assignment)
  "Condition the BDD (i.e., restrict) on the given assignment. Makes
assignment 1, and all other asignments to that variable 0. Uses the
encoding."
  (declare (optimize (speed 3))
           (type bdd-encoder bde)
           (type decision-variable-assignment assignment))

  (with-slots (bm) bde
    (let ((values (make-hash-table :test #'eql))
          (var (assignment-variable assignment)))
      ;; Create the values to condition on: 1 for the variable representing xi=vi,
      ;; 0 for all xi!=vi.
      (dolist (val (decision-variable-values var))
        (if (eql (assignment var val) assignment)
            (setf (gethash (assignment var val) values) 1)
            (setf (gethash (assignment var val) values) 0)))
      ;; Condition!
      (bdd-condition bm f values))))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  Variable ordering heuristics
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun bdd-variable-order-default (all-variables &key (use-binaries t))
  "Given a set of decision variables, generate a default variable ordering
for use in the BDD. Returns a list of ordered assignments. If use-binaries is
true, we assume a one-hot encoding and skip redundant assignments for domains
of size 2."
  ;; Create a "clustered" variable ordering (Hadzic paper has ref) given
  ;; the one that was registered before
  (let (assignments-order)
    (dolist (v all-variables)
      (let ((values (decision-variable-values v)))
        (cond
          ;; One or two domain values -- just use a single binary variable
          ((and use-binaries
                (= (length values) 2))
           (push (assignment v (first values)) assignments-order))
          ;; Otherwise use normal direct / one-hot encoding
          (t
           (dolist (val (decision-variable-values v))
             (push (assignment v val) assignments-order))))))
    (reverse assignments-order)))


(defun bdd-variable-order-random (all-variables &key (use-binaries t))
  (let ((assignments-order (bdd-variable-order-default all-variables :use-binaries use-binaries)))
    (sort assignments-order #'(lambda (x y) (declare (ignore x y)) (> (- (random 1.00) 0.50) 0)))))


(defun bdd-variable-order-heuristic-force (all-variables constraints &key (use-binaries t) (N-iterations-factor 1))
  "Uses the FORCE heuristic (Aloul et al 2003) to come up with a reasonable
variable ordering based on the given constraints."

  ;; First, convert the constraints to CNF (we here use the Tseitin encoding
  ;; which may add additional binary variables)

  ;;(Format t "All vars: ~a~%" all-variables)
  ;; TODO Fixup below. It helps a lot with Anytime Bounded BDD Construction,
  ;; because it tends to put the theta variables first.
  (setf all-variables (reverse all-variables))

  (let* ((sat (encode-constraints-as-sat-cnf constraints all-variables))
         (variables (loop for i from 1 to (1- (length (sat-encoding-id->sat-variable sat))) collecting (aref (sat-encoding-id->sat-variable sat) i)))

         (edges nil) ;; Will contain edges, which are lists of variables (we don't care about positive / negative)
         (variable->edges (make-hash-table :test #'eql)) ;; Mapping from each variable to the list of edges it is represented in
         (position (make-hash-table :test #'eql)) ;; Positions of variables in ordering (1 dimensional position)
         (cog (make-hash-table :test #'eql)) ;; "Center of gravity" of the hyperedges
         (positions-tentative nil) ;; List of tuples <variable, pos-tentative>
         (positions-current nil) ;; Current linearized set of positions
         (positions-last nil)) ;; Last linearized generated set of positions.


    ;; Helper function to compute the average edge span
    (flet ((compute-average-edge-span ()
             (let ((total-span 0))
               (dolist (e edges)
                 (let (e-indices)
                   (setf e-indices (mapcar #'(lambda (v) (gethash v position)) e))
                   (incf total-span
                         (- (apply #'max e-indices)
                            (apply #'min e-indices)))))
               (if (= 0 (length edges))
                   0
                   (/ total-span (length edges))))))

      ;; If we're using binaries, modify the SAT encoding to take this into account!
      (when use-binaries
        (dolist (v all-variables)
          ;; Binary variable
          (when (= 2 (length (decision-variable-values v)))
            (substitute-literal sat
                                (make-instance 'sat-literal
                                               :variable (retrieve-sat-variable-for-assignment sat (assignment v (second (decision-variable-values v))))
                                               :positive nil)
                                (make-instance 'sat-literal
                                               :variable (retrieve-sat-variable-for-assignment sat (assignment v (first (decision-variable-values v))))
                                               :positive t))
            (setf variables
                  (remove (retrieve-sat-variable-for-assignment sat (assignment v (second (decision-variable-values v)))) variables)))))

      ;; Construct the "hypergraph" whose vertices are the SAT variables, and whose edges
      ;; are the clauses (ignoring positive / negative). Keep track of the edges associated with
      ;; each variable, and the variables associated with each edge.
      (dolist (clause (clauses sat))
        (let ((e (mapcar #'sat-literal-variable clause)))
          (push e edges)
          (dolist (v e)
            (push e (gethash v variable->edges)))))

      ;; Shuffle!
      ;; (setf variables (sort variables #'(lambda (x y) (declare (ignore x y)) (> (- (random 1.00) 0.50) 0))))

      ;; Assign an initial position to each in the hash table
      (let ((i 0))
        (dolist (v variables)
          (setf (gethash v position)
                (incf i))))
      (setf positions-last variables)

      ;; (format t "Initial Order: ~a~%" (mapcar #'first (sort (mapcar #'(lambda (v) `(,v ,(gethash v position))) variables) #'< :key #'second)))
      ;; (format t "Initial average span: ~a~%" (float (compute-average-edge-span)))
      (block force-iterate
        (dotimes (i (* N-iterations-factor (length edges)))

          ;; Compute the COG (center of gravity) for each edge
          (dolist (e edges)
            (setf (gethash e cog)
                  (/ (apply #'+ (mapcar #'(lambda (v)
                                            (gethash v position))
                                        e))
                     (length e))))

          ;; Compute new tentative positions for each variable
          (setf positions-tentative nil)
          (dolist (v variables)
            (let* ((E-v (gethash v variable->edges))
                   (pos-updated (/  (apply #'+ (mapcar #'(lambda (e)
                                                           (gethash e cog))
                                                       E-v))
                                    (length E-v))))
              (push `(,v ,pos-updated) positions-tentative)))

          ;; Sort the new positions tentative, and copy over to positions (making integer again)
          (setf positions-tentative (stable-sort positions-tentative #'< :key #'second))
          (let ((i 0))
            (dolist (m positions-tentative)
              (setf (gethash (first m) position) (incf i))))
          (setf positions-last positions-current)
          (setf positions-current (mapcar #'first positions-tentative))

          ;; (format t "Order: ~a~%" (mapcar #'first (sort (mapcar #'(lambda (v) `(,v ,(gethash v position))) variables) #'< :key #'second)))
          ;; (format t "Intermediate average span (~a): ~a~%" i (float (compute-average-edge-span)))

          ;; Possible early termination: if we've reached a fixed point!
          (when (equal positions-current positions-last)
            ;; (format t "Early termination!~%")
            (return))))

      (let ((final-order (mapcar #'assignment-constraint-assignment (remove-if-not #'(lambda (x) (typep x 'assignment-constraint)) (mapcar #'sat-variable-tag positions-current)))))
        ;;(format t "Final order:~%")
        ;; (dolist (v final-order)
        ;;   (format t "  ~a~%" v))
        (format t "Force BDD variable order heuristic: final average edge span ~a~%" (float (compute-average-edge-span)))
        final-order))))



(defun bdd-variable-order-heuristic-force-clustered (all-variables constraints &key (use-binaries t) (N-iterations-factor 1))
  "A modified version of the FORCE heuristic (Aloul et al 2003) to come up with a reasonable
variable ordering based on the given constraints. The modification is designed to keep one-hot binary variables
associated with the same finite-domain variable together."

  ;; First, convert the constraints to CNF (we here use the Tseitin encoding
  ;; which may add additional binary variables)

  ;;(Format t "All vars: ~a~%" all-variables)
  ;; TODO Fixup below. It helps a lot with Anytime Bounded BDD Construction,
  ;; because it tends to put the theta variables first.
  (setf all-variables (reverse all-variables))

  (let* ((sat (encode-constraints-as-sat-cnf constraints all-variables))
         (variables (loop for i from 1 to (1- (length (sat-encoding-id->sat-variable sat))) collecting (aref (sat-encoding-id->sat-variable sat) i)))

         (edges nil) ;; Will contain edges, which are lists of variables (we don't care about positive / negative)
         (variable->edges (make-hash-table :test #'eql)) ;; Mapping from each variable to the list of edges it is represented in
         (position (make-hash-table :test #'eql)) ;; Positions of variables in ordering (1 dimensional position)
         (cog (make-hash-table :test #'eql)) ;; "Center of gravity" of the hyperedges
         (positions-tentative nil) ;; List of tuples <variable, pos-tentative>
         (positions-current nil) ;; Current linearized set of positions
         (positions-last nil)) ;; Last linearized generated set of positions.


    ;; Helper function to compute the average edge span
    (flet ((compute-average-edge-span ()
             (let ((total-span 0))
               (dolist (e edges)
                 (let (e-indices)
                   (setf e-indices (mapcar #'(lambda (v) (gethash v position)) e))
                   (incf total-span
                         (- (apply #'max e-indices)
                            (apply #'min e-indices)))))
               (/ total-span (length edges)))))

      ;; Construct the "hypergraph" whose vertices are variables (either Tseitin-type SAT variables, or finite-domain decision variables), and whose edges
      ;; are the clauses (ignoring positive / negative). Keep track of the edges associated with
      ;; each variable, and the variables associated with each edge.

      ;; Update the variables to take into account decision variables.

      (setf variables (remove-duplicates (mapcar #'(lambda (v)
                                                     (if (typep (sat-variable-tag v) 'assignment-constraint)
                                                         (assignment-variable (assignment-constraint-assignment (sat-variable-tag v)))
                                                         v))
                                                 variables)))

      (dolist (clause (clauses sat))
        (let ((e (remove-duplicates (mapcar #'(lambda (v)
                                                (if (typep (sat-variable-tag v) 'assignment-constraint)
                                                    (assignment-variable (assignment-constraint-assignment (sat-variable-tag v)))
                                                    v))
                                            (mapcar #'sat-literal-variable clause)))))
          (push e edges)
          (dolist (v e)
            (push e (gethash v variable->edges)))))

      ;; Shuffle!
      ;; (setf variables (sort variables #'(lambda (x y) (declare (ignore x y)) (> (- (random 1.00) 0.50) 0))))

      ;; Assign an initial position to each in the hash table
      (let ((i 0))
        (dolist (v variables)
          (setf (gethash v position)
                (incf i))))
      (setf positions-last variables)

      ;; (format t "Initial Order: ~a~%" (mapcar #'first (sort (mapcar #'(lambda (v) `(,v ,(gethash v position))) variables) #'< :key #'second)))
      ;; (format t "Initial average span: ~a~%" (float (compute-average-edge-span)))
      (block force-iterate
        (dotimes (i (* N-iterations-factor (length edges)))

          ;; Compute the COG (center of gravity) for each edge
          (dolist (e edges)
            (setf (gethash e cog)
                  (/ (apply #'+ (mapcar #'(lambda (v)
                                            (gethash v position))
                                        e))
                     (length e))))

          ;; Compute new tentative positions for each variable
          (setf positions-tentative nil)
          (dolist (v variables)
            (let* ((E-v (gethash v variable->edges))
                   (pos-updated (/  (apply #'+ (mapcar #'(lambda (e)
                                                           (gethash e cog))
                                                       E-v))
                                    (length E-v))))
              (push `(,v ,pos-updated) positions-tentative)))

          ;; Sort the new positions tentative, and copy over to positions (making integer again)
          (setf positions-tentative (stable-sort positions-tentative #'< :key #'second))
          (let ((i 0))
            (dolist (m positions-tentative)
              (setf (gethash (first m) position) (incf i))))
          (setf positions-last positions-current)
          (setf positions-current (mapcar #'first positions-tentative))

          ;; (format t "Order: ~a~%" (mapcar #'first (sort (mapcar #'(lambda (v) `(,v ,(gethash v position))) variables) #'< :key #'second)))
          ;; (format t "Intermediate average span (~a): ~a~%" i (float (compute-average-edge-span)))

          ;; Possible early termination: if we've reached a fixed point!
          (when (equal positions-current positions-last)
            ;; (format t "Early termination!~%")
            (return))))

      ;; Compute the final order
      (let (final-order)
        (dolist (v positions-current)
          (cond
            ((typep v 'decision-variable)
             (cond
               ((and use-binaries (= 2 (length (decision-variable-values v))))
                (push (assignment v (first (decision-variable-values v))) final-order))
               (t
                (dolist (val (decision-variable-values v))
                  (push (assignment v val) final-order)))))

            ((typep v 'sat-variable)
             t) ;; Do nothing!

            (t
             (error "Invalid variable type!"))))

        ;;(format t "Final order:~%")
        ;; (dolist (v final-order)
        ;;   (format t "  ~a~%" v))
        (setf final-order (reverse final-order))
        (format t "Force BDD variable order heuristic (clustered): final average edge span ~a~%" (float (compute-average-edge-span)))
        final-order))))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Procedures for the Incremental, best-first, approximate BDD-TMS.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



(defun bdd-begin-apply (bm operands)
  "Set up an incremental apply operation."
  (with-slots (op-unique-table node-ops queue-ops n-iterations node->heuristic) bm
    ;; Initialize the priority queue
    (setf queue-ops (pileup:make-heap #'>
                                      :name "Operation priority queue"
                                      :key #'(lambda (n)
                                               (gethash (bdd-node-id n) node->heuristic))))
    ;; Set up the heuristic function
    ;; (if (typep heuristic-fn 'function)
    ;;     (setf bdd-op-heuristic-fn heuristic-fn)
    ;;     (setf bdd-op-heuristic-fn #'(lambda (n-op)
    ;;                                   (declare (optimize (speed 3))
    ;;                                            (type bdd-op-node n-op))
    ;;                                   (bdd-wmc-heuristic bm n-op))))

    (setf n-iterations 0)
    ;; Simplify the given set of operands
    (let* ((F (sort-nodes-by-id (bdd-simplify-and bm operands)))
           (key `(:and ,@(mapcar #'bdd-node-id F))))
      (cond
        ;; If we have a single node, no need to use an op! We're done. Just record key.
        ((= 1 (length F))
         (let ((n-result (first F)))
           (setf (gethash key op-unique-table) n-result)
           (push key (gethash (bdd-node-id n-result) node-ops))))

        ;; If not a single node, set up an op.
        (t
         (let ((n-result (get-bdd-op-or-result bm :and F)))
           (when (typep n-result 'bdd-op-node)
             (setf (gethash (bdd-node-id n-result) node->heuristic)
                   (bdd-wmc-heuristic bm n-result))
             (pileup:heap-insert n-result queue-ops)))))

      ;; Return a key for which the operation can be tracked.
      key)))


(defun bdd-retrieve-current-approximation (bm key)
  (with-slots (op-unique-table) bm
    (gethash key op-unique-table)))


(defun bdd-apply-once (bm)
  "Queue-based apply operation! Go!"
  (declare (optimize (speed 3))
   (type bdd-manager bm))
  (with-slots (op-unique-table queue-ops variable->index node->heuristic n-iterations computed-table-approx) bm

    (let (n-op op F found?)
      ;; Keep popping off of the queue until we find an operation
      ;; that hasn't already been resolved to a (partial) solution
      (loop
         do
           (setf n-op (pileup:heap-pop queue-ops))
           (setf op (bdd-op-node-operation n-op))
           (setf F (bdd-op-node-operands n-op))
           (multiple-value-setq (n-op found?) (gethash `(,op ,@(mapcar #'bdd-node-id F)) op-unique-table))
         while (and found?
                    (not (typep n-op 'bdd-op-node))
                    (not (pileup:heap-empty-p queue-ops))))
      ;; If no more ops, then return!
      (unless (typep n-op 'bdd-op-node)
        (return-from bdd-apply-once nil))

      ;; Select the earliest variable amongst Var(f) for f in F
      (let ((x-min (argmin (mapcar #'bdd-node-variable F) #'< #'(lambda (v) (gethash v variable->index))))
            F-low F-high n-low n-high n-result R)
        ;; Compute simplified high and low sets Shannon cofactors for each f in F.
        (dolist (f-i F)
          (cond
            ;; f-i is split on x-min. Shannon co-factors are the children.
            ((eql x-min (bdd-node-variable f-i))
             (push (bdd-node-child-low f-i) F-low)
             (push (bdd-node-child-high f-i) F-high))
            ;; f-i split on a variable lower than x-min. Shannon co-factor is node itself.
            (t
             (push f-i F-low)
             (push f-i F-high))))
        ;; Simplify the sets of low and high foctors.
        (setf F-low (bdd-simplify-and bm F-low))
        (setf F-high (bdd-simplify-and bm F-high))
        ;; Select n-low: either a new op node, or a result.
        (cond
          ((= 1 (length F-low))
           (setf n-low (first F-low)))
          (t
           (setf n-low (get-bdd-op-or-result bm op F-low :record-parents t))))
        ;; Select n-high: either a new op node, or a result.
        (cond
          ((= 1 (length F-high))
           (setf n-high (first F-high)))
          (t
           (setf n-high (get-bdd-op-or-result bm op F-high :record-parents t))))
        ;; Now that we have n-low and n-high, construct a new result node
        (cond
          ((eql n-high n-low)
           (setf n-result n-low))
          (t
           (setf n-result (get-bdd-node bm x-min n-low n-high :record-parents t))))


        ;; Replace n-op with n-result, and do any necessary reductions.
        (setf R (bdd-replace bm n-op n-result))
        (bdd-reduce bm R)

        (when (typep n-low 'bdd-op-node)
          (setf (gethash (bdd-node-id n-low) node->heuristic)
                (bdd-wmc-heuristic bm n-low))
          (pileup:heap-insert n-low queue-ops))

        (when (typep n-high 'bdd-op-node)
          (setf (gethash (bdd-node-id n-high) node->heuristic)
                (bdd-wmc-heuristic bm n-high))
          (pileup:heap-insert n-high queue-ops))

        ;; Optimization: allows approximations to be cached. Clear the ancestors
        ;; from the approx table for correct results.
        ;; TODO: Do we need this? Check other places. We don't need this so long as
        ;; either (1) we don't cache approximations, or (2) we clear the cache between
        ;; each call to approximations (but still cache within each call). We do (2),
        ;; so below can safely be commented out.
        ;; (dolist (ancestor (bdd-get-all-ancestors bm `(,n-result)))
        ;;   (remhash `(,(bdd-node-id ancestor) 0) computed-table-approx)
        ;;   (remhash `(,(bdd-node-id ancestor) 1) computed-table-approx))

        ;; Keep track of how many iterations
        (incf n-iterations)))))


(defun bdd-wmc-heuristic (bm n-op &key (node-bn nil))
  "The heuristic for the BDD operation priority queue."
  (declare (optimize (speed 3))
           (type bdd-manager bm)
           (type bdd-op-node n-op))

  (let (h-final
        h-dfs
        h-bfs
        h-min-var
        h-max-var
        h-mean-var
        h-parent
        h-frechet-lb
        h-frechet-ub
        h-wmc-proj-and
        h-max-wmc-and
        h-path-single
        h-full-path
        h-max-subtheory)

    ;; TODO: Use a real heuristic here!

    ;; DFS heuristic
    ;; (setf h-dfs (bdd-wmc-heuristic-dfs bm n-op))

    ;; BFS heuristic
    ;; (setf h-bfs (bdd-wmc-heuristic-bfs bm n-op))

    ;; Min indexed variable
    ;;(setf h-min-var (bdd-wmc-heuristic-min-child-index bm n-op))

    ;; Max index variable -- TODO always seems to be constant??
    ;;(setf h-max-var (bdd-wmc-heuristic-max-child-index bm n-op))

    ;; Mean variable index
    ;; (setf h-mean-var (/ (bdd-wmc-heuristic-mean-child-index bm n-op)
    ;;                     (length (bdd-manager-index->variable bm))))

    ;; Parent count
    ;;(setf h-parent (bdd-wmc-heuristic-parent-count bm n-op))

    ;; WMC-type bounds
    ;; Frechet lowerbound
    ;;(setf h-frechet-lb (bdd-wmc-heuristic-frechet-lowerbound bm n-op node-bn))
    ;; Frechet upperbound
    ;;(setf h-frechet-ub (bdd-wmc-heuristic-frechet-upperbound bm n-op node-bn))

    ;; Maximum possible over a subtheory from x_min, ..., x_N
    ;;(setf h-max-subtheory (bdd-wmc-heuristic-highest-possible-subtheory-wmc bm n-op))




    ;; Good one theoretically: optimistic bound of projection of conjunction
    ;; (setf h-wmc-proj-and (bdd-wmc-heuristic-wmc-proj-conjunction-approx bm n-op)) ;; ProjWMC
    (setf h-max-wmc-and (bdd-wmc-heuristic-max-wmc-conjunction-approx bm n-op)) ;; MaxWMC

    ;; Single path trace
    (setf h-path-single (bdd-wmc-heuristic-path-trace-single-to-root bm n-op))

    ;; Full path
    ;; (setf h-full-path (* h-path-single h-wmc-proj-and))
    (setf h-full-path (* h-path-single h-max-wmc-and))

    ;; Some weighted combination?
    (setf h-final h-full-path)

    ;; (setf h-final (+ (* 1 (* h-path-single h-wmc-proj-and))
    ;;                  (* 1.2 h-mean-var)))

    ;; (setf h-final h-dfs)
    ;; (format t "h(~a) = ~a~%" n-op (coerce h-final 'float))
    (the number h-final)))


(defun bdd-wmc-heuristic-dfs (bm n-op)
  "Implements depth-first search (DFS). QUeue is effectively
a stack."
  (declare (optimize (speed 3))
           (type bdd-manager bm)
           (ignore n-op))
  (with-slots (n-iterations) bm
    n-iterations))

(defun bdd-wmc-heuristic-bfs (bm n-op)
  "Implements breadth-first search (BFS). Queue is effectively a
list."
  (declare (optimize (speed 3))
           (type bdd-manager bm)
           (ignore n-op))
  (with-slots (n-iterations) bm
    (- n-iterations)))

(defun bdd-wmc-heuristic-min-child-index (bm n-op)
  "Takes the min over child variable ID's."
  (declare (optimize (speed 3))
           (type bdd-manager bm)
           (type bdd-op-node n-op))
  (with-slots (variable->index) bm
    (apply #'min (mapcar #'(lambda (f-i)
                             (declare (type bdd-node f-i))
                             (gethash (bdd-node-variable f-i) variable->index))
                         (bdd-op-node-operands n-op)))))

(defun bdd-wmc-heuristic-max-child-index (bm n-op)
  "Takes the max over child variable ID's."
  (declare (optimize (speed 3))
           (type bdd-manager bm)
           (type bdd-op-node n-op))
  (with-slots (variable->index) bm
    (apply #'max (mapcar #'(lambda (f-i)
                             (declare (type bdd-node f-i))
                             (gethash (bdd-node-variable f-i) variable->index))
                         (bdd-op-node-operands n-op)))))

(defun bdd-wmc-heuristic-mean-child-index (bm n-op)
  "A heuristic that takes the average of the operand variable ID's."
  (declare (optimize (speed 3))
           (type bdd-manager bm)
           (type bdd-op-node n-op))
  (with-slots (variable->index) bm
    (/ (apply #'+ (mapcar #'(lambda (f-i)
                             (declare (type bdd-node f-i))
                             (gethash (bdd-node-variable f-i) variable->index))
                          (bdd-op-node-operands n-op)))
       (length (bdd-op-node-operands n-op)))))


(defun bdd-wmc-heuristic-parent-count (bm n-op)
  "Heuristic that counts the number of parents of this node. The idea is
that a node with more parents may be more influential, and thus should be
expanded sooner."
  (declare (optimize (speed 3))
           (type bdd-manager bm)
           (type bdd-op-node n-op))
  (with-slots (parents) bm
    (length (gethash (bdd-node-id n-op) parents))))


(defun bdd-wmc-heuristic-frechet-lowerbound (bm n-op n-bn)
  "Frechet lowerbound. Requires n-bn to be given."
  (declare (optimize (speed 3))
           (type bdd-manager bm)
           (type bdd-op-node n-op))
  (coerce (max 0
               (+ (- 1 (length (bdd-op-node-operands n-op)))
                  (apply #'+ (mapcar #'(lambda (f-i)
                                         (bdd-wmc bm
                                                  (bdd-project bm (bdd-and bm
                                                                           f-i
                                                                           n-bn))))
                                     (bdd-op-node-operands n-op)))))
          'float))


(defun bdd-wmc-heuristic-frechet-upperbound (bm n-op n-bn)
  "Frechet upperbound. Requires n-bn to be given."
  (declare (optimize (speed 3))
           (type bdd-manager bm)
           (type bdd-op-node n-op))

  (apply #'min (mapcar #'(lambda (f-i)

                           (let (wmc)
                             (setf wmc (bdd-wmc bm
                                                (bdd-project bm
                                                             (bdd-and bm f-i n-bn)))) ;; TODO: Which one?? This or just f-i ? I think this one.
                             wmc))
                       (bdd-op-node-operands n-op))))


(defun bdd-wmc-heuristic-wmc-proj-conjunction-approx (bm n-op)
  "An optimistic admissible upperbound on the WMC of an and node.
See my resarch notebook for details."
  (declare (optimize (speed 3))
           (type bdd-manager bm)
           (type bdd-op-node n-op))
  (with-slots (variable->index index->variable index->projected-onto?) bm
    ;; Find the minimal variable index over all the operands. The conjunction can be viewed as a subtheory
    ;; over variables {x_{i_min}, ..., x_N}.
    (let ((i-var-min (the fixnum (apply #'min (mapcar #'(lambda (f-i)
                                                          (gethash (bdd-node-variable f-i) variable->index))
                                                      (bdd-op-node-operands n-op)))))
          (wmcs_subtheory nil))
      ;; (format t "~a: ~{~a,~^~}~%" i-var-min (mapcar #'(lambda (f-i)
      ;;                                                   (gethash (bdd-node-variable f-i) variable->index))
      ;;                                               (bdd-op-node-operands n-op)))
      (dolist (f-i (bdd-op-node-operands n-op))
        (let ((i-var (the fixnum (gethash (bdd-node-variable f-i) variable->index)))
              (prod (bdd-wmc-compute-alpha bm
                                           (bdd-project bm f-i))))


          (loop for i from i-var-min to (1- i-var) do
               (when (aref index->projected-onto? i)
                 (setf prod (* prod
                               (+ (bdd-wmc-get-weight bm (aref index->variable i) t)
                                  (bdd-wmc-get-weight bm (aref index->variable i) nil))))))

          (push prod wmcs_subtheory)))
      (apply #'min wmcs_subtheory))))


(defun bdd-wmc-heuristic-max-wmc-conjunction-approx (bm n-op)
  "An optimistic admissible upperbound on the WMC of an and node.
See my resarch notebook for details."
  (declare (optimize (speed 3))
           (type bdd-manager bm)
           (type bdd-op-node n-op))
  (with-slots (variable->index index->variable index->projected-onto?) bm
    ;; Find the minimal variable index over all the operands. The conjunction can be viewed as a subtheory
    ;; over variables {x_{i_min}, ..., x_N}.
    (let ((i-var-min (the fixnum (apply #'min (mapcar #'(lambda (f-i)
                                                          (gethash (bdd-node-variable f-i) variable->index))
                                                      (bdd-op-node-operands n-op)))))
          (wmcs_subtheory nil))
      ;; (format t "~a: ~{~a,~^~}~%" i-var-min (mapcar #'(lambda (f-i)
      ;;                                                   (gethash (bdd-node-variable f-i) variable->index))
      ;;                                               (bdd-op-node-operands n-op)))
      (dolist (f-i (bdd-op-node-operands n-op))
        (let ((i-var (the fixnum (gethash (bdd-node-variable f-i) variable->index)))
              (prod (bdd-wmc-compute-beta bm f-i)))

          ;; (format t "Done for ~a~%" f-i)
          (loop for i from i-var-min to (1- i-var) do
               (when (aref index->projected-onto? i)
                 (setf prod (* prod
                               (+ (bdd-wmc-get-weight bm (aref index->variable i) t)
                                  (bdd-wmc-get-weight bm (aref index->variable i) nil))))))

          (push prod wmcs_subtheory)))
      (apply #'min wmcs_subtheory))))


(defun bdd-wmc-heuristic-path-trace-single-to-root (bm n-op)
  "A path heuristic based on taking a single path (as opposed to the sum
over all paths) from the given op node to the root. Along the path, takes the
weight of any chosen literals, and for intermediate literals takes both. For variables
being projected away, just uses a weight of 1."
  ;; TODO: This procedure starts from the op node, and traces the parents upwards to a root.
  ;; What happens if we don't reach "the" root of the function we're trying to compute??
  ;; As a result, I believe it may be possible that this procedure may not always return the
  ;; correct result. Further testing should be performed.
  (declare (optimize (speed 3))
           (type bdd-manager bm)
           (type bdd-op-node n-op))
  (with-slots (index->projected-onto? variable->index index->variable parents) bm
    (let ((path-prod 1)
          (n-current n-op)
          (n-last n-op)
          (i-var-current -1)
          ;; Keep track of the last variable index. Initially start with the min
          ;; variable of the and node.
          (i-var-last (the fixnum (apply #'min (mapcar #'(lambda (f-i)
                                                           (gethash (bdd-node-variable f-i) variable->index))
                                                       (bdd-op-node-operands n-op))))))

      (loop while (setf n-current (first (gethash (bdd-node-id n-current) parents))) do
         ;; We have an ancestor. If this ancestor is one of the variables we're projecting
         ;; onto, then multiply in the appropriate weight to the child -- depending on the
         ;; low versus high branch. Otherwise if it's a variable being projected away, don't
         ;; multiply in any weight (so effectively 1). Also, multiply in both weights for any
         ;; intermediate variables that are being projected onto, similar to the compute alpha case.
           (setf i-var-current (the fixnum (gethash (bdd-node-variable n-current) variable->index)))
           (when (aref index->projected-onto? i-var-current)
             ;; Multiply in the appropriate weight
             (cond
               ((eql n-last (bdd-node-child-low n-current))
                (setf path-prod (* path-prod
                                   (bdd-wmc-get-weight bm (aref index->variable i-var-current) nil))))
               ((eql n-last (bdd-node-child-high n-current))
                (setf path-prod (* path-prod
                                   (bdd-wmc-get-weight bm (aref index->variable i-var-current) t))))))

         ;; Multiply in the intermediate ones
           (loop for i from (1+ i-var-current) to (1- i-var-last) do
                (when (aref index->projected-onto? i)
                  (setf path-prod (* path-prod
                                     (+ (bdd-wmc-get-weight bm (aref index->variable i) t)
                                        (bdd-wmc-get-weight bm (aref index->variable i) nil))))))



         ;; Update for the next iteration
           (setf i-var-last i-var-current)
           (setf n-last n-current))

      path-prod)))


(defun bdd-wmc-heuristic-path-trace-all-to-root (bm n-op)
  "A path heuristic based on taking all paths from the given op node to the root. Along the path, takes the
weight of any chosen literals, and for intermediate literals takes both. For variables
being projected away, just uses a weight of 1."
  (declare (optimize (speed 3))
           (type bdd-manager bm)
           (type bdd-op-node n-op))

  1)


(defun compute-sigma (bm n node->sigma)

  (with-slots (index->projected-onto? variable->index index->variable parents) bm

    ;; If this is the root node / no parents ...

    ;; But if it's not "the" root node??

    1))


(defun bdd-wmc-heuristic-highest-possible-subtheory-wmc (bm n-op)
  "A heuristic that optimistically assumes the highest possible projected
weighted model count for the n-op node. Treats n-op as some arbitrary subtheory
starting from x_min (over all f-i operands of n-op). Then computers the highest
possible projected WMC that would be possible over any such subtheory over those
varaibles -- namely, assumes that all possible combinations
of the projected variables can hold together -- and takes the WMC."
  (declare (optimize (speed 3))
           (type bdd-manager bm)
           (type bdd-op-node n-op))

  (with-slots (index->projected-onto? index->variable variable->index) bm
    (let ((prod 1)
          (i-var-min (the fixnum (apply #'min (mapcar #'(lambda (f-i)
                                                          (gethash (bdd-node-variable f-i) variable->index))
                                                      (bdd-op-node-operands n-op)))))
          (i-var-max (the fixnum (- (length index->variable) 3))))

      (loop for i from i-var-min to i-var-max do
           (when (aref index->projected-onto? i)
             (setf prod (* prod
                           (+ (bdd-wmc-get-weight bm (aref index->variable i) t)
                              (bdd-wmc-get-weight bm (aref index->variable i) nil))))))
      prod)))


(defun bdd-reduce (bm nodes)
  "Continually reduce!"
  (declare (optimize (speed 3)))
  (declare (type bdd-manager bm))
  ;;(format t "Reduce: **************************~%")
  ;;(format t "Reduce: Starting with ~a~%" nodes)
  (with-slots (op-unique-table unique-table variable->index) bm
    ;; Create a priority queue ordered in *decreasing* variable order
    ;; (so, bottom-most nodes are popped first)
    (let ((Q (pileup:make-heap #'>
                               :name "Node priority queue"
                               :key (lambda (v)
                                      (gethash (bdd-node-variable v) variable->index)))))
      ;; Add the nodes to the priority queue
      (dolist (n nodes)
        (when (not (typep n 'bdd-op-node))
          (pileup:heap-insert n Q)))

      ;; Keep reducing until nothing left to reduce
      (loop while (not (pileup:heap-empty-p Q)) do
           (block reduce-iteration
             (let ((n (pileup:heap-pop Q))
                   R)
               ;;(format t "Reduce: Reducing ~a~%" n)
               ;; Check different reduction conditions
               (with-slots (variable child-low child-high) n
                 ;; Does Low(n) == High(n)?
                 (when (eql child-low child-high)
                   (setf R (bdd-replace bm n child-low))
                   (dolist (n-r R)
                     (when (not (typep n-r 'bdd-op-node))
                       (pileup:heap-insert n-r Q)))
                   (return-from reduce-iteration))
                 ;; Is there an existing and different canonical version of this node
                 ;; already?
                 (let ((key (if (and (typep child-low 'bdd-node) (typep child-high 'bdd-node))
                                `(,variable ,(bdd-node-id child-low) ,(bdd-node-id child-high))
                                `(,variable ,child-low ,child-high)))
                       n-canonical found?)
                   (multiple-value-setq (n-canonical found?) (gethash key unique-table))

                   (cond
                     (found?
                      (when (not (eql n-canonical n))
                        (setf R (bdd-replace bm n n-canonical))
                        (dolist (n-r R)
                          (when (not (typep n-r 'bdd-op-node))
                            (pileup:heap-insert n-r Q)))))

                     (t
                      (setf (gethash key unique-table) n)))))))))))


(defun bdd-replace (bm node-from node-to)
  "Replace a BDD node with another node, in our incrementally generated
BDD result. Returns a list of nodes that may now need to be reduced. Updates
other datastructures appropriately"
  (declare (optimize (debug 3)))
  ;;(format t "Replace: Replacing ~a --> ~a~%" node-from node-to)
  (with-slots (parents node-ops op-unique-table computed-table-alpha computed-table-projection) bm
    ;; First, update all parents
    (let ((R (gethash (bdd-node-id node-from) parents)))
      ;; (format t "Replace: Parents of ~a: ~a~%" node-from R)
      (dolist (parent (gethash (bdd-node-id node-from) parents))
        ;; Update the pointer appropriately
        (cond
          ;; An OP node
          ((typep parent 'bdd-op-node)
           (with-slots (operands) parent
             (setf operands (remove node-from operands))
             (push node-to operands)))

          ;; A normal BDD node
          (t
           (with-slots (child-low child-high) parent
             (cond
               ((eql node-from child-low) (setf child-low node-to))
               ((eql node-from child-high) (setf child-high node-to))))))

        ;; node-from no longer has this parent, node-to does
        (setf (gethash (bdd-node-id node-from) parents) (remove parent (gethash (bdd-node-id node-from) parents)))
        (push parent (gethash (bdd-node-id node-to) parents)))

      ;; Update other datastructures, such as Op-Unique-Tabel, and NodeOps
      (dolist (key (gethash (bdd-node-id node-from) node-ops))
        (setf (gethash (bdd-node-id node-from) node-ops) (remove key (gethash (bdd-node-id node-from) node-ops)))
        (push key (gethash (bdd-node-id node-to) node-ops))
        (setf (gethash key op-unique-table) node-to))

      ;; TODO: Invalidate other things, liked cached WMC's, projections, etc.
      ;;(format t "Ancestors of ~a: ~a~%" node-to (bdd-get-all-ancestors bm node-to))
      ;; TODO: This can be made more efficient I bet. Not doing duplicates up the tree for many calls to replace with reduce, etc.
      ;; Note 6/18/2018: The below can safely be commented out, due to the following justification. We would only need to clear
      ;; the computed table alpha or projections, of we re-arrange part of an existing tree and then calL WMC or projection
      ;; on that changed tree. However, in our implementation of the anytime bounded BDD, this never occurs. We always
      ;; get an approximation, which returns a canonical BDD that will never be "updated". When we get different approximations
      ;; later on, we're not changing the existing approximation -- we're generating a new one, with new nodes -- thanks to
      ;; calling get-bdd-node etc. We only ever call projections and WMC on those canonical approximations, so the below
      ;; can be safely commented out in order to improve performance.
      ;; (dolist (ancestor (bdd-get-all-ancestors bm `(,node-to)))
      ;;   ;; alpha values for WMC and projections
      ;;   (remhash (bdd-node-id ancestor) computed-table-alpha)
      ;;   (remhash (bdd-node-id ancestor) computed-table-projection))


      ;; Return the list of parents, which should be reduced.
      R)))


(defun get-bdd-op-or-result (bm op operands &key (record-parents t))
  "Look up an op on the given operands in the operator / unique table.
Returns either an op node, or a normal BDD node representing the result.
note that the BDD node may not be fully reduced, and could change later!"
  (declare (optimize (speed 3)))
  (declare (type bdd-manager bm))
  (with-slots (op-unique-table node-ops num-nodes nodes parents) bm
    (let* ((F (sort-nodes-by-id operands))
           (key `(,op ,@(mapcar #'bdd-node-id F)))
           n-result found?)
      (multiple-value-setq (n-result found?) (gethash key op-unique-table))
      ;; Does this key exist in the table? If so, return the result.
      (when found?
        ;; (format t "Op unique table cache hit!~%")
        (return-from get-bdd-op-or-result (the bdd-node n-result)))
      ;; Does not exist, so create a new op node.
      (setf n-result (make-bdd-op-node
                      :id num-nodes
                      :variable nil
                      :child-low nil
                      :child-high nil
                      :operation op
                      :operands F))
      (setf num-nodes (+ num-nodes 1))
      ;; (push n-result nodes)
      ;; Possibly record parents
      (when record-parents
        (dolist (operand operands)
          (push n-result (gethash (bdd-node-id operand) parents))))
      ;; Store this new node in the op-unique-table
      (setf (gethash key op-unique-table) n-result)
      (push key (gethash (bdd-node-id n-result) node-ops))
      ;; Return the result
      (the bdd-node n-result))))


(defun bdd-simplify-and (bm operands)
  "Return a simplified set of operands for the logical AND function! Returns a copy."
  (declare (optimize (speed 3)))
  ;; If any of the nodes are the terminal 0, return {0}
  (when (some #'(lambda (n)
                  (bdd-node-terminal? n :value 0))
              operands)
    (return-from bdd-simplify-and (list (get-bdd-terminal bm 0))))
  ;; Remove any duplicates
  (setf operands (remove-duplicates operands))
  ;; Remove any terminal 1's, which wouldn't change the conjunction
  (setf operands (remove-if #'(lambda (n)
                                (bdd-node-terminal? n :value 1))
                            operands))
  ;; If the set of operands is now empty, it must be vacuously true. Return 1.
  ;; Otherwise, return the simplified set.
  (if (eql nil operands)
      (list (get-bdd-terminal bm 1))
      operands))


(defun bdd-compute-underapproximation (bm key)
  "Compute an overapproximation of the solution BDD, n_overrapprox.
That is, n_final |= o_overapprox. Approximates by setting any op
nodes to 1."
  (declare (optimize (speed 3))
           (type bdd-manager bm))
  (with-slots (op-unique-table computed-table-approx) bm

    ;; TODO: Does this work?
    (clrhash computed-table-approx)

    (bdd-compute-approximation bm
                               (gethash key op-unique-table)
                               (get-bdd-terminal bm 0))))




(defun bdd-compute-overapproximation (bm key)
  "Compute an overapproximation of the solution BDD, n_overrapprox.
That is, n_final |= o_overapprox. Approximates by setting any op
nodes to 1."
  (declare (optimize (speed 3))
           (type bdd-manager bm))
  (with-slots (op-unique-table computed-table-approx) bm

    ;; TODO: Does this work?
    (clrhash computed-table-approx)

    (bdd-compute-approximation bm
                               (gethash key op-unique-table)
                               (get-bdd-terminal bm 1))))


(defun bdd-compute-approximation (bm n op-sub-node)
  "The key function to compute BDD approximations. Substitue all op
nodes for op-sub-node, and reduce the result. Computes by traveling
down the BDD, then back up reducing."
  (declare (optimize (speed 3))
           (type bdd-manager bm)
           (type bdd-node op-sub-node n))

  (with-slots (computed-table-approx) bm
    ;; If we've reached a terminal node, simply return it.
    (when (bdd-node-terminal? n)
      (return-from bdd-compute-approximation n))

    ;; If we've reached an op node, return the approximation.
    (when (typep n 'bdd-op-node)
      (return-from bdd-compute-approximation op-sub-node))

    ;; Otherwise, check the cache.
    (let ((key `(,(bdd-node-id n) ,(bdd-node-id op-sub-node)))
          result found?)
      ;; Check if it's already in the computed cache
      (multiple-value-setq (result found?) (gethash key computed-table-approx))
      (when found?
        ;; (format t "Approx cache hit!~%")
        (return-from bdd-compute-approximation result))

      ;; Compute recursively n_low_approx and n_high_approx
      (let ((n-low-approx (bdd-compute-approximation bm (bdd-node-child-low n) op-sub-node))
            (n-high-approx (bdd-compute-approximation bm (bdd-node-child-high n) op-sub-node))
            n-result)
        (declare (type bdd-node n-low-approx n-high-approx))


        (cond
          ;; If they're the same, just return one.
          ((eql n-low-approx n-high-approx)
           (setf n-result n-low-approx))
          ;; Otherwise return the appropriate node using get-bdd-node!
          (t
           (setf n-result (get-bdd-node bm
                                        (bdd-node-variable n)
                                        n-low-approx
                                        n-high-approx))))

        ;; Store the result in the cache
        (setf (gethash key computed-table-approx) n-result)

        ;; Return the result
        n-result))))



(defun sort-nodes-by-id (nodes)
  "Helper function to sort a bunch of nodes by their ID"
  (declare (optimize (speed 3))
           (type list nodes))
  (sort (copy-seq nodes) #'< :key #'bdd-node-id))
