;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
;;;
;;; A highly-optmized, bit vector-based ATMS implementation
;;;
;;; Note that there may be algorithmic improvements possible (see BPS and papers) to make it even faster.
;;;
;;;
;;; Note that we explicitly use structs instead of classes, for better type inference
;;; by some compilers (ex., SBCL).
(in-package #:pike)


(defconstant +inf+ float-features:double-float-positive-infinity
  "The value used to represent infinity when parsing numbers.")

(defconstant +-inf+ float-features:double-float-negative-infinity
  "The value used to represent -infinity when parsing numbers.")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Macros - various useful macros
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmacro do-pairs (variables-and-list &rest body)
  "A macro to easily loop over pairs in a list, order doesn't matter.
   If L is a list like (list 1 2 3 4), you can use this
   macro with something like:

  (do-pairs (c1 c2 L)
    (print (list c1 c2)))

  which will result in pairs being printed out like:
  (1 2), (1 3), (1 4), (2 3), (2 4), (3 4). Will
  iterate over all N choose 2 pairs, assuming that
  order doesn't matter (and no 'identify' pairs, like (1 1))."
  (let ((v1 (first variables-and-list))
        (v2 (second variables-and-list))
        (l (third variables-and-list))
        (x1 (gensym))
        (x2 (gensym)))
    `(do ((,x1 ,l (cdr ,x1)))
         ((eql ,x1 nil) T)
       (do ((,x2 (cdr ,x1) (cdr ,x2)))
           ((eql ,x2 nil) T)
         (let ((,v1 (car ,x1))
               (,v2 (car ,x2)))
           ,@body)))))


(defmacro do-pairs-ordered (variables-and-list &rest body)
  "A macro to easily loop over pairs in a list, order matters.
   If L is a list like (list 1 2 3), you can use this
   macro with something like:

  (do-pairs-ordered (c1 c2 L)
    (print (list c1 c2)))

  which will result in pairs being printed out like:
  (1 2), (1 3), (2 1), (2 3), (3 1), (3 2). Will
  iterate over all N choose 2 pairs, assuming that
  order doesn't matter (and no 'identify' pairs, like (1 1))."
  (let ((v1 (first variables-and-list))
        (v2 (second variables-and-list))
        (lv (third variables-and-list))
        (l (gensym)))
    `(let ((,l ,lv))
       (dolist (,v1 ,l)
         (dolist (,v2 ,l)
           (unless (eq ,v1 ,v2)
             ,@body))))))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Debug macros
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defmacro when-debug ((ps) &rest body)
  `(when (pike-debug-mode (pike-options ,ps))
     ,@body))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Removing one element from a list, and letting you know if it was removed.
;; Note: maybe modify the list!!
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun delete-one-from-list-and-tell! (item l-orig &key (test #'eql))
  "Delete the given item from the given list, using the given test function.
   Deletes at most once, and returns a new pointer to the start of the list.
   Also returns a second value: t if anything was removed, nil otherwise."
  (let (l-new deleted?)
    ;; Works under the assumption that delete-if will only call it's predicate
    ;; sequentially until it finds something to delete.
    (setf l-new (delete-if #'(lambda (x)
                               (setf deleted? (funcall test x item)))
                           l-orig
                           :count 1))
    (values l-new deleted?)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Check and see if two lists, as sets, are equal.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun sets-equal (c1 c2 &key (test #'eql))
  "Helper function to check if two lists c1, and c2,
  are equal as sets (they contain the same elements). Checks
  the equality of elements with eql by default (same actual object)"
  (null (set-exclusive-or c1 c2 :test test)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Take the cartesian product of a list of lists.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun cartesian-product (lists)
  "Take the cartesian product of a list of lists.

  In terms of ordering of results, it follows sort of a binary counting
type pattern. The first list changes the most slowly, moving more and more quickly
to the last list."
  (let ((cross-products `(()))
        (lists-remaining (copy-seq lists)))
    (loop while (> (length lists-remaining) 0) do
         (let ((l (pop lists-remaining))
               cross-temp)
           (dolist (c cross-products)
             (dolist (li l)
               (push `(,@c ,li) cross-temp)))
           (setf cross-products (nreverse cross-temp))))
    cross-products))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Pretty, colorized printing
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defvar *colorize-output-mode* :none)
;; Current supported colorize modes: :terminal, and :none

(defun colorize-set-none ()
  (setf *colorize-output-mode* :none))

(defun colorize-set-terminal ()
  (setf *colorize-output-mode* :terminal))

(defun colorize (color-kw text &key (bold nil))
  (let* ((color-code "")
         (bold-mode (if bold "1;" "")))
    (cond ((equalp *colorize-output-mode* :terminal)
           (cond ((equalp color-kw :red)
                  (setf color-code "31m"))
                 ((equalp color-kw :green)
                  (setf color-code "32m"))
                 ((equalp color-kw :yellow)
                  (setf color-code "33m"))
                 ((equalp color-kw :blue)
                  (setf color-code "34m"))
                 ((equalp color-kw :purple)
                  (setf color-code "35m"))
                 ((equalp color-kw :cyan)
                  (setf color-code "36m")))
           (format nil "~c[~a~a~a~c[0m" #\esc bold-mode color-code text #\esc))
          (T ;; Otherwise, don't format the text!
           text))))



(defun format-time (destination time control-string &rest arguments)
  (format destination (colorize :blue "~3$: ") time)
  (apply #'format destination control-string arguments))


(defun pretty-number (num)
  #+sbcl
  (cond
    ((or (eql num sb-ext:double-float-positive-infinity)
         (eql num sb-ext:single-float-positive-infinity))
     "∞")
    ((or (eql num sb-ext:double-float-negative-infinity)
         (eql num sb-ext:single-float-negative-infinity))
     "-∞")
    (T
     (format nil "~a" num)))

  #-sbcl
  (format nil "~a" num))


(defun rationalize-number (num)
  (cond
    ((= num +inf+) num)
    ((= num +-inf+) num)
    (t (rationalize num))))


(defun argmin (sequence predicate-fn key-fn)
  "Helper function to take the argmin of a set of objects."
  (declare (optimize (speed 3))
           (type sequence sequence)
           (type function predicate-fn key-fn))
  ;; Can't be empty!
  (assert (not (eql sequence nil)))

  (let* ((x-best (first sequence))
         (val-x-best (funcall key-fn x-best)))
    (declare (type real val-x-best))
    (dolist (x (cdr sequence))
      (let ((val-x (funcall key-fn x)))
        (declare (Type real val-x))
        (when (funcall predicate-fn val-x val-x-best)
          (setf x-best x)
          (setf val-x-best val-x))))
    x-best))


;; TODO - remove this. This is to roughly measure the
;; overhead we could incur having general functions in LVS's.
;; (defun test-1 ()
;;   (declare (optimize (speed 3)))
;;   (let ((a 0.0)
;;         (b 0.0)
;;         (N 10000000))
;;     (declare (type single-float a b)
;;              (type function *f*))


;;     (setf a 0.0)
;;     (time (dotimes (i N)
;;             (setf b (+ a a))
;;             (incf a)
;;             ))


;;     (setf a 0.0)
;;     (time (dotimes (i N)
;;             (setf b (funcall *f* a a))
;;             (incf a)
;;             ))


;;     ;; TODO: test subsumes? Maybe the above differences
;;     ;; can be insignificant compared to other things...


;;     )


;;   )
