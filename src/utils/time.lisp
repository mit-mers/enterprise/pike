;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)

(in-package #:pike)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Methods for dealing with time
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun unix-time-seconds ()
  "Seconds since midnight, January 1, 1970. Returns an integer number of seconds."
  (- (get-universal-time) 2208988800))

(defvar *time-base* (unix-time-seconds)
  "Holds unix time (rounded to the nearest second) when things were started")

(defvar *internal-time-base* (get-internal-real-time)
  "Holds CL's internal time when things were started")

(defun unix-time-drift ()
  "Also returns the unix time, but also keeps track of subsecond accuracy from when the
   process started. As a result, this could be off by as much as a second from the
   actual system time, depending on when the lisp image was started and if it aligns
   with the system clock."
  (float (+ *time-base* (/ (- (get-internal-real-time) *internal-time-base*) internal-time-units-per-second)) 0.0L0))

(defun unix-time ()
  #+sbcl
  (unix-time-sbcl) ;; Superior performance! Accurate to the system microsecond.
  #+-sbcl
  (unix-time-drift)) ;; Drift up to 1 second, but works on all platforms...

#+sbcl
(defun unix-time-sbcl ()
  "SBCL has better time support for getting the current time than the CL standard.
   It's accurate to the microsecond."
  (let (s us)
    (multiple-value-setq (s us) (sb-ext:get-time-of-day))
    (float (+ s
              (/ us 1000000)) 0.0L0)))
