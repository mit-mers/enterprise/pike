;;;; Copyright (c) 2017 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
;;;;
(in-package #:pike)

(defun parse-xmlbif-file (filename &key (ss nil) (rationalize t) (warn-about-new-variables t))
  "Parse a Bayesian Network, encoded in the open standard
.xmlbif file format. Returns a bayesian network object.

  If provided with a state space bits object, then this also
matches the variable names defined in the .xmlbif file with those
uncontrollable ones, and issues some warnings if things don't seem
to match up."
  (unless ss
    (error "Currently, a state space must be provided!"))

  ;; First, parse the xmlbif into an XML struct using the s-xml
  (let ((xml (s-xml:parse-xml-file filename :output-type :xml-struct))
        uncontrollable-vars-in-ss
        uncontrollable-vars-in-bayes-net
        network-xml
        (var-to-val-order (make-hash-table))
        cpts)

    ;; Do some preliminary validation on the .xmlbif file
    (assert (eql :BIF (s-xml:xml-element-name xml)) nil "Invalid .xmlbif file; must start with a <BIF> element.")
    (assert (= 1 (length (s-xml:xml-element-children xml))) nil "Only support parsing .xmlbif file with a single network")
    (setf network-xml (first (s-xml:xml-element-children xml)))
    (assert (eql :NETWORK (s-xml:xml-element-name network-xml)) nil "A <network> must be embedded inside <bif> tag in .xmlbif file")

    ;; Populate the list of uncontrollable variables from the state space.
    (setf uncontrollable-vars-in-ss (loop for var in (state-space-bits-variables ss)
                                       when (uncontrollable? var) collect var))

    ;; Now, cross reference these uncontrollable variables to those represented by the
    ;; Bayesian network. The uncontrollable variables should be a subset of those from
    ;; the Bayes net, and the domain values should precisely match for those. Note that
    ;; it is allowable for there to be latent / hidden variables in the Bayes net that are
    ;; not uncontrollable choices in the state space (inference over these hidden variables
    ;; could influence other variables)
    (dolist (child-xml (s-xml:xml-element-children network-xml))
      (when (and (eql :VARIABLE (s-xml:xml-element-name child-xml))
                 (member (cons :type "nature") (s-xml:xml-element-attributes child-xml) :test #'equalp))
        ;; Cool! Found an uncontrollable variable in the Bayes net.
        (let (name values dv)
          (dolist (prop-xml (s-xml:xml-element-children child-xml))
            (cond
              ;; Parse variable name
              ((eql :NAME (s-xml:xml-element-name prop-xml))
               (assert (= 1 (length (s-xml:xml-element-children prop-xml))) nil "Invalid .xmlbif file: variable name should not have children")
               (assert (stringp (first (s-xml:xml-element-children prop-xml))) nil "Invalid .xmlbif file: variable name must be a string")
               (setf name (first (s-xml:xml-element-children prop-xml))))

              ;; Parse domain value
              ((eql :OUTCOME (s-xml:xml-element-name prop-xml))
               (assert (= 1 (length (s-xml:xml-element-children prop-xml))) nil "Invalid .xmlbif file: variable outcome should not have children")
               (assert (stringp (first (s-xml:xml-element-children prop-xml))) nil "Invalid .xmlbif file: variable outcome must be a string")
               (push (first (s-xml:xml-element-children prop-xml)) values))))



          ;; At this point, we should have parsed all variable and value tags. Now, verify them all.
          (setf dv (find name uncontrollable-vars-in-ss :key #'decision-variable-name :test #'equal))
          (when dv
            ;; Cool! We've cross-referenced the variable names. Make sure the domain values match. If not, we won't be
            ;; be able to make an assignment as below.
            (assert (= (length values) (length (decision-variable-values dv))) nil "Decision variables for ~a don't match in state space and bayes net!" name)
            (dolist (val values)
              (assert (find val (decision-variable-values dv) :test #'equal) nil "Domain for variable ~a doesn't have the domain value ~a listed in bayes net!" name val)))
          (unless dv
            ;; Found a new variable not mentioned in the state space! Create a new one.
            (when warn-about-new-variables
              (format t "Bayesian network has new variable: ~a~%" name))
            (setf dv (make-instance 'decision-variable :name name :domain values :controllable? nil))
            (add-variable! ss dv))
          (push dv uncontrollable-vars-in-bayes-net)
          ;; Ensure that the values are listed in the order specified in the .xmlbif -- this will be important later
          ;; when parsing conditional probability tables
          (setf values (reverse values))
          (setf (gethash dv var-to-val-order) values))))


    ;; Make sure that every uncontrollable variable in the state space is also represented in the Bayes net.
    (dolist (dv uncontrollable-vars-in-ss)
      (assert (member dv uncontrollable-vars-in-bayes-net) nil "Couldn't find variable ~a in Bayes net!" dv))

    ;; All the variable check out! Now it's time to parse the conditional probability tables (CPT's)
    (dolist (child-xml (s-xml:xml-element-children network-xml))
      (when (eql :DEFINITION (s-xml:xml-element-name child-xml))
        ;; Nice! A table should be in here. Pick out the entries
        (let (var (parents nil) table-data)
          (dolist (prop-xml (s-xml:xml-element-children child-xml))
            (cond
              ;; The variable of interest
              ((eql :FOR (s-xml:xml-element-name prop-xml))
               (assert (= 1 (length (s-xml:xml-element-children prop-xml))) nil "Invalid .xmlbif file: for variable should have one child!")
               (assert (stringp (first (s-xml:xml-element-children prop-xml))) nil "Invalid .xmlbif file: for variable should be a string!")
               (setf var (find (first (s-xml:xml-element-children prop-xml)) uncontrollable-vars-in-bayes-net :key #'decision-variable-name :test #'equal)))
              ;; Any parent variables
              ((eql :GIVEN (s-xml:xml-element-name prop-xml))
               (assert (= 1 (length (s-xml:xml-element-children prop-xml))) nil "Invalid .xmlbif file: given variable should have one child!")
               (assert (stringp (first (s-xml:xml-element-children prop-xml))) nil "Invalid .xmlbif file: given variable should be a string!")
               (push (find (first (s-xml:xml-element-children prop-xml)) uncontrollable-vars-in-bayes-net :key #'decision-variable-name :test #'equal) parents))
              ;; Table probability data!
              ((eql :TABLE (s-xml:xml-element-name prop-xml))
               (assert (= 1 (length (s-xml:xml-element-children prop-xml))) nil "Invalid .xmlbif file: table data should have one child!")
               (assert (stringp (first (s-xml:xml-element-children prop-xml))) nil "Invalid .xmlbif file: table data should be a string!")
               (setf table-data (first (s-xml:xml-element-children prop-xml))))))

          ;; Make sure parents stays in the order specified in the file
          (setf parents (reverse parents))

          ;; Parse out the floats from the table data
          (setf table-data (mapcar #'read-from-string (cl-ppcre:split "\\s+" (string-trim `(#\Space #\Tab #\Newline) table-data))))
          ;; If desired, "rationalize" all of these floating point numbers to convert them to
          ;; exact fractions.
          (when rationalize
            ;; (format t "Rationalizing Bayesian network floats to fractions~%")
            (setf table-data (mapcar #'rationalize table-data)))

          ;; Check that we've read in the correct number of values
          (assert (= (length table-data)
                     (apply #'* (mapcar #'(lambda (v) (length (decision-variable-values v))) `(,var ,@parents))))
                  nil "Invalid number of table data entries in .xmlbif file!")

          ;; Now, construct the CPT.
          (let (cpt-entries)
            ;; Take the cartesian product of all parents! Note that order matters here!
            (dolist (parent-combination (cartesian-product (mapcar #'(lambda (v) (gethash v var-to-val-order)) parents)))
              ;; For each combination, read in one "row" of data -- which corresponds to the different values
              ;; of the variable of interest
              (dolist (val (gethash var var-to-val-order))
                (push `(,val :given ,@parent-combination :prob ,(pop table-data))
                      cpt-entries)))
            ;; Nice! We've theoretically parsed the table data in the correct order.
            (push (make-cpt var
                            :parents parents
                            :cpt-rows cpt-entries)
                  cpts)))))

    ;;(print network-xml)

    ;; We're ready to return the final Bayesian network!
    (make-instance 'bayesian-network :cpts cpts)))
