;;;; Copyright (c) 2017 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
;;;;
(in-package #:pike)

(defclass bayesian-network ()
  ((all-variables
    :type list
    :initform nil
    :accessor bayesian-network-variables)
   (cpts
    :type list
    :initform (error "Must specify CPTs! (conditional probability tables)")
    :initarg :cpts
    :accessor conditional-probability-tables
    :documentation "A list of conditional probability tables
that define this Bayesian network")))

(defmethod print-object ((bn bayesian-network) s)
  (format s "#<BAYESIAN-NETWORK ~{~a~^ ~}>" (conditional-probability-tables bn)))


(defclass conditional-probability-table ()
  ((variable
    :type decision-variable
    :initarg :variable
    :accessor cpt-variable
    :initform (error "Must specify decision variable")
    :documentation "The variable over which we are defining
a conditional probabiltiy distribution. X in Pr(X | Y1, Y2, ...)")
   (parents
    :type list
    :initform nil
    :initarg :parents
    :accessor cpt-parents
    :documentation "The parents over which we are defining
a condition probability distribuition. The list `(Y1 Y2 ...)
in Pr(X | Y1, Y2, ...)")
   (entries
    :initform (make-hash-table :test #'equal)
    :documentation "A Sequence of nested hash tables allowing
us to look up probability values in the order X, Y1, Y2, ...")))


(defmethod print-object ((cpt conditional-probability-table) s)
  (with-slots (variable parents) cpt
    (if parents
        (format s "#<CPT Pr(~a | ~a)>" (decision-variable-name variable)
                (format nil "~{~a~^, ~}" (mapcar #'decision-variable-name parents)))
        (format s "#<CPT Pr(~a)>" (decision-variable-name variable)))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Main API
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun make-cpt (variable &key (parents nil) (cpt-rows nil) (try-to-fix-small-sum-errors t) (sum-error-threshold (/ 1 10000000)))
  "Function that constructs a conditional probability table"
  (let ((cpt (make-instance 'conditional-probability-table
                            :variable variable
                            :parents parents)))
    (with-slots (entries) cpt
      ;; Sanity check: make sure the parents are unique, and do not contain the main variable
      (assert (= (length parents)
                 (length (remove-duplicates parents)))
              nil "Parents contains repeated variables!")
      (assert (not (member variable parents)) nil "Main variable can not also be in parents list!")

      ;; Sanity check: make sure we have the correct number of values
      ;; in our table
      (let ((table-size-correct (length (decision-variable-values variable))))
        ;; Multiply in the domain size of each of the parents
        (dolist (parent parents)
          (setf table-size-correct (* table-size-correct (length (decision-variable-values parent)))))
        ;; We now have the theoretical table size. Compare to the number of entries we were given
        (assert (= (length cpt-rows) table-size-correct) nil "Invalid number of CPT entries provided!"))


      ;; Load in all of the table entries. All value tests performed with #'equal, as per API for
      ;; decision variables.
      (dolist (row cpt-rows)
        ;; First, parse this table entry. It should be of the form `(val_variable :given val_parent_1 ... val_parent_k :prob p)
        (let (entry p (row-cp (copy-seq row)))
          (assert (= (length row-cp) (+ 4 (length parents))) nil "Malformed row in CPT; wrong size")
          (push (pop row-cp) entry)
          (assert (eql :given (pop row-cp)) nil "Malformed row in CPT; expected :given!")
          (dotimes (i (length parents))
            (push (pop row-cp) entry))
          (assert (eql :prob (pop row-cp)) nil "Malformed row in CPT; expecected :prob!")
          (setf p (pop row-cp))
          (assert (typep p 'real) nil "Malformed row in CPT; probability value must be a real")
          (setf entry (nreverse entry))

          ;; Next, make sure this table entry doesn't already exist.
          (assert  (not (cpt-lookup-value cpt (first entry) (rest entry))) nil "Duplicate entry in CPT!")
          ;; Entry is new -- add it.
          (setf (gethash entry entries) p)))

      ;; Sanity check: make sure that, for each combination of parents,
      ;; the sum of variable values is 1. If it's just a smidge off, then fix it
      (let ((parent-value-combinations (cartesian-product (mapcar #'decision-variable-values parents))))
        (dolist (parent-values parent-value-combinations)
          (let ((sum 0))
            (dolist (vi (decision-variable-values variable))
              (incf sum (cpt-lookup-value cpt vi parent-values)))
            ;; The sum should be 1 here! If it's not, raise an error.
            (when (and try-to-fix-small-sum-errors
                       (not (= 1 sum))
                       (< (abs (- sum 1)) sum-error-threshold))
              ;; Just a little bit off -- fix it by bumping up last value a smidge
              (cpt-set-value cpt (first (last (decision-variable-values variable))) parent-values
                             (+ (cpt-lookup-value cpt (first (last (decision-variable-values variable))) parent-values)
                                (- 1 sum)))
              ;; Recompute as a sanity check!
              (setf sum 0)
              (dolist (vi (decision-variable-values variable))
                (incf sum (cpt-lookup-value cpt vi parent-values))))
            (assert (= 1 sum) nil "CPT doesn't sum to 1; it sums to ~a for parent value combination ~a (max error: ~a)" sum parent-values sum-error-threshold)))))
    cpt))



(defun cpt-lookup-value (cpt variable-value parent-values)
  "Helper function that looks up the values of this varaible given the list
   of in-order parent values. Returns the looked up value, or nil if it wasn't found."
  (with-slots (entries) cpt
    (gethash `(,variable-value ,@parent-values) entries)))


(defun cpt-set-value (cpt variable-value parent-values prob)
  "Helper function that sets the values of this varaible given the list
   of in-order parent values. Returns the looked up value, or nil if it wasn't found."
  (with-slots (entries) cpt
    (setf (gethash `(,variable-value ,@parent-values) entries) prob)))


(defmacro do-cpt-assignments (variables-and-list cpt &rest body)
  "A macro to easily loop over assignments in this conditional
   probability table:

  (do-cpt-assignments (assignment-var assignments-parents p) cpt
    (format t \"Pr(~a | ~a) = ~a~%\" x-var (format nil \"~{~a~^, ~}\" x-parents) p))

   assignment-var will be bound to a decision variable assignment value,
and assignments-parents will be bound to assignments for parents. p
will be bound to the corresponding probability."
  (let ((v-main-var (first variables-and-list))
        (v-parents (second variables-and-list))
        (p (third variables-and-list))
        (x-parent-values-products (gensym))
        (x-parent-values (gensym))
        (x-main-value (gensym)))
    `(let ((,x-parent-values-products (cartesian-product (mapcar #'decision-variable-values (cpt-parents ,cpt)))))
       (dolist (,x-parent-values ,x-parent-values-products)
         (let ((,v-parents (mapcar #'assignment (cpt-parents ,cpt) ,x-parent-values)))
           (dolist (,x-main-value (decision-variable-values (cpt-variable ,cpt)))
             (let ((,v-main-var (assignment (cpt-variable ,cpt) ,x-main-value))
                   (,p (cpt-lookup-value ,cpt ,x-main-value ,x-parent-values)))
               (when (typep ,p 'number)
                 ,@body))))))))


(defmethod initialize-instance :after ((bn bayesian-network) &key)
  "Does some validation on the Bayesian Network. Specifically, makes
sure that we have a conditional probability table for each variable,
and that we don't depend on any parent variables where no CPT
is available."
  (when (typep bn 'influence-diagram)
    (return-from initialize-instance))
  (with-slots (cpts all-variables) bn
    ;; Assert that each of the CPT's covers a different variable
    (let ((cpt-main-variables (mapcar #'cpt-variable cpts)))
      (assert (= (length cpt-main-variables)
                 (length (remove-duplicates cpt-main-variables)))
              nil "Multiple CPTs are for a single variable! Must be 1:1"))

    ;; Next, collect all the variables mentioned in the CPTs
    (dolist (cpt cpts)
      (pushnew (cpt-variable cpt) all-variables)
      (dolist (parent (cpt-parents cpt))
        (pushnew parent all-variables)))
    (assert (= (length cpts)
               (length all-variables))
            nil "Number of CPTs doesn't match number of mentioned variables!")

    ;; Cool beans! Now that we have all the variables, go through
    ;; and make sure that we have a CPT for each of these variables.
    (let ((variables-remaining (copy-seq all-variables)))
      (dolist (cpt cpts)
        (assert (member (cpt-variable cpt) variables-remaining) nil "CPT variable / parents mismatch")
        (setf variables-remaining (remove (cpt-variable cpt) variables-remaining)))
      (assert (eql variables-remaining nil) nil "CPT variable / parents mismatch!"))))


(defmethod sample-from-bayesian-network ((bn bayesian-network))
  "Generate a random sample from this Bayesian network.
  Implements forward sampling, with no evidence.
  Works by sampling in topological order.

  Returns a sample: a hash table mapping each
of the variables to an assignment."
  ;; Before we begin, make sure the random number generator is randomly seeded...
  #+sbcl
  (setf *random-state* (make-random-state t))
  (with-slots (all-variables cpts) bn
    (let ((sample (make-hash-table :test #'eql))
          (var-predecessors (make-hash-table :test #'eql))
          (cpts-sorted nil)
          (cpts-all (copy-seq cpts)))

      ;; First, sort the CPTs in topological order
      ;; Assemble predecessors
      (dolist (cpt cpts-all)
        (setf (gethash (cpt-variable cpt) var-predecessors) (copy-seq (cpt-parents cpt))))
      (dotimes (i (length cpts-all))
        ;; Select a variable that has no un-sorted predecessors
        (dolist (cpt cpts-all)
          (when (eql nil (gethash (cpt-variable cpt) var-predecessors))
            ;; Cool! Select this one.
            (push cpt cpts-sorted)
            ;; No longer an option to sort this variable.
            (setf cpts-all (remove cpt cpts-all))
            ;; Now that it's sorted, remove it as a predecessor from all the choice variables
            (dolist (cpt-other cpts-all)
              (setf (gethash (cpt-variable cpt-other) var-predecessors)
                    (remove (cpt-variable cpt) (gethash (cpt-variable cpt-other) var-predecessors))))
            (return))))
      (unless (eql nil cpts-all)
        (error "Cannot choose total ordering of variables... is there a cycle??"))
      (setf cpts-sorted (reverse cpts-sorted))
      ;;(format t "CPT order: ~a~%" cpts-sorted)

      ;; Iterate in order over the CPTs, and sample from each by following the proper distribution.
      (dolist (cpt cpts-sorted)
        (let ((parent-values (loop for par in (cpt-parents cpt) collecting (assignment-value (gethash par sample))))
              (r (random 1.0))
              (cumulative 0))
          (dolist (val (decision-variable-values (cpt-variable cpt)))
            ;; Prevent a rounding edge case
            (incf cumulative (cpt-lookup-value cpt val parent-values))
            (when (eql val (first (last (decision-variable-values (cpt-variable cpt)))))
              (setf cumulative 1))
            (when (<= r cumulative)
              ;; Set it!
              (setf (gethash (cpt-variable cpt) sample)
                    (assignment (cpt-variable cpt) val))
              ;; (format t "~a~%" (assignment (cpt-variable cpt) val))
              (return)))))

      sample)))
