;;;; Copyright (c) 2018 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
;;;;
(in-package #:pike)

(defun parse-pgmx-file (filename &key (ss nil) (rationalize t))
  "Parse an influence diagram, encoded in the open standard
.pgmx file format (see OpenMarkov website). Returns an influence diagram format.

  If provided with a state space bits object, then this also
matches the variable names defined in the .xmlbif file with those
uncontrollable ones, and issues some warnings if things don't seem
to match up."
  (unless ss
    (error "Currently, a state space must be provided!"))

  ;; First, parse the pgmx into an XML struct using the s-xml
  (let ((xml (s-xml:parse-xml-file filename :output-type :xml-struct))
        variables-in-ss
        variables-in-bayes-net
        network-xml
        (var-to-val-order (make-hash-table))
        cpts
        information-arcs)

    ;; Do some preliminary validation on the .xmlbif file
    (assert (eql :|ProbModelXML| (s-xml:xml-element-name xml)) nil "Invalid .pgmx file; must start with a <ProbModelXML> element.")
    (setf network-xml (first (s-xml:xml-element-children xml)))
    (assert (eql :|ProbNet| (s-xml:xml-element-name network-xml)) nil "A <ProbNet> must be embedded inside <ProbModelXML> tag in .xmlbif file")
    (assert (equalp "InfluenceDiagram" (s-xml:xml-element-attribute network-xml :|type|)) nil "Unsupported .pgmx file: type must be InfluenceDiagram.")

    ;; Populate the list of uncontrollable and controllable variables from the state space.
    (setf variables-in-ss (copy-seq (state-space-bits-variables ss)))


    ;; Now, cross reference these uncontrollable variables to those represented by the
    ;; influence diagram. The uncontrollable variables should be a subset of those from
    ;; the influence diagram, and the domain values should precisely match for those. Note that
    ;; it is allowable for there to be latent / hidden variables in the influence diagram that are
    ;; not uncontrollable choices in the state space (inference over these hidden variables
    ;; could influence other variables)
    (dolist (child-xml (s-xml:xml-element-children network-xml))
      (when (eql :|Variables| (s-xml:xml-element-name child-xml))
        (dolist (variable-xml (s-xml:xml-element-children child-xml))
          (assert (eql :|Variable| (s-xml:xml-element-name variable-xml)) nil "Invalid .pgmx file: found non-<Variable> in <Variables>")
          (assert (equalp "finiteStates" (s-xml:xml-element-attribute variable-xml :|type|)) nil "Unsupported .pgmx file: only finiteStates variables supported.")

          ;; Cool! Found a variable in the influence diagram.
          (let (name values stochastic-variable? dv)
            ;; Parse the name
            (setf name (s-xml:xml-element-attribute variable-xml :|name|))
            ;; Parse whether it's a decision (controllable) or stochastic (uncontrollable) variable
            (cond
              ((equalp "chance" (s-xml:xml-element-attribute variable-xml :|role|))
               (setf stochastic-variable? t))
              ((equalp "decision" (s-xml:xml-element-attribute variable-xml :|role|))
               (setf stochastic-variable? nil))
              (t
               (error "Invalid <Variable> in .pgmx file: must be chance or decision. No utility allowed here by the way.")))

            ;; Parse out the domain
            (dolist (prop-xml (s-xml:xml-element-children variable-xml))
              (when (eql :|States| (s-xml:xml-element-name prop-xml))
                (dolist (state-xml (s-xml:xml-element-children prop-xml))

                  (let (val)
                    ;; Parse domain value
                    (when (eql :|State| (s-xml:xml-element-name state-xml))
                      ;; Pull out this domain value, preserving the order.
                      (setf val (s-xml:xml-element-attribute state-xml :|name|))
                      ;; Map "unobserved to easier ∘
                      (when (equal val "unobserved")
                        (setf val "∘"))
                      (setf values `(,@values ,val)))))))



            ;; At this point, we should have parsed all the values. Now, verify them all.
            (setf dv (find name variables-in-ss :key #'decision-variable-name :test #'equal))
            (when dv
              ;; Cool! We've cross-referenced the variable name. Make sure the domain values match. If not, we won't be
              ;; be able to make an assignment as below.
              (assert (= (length values) (length (decision-variable-values dv))) nil "Choice variables for ~a don't match in state space and influence diagram!" name)
              (dolist (val values)
                (assert (find val (decision-variable-values dv) :test #'equal) nil "Domain for variable ~a doesn't have the domain value ~a listed in influence diagram!" name val))

              (assert (eql (controllable? dv) (not stochastic-variable?)) nil "Choice variable controllability for ~a doesn't match in state space and influence diagram!") name)
            (unless dv
              ;; Found a new variable not mentioned in the state space! Create a new one.
              (format t "Influence diagram has new variable: ~a~%" name)
              (setf dv (make-instance 'decision-variable :name name :domain values :controllable? (not stochastic-variable?)))
              (add-variable! ss dv))

            (push dv variables-in-bayes-net)
            (setf (gethash dv var-to-val-order) values)))))


    ;; Make sure that every uncontrollable variable in the state space is also represented in the Bayes net.
    (dolist (dv variables-in-ss)
      (when (not (controllable? dv))
        (assert (member dv variables-in-bayes-net) nil "Couldn't find variable ~a in influence diagram!" dv)))

    ;; Make sure that for every uncontrollable variable v_u: v_u has the unassigned ∘ domain value
    ;; if and only if there is a corresponding controllable active_v_u variable with domain "y" and "n".
    (dolist (v variables-in-ss)
      (when (controllable? v)
        (let ((has-unassigned-value? (member "∘" (decision-variable-values v) :test #'equal))
              (active-variable (find (format nil "active_~a" (decision-variable-name v)) variables-in-ss :key #'decision-variable-name :test #'equal)))
          (when (and has-unassigned-value? (not active-variable))
            (error "Variable ~a has domain value ∘ but no associated active_~a variable!" (decision-variable-name v) (decision-variable-name v)))
          (when (and (not has-unassigned-value?) active-variable)
            (error "Variable ~a does NOT have domain value ∘ but there exists a variable called active_~a!" (decision-variable-name v) (decision-variable-name v)))
          (when active-variable
            (assert (sets-equal `("y" "n") (decision-variable-values active-variable)) nil "Active variable ~a must have domain of {y, n}" (decision-variable-name active-variable))))))


    ;; All the variable check out! Now it's time to parse the conditional probability tables (CPT's)
    (dolist (child-xml (s-xml:xml-element-children network-xml))
      (when (eql :|Potentials| (s-xml:xml-element-name child-xml))
        ;; Cool! Find all potentials.
        (dolist (potential-xml (s-xml:xml-element-children child-xml))
          (when (eql :|Potential| (s-xml:xml-element-name potential-xml))
            ;; Found one!
            (assert (equalp "Table" (s-xml:xml-element-attribute potential-xml :|type|)) nil "Potentials in .pgmx file must have tables (not be uniform etc.).")

            (let (var parents var-name potential-names table-data)
              ;; Find the variables involved.
              (dolist (variables-xml (s-xml:xml-element-children potential-xml))
                (when (eql :|Variables| (s-xml:xml-element-name variables-xml))
                  (dolist (variable-xml (s-xml:xml-element-children variables-xml))
                    (when (eql :|Variable| (s-xml:xml-element-name variable-xml))
                      (let ((name (s-xml:xml-element-attribute variable-xml :|name|)))
                        (setf potential-names `(,@potential-names ,name))))))

                (when (eql :|Values| (s-xml:xml-element-name variables-xml))
                  (setf table-data (first (s-xml:xml-element-children variables-xml)))))
              ;; The main variable is assumed to be the first one.
              (setf var-name (pop potential-names))
              ;; Map names to actual variables
              (setf var (find var-name variables-in-bayes-net :key #'decision-variable-name :test #'equal))
              (dolist (potential-name potential-names)
                (let (parent-var)
                  (setf parent-var (find potential-name variables-in-bayes-net :key #'decision-variable-name :test #'equal))
                  (setf parents `(,@parents ,parent-var))))


              ;; Make sure parents stays in the order specified in the file
              (setf parents (reverse parents))

              ;; Parse out the floats from the table data
              (setf table-data (mapcar #'read-from-string (cl-ppcre:split "\\s+" (string-trim `(#\Space #\Tab #\Newline) table-data))))
              ;; If desired, "rationalize" all of these floating point numbers to convert them to
              ;; exact fractions.
              (when rationalize
                ;; (format t "Rationalizing Bayesian network floats to fractions~%")
                (setf table-data (mapcar #'rationalize table-data)))

              ;; Check that we've read in the correct number of values
              (assert (= (length table-data)
                         (apply #'* (mapcar #'(lambda (v) (length (decision-variable-values v))) `(,var ,@parents))))
                      nil "Invalid number of table data entries in .xmlbif file!")

              ;; Now, construct the CPT.
              (let (cpt-entries)
                ;; Take the cartesian product of all parents! Note that order matters here!
                (dolist (parent-combination (cartesian-product (mapcar #'(lambda (v) (gethash v var-to-val-order)) parents)))
                  ;; For each combination, read in one "row" of data -- which corresponds to the different values
                  ;; of the variable of interest
                  (dolist (val (gethash var var-to-val-order))
                    (push `(,val :given ,@parent-combination :prob ,(pop table-data))
                          cpt-entries)))
                ;; Nice! We've theoretically parsed the table data in the correct order.
                (push (make-cpt var
                                :parents parents
                                :cpt-rows cpt-entries)
                      cpts)))))))


    ;; Parse out information arcs and store them, too
    (dolist (child-xml (s-xml:xml-element-children network-xml))
      (when (eql :|Links| (s-xml:xml-element-name child-xml))
        ;; Cool! Find check the links..
        (dolist (link-xml (s-xml:xml-element-children child-xml))
          (when (eql :|Link| (s-xml:xml-element-name link-xml))
            ;; Found one!
            (assert (equalp "true" (s-xml:xml-element-attribute link-xml :|directed|)) nil "Links in the .pgmx file must be directed!")
            ;; Parse out the names
            (let (variable-names variable-from variable-to)
              (dolist (variable-xml (s-xml:xml-element-children link-xml))
                (when (eql :|Variable| (s-xml:xml-element-name variable-xml))
                  (let ((name (s-xml:xml-element-attribute variable-xml :|name|)))
                    (setf variable-names `(,@variable-names ,name)))))
              ;; Now that we have the names, find the associated variables
              (setf variable-from (find (first variable-names) variables-in-bayes-net :key #'decision-variable-name :test #'equal))
              (setf variable-to  (find (second variable-names) variables-in-bayes-net :key #'decision-variable-name :test #'equal))
              ;; This is an information arc if variable-to is controllable.
              (when (controllable? variable-to)
                (push `(,variable-from ,variable-to) information-arcs)))))))

    ;; We're ready to return the final Bayesian network!
    (make-instance 'influence-diagram
                   :cpts cpts
                   :information-arcs information-arcs)))
