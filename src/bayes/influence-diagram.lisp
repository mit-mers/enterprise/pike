;;;; Copyright (c) 2018 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
;;;;
(in-package #:pike)

(defclass influence-diagram (bayesian-network)
  ((decision-variables
    :type list
    :initform nil
    :accessor influence-diagram-decision-variables)
   (stochastic-variables
    :type list
    :initform nil
    :accessor influence-diagram-stochastic-variables)
   (information-arcs
    :type list
    :initform nil
    :initarg :information-arcs
    :accessor influence-diagram-information-arcs
    :documentation "A list of information arcs, where each arc
is a tuple of (start, end).")))

(defmethod print-object ((infdiag influence-diagram) s)
  (format s "#<INFLUENCE-DIAGRAM ~{~a~^ ~}>" (conditional-probability-tables infdiag)))





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Main API
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod initialize-instance :after ((infdiag influence-diagram) &key)
  "Does some validation on the Influence Diagram. Specifically, makes
sure that we have a conditional probability table for each variable,
and that we don't depend on any parent variables where no CPT
is available."
  (with-slots (cpts all-variables decision-variables stochastic-variables) infdiag
    ;; Assert that each of the CPT's covers a different variable
    (let ((cpt-main-variables (mapcar #'cpt-variable cpts)))
      (assert (= (length cpt-main-variables)
                 (length (remove-duplicates cpt-main-variables)))
              nil "Multiple CPTs are for a single variable! Must be 1:1"))

    ;; TODO Probably add more checks here to replace below
    ;; Next, collect all the variables mentioned in the CPTs
    (dolist (cpt cpts)
      (pushnew (cpt-variable cpt) all-variables)
      (dolist (parent (cpt-parents cpt))
        (pushnew parent all-variables)))

    ;; Separate into controllable and uncontrollable variables
    (dolist (v all-variables)
      (cond
        ((controllable? v) (push v decision-variables))
        (t (push v stochastic-variables))))

    (assert (= (length cpts)
               (length stochastic-variables))
            nil "Number of CPTs doesn't match number of mentioned variables!")

    ;; Cool beans! Now that we have all the variables, go through
    ;; and make sure that we have a CPT for each of these variables.
    (let ((variables-remaining (copy-seq stochastic-variables)))
      (dolist (cpt cpts)
        (assert (member (cpt-variable cpt) variables-remaining) nil "CPT variable / parents mismatch")
        (setf variables-remaining (remove (cpt-variable cpt) variables-remaining)))
      (assert (eql variables-remaining nil) nil "CPT variable / parents mismatch!"))))


(defmethod sample-from-bayesian-network ((infdiag influence-diagram))
  (error "Operation not supported yet! Would depend on choice order too."))



(defmethod influence-diagram-construct-information-graph ((infdiag influence-diagram))
  "Constructs a graph, viewing CPTs and information arcs as directed edges."
  (with-slots (cpts information-arcs all-variables) infdiag
    (let (G)
      ;; ;; Construct a directed graph that looks like the influence diagram. CPTs and information arcs become directed edges.
      ;; ;; Add nodes
      (setf G (make-instance 'mtk-graph:directed-graph))
      (dolist (v all-variables)
        (mtk-graph:add-vertex v G))
      ;; Convert CPTs to edges
      (dolist (cpt cpts)
        (dolist (par (cpt-parents cpt))
          (mtk-graph:add-edge (list par (cpt-variable cpt)) (mtk-graph:get-edges G))))
      ;; Add information arcs too
      (dolist (ia information-arcs)
        (mtk-graph:add-edge ia (mtk-graph:get-edges G)))

      ;; Below uses cl-graph instead
      ;; Construct a directed graph that looks like the influence diagram. CPTs and information arcs become directed edges.
      ;; Add nodes
      ;; (setf G (cl-graph:make-graph 'cl-graph:graph-container))
      ;; (dolist (v all-variables)
      ;;   (cl-graph:add-vertex G v))
      ;; ;; Convert CPTs to edges
      ;; (dolist (cpt cpts)
      ;;   (dolist (par (cpt-parents cpt))
      ;;     (cl-graph:add-edge-between-vertexes G par (cpt-variable cpt) :edge-type :directed)))
      ;; ;; Add information arcs too
      ;; (dolist (ia information-arcs)
      ;;   (cl-graph:add-edge-between-vertexes G (first ia) (second ia) :edge-type :directed))

      G)))

(defmethod influence-diagram-compute-node-dependencies ((infdiag influence-diagram))
  (with-slots (all-variables) infdiag
    (let* ((G (influence-diagram-construct-information-graph infdiag))
           (apsp (mtk-graph-algorithms:floyd-warshall G :weight-function #'(lambda (e) (declare (ignore e)) -1))))
      (dolist (v-i all-variables)
        (dolist (v-j all-variables)
          (let ((val (gethash `(,v-i ,v-j) apsp)))
            (setf (gethash `(,v-i ,v-j) apsp)
                  (if (>= val 0)
                      nil
                      t)))))
      apsp)))


(defmethod influence-diagram-verify-variable-order ((infdiag influence-diagram) variable-order)
  "This method verifies that the variable order provided is consistent with the variable order of the influence diagram.
Basically, it checks to see if there exists a directed path that satisfies the necessary conditions.
For detailed information, please see pages 304-306 of the textbook Bayesian Network and Decision Diagrams, by Jensen and Nielsen (2007)."
  ;; APSP with -1 weights... check for cycles by checking all diagonals for 0. Check for paths to see if less than inf.
  (with-slots (cpts information-arcs all-variables) infdiag
    (let (G apsp)

      ;; Make sure number of variables matches
      (unless (= (length variable-order) (length all-variables))
        (error "Different number of variables in variable order than in influence diagram!"))

      (setf G (influence-diagram-construct-information-graph infdiag))


      ;; If we assigned a -1 edge weight to each directed edge, compute the APSP. This will allow us to both
      ;; check for negative cycles, and check for the existence of a path between two nodes.

      ;; TODO Can maybe make this more efficient?
      (setf apsp (mtk-graph-algorithms:floyd-warshall G :weight-function #'(lambda (e) (declare (ignore e)) -1)))

      ;; Check for negative cycles
      (dolist (v all-variables)
        (unless (= 0 (gethash `(,v ,v) apsp))
          (error "Error! The influence diagram has cycles in it -- must be a DAG.")))

      ;; We can now verify the variable ordering. First, we derive the partition of variables.
      ;; If the variable ordering is correct, we should be able to derive the partitions from it.
      (let (partitions)
        ;; Partition the provided variable order into pairs of the form (I_{j-1}, D_j) -- see the textbook. The last D_{j+1} will be nil.
        (let ((vs (copy-seq variable-order))
              v uncontrollables)
          (iter (while t)
                ;; If we've reached the end of the list, push whatever we have and exit out.
                (when (eql nil vs)
                  (setf partitions `(,@partitions (,uncontrollables nil)))
                  (return))

                ;; Pop the next one off
                (setf v (pop vs))
                (cond
                  ((controllable? v)
                   (setf partitions `(,@partitions (,uncontrollables ,v)))
                   (setf uncontrollables nil))
                  (t (push v uncontrollables)))))

        (setf partitions (remove (list nil nil) partitions :test #'equal))
        ;;(format t "Partitions: ~a~%" partitions)

        ;; Great! We can now verify that the partitions (representing the variable ordering)
        ;; are consistent with the directed structure of the influence diagram.
        (let (v-d-last)
          (dolist (partition partitions)
            (let ((uncontrollables (first partition))
                  (v-d (second partition)))

              ;; Verify that there is a direct edge from each uncontrollable variable to the decision variable
              (when v-d
                (dolist (v-u uncontrollables)
                  (unless (member v-u (mtk-graph:find-parents-of-vertex v-d G))
                    (error "Error! There should be a directed edge from ~a to ~a in influence diagram, but there isn't" v-u v-d))))

              ;; Verify that there exists a directed path from the last v-d to this one..
              (when (and v-d-last v-d)
                (when (= +inf+ (gethash `(,v-d-last ,v-d) apsp))
                  (error "Error! There should be a directed path from ~a to ~a in influence diagram, but there isn't" v-d-last v-d)))

              (setf v-d-last v-d)))))
      t)))


(defmethod compile-bayesian-network-to-influence-diagram (bn-filename ss choice-variable-guards)
  "This method compiles a Bayesian network that does not contain any inactivated domain values '∘',
into an influence diagram that does support this conditional nature. The new influence diagram has these properties:
    - For every variable in the pTPN with a non-True guard (i.e., could be inactivated), the associated variable
      in the ID will have the '∘' domain value, and there will exist


 "

  (let ((bn-var->id-var (make-hash-table))
        (id-var->bn-var (make-hash-table))
        bn
        bni
        ss-bn

        ;; Fields for the influence diagram
        cpts information-arcs)

    (format t "Compiling Bayesian network to influence diagram.~%")

    ;; Parse the Bayesian network .xmlbif file, and put variables in an isolated
    ;; state space for now.
    (setf ss-bn (make-state-space-bits))
    (setf bn (parse-xmlbif-file bn-filename
                                :ss ss-bn
                                :warn-about-new-variables nil))

    ;; Now, map BN variables to their equivalents in the ss, ensuring that everything matches (name, domain
    ;; except possibly '∘', and controllability) all match.
    (dolist (var-bn (bayesian-network-variables bn))
      (let (var)
        ;; Find the corresponding variable. If it doesn't exist, add it to the state space.
        (setf var (find (decision-variable-name var-bn) (state-space-bits-variables ss) :key #'decision-variable-name :test #'equal))
        (unless var
          (format t "Bayesian network has new variable: ~a~%" (decision-variable-name var-bn))
          (add-variable! ss var-bn)
          (setf var var-bn))

        ;; Check the domain and controllability
        (assert (eql (controllable? var-bn) (controllable? var)) nil "Variable ~a is controllable in pTPN but in Bayesian network!" (decision-variable-name var))
        (assert (subsetp (set-difference (decision-variable-values var)
                                         (decision-variable-values var-bn)
                                         :test #'equal)
                         `("∘")
                         :test #'equal)
                nil "Choice variable ~a in Bayesain network must match domain in pTPN (except for unassigned '∘' value)")
        ;; Checks pass! Record the mapping.
        (setf (gethash var-bn bn-var->id-var) var)
        (setf (gethash var id-var->bn-var) var-bn)))

    ;; Ensure that every uncontrollable variable in ss is represented in the BN.
    (dolist (var (state-space-bits-variables ss))
      (when (not (controllable? var))
        (unless (gethash var id-var->bn-var)
          (error "Bayesian network .xmlbif does not have variable ~a!" (decision-variable-name var)))))

    ;; In order to help construct new CPTs, construct an inference syste
    (setf bni (make-instance 'bayesian-network-inference-bdd
                             :bayesian-network bn
                             :ss ss-bn))
    (initialize-for-inference bni)

    ;; Construct a new CPT for each uncontrollable variable in the BN.
    (dolist (var (state-space-bits-variables ss))
      (when (not (controllable? var))
        ;; Find the CPT corresponding to this var
        (let* ((var-bn (gethash var id-var->bn-var))
               (cpt (find var-bn (conditional-probability-tables bn) :key #'cpt-variable))
               cpt-entries
               parents
               active-var)
          ;; Find the parents (in the main state space) for this variable
          (setf parents (mapcar #'(lambda (x_par) (gethash x_par bn-var->id-var)) (cpt-parents cpt)))

          ;; If this is a conditional variable, then add the appropriate active variable as a parent
          ;; and note the information arc
          (let (found? guard)
            (multiple-value-setq (guard found?) (gethash var choice-variable-guards))
            (when (and found? guard)
              ;; This uncontrollable variable may be conditional! Make sure active_var
              ;; as the first parent.
              (setf active-var (find-variable (state-space-bits-variables ss) (format nil "active_~a" (decision-variable-name var))))
              (push active-var parents)
              ;; Add an information arc.
              (push `(,active-var ,var) information-arcs)))

          ;; Compute appropriate CPT values. Frames and solves a WMC problem when appropriate.
          (dolist (val (decision-variable-values var))
            (dolist (parent-combination (cartesian-product (mapcar #'decision-variable-values parents)))

              (let ((parent-combination-bn (copy-seq parent-combination))
                    active?
                    p)
                ;; The original BN combination wouldn't have the active variable assigned (if there is one)
                (cond
                  (active-var
                   (setf active? (pop parent-combination-bn)))
                  (t (setf active? "y")))

                ;; Now, based on the following logic, we can compute the probability for this combination based on
                ;; 1. Analyzing which values are unassigned '∘'
                ;; 2. Original values for the CPT
                ;; 3. Framing a series of WMC problems if needed
                (cond
                  ((equal "n" active?)
                   (cond ((equal "∘" val) (setf p 1))
                         (t (setf p 0))))

                  (t
                   (cond ((equal "∘" val) (setf p 0))
                         ((not (member "∘" parent-combination-bn :test #'equal))
                          (setf p (cpt-lookup-value cpt val parent-combination-bn)))
                         (t
                          ;; Frame a weighted model counting problem
                          (let (X_active)
                            ;; Get the assignments to BN variables that are not unassigned
                            (setf X_active (mapcar #'(lambda (pair) (assignment (first pair) (second pair)))
                                                   (remove-if #'(lambda (pair) (equal "∘" (second pair)))
                                                              (mapcar #'list (cpt-parents cpt) parent-combination-bn))))

                            ;; Compute the conditional probability P(x=v | X_active) = P(x=v, X_active) / P(X_active)
                            (setf p (/ (compute-marginal-probability-of-uncontrollable-choices bni `(,(assignment var-bn val) ,@X_active))
                                       (compute-marginal-probability-of-uncontrollable-choices bni X_active))))))))


                ;; Add this entry to the CPT that we will be building
                (push `(,val :given ,@parent-combination :prob ,p)
                      cpt-entries))))

          ;; Store the CPT
          (push (make-cpt var
                          :parents parents
                          :cpt-rows cpt-entries)
                cpts))))

    ;; Build and return the influence diagram
    (make-instance 'influence-diagram
                   :cpts cpts
                   :information-arcs information-arcs)))
