;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)


;;; Less optimized but more generic version of an lvs
;;; Steve Levine (sjlevine@mit.edu)

(in-package #:pike)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Labeled values (LV's)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass lv-generic ()
  ((value
    :initform (error "Must specify value")
    :initarg :value
    :accessor value
    :documentation "The value")
   (label
    :initform (error "Must specify label")
    :initarg :label
    :accessor label
    :documentation "The value")
   (ss
    :initarg :ss
    :type state-space-bits
    :accessor lv-generic-ss
    :initform (error "Must specify state space!")))
  (:documentation "A labeled value"))

(defmethod print-object ((lv lv-generic) s)
  (with-slots (value label ss) lv
    (format s "(~a, " value)
    (print-environment label ss s)
    (format s ")")))




(defun create-lv-generic-with-universal-env (ss &key (value 0.0))
  (declare (type state-space-bits ss))
  (create-lv-generic ss value (make-environment ss)))


(defun create-lv-generic (ss value label)
  (declare (type environment label)
           (type state-space-bits ss))
  (make-instance 'lv-generic
                 :value value
                 :label label
                 :ss ss))


(defun dominates?-generic (lv1 lv2 R)
  (declare (type lv-generic lv1)
           (type lv-generic lv2)
           (type function R))
  (and (funcall R (value lv1) (value lv2))
       (subsumes? (label lv1) (label lv2) (lv-generic-ss lv1))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Labeled values sets (LVS's)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass lvs-generic ()
  ((pairs
    :type list
    :initform nil
    :initarg :pairs
    :accessor pairs
    :documentation "The pairs within this LVS")
   (R
    :type function
    :initform #'<
    :initarg :relation
    :accessor relation
    :documentation "The ordering relation for
this LVS"))
  (:documentation "A less-optimized but more
flexible LVS"))

(defmethod print-object ((lvs lvs-generic) s)
  (format s "{~{~a~^, ~}}" (pairs lvs)))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Core algorithms
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun add-pair-generic (lvs pair)
  "Add a pair to the LVS minimally."
  (declare (type lvs-generic lvs)
           (type lv-generic pair))
  (with-slots (pairs R) lvs
    ;; Make sure pair isn't dominated by anything already in the lvs
    (dolist (p pairs)
      (if (dominates?-generic p pair R)
          (return-from add-pair-generic nil)))

    ;; Not dominated! Remove any pairs that are dominated by pair.
    (setf pairs (delete-if #'(lambda (p)
                               (dominates?-generic pair p R))
                           pairs))
    (push pair pairs)
    T))


(defun lvs-binary-operation (ss lvs-a lvs-b fn)
  "Compute a new LVS that represents the max binary function (basically a cross product)"
  (declare (type state-space-bits ss)
           (type lvs-generic lvs-a)
           (type lvs-generic lvs-b)
           (type function fn))
  (assert (eql (relation lvs-a) (relation lvs-b)))
  (let* ((lvs-result (make-instance 'lvs-generic :relation (relation lvs-a))))
    (dolist (pair-a (pairs lvs-a))
      (dolist (pair-b (pairs lvs-b))
        (let* ((e-m  (bit-ior (label pair-a) (label pair-b))))
          (declare (type environment e-m))
          ;; Compute the environmental union and see if it's valid
          (when (env-valid? ss e-m)
            (add-pair-generic lvs-result
                              (make-instance 'lv-generic
                                             :value (funcall fn (lv-value pair-a) (lv-value pair-b))
                                             :label e-m
                                             :ss ss))))))
    lvs-result))



(defun query-lvs-generic (ss lvs max-val env)
  "Find and return the smallest/tightest value in this LVS
   (corresponding to a pair (a_i, phi_i) ) where phi_i
   subsumes the given env."
  (declare (type state-space-bits ss)
           (type lvs-generic lvs)
           (type environment env))
  (declare (type environment env))
  (let ((a-i-tightest max-val))
    (dolist (pair (pairs lvs))
      (declare (type lv-generic pair))
      (let ((a-i (value pair))
            (phi-i (label pair)))
        (when (and (< a-i a-i-tightest)
                   (subsumes? phi-i env ss))
          (setf a-i-tightest a-i))))
    a-i-tightest))
