;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)

(defun lvs-test-1 ()
  (let* ((ss (make-state-space-bits))
         (x (make-instance 'decision-variable
                           :name "x"
                           :domain (list 1 2)))
         (y (make-instance 'decision-variable
                           :name "y"
                           :domain (list 1 2)))
         (z (make-instance 'decision-variable
                           :name "z"
                           :domain (list 1 2)))


         (lvs (make-lvs)))

    (add-variable! ss x)
    (add-variable! ss y)
    (add-variable! ss z)


    (add-pair lvs
              (make-lv :value 1.0
                       :label (make-environment-from-assignments ss (list (assignment x 1)))
                       :ss ss))

    (add-pair lvs
              (make-lv :value 2.0
                       :label (make-environment-from-assignments ss (list))
                       :ss ss))

    (add-pair lvs
              (make-lv :value 3.0
                       :label (make-environment-from-assignments ss (list (assignment x 1)))
                       :ss ss))

    ;; (let ((lv (make-lv :value 3.0
    ;;                    :label (make-environment-from-assignments ss (list (assignment x 1)))
    ;;                    :ss ss)))
    ;;   (time (dotimes (i 100000000)
    ;;           (add-pair lvs lv))))

    lvs))



(defun lvs-generic-test-1 ()
  (let* ((ss (make-state-space-bits))
         (x (make-instance 'decision-variable
                           :name "x"
                           :domain (list 1 2)))
         (y (make-instance 'decision-variable
                           :name "y"
                           :domain (list 1 2)))
         (z (make-instance 'decision-variable
                           :name "z"
                           :domain (list 1 2)))


         (lvs (make-instance 'lvs-generic
                             :relation #'<)))

    (add-variable! ss x)
    (add-variable! ss y)
    (add-variable! ss z)


    (add-pair-generic lvs
              (make-instance 'lv-generic
                             :value 1.0
                             :label (make-environment-from-assignments ss (list (assignment x 1)))
                             :ss ss))

    (add-pair-generic lvs
              (make-instance 'lv-generic
                             :value 2.0
                             :label (make-environment-from-assignments ss (list))
                             :ss ss))

    (add-pair-generic lvs
              (make-instance 'lv-generic
                             :value 3.0
                             :label (make-environment-from-assignments ss (list (assignment x 1)))
                             :ss ss))

    lvs))
