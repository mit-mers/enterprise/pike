;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)


;;; Highly optimized version of a numerical lvs, suitable for fast APSP's
;;; Steve Levine (sjlevine@mit.edu)

(in-package #:pike)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Labeled values (LV's)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defstruct (lv (:print-function print-lv))
  (value (error "Must specify value!")
         :type number)
  (label (error "Must specify label!")
         :type environment)
  (ss (error "Must specify state space!")
      :type state-space-bits))


(defun print-lv (lv stream ignore)
  (declare (ignore ignore))
  (with-slots (value label ss) lv
    (format stream
            "(~a, "
            (cond ((= value +inf+) "∞")
                  ((= value +-inf+) "-∞")
                  (T value)))
    (print-environment label ss stream)
    (format stream ")")))



(declaim (inline create-lv))
(defun create-lv (ss value label)
  (declare (type state-space-bits ss)
           (type environment label))
  (make-lv :value value ;; (coerce value 'single-float)
           :label label
           :ss ss))


(declaim (inline create-lv-with-universal-env))
(defun create-lv-with-universal-env (ss &key (value 0.0))
  (create-lv ss value (make-environment ss)))


(declaim (inline dominates?))
(defun dominates? (lv1 lv2)
  (declare (type lv lv1 lv2))
  (and (<= (lv-value lv1) (lv-value lv2))
       (subsumes? (lv-label lv1) (lv-label lv2) (lv-ss lv1))))

(declaim (inline recast-lv))
(defun recast-lv (lv)
  (declare (type lv lv))
  (setf (lv-label lv) (recast-environment (lv-label lv) (lv-ss lv))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Labeled values sets (LVS's)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defstruct (lvs (:print-function print-lvs))
  (pairs nil :type list))


(defun print-lvs (lvs stream ignore)
  "Pretty-print an LVS"
  (declare (ignore ignore))
  (format stream "{~{~a~^, ~}}" (lvs-pairs lvs)))


(defun recast-lvs (lvs)
  (declare (type lvs lvs))
  (dolist (pair (lvs-pairs lvs))
    (recast-lv pair)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Core algorithms
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(declaim (inline add-pair))
(defun add-pair (lvs pair)
  "Add a pair to the LVS minimally."
  (declare (optimize (speed 3)))
  (declare (type lvs lvs)
           (type lv pair))

  (with-slots (pairs) lvs
    ;; Make sure pair isn't dominated by anything already in the lvs
    (dolist (p pairs)
      (declare (type lv p))
      (if (dominates? p pair)
          (return-from add-pair nil)))

    ;; not dominated! Remove any pairs that are dominated by pair.
    (setf pairs (delete-if #'(lambda (p)
                               (declare (optimize (speed 3)))
                               (dominates? pair p))
                           pairs))
    (push pair pairs)
    T))



(defun lvs-plus (ss lvs-a lvs-b)
  "Compute a new LVS that represents the max binary function (basically a cross product)"
  (declare (type state-space-bits ss)
           (type lvs lvs-a lvs-b))

  (let* ((lvs-result (make-lvs)))
    (dolist (pair-a (lvs-pairs lvs-a))
      (dolist (pair-b (lvs-pairs lvs-b))
        (let* ((e-m  (bit-ior (lv-label pair-a) (lv-label pair-b))))
          (declare (type environment e-m))
          ;; Compute the environmental union and see if it's valid
          (when (env-valid? ss e-m)
            (add-pair lvs-result
                      (make-lv :value (+ (lv-value pair-a) (lv-value pair-b))
                               :label e-m
                               :ss ss))))))
    lvs-result))

(defun lvs-minus (ss lvs-a lvs-b)
  "Compute a new LVS that represents the max binary function (basically a cross product)"
  (declare (type state-space-bits ss)
           (type lvs lvs-a lvs-b))

  (let* ((lvs-result (make-lvs)))
    (dolist (pair-a (lvs-pairs lvs-a))
      (dolist (pair-b (lvs-pairs lvs-b))
        (let* ((e-m  (bit-ior (lv-label pair-a) (lv-label pair-b))))
          (declare (type environment e-m))
          ;; Compute the environmental union and see if it's valid
          (when (env-valid? ss e-m)
            (add-pair lvs-result
                      (make-lv :value (- (lv-value pair-a) (lv-value pair-b))
                               :label e-m
                               :ss ss))))))
    lvs-result))


(defun lvs-max (ss lvs-a lvs-b)
  "Compute a new LVS that represents the max binary function (basically a cross product)"
  (declare (type state-space-bits ss)
           (type lvs lvs-a lvs-b))

  (let* ((lvs-result (make-lvs)))
    (dolist (pair-a (lvs-pairs lvs-a))
      (dolist (pair-b (lvs-pairs lvs-b))
        (let* ((e-m  (bit-ior (lv-label pair-a) (lv-label pair-b))))
          (declare (type environment e-m))
          ;; Compute the environmental union and see if it's valid
          (when (env-valid? ss e-m)
            (add-pair lvs-result
                      (make-lv :value (max (lv-value pair-a) (lv-value pair-b))
                               :label e-m
                               :ss ss))))))
    lvs-result))


(defun find-completions (ss lvs var-bg)
  "Given an LVS, and a set of variable assignment bit masks (all for a single variable), return an LVS
   containing suggestions to be added to the LVS for completion."
  (declare (type state-space-bits ss)
           (type lvs lvs)
           (type list var-bg))

  (let* ((W (make-lvs))
         (Y (make-lvs))
         (ready-to-cross? nil))

    (dolist (ass-bits var-bg)
      (declare (type environment ass-bits))
      ;; Clear Y
      (setf (lvs-pairs Y) nil)

      ;; Compute Y
      (dolist (pair (lvs-pairs lvs))
        (let* ((e-m (bit-andc2 (lv-label pair) ass-bits)))
          (declare (type environment e-m))
          (when (not (equal e-m (lv-label pair)))
            ;; The label contained this assignment, and it's now removed in e-m.
            (add-pair Y (make-lv :value (lv-value pair)
                                 :label e-m
                                 :ss ss)))))

      ;; If we're ready to cross, set W = W x Y
      (when ready-to-cross?
        (setf W (lvs-max ss W Y)))

      ;; If there's no W yet, just copy over to Y
      (unless ready-to-cross?
        (setf (lvs-pairs W) (lvs-pairs Y))
        (setf ready-to-cross? T)))
    W))


(declaim (inline get-all-assignments-to-assigned-variables))
(defun get-all-assignments-to-assigned-variables (ss e-b)
  (declare (type state-space-bits ss)
           (type environment e-b))
  (let ((variable-groups nil))
    (dotimes (i (state-space-bits-n-bits ss))
      (when (= (sbit e-b i) 1)
        (push (aref (state-space-bits-bit-index-to-all-variable-assignment-environments ss) i) variable-groups)))
    variable-groups))


(defun merge-lvs (lvs lvs-other)
  "Incorporates all of the labeled value pairs in lvs-other, into lvs."
  (dolist (pair (lvs-pairs lvs-other))
    (add-pair lvs pair)))


(defun merge-lvs-with-completions (ss lvs lvs-other)
  (declare (type state-space-bits ss)
           (type lvs lvs lvs-other))

  (let ((Q nil))
    (loop for pair_l in (lvs-pairs lvs-other)
       with added? do
         (setf added? (add-pair lvs pair_l))
         (when added?
           (dolist (variable-group (get-all-assignments-to-assigned-variables ss (lv-label pair_l)))
             (when (not (member variable-group Q :test #'equalp))
               (push variable-group Q)))))

    (loop while Q do
         (let* ((variable-group (pop Q))
                (completions-lvs (find-completions ss lvs variable-group)))

           (loop for pair_c in (lvs-pairs completions-lvs)
              with added? do
                (setf added? (add-pair lvs pair_c))
                (when added?
                  (dolist (variable-group (get-all-assignments-to-assigned-variables ss (lv-label pair_c)))
                    (when (not (member variable-group Q :test #'equalp))
                      (push variable-group Q)))))))))


(defun query-lvs (ss lvs env)
  "Find and return the smallest/tightest value in this LVS
   (corresponding to a pair (a_i, phi_i) ) where phi_i
   subsumes the given env."
  (declare (type state-space-bits ss)
           (type lvs lvs)
           (type environment env))
  (let ((a-i-tightest (coerce +inf+ 'single-float)))
    ;; (declare (type single-float a-i-tightest))
    (dolist (pair (lvs-pairs lvs))
      (declare (type lv pair))
      (let ((a-i (lv-value pair))
            (phi-i (lv-label pair)))
        (when (and (< a-i a-i-tightest)
                   (subsumes? phi-i env ss))
          (setf a-i-tightest a-i))))
    a-i-tightest))


(defun prune-lvs (lvs fn-env)
  "Acccepts an LVS and a function fn-env. Removes all
  labeled pairs from this LVS such that calling fn-env
  on the environment of that pair returns nil.

  If fn-env returns T for a pair, that pair will be kept.
  Otherwise, it will be pruned."
  (declare (type lvs lvs)
           (type function fn-env))
  (setf (lvs-pairs lvs)
        (delete-if #'(lambda (pair)
                       (declare (type lv pair))
                       (not (funcall fn-env (lv-label pair))))
                   (lvs-pairs lvs))))
