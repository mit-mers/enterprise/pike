;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
;;;
;;; An implementation of the piATMS, a prime-implicant generating ATMS.
;;;
;;;
;;;
;;; Note that we explicitly use structs instead of classes, for better type inference
;;; by some compilers (ex., SBCL).
(in-package #:pike)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; piATMS definitions / basic functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defstruct (piatms (:include atms) (:print-function print-piatms)))


(defun print-piatms (atms stream ignore)
  "Pretty print."
  (declare (ignore ignore))
  (format stream "#<piATMS ~a>" (atms-title atms)))


(defun create-piatms (title &key ss)
  "Create a new ATMS!"
  (let ((piatms (make-piatms :title title
                             :ss ss)))
    (with-accessors ((contra-node atms-contra-node)) piatms
      (setf contra-node (tms-create-node piatms
                                         "Contradiction"
                                         :contradictoryp T)))

    piatms))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Core algorithms
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun justify-node-piatms (reason
                     consequent
                     antecedents)
  (let ((just (make-just :reason reason
                         :consequent consequent
                         :antecedents antecedents))
        (zero-vector (state-space-bits-zero-vector (atms-ss (node-atms consequent)))))
    (push just (atms-justs (node-atms consequent)))
    ;; Register this just with each antecedent!
    (dolist (a antecedents)
      (push just (node-justs a)))

    ;; Start the propagation!
    (propagate-piatms (node-atms consequent) just nil (list zero-vector))))



;; (defmethod node-consistent-with? ((node node) e-b)
;;   (declare (optimize (speed 3)))
;;   (declare (type environment e-b))
;;   (with-slots (atms label) node
;;     (with-slots (ss nogoods) atms
;;       (with-slots (temp-scratch-space-2) ss
;;         (declare (type environment temp-scratch-space-2))
;;         (some #'(lambda (e-i)
;;                   (declare (type environment e-i))
;;                   (environment-union-in-place e-i e-b temp-scratch-space-2 ss)
;;                   (notany #'(lambda (e-nogood)
;;                               (declare (type simple-bit-vector e-nogood))
;;                               (subsumes? e-nogood temp-scratch-space-2 ss))
;;                           nogoods))
;;               label)))))


;; (defmethod node-consistent-with-after-conflicts? ((node node) e-b conflicts)
;;   (declare (type environment e-b))
;;   (declare (type list conflicts))
;;   (let* ((atms (node-atms node))
;;          (ss (atms-ss atms))
;;          (label (node-label node))
;;          (nogoods (atms-nogoods atms))
;;          (e-m (state-space-bits-temp-scratch-space-2 ss))
;;          (conflicts-minimal nil))

;;     (declare (type environment e-m)
;;              (type list conflicts-minimal)
;;              (type atms atms))

;;     ;; Make the list of conflicts minimal
;;     (dolist (c conflicts)
;;       (setf conflicts-minimal (add-minimal conflicts-minimal c ss)))

;;     (some #'(lambda (e-i)
;;               (declare (optimize (speed 3)))
;;               (declare (type environment e-i))
;;               (environment-union-in-place e-i e-b e-m ss)
;;               (and (notany #'(lambda (e-nogood)
;;                                (declare (optimize (speed 3)))
;;                                (declare (type environment e-nogood))
;;                                (subsumes? e-nogood e-m ss))
;;                            nogoods)
;;                    (notany #'(lambda (e-new-nogood)
;;                                (declare (optimize (speed 3)))
;;                                (declare (type environment e-new-nogood))
;;                                (subsumes? e-new-nogood e-m ss))
;;                            conflicts-minimal)))
;;           label)))


;; (declaim (inline in-node?))
;; (defmethod in-node? ((node node))
;;   ;; TODO Not quite the same as the BPS implementation
;;   (not (null (node-label node))))




(defun propagate-piatms (piatms J a I)
  (declare (optimize (speed 3)))
  (declare (type piatms piatms)
           (type list I)
           (type just J))
  (let ((L nil))
    (setf L (weave piatms a I (just-antecedents J)))
    (update-piatms piatms L (just-consequent J))))


;; TODO -- this needs to be fixed! The update(...) below
;; is slightly different than in BPS. Namely, it propagates
;; the entire L list -- but it should only propagate those that
;; were added minimally, so it is unncessarily inefficient in
;; some situations. Additionally, for the(dolist (j justs) ...)
;; at the end, we could possibly even prune some things from L
;; (since each call to (propagate ...) good potentially add more
;; no-goods, some of which could be in L). The pseudo code is outlined
;; in the BPS book.
(defun update-piatms (piatms L n)
  (declare (optimize (speed 3)))
  (declare (type piatms piatms)
           (type list L)
           (type node n))
  (with-slots (label contradictoryp justs) n
    (with-slots (nogoods ss) piatms
      (let ((L-added nil))
        (declare (type list L-added))

        (when contradictoryp
          ;; Add directly as no goods!

          ;; (dolist (e L)
          ;;   (setf nogoods (add-minimal nogoods e ss)))
          (let ((nogoods-added nil))
            (declare (type list nogoods-added))

            (multiple-value-setq (nogoods nogoods-added) (add-and-compactify nogoods L ss))

            ;; (prune-all-nogoods atms) ;; TODO: do splitting?
            (dolist (ec nogoods-added)
              (declare (type environment ec))
              (split-all-nodes-on-conflict piatms ec))

            (return-from update-piatms nil)))

        ;; If we get here, not a contradiciton - just add everything!
        ;; (dolist (e L)
        ;;   (setf label (add-minimal label e ss)))
        (multiple-value-setq (label L-added) (add-and-compactify label L ss))

        ;; Propagate recursively through any justifications containing
        ;; n as an antecedent.
        (dolist (j justs)
          (propagate-piatms piatms j n L-added))))))


(defun node-consistent-with-after-conflicts?-piatms (node e-b conflicts)
  "This method is similar to the ATMS version, except that we don't need to
   check existing conflicts -- only newly-added conflicts. This is because
   (as of this writing) every extension of any environment in a goal node
   of the piATMS is guaranteed to be an implicant / solution. They all
   necessarily resolve all conflicts, due to the splitting label procedures.

   TODO: Is this still the case?"

  (declare (optimize (speed 3))
           (type list conflicts)
           (type node node)
           (type environment e-b))
  (let* ((piatms (node-atms node))
         (ss (atms-ss piatms))
         (label (node-label node))
         (e-m (make-environment ss))
         (conflicts-minimal nil))

    (declare (type environment e-m)
             (type list conflicts-minimal)
             (type piatms piatms))

    ;; Make the list of conflicts minimal
    (dolist (c conflicts)
      (setf conflicts-minimal (add-minimal conflicts-minimal c ss)))

    (some #'(lambda (e-i)
              (declare (optimize (speed 3)))
              (declare (type environment e-i))
              (environment-union-in-place e-i e-b e-m ss)
              (and (env-valid? ss e-m)
                   (notany #'(lambda (e-new-nogood)
                               (declare (optimize (speed 3)))
                               (declare (type environment e-new-nogood))
                               (subsumes? e-new-nogood e-m ss))
                           conflicts-minimal)))
          label)))


(defun split-all-nodes-on-conflict (atms ec)
  "Splitting every node in the ATMS on the given conflict."
  (declare (optimize (speed 3)))
  (declare (type piatms atms)
           (type environment ec))
  (with-slots (nodes ss) atms
    (dolist (node nodes)
      ;; For this node, delete any labels that are subsumed by any
      ;; nogoods recorded by the ATMS
      (setf (node-label node)
            (split-label-on-conflict (node-label node) ec ss)))))
