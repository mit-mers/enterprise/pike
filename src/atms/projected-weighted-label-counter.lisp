;;;; Copyright (c) 2017 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
;;;
;;;
;;;
(in-package #:pike)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Projected, weighted label counter
;;
;; The functions and classes in this file allow you to
;; count the probability of a label, even when the
;; environments in that label are not mutually exclusive
;; (i.e., they may overlap). Essentially, this boils down
;; to the Inclusion-Exclusion principle. We implement
;; an incremental version of this that exploits pairwise
;; conflicts between pairs of environments that are mutually
;; exclusive, in an attempt to prune computation.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defstruct (pwlc (:print-function print-pwlc))
  (ss (error "Must specify a state space.")
      :type state-space-bits)
  (layers (make-array 1 :adjustable t :initial-element nil))
  (conflicts nil :type list)
  (weight 0)
  (environments nil :type list)
  (weight-fn #'(lambda (e) (declare (ignore e)) 0) :type function)
  (N 1))


(defun print-pwlc (pwlc s ignore)
  (declare (ignore ignore))
  (format s "#<PWLC with ~a nodes>" (pwlc-n pwlc)))


(defstruct (pwlc-node (:print-function print-pwlc-node))
  (pwlc)
  (combination nil :type list)
  (environment (error "Must specify environment") :type environment)
  (weight 0)
  (children nil :type list)
  (parents nil :type list))


(defun print-pwlc-node (pwlcn s ignore)
  (declare (ignore ignore))
  (format s "#<PWLC-NODE ~a>" (print-label (pwlc-node-combination pwlcn)
                                           (pwlc-ss (pwlc-node-pwlc pwlcn))
                                           nil)))


(defun create-pwlc (ss &key (weight-fn #'(lambda (x) (declare (ignore x)) 0)))
  (let* ((pwlc (make-pwlc :ss ss
                          :weight-fn weight-fn))
         (node-init (make-pwlc-node :environment (make-environment ss)
                                    :combination nil
                                    :weight 0
                                    :children nil
                                    :parents nil
                                    :pwlc pwlc)))
    (setf (aref (pwlc-layers pwlc) 0) (list node-init))
    pwlc))


(defun add-environment-to-pwlc (pwlc e)
  (with-slots (layers conflicts weight weight-fn ss environments N) pwlc
    ;; Add to the list of environments in the node (used to create "transitions")
    (push e environments)
    ;; We may need another layer, so add an additional layer
    (adjust-array layers (+ (length environments) 1) :initial-element nil)

    ;; Update conflicts
    (dolist (e-j environments)
      (unless (env-valid? ss (environment-union e-j e ss))
        ;; Add a new new conflict!
        (push `(,e-j ,e) conflicts)))

    (let (nodes-added-in-previous-step nodes-new)
      (dotimes (i (- (length layers) 1))
        ;; For each node that was originally in this layer: Add a new outgoing edge,
        ;; corresponding to unioning with this new environment e. Create additional
        ;; nodes in the next layer as needed.
        (dolist (node-i (set-difference (aref layers i) nodes-added-in-previous-step))
          (let* ((comb-child (concatenate 'list
                                          (pwlc-node-combination node-i)
                                          `(,e)))
                 e-child
                 weight-child
                 node-child)

            ;; If this combination manifests a conflict, don't continue processing
            ;; this node (or any of it's children!)
            (unless (some #'(lambda (c)
                              (subsetp c comb-child))
                          conflicts)
              ;; Not a conflict! Continue processing this node.
              (setf e-child (environment-union (pwlc-node-environment node-i) e ss))
              (setf weight-child (funcall weight-fn e-child))
              (setf node-child (get-node-with-combination pwlc comb-child))


              (when node-child
                ;; This node already exists: simply add a child reference
                (push node-child (pwlc-node-children node-i)))

              (unless node-child
                ;; This node didn't already exist -- make it!
                (setf node-child (make-pwlc-node :combination comb-child
                                                 :environment e-child
                                                 :weight weight-child
                                                 :children nil
                                                 :pwlc pwlc))
                ;; Connect the parent and child
                (connect-nodes node-i node-child)
                ;; Push the new node to the proper layer list
                (push node-child (aref layers (+ i 1)))
                ;; Also keep track that it'll be a considered a new node for the next layer.
                (push node-child nodes-new)
                ;; Add the corresponding weight to our global, incremetental weight,
                ;; taking into account the correct polarity
                (incf weight (* (if (oddp (+ i 1)) 1 -1) weight-child))
                ;; Finally, increment the global node counter
                (incf N)))))

        ;; For each new node that was just added to this layer in the last step,
        ;; create outgoing edges corresponding to all other environments in this label.
        ;; Exclude any edges though correspoinding to envs already in that combination.
        ;; (Dont' need to do this above because guaraunteed new since new env).
        (dolist (node-i nodes-added-in-previous-step)
          (dolist (e-j environments)
            (unless (member e-j (pwlc-node-combination node-i))
              (let* ((comb-child (concatenate 'list
                                              (pwlc-node-combination node-i)
                                              `(,e-j)))
                     ;; (e-child (environment-union (pwlc-node-environment node-i) e-j ss))
                     ;; (weight-child (funcall weight-fn e-child))
                     (node-child (get-node-with-combination pwlc comb-child)))


                ;; Any node here should have already been created by the earlier loop, over
                ;; nodes that originally existed, except for ones that weren't created because
                ;; they manifest conflicts -- and those ones can be ignored.
                (when node-child
                  ;; This node already exists: simply add a child reference
                  (connect-nodes node-i node-child))))))

        ;; Update the nodes added in previous step!
        (setf nodes-added-in-previous-step nodes-new)
        (setf nodes-new nil)))))




(defun remove-environment-from-pwlc (pwlc e)
  "Helper function to remove the given environment e from being counted.
   Must also remove all combinations that depend on e, and update the weights
   as appropriate."
  (with-slots (conflicts layers N weight environments) pwlc

    ;; Find the actual environment object that's equivalent
    ;; to e (note that it won't be the same exact object)
    (let ((e-del (find e environments :test #'equal)))
      (unless e-del
        (error "Couldn't find environment in pwlc, can't remove!"))

      ;; Remove e from environments
      (setf environments (delete e-del environments))

      ;; Remove any conflicts involving e
      (setf conflicts (delete-if #'(lambda (c)
                                     (member e-del c))
                                 conflicts))

      (let (nodes-to-remove-in-current-layer nodes-to-remove-in-next-layer)
        ;; Find the node in layer 1 that corresponds to just this assignment.
        ;; We'll want to delete that one, so mark it. Also, remove it from the
        ;; root node's children.
        (let ((node-e (get-node-with-combination pwlc `(,e-del)))
              (node-root (first (aref layers 0))))
          (push node-e nodes-to-remove-in-current-layer)
          (setf (pwlc-node-children node-root) (delete node-e (pwlc-node-children node-root))))

        ;; Loop over each layer: for each, remove the nodes from that layer that
        ;; we marked for removal from the last layer. Also update the probabilty
        ;; weighting appropriately. And decrement the node count.
        (loop for i from 1 to (- (length layers) 1) do
           ;; Actually delete each node from the layer
             (dolist (node-j nodes-to-remove-in-current-layer)
               (setf (aref layers i) (delete node-j (aref layers i)))
               (decf weight (* (if (oddp i) 1 -1) (pwlc-node-weight node-j)))
               (decf N)
               ;; Disconnect each node we're deleting from it's parents, so that next time
               ;; if there's another deletion we won't double count and try to delete this
               ;; node again
               (dolist (node-parent (pwlc-node-parents node-j))
                 (disconnect-nodes node-parent node-j))
               ;; For each of those nodes we've just removed, aggregate all of their children
               ;; -- those will be marked for removal in the next layer.
               (dolist (node-child (pwlc-node-children node-j))
                 (push node-child nodes-to-remove-in-next-layer))
               (setf nodes-to-remove-in-next-layer (remove-duplicates nodes-to-remove-in-next-layer)))

           ;; Move onto the next layer!
             (setf nodes-to-remove-in-current-layer nodes-to-remove-in-next-layer)
             (setf nodes-to-remove-in-next-layer nil)))

      ;; Make layer size smaller (provably the last layer is no longer needed!)
      (adjust-array layers (+ (length environments) 1) :initial-element nil))))



(defun get-weight (pwlc)
  (pwlc-weight pwlc))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Helper functions
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun get-node-with-combination (pwlc comb)
  (with-slots (layers) pwlc
    (let ((i (length comb))
          node-found)
      (setf node-found (find comb (aref layers i) :key #'pwlc-node-combination :test #'sets-equal))
      ;; If we found it, then return it!
      node-found)))


(defun connect-nodes (parent child)
  (push child (pwlc-node-children parent))
  (push parent (pwlc-node-parents child)))


(defun disconnect-nodes (parent child)
  (setf (pwlc-node-children parent)
        (delete child (pwlc-node-children parent)))
  (setf (pwlc-node-parents child)
        (delete parent (pwlc-node-parents child))))
