;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
;;;
;;; A highly-optmized, bit vector-based ATMS implementation
;;;
;;; Note that there may be algorithmic improvements possible (see BPS and papers) to make it even faster.
;;;
;;;
;;; Note that we explicitly use structs instead of classes, for better type inference
;;; by some compilers (ex., SBCL).
(in-package #:pike)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ATMS definitions / basic functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defstruct (atms (:print-function print-atms))
  (title "ATMS"
         :type string)
  (ss (error "Must specify a state space.")
      :type state-space-bits)
  (nodes nil
         :type list)
  (assumptions nil
               :type list)
  (justs nil
         :type list)
  (nogoods nil
           :type list)
  (contra-node nil))


(defun print-atms (atms stream ignore)
  "Pretty print."
  (declare (ignore ignore))
  (format stream "#<ATMS ~a>" (atms-title atms)))


(defun create-atms (title &key ss)
  "Create a new ATMS!"
  (let ((atms (make-atms :title title
                         :ss ss)))
    (with-accessors ((contra-node atms-contra-node)) atms
      (setf contra-node (tms-create-node atms
                                         "Contradiction"
                                         :contradictoryp T)))

    atms))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Node definitions / basic functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defstruct (node (:print-function print-node))
  (atms (error "Must specify ATMS")
        :type atms)
  (assumptionp nil
               :type (member nil T))
  (contradictoryp nil
                  :type (member nil T))
  (datum nil)
  (justs nil
         :type list)
  (label nil
         :type list))


;; For API compatibility with the BPS ATMS
(defun tms-node-assumption? (node)
  (declare (type node node))
  (node-assumptionp node))

;; For API compatibility with the BPS ATMS
(defun tms-node-contradictory? (node)
  (declare (type node node))
  (node-contradictoryp node))

;; For API compatibility with the BPS ATMS
(defun tms-node-datum (node)
  (declare (type node node))
  (node-datum node))


(defun print-node (node stream ignore)
  "Pretty print a node"
  (declare (ignore ignore))
  (cond ((node-assumptionp node)
         (format stream "#<ASSUMPTION-NODE ~a>" (node-datum node)))

        ((node-contradictoryp node)
         (format stream "#<CONTR-NODE ~a>" (node-datum node)))

        (T
         (format stream "#<NODE ~a>" (node-datum node)))))


(defgeneric tms-create-node (atms datum &key &allow-other-keys))
(defmethod tms-create-node ((atms atms)
                            datum &key
                                    (contradictoryp nil)
                                    (assumptionp nil))
  "Creates a new node in the ATMS."
  (with-slots (ss nodes assumptions) atms
    (let ((node (make-node :assumptionp (if assumptionp
                                            T
                                            nil)
                           :contradictoryp contradictoryp
                           :datum datum
                           :atms atms)))
      ;; If it's an assumption set up its bitmask
      (when assumptionp
        (let ((assignment assumptionp)
              environment)
          ;; If it's an assumption, we've just been passed in
          ;; a decision variable assignment. Set the label of this
          ;; assumption to contain the correspinding environment
          ;; for it.
          (setf environment
                (make-environment-from-assignments ss (list assignment)))

          ;; Also set the label to just be this assumption.
          (push environment (node-label node))
          (push node assumptions)))

      (push node nodes)
      node)))


(declaim (inline tms-node-label))
(defun tms-node-label (node)
  (declare (type node node))
  (node-label node))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Justification definitions / basic functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defstruct (just)
  (consequent nil :type node)
  (antecedents nil :type list)
  (reason nil))

;; For API compatibility with BPS ATMS
(defun just-consequence (just)
  (declare (type just just))
  (just-consequent just))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Core algorithms
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun justify-node (reason
                     consequent
                     antecedents)
  (let ((just (make-just :reason reason
                         :consequent consequent
                         :antecedents antecedents))
        (zero-vector (state-space-bits-zero-vector (atms-ss (node-atms consequent)))))
    (push just (atms-justs (node-atms consequent)))
    ;; Register this just with each antecedent!
    (dolist (a antecedents)
      (push just (node-justs a)))

    ;; Start the propagation!
    (propagate (node-atms consequent) just nil (list zero-vector))))



(defun why-node (node)
  (format t "{~%")
  (loop for e in (node-label node) do
       (format t "    ")
       (print-environment e (atms-ss (node-atms node)) t)
       (format t "~%"))
  (format t "}~%"))



(defun node-consistent-with? (node e-b)
  (declare (optimize (speed 3)))
  (declare (type node node)
           (type environment e-b))
  (let ((atms (node-atms node))
        (label (node-label node)))
    (with-slots (ss nogoods) atms
      (with-slots (temp-scratch-space-2) ss ;; TODO: Using temp-scratch-space-2 here is very dangerous!!
        (declare (type environment temp-scratch-space-2))
        (some #'(lambda (e-i)
                  (declare (optimize (speed 3)))
                  (declare (type environment e-i))
                  (environment-union-in-place e-i e-b temp-scratch-space-2 ss)
                  (notany #'(lambda (e-nogood)
                              (declare (optimize (speed 3)))
                              (declare (type simple-bit-vector e-nogood))
                              (subsumes? e-nogood temp-scratch-space-2 ss))
                          nogoods))
              label)))))



(defun node-consistent-with-after-conflicts? (node e-b conflicts)
  (declare (optimize (speed 3))
           (type list conflicts)
           (type node node)
           (type environment e-b))
  (let* ((atms (node-atms node))
         (ss (atms-ss atms))
         (label (node-label node))
         (nogoods (atms-nogoods atms))
         (e-m (make-environment ss))
         (conflicts-minimal nil))

    (declare (type environment e-m)
             (type list conflicts-minimal)
             (type atms atms))

    ;; Make the list of conflicts minimal
    (dolist (c conflicts)
      (setf conflicts-minimal (add-minimal conflicts-minimal c ss)))

    (some #'(lambda (e-i)
              (declare (optimize (speed 3)))
              (declare (type environment e-i))
              (environment-union-in-place e-i e-b e-m ss)
              (and (notany #'(lambda (e-nogood)
                               (declare (optimize (speed 3)))
                               (declare (type environment e-nogood))
                               (subsumes? e-nogood e-m ss))
                           nogoods)
                   (notany #'(lambda (e-new-nogood)
                               (declare (optimize (speed 3)))
                               (declare (type environment e-new-nogood))
                               (subsumes? e-new-nogood e-m ss))
                           conflicts-minimal)))
          label)))



(declaim (inline in-node?))
(defun in-node? (node)
  ;; TODO Not quite the same as the BPS implementation
  (declare (type node node))
  (not (null (node-label node))))


(defun propagate (atms J a I)
  (declare (optimize (speed 3)))
  (let ((L nil))
    (setf L (weave atms a I (just-antecedents J)))
    (update atms L (just-consequent J))))


;; TODO -- this needs to be fixed! The update(...) below
;; is slightly different than in BPS. Namely, it propagates
;; the entire L list -- but it should only propagate those that
;; were added minimally, so it is unncessarily inefficient in
;; some situations. Additionally, for the(dolist (j justs) ...)
;; at the end, we could possibly even prune some things from L
;; (since each call to (propagate ...) good potentially add more
;; no-goods, some of which could be in L). The pseudo code is outlined
;; in the BPS book.
;;
;; Update: Part of the above was addressed on 11.8.2016
(defun update (atms L n)
  (declare (optimize (speed 3)))
  (let ((added? nil))
    (declare (type atms atms)
             (type list L)
             (type node n)
             (type boolean added?))
    (with-slots (label contradictoryp justs) n
      (with-slots (nogoods ss) atms
        (when contradictoryp
          ;; Add directly as no goods!
          (let ((need-to-prune-nogoods? nil))
            (declare (type boolean need-to-prune-nogoods?))
            (dolist (e L)
              (multiple-value-setq (nogoods added?) (add-minimal nogoods e ss))
              (setf need-to-prune-nogoods? (or need-to-prune-nogoods? added?)))
            (when need-to-prune-nogoods?
              (prune-all-nogoods atms))
            (return-from update nil)))

        ;; If we get here, not a contradiction - just add everything!
        (let ((L-added nil))
          (declare (type list L-added))
          (dolist (e L)
            (multiple-value-setq (label added?) (add-minimal label e ss))
            (when added?
              (push e L-added)))
          ;; Propagate recursively through any justifications containing
          ;; n as an antecedent.
          (dolist (j justs)
            (propagate atms j n L-added)))))))



(defun weave (atms a I X)
  (declare #+:sbcl (sb-ext:muffle-conditions sb-ext:compiler-note))
  (declare (optimize (speed 3)))
  (declare (type atms atms))
  ;; TODO: Sort more intelligently here? Find list of labels we're taking the cross of, and call cross (which sorts automatically by label size)?
  ;; Would that be faster? I'm not positive, but I think it will. (Simple case: empty label, would terminate almost immediately).
  (with-slots (nogoods ss) atms
    (let ((I-new nil)
          (e-m (make-environment ss)))
      (dolist (h X)
        (unless (eq a h)
          (setf I-new nil)
          (dolist (e-i I)
            (declare (type environment e-i))
            (dolist (e-h (node-label h))
              (declare (environment e-h))
              (setf e-m (bit-ior e-i e-h)) ;; Deliberately creating a new one - not that much overhead
              ;; If e-m is not a no-good, add it to I-new!
              (when (notany #'(lambda (e-nogood)
                                (declare (optimize (speed 3)))
                                (declare (type environment e-nogood))
                                (subsumes? e-nogood e-m ss))
                            nogoods)

                (setf I-new (add-minimal I-new e-m ss)))))
          (setf I I-new)))
      I)))




(defun prune-all-nogoods (atms)
  ;; Loop for each node
  (declare (optimize (speed 3)))
  (with-slots (nogoods nodes ss) atms
    (dolist (node nodes)
      ;; For this node, delete any labels that are subsumed by any
      ;; nogoods recorded by the ATMS
      (setf (node-label node)
            (delete-if #'(lambda (e)
                           (declare (optimize (speed 3)))
                           (declare (type simple-bit-vector e))
                           (some #'(lambda (e-i)
                                     (declare (optimize (speed 3)))
                                     (declare (type environment e-i))
                                     (subsumes? e-i e ss))
                                 nogoods))
                       (node-label node))))))
