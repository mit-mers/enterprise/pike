;;;; Copyright (c) 2015 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Various conversion routines
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric atms-to-json (atms))
(defmethod atms-to-json ((atms atms))
  "Output a JSON representation of this ATMS."
  ;; Construct a hash table / dictionary {}, and
  ;; populate it with stuff.
  (let ((d (make-hash-table :test #'equal))
        (node-ht-index (make-hash-table))
        (just-ht-index (make-hash-table))
        nodes justs)

    ;; Associate each node with its index
    (let ((i 0))
      (dolist (node (atms-nodes atms))
        (setf (gethash node node-ht-index) (format nil "n~a" i))
        (incf i)))

    ;; Do the same with justifications
    (let ((i 0))
      (dolist (just (atms-justs atms))
        (setf (gethash just just-ht-index) (format nil "j~a" i))
        (incf i)))

    ;; Encode each node
    (dolist (node (atms-nodes atms))
      (let ((node-d (make-hash-table :test #'equal)))
        ;; Set the index
        (setf (gethash "id" node-d) (gethash node node-ht-index))
        ;; Set other fields
        (setf (gethash "contradiction" node-d) (node-contradictoryp node))
        (setf (gethash "assumption" node-d) (node-assumptionp node))
        (setf (gethash "tag" node-d) (format nil "~a" (node-datum node)))
        ;; Push it
        (push node-d nodes)))

    ;; Encode each justification
    (dolist (just (atms-justs atms))
      (let ((just-d (make-hash-table :test #'equal)))
        ;; Set the index
        (setf (gethash "id" just-d) (gethash just just-ht-index))
        ;; Set the other fields
        (setf (gethash "antecedents" just-d) (loop for n in (just-antecedents just)
                                                  collecting (gethash n node-ht-index)))
        (setf (gethash "consequent" just-d) (gethash (just-consequent just) node-ht-index))
        (setf (gethash "tag" just-d) (format nil "~a" (just-reason just)))
        ;; Push it
        (push just-d justs)))


    ;; Assemble it
    (setf (gethash "id" d) "atms")
    (setf (gethash "name" d) "ATMS")
    (setf (gethash "nodes" d) nodes)
    (setf (gethash "justifications" d) justs)

    ;; Encode the result in JSON and return it as a string
    (cl-json:encode-json-to-string d)))


(defgeneric atms-to-json-file (ps filename))
(defmethod atms-to-json-file ((ps pike-session) filename)
  "Helper method to write the ATMS in this pike session to a JSON file."
  (with-open-file (stream filename :direction :output :if-exists :supersede)
    (format stream (atms-to-json (constraint-knowledge-base-atms-atms (pike-constraint-knowledge-base ps))))))
