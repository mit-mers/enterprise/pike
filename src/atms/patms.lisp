;;;; Copyright (c) 2015-2017 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
;;;
;;; An experimental probabilistic ATMS.
;;;
;;;
(in-package #:pike)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ATMS definitions / basic functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defstruct (patms (:print-function print-patms)
                  (:include atms))
  (Q (pileup:make-heap #'>
                       :name "Priority Queue"
                       :key #'first)
     :type pileup:heap)
  (bni) ;; Bayesian network inference class
  (goal-node)
  ;; Tools for projected weighted label counting
  (L-goal-projected nil)
  (e-projection-mask)
  (pwlc))


(defun print-patms (patms stream ignore)
  "Pretty print."
  (declare (ignore ignore))
  (format stream "#<PATMS ~a>" (atms-title patms)))


(defun create-patms (title &key ss)
  "Create a new PATMS!"
  (let ((patms (make-patms :title title
                           :ss ss)))
    (with-slots (contra-node p-tables) patms
      (setf contra-node (tms-create-node patms
                                         "Contradiction"
                                         :contradictoryp T)))
    patms))


(defun set-patms-goal-node (patms n)
  (with-slots (goal-node pwlc ss bni) patms
    ;; Set the goal node
    (setf goal-node n)
    ;; Also set up a projected weighted label counter over
    ;; the goal node
    (setf pwlc (create-pwlc ss
                            :weight-fn #'(lambda (e)
                                           (compute-environment-probability patms e))))))


(defun set-bayesian-network-inference (patms b)
  (declare (type bayesian-network-inference b))
  (with-slots (bni) patms
    (setf bni b)))


;; TODO remove below -- no longer needed!

;; (defun set-dummy-probabilities (patms)
;;   (with-slots (p-tables ss) patms
;;     (set-uniform-independent-probabilities p-tables ss)))



;; (defun set-probabilities-from-description (patms description)
;;   "Helper function that sets the probabilities for all variables.
;;    For any assignments mentioned in the description, set to those variables.
;;    For all other variabes, set to uniform probabilities."
;;   (with-slots (p-tables ss) patms
;;     (let (vars-specified)
;;       ;; Add in all specified assignments
;;       (loop for (variable-name val prob) in description do
;;            (let ((var (find variable-name (state-space-bits-variables ss) :test #'equal :key #'decision-variable-name)))
;;              (add-marginal (assignment var val) prob p-tables)
;;              (unless (member var vars-specified) (push var vars-specified))))
;;       ;; For all the over variables, set to uniform.
;;       (with-slots (variables) ss
;;         (loop for var in variables when (not (member var vars-specified)) do
;;              (let* ((domain (decision-variable-values var))
;;                     (N (length domain)))
;;                (dolist (d domain)
;;                  ;; TODO HACK: put back!
;;                  ;;(add-marginal (assignment var d) (float (/ 1 N)) p-tables)
;;                  (add-marginal (assignment var d) 1 p-tables)
;;                  )))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Node definitions / basic functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod tms-create-node ((patms patms)
                            datum &key
                                    (contradictoryp nil)
                                    (assumptionp nil))
  "Creates a new node in the PATMS."
  (with-slots (ss nodes assumptions) patms
    (let ((node (make-node :assumptionp (if assumptionp
                                            T
                                            nil)
                           :contradictoryp contradictoryp
                           :datum datum
                           :atms patms)))

      ;; If it's an assumption set up its bitmask
      ;; and "note it" - i.e., add it to our priority Q
      (when assumptionp
        (let ((assignment assumptionp))
          (note-assumption-node patms node assignment)
          (push node assumptions)))

      (push node nodes)
      node)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Core algorithms
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric note-assumption-node (patms node assignment))
(defmethod note-assumption-node ((patms patms) node assignment)
  "Note an assumption node - i.e., add it to the priority Q"
  (with-slots (ss Q nogoods) patms
    (let ((e (make-environment-from-assignments ss (list assignment))))

      (unless (node-contradictoryp node)
        (let ((p (compute-environment-probability patms e)))
          (pileup:heap-insert (list p e node) Q))

        (when (node-contradictoryp node)
          (setf nogoods (add-minimal nogoods e ss))
          (prune-all-nogoods-patms patms))))))


(defun compute-environment-probability (patms env)
  "Makes a strong independence assumption."
  ;;(declare #+:sbcl (sb-ext:muffle-conditions sb-ext:compiler-note))
  (declare (optimize (speed 3))
           (type patms patms)
           (type environment env))
  (with-slots (bni e-projection-mask) patms
    (compute-marginal-probability bni :env (project-environment env e-projection-mask))))


(declaim (inline is-nogood?))
(defun is-nogood? (atms e)
  (declare #+:sbcl (sb-ext:muffle-conditions sb-ext:compiler-note))
  (declare (optimize (speed 3)))
  (declare (type atms atms)
           (type environment e))
  (with-slots (ss nogoods) atms
    (some #'(lambda (e-nogood)
              (declare (type environment e-nogood))
              (subsumes? e-nogood e ss))
          nogoods)))


(defun justify-node-patms (reason
                           consequent
                           antecedents)
  (declare (optimize (speed 3)))
  (let* ((patms (node-atms consequent))
         (just (make-just :reason reason
                          :consequent consequent
                          :antecedents antecedents))
         (env-empty (state-space-bits-zero-vector (atms-ss patms))))
    (push just (atms-justs (node-atms consequent)))
    ;; Register this just with each antecedent!
    (dolist (a antecedents)
      (push just (node-justs a)))

    ;; Start the propagation!
    (propagate-step patms just nil env-empty)))



(defun propagate-step (patms J a e)
  "Called if we just added an environment e to a's label. a is
   an antecedent of justification J."
  (declare #+:sbcl (sb-ext:muffle-conditions sb-ext:compiler-note))
  (declare (optimize (speed 3)))
  (with-slots (Q nogoods ss prop-counter) patms
    (let ((I nil))
      ;; Weave, sorted by label size! Added 10/27/2017
      (setf I (weave patms a (list e) (sort (copy-seq (just-antecedents J))
                                            #'>
                                            :key #'(lambda (x)
                                                     (length (node-label x))))))
      (unless (node-contradictoryp (just-consequent J))
        (dolist (e-i I)

          (unless (is-nogood? patms e-i)
            (let ((p (compute-environment-probability patms e-i)))
              (pileup:heap-insert (list p e-i (just-consequent J)) Q)))))

      (when (node-contradictoryp (just-consequent J))
        ;; We want to discover nogoods as early as possible. So if the
        ;; consequent is a contradiction, add it to the nogoods now
        ;; untiling waiting later for when we pop it from the Q!
        (dolist (e-i I)

          (setf nogoods (add-minimal nogoods e-i ss)))
        (prune-all-nogoods-patms patms))))
  (format t "P~%"))




(defun update-step (patms)
  (declare (optimize (speed 3)))
  (with-slots (Q ss env-added-callbacks goal-node) patms
    ;; Pop the highest probability (e, a) from the Q [the p is the first entry]
    (let* ((entry (pileup:heap-pop Q))
           (p (first entry))
           (e (second entry))
           (a (third entry))
           actually-added?
           label-new)
      ;; (format t "Popped (~a, ~a) with p=~a~%" e a p)
      ;; Add e to a's label minimally
      (multiple-value-setq (label-new actually-added?) (add-minimal (node-label a) e ss))
      (setf (node-label a) label-new)

      ;; Special case if this is the goal node!
      (when (eql a goal-node)
        ;; Trigger our projected weighted label counter
        (project-and-add-to-goal-with-counting patms e))

      (call-iteration-callback patms e a p)


      ;; Propagate through each justification where a is an antecedent
      (when actually-added?
        (dolist (J (node-justs a))
          (propagate-step patms J a e))))))




(defun prune-all-nogoods-patms (patms)
  ;; Loop for each node
  (declare #+:sbcl (sb-ext:muffle-conditions sb-ext:compiler-note))
  (declare (optimize (speed 3)))
  (with-slots (Q nodes ss) patms

    ;; Delete all entries from the priority Q that
    ;; we will no longer need.
    (let (Q-to-delete)
      ;; Collect a list of all entries to be pruned out
      (pileup:map-heap  #'(lambda (entry)
                            (declare (optimize (speed 3)))
                            (when (is-nogood? patms (second entry))
                              (push entry Q-to-delete)))
                        Q
                        :ordered nil)
      ;; ... and delete them
      (dolist (entry Q-to-delete)
        (pileup:heap-delete entry Q :count 1)))

    ;; Delete all entries from nodes.
    (dolist (node nodes)
      ;; For this node, delete any labels that are subsumed by any
      ;; nogoods recorded by the ATMS
      (setf (node-label node)
            (delete-if #'(lambda (e)
                           (declare (optimize (speed 3)))
                           (is-nogood? patms e))
                       (node-label node))))))


(defun compute-probability-of-disjoint-node (patms node)
  "Computes the total probability represented in the label of a node.
This method assumes that all environments in that label are pairwise disjoint;
hence the probabilities can be simply summed."
  (apply #'+
         (mapcar #'(lambda (phi)
                     (compute-environment-probability patms phi))
                 (node-label node))))



(defun project-and-add-to-goal-with-counting (patms e)
  "Project the given environment, and add it to our separate
  goal. Use projected weighted label counting to compute the probability
  of the goal."
  (with-slots (pwlc e-projection-mask L-goal-projected) patms
    (let* ((e-projected (project-environment e e-projection-mask))
           L-goal-projected-new L-added L-removed)

      (multiple-value-setq (L-goal-projected-new L-added L-removed) (add-minimal-return-delta L-goal-projected e-projected (patms-ss patms)) )
      (setf L-goal-projected L-goal-projected-new)

      ;; Now that we've kept track of the changes to the projected goal node, update the PWLC
      (dolist (e-removed L-removed)
        (remove-environment-from-pwlc pwlc e-removed))
      ;; Add in the new ones
      (dolist (e-added L-added)
        (add-environment-to-pwlc pwlc e-added)))))


(defvar *patms-iter-counter* 0)
(defvar *patms-trace* nil)

(defun call-iteration-callback (patms e a p)
  (incf *patms-iter-counter*)
  (push (list *patms-iter-counter*
              (unix-time)
              ;;(compute-probability-of-disjoint-node patms (patms-goal-node patms))
              (get-weight (patms-pwlc patms))
              e
              a
              p)
        *patms-trace*) )
  ;;(format t "Popped: ~a <- ~a (~f)~%" a (print-environment e (patms-ss patms) nil) p)
  ;;(format t "~f: N labels in goal: ~a~%" *patms-iter-counter* (length (node-label (patms-goal-node patms))))


(defun clear-patms-benchmarking ()
  (setf *patms-iter-counter* 0)
  (setf *patms-trace* nil))

(defun output-patms-benchmarking-to-file (filename)
  (let ((t-start 0)
        (trace (reverse *patms-trace*)))
    ;; If there's at least one entry, choose t-start (an offset) to be its time!
    (when trace
      (setf t-start (second (first trace))))

    (with-open-file (f filename :direction :output :if-exists :supersede :if-does-not-exist :create)
      (loop for (c time p-total e a p) in trace do
         ;; The following line outputs the environment being propagated. Can lead to much bigger files!
         ;; (format f "~f, ~f, ~f,\"~a\", \"~a\", ~f~%"
         ;;         c
         ;;         (- time t-start)
         ;;         p-total
         ;;         (print-environment e (patms-ss patms) nil)
         ;;         a
         ;;         p)

         ;; The following line does not output the environment being propagated.
           (format f "~f, ~f, ~f, \"~a\", ~f~%"
                   c
                   (- time t-start)
                   p-total
                   a
                   p)))))
