;;;; Copyright (c) 2015 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)

(in-package #:pike)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Implements strong controllability for labeled distance graphs (the
;; labeled generalization of distance graphs). The algorithm is implemented
;; as noted in our ICAPS 2015 Paper: "Robust Execution of Plans for Human-
;; Robot Teams" (Karpas, Levine, Yu, Williams 2015). Namely, it is very similar
;; to the normal strong controllabilty algorithm, which maps an distance graph
;; with contingent (uncontrollable) links to a new distance graph by adding
;; temporal constraints (i.e., basically converting an STNU to an STN).
;;
;; The big difference with this version is that we must consider guard conditions,
;; as noted in the paper. They aren't hard to compute, just the conjunction
;; of the appropriate edges, but they are important and must be considered.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric add-strong-controllability-constraints (ps &key &allow-other-keys))
(defmethod add-strong-controllability-constraints ((ps pike-session) &key (remove-original? T))
  "Adds appropriate temporal constraints to the this labeled distance
   graph to enforce strong controllability.

   This algorithm works as follows:
       - Get the list of all edges that are 'dirty' - i.e., requirements links
         that touch an uncontrollable (square) event
       - For each of those edges, repeatedely 'slide it backwards' by applying
         similar reductions as noted in (Vidal & Fargier 1999), but also
         keeping track of labels
       - Add these new constraints to the ldg (and optionally remove original edges)"

  (with-slots (ldg ss) ps
    (let (dirty-edges)

      ;; Assemble the list of "dirty" edges - i.e., those that are NOT
      ;; contingent links, but still touch an uncontrollable event. These
      ;; are the edges that will be dominated away.
      (dolist (edge (mtk-graph:get-edge-list ldg))
        (let ((e-from (first (mtk-graph:get-vertices edge)))
              (e-to (second (mtk-graph:get-vertices edge))))
          (when (and (not (contingent-link? edge ldg))
                     (or (event-uncontrollable? e-from ldg)
                         (event-uncontrollable? e-to ldg)))
            (push edge dirty-edges))))


      ;; Iterate over the dirty edges, and for each,
      ;; repeatedely 'slide it back' until it is between two controllable events.
      (dolist (edge dirty-edges)
        (let ((e-from (first (mtk-graph:get-vertices edge)))
              (e-to (second (mtk-graph:get-vertices edge)))
              (w (mtk-graph:get-weight edge ldg))
              contingent-edge u -l)

          ;; Keep sliding it back until both the from and to events are controllable.
          (loop until (and (not (event-uncontrollable? e-from ldg))
                           (not (event-uncontrollable? e-to ldg))) do
             ;; We know that at least one of the from/to events in this edge
             ;; touch an uncontrollable event. Switch on it.
               (cond
                 ((event-uncontrollable? e-to ldg)
                  ;; Case I: Upperbounds
                  ;; Get the incoming contingent edge (there must be exactly 1 by SC requirements)
                  ;; This will be an upper bound edge, call it u.
                  (setf contingent-edge (get-contingent-link-of-uncontrollable-event e-to ldg :incoming))
                  (setf u (mtk-graph:get-weight contingent-edge ldg))
                  ;; Slide it back - move e-to to the start of contingent,
                  ;; and compute an updated LVS weight
                  ;; TODO (should this [and below] take into account LVS completions?)
                  (setf e-to (first (mtk-graph:get-vertices contingent-edge)))
                  (setf w (lvs-minus ss w u)))

                 ((event-uncontrollable? e-from ldg)
                  ;; case II: Lowerbounds
                  ;; Get the outgoing contingent edge (there must be exactly 1 by SC requirements)
                  ;; This will be a lowerbound edge. Call it -l (since it's a distance graph, remember)
                  (setf contingent-edge (get-contingent-link-of-uncontrollable-event e-from ldg :outgoing))
                  (setf -l (mtk-graph:get-weight contingent-edge ldg))
                  ;; Slide it back - move e-from back to target of contingent link
                  ;; and compute an updated LVS weight.
                  (setf e-from (second (mtk-graph:get-vertices contingent-edge)))
                  (setf w (lvs-minus ss w -l)))))

          ;; When we get here, e-from, e-to, and w are all set to add a new constraint.
          ;; e-from and e-to are guaranteed to be controllable. Add it!
          (add-edge e-from e-to w ldg)
          ;; Optionally remove the original edge
          (when remove-original?
            (remove-edge edge ldg)))))))



(defun get-contingent-link-of-uncontrollable-event (e-uncontrollable ldg direction)
  "Direction must be either :incoming or :outgoing. Get the contingent link corresponding to
   this uncontrollable event. Note that by uncontrollability semantics, there must be exactly 1."
  (declare (type labeled-distance-graph ldg))
  (let (edges)
    (cond
      ((equal direction :outgoing)
       (setf edges (mtk-graph:find-edges-with-source-vertex e-uncontrollable ldg)))
      ((equal direction :incoming)
       (setf edges (mtk-graph:find-edges-with-target-vertex e-uncontrollable ldg)))
      (T
       (error "Direction must be either :incoming or :outgoing")))

    (dolist (edge edges)
      (when (contingent-link? edge ldg)
        (return-from get-contingent-link-of-uncontrollable-event edge)))

    ;; If we get here, our invariant that there must be exactly one contingent link has
    ;; somehow been compromised! Throw an error.
    (error "No contingent link corresponding to uncontrollable event; invariant / semantics compromised!")))
