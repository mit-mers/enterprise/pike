(in-package #:pike)

(defconstant +inf+ float-features:double-float-positive-infinity
  "The value used to represent infinity when parsing numbers.")

(defconstant +-inf+ float-features:double-float-negative-infinity
  "The value used to represent -infinity when parsing numbers.")
