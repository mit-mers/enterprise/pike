;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)

;; TODO - should rename the file that this is in
(defgeneric initialize-from-plan-and-plant-model (ps &key &allow-other-keys))
(defmethod initialize-from-plan-and-plant-model ((ps pike-session) &key (rationalize t) (apply-initial-choices? t) (add-unobserved-domain-values t))
  "Processes the loaded plan and plant model to produce useful
   internal representations:
      1.) A labeled distance graph (LDG) based on the TPN
      2.) A state space (for the decision variables)
      3.) A set of activities (consistent with the plant model)
      4.) An initial state
  "
  (with-accessors ((ldg pike-ldg)
                   (tpn pike-tpn)
                   (plant-model pike-plant-model)
                   (activities pike-activities)
                   (ss pike-state-space)
                   (initial-state pike-initial-state)
                   (goal-state pike-goal-state)
                   (po pike-options)) ps

    (let* ((tpn-event->ldg-event (make-hash-table))
           (tpn-dv->ss-dv (make-hash-table))
           (env-initial-choices nil))

      ;; Set up the state space with the decision variables
      (do-decision-variables (tpn-dv tpn)
        (let (ss-dv
              (guard-true? (or (not (tpn:decision-variable-at-event tpn-dv))
                                (is-guard-true? (tpn:guard (tpn:decision-variable-at-event tpn-dv))))))
          (setf ss-dv (make-instance 'decision-variable
                                     :name (string (tpn:id tpn-dv))
                                     :domain `(,@(loop for tpn-dv-value in (tpn:decision-variable-domain tpn-dv)
                                                    collecting (tpn:decision-variable-value-name tpn-dv-value))
                                                 ,@(if guard-true?
                                                       nil
                                                       (list "∘")))
                                     :utilities `(,@(loop for tpn-dv-value in (tpn:decision-variable-domain tpn-dv)
                                                       collecting (tpn:decision-variable-value-utility tpn-dv-value))
                                                    ,@(if guard-true?
                                                          nil
                                                          (list 0))) ;; A bit of a hack?
                                     :controllable? (ecase (tpn:decision-variable-type tpn-dv)
                                                      (:controllable T)
                                                      (:uncontrollable nil)
                                                      (:probabilistic nil))))
          (setf (gethash tpn-dv tpn-dv->ss-dv) ss-dv)
          (add-variable! ss ss-dv)))

      ;; Create the env corresponding to any initial choice assignments
      (setf env-initial-choices (make-environment-from-assignments ss (loop for (var-name val-name) in (pike-options-initial-choice-assignments po)
                                                                         collecting (find-variable-assignment ps var-name val-name))))

      ;; Create an event for each TPN event (we use a
      ;; different data structure), and keep track
      ;; of the mapping.
      (do-events (e tpn)
        (let ((guard (convert-guard-to-environment (tpn:guard e) ss tpn-dv->ss-dv)))
          (when apply-initial-choices?
            (setf guard (apply-initial-assignments-to-guard guard env-initial-choices ss)))
          (when guard ;; Might be nil after simplification
            (setf (gethash e tpn-event->ldg-event)
                  (make-instance 'event
                                 :id (tpn:id e)
                                 :name (tpn:name e)
                                 :guard guard)))))


      ;; Now, extract activities using the plant model.
      (do-constraints (c tpn)
        (let ((dispatch (tpn:dispatch c))
              action
              activity start-event end-event)
          (when (is-dispatch-field? dispatch)
            (setf action (parse-action dispatch plant-model))
            (setf start-event (gethash (tpn:from-event c) tpn-event->ldg-event))
            (setf end-event (gethash (tpn:to-event c) tpn-event->ldg-event))
            (when (and start-event end-event)
              (setf activity (make-instance 'activity
                                            :action action
                                            :id (tpn:id c)
                                            :start-event start-event
                                            :end-event end-event))
              (push activity activities)
              (setf (event-activity-start start-event) activity)
              (setf (event-activity-end end-event) activity)
              ;; Associate preconditions and effects with corresponding events

              ;; TODO: think about :over-all conditions / effects
              (setf (preconditions start-event) (get-conditions action :at-start))
              (setf (effects start-event) (get-effects action :at-start))
              (setf (preconditions end-event) (get-conditions action :at-end))
              (setf (effects end-event) (get-effects action :at-end))))))

      ;; Now, construct the LDG.
      (setf ldg (make-instance 'labeled-distance-graph))

      ;; Add all events into the LDG (set as controllable for now)
      (loop for ldg-event being the hash-values of tpn-event->ldg-event do
           (add-event ldg-event ldg :uncontrollable? nil))

      ;; Add all temporal constraints into the LDG
      (do-constraints (c tpn)
        (let ((event-start (gethash (tpn:from-event c) tpn-event->ldg-event))
              (event-end (gethash (tpn:to-event c) tpn-event->ldg-event))
              (lvs-ub (make-lvs))
              (lvs-lb (make-lvs))
              (is-contingent? (typep c 'tpn:simple-guarded-contingent-temporal-constraint))
              (guard (convert-guard-to-environment (tpn:guard c) ss tpn-dv->ss-dv)))
          ;; Possibly simplify the guard...
          (when apply-initial-choices?
            (setf guard (apply-initial-assignments-to-guard guard env-initial-choices ss)))
          (when guard
            ;; Create two lvs's: lvs-ub and lvs-lb, that will be the weights
            ;; on the two edges.
            (add-pair lvs-ub
                      (create-lv ss
                                 (if rationalize
                                     (rationalize-number (tpn:upper-bound c))
                                     (tpn:upper-bound c))
                                 guard))
            (add-pair lvs-lb
                      (create-lv ss
                                 (- (if rationalize
                                        (rationalize-number (tpn:lower-bound c))
                                        (tpn:lower-bound c)))
                                 guard))

            ;; Add the edges with these lvs's
            (add-edge event-start event-end lvs-ub ldg :contingent? is-contingent?)
            (add-edge event-end event-start lvs-lb ldg :contingent? is-contingent?)
            ;; Also mark the end event as possibly uncontrollable.
            (when is-contingent? (mark-event-as-uncontrollable event-end ldg)))))


      ;; Enforce that all dispatchable actions take positive time, i.e.
      ;; in [epsilon, infinity]. If they don't, manually add such
      ;; a constraint!
      (do-constraints (c tpn)
        (when (is-dispatch-field? (tpn:dispatch c))
          (let ((event-start (gethash (tpn:from-event c) tpn-event->ldg-event))
                (event-end (gethash (tpn:to-event c) tpn-event->ldg-event))
                (lb (tpn:lower-bound c))
                (guard (convert-guard-to-environment (tpn:guard c) ss tpn-dv->ss-dv)))
            ;; Possibly simplify guard...
            (when apply-initial-choices?
              (setf guard (apply-initial-assignments-to-guard guard env-initial-choices ss)))
            (when (and guard event-start event-end
                       (<= lb 0))
              (let ((lvs-ub (make-lvs))
                    (lvs-lb (make-lvs)))
                ;; Create two lvs's: lvs-ub and lvs-lb, that will be the weights
                ;; on the two edges.
                (add-pair lvs-ub
                          (create-lv ss
                                     +inf+
                                     guard))
                (add-pair lvs-lb
                          (create-lv ss
                                     (- (if rationalize
                                            (rationalize-number 0.001)
                                            0.001)) ;; TODO - don't hard code
                                     guard))

                ;; TODO debug print this
                (when-debug (ps)
                            (format t "Adding implicit ordering constraint from event ~a to event ~a~%" (id event-start) (id event-end)))

                ;; Add the edges with these lvs's
                (add-edge event-start event-end lvs-ub ldg)
                (add-edge event-end event-start lvs-lb ldg))))))

      ;; Associate decision variables with the events at which they occur
      (with-slots (decision-variable->at-event at-event->decision-variable) ldg
        (do-decision-variables (dv tpn)
          (let* ((ss-dv (gethash dv tpn-dv->ss-dv))
                 (ss-event (gethash (tpn:decision-variable-at-event dv) tpn-event->ldg-event)))
            (setf (gethash ss-dv decision-variable->at-event) ss-event)
            (when ss-event
              (setf (gethash ss-event at-event->decision-variable) ss-dv)))))

      ;; Add the special start and end events
      (setup-start-and-end-events ps ldg :rationalize rationalize))))

(defgeneric setup-start-and-end-events (ps ldg &key &allow-other-keys))
(defmethod setup-start-and-end-events ((ps pike-session) (ldg labeled-distance-graph) &key (rationalize t))
  (with-slots (ss initial-state goal-state plant-model) ps
    ;; Set up the initial and states
    (setf initial-state (get-initial-state ps plant-model))
    (setf goal-state (get-goal-state ps plant-model))

    ;; Create special start and end events (not to be dispatched)
    ;; and corresponding ordering constraints, so that the initial state
    ;; and goal states can be asserted for causal link extraction.
    (let ((event-initial (make-instance 'event
                                        :id '#:|ev-initial|
                                        :name "ev-initial"
                                        :guard (make-environment ss)
                                        :effects (state-conditions initial-state)))
          (event-final (make-instance 'event
                                      :id '#:|ev-final|
                                      :name "ev-final"
                                      :guard (make-environment ss)
                                      :preconditions (state-conditions goal-state))))

      (setf (event-initial ldg) event-initial)
      (setf (event-final ldg) event-final)

      ;; Mark them as controllable
      (setf (gethash event-initial (ldg-uncontrollable-events ldg)) nil)
      (setf (gethash event-final (ldg-uncontrollable-events ldg)) nil)

      ;; Add these two events to the ldg and add appropriate ordering constraints
      (add-event event-initial ldg)
      (dolist (e (mtk-graph:get-vertices ldg))
        (unless (eql e event-initial)
          (let ((lvs-ub (make-lvs))
                (lvs-lb (make-lvs)))
            ;; Create two lvs's: lvs-ub and lvs-lb, that will be the weights
            ;; on the two edges.
            (add-pair lvs-ub
                      (create-lv ss
                                 +inf+
                                 (make-environment ss)))
            (add-pair lvs-lb
                      (create-lv ss
                                 (if rationalize
                                     (rationalize-number (- 0.001)) ;; 0.00001
                                     (- 0.001))
                                 (make-environment ss)))

            ;; Add the edges with these lvs's
            (add-edge event-initial e lvs-ub ldg)
            (add-edge e event-initial lvs-lb ldg))))


      (add-event event-final ldg)
      (dolist (e (mtk-graph:get-vertices ldg))
        (unless (eql e event-final)
          (let ((lvs-ub (make-lvs))
                (lvs-lb (make-lvs)))
            ;; Create two lvs's: lvs-ub and lvs-lb, that will be the weights
            ;; on the two edges.
            (add-pair lvs-ub
                      (create-lv ss
                                 +inf+
                                 (make-environment ss)))
            (add-pair lvs-lb
                      (create-lv ss
                                 (if rationalize
                                     (rationalize-number (- 0.001))
                                     (- 0.001))
                                 (make-environment ss)))

            ;; Add the edges with these lvs's
            (add-edge e event-final lvs-ub ldg)
            (add-edge event-final e lvs-lb ldg)))))))



(defun is-dispatch-field? (disp)
  "Helper method that determines if we have a dispatch field."
  (and (stringp disp)
       (not (equal (string-trim '(#\Space) disp) ""))))



(defun convert-guard-to-environment (guard ss tpn-dv->ss-dv)
  "Helper method that converts a guard condition (from the TPN datastructures)
   into an environment (i.e., a bit vector). This involves walking up the guard tree
   recursively to get pre-requisities, as well as converting between the data structures."
  (declare (type tpn:boolean-expression guard))
  (make-environment-from-assignments
   ss
   (expand-guard-assignment guard ss tpn-dv->ss-dv)))


(defun apply-initial-assignments-to-guard (env-guard env-initial ss)
  "Helper function that, given a guard for something, as well as an env representing
initial assignments, simplifies the guard. In particular:
  - If env-initial |= env-guard, just return the empty environment representing True.
  - If env-initial & env-guard is not self-consistent, return nil (representing False)
  - Otherwise, return env-guard unchanged."
  (cond
    ((subsumes? env-guard env-initial ss)
     (make-environment ss))
    ((not (env-valid? ss (environment-union env-guard env-initial ss)))
     nil)
    (t
     env-guard)))


(defgeneric expand-guard-assignment (obj ss tpn-dv->ss-dv))
(defmethod expand-guard-assignment ((be tpn:boolean-expression) ss tpn-dv->ss-dv)
  ;; If there's no more specific applicable method, not supported!
  ;; This sffectively prevents things like disjunctions in guards.
  (error "Unsupported boolean expression in guard!"))


(defmethod expand-guard-assignment ((bc tpn:boolean-conjunction) ss tpn-dv->ss-dv)
  (let (expanded-guard)
    (dolist (conjunct (tpn:boolean-conjunction-conjuncts bc))
      (dolist (term (expand-guard-assignment conjunct ss tpn-dv->ss-dv))
        (push term expanded-guard)))
    expanded-guard))

(defmethod expand-guard-assignment ((dva tpn:decision-variable-assignment) ss tpn-dv->ss-dv)
  (let ((expanded-guard (expand-guard-assignment (tpn:guard (tpn:decision-variable-assignment-variable dva)) ss tpn-dv->ss-dv)))
    (push (assignment (gethash (tpn:decision-variable-assignment-variable dva) tpn-dv->ss-dv)
                      (tpn:decision-variable-assignment-value dva))
          expanded-guard)
    expanded-guard))

(defmethod expand-guard-assignment ((bd tpn:boolean-disjunction) ss tpn-dv->ss-dv)
  "Allow disjunctions, but not really! If this disjunction contains ONLY a single conjuction,
  it is essentially a conjunction and we should treat it as such. Same goes for a single
  decision variable assignment. Otherwise, if there are more than one disjuncts inside of
  this disjunction, don't allow it."
  ;; An empty disjunction is equivalent to false. Throw an error if we ever see one (as we
  ;; do with false below).
  (when (= (length (tpn:boolean-disjunction-disjuncts bd)) 0)
    (error "Empty disjunction found in guard, which is equivalent to False! Did you make a mistake?"))

  (when (= (length (tpn:boolean-disjunction-disjuncts bd)) 1)
    (let ((dis (first (tpn:boolean-disjunction-disjuncts bd))))
      ;; We found exactly one disjunct.
      ;; It's okay if it's a conjunction, variable assignment, constant that's true, or
      ;; another single disjunction
      (return-from expand-guard-assignment (expand-guard-assignment dis ss tpn-dv->ss-dv))))
  (error "Disjunctions not supported in guards! Any real disjunction (with more than one disjunct) isn't allowed."))


(defmethod expand-guard-assignment ((bc tpn:boolean-constant) ss tpn-dv->ss-dv)
  (if (tpn:boolean-constant-value bc)
      (list)
      (error "False guard! What?")))



(defun is-guard-true? (be)
  "Helper function that checks if a guard is equivalent to True or not."
  (cond
    ((typep be 'tpn:decision-variable-assignment) nil)
    ((typep be 'tpn:boolean-constant) (tpn:boolean-constant-value be))
    ((typep be 'tpn:boolean-conjunction) (every #'is-guard-true? (tpn:boolean-conjunction-conjuncts be)))
    ((typep be 'tpn:boolean-disjunction) (some #'is-guard-true? (tpn:boolean-disjunction-disjuncts be)))
    (t nil)))
