;;;; Copyright (c) 2011-2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)


(defclass labeled-distance-graph (mtk-graph:weighted-directed-graph)
  ((event-initial
    :type event
    :accessor event-initial
    :initarg :event-initial
    :documentation "A special initial / start event")
   (event-final
    :type event
    :accessor event-final
    :initarg :event-final
    :documentation "A special final / end event")
   (contingent-links
    :type hash-table
    :initform (make-hash-table :test #'eql)
    :accessor ldg-contingent-links
    :documentation "A hash table mapping edges in the graph to T if they're
contingent, or nil if they're not.")
   (uncontrollable-events
    :type hash-table
    :initform (make-hash-table :test #'eql)
    :accessor ldg-uncontrollable-events
    :documentation "A hash table mapping events in the graph to T if they're
uncontrollable (denoted by a square in literature), or nil if they're
free / controllable (denoted by a circle).")
   (decision-variable->at-event
    :type hash-table
    :initform (make-hash-table :test #'eql)
    :accessor ldg-decision-variable->at-event
    :documentation "A hash table mapping decision variables from the state space represented in the TPN,
to the events at which they must be made or observed at.")
   (at-event->decision-variable
    :type hash-table
    :initform (make-hash-table :test #'eql)
    :accessor ldg-at-event->decision-variable
    :documentation "The reverse of the above; a hash table mapping events in the TPN to the corresponding
decision variables that must be made there.")))


(defmethod print-object ((ldg labeled-distance-graph) stream)
  (call-next-method ldg stream))


(defun event-uncontrollable? (e ldg)
  (declare (type labeled-distance-graph ldg))
  (gethash e (ldg-uncontrollable-events ldg)))


(defun contingent-link? (edge ldg)
  (declare (type labeled-distance-graph ldg))
  (gethash edge (ldg-contingent-links ldg)))


(defun add-event (e ldg &key (uncontrollable? nil))
  "Helper method to add an event."
  (declare (type labeled-distance-graph ldg))
  (mtk-graph:add-vertex e ldg)
  (setf (gethash e (ldg-uncontrollable-events ldg)) uncontrollable?))


(defun add-edge (e-from e-to w ldg &key (contingent? nil))
  "Adds the given edge from e-from to e-to with weight w to the ldg"
  (declare (type labeled-distance-graph ldg))
  (let ((edge (mtk-graph:add-weighted-edge (list (list e-from e-to) w) ldg)))
    (setf (gethash edge (ldg-contingent-links ldg)) contingent?)
    edge))


(defun remove-edge (edge ldg)
  "Remove the given edge."
  (declare (type labeled-distance-graph ldg))
  (mtk-graph:delete-weighted-edge edge ldg)
  (remhash edge (ldg-contingent-links ldg)))


(defun mark-event-as-uncontrollable (event ldg)
  "Helper function to mark the given event as uncontrollable."
  (declare (type labeled-distance-graph ldg))
  (setf (gethash event (ldg-uncontrollable-events ldg)) T))


(defun recast-ldg (ldg ss)
  ;; Recast event environments
  (declare (type labeled-distance-graph ldg))
  (dolist (event (mtk-graph:get-vertices ldg))
    (with-slots (guard) event
      (setf guard (recast-environment guard ss))))
  (setf (guard (event-initial ldg)) (recast-environment (guard (event-initial ldg)) ss))
  (setf (guard (event-final ldg)) (recast-environment (guard (event-final ldg)) ss))

  ;; Recast edge weights
  (loop for edge being the hash-keys of (mtk-graph:get-weight-ht-edge ldg)
     using (hash-value weight) do
       (recast-lvs weight)))
