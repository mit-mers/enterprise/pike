;;;; Copyright (c) 2014-2017 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)


(defgeneric plan-looks-sane? (ps))
(defmethod plan-looks-sane? ((ps pike-session))
  ;; TODO - these checks need to be implemented!

  ;; All episodes with dispatch fields have unique start and end events


  ;; Start & end event guards for dispatch episodes are the same


  ;; Guard of episode entails / implies guard of start and end events


  ;; All episodes must be in [0, infinity]


  ;; All dispatchable activities must be in [epsilon, infinity] - added if not


  ;; All dispatchable activities are uncontrollable? And those are the only
  ;; uncontrollable things?


  ;; Look for (and warn about?) about non-overlapping
  ;; dispatch windows (see test case todo-1)


  ;; Strong controllability: that all of the end events of
  ;; uncontrollable durations are unique. I.e., you can't have
  ;; two uncontrollable durations ending at the same event.

  ;; Strong controllability: ub >= lb >= 0


  t)


(defgeneric compile-plan (ps))
(defmethod compile-plan ((ps pike-session))
  "Top-level compilation function. This prepares
   Pike for execution, and may throw errors if problems
   are detected."

  ;; TODO - check success for each for each of these!
  ;; Use signals, etc. Make sure each of these throws
  ;; properly.

  ;; Load the TPN plan file
  (load-plan ps)

  ;; Load an appropriate plant model for this plan
  ;; ex., PDDL, RMPL, NO-OP, etc.
  (load-plant-model ps)

  ;; Check validity of TPN
  (unless (plan-looks-sane? ps)
    (return-from compile-plan nil))

  ;; Initializes internal data structures
  ;; from the now-loaded TPN and plant model
  (initialize-from-plan-and-plant-model ps)

  ;; Compile away the conditional nature of the problem
  ;; Adds an "inactivated" value to some variables, and
  ;; some extra constraints
  (process-conditional-variables ps)

  ;; If applicable, load a probability distribution
  ;; over the uncontrollable variables.
  (load-probability-distribution ps)

  ;; Possibly add strong controllability constraints
  ;; for uncontrollable durations in plan.
  (apply-strong-controllability ps)

  ;; If desired for debugging, output an SMT2-compatible file
  ;; containing an encoding of all relevant temporal and
  ;; causal link constraints
  (when (pike-generate-debug-files? (pike-options ps))
    (write-smt-file ps "pike-smt-encoding.smt2"))

  ;; Run labeled APSP on LDG
  (compute-labeled-apsp ps)

  ;; If desired, output this labeled APSP table
  (when (pike-generate-debug-files? (pike-options ps))
    (write-apsp-table-to-file ps "pike-apsp-first.txt"))

  ;; Check for temporal inconsistency! If there is, error
  ;; out. Otherwise, there may be glitches in causal link extraction
  ;; due to the precedence operator acting weird. It also would
  ;; make no sense to proceed anyways.
  (extract-temporal-conflicts ps)
  (check-temporal-feasibility ps)

  ;; Extract labeled causal links & resolve threats (this may
  ;; add more temporal constraints). This will also extract
  ;; propositional constraints representing the causal links.
  (when-debug (ps) (format t "Extracting causal links...~%"))
  (extract-causal-links ps)
  (when-debug (ps) (format t "Done extracting causal links~%"))

  ;; If we added new temporal constraints during causal link extraction,
  ;; We'll need to re-do our temporal reasoning.
  (when (not (null (pike-causal-link-temporal-constraints ps)))
    ;; Add any new strong controllability constraints if necessary
    (apply-strong-controllability ps)
    ;; Run labeled APSP on LDG
    (when-debug (ps) (format t "Re-running APSP~%"))
    (compute-labeled-apsp ps)
    ;; If desired, output this labeled APSP table
    (when (pike-generate-debug-files? (pike-options ps))
      (write-apsp-table-to-file ps "pike-apsp-second.txt"))
    (extract-temporal-conflicts ps)
    (check-temporal-feasibility ps))

  ;; Do further validity checking?


  ;; Convert the APSP into an LDG (which will be in dispatchable form).
  (convert-apsp-to-ldg ps :minimize nil)

  ;; Encode a constraint based knowledge base with:
  ;;   1.) Temporal conflicts (found from APSP)
  ;;   2.) Causal link-based constraints
  ;; First, initialize the constraint knowledge base.
  (initialize-constraint-knowledge-base ps)

  ;; Next, encode the temporal conflicts from the APSP.
  (add-temporal-conflicts-to-knowledge-base ps)

  ;; Then, encode the causal link constraints.
  (add-causal-link-constraints-to-knowledge-base ps)

  ;; If there are any additional constraints, add those too
  (add-any-additional-constraints-to-knowledge-base ps)

  ;; Certain constraint knowledge bases also require information
  ;; about conflicts that could be added online. If necessary,
  ;; take care of this.
  (gather-possible-conflicts-for-knowledge-base ps)

  ;; If desired, generate debugging files detailing
  ;; the extracted constraints
  (when (pike-generate-debug-files? (pike-options ps))
    (write-causal-link-sets-to-file ps "pike-extracted-causal-links.txt")
    (write-extracted-constraints-to-text-file ps)
    (write-extracted-constraints-to-cnf-files ps)
    (write-extracted-constraints-and-all-possible-conflicts-to-cnf-files ps))

  ;; Finally process all of the constraints (for instance,
  ;; results in the ATMS being encoded and propagation
  ;; occuring)
  (when-debug (ps) (format t "Processing constraints..."))
  (process-constraints (pike-constraint-knowledge-base ps))
  (when-debug (ps) (format t "done~%"))

  ;; If desired, output an ATMS JSON file, and also a list
  ;; of all found solutions
  (when (pike-generate-debug-files? (pike-options ps))
    ;; (atms-to-json-file ps "pike-tms.json")
    (print-solutions-to-file (pike-constraint-knowledge-base ps) "pike-constraint-solutions.txt"))

  ;; (format t "HACK! Collapsing labels at very end...~%")
  ;; (setf (node-label (constraint-knowledge-base-atms-goal-node (pike-constraint-knowledge-base ps)))
  ;;       (fully-compactify  (node-label (constraint-knowledge-base-atms-goal-node (pike-constraint-knowledge-base ps))) (pike-state-space ps)))

  ;; Check and see if any solutions exist (throw an error
  ;; if they don't)
  (check-if-solutions-exist ps)

  ;; Setup datastructures and do a bit more work to get ready to execute.
  (initialize-for-execution ps)

  T)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Helper functions for compilation
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric load-plan (ps))
(defmethod load-plan ((ps pike-session))
  "Loads the TPN plan file."
  (with-accessors ((po pike-options)
                   (tpn pike-tpn)) ps
    (with-accessors ((plan-filename pike-options-plan-filename)) po
      (setf tpn (tpn:parse-network-from-xml-file plan-filename))))
  T)


(defgeneric load-plant-model (ps))
(defmethod load-plant-model ((ps pike-session))
  "Loads an appropriate plant model for causal link analysis."
  (with-accessors ((plant-model pike-plant-model)
                   (po pike-options)) ps
    (with-slots (domain-filename problem-filename) po

      (unless domain-filename
        ;; If we get here, no plant model was specified - use the noop model
        (format t "Using No-op plant model~%")
        (setf plant-model (make-instance 'plant-model-noop))
        (return-from load-plant-model T))


      ;; If it's a filename, convert it to a string
      (let ((domain-filename-string (if (pathnamep domain-filename)
                                        (namestring domain-filename)
                                        domain-filename)))
        (cond
          ((equalp ".pddl" (subseq domain-filename-string (- (length domain-filename-string) 5)))
           ;; PDDL domain file
           (unless problem-filename
             (error "PDDL domain models must also have corresponding PDDL problem files!"))
           (format t "Loading PDDL plant model~%")
           (setf plant-model (make-instance 'plant-model-pddl
                                            :domain-filename domain-filename
                                            :problem-filename problem-filename)))

          ((equalp ".rmpl.xml" (subseq domain-filename-string (- (length domain-filename-string) 9)))
           ;; RMPL domain file
           (format t "Loading RMPL plant model~%")
           (setf plant-model (make-instance 'plant-model-rmpl
                                            :domain-filename domain-filename)))

          (T
           ;; Unsupport plant model
           (format t (colorize :red "Unsupported plant model!~%"))
           (return-from load-plant-model nil)))))
    T))

(defgeneric load-probability-distribution (ps))
(defmethod load-probability-distribution ((ps pike-session))
  "Load a probability distribution from the specified file, if one was specified.
Assumes the distribution is a Bayesian network over finite-domain
variables (namely, exactly the uncontrollable variable defined in the TPN)
and the file format is the standard open .xmlbif format."
  (with-accessors ((po pike-options)
                   (bn pike-bayesian-network)
                   (ss pike-state-space)) ps
    (when (pike-options-probability-distribution po)
      (let ((prob-dist-filename-string (if (pike-options-probability-distribution po)
                                           (namestring (pike-options-probability-distribution po))
                                           (pike-options-probability-distribution po))))

        (cond
          ;; .xmlbif or .xml: Parse a Bayesian network
          ((or (equalp ".xmlbif" (subseq prob-dist-filename-string (- (length prob-dist-filename-string) 7)))
               (equalp ".xml" (subseq prob-dist-filename-string (- (length prob-dist-filename-string) 7))))

           ;; (setf bn (parse-xmlbif-file (pike-options-probability-distribution po)
           ;;                             :ss ss))

           (setf bn (compile-bayesian-network-to-influence-diagram (pike-options-probability-distribution po)
                                                                   ss
                                                                   (generate-choice-variable-guards ps))))

          ;; .pgmx: Load an influence diagram (see ProgGraphicalModelXml format, also OpenMarkov software)
          ((equalp ".pgmx" (subseq prob-dist-filename-string (- (length prob-dist-filename-string) 5)))
           (setf bn (parse-pgmx-file (pike-options-probability-distribution po)
                                     :ss ss)))))
      (recast-ldg (pike-ldg ps) ss))))


(defgeneric initialize-constraint-knowledge-base (ps))
(defmethod initialize-constraint-knowledge-base ((ps pike-session))
  (with-accessors ((ckb pike-constraint-knowledge-base)
                   (ss pike-state-space)
                   (po pike-options)) ps
    (let (ckb-internal)
      ;; TODO - this is where we choose PATMS or ATMS or piATMS
      (when-debug (ps)
                  (format t "Constraint back end: ~a" (pike-options-constraint-knowledge-base-type po))
                  (if (pike-options-memoize-constraint-calls po)
                      (format t " (smart memoized)~%")
                      (format t "~%")))

      ;; Select the proper constraint knowledge base type
      (cond
        ((or (eql :atms (pike-options-constraint-knowledge-base-type po))
             (eql :piatms (pike-options-constraint-knowledge-base-type po)))
         (setf ckb-internal (make-instance 'constraint-knowledge-base-atms
                                           :ss ss
                                           :atms-type (pike-options-constraint-knowledge-base-type po)
                                           :encode-causal-support-variables (pike-options-encode-causal-support-variables po))))

        ((eql :patms (pike-options-constraint-knowledge-base-type po))
         (setf ckb-internal (make-instance 'constraint-knowledge-base-patms
                                           :ss ss
                                           :atms-type (pike-options-constraint-knowledge-base-type po)
                                           :bayesian-network (pike-bayesian-network ps)
                                           :encode-causal-support-variables (pike-options-encode-causal-support-variables po))))

        ((eql :sat (pike-options-constraint-knowledge-base-type po))
         (setf ckb-internal (make-instance 'constraint-knowledge-base-sat-solver
                                           :ss ss)))

        ((eql :bdd (pike-options-constraint-knowledge-base-type po))
         (setf ckb-internal (make-instance 'constraint-knowledge-base-bdd
                                           :ss ss)))

        ((eql :cc-wmc-ddnnf (pike-options-constraint-knowledge-base-type po))
         ;; Make sure we have a Bayesian network
         (unless (pike-bayesian-network ps)
           (error "Cannot use the chance-constrained d-DNNF WMC knowledge base without specifying a Bayesian network!"))
         (setf ckb-internal (make-instance 'constraint-knowledge-base-cc-wmc-ddnnf
                                           :ss ss
                                           :chance-constraint (pike-options-chance-constraint po)
                                           :bayesian-network (pike-bayesian-network ps))))

        ((eql :cc-wmc-bdd (pike-options-constraint-knowledge-base-type po))
         ;; Make sure we have a Bayesian network
         (unless (pike-bayesian-network ps)
           (error "Cannot use the BDD WMC knowledge base without specifying a Bayesian network!"))
         (select-variable-total-ordering ps)
         (setf ckb-internal (make-instance 'constraint-knowledge-base-cc-wmc-bdd
                                           :ss ss
                                           :chance-constraint (pike-options-chance-constraint po)
                                           :bayesian-network (pike-bayesian-network ps)
                                           :variable-total-ordering (pike-variable-total-ordering ps)
                                           :causal-link-support-variable-orderings (compute-causal-link-support-variable-orderings ps)
                                           :choice-variable-guards (generate-choice-variable-guards ps))))

        ((eql :cc-wmc-ab-bdd (pike-options-constraint-knowledge-base-type po))
         ;; Make sure we have a Bayesian network
         (unless (pike-bayesian-network ps)
           (error "Cannot use the Anytime-Bounded BDD (AB-BDD) WMC knowledge base without specifying a Bayesian network!"))
         (select-variable-total-ordering ps)
         (setf ckb-internal (make-instance 'constraint-knowledge-base-cc-wmc-ab-bdd
                                           :ss ss
                                           :chance-constraint (pike-options-chance-constraint po)
                                           :bayesian-network (pike-bayesian-network ps)
                                           :variable-total-ordering (pike-variable-total-ordering ps)
                                           :causal-link-support-variable-orderings (compute-causal-link-support-variable-orderings ps)
                                           :choice-variable-guards (generate-choice-variable-guards ps))))

        (T
         (error "Invalid constraint knowledge solver backened type!")))

      ;; Set up a smart memoizer if desired!
      (if (pike-options-memoize-constraint-calls po)
          (setf ckb (make-instance 'constraint-knowledge-base-memoizer
                                   :ckb-internal ckb-internal
                                   :ss ss))
          (setf ckb ckb-internal)))
    t))


(defgeneric extract-temporal-conflicts (ps))
(defmethod extract-temporal-conflicts ((ps pike-session))
  "Extract a minimal set of temporal conflict environments"
  (with-accessors ((apsp pike-apsp)
                   (temporal-conflict-environments pike-temporal-conflict-environments)) ps
      (setf temporal-conflict-environments (get-temporal-conflict-environments apsp))))


(defgeneric check-temporal-feasibility (ps))
(defmethod check-temporal-feasibility ((ps pike-session))
  "Returns T if the plan could be temporally feasible, or
   throws an error if it is guaranteed to be temporally
   infeasible."
  ;; TODO: Use a different error type? Some sort of special plan compilation error?
  (with-slots (temporal-conflict-environments ss) ps
    (if (some #'(lambda (e) (is-empty-environment? e ss)) temporal-conflict-environments)
        (error 'temporally-infeasible-error :reason "Plan is temporally inconsistent!")
        T)))


(defgeneric add-temporal-conflicts-to-knowledge-base (ps))
(defmethod add-temporal-conflicts-to-knowledge-base ((ps pike-session))
  "Add temporal conflict environments (already computed)
   to the constraint knowledge base."
  (with-accessors ((ckb pike-constraint-knowledge-base)
                   (ss pike-state-space)
                   (conflicts pike-temporal-conflict-environments)) ps
    (dolist (conflict conflicts)
      (add-constraint! ckb (environment->conflict conflict ss)))))


(defgeneric add-causal-link-constraints-to-knowledge-base (ps))
(defmethod  add-causal-link-constraints-to-knowledge-base ((ps pike-session))
  "Add constraints related to causal links to the constraint
   knowledge base (they've already beenc computed)."
  (with-slots ((ckb constraint-knowledge-base) causal-link-sets) ps
    (dolist (cls causal-link-sets)
      (dolist (c (constraints cls))
        (add-constraint! ckb c)))))


(defgeneric add-any-additional-constraints-to-knowledge-base (ps))
(defmethod add-any-additional-constraints-to-knowledge-base ((ps pike-session))
  "Add any additional constraints for the problem to the constraint
knowledge base."
  (with-slots (additional-constraints (ckb constraint-knowledge-base)) ps
    (dolist (c additional-constraints)
      (add-constraint! ckb c))))


(defgeneric check-if-solutions-exist (ps))
(defmethod check-if-solutions-exist ((ps pike-session))
  (with-slots (constraint-knowledge-base) ps
    (if (solution-exists? constraint-knowledge-base)
        T
        (error 'no-solutions-exist-error :reason "Plan is not correct"))))


(defgeneric recast-environments (ps))
(defmethod recast-environments ((ps pike-session))
  "Helper function to recast the environments for anything
  inside this pike session that uses environments.

  This needs to be called whenever we add variables to the
  state space."
  (with-slots (apsp ldg ss temporal-conflict-environments) ps
    ;; Recast the APSP
    (recast-apsp apsp)

    ;; And the LDG
    (recast-ldg ldg ss)

    ;; And any temporal conflicts we've already extracted
    (setf temporal-conflict-environments
          (mapcar #'(lambda (e)
                      (recast-environment e ss))
                  temporal-conflict-environments))))


(defgeneric apply-strong-controllability (ps))
(defmethod apply-strong-controllability ((ps pike-session))
  "If desired, add temporal constraints to enforce strong controllability."
  (when (and (pike-options-use-strong-controllability? (pike-options ps))
             (plan-has-uncontrollable-durations? ps))
    (format t "Adding strong controllability constraints~%")
    (add-strong-controllability-constraints ps :remove-original? T)))


(defgeneric plan-has-uncontrollable-durations? (ps))
(defmethod plan-has-uncontrollable-durations? ((ps pike-session))
  "Helper method to return T if the plan has uncontrollable durations, and
   false otherwise."
  (with-slots (tpn) ps
    (do-constraints (c tpn)
      (when (typep c 'tpn:simple-contingent-temporal-constraint)
        (return-from plan-has-uncontrollable-durations? T))))
  nil)


(defgeneric gather-possible-conflicts-for-knowledge-base (ps))
(defmethod gather-possible-conflicts-for-knowledge-base ((ps pike-session))
  "Certain constraint knowledge bases in Pike require a complete set of
all possible conflicts that could possibly be committed to online, to be
provided upfront before execution. These conflicts can, for example, be
compiled away. This method finds all such conflicts, and provides them to the
knowledge base"
  (let ((ckb (pike-constraint-knowledge-base ps)))
    ;; If we're using memoizing, skip onto the actual knowledge base
    (when (typep ckb 'constraint-knowledge-base-memoizer)
      (setf ckb (ckb-internal ckb)))

    ;; If the knowledge base is one of the acceptable types, then
    ;; collect all conflicts and encode
    (when (typep ckb 'constraint-knowledge-base-cc-wmc-ddnnf)
      (let ((conflicts-possible (collect-all-possible-online-conflicts ps)))
        (register-possible-online-conflicts ckb conflicts-possible)))))


(defgeneric collect-all-possible-online-conflicts (ps))
(defmethod collect-all-possible-online-conflicts ((ps pike-session))
  "Helper method that collects the set of all possible conflicts that might
be committed to during online execution. Returns a list of environments."
  (declare (optimize (speed 3)))
  (with-slots (ss ldg) ps
    (let (conflicts-possible)
      ;; Construct a list of all possible conflicts that we could commit to
      ;; 1. The empty environment {}
      (push (make-environment ss) conflicts-possible)

      ;; 2.Any possible event environments
      (dolist (ei (mtk-graph:get-vertices ldg))
        (push (guard ei) conflicts-possible))

      ;; 3. Any possible environment in ldg edge
      (dolist (edge (mtk-graph:get-edge-list (mtk-graph:get-edges ldg)))
        (let ((w (mtk-graph:get-weight edge ldg)))
          (dolist (pair (lvs-pairs w))
            (push (lv-label pair) conflicts-possible))))

      ;; 4. All possible s_{p, e_c} type conflicts
      (dolist (cls (pike-causal-link-sets ps))
        (let* ((spec (causal-link-choice-variable cls))
               (values (remove "∘" (decision-variable-values spec) :test #'equal)))
          (dolist (value values)
            (push (make-environment-from-assignments ss (list (assignment spec value))) conflicts-possible))))

      ;; Cool! Remove duplicates. Might take a little while. For some reason calling this once
      ;; here is much faster than calling pushnew repeatedely in SBCL.
      (setf conflicts-possible (remove-duplicates conflicts-possible :test #'equal))

      ;; Return the list!
      (format t "Found ~a possible online conflicts~%" (length conflicts-possible))
      conflicts-possible)))


(defgeneric process-conditional-variables (ps))
(defmethod process-conditional-variables ((ps pike-session))
  "Modify variable domains to add in ∘ value, representing
 that the variable is unassigned / inactivated / unobserved. Also
  returns some extra constraints to enforce these."
  (with-slots (additional-constraints ss ldg) ps
    (let ((choice-variable-guards (generate-choice-variable-guards ps)) ;; TODO Just store as slot in pike-session or reuse later?
          constraints-conditional
          new-variables-flag)
      (dolist (var (state-space-bits-variables ss))
        (let (guard-constraint guard found?)
          ;; Get a guard for this variable
          (multiple-value-setq (guard found?) (gethash var choice-variable-guards))
          (cond
            ;; Variable has a guard -- so we know when it will be observed.
            (found?
             (setf guard-constraint (make-instance 'conjunction-constraint :conjuncts (loop for a in guard
                                                                                         collecting (make-instance 'assignment-constraint :assignment a)))))

            ;; Variable has no guard -- means it is always a latent variable and never observed.
            ((not found?)
             (setf guard-constraint (make-instance 'constant-constraint :constant nil))))


          ;; If the guard is not True, add a new domain value.
          (unless (and found?
                       (not guard))
            (let ((active-variable (make-instance 'decision-variable
                                                  :name (format nil "active_~a" (decision-variable-name var))
                                                  :domain (list "y" "n")
                                                  :controllable? t)))
              (add-variable! ss active-variable)
              (setf new-variables-flag t)


              ;; Add a new domain value to signify inactivated
              ;; (add-new-domain-value var "∘") ;; Already added in tpn-ldg-conversion because easier there!

              ;; Record a constraint that ensures that if the guard holds, that this variable will be assigned one of its real values.
              (push (make-instance 'equivalence-constraint
                                   :lhs (make-instance 'conjunction-constraint
                                                       :conjuncts (loop for a in guard
                                                                     collect (make-instance 'assignment-constraint :assignment a)))
                                   :rhs (make-instance 'assignment-constraint :assignment (assignment active-variable "y")))
                    constraints-conditional)

              (push (make-instance 'equivalence-constraint
                                   :lhs (make-instance 'assignment-constraint :assignment (assignment active-variable "y"))
                                   :rhs (make-instance 'disjunction-constraint
                                                       :disjuncts (loop for val in (decision-variable-values var) when (not (equal val "∘"))
                                                                     collect (make-instance 'assignment-constraint :assignment (assignment var val)))))
                    constraints-conditional)))))

      ;; Add the conditional constraints to Riker's store.
      (dolist (c constraints-conditional)
        (push c additional-constraints))

      ;; Recast the LDG
      (when new-variables-flag
        (recast-ldg ldg ss))

      t)))


(defgeneric select-variable-total-ordering (ps))
(defmethod select-variable-total-ordering ((ps pike-session))
  "This method computes a worst-case total ordering of the decision variables in the TPN / pTPN.
This is important for probabilistic reasoning, since estimating the probability of execution success
depends significantly on the order in which controllable and uncontrollable choices are made. Intuitively,
this is because the executive can generally guarantee a higher probability of success if it has observed
more choices made by the human / environment already, for example.

   The reason we must choose a total ordering is that, in general, choices may be partially ordered in the
pTPN. We may know the relative order of certain choices due to very flexible temporal constraints. Our
intuition here is to choose the worse-case total ordering, in the following sense: when in doubt / when there
are multiple possible total ordering for the partial ordering, choose ones in which the controllable choices
appear before the uncontrollable ones. The result is that the probability estimates will be lower / conservative,
but we're still handling the general case.

   Our algorithm constructs a partial order over the choice nodes by means of the temporal constraints of the problem.
We then generate a total ordering using Kahn's algorithm, where the we check controllable choices first.
"
  (with-slots (variable-total-ordering ldg ss) ps
    (with-slots (decision-variable->at-event) ldg
      (let (choice-variables
            (choice-predecessors (make-hash-table :test #'eql))
            total-order)
        ;; First, obtain all of the choice variables
        (setf choice-variables (loop for key being the hash-keys of decision-variable->at-event collect key))
        ;; (format t "Choice variables: ~a~%" choice-variables)

        ;; Extract partial order relations. Effectively generates a graph whose nodes are variables, and whose
        ;; edges represent ordering constraints. Represented as a hash table mapping choice variables to their
        ;; predecessors
        (dolist (choice-variable choice-variables)
          (setf (gethash choice-variable choice-predecessors) nil)
          (dolist (var choice-variables)
            (when (and (not (eql choice-variable var))
                       (precedes? ps
                                  (gethash var decision-variable->at-event)
                                  (gethash choice-variable decision-variable->at-event)))
              ;; (format t "~a precedes ~a~%" var choice-variable)
              (push var (gethash choice-variable choice-predecessors)))))

        ;; Now run a topological sorting algorithm (Kahn's algorithm).
        ;; Sort the choice variables so that we'll check controllable ones one first
        ;; (hence preferring them earlier in the total ordering)
        (setf choice-variables (sort choice-variables #'< :key #'(lambda (v) (if (controllable? v) 1 2))))
        (dotimes (i (length choice-variables))
          ;; Select a variable that has no un-sorted predecessors
          (dolist (var choice-variables)
            (when (eql nil (gethash var choice-predecessors))
              ;; Cool! Select this one.
              (push var total-order)
              ;; No longer an option to sort this variable.
              (setf choice-variables (remove var choice-variables))
              ;; Now that it's sorted, remove it as a predecessor from all the choice variables
              (dolist (var-other choice-variables)
                (setf (gethash var-other choice-predecessors)
                      (remove var (gethash var-other choice-predecessors))))
              (return))))

        (unless (eql nil choice-variables)
          (error "Cannot choose total ordering of variables... is there a cycle??"))

        ;; Reverse
        (setf total-order (reverse total-order))
        ;; Return the resutling total order.
        (when-debug (ps)
                    (format t "Total order of variables: ~{~a~^, ~}~%" (mapcar #'decision-variable-name total-order)))
        (setf variable-total-ordering total-order)
        total-order))))


(defgeneric compute-causal-link-support-variable-orderings (ps))
(defmethod compute-causal-link-support-variable-orderings ((ps pike-session))
  (with-slots (ss causal-link-sets ldg) ps
    (with-slots (decision-variable->at-event) ldg
      (let ((var->succeeding-choices (make-hash-table :test #'eql))
            (choice-variables (loop for key being the hash-keys of decision-variable->at-event collect key)))
        ;; Iterete over each causal link set
        (dolist (cls causal-link-sets)
          (with-slots (causal-link-choice-variable e-c producer-events threat-events threat-resolver-choice-variables-by-events) cls
            ;; For each, find the set of choice-variables for which *all* of the events associated with this causal link set
            ;; are guaranteed to precede.
            (let ((events-all (concatenate 'list
                                           (list e-c)
                                           producer-events
                                           threat-events))
                  (vars-succeeding nil))

              (dolist (var choice-variables)


                ;; TODO: Switching away from below.
                ;; Checks if ALL of the events (e-c, producers, threats) precede this choice variable.
                ;; (when (every #'(lambda (e)
                ;;                  (precedes? ps e (gethash var decision-variable->at-event)))
                ;;              events-all)
                ;;   (push var vars-succeeding))

                ;; Checks only if e-c precedes this choice variable.
                (when (precedes? ps e-c (gethash var decision-variable->at-event))
                  (push var vars-succeeding)))


              ;; Keep track of this set for spec and any ordering variables.
              (setf (gethash causal-link-choice-variable var->succeeding-choices) vars-succeeding)
              (dolist (var (loop for val being the hash-values of threat-resolver-choice-variables-by-events collect val))
                (setf (gethash var var->succeeding-choices) vars-succeeding)))))
        var->succeeding-choices))))


(defgeneric generate-choice-variable-guards (ps))
(defmethod generate-choice-variable-guards ((ps pike-session))
  "A helper function that computes the guard (here, represented as a list of assignments)
for each choice variable to be activated. Note that for uncontrollable choice variables in a pTPN,
the variable will become an observation to Riker if and only if this guard holds (otherwise Riker
does not get to observe it during execution)."
  (let ((var-ht-guard (make-hash-table :test #'eql)))
    (with-slots (ss ldg) ps
      (dolist (var (state-space-bits-variables ss))
        (let (event found?)
          (multiple-value-setq (event found?) (gethash var (ldg-decision-variable->at-event ldg)))
          (when found?
            (setf (gethash var var-ht-guard)
                (get-assignments-from-environment ss (guard event))))))
      var-ht-guard)))


(defgeneric write-extracted-constraints-to-cnf-files (ps))
(defmethod write-extracted-constraints-to-cnf-files ((ps pike-session))
  "Helper function that outputs a DIMACS .cnf file. Uses the
Tseitin transformation."
  (let* ((ss (pike-state-space ps))
         (constraints (constraint-knowledge-base-constraints (pike-constraint-knowledge-base ps)))
         (sat (encode-constraints-as-sat-cnf constraints (state-space-bits-variables ss))))
    (write-dimacs-cnf-file sat "pike-constraints-tseitin.cnf")
    (write-dimacs-cnf-variable-mapping-file sat "pike-constraints-tseitin-mapping.txt")))


(defgeneric write-extracted-constraints-and-all-possible-conflicts-to-cnf-files (ps))
(defmethod write-extracted-constraints-and-all-possible-conflicts-to-cnf-files ((ps pike-session))
  "Helper function that outputs a DIMACS .cnf file. Uses the
Tseitin transformation. Also adds additional auxiliary constraints for committing
to all possible conflicts."
  (with-slots (ss) ps
    (let* ((all-variables (copy-seq (state-space-bits-variables ss)))
           (conflicts-possible (collect-all-possible-online-conflicts ps))
           (conflict-constraints-possible nil)
           (constraints (copy-seq (constraint-knowledge-base-constraints (pike-constraint-knowledge-base ps))))
           sat)

      ;; Now, convert each of the above (environments) into conflict constraints.
      (dolist (c conflicts-possible)
        (push (environment->conflict c ss) conflict-constraints-possible))

      ;; (dolist (c conflict-constraints-possible)
      ;;   (format t "~a~%" c))

      ;; For each of these conflicts, create a new decision variable, and some new associated
      ;; constraints
      (let ((i 0))
        (dolist (c conflict-constraints-possible)
          (let ((x_aux (make-instance 'decision-variable
                                      :name (format nil "x_aux_~a" (incf i))
                                      :controllable? t
                                      :domain (list "1" "0"))))
            (push x_aux all-variables)
            ;; Now, create a new constraint! x_aux <-> conflict

            ;; (push (make-instance 'conjunction-constraint
            ;;                :conjuncts (list
            ;;                            (make-instance 'implication-constraint
            ;;                                           :implicant (make-instance 'assignment-constraint :assignment (assignment x_aux "1"))
            ;;                                           :consequent c)
            ;;                            (make-instance 'implication-constraint
            ;;                                           :implicant c
            ;;                                           :consequent (make-instance 'assignment-constraint :assignment (assignment x_aux "1")))))
            ;;       constraints)


            ;; (push (make-instance 'implication-constraint
            ;;                      :implicant (make-instance 'assignment-constraint :assignment (assignment x_aux "1"))
            ;;                      :consequent c)
            ;;       constraints)

            (push (make-instance 'equivalence-constraint
                                 :lhs  (make-instance 'assignment-constraint :assignment (assignment x_aux "1"))
                                 :rhs c)
                  constraints))))

      ;; (dolist (c constraints)
      ;;   (format t "~a~%" c))

      (setf sat (encode-constraints-as-sat-cnf constraints all-variables))
      (write-dimacs-cnf-file sat "pike-constraints-with-all-possible-conflicts-tseitin.cnf")
      (write-dimacs-cnf-variable-mapping-file sat "pike-constraints-with-all-possible-conflicts-tseitin-mapping.txt"))))


(defgeneric write-extracted-constraints-to-text-file (ps))
(defmethod write-extracted-constraints-to-text-file ((ps pike-session))
  "Helper function that outputs the constraints to a human-readable text file."
  (with-open-file (f "pike-constraints.txt" :direction :output :if-exists :supersede :if-does-not-exist :create)
    ;; Print variables and domains
    (with-slots (ss) ps
      (dolist (variable (state-space-bits-variables ss))
        (format f "Variable: ~a~%" (decision-variable-name variable))
        (dolist (val (decision-variable-values variable))
          (format f "    ~a~%" val))
        (format f "~%")))


    ;; Print constraints
    (with-slots (causal-link-sets temporal-conflict-environments ss) ps
      (format f "Temporal conflicts:~%")
      (dolist (tc temporal-conflict-environments)
        (format f "  ~a~%" (environment->conflict tc ss)))

      (format f "~%Causal Link Constraints:~%")
      (dolist (cls causal-link-sets)
        (dolist (c (constraints cls))
          (format f "  ~a~%" c))
        (format f "~%")))))


(defgeneric write-apsp-table-to-file (ps filename))
(defmethod write-apsp-table-to-file ((ps pike-session) filename)
  "Helper function to write a text file for an APSP."
  (with-open-file (f filename :direction :output :if-exists :supersede :if-does-not-exist :create)
    (write-apsp-table-to-stream (pike-apsp ps) f)))


(defgeneric write-causal-link-sets-to-file (ps filename))
(defmethod write-causal-link-sets-to-file ((ps pike-session) filename)
  "Helper function to write the causal link sets to a debug file."
  (with-open-file (f filename :direction :output :if-exists :supersede :if-does-not-exist :create)
    (format f "Pike found these causal links:~%~%")
    (dolist (cls (pike-causal-link-sets ps))
      (format f "  Causal link for precondition ~a of event ~a:~%"
              (predicate cls)
              (id (consumer-event cls)))
      (format f "    Producers: ~{~a~^, ~}~%" (mapcar #'id (producer-events cls)))
      (format f "    Threats  : ~{~a~^, ~}~%" (mapcar #'id (threat-events cls)))
      (format f "~%"))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Conditions / errors
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-condition pike-compilation-error (error)
  ((reason
    :initarg :reason
    :accessor reason
    :type string)))

(define-condition temporally-infeasible-error (pike-compilation-error)
  ())

(define-condition no-solutions-exist-error (pike-compilation-error)
  ())
