;;;; Copyright (c) 2011-2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Class definitions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass plant-model-noop (plant-model)
  ((name
    :type string
    :initform "NOOP Plant Model"
    :initarg :name
    :accessor plant-model-name))
  (:documentation "Represents a basic plant model
that doesn't really do anything."))


(defclass state-condition-noop (state-condition)
  ()
  (:documentation "Represents a state condition. But there
aren't really any state conditions!"))


(defclass state-noop (state)
  ()
  (:documentation "Represents a state, but there aren't
really states!"))


(defclass action-noop (action)
  ((dispatch
    :type string
    :initarg :dispatch
    :accessor action-noop-dispatch))
  (:documentation "Represents an action."))

(defmethod print-object ((a action-noop) s)
  (with-slots (dispatch) a
    (format s "<ACTION-NOOP ~a>" dispatch)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Implement the required methods
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod parse-action ((action-string string) (pm plant-model-noop))
  (make-instance 'action-noop :dispatch action-string :plant-model pm))

(defmethod parse-state-condition ((state-condition-string string) (pm plant-model-noop))
  (make-instance 'state-condition-noop
                 :plant-model pm))

(defmethod parse-state ((string-state-conditions list) (pm plant-model-noop))
  (make-instance 'state-noop
                 :plant-model pm))

(defmethod get-conditions ((action action-noop) when)
  "No conditions"
  nil)

(defmethod get-effects ((action action-noop) when)
  "No effects"
  nil)

(defmethod action-dispatch ((action action-noop))
  (action-noop-dispatch action))

(defmethod about ((state state-condition-noop))
  nil)

(defmethod state-conditions-equal ((sc1 state-condition-noop) (sc2 state-condition-noop))
  nil)

(defmethod get-initial-state ((ps pike-session) (pm plant-model-noop))
  (make-instance 'state-noop
                 :plant-model pm))

(defmethod get-goal-state ((ps pike-session) (pm plant-model-noop))
  (make-instance 'state-noop
                 :plant-model pm))

(defmethod get-empty-state ((ps pike-session) (pm plant-model-noop))
  (make-instance 'state-noop
                 :plant-model pm))
