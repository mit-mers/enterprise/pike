;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)





(defclass plant-model-rmpl (plant-model)
  ((name
    :type string
    :initform "RMPL Plant Model"
    :initarg :name
    :accessor plant-model-name)
   (classes
    :type list
    :initform nil
    :initarg :classes
    :accessor plant-model-rmpl-classes)
   (methods-by-name
    :type hash-table
    :initform (make-hash-table :test #'equal)
    :accessor plant-model-rmpl-methods-by-name
    :documentation "A mapping from method names of the
form \"Class.MethodName\" to method objects"))
  (:documentation "Represents an RMPL plant model."))

(defclass state-condition-rmpl (state-condition)
  ((predicate
    :type string
    :initarg :predicate
    :accessor predicate
    :documentation "A string representation of the predicate")))

(defmethod print-object ((a state-condition-rmpl) s)
  (with-slots (predicate negative?) a
    (if (not negative?)
        (format s "~a" predicate)
        (format s "¬~a" predicate))))


(defclass action-rmpl (action)
  ((dispatch
    :type string
    :initarg :dispatch
    :accessor action-rmpl-dispatch)
   (preconditions
    :type list
    :initform nil
    :initarg :preconditions
    :accessor action-rmpl-preconditions)
   (postconditions
    :type list
    :initform nil
    :initarg :postconditions
    :accessor action-rmpl-postconditions))
  (:documentation "Represents an RMPL action"))

(defmethod print-object ((a action-rmpl) s)
  (with-slots (dispatch) a
    (format s "<ACTION-RMPL ~a>" dispatch)))


(defclass state-rmpl (state)
  ()
  (:documentation "Represents a state, or a set of
RMPL state conditions / predicates"))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; RMPL classes, methods, etc.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass rmpl-class ()
  ((name
    :type string
    :initarg :name
    :accessor name
    :initform (error "Must specify RMPL class name"))
   (modes
    :type list
    :initform nil
    :initarg :modes
    :accessor modes
    :documentation "A list of string modes of the class")
   (methods
    :type list
    :initform nil
    :initarg :methods
    :accessor methods
    :documentation "A list of RMPL methods in this class"))
  (:documentation "Represents an RMPL class"))

(defmethod print-object ((c rmpl-class) s)
  (format s "<RMPL-CLASS \"~a\">" (name c)))


(defclass rmpl-method ()
  ((name
    :type string
    :accessor name
    :initarg :name
    :documentation "The name of this method")
   (parent-class
    :type rmpl-class
    :accessor parent-class
    :initarg :parent-class
    :initform (error "Must specify a class")
    :documentation "The class to which this method belongs")
   (arguments
    :type list
    :accessor arguments
    :initarg :arguments
    :initform nil
    :documentation "A list of arguments to this method call, each of type rmpl-method-argument")
   (preconditions
    :type list
    :accessor preconditions
    :initarg :preconditions
    :initform nil
    :documentation "A list of preconditions, to be interpreted as a conjunction")
   (postconditions
    :type list
    :accessor postconditions
    :initarg :postconditions
    :initform nil
    :documentation "A list of postconditions, to be inrepreted as a conjunction"))
  (:documentation "Represents a method in an RMPL class"))

(defmethod print-object ((m rmpl-method) s)
  (format s "<RMPL-METHOD \"~a\">" (name m)))

(defclass rmpl-method-argument ()
  ((argument-type
    :type rmpl-class
    :initarg :type
    :accessor argument-type
    :initform (error "Must specify a type / class of this argument")
    :documentation "The class of this argument")
   (name
    :type string
    :initarg :name
    :accessor name
    :initform (error "Must specify an argument name")))
  (:documentation "Represents a method argument like 'Location a' for example."))

(defmethod print-object ((ma rmpl-method-argument) s)
  (format s "<RMPL-METHOD-ARGUMENT \"~a ~a\">" (name (argument-type ma)) (name ma)))



(defclass rmpl-argument-bindings ()
  ((bindings
    :type hash-table
    :initform (make-hash-table)))
  (:documentation "Represents an assignment to arguments"))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Methods for deadling with the above classes
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun class-has-mode? (c mode)
  "Returns T if the given class has the given mode, and nil otherwise"
  (not (not (find mode (modes c) :test #'equal))))


(defun complementary-modes (c mode)
  "Returns all the other modes of the class except this one"
  (loop for m in (modes c)
       unless (equal m mode) collect m))


(defun lookup-argument-type (m name)
  "Looks up the type of the given argument"
  (dolist (arg (arguments m))
    (when (equal name (name arg))
      (return-from lookup-argument-type (argument-type arg))))
  (error (format nil "Unknown argument \"~a\"" name)))


(defmethod initialize-instance :after ((pm plant-model-rmpl) &key (domain-filename (error "Must specify RMPL domain file")))
  "Upon creation, parse the plant model."
  (parse-rmpl-plant-model pm domain-filename))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Parsing code!
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun node-has-attributes? (sxml-node)
  "=> boolean

Returns T iff the sxml node has attributes.

This is determined by looking at the second element in the list and seeing if it
is a list that starts with :@"
  (and (listp (second sxml-node))
       (eql :@ (first (second sxml-node)))))

(defun node-has-children? (sxml-node)
  "=> boolean

Returns T iff the sxml node has children.

This is determined by looking at the second or third element in the
list (depending on if the list has attributes) and seeing if it is a list."
  (if (node-has-attributes? sxml-node)
      (some #'listp (rest (rest sxml-node)))
      (some #'listp (rest sxml-node))))

(defun node-has-value? (sxml-node)
  "=> boolean

Returns T iff the sxml node has a value (no children).

This is determined by looking at the second or third element in the
list (depending on if the list has attributes) and seeing if it is a string."
  (if (node-has-attributes? sxml-node)
      (if (>= (length sxml-node) 3)
          (and (stringp (third sxml-node))
               (not (cdddr sxml-node)))
          t)
      (if (>= (length sxml-node) 2)
          (and (stringp (second sxml-node))
               (not (cddr sxml-node)))
          t)))

(defun node-value (sxml-node)
  (assert (node-has-value? sxml-node))
  (if (node-has-attributes? sxml-node)
      (if (>= (length sxml-node) 3)
          (third sxml-node)
          "")
      (if (>= (length sxml-node) 2)
          (second sxml-node)
          "")))

(defun node-attributes (sxml-node)
  "Returns a hashtable of attributes"
  (assert (node-has-attributes? sxml-node))
  (let ((attribute-ht (make-hash-table))
        (attribute-list (rest (second sxml-node))))
    (dolist (attribute-item attribute-list)
      (let ((key (first attribute-item))
            (val (second attribute-item)))
        (setf (gethash key attribute-ht) val)))
    attribute-ht))


(defun node-children (sxml-node)
  (if (node-has-attributes? sxml-node)
      (remove-if-not #'listp (rest (rest sxml-node)))
      (remove-if-not #'listp (rest sxml-node))))

(defmacro do-node-children ((child sxml-node) &rest body)
  `(dolist (,child (node-children ,sxml-node))
     ,@body))

(defun node-name (sxml-node)
  (first sxml-node))




(defun parse-rmpl-plant-model (pm domain-filename)
  "Parses the given RMPL file (*.rmpl.xml) into a plant model by looking at
   the methods' preconditions and postconditions"
  (let* ((root (s-xml:parse-xml-file domain-filename :output-type :sxml)))
    ;; Parse each class! Some fields will need to be changed later though.
    (do-node-children (child root)
      (%parse-rmpl-tag pm (node-name child) child)))


  (with-slots (classes methods-by-name) pm

    ;; Resolve method parameters!
    ;; For each method of each class, resolve the arguments.
    (dolist (c classes)
      (dolist (m (methods c))
        (setf (arguments m)
              (mapcar #'(lambda (arg-list)
                          (let* ((atype (first arg-list))
                                 (aname (second arg-list))
                                 (class-type (find atype classes :key #'name :test #'equal)))
                            (when (null class-type)
                              (error 'load-plant-model-error :reason (format nil "RMPL class arg type not found: \"~a\"" aname)))
                            (make-instance 'rmpl-method-argument
                                           :name aname
                                           :type class-type)))
                      (arguments m)))))



    ;; Resolve the argument names to rmpl-method-argument objects
    ;; in the (:mode-of-argument ...) preconditions and postconditions
    ;; of each method
    (dolist (c classes)
      (dolist (m (methods c))
        ;; Define a resolver function
        (flet ((resolver-fn (cond)
                 (case (first cond)
                   (:mode-of-argument
                    (let ((method-arg (find (second cond) (arguments m) :key #'name :test #'equal)))
                      (unless method-arg
                        (error 'load-plant-model-error :reason (format nil "Unknown method argument \"~a\" of method \"~a\" of class \"~a\"" (second cond) (name m) (name c))))
                      `(:mode-of-argument ,method-arg ,(third cond))))
                   (otherwise cond))))
          ;; Apply it to all preconditions and postconditions of each method
          (setf (preconditions m) (mapcar #'resolver-fn (preconditions m)))
          (setf (postconditions m) (mapcar #'resolver-fn (postconditions m))))))

    ;; Cross-check to make sure the mode assignments of each precondition
    ;; and postcondition to make sure they make sense!
    (dolist (c classes)
      (dolist (m (methods c))
        (flet ((checker-fn (cond)
                 (case (first cond)
                   (:mode-of-self
                    (class-has-mode? c (second cond)))
                   (:mode-of-argument
                    (class-has-mode? (argument-type (second cond)) (third cond))))))
          (unless (and (every #'checker-fn (preconditions m))
                       (every #'checker-fn (postconditions m)))
            (error 'load-plant-model-error :reason (format nil "A mode assignment is invalid in RMPL method \"~a\"!" (name m)))))))



    ;; Assemble a mapping from "Class.Method" -> method object
    (dolist (c classes)
      (dolist (m (methods c))
        (setf (gethash (format nil "~a.~a" (name c) (name m)) methods-by-name)
              m)))))




(defgeneric %parse-rmpl-tag (pm tag-name node))

(defmethod %parse-rmpl-tag ((pm plant-model-rmpl) (tag-name (eql :|Class|)) node)
  (with-slots (classes) pm
    (let* ((class-attributes (node-attributes node))
           (class-name (gethash :|name| class-attributes))
           (class-instance (make-instance 'rmpl-class :name class-name)))

      ;; Parse the contents of the class
      (do-node-children (child node)
        (case (node-name child)
          (:|Mode|
            (push (parse-mode child) (modes class-instance)))

          (:|Method|
            (push (parse-method child class-instance) (methods class-instance)))))

      ;; Now that we've finished parsing the class, make an instance and add
      ;; it to the plant model.
      (push class-instance classes))))



(defun parse-mode (node)
  "Parses a mode and return it as a string"
  (let ((attributes (node-attributes node)))
    (gethash :|name| attributes)))


(defun parse-method (node class)
  "Parses a method and returns a class!"
  (let ((name (gethash :|name| (node-attributes node)))
        arguments preconditions postconditions)
    (do-node-children (child node)
      (case (node-name child)
        (:|ParameterList|
          ;; Parse out the arguments of this method
          (do-node-children (achild child)
            (let* ((argument-attributes (node-attributes achild))
                   (arg-type (gethash :|type| argument-attributes))
                   (arg-name (gethash :|name| argument-attributes)))
              (setf arguments (append  arguments `((,arg-type ,arg-name)))))))

        (:|Behavior|
          ;; Parse preconditions and postconditions
          (do-node-children (bchild child)
            (case (node-name bchild)
              (:|PreCondition|
                (let ((expressions (parse-rmpl-expressions bchild)))
                  (dolist (expr expressions) (push expr preconditions))))

              (:|PostCondition|
                (let ((expressions (parse-rmpl-expressions bchild)))
                  (dolist (expr expressions) (push expr postconditions)))))))))

    (make-instance 'rmpl-method
                   :name name
                   :parent-class class
                   :arguments arguments
                   :preconditions preconditions
                   :postconditions postconditions)))


(defun parse-identifier (node)
  "Parses an identifier and returns it as a string"
  (let ((attributes (node-attributes node)))
    (gethash :|name| attributes)))


(defun parse-rmpl-expressions (node)
  ;; Use a do* so we can more easily increment by more than one
  (let (expressions)
    (do* ((children (node-children node) (cdr children))
          (child (car children) (car children)))
         ((null children) T)

      (ecase (node-name child)
        (:|Conjunction|
          ;; Just recurse inside and add
          (dolist (expr (parse-rmpl-expressions child))
            (push expr expressions)))

        (:|ThisEquals|
          ;; Create a :mode-of-self condition
          (let* ((mode-identifier-node (first (node-children child)))
                 (mode (parse-identifier mode-identifier-node)))
            (push `(:mode-of-self ,mode) expressions)))


        (:|Expression2|
          ;; Create a :mode-of-argument condition

          ;; Make sure it's of type EQUALITY, the only type supported
          (let ((expression-type (node-name (first (node-children child))))
                id1 id2)
            (unless (eql :|EQUALITY| expression-type)
              (error 'load-plant-model-error :reason (format nil "Unsupported RMPL plant model: \"~a\" is not supported in precondition / postconditions!" expression-type)))

            ;; Incremement to the next node, which should be an identifier, and get it.
            (setf children (cdr children))
            (setf child (car children))
            (setf id1 (parse-identifier child))

            ;; Incremement to the next node, which should be an identifier, and get it.
            (setf children (cdr children))
            (setf child (car children))
            (setf id2 (parse-identifier child))


            (push `(:mode-of-argument ,id1 ,id2) expressions)))

        (:|True|)

        (:|Identifier|
          ;; If we see this here, things are malformed!
          (error 'load-plant-model-error :reason (format nil "Can't parse RMPL plant model! Misplaced Identifier tag")))))
    expressions))





(defmethod %parse-rmpl-tag ((pm plant-model-rmpl) tag-name node)
  "For all other tags that are unknown")



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Required methods to implement the plant model interface
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defmethod parse-action ((action-string string) (pm plant-model-rmpl))

  (let* ((terms (split-sequence:split-sequence #\Space (string-trim "() " action-string))))
    ;; Ensure there is at least the Class.Method and self arguments
    (when (< (length terms) 2)
      (error "Malformed RMPL action: \"~a\"" action-string))


    (let ((method-name (first terms))
          (self (second terms))
          (args (cddr terms))
          found? method bindings
          preconditions postconditions)

      ;; Lookup the appropriate method
      (multiple-value-setq (method found?) (gethash method-name (plant-model-rmpl-methods-by-name pm)))
      (unless found?
        (error "Unknown RMPL method in: \"~a\"" action-string))

      ;; Compute a parameter binding
      (setf bindings (compute-parameter-bindings method self args))


      ;; Compute grounded preconditions and postconditions
      ;; Preconditions
      (dolist (p (preconditions method))
        (case (first p)
          (:mode-of-self
           (push (make-instance 'state-condition-rmpl
                                :predicate (format nil "(~a == ~a)" self (second p))
                                :plant-model pm)
                 preconditions))
          (:mode-of-argument
           (push (make-instance 'state-condition-rmpl
                                :predicate (format nil "(~a == ~a)" (get-binding (second p) bindings) (third p))
                                :plant-model pm)
                 preconditions))))
      ;; Postconditions
      (dolist (p (postconditions method))
        (case (first p)
          (:mode-of-self
           ;; Add a positive effect for this one
           (push (make-instance 'state-condition-rmpl
                                :predicate (format nil "(~a == ~a)" self (second p))
                                :plant-model pm)
                 postconditions)
           ;; Explicitly negate all other modes! Allows threats to work properly with Pike. This
           ;; effectively enforces that just one mode can hold at a time.
           (dolist (~p (complementary-modes (parent-class method) (second p)))
             (push (make-instance 'state-condition-rmpl
                                  :predicate (format nil "(~a == ~a)" self ~p)
                                  :plant-model pm
                                  :negative T)
                   postconditions)))

          (:mode-of-argument
           ;; Add a positive effect for this one
           (push (make-instance 'state-condition-rmpl
                                :predicate (format nil "(~a == ~a)" (get-binding (second p) bindings) (third p))
                                :plant-model pm)
                 postconditions)
           ;; Explicitly negate all other modes! Allows threats to work properly with Pike. This
           ;; effectively enforces that just one mode can hold at a time.
           (dolist (~p (complementary-modes (argument-type (second p)) (third p)))
             (push (make-instance 'state-condition-rmpl
                                  :predicate (format nil "(~a == ~a)" (get-binding (second p) bindings) ~p)
                                  :plant-model pm
                                  :negative T)
                   postconditions)))))


      ;; Assemble and return a finished RMPL action
      (make-instance 'action-rmpl
                     :plant-model pm
                     :dispatch action-string
                     :preconditions preconditions
                     :postconditions postconditions))))


(defmethod parse-state-condition ((state-condition-string string) (pm plant-model-rmpl))
  "Parses a state condition."
  (make-instance 'state-condition-rmpl
                 :predicate state-condition-string
                 :negative nil
                 :plant-model pm))


(defmethod parse-state ((string-state-conditions list) (pm plant-model-rmpl))
  "Parses and returns a state."
  (make-instance 'state-rmpl
                 :state-conditions (mapcar #'(lambda (ssc) (parse-state-condition ssc pm)) string-state-conditions)
                 :plant-model pm))


(defmethod state-conditions-equal ((sc1 state-condition-rmpl) (sc2 state-condition-rmpl))
  "Checks to see if two state conditions are equivalent."
  (equal (predicate sc1) (predicate sc2)))


(defun compute-parameter-bindings (method self args)
  (let ((arg-bindings (make-instance 'rmpl-argument-bindings)))
    (with-slots (bindings) arg-bindings
      ;; Set a special self binding
      (setf (gethash :|self| bindings) self)

      ;; Now set all the other args, in order!
      ;; First, ensure that the number of parameters match.
      (unless (= (length (arguments method)) (length args))
        (error "Invalid number of arguments called for RMPL method!"))

      ;; Now, assign in order
      (dotimes (i (length (arguments method)))
        (setf (gethash (nth i (arguments method)) bindings)
              (nth i args))))
    arg-bindings))

(defun get-binding (arg arg-bindings)
  "Helper function to look up the binding for this argument"
  (with-slots (bindings) arg-bindings
    (gethash arg bindings)))


(defmethod get-conditions ((action action-rmpl) when)
  "Returns the preconditions of this action"
  (ecase when
    (:at-start (action-rmpl-preconditions action))
    (:at-end nil)
    (:over-all nil)))



(defmethod get-effects ((action action-rmpl) when)
  "Returns the postconditions of this action"
  (ecase when
    (:at-start nil)
    (:at-end (action-rmpl-postconditions action))
    (:over-all nil)))


(defmethod action-dispatch ((action action-rmpl))
  "Returns what should be dispatched for this action"
  (action-rmpl-dispatch action))

(defmethod about ((state state-condition-rmpl))
  (predicate state))


(defmethod get-initial-state ((ps pike-session) (pm plant-model-rmpl))
  "Retrieve the initial state form the TPN"
  (let (predicates)
    ;; Construct an RMPl initial state
    (tpn:do-state-variables (sv (pike-tpn ps))
      (push (make-instance 'state-condition-rmpl
                           :plant-model pm
                           :predicate (format nil "(~a == ~a)" (tpn:name sv) (tpn:state-variable-initial sv))
                           :negative nil)
            predicates))

    (make-instance 'state-rmpl
                   :state-conditions predicates
                   :plant-model pm)))


(defmethod get-goal-state ((ps pike-session) (pm plant-model-rmpl))
  "RMPL programs don't have explicit goal states"
  (make-instance 'state-rmpl
                 :state-conditions nil
                 :plant-model pm))

(defmethod get-empty-state ((ps pike-session) (pm plant-model-rmpl))
  "Returns an empty state"
  (make-instance 'state-rmpl
                 :state-conditions nil
                 :plant-model pm))
