;;;; Copyright (c) 2011-2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Class definitions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass plant-model-pddl (plant-model)
  ((name
    :type string
    :initform "PDDL Plant Model"
    :initarg :name
    :accessor plant-model-name)
   (operator-name->operator
    :type hash-table
    :initform (make-hash-table :test #'equal)
    :documentation "A hash table mapping the names of
operators / actions, to the operator schema datastructures")
   (initial-conditions
    :type list
    :initform nil
    :documentation "A list of state conditions representing the
initial conditions")
   (goals
    :type list
    :initform nil
    :documentation "A list of state conditions representing the
goals"))
  (:documentation "Represents a PDDL plant model"))


(defclass state-condition-pddl (state-condition)
  ((predicate
    :type string
    :initarg :predicate
    :accessor predicate
    :documentation "A string representation of the predicate")))

(defmethod print-object ((sc state-condition-pddl) s)
  (with-slots (predicate negative?) sc
    (if (not negative?)
        (format s "~a" predicate)
        (format s "¬~a" predicate))))


(defclass action-pddl (action)
  ((dispatch
    :type string
    :initarg :dispatch
    :accessor action-pddl-dispatch)
   (conditions
    :type temporal-predicate-set
    :initarg :conditions
    :initform (make-instance 'temporal-predicate-set)
    :accessor conditions
    :documentation "Represents conditions at start, at end, and overall")
   (effects
    :type temporal-predicate-set
    :initarg :effects
    :initform (make-instance 'temporal-predicate-set)
    :accessor effects
    :documentation "Represents effects at start, at end, and overall")))

(defmethod print-object ((a action-pddl) s)
  "Pretty print"
  (with-slots (dispatch) a
    (format s dispatch)))


(defclass state-pddl (state)
  ()
  (:documentation "Represents a state, or a set of
PDDL predicates."))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Required methods to implement the plant model interface
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defmethod parse-action ((action-string string) (pm plant-model-pddl))
  "Parses the string representation of an action, computing conditions & effects
   as appropriate."
  (let* ((action-expression (expression-string->expression action-string))
         (action-name (first action-expression))
         (operator (get-operator-by-name pm action-name))
         (bindings (compute-pddl-parameter-bindings action-expression operator)))

    ;; Set them
    (make-instance 'action-pddl
                   :dispatch (expression->expression-string action-expression)
                   :conditions  (make-state-conditions (ground-predicates (conditions operator) bindings) pm)
                   :effects (make-state-conditions (ground-predicates (effects operator) bindings) pm)
                   :plant-model pm)))

(defmethod parse-state-condition ((state-condition-string string) (pm plant-model-pddl))
  "Parses and returns a state condition representation of this string."
  (make-instance 'state-condition-pddl
                 ;; Do a little whitespace cleanup
                 :predicate (expression->expression-string (expression-string->expression state-condition-string))
                 :negative nil
                 :plant-model pm))

(defmethod parse-state ((string-state-conditions list) (pm plant-model-pddl))
  "Parses and returns a state."
  (make-instance 'state-pddl
                 :state-conditions (mapcar #'(lambda (ssc) (parse-state-condition ssc pm)) string-state-conditions)
                 :plant-model pm))


(defmethod get-conditions ((action action-pddl) when)
  "Retrieve the cached predicates (each of which is a state condition) at the
  given time"
  (get-predicates-at-time (conditions action) :when when))


(defmethod get-effects ((action action-pddl) when)
  "Retrieve the cached predicates (each of which is a state condition) at the
  given time"
  (get-predicates-at-time (effects action) :when when))


(defmethod action-dispatch ((action action-pddl))
  "Retrieve the action dispatch."
  (action-pddl-dispatch action))


(defmethod about ((state-condition state-condition-pddl))
  "The predicate string is a representation that does not take into account positive / negativeness, so
   return that"
  (predicate state-condition))

(defmethod state-conditions-equal ((sc1 state-condition-pddl) (sc2 state-condition-pddl))
  "Returns nil/T if the two state conditions are equivalent."
  (equal (predicate sc1) (predicate sc2)))


(defmethod get-initial-state ((ps pike-session) (pm plant-model-pddl))
  "Return the initial conditions."
  (with-slots (initial-conditions) pm
      (make-instance 'state-pddl
                     :state-conditions (copy-seq initial-conditions)
                     :plant-model pm)))


(defmethod get-goal-state ((ps pike-session) (pm plant-model-pddl))
  "Return the goal."
  (with-slots (goals) pm
      (make-instance 'state-pddl
                     :state-conditions (copy-seq goals)
                     :plant-model pm)))


(defmethod get-empty-state ((ps pike-session) (pm plant-model-pddl))
  "Return an empty state."
  (make-instance 'state-pddl
                 :state-conditions nil
                 :plant-model pm))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Parsing and helper routines
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defmethod initialize-instance :after ((pm plant-model-pddl) &key
                                                               (domain-filename (error "Must specify domain file"))
                                                               (problem-filename (error "Must specify problem file")))
  "Upon creation, parse the plant model."
  (let ((operators (parse-pddl-domain-file domain-filename))
        (problem (parse-pddl-problem-file problem-filename)))
    (with-slots (operator-name->operator initial-conditions goals) pm

      ;; Populate hash table mapping operator names to operators
      (dolist (op operators)
        (setf (gethash (operator-name op) operator-name->operator) op))

      ;; Set up initial conditions
      (setf initial-conditions (make-state-conditions (problem-init problem) pm :negative nil))

      ;; Set up goal conditions
      (setf goals (make-state-conditions (problem-goal problem) pm :negative nil)))

    pm))


(defun get-operator-by-name (pm name)
  "Given the name of an operator, such as \"pick-up-block\", return the
  operator data structure it is associated with."
  (declare (type plant-model-pddl pm)
           (type string name))
  (with-slots (operator-name->operator) pm
    (let (op found?)
      (multiple-value-setq (op found?) (gethash name operator-name->operator))
      (unless found?
        (error "Can't find the operator corresponding to action name \"~a\"!" name))
      op)))



(defgeneric make-state-conditions (predicates pm &key &allow-other-keys))
(defmethod make-state-conditions ((predicates temporal-predicate-set) (pm plant-model-pddl) &key &allow-other-keys)
  "Takes in a temporal predicate set of (assuming grounded) predicates (each of which is a list of strings),
   and converts to a temporal predicate set of PDDL state conditions."
  (map-predicates predicates
                  #'(lambda (predicate when positive?)
                      (declare (ignore when))
                      (make-instance 'state-condition-pddl
                                     :predicate (expression->expression-string predicate)
                                     :negative (not positive?)
                                     :plant-model pm))
                  :filter-fn #'(lambda (predicate)
                                 (cond
                                   ((is-predicate-supported? predicate) T)
                                   (T (format t "~a Ignoring unsupported predicate ~a~%"
                                              (colorize :yellow "Warning:")
                                              (expression->expression-string predicate))
                                      nil)))))

(defmethod make-state-conditions ((predicates list) (pm plant-model-pddl) &key (negative nil))
  "Takes a list of (grounded) predicates (each of which is a list of strings,
   and converts them to a set of PDDL state conditions."
  (let (scs)
    (dolist (predicate predicates)
      (if (is-predicate-supported? predicate)
          (push (make-instance 'state-condition-pddl
                               :predicate (expression->expression-string predicate)
                               :negative negative
                               :plant-model pm)
                scs)
          (format t "~a Ignoring unsupported predicate ~a~%"
                  (colorize :yellow "Warning:")
                  (expression->expression-string predicate))))
    scs))



(defun expression-string->expression (expression-string)
  "Converts an expression string (such as \"(on blockA blockB\")
  into a list of lower case terms"
  (declare (type string expression-string))
  (cl-ppcre:split "[\\s]+" (string-downcase (string-trim " ()" expression-string))))


(defun expression->expression-string (expression)
  "Convert an expression (i.e., a list of strings) into a PDDL-esque string"
  (declare (type list expression))
  (format nil "(~{~a~^ ~})" expression))
