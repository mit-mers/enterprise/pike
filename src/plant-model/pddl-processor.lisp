;;;; Copyright (c) 2013-2015 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine
(in-package #:pike)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Core classes
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass operator-schema ()
  ((name
    :type string
    :initform ""
    :initarg :name
    :accessor operator-name
    :documentation "The name of this action (lowercase string).")
   (parameter-list
    :type list
    :initform nil
    :initarg :parameter-list
    :accessor parameter-list
    :documentation "An ordered list of the parameters accepted by this PDDL operator.")
   (conditions
    :type temporal-predicate-set
    :initform (make-instance 'temporal-predicate-set)
    :accessor conditions
    :documentation "The conditions (at start, at end, and overall)")
   (effects
    :type temporal-predicate-set
    :initform (make-instance 'temporal-predicate-set)
    :accessor effects
    :documentation "The effects (at start, at end, and overall)")
   (lower-bound
    :initform 0.0
    :initarg :lower-bound
    :accessor lower-bound
    :documentation "The lower temporal bound of this action.")
   (upper-bound
    :initform +inf+
    :initarg :upper-bound
    :accessor upper-bound
    :documentation "The upper temporal bound of this action."))
  (:documentation "Represents a temporal PDDL operator schema."))


(defmethod print-object ((op operator-schema) s)
  "Pretty print"
  (with-slots (name) op
    (format s "<OPERATOR-SCHEMA ~a>" name)))


(defclass pddl-problem ()
  ((name
    :initform "default-problem-name"
    :initarg :problem-name
    :accessor problem-name
    :documentation "The name of this problem.")
   (domain-name
    :initform ""
    :initarg :problem-domain
    :accessor problem-domain
    :documentation "The name of the problem domain")
   (init
    :initform nil
    :initarg :problem-init
    :accessor problem-init
    :documentation "The initial conditions")
   (goal
    :initform nil
    :initarg :problem-goal
    :accessor problem-goal
    :documentation "The problem goals")
   (objects
    :initform nil
    :initarg :problem-objects
    :accessor problem-objects
    :documentation "Tuples of objects"))
  (:documentation "Data structures corresponding to a PDDL problem file."))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Keeping track of predicates
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass temporal-predicate-set ()
  ((predicates
    :type list
    :initform nil
    :accessor predicates))
  (:documentation "Helps keep track of lists of predicates, such as
when they occur and if they're positive or negative"))

(defmethod print-object ((tps temporal-predicate-set) s)
  (with-slots (predicates) tps
    (format s "~{~a~^, ~}" (loop for (p when positive) in predicates
                              collecting (format nil "(~a~a ~a~a)"
                                                 when
                                                 (if positive "" " (not")
                                                 p
                                                 (if positive "" ")"))))))


(defun add-predicate (tps p &key
                              (when (error "Must specify when"))
                              (positive (error "Must specify if positive")))
  "Adds a predicate (positive or negative) at the given time to the database"
  (declare (type temporal-predicate-set tps))
  (with-slots (predicates) tps
    (push (list p when positive) predicates)))


(defun get-predicates (tps &key
                             (when (error "Must specify when"))
                             (positive (error "Must specify if positive")))
  "Retrieves all predicates at the corresponding time and having the given positiveness"
  (with-slots (predicates) tps
    (loop for p in predicates
       when (and (equalp (second p) when)
                 (equalp (third p) positive))
       collect (first p))))


(defun get-predicates-at-time (tps &key
                                     (when (error "Must specify when")))
  "Retrieves all predicates at the corresponding time (positive or negative)"
  (declare (type temporal-predicate-set tps))
  (append
   (get-predicates tps :positive T :when when)
   (get-predicates tps :positive nil :when when)))


(defun map-predicates (tps fn &key (filter-fn #'(lambda (p) (declare (ignore p)) T)))
  "Takes in a fn: predicate, when, positive -> new predicate, and returns
  the mapping from all current predicates to new ones as a new
  temporal predicate set.

  Also accepts an optinal filter-fn parameter, that is applied to each predicate potentially being
  added. If it returns T, it's added, otherwise if nil, it's not!"
  (declare (type temporal-predicate-set tps)
           (type function fn))
  (let ((tps-new (make-instance 'temporal-predicate-set)))
    (with-slots (predicates) tps
      (dolist (p predicates)
        (when (funcall filter-fn p)
          (add-predicate tps-new (funcall fn (first p) (second p) (third p)) :when (second p) :positive (third p)))))
    tps-new))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Parsing from a file
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun parse-pddl-domain-file (filename)
  "Parse the file symbolically."
  (let* ((operator-file (read-file-parsing-parenthesis filename))) ;read-file-forms
    (loop for object in operator-file do
         (when (listp object)
           (let* ((label (first object)))
             (when (equalp label :REQUIREMENTS)
               ;; See if :DURATIVE-ACTIONS is here.
               (cond
                 ((find :DURATIVE-ACTIONS object)
                  (return-from parse-pddl-domain-file (parse-pddl-domain-file-temporal operator-file)))
                 (t
                  ;; (return-from parse-pddl-operator-file (parse-pddl-domain-file-non-temporal operator-file))
                  (error "Non-temporal PDDL currently not supported!")))))))))


(defun parse-pddl-domain-file-temporal (operator-file)
  "Parses a PDDL operator file, and returns a list of operator schema.
   This one only works for old-style PDDL files with no durative actions."
  (let (operators)
    (dolist (object operator-file)
      (when (listp object)
        (let ((label (first object)))
          (when (equal label :DURATIVE-ACTION)
            ;; Process this action!
            (let* ((operator (make-instance 'operator-schema))
                   (action-name (string-downcase (string-trim " " (string (second object)))))
                   (parameters (getf object :parameters))
                   (conditions-sexp (getf object :condition))
                   (effects-sexp (getf object :effect)))

              (with-slots (conditions effects parameter-list name) operator
                ;; Set the name
                (setf name action-name)

                ;; Process the parameter list
                (dolist (word parameters)
                  (when (equal (char (string-trim " " (string word)) 0) #\?)
                    (push (string-downcase (string word)) parameter-list)))
                (setf parameter-list (nreverse parameter-list))

                ;; Process the conditions
                (unless (listp conditions-sexp)
                  (error "Conditions must be a list: ~a" conditions-sexp))
                (if  (equal (first conditions-sexp) 'and)
                     ;; We have a conjunction. All set!
                     (setf conditions-sexp (rest conditions-sexp))
                     ;; Otherwise, assume it's a single fluent
                     (setf conditions-sexp (list conditions-sexp)))
                (dolist (condition-sexp conditions-sexp)
                  (cond
                    ((is-temporally-annotated-predicate? condition-sexp)
                     (let* ((cond-temp (parse-temporally-annotated-predicate condition-sexp))
                            (time (first cond-temp))
                            (positive (second cond-temp))
                            (p (third cond-temp))
                            (predicate-processed (mapcar #'string-downcase
                                                         (mapcar #'(lambda (x) (format nil "~a" x)) p))))

                       (if (is-predicate-supported? predicate-processed)
                           (add-predicate conditions predicate-processed
                                          :positive positive :when time)
                           (format t "~a Ignoring unsupported condition ~a~%"
                                   (colorize :yellow "Warning:")
                                   predicate-processed))))

                    (T
                     (format t "~a Ignoring unsupported condition ~a~%"
                             (colorize :yellow "Warning:")
                             condition-sexp))))

                ;; Process the effects
                (unless (listp effects-sexp)
                  (error "Effects must be a list: ~a" effects-sexp))
                (if  (equal (first effects-sexp) 'and)
                     ;; We have a conjunction. All set!
                     (setf effects-sexp (rest effects-sexp))
                     ;; Otherwise, assume it's a single fluent
                     (setf effects-sexp (list effects-sexp)))
                ;; Iterate over all the effects
                (dolist (eff effects-sexp)
                  (cond
                    ((is-temporally-annotated-predicate? eff)
                     (let* ((eff-temp (parse-temporally-annotated-predicate eff))
                            (time (first eff-temp))
                            (positive (second eff-temp))
                            (p (third eff-temp))
                            (predicate-processed (mapcar #'string-downcase
                                                         (mapcar #'(lambda (x) (format nil "~a" x)) p))))

                       (if (is-predicate-supported? predicate-processed)
                           (add-predicate effects predicate-processed
                                          :positive positive :when time)
                           (format t "~a Ignoring unsupported effect ~a~%"
                                   (colorize :yellow "Warning:")
                                   predicate-processed))))

                    (T
                     (format t "~a Ignoring unsupported effect ~a~%"
                             (colorize :yellow "Warning:")
                             eff))))

                ;; Sanity check and print out warning for unsupported annotations
                ;; No negative conditions are allowed
                (when (or (get-predicates conditions :when :at-start :positive nil)
                          (get-predicates conditions :when :overall :positive nil)
                          (get-predicates conditions :when :at-end :positive nil))
                  (format t (colorize :yellow "Warning: Negative conditions are not allowed~%")))

                ;; No overall effects are allowed either
                (when (or (get-predicates effects :when :overall :positive nil)
                          (get-predicates effects :when :overall :positive t))
                  (format t (colorize :yellow "Warning: Overall effects are not allowed~%")))


                ;; Push it onto the list
                (push operator operators)))))))
    operators))


(defun is-predicate-supported? (predicate)
  "Takes in a predicate (which is a list of strings), and returns T if
  it is supported, nil otherwise."
  (not (or (is-numeric-predicate? predicate))))


(defun is-numeric-predicate? (predicate)
  "Takes in a predicate (which is a list of strings), and return T if it
   uses the numeric fluent features of PDDL, and nil otherwise"
  (member (first predicate)
          `("=" ">=" "<=" "increase" "decrease" "scale-up" "scale-down" "assign")
          :test #'equal))


(defun is-temporally-annotated-predicate? (p)
  "Returns T if this predicate is a temporally annotated predicate, nil otherwise."
  (let ((ta1 (string-downcase (string (first p)))))
    (member ta1 `("at" "over") :test #'equal)))


(defun parse-temporally-annotated-predicate (p)
  "Parses a predicate that uses the PDDL 2.1 temporal annotations"
  (unless (= (length p) 3)
    (error "Malformed temporal annotation in PDDL: ~a" p))

  (let* ((ta1 (string-downcase (string (first p))))
         (ta2 (string-downcase (string (second p))))
         (pred-parsed (parse-positive-or-negative-predicate (third p)))
         (positive? (first pred-parsed))
         (pred (second pred-parsed)))

    (cond
      ((and (equal ta1 "at") (equalp ta2 "start"))
       (list :at-start positive? pred))

      ((and (equal ta1 "at") (equalp ta2 "end"))
       (list :at-end positive? pred))

      ((and (equal ta1 "over") (equal ta2 "all"))
       (list :overall positive? pred))

      (t
       (error "Unknown temporal annotation! ~a" p)))))


(defun read-file-parsing-parenthesis (filename)
  "Use Lisp's built in reader to read in something and
   automagically parse parenthesis!"
  (with-open-file (stream filename :direction :input)
    (read stream)))


(defun parse-positive-or-negative-predicate (p)
  "Takes something like (clear-above ?t) or (not (clear-above ?t))
   and returns either (t (clear-above ?t)) or (nil (clear-above ?t))
   depending on positive or negative."
  (if (equalp  (string (first p)) "NOT")
      (list nil (second p))
      (list t p)))


(defun parse-pddl-domain-file-non-temporal (operator-file)
  "Parses a PDDL operator file, and returns a list of operator schema.
   This one only works for old-style PDDL files with no durative actions."
  ;;; TODO - this doesn't quite work after code cleanup! Not hard to fix, but
  ;;; not doing right now.
  (let* ((operators nil))
    (loop for object in operator-file do
         (when (listp object)
           (let* ((label (first object)))
             ;; (format t "~a~%" label)
             (when (equalp label :ACTION)
               ;; Process this action!
               (let* ((operator (make-instance 'operator-schema))
                      (action-name (string-downcase (string-trim " " (string (second object)))))
                      (parameters (getf object :parameters))
                      (preconditions-sexp (getf object :precondition))
                      (effects-sexp (getf object :effect))
                      (parameter-list nil))

                 (with-slots (conditions effects) operator

                   ;; Process the parameter list
                   (loop for word in parameters do
                        (when (equalp (char (string-trim " " (string word)) 0) #\?)
                          (push word parameter-list)))
                   (setf parameter-list (nreverse parameter-list))

                   ;; Process the preconditions
                   (dolist (precond preconditions-sexp)
                     (when (listp precond)
                       (add-predicate conditions precond :positive T :when :at-start)))

                   ;; Process the effects
                   (dolist (effect effects-sexp)
                     (when (listp effect)
                       (if (equalp (string (first effect)) "NOT")
                           (add-predicate effects (second effect) :positive nil :when :at-end)
                           (add-predicate effects effect :positive T :when :at-end))))

                   ;; Set up an operator schema and push it onto the list
                   (setf (operator-name operator) action-name)
                   (setf (parameter-list operator) parameter-list)
                   (push operator operators)))))))
    operators))


(defun parse-pddl-problem-file (filename)
  "Parse a PDDL problem (fact) file!"
  (let ((problem-file (first (asdf::read-file-forms filename)))
        (problem (make-instance 'pddl-problem)))

    (with-slots (init goal objects name domain-name) problem
      (dolist (object problem-file)
        (when (listp object)
          (let ((label (first object)))

            ;; Process each initial condition
            (when (equal label :init)
              (dolist (term object)
                (when (listp term)
                  (push (mapcar #'string-downcase (mapcar #'(lambda (x)
                                                              (format nil "~a" x)) term))
                        init))))

            ;; Process each goal condition
            (when (equal label :goal)
              (dolist (term (second object))
                (when (listp term)
                  (push (mapcar #'string-downcase (mapcar #'(lambda (x)
                                                              (format nil "~a" x)) term))
                        goal))))

            ;; Keep track of a list of objects
            (when (equal label :objects)
              (loop for i from 0 to (- (/ (- (length object) 1) 3) 1) do
                   (push (list (nth (+ (* 3 i) 1) object) (nth (+ (* 3 i) 3) object)) objects)))

            ;; Set the domain name
            (when (equal label :domain)
              (setf domain-name (string-downcase (string (second object)))))

            ;; Set the problem name
            (when (equalp label 'problem)
              (setf name (string-downcase (string (second object)))))))))

    problem))


(defun write-pddl-problem-file (problem stream)
  "Write a problem file to a given stream"
  (format stream "(define (problem ~s)~%" (problem-name problem))
  (format stream "    (:domain ~s)~%" (problem-domain problem))
  (format stream "    (:objects ~%")
  (loop for obj in (problem-objects problem) do
       (format stream "        ~a - ~a~%" (first obj) (second obj)))
  (format stream "    )~%    (:init~%")
  (loop for i in (problem-init problem) do
       (format stream "        ~a~%" i))
  (format stream "    )~%    (:goal~%        (and~%")
  (loop for g in (problem-goal problem) do
       (format stream "            ~a~%" g))
  (format stream ")))"))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Bindings and grounding
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun compute-pddl-parameter-bindings (action-expression operator)
  "Compute a parameter binding list, or namely a hashtable mapping operator arguments
  to the parameters they are bound to in this action."
  (let* ((action-terms (copy-seq action-expression))
         (params (copy-seq (parameter-list operator)))
         (bindings (make-hash-table :test #'equal))
         (action-name (pop action-terms))) ;; The action name

    (unless (= (length action-terms) (length params))
      (error (format nil "Wrong number of arguments for action \"~a\"! Got parameters: ~a. Cannot compute PDDL bindings" action-name action-terms)))

    (dotimes (i (length params))
      (setf (gethash (pop params) bindings) (pop action-terms)))

    ;; Return the bindings
    bindings))


(defun lookup-binding (bindings var)
  "Lookup a binding for the given variable from the given list of bindings."
  (let (term found?)
    (multiple-value-setq (term found?) (gethash var bindings))
    (unless found?
      (error "Could not bind variable \"~a\"!" var))
    term))


(defun apply-binding (expression bindings)
  "Apply a binding (mapping from variables to values) to an expression (list of things that might be variables)
   to yield a new expression."
  (let (bound-expression)
    (dolist (term expression)
      (if (equal (char term 0) #\?)
          (push (lookup-binding bindings term) bound-expression)
          (push term bound-expression)))
    (nreverse bound-expression)))



(defun ground-predicates (predicates bindings)
  "Takes in a temporal predicate set of ungrounded predicates, and grounds them. Each is
   still a list-of-strings expression. Returns a new temporal predicate set."
  (map-predicates predicates
                  #'(lambda (expression when positive)
                      (declare (ignore when positive))
                      (apply-binding expression bindings))))
