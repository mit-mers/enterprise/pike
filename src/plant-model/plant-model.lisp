;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Generic version of a plant model (specific implementations are
;; in the accompanying files). Any implementation must subclass:
;;      * plant-model
;;      * state-condition
;;      * state
;;      * action
;; and implement the generic methods listed below.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass plant-model ()
  ((name
    :type string
    :initform "(Fill in model type)"
    :initarg :name
    :accessor plant-model-name)))


(defclass plant-model-member ()
  ((plant-model
    :type plant-model
    :initarg :plant-model
    :accessor plant-model
    :initform (error "Must specify a plant model!")))
  (:documentation "All state conditions, states, and actions
must be associated with a plant model instance. This mixin
ensures that."))


(defclass state-condition (plant-model-member)
  ((negative?
    :type (or nil T)
    :accessor negative?
    :initarg :negative
    :initform nil
    :documentation "T if this is a negative condition such as (not (on block a b))."))
  (:documentation "A state-condition represents a specific
property of the state - such as (on-block a b) for exmaple."))


(defclass state (plant-model-member)
  ((state-conditions
    :type list
    :initarg :state-conditions
    :accessor state-conditions
    :initform nil
    :documentation "A list of true state conditions / predicates
(each of type state-condition)"))
  (:documentation "A state is, intuitively, a bunch of
state conditions that are currently true."))


(defmethod print-object ((state state) s)
  "Pretty print this state"
  (format s "#<STATE ~{~a~^ ~}>" (state-conditions state)))


(defclass action (plant-model-member)
  ()
  (:documentation "An action can have conditions
and effects."))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Generic methods
;;   Any implementation must override these to be valid!
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric parse-action (action-string plant-model)
  (:documentation "Parses a string representation of an action
into an appropriate action objects, which is then returned."))

(defmethod parse-action (action-string plant-model)
  (error "This method isn't implemented but needs to be!"))



(defgeneric get-conditions (action when)
  (:documentation "Get conditions for this action.
Returns a list of state-conditions.
when can be any of :AT-START, :AT-END, or :OVER-ALL"))

(defmethod get-conditions (action when)
  (error "This method isn't implemented but needs to be!"))



(defgeneric get-effects (action when)
  (:documentation "Gets the effects of this action.
Returns a list of state conditions.
when can be any of :AT-START, :AT-END, or :OVER-ALL"))

(defmethod get-effects (action when)
  (error "This method isn't implemented but needs to be!"))



(defgeneric action-dispatch (action)
  (:documentation "Returns a string that should be dispatched
when this action is executed. Note that this is deliberately not
just a slot in the action class, in case we want to call a function
or do anything fancy dynamically online."))

(defmethod action-dispatch (action)
  (error "This method isn't implemented but needs to be!"))



(defgeneric about (state-condition)
  (:documentation "Returns a of \"summary\" of the state
condition that doesn't take into account negation, and can
be compared using (equal ...). For instance, a state condition
#<SC (on-block A B)> and #<SC (not (on-block A B))> could both
return the string \"(on-block A B)\", since strings compare
under equal and the not isn't in that string."))

(defmethod about (state-condition)
  (error "This method isn't implemented but needs to be!"))


(defgeneric state-conditions-equal (sc1 sc2)
  (:documentation "Returns T or nil, corresponding to whether the
given state conditions are equivalent. For example, if we have two
state conditions such as #<SC (on-block A B)> and #<SC (on-block A B)>,
T should be returned. This function should not change if the arguments
are switched."))

(defmethod state-conditions-equal (sc1 sc2)
  (error "This method isn't implemented but needs to be!"))


(defgeneric parse-state-condition (state-condition-string plant-model)
  (:documentation "Given a string representation of a state condition
and a plant model, return a state-condition object representing that string.
For example, calling this method on the string \"(on-block A B)\" should
return a state-condition representation of it, such as #<SC (on-block A B)>."))

(defmethod parse-state-condition (state-condition-string plant-model)
  (error "This method isn't implemented but needs to be!"))


(defgeneric parse-state (string-state-conditions plant-model)
  (:documentation "GIven a list of string representations of
state conditions, parse it into a state object."))

(defmethod parse-state (string-state-conditions plant-model)
  (error "This method isn't implemented but needs to be!"))


;; TODO - since we have get-empty-state, we may no
;; longer need get-initial-state or get-goal-state. Is
;; that true? If so we can remove it.
(defgeneric get-initial-state (ps plant-model)
  (:documentation "Returns a state object representing
the initial state of the world."))

(defmethod get-initial-state (ps plant-model)
  (error "This method isn't implemented but needs to be!"))


(defgeneric get-goal-state (ps plant-model)
  (:documentation "Returns a state object representing
the goal state of the world."))

(defmethod get-goal-state (ps plant-model)
  (error "This method isn't implemented but needs to be!"))

(defgeneric get-empty-state (ps plant-model)
  (:documentation "Returns an \"empty\" state
object."))

(defmethod get-empty-state (ps plant-model)
  (error "This method isn't implemented but needs to be!"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Applying effects
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric apply-effects! (state effects)
  (:documentation "Applies the given effects to the given state. The
state object itself will be modified. Effects should be a list of
state conditions."))

(defmethod apply-effects! ((state state) (effects list))
  "Apply the given effects to the given state, modifying it. Effects
is a list of state conditions."
  (with-slots (state-conditions) state
    (let ((effects-negative (loop for sc in effects when (negative? sc) collect sc))
          (effects-positive (loop for sc in effects when (not (negative? sc)) collect sc)))
      ;; Delete any state conditions from the state that are negative effects with equal (about ...).
      (dolist (effect-negative effects-negative)
        (setf state-conditions (delete (about effect-negative) state-conditions :key #'about :test #'equal)))
      ;; Add in the effects, being sure to disallow duplicates
      (dolist (effect-positive effects-positive)
        (when (not (member (about effect-positive) state-conditions :key #'about :test #'equal))
          (push effect-positive state-conditions))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Errors and conditions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-condition load-plant-model-error (error)
  ((reason
    :initarg :reason
    :accessor reason
    :type string)))
