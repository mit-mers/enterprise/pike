;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Tests in support of thesis / journal paper.
;;
;; These tests run on the k-scalable domain, which
;; intuitively show that concurrent intent recognition
;; and adaptation is preferable to separate intent recognition
;; and adaptation. Furthermore, maintaining multiple candidates
;; enables simultaneous IR&A.
;;
;; Prerequisite to running these tests: CLARK (from Pedro)
;; is installed (you'll need to change the Python PATH below).
;; CLARK is used (specifically, RMPyL) to generate the test
;; TPNs easily.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(in-package :pike/test)


(defun test-k-scalable-domain ()
  (let ((filename (asdf:system-relative-pathname :pike "test-cases/k-scalable/data.dat"))
        (iss (make-instance 'intent-sampler
                            :bases (list 2 3)
                            :range (list 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14)))
        (k-min 2)
        (k-max 10000)

        (time-failure-factor 0.5)
        (temporal-offset-max 0.5)

        (N-iterations 2000))

    ;; Set up the intent sampler
    (initialize-intent-sampler iss)

    ;; Delete the file
    ;;(with-open-file (s filename :direction :output :if-exists :supersede :if-does-not-exist :create))

    ;; Now open it, and keep adding stuff to it.
    (dotimes (i N-iterations)
      (let (replan-count-separate-ir&a
            replan-count-concurrent-ir&a
            time-separate-ir&a
            time-concurrent-ir&a
            time-penalty-separate-ir&a
            time-penalty-concurrent-ir&a
            ps k_target factorization structure temporal_offsets)
        ;; Sample some structure
        ;; Sample logarathmically for the desired number of intents
        (setf k_target (logarithmic-sample k-min k-max))
        ;; Get a structure from the intent sampler that will come as close to this target.
        (setf factorization (intent-sampler-closest-factorization iss k_target))
        (setf structure (intent-sampler-unroll-factorization iss factorization))
        (random-shuffle structure)
        ;; Generate random temporal structure
        (setf temporal_offsets (loop for i from 1 to (* 2 (length structure)) collect (random temporal-offset-max)))
        (format t "k_target: ~a, Structure: ~a, Temporal offsets: ~a~%" k_target structure temporal_offsets)

        ;; (multiple-value-setq (ps replan-count) (run-separate-ir&a structure))
        ;; (multiple-value-setq (ps replan-count) (run-concurrent-ir&a structure))

        (multiple-value-setq (ps replan-count-separate-ir&a time-separate-ir&a time-penalty-separate-ir&a)     (run-separate-ir&a-timed   structure temporal_offsets :time-failure-factor time-failure-factor))
        (multiple-value-setq (ps replan-count-concurrent-ir&a time-concurrent-ir&a time-penalty-concurrent-ir&a) (run-concurrent-ir&a-timed structure temporal_offsets :time-failure-factor time-failure-factor))

        ;; Append data to the file.
        (with-open-file (s filename :direction :output :if-exists :append :if-does-not-exist :create)
          (format s "~{~a~^,~}: ~a ~a ~a ~a ~a ~a [~{~a~^,~}]~%" structure replan-count-separate-ir&a time-separate-ir&a time-penalty-separate-ir&a replan-count-concurrent-ir&a time-concurrent-ir&a time-penalty-concurrent-ir&a temporal_offsets))))))


(defun run-separate-ir&a (structure)
  (let* (po ;; Pike options and session
         ps
         (execution-complete nil) ;; t if we've completed execute-replan loop successfully.
         intent-real ;; Real human's intent; kept secret from Pike though revealed online.
         partial-intent-revealed ;; The assignments of intent-real that have been observed / revealed online
         (i_last_revealed 0) ;; Index of variable last revealed (0 for none)
         (replan-count 0))

    ;; Sample the human's "real" / true intent. Will be kept a secret from Pike,
    ;; except that it's revealed one assignment at a time during online execution.
    (setf intent-real (sample-k-scalable-intent :structure structure))
    (format t "Real intent: ~a~%" intent-real)

    ;; Begin execute-replan loop.
    (loop while (not execution-complete) do
         (let (inferred-intent)

           ;; Create a problem instance (starting from wherever we left off)
           (generate-k-scalable-tpn-suffix :structure structure
                                           :i_start (if (= 0 i_last_revealed)
                                                        1
                                                        i_last_revealed))

           ;; Sample a possible "inferred" intent -- i.e., some full assignment to uncontrollable choices
           ;; that is consistent with the partially-observed "real" intent. This therefore represents
           ;; a single hypothesis.
           (setf inferred-intent (sample-k-scalable-intent :structure structure
                                                           :observations partial-intent-revealed))
           (format t "Inferred intent: ~a~%" inferred-intent)
           (format t "i-last-revealed: ~a~%" i_last_revealed)

           ;; Create a pike session that will load in this domain / problem
           (setf po (make-instance 'pike-options
                                   :plan-filename (pike-test-file "k-scalable/plan.tpn")
                                   :domain-filename (pike-test-file "k-scalable/domain.pddl")
                                   :problem-filename (pike-test-file "k-scalable/problem.pddl")
                                   :epsilon 0.005
                                   :constraint-knowledge-base-type :piatms
                                   :debug-mode nil))
           (setf ps (create-pike-session po))


           ;; Compile the plan
           (pike-compile ps)

           ;; Commit to the inferred intent (a single hypothesis)
           (let ((inferred-intent-copy (copy-seq inferred-intent)))
             (dotimes (i i_last_revealed)
               (pop inferred-intent-copy))
             (loop for (var-name val) in inferred-intent-copy do
                  (pike::pike-notify-choice-made ps var-name val)))

           ;; Add appropriate hooks
           (pike-register-hook ps :updated-choices-being-waited-on
                               #'(lambda (choices time)
                                   (declare (ignore time))
                                   (dolist (v choices)

                                     (let* ((var-name (pike::decision-variable-name v))
                                            (val (lookup-variable-in-k-scalable-intent intent-real var-name)))
                                       (push `(,var-name ,val) partial-intent-revealed)
                                       (setf i_last_revealed (parse-integer (subseq var-name 1))) ;; Read from index, like H23
                                       (pike::pike-notify-choice-made ps var-name val)))))

           ;; Simulate execution!
           (run-pike-with-sim-dt ps
                                 :disturbances nil
                                 :dt 0.1)
           ;; Help prevent Emacs's color parsing from getting confused.
           (format t "~c[0m~c[0m" #\esc #\esc)

           ;; HACK!
           ;; (setf execution-complete t)

           ;; If we're done, break out of the loop
           (if (eql :complete (execution-context-status (pike-execution-context ps)))
               (setf execution-complete t)
               (incf replan-count))))

    (sb-ext:gc :full t)
    ;; Analyze the results

    (values ps replan-count)))



(defun run-concurrent-ir&a (structure)
  (let* (po ;; Pike options and session
         ps
         (execution-complete nil) ;; t if we've completed execute-replan loop successfully.
         intent-real ;; Real human's intent; kept secret from Pike though revealed online.
         partial-intent-revealed ;; The assignments of intent-real that have been observed / revealed online
         (i_last_revealed 0) ;; Index of variable last revealed (0 for none)
         (replan-count 0))

    ;; Sample the human's "real" / true intent. Will be kept a secret from Pike,
    ;; except that it's revealed one assignment at a time during online execution.
    (setf intent-real (sample-k-scalable-intent :structure structure))
    (format t "Real intent: ~a~%" intent-real)

    ;; Begin execute-replan loop.
    (loop while (not execution-complete) do
         (let (inferred-intent)

           ;; Create a problem instance (starting from wherever we left off)
           (generate-k-scalable-tpn-suffix :structure structure
                                           :i_start (if (= 0 i_last_revealed)
                                                        1
                                                        i_last_revealed))


           ;; No need to sample an inferred intent here -- Pike will keep track of
           ;; multiple hypotheses concurrently
           (format t "i-last-revealed: ~a~%" i_last_revealed)

           ;; Create a pike session that will load in this domain / problem
           (setf po (make-instance 'pike-options
                                   :plan-filename (pike-test-file "k-scalable/plan.tpn")
                                   :domain-filename (pike-test-file "k-scalable/domain.pddl")
                                   :problem-filename (pike-test-file "k-scalable/problem.pddl")
                                   :epsilon 0.005
                                   :constraint-knowledge-base-type :piatms
                                   :debug-mode nil))
           (setf ps (create-pike-session po))

           ;; Compile the plan
           (pike-compile ps)

           ;; Add appropriate hooks
           (pike-register-hook ps :updated-choices-being-waited-on
                               #'(lambda (choices time)
                                   (declare (ignore time))
                                   (dolist (v choices)

                                     (let* ((var-name (pike::decision-variable-name v))
                                            (val (lookup-variable-in-k-scalable-intent intent-real var-name)))
                                       (push `(,var-name ,val) partial-intent-revealed)
                                       (setf i_last_revealed (parse-integer (subseq var-name 1))) ;; Read from index, like H23
                                       (pike::pike-notify-choice-made ps var-name val)))))

           ;; Simulate execution!
           (run-pike-with-sim-dt ps
                                 :disturbances nil
                                 :dt 0.1)
           ;; Help prevent Emacs's color parsing from getting confused.
           (format t "~c[0m~c[0m" #\esc #\esc)


           ;; HACK!
           ;; (setf execution-complete t)


           ;; If we're done, break out of the loop
           (if (eql :complete (execution-context-status (pike-execution-context ps)))
               (setf execution-complete t)
               (incf replan-count))))

     (sb-ext:gc :full t)
    ;; Analyze the results

     (values ps replan-count)))


(defun run-separate-ir&a-timed (structure temporal_offsets &key (time-failure-factor 0.5))
  (let* (po ;; Pike options and session
         ps
         (execution-complete nil) ;; t if we've completed execute-replan loop successfully.
         intent-real ;; Real human's intent; kept secret from Pike though revealed online.
         partial-intent-revealed ;; The assignments of intent-real that have been observed / revealed online
         (i_last_revealed 0) ;; Index of variable last revealed (0 for none)
         (replan-count 0)
         (time 0)
         (time-compile 0)
         (time-penalty 0))

    ;; Sample the human's "real" / true intent. Will be kept a secret from Pike,
    ;; except that it's revealed one assignment at a time during online execution.
    (setf intent-real (sample-k-scalable-intent :structure structure))
    (format t "Real intent: ~a~%" intent-real)

    ;; Begin execute-replan loop.
    (loop while (not execution-complete) do
         (let (inferred-scenario inferred-scenario-suffix)

           (setf time-compile
                 (measure-time

                  ;; Create a problem instance (starting from wherever we left off)
                  (generate-k-scalable-tpn-suffix :structure structure
                                                  :temporal_offsets temporal_offsets
                                                  :i_start (if (= 0 i_last_revealed)
                                                               1
                                                               i_last_revealed))

                  ;; Sample a possible "inferred" scenario -- i.e., some full assignment to both controllable and uncontrollable choices
                  ;; that is consistent with the partially-observed "real" intent. This therefore represents
                  ;; a single hypothesis.
                  ;; This represents "really fast TPN planning".
                  (setf inferred-scenario (sample-k-scalable-intent-and-adaptation :structure structure
                                                                                   :observations partial-intent-revealed))

                  ;; Commit to the inferred intent (a single hypothesis)
                  ;; (will get passed into pike-options later).
                  ;; This will effectively "flatten" the TPN to a single candidate subplan
                  ;; (before temporal reasoning!)
                  (setf inferred-scenario-suffix  (copy-seq inferred-scenario))
                  (dotimes (i (1- i_last_revealed))
                    (pop inferred-scenario-suffix)
                    (pop inferred-scenario-suffix))

                  (format t "Inferred scenario: ~a~%" inferred-scenario)
                  (format t "i-last-revealed: ~a~%" i_last_revealed)

                  ;; Create a pike session that will load in this domain / problem
                  (setf po (make-instance 'pike-options
                                          :plan-filename (pike-test-file "k-scalable/plan.tpn")
                                          :domain-filename (pike-test-file "k-scalable/domain.pddl")
                                          :problem-filename (pike-test-file "k-scalable/problem.pddl")
                                          :epsilon 0.005
                                          :initial-choice-assignments inferred-scenario-suffix ;; Causes TPN to become just this candidate subplan basically
                                          :constraint-knowledge-base-type :piatms
                                          :debug-mode nil))
                  (setf ps (create-pike-session po))


                  ;; Compile the plan
                  (pike-compile ps)))

           ;; Only count this time if we haven't replanned yet (first run!)
           (cond
             ((= replan-count 0)
              (format t "Not counting first-compilation time (assumed to be offline): ~a s~%" time-compile))
             (t
              (format t "Counting re-compilation time: ~a s~%" time-compile)
              (incf time time-compile)))


           ;; Add appropriate hooks
           (pike-register-hook ps :event-executed
                               #'(lambda (ps event time)
                                   (declare (ignore time))

                                   (let ((var (gethash event (pike::ldg-at-event->decision-variable (pike::pike-ldg ps)))))
                                     ;; Commit to that variable
                                     (when (and var
                                                (not (pike::controllable? var)))

                                       (let* ((var-name (pike::decision-variable-name var))
                                              (val (lookup-variable-in-k-scalable-intent intent-real var-name)))
                                         (push `(,var-name ,val) partial-intent-revealed)
                                         (setf i_last_revealed (parse-integer (subseq var-name 1))) ;; Read from index, like H23
                                         (pike::pike-notify-choice-made ps var-name val))))))

           (pike-register-hook ps :updated-choices-being-waited-on
                               #'(lambda (choices time)
                                   (declare (ignore time))
                                   (dolist (v choices)

                                     (let* ((var-name (pike::decision-variable-name v))
                                            (val (lookup-variable-in-k-scalable-intent intent-real var-name)))
                                       (push `(,var-name ,val) partial-intent-revealed)
                                       (setf i_last_revealed (parse-integer (subseq var-name 1))) ;; Read from index, like H23
                                       (pike::pike-notify-choice-made ps var-name val)))))


           ;; Simulate execution with time!
           (incf time (measure-time (run-pike-with-sim ps)))


           ;; Help prevent Emacs's color parsing from getting confused.
           (format t "~c[0m~c[0m" #\esc #\esc)

           ;; HACK!
           ;; (setf execution-complete t)

           ;; If we're done, break out of the loop
           (cond
             ;; Done!
             ((eql :complete (execution-context-status (pike-execution-context ps)))
              (setf execution-complete t))
             ;; Replanning needed!
             (t
              ;; Pay a cost for replanning: we assume here that we must pay a time penalty
              ;; equal to 1/2 of the action's duration that we got wrong. This is reasonable,
              ;; since an incorrectly executed action must be "backtracked" to fix state.
              (let* ((duration-failed-activity (+ 1 (nth (* 2 (1- i_last_revealed)) temporal_offsets)))
                     (time-failure-wait (* time-failure-factor duration-failed-activity))
                     (t-penalty-actual 0))
                (format t "Simulating time to repair world state: ~a s~%" time-failure-wait)
                (setf t-penalty-actual (measure-time (sleep time-failure-wait)))
                (incf time-penalty t-penalty-actual)
                (incf time t-penalty-actual))
              ;; Increment the replan count
              (incf replan-count)))))

    (format t "Total time (replans: ~a): ~a~%" replan-count time)

    (sb-ext:gc :full t)
    ;; Analyze the results

    (values ps replan-count time time-penalty)))


(defun run-concurrent-ir&a-timed (structure temporal_offsets &key (time-failure-factor 0.5))
  (let* (po ;; Pike options and session
         ps
         (execution-complete nil) ;; t if we've completed execute-replan loop successfully.
         intent-real ;; Real human's intent; kept secret from Pike though revealed online.
         partial-intent-revealed ;; The assignments of intent-real that have been observed / revealed online
         (i_last_revealed 0) ;; Index of variable last revealed (0 for none)
         (replan-count 0)
         (time 0)
         (time-compile 0)
         (time-penalty 0))

    ;; Sample the human's "real" / true intent. Will be kept a secret from Pike,
    ;; except that it's revealed one assignment at a time during online execution.
    (setf intent-real (sample-k-scalable-intent :structure structure))
    (format t "Real intent: ~a~%" intent-real)

    ;; Begin execute-replan loop.
    (loop while (not execution-complete) do

         (setf time-compile
               (measure-time


                ;; Create a problem instance (starting from wherever we left off)
                (generate-k-scalable-tpn-suffix :structure structure
                                                :temporal_offsets temporal_offsets
                                                :i_start (if (= 0 i_last_revealed)
                                                             1
                                                             i_last_revealed))


                ;; No need to sample an inferred intent here -- Pike will keep track of
                ;; multiple hypotheses concurrently
                (format t "i-last-revealed: ~a~%" i_last_revealed)

                ;; Create a pike session that will load in this domain / problem
                (setf po (make-instance 'pike-options
                                        :plan-filename (pike-test-file "k-scalable/plan.tpn")
                                        :domain-filename (pike-test-file "k-scalable/domain.pddl")
                                        :problem-filename (pike-test-file "k-scalable/problem.pddl")
                                        :epsilon 0.005
                                        :constraint-knowledge-base-type :piatms
                                        :debug-mode nil))
                (setf ps (create-pike-session po))

                ;; Compile the plan
                (pike-compile ps)))

       ;; Only count this time if we haven't replanned yet (first run!)
         (cond
           ((= replan-count 0)
            (format t "Not counting first-compilation time (assumed to be offline): ~a s~%" time-compile))
           (t
            (format t "Counting re-compilation time: ~a s~%" time-compile)
            (incf time time-compile)))

       ;; Add appropriate hooks
         (pike-register-hook ps :updated-choices-being-waited-on
                             #'(lambda (choices time)
                                 (declare (ignore time))
                                 (dolist (v choices)

                                   (let* ((var-name (pike::decision-variable-name v))
                                          (val (lookup-variable-in-k-scalable-intent intent-real var-name)))
                                     (push `(,var-name ,val) partial-intent-revealed)
                                     (setf i_last_revealed (parse-integer (subseq var-name 1))) ;; Read from index, like H23
                                     (pike::pike-notify-choice-made ps var-name val)))))

       ;; Simulate execution with time!
         (incf time (measure-time (run-pike-with-sim ps)))

       ;; Help prevent Emacs's color parsing from getting confused.
         (format t "~c[0m~c[0m" #\esc #\esc)


       ;; HACK!
       ;; (setf execution-complete t)


       ;; If we're done, break out of the loop
         (cond
           ;; Done!
           ((eql :complete (execution-context-status (pike-execution-context ps)))
            (setf execution-complete t))
           ;; Replanning needed!
           (t
            ;; Pay a cost for replanning: we assume here that we must pay a time penalty
            ;; equal to 1/2 of the action's duration that we got wrong. This is reasonable,
            ;; since an incorrectly executed action must be "backtracked" to fix state.
            (let* ((duration-failed-activity (+ 1 (nth (* 2 (1- i_last_revealed)) temporal_offsets)))
                   (time-failure-wait (* time-failure-factor duration-failed-activity))
                   (t-penalty-actual 0))
              (format t "Simulating time to repair world state: ~a s~%" time-failure-wait)
              (setf t-penalty-actual (measure-time (sleep time-failure-wait)))
              (incf time-penalty t-penalty-actual)
              (incf time t-penalty-actual))
            ;; Increment the replan count
            (incf replan-count))))

    (format t "Total time (replans: ~a): ~a~%" replan-count time)

    (sb-ext:gc :full t)
    ;; Analyze the results

    (values ps replan-count time time-penalty)))


(defun sample-k-scalable-intent (&key (structure `(3 3)) (observations nil))
  ;; Sample an intent
  (let (intent)
    (loop for i from 1 to (length structure) do
         (let ((var (format nil "H~a" i))
               val)
           (if (lookup-variable-in-k-scalable-intent observations var)
               (setf val (lookup-variable-in-k-scalable-intent observations var))
               (setf val (format nil "h~a" (1+ (random (nth (- i 1) structure))))))
           (push `(,var ,val) intent)))
    (reverse intent)))


(defun sample-k-scalable-intent-and-adaptation (&key (structure `(3 3)) (observations nil))
  ;; Sample an intent
  (let (scenario)
    (loop for i from 1 to (length structure) do
         (let ((var-intent (format nil "H~a" i))
               (var-adapt (format nil "R~a" i))
               val-intent
               val-adapt)

           (cond
             ;; If we've already observed this uncontrollable choice, stick to it.
             ((lookup-variable-in-k-scalable-intent observations var-intent)
              (setf val-intent (lookup-variable-in-k-scalable-intent observations var-intent))
              (setf val-adapt (format nil "r~a" (subseq val-intent 1))))

             ;; Otherwise, pick one at random.
             (t
              (let ((val-index (1+ (random (nth (- i 1) structure)))))
                (setf val-intent (format nil "h~a" val-index))
                (setf val-adapt (format nil "r~a" val-index)))))

           (push `(,var-intent ,val-intent) scenario)
           (push `(,var-adapt ,val-adapt) scenario)))
    (reverse scenario)))




(defun lookup-variable-in-k-scalable-intent (intent var)
  (second (find var intent :test #'equal :key #'first)))


(defun generate-k-scalable-tpn-suffix (&key (structure `(3 3)) (temporal_offsets `(0 0 0 0)) (i_start 1))
  ;; Change to the appropriate directory
  (let ((envs (sb-ext:posix-environ))
        p)
    ;; Change directories
    (sb-posix:chdir (asdf:system-relative-pathname :pike "test-cases/k-scalable/"))
    ;; Set the Python path so that CLARK python code (For RMPyL) can be found
    ;; to generate the TPN
    (push "PYTHONPATH=/home/steve/clark/" envs)
    ;; Call it
    (setf p (sb-ext:run-program #p"/home/steve/Desktop/random_tpn/k-scalable.py" ;;(asdf:system-relative-pathname :pike "test-cases/k-scalable/scripts/k-scalable.py")

                                (list
                                 "--structure" (format nil "~{~a~^,~}" structure)
                                 "--temporal_offsets" (format nil "~{~a~^,~}" temporal_offsets)
                                 "--i_start" (format nil "~a" i_start))
                                :environment envs))
    (unless (= 0 (sb-ext:process-exit-code p))
      (error "k-scalable.py threw an error!"))))



(defun random-shuffle (sequence)
  "Credit to: https://www.pvk.ca/Blog/Lisp/trivial_uniform_shuffling.html"
  (map-into sequence #'car
            (sort (map 'vector (lambda (x)
                                 (cons x (random 1d0)))
                       sequence)
                  #'< :key #'cdr)))


(defclass intent-sampler()
  ((bases
    :initform (list 2 3 5)
    :initarg :bases
    :type list)
   (range
    :initform (list 0 1 2 3 4 5)
    :initarg :range
    :type list)
   (d
    :initform nil
    :type list)))

(defmethod initialize-intent-sampler ((iss intent-sampler))
  (with-slots (bases range d) iss
    (setf d nil)
    (let (ranges exponents)
      (dotimes (i (length bases))
        (push (copy-seq range) ranges))
      (setf exponents (pike::cartesian-product ranges))
      (dolist (exponent exponents)
        (push (list (apply #'* (mapcar #'(lambda (b p) (expt b p)) bases exponent))
                    exponent)
              d)))))


(defmethod intent-sampler-closest-factorization ((iss intent-sampler) k)
  (with-slots (d) iss
    (second (alexandria:extremum d #'< :key #'(lambda (m)
                                                (expt (- (first m) k) 2))))))



(defmethod intent-sampler-unroll-factorization ((iss intent-sampler) factorization)
  (with-slots (bases) iss
    (let (dimensions)
      (dotimes (i (length bases))
        (dotimes (j (nth i factorization))
          (push (nth i bases) dimensions)))
      dimensions)))


(defun logarithmic-sample (k-min k-max)
  (let* ((lk-min (log k-min))
         (lk-max (log k-max))
         (lk (+ lk-min
                (* (random 1.0)
                   (- lk-max lk-min)))))
    (exp lk)))
