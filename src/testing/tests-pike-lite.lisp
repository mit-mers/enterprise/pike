;;;; Copyright (c) 2011-2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)






(defun test-pike-lite-1 ()
  (let* (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "blocksworld/blocksworld-1.rmpl.fixed.tpn")
                            :domain-filename (pike-test-file "blocksworld/blocksworld-domain-temporal.pddl")
                            :problem-filename (pike-test-file "blocksworld/blocksworld-1-problem.pddl")
                            :epsilon 0.005
                            :debug-mode T))
    ;;(setf ps (test-pike-with-sim po))

    (setf ps (create-pike-session-lite po))

    (handler-case (pike-compile ps)
      (condition (e) (format t "Error: ~a~%" e)))

    ps))


(defun test-pike-compile-lite-with-file (plan-filename domain-filename problem-filename)
  (let* (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename plan-filename
                            :domain-filename domain-filename
                            :problem-filename problem-filename
                            :epsilon 0.005
                            :make-uncontrollable-choices? nil ;; HACK!
                            :debug-mode T))
    ;;(setf ps (test-pike-with-sim po))

    (setf ps (create-pike-session-lite po))

    (handler-case (compile-plan-lite ps)
      (condition (e) (format t "Error: ~a~%" e)))

    ps))
