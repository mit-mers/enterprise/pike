(in-package #:cl-user)

(uiop:define-package #:pike/test
  (:use :cl :cl-user :pike)
  (:import-from :fiveam
                :is
                :is-true
                :is-false
                :signals
                :test
                :run
                :run!
                :explain!
                :def-suite
                :in-suite)
  (:export :profiler-db
           :profiler-engage
           :profiler-disengage
           :profiler-disengage-all
           :is-profiled?
           :profiler-get-raw-data
           :profiler-get-count
           :profiler-get-average
           :profiler-get-stats
           :compute-stats
           :profiler-clear-data-for-function
           :profiler-clear-all-data))
