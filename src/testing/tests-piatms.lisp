;;;; Copyright (c) 2016 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike/test)

(def-suite :tests-piatms
    :description "Tests for the prime-implicant generating piATMS."
    :in :tests-pike)

(in-suite :tests-piatms)

(defmethod setup-piatms-1 (&key (constraint-knowledge-base-type :piatms))
  (let (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "causal-links/pddl/thing-4.rmpl.tpn.out")
                            :domain-filename (pike-test-file "causal-links/pddl/simple-domain.pddl")
                            :problem-filename (pike-test-file "causal-links/pddl/simple-problem.pddl")
                            :debug-mode T
                            :constraint-knowledge-base-type constraint-knowledge-base-type))
    (setf ps (create-pike-session po))
    ps))
