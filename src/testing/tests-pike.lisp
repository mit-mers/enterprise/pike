;;;; Copyright (c) 2011-2018 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike/test)



(defun test-pike-13 ()
  (let* (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "blocksworld/blocksworld-1.rmpl.fixed.tpn")
                            :domain-filename (pike-test-file "blocksworld/blocksworld-domain-temporal.pddl")
                            :problem-filename (pike-test-file "blocksworld/blocksworld-1-problem.pddl")
                            :epsilon 0.005
                            :debug-mode T))
    ;;(setf ps (test-pike-with-sim po))

    (setf ps (create-pike-session po))

    (handler-case (pike-compile ps)
      (condition (e) (format t "Error: ~a~%" e)))

    ps))


(defun test-pike-compile-with-file (plan-filename domain-filename problem-filename &key
                                                                                     (constraint-knowledge-base-type :bdd)
                                                                                     (bn nil)
                                                                                     (cc 0.95)
                                                                                     (make-uncontrollable-choices t))
  (let* (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename plan-filename
                            :domain-filename domain-filename
                            :problem-filename problem-filename
                            :probability-distribution bn
                            :chance-constraint cc
                            :epsilon 0.005
                            :constraint-knowledge-base-type constraint-knowledge-base-type
                            :debug-mode T
                            :make-uncontrollable-choices? make-uncontrollable-choices))
    ;;(setf ps (test-pike-with-sim po))

    (setf ps (create-pike-session po))

    ;; (handler-case (pike-compile ps)
    ;;   (condition (e) (format t "Error: ~a~%" e)))
    ;;(pike-compile ps)

    ps))






(defmethod setup-wing-assembly (&key
                                  (constraint-knowledge-base-type :piatms)
                                  (encode-causal-support-variables t)
                                  (memoize-constraint-calls t)
                                  (probability-distribution nil))
  (let* (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "wing-assembly/Boeing2014-fixed.tpn.out.out")
                            :domain-filename (pike-test-file "wing-assembly/domain.pddl")
                            :problem-filename (pike-test-file "wing-assembly/problem.pddl")
                            :epsilon 0.005
                            :make-uncontrollable-choices? T
                            :memoize-constraint-calls memoize-constraint-calls
                            :constraint-knowledge-base-type constraint-knowledge-base-type
                            :probability-distribution probability-distribution
                            :encode-causal-support-variables encode-causal-support-variables
                            :debug-mode T))
    (setf ps (create-pike-session po))
    ps))



(defmethod setup-kitchen-simple (&key
                                   (constraint-knowledge-base-type :piatms)
                                   (memoize-constraint-calls t)
                                   (encode-causal-support-variables t))
  (let* (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "kitchen/kitchen-simple/kitchen-simple.tpn")
                            :domain-filename (pike-test-file "kitchen/kitchen-simple/domain.pddl")
                            :problem-filename(pike-test-file "kitchen/kitchen-simple/problem.pddl")
                            :probability-distribution (pike-test-file "kitchen/kitchen-simple/kitchen.xmlbif")
                            :epsilon 0.005
                            :make-uncontrollable-choices? T ;; T
                            :memoize-constraint-calls memoize-constraint-calls
                            :constraint-knowledge-base-type constraint-knowledge-base-type

                            :encode-causal-support-variables encode-causal-support-variables
                            :debug-mode T))

    (setf ps (create-pike-session po))
   ps))



(defun test-pike-kitchen-simple (&key (constraint-knowledge-base-type :piatms))
  (let ((ps (setup-kitchen-simple :constraint-knowledge-base-type constraint-knowledge-base-type)))
    ;; (pike-compile ps)
    (handler-case (pike-compile ps)
      (condition (e) (format t "Error: ~a~%" e)))

    ps))







(defun benchmark-patms-1 ()
  (let* ((ps (setup-kitchen-simple :constraint-knowledge-base-type :patms
                                   :encode-causal-support-variables nil
                                   :memoize-constraint-calls nil)))
    ;; Clear any previous benchmarking results first
    (pike::clear-patms-benchmarking)

    ;; Compile!
    (pike-compile ps)

    ;; Output results
    (pike::output-patms-benchmarking-to-file (pike::constraint-knowledge-base-atms-atms (pike::pike-constraint-knowledge-base ps)) "patms_trace.csv")

    ;; Output ATMS graph
    ;;(pike::atms-to-json-file ps "patms.json")

    ps))


(defun benchmark-patms-2 ()
  (let* ((ps (setup-wing-assembly :constraint-knowledge-base-type :patms
                                  :encode-causal-support-variables nil
                                  :memoize-constraint-calls nil )))
                                  ;; :probability-description `(("x_A1" "mug" 0.9)
                                  ;;                            ("x_A1" "glass" 0.1)
                                  ;;                            ("x_A2" "coffee" 0.9)
                                  ;;                            ("x_A2" "juice" 0.1)
                                  ;;                            ("x_A3" "cereal" 0.9)
                                  ;;                            ("x_A3" "bagel" 0.1)
                                  ;;                            ("x_A4" "cereal" 0.9)
                                  ;;                            ("x_A4" "bagel" 0.1))
                                  ;; :probability-description `(("x_human_do_what_1" "Pickup Cleco" 0.3)
                                  ;;                            ("x_human_do_what_1" "Pickup Gun" 0.1)
                                  ;;                            ("x_human_do_what_1" "Pickup Both" 0.1)

                                  ;;                            ("x_human_do_what_2" "Pickup Cleco" 0.1)
                                  ;;                            ("x_human_do_what_2" "Pickup Nothing" 0.9))


    ;; Clear any previous benchmarking results first
    (pike::clear-patms-benchmarking)

    ;; Compile!
    (pike-compile ps)

    ;; Output results
    (pike::output-patms-benchmarking-to-file (pike::constraint-knowledge-base-atms-atms (pike::pike-constraint-knowledge-base ps)) "patms_trace.csv")

    ;; Output ATMS graph
    ;;(pike::atms-to-json-file ps "patms.json")

    ps))


(defun test-wmc-formulation-1 ()
  (let ((latent-var (make-instance 'pike::decision-variable :name "G" :domain `("good" "faulty")))
        (dep-vars (list (make-instance 'pike::decision-variable :name "d1" :domain `("dropped" "held"))
                        (make-instance 'pike::decision-variable :name "d2" :domain `("dropped" "held")) ))
                        ;;(make-instance 'pike::decision-variable :name "d3" :domain `("dropped" "held"))
                        ;;(make-instance 'pike::decision-variable :name "d4" :domain `("dropped" "held"))
        theta-vars constraints sat)

    ;; Make theta variables and constraints
    (dolist (dv dep-vars)
      (let ((theta (make-instance 'pike::decision-variable
                                  :name (format nil "theta_~a|~a"
                                                (pike::decision-variable-name dv)
                                                (pike::decision-variable-name latent-var))
                                  :domain (let (cross-domain)
                                            (dolist (v1 (pike::decision-variable-values dv))
                                              (dolist (v2 (pike::decision-variable-values latent-var))
                                                (push (format nil "~a-~a" v1 v2)
                                                      cross-domain)))
                                            cross-domain))))
        (push theta theta-vars)

        ;; Add all constraints
        (dolist (v1 (pike::decision-variable-values dv))
          (dolist (v2 (pike::decision-variable-values latent-var))
            (push (pike::make-constraint
                   `(and (=> (= ,theta ,(format nil "~a-~a" v1 v2))
                             (and (= ,dv ,v1)
                                  (= ,latent-var ,v2)))
                         (=> (and (= ,dv ,v1)
                                  (= ,latent-var ,v2))
                             (= ,theta ,(format nil "~a-~a" v1 v2)))))
                  constraints)))))

    ;; One more theta for the latent var
    (let ((theta (make-instance 'pike::decision-variable
                                :name (format nil "theta_~a" (pike::decision-variable-name latent-var))
                                :domain (copy-seq (pike::decision-variable-values latent-var)))))
      (push theta theta-vars)
      (dolist (v (pike::decision-variable-values latent-var))
        (push (pike::make-constraint
               `(and (=> (= ,theta ,v)
                         (= ,latent-var ,v))
                     (=> (= ,latent-var ,v)
                         (= ,theta ,v))))
              constraints)))

    ;; Encode with SAT
    (setf sat (pike::encode-constraints-as-sat-cnf constraints (concatenate 'list (list latent-var) dep-vars theta-vars)))
    (pike::write-dimacs-cnf-file sat "wmc.cnf")
    (pike::write-dimacs-cnf-variable-mapping-file sat "wmc-var-mapping.txt")


    (values sat constraints)))




(defmethod setup-faulty-gripper-1 (&key
                                     (constraint-knowledge-base-type :sat)
                                     (encode-causal-support-variables t)
                                     (probability-description nil))
  (let* (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "faulty-gripper/faulty_gripper_2_processed.tpn")
                            :domain-filename (pike-test-file "faulty-gripper/domain.pddl")
                            :problem-filename (pike-test-file "faulty-gripper/problem.pddl")
                            :epsilon 0.005
                            :make-uncontrollable-choices? T
                            :constraint-knowledge-base-type constraint-knowledge-base-type
                            :probability-distribution probability-description
                            :encode-causal-support-variables encode-causal-support-variables
                            :debug-mode T))

    (setf ps (create-pike-session po))
    ps))

(defmethod setup-faulty-gripper-2 (&key
                                     (constraint-knowledge-base-type :sat)
                                     (encode-causal-support-variables t)
                                     (probability-description nil))
  (let* (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "faulty-gripper/faulty_gripper_4.tpn")
                            :domain-filename (pike-test-file "faulty-gripper/domain.pddl")
                            :problem-filename (pike-test-file "faulty-gripper/problem.pddl")
                            :epsilon 0.005
                            :make-uncontrollable-choices? T
                            :constraint-knowledge-base-type constraint-knowledge-base-type
                            :probability-distribution probability-description
                            :encode-causal-support-variables encode-causal-support-variables
                            :debug-mode T))

    (setf ps (create-pike-session po))
    ps))


(defmethod setup-faulty-gripper-3 (&key
                                     (constraint-knowledge-base-type :cc-wmc-bdd)
                                     (encode-causal-support-variables t))
  (let* (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "faulty-gripper/faulty_gripper_4.tpn")
                            :domain-filename (pike-test-file "faulty-gripper/domain.pddl")
                            :problem-filename (pike-test-file "faulty-gripper/problem.pddl")
                            :probability-distribution (pike-test-file "faulty-gripper/bn_gripper_4.xmlbif")
                            :epsilon 0.005
                            :make-uncontrollable-choices? T
                            :constraint-knowledge-base-type constraint-knowledge-base-type
                            :encode-causal-support-variables encode-causal-support-variables
                            :memoize-constraint-calls nil ;; TODO Need to fix this
                            :debug-mode T))

    (setf ps (create-pike-session po))
    ps))
