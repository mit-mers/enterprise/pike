;;;; Copyright (c) 2017 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike/test)

(def-suite :tests-constraints-to-tseitin-sat
    :description "Tests converting constraints to CNF via Tseitin transformation"
    :in :tests-pike)

(in-suite :tests-constraints-to-tseitin-sat)


(defun setup-constraints-tseitin-1 ()
  (let* ((ss (make-state-space-bits))
         (x (make-instance 'decision-variable
                           :name "x"
                           :domain (list 1 2 3)))
         (y (make-instance 'decision-variable
                           :name "y"
                           :domain (list 1 2 3)))
         (z (make-instance 'decision-variable
                           :name "z"
                           :domain (list 1 2 3))))

    (add-variable! ss x)
    (add-variable! ss y)
    (add-variable! ss z)

    ;; Removing False's
    (let* ((c1 (make-constraint `(and (= ,x 1)
                                      (= ,y 1)))))
      (values ss (list c1)))))


(defun testing-1 ()
  (let (ss constraints sat)
    (multiple-value-setq (ss constraints) (setup-constraints-tseitin-1))

    (setf sat (encode-constraints-as-sat-cnf constraints (pike::state-space-bits-variables ss)))

    (format t "CNF: ~%")
    (write-dimacs-cnf-to-stream sat t)

    (format t "~%~%Variable mapping:~%")
    (write-dimacs-cnf-variable-mapping-to-stream sat t)

    (values sat ss)))
