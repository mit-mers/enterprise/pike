;;;; Copyright (c) 2011-2018 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike/test)

(in-package #:pike/test)

(def-suite :tests-cc-riker
    :description "Various other tests for Riker!"
    :in :tests-pike)

(in-suite :tests-cc-riker)


(defun test-riker-simulate-with-file (plan-filename
                                      domain-filename
                                      problem-filename
                                      bn
                                      &key
                                        (cc 0.95)
                                        (constraint-knowledge-base-type :cc-wmc-bdd))
  (let* (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename plan-filename
                            :domain-filename domain-filename
                            :problem-filename problem-filename
                            :probability-distribution bn
                            :chance-constraint cc
                            :epsilon 0.005
                            :constraint-knowledge-base-type constraint-knowledge-base-type
                            :debug-mode nil))

    (setf ps (create-pike-session po))

    ;; Compile
    (pike-compile ps)

    (let (intent-sample
          activities-dispatched)
      ;; Sample the true intent from the Bayesian network
      (setf intent-sample (pike::sample-from-bayesian-network (pike::pike-bayesian-network ps)))

      ;; Add appropriate hooks
      (pike-register-hook ps :updated-choices-being-waited-on
                          #'(lambda (choices time)
                              (declare (ignore time))
                              (dolist (v choices)
                                (let ((a (gethash v intent-sample)))
                                  (pike::pike-notify-choice-made ps
                                                                 (pike::decision-variable-name v)
                                                                 (pike::assignment-value a))))))

      (pike-register-hook ps :activity-dispatch
                          #'(lambda (activity lb ub time)
                              (declare (ignore lb ub time))
                              (push (pike::action-dispatch (pike::activity-action activity))
                                    activities-dispatched)))

      ;; If Pike asks a question (it's in probablisitic deadlock),
      ;; the following ends execution.
      (pike-register-hook ps :ask-question
                          #'(lambda (variable-question time)
                              (declare (ignore variable-question time))
                              (pike::preempt-execution ps)))


      ;; Execute
      (run-pike-with-sim-dt ps :dt 0.01)
      (setf activities-dispatched (reverse activities-dispatched))

      (values ps activities-dispatched))))



(defun setup-simple-n (N &key (constraint-knowledge-base-type :cc-wmc-bdd) (memoize-constraint-calls nil))
  (let (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file (format nil "choice-order/simple-n/controllable-first-~a.tpn" N))
                            :domain-filename (pike-test-file "choice-order/simple-n/domain.pddl")
                            :problem-filename (pike-test-file (format nil "choice-order/simple-n/problem-~a.pddl" N))
                            :probability-distribution (pike-test-file (format nil "choice-order/simple-n/bn-~a.xmlbif" N))
                            :chance-constraint 0.9
                            :debug-mode T
                            :memoize-constraint-calls memoize-constraint-calls ;; nil for testing
                            :constraint-knowledge-base-type constraint-knowledge-base-type))
    (setf ps (create-pike-session po))
    ps))



(test riker-simple-n
  "Check with the simple-n domain in my thesis. Checks probability computation, and also makes sure that the
   policy BDD sizes match what is expected (and grow linearly). Note that if we change our constraint encoding,
   those relevant checks below will expectedely break!!"
  (dolist (cc-type `(:cc-wmc-bdd))
    ;; N = 1
    (let ((ps (setup-simple-N 1 :constraint-knowledge-base-type cc-type)))
      ;; Make sure it compiles properly
      (is-true (pike-compile ps))

      ;; Get some relevant variables
      (let* ((ckb (pike-constraint-knowledge-base ps))
             (y1 (find-variable ps "y1")))

        ;; Test probability computations!
        (is  (= (/ 9 10)
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list )
                 :conflicts (list ))))

        (is  (= 1
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment y1 "c1"))
                 :conflicts (list ))))

        (is (= 11
               (pike::bdd-count-children (pike::node-correct ckb))))))

    ;; N = 4
    (let ((ps (setup-simple-N 4 :constraint-knowledge-base-type cc-type)))
      ;; Make sure it compiles properly
      (is-true (pike-compile ps))

      ;; Get some relevant variables
      (let* ((ckb (pike-constraint-knowledge-base ps))
             (y2 (find-variable ps "y2")))

        ;; Test probability computations!
        (is  (= (expt (/ 9 10) 4)
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list )
                 :conflicts (list ))))

        (is  (= (expt (/ 9 10) 3)
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment y2 "c1"))
                 :conflicts (list ))))

        (is (= 38
               (pike::bdd-count-children (pike::node-correct ckb))))))

    ;; N = 6
    (let ((ps (setup-simple-N 6 :constraint-knowledge-base-type cc-type)))
      ;; Make sure it compiles properly
      (is-true (pike-compile ps))

      ;; Get some relevant variables
      (let* ((ckb (pike-constraint-knowledge-base ps))
             (y2 (find-variable ps "y2")))

        ;; Test probability computations!
        (is  (= (expt (/ 9 10) 6)
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list )
                 :conflicts (list ))))

        (is  (= (expt (/ 9 10) 5)
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment y2 "c1"))
                 :conflicts (list ))))

        (is (= 56
               (pike::bdd-count-children (pike::node-correct ckb))))))))





;; TODO Below should be cleaned up! Probably no longer needed?
(defun test-unobserved-choices-1()
  "See example from paper / lab notebook / phone from 10/11/2018"
  (let* ((ss (make-state-space-bits))

         ;; pTPN variables
         (H1 (make-instance 'decision-variable
                            :name "H1"
                            :domain (list "c1" "c2")))
         (H2 (make-instance 'decision-variable
                            :name "H2"
                            :domain (list "c1" "c2")))
         (R1 (make-instance 'decision-variable
                            :name "R1"
                            :domain (list "c1" "c2")))
         (R2 (make-instance 'decision-variable
                            :name "R2"
                            :domain (list "c1" "c2")))

         ;; Bayesian network variables
         (θ1 (make-instance 'decision-variable
                            :name "θ1"
                            :domain (list "1" "0")))
         (θ2 (make-instance 'decision-variable
                            :name "θ2"
                            :domain (list "1" "0")))
         (θ3 (make-instance 'decision-variable
                            :name "θ3"
                            :domain (list "1" "0")))
         (θ4 (make-instance 'decision-variable
                            :name "θ4"
                            :domain (list "1" "0")))
         (θ5 (make-instance 'decision-variable
                            :name "θ5"
                            :domain (list "1" "0")))
         (θ6 (make-instance 'decision-variable
                            :name "θ6"
                            :domain (list "1" "0")))

         bm bde n weights bn-variables
         constraints)

    ;; Set up the ss
    (add-variable! ss H1) (add-variable! ss H2) (add-variable! ss R1) (add-variable! ss R2)
    (add-variable! ss θ1) (add-variable! ss θ2) (add-variable! ss θ3) (add-variable! ss θ4) (add-variable! ss θ5) (add-variable! ss θ6)

    ;; Bayesian network constraints (original)
    (push (make-constraint `(and (<=> (= ,θ1 "1")
                                      (= ,H2 "c1"))

                                 (<=> (= ,θ2 "1")
                                      (= ,H2 "c2"))

                                 (<=> (= ,θ3 "1")
                                      (and (= ,H1 "c1")
                                           (= ,H2 "c1")))

                                 (<=> (= ,θ4 "1")
                                      (and (= ,H1 "c1")
                                           (= ,H2 "c2")))

                                 (<=> (= ,θ5 "1")
                                      (and (= ,H1 "c2")
                                           (= ,H2 "c1")))

                                 (<=> (= ,θ6 "1")
                                      (and (= ,H1 "c2")
                                           (= ,H2 "c2")))

                                 ;; Encode 0 probability determinism
                                 (not (and (= ,H1 "c1")
                                           (= ,H2 "c2")))

                                 (not (and (= ,H1 "c2")
                                           (= ,H2 "c1")))))
          constraints)

    ;; Causal link constraints
    (push (make-constraint `(and (=> (= ,H1 "c1")
                                     (= ,R2 "c1"))

                                 (=> (= ,H1 "c2")
                                     (= ,R2 "c2"))))
          constraints)

    ;; Optional: even if we choose R1 = c2 (so that H2 shouldn't be observed!),
    ;; same result with this encoding
    (push (make-constraint `(= ,R1 "c2"))
          constraints)

    ;; Set weights
    (setf weights (make-hash-table :test #'pike::eql-list-2))
    (setf (gethash `(,(assignment θ1 "1") t) weights) (/ 1 2))
    (setf (gethash `(,(assignment θ2 "1") t) weights) (/ 1 2))
    (setf (gethash `(,(assignment θ3 "1") t) weights) 1)
    (setf (gethash `(,(assignment θ4 "1") t) weights) 0)
    (setf (gethash `(,(assignment θ5 "1") t) weights) 0)
    (setf (gethash `(,(assignment θ6 "1") t) weights) 1)

    (setf bn-variables (list (assignment H1 "c1") (assignment H1 "c2")
                             (assignment H2 "c1") (assignment H2 "c2")
                             (assignment θ1 "1") (assignment θ1 "0")
                             (assignment θ2 "1") (assignment θ2 "0")
                             (assignment θ3 "1") (assignment θ3 "0")
                             (assignment θ4 "1") (assignment θ4 "0")
                             (assignment θ5 "1") (assignment θ5 "0")
                             (assignment θ6 "1") (assignment θ6 "0")))

    ;; Start a BDD manager
    (setf bm (pike::create-bdd-manager))

    ;; Create a BDD encoder (which connects a state space and BDD manager)
    (setf bde (pike::make-bdd-encoder :ss ss
                                      :bm bm))

    ;; Set up the encoder
    (pike::bdd-encoder-initialize bde
                                  `(,R1 ,H2 ,R2 ,H1 ,θ1 ,θ2 ,θ3 ,θ4 ,θ5 ,θ6)
                                  :use-binaries nil ;; Simpler for testing
                                  :variable-order-heuristic :default ;; Use pTPN order above
                                  :constraints constraints)

    ;; Set sum product / projection variables
    (pike::bdd-set-Σ∏-variables bm bn-variables)
    (pike::bdd-set-projection-variables bm bn-variables)
    (pike::bdd-set-weights bm weights)

    (setf n (pike::bdd-encoder-get-variable-constraints-bdd bde))
    (dolist (c constraints)
      (setf n (pike::bdd-and bm
                             n
                             (pike::constraint->bdd bde c))))

    (format t "Number of BDD nodes in n: ~a~%" (pike::bdd-count-children n))
    (format t "Max Sum Product: ~a~%" (pike::bdd-maxσ∏ bm n))
    (values bde bm)))



(defun test-unobserved-choices-2()
  "See example from paper / lab notebook / phone from 10/11/2018."
  (let* ((ss (make-state-space-bits))

         ;; pTPN variables
         (H1 (make-instance 'decision-variable
                            :name "H1"
                            :domain (list "c1" "c2")))
         (H2 (make-instance 'decision-variable
                            :name "H2"
                            :domain (list "c1" "c2" "unobs")))
         (R1 (make-instance 'decision-variable
                            :name "R1"
                            :domain (list "c1" "c2")))
         (R2 (make-instance 'decision-variable
                            :name "R2"
                            :domain (list "c1" "c2")))

         ;; Bayesian network variables
         (θ1 (make-instance 'decision-variable
                            :name "θ1"
                            :domain (list "1" "0")))
         (θ2 (make-instance 'decision-variable
                            :name "θ2"
                            :domain (list "1" "0")))
         (θ3 (make-instance 'decision-variable
                            :name "θ3"
                            :domain (list "1" "0")))
         (θ4 (make-instance 'decision-variable
                            :name "θ4"
                            :domain (list "1" "0")))
         (θ5 (make-instance 'decision-variable
                            :name "θ5"
                            :domain (list "1" "0")))
         (θ6 (make-instance 'decision-variable
                            :name "θ6"
                            :domain (list "1" "0")))

         bm bde n weights bn-variables
         constraints)

    ;; Set up the ss
    (add-variable! ss H1) (add-variable! ss H2) (add-variable! ss R1) (add-variable! ss R2)
    (add-variable! ss θ1) (add-variable! ss θ2) (add-variable! ss θ3) (add-variable! ss θ4) (add-variable! ss θ5) (add-variable! ss θ6)

    ;; Bayesian network constraints (original)
    ;; (push (make-constraint `(and (<=> (= ,θ1 "1")
    ;;                                   (or (= ,H2 "c1") (= ,H2 "unobs")))

    ;;                              (<=> (= ,θ2 "1")
    ;;                                   (or (= ,H2 "c2") (= ,H2 "unobs")))

    ;;                              (<=> (= ,θ3 "1")
    ;;                                   (and (= ,H1 "c1")
    ;;                                        (or (= ,H2 "c1") (= ,H2 "unobs"))))

    ;;                              (<=> (= ,θ4 "1")
    ;;                                   (and (= ,H1 "c1")
    ;;                                        (or (= ,H2 "c2") (= ,H2 "unobs"))))

    ;;                              (<=> (= ,θ5 "1")
    ;;                                   (and (= ,H1 "c2")
    ;;                                        (or (= ,H2 "c1") (= ,H2 "unobs"))))

    ;;                              (<=> (= ,θ6 "1")
    ;;                                   (and (= ,H1 "c2")
    ;;                                        (or (= ,H2 "c2") (= ,H2 "unobs"))))

    ;;                              ;; Encode 0 probability determinism
    ;;                              ;; (not (and (= ,H1 "c1")
    ;;                              ;;           (= ,H2 "c2")))

    ;;                              ;; (not (and (= ,H1 "c2")
    ;;                              ;;           (= ,H2 "c1")))

    ;;                              ))
    ;;       constraints)


    ;; Manual override on Bayesian network constraints... specifying 8 models...
    (push (make-constraint `(or
                             (and (= ,θ1 "1" )  (= ,θ2 "0" )  (= ,θ3 "1" )  (= ,θ4 "0" )  (= ,θ5 "0" )  (= ,θ6 "0" )  (= ,H1 "c1" )  (= ,H2 "c1" ))
                             (and (= ,θ1 "1" )  (= ,θ2 "0" )  (= ,θ3 "0" )  (= ,θ4 "0" )  (= ,θ5 "1" )  (= ,θ6 "0" )  (= ,H1 "c2" )  (= ,H2 "c1" ))
                             (and (= ,θ1 "0" )  (= ,θ2 "1" )  (= ,θ3 "0" )  (= ,θ4 "1" )  (= ,θ5 "0" )  (= ,θ6 "0" )  (= ,H1 "c1" )  (= ,H2 "c2" ))
                             (and (= ,θ1 "0" )  (= ,θ2 "1" )  (= ,θ3 "0" )  (= ,θ4 "0" )  (= ,θ5 "0" )  (= ,θ6 "1" )  (= ,H1 "c2" )  (= ,H2 "c2" ))
                             (and (= ,θ1 "1" )  (= ,θ2 "0" )  (= ,θ3 "1" )  (= ,θ4 "0" )  (= ,θ5 "0" )  (= ,θ6 "0" )  (= ,H1 "c1" )  (= ,H2 "unobs" ))
                             (and (= ,θ1 "0" )  (= ,θ2 "1" )  (= ,θ3 "0" )  (= ,θ4 "1" )  (= ,θ5 "0" )  (= ,θ6 "0" )  (= ,H1 "c1" )  (= ,H2 "unobs" ))
                             (and (= ,θ1 "1" )  (= ,θ2 "0" )  (= ,θ3 "0" )  (= ,θ4 "0" )  (= ,θ5 "1" )  (= ,θ6 "0" )  (= ,H1 "c2" )  (= ,H2 "unobs" ))
                             (and (= ,θ1 "0" )  (= ,θ2 "1" )  (= ,θ3 "0" )  (= ,θ4 "0" )  (= ,θ5 "0" )  (= ,θ6 "1" )  (= ,H1 "c2" )  (= ,H2 "unobs" ))))
          constraints)


    ;; ;; Causal link constraints
    (push (make-constraint `(and (=> (= ,H1 "c1")
                                     (= ,R2 "c1"))

                                 (=> (= ,H1 "c2")
                                     (= ,R2 "c2"))))
          constraints)

    ;; Observation constraint
    (push (make-constraint `(<=> (not (= ,R1 "c1"))
                                 (= ,H2 "unobs")))
          constraints)

    ;; ;; Optional: even if we choose R1 = c2 (so that H2 shouldn't be observed!),
    ;; ;; same result with this encoding
    (push (make-constraint `(= ,R1 "c2"))
          constraints)

    ;; Testing
    ;; (push (make-constraint `(and (= ,H2 "unobs") (=, H1 "c1")))
    ;;       constraints)

    ;; Set weights
    (setf weights (make-hash-table :test #'pike::eql-list-2))
    (setf (gethash `(,(assignment θ1 "1") t) weights) (/ 1 2))
    (setf (gethash `(,(assignment θ2 "1") t) weights) (/ 1 2))
    (setf (gethash `(,(assignment θ3 "1") t) weights) 1)
    (setf (gethash `(,(assignment θ4 "1") t) weights) 0)
    (setf (gethash `(,(assignment θ5 "1") t) weights) 0)
    (setf (gethash `(,(assignment θ6 "1") t) weights) 1)

    (setf bn-variables (list (assignment H1 "c1") (assignment H1 "c2")
                             (assignment H2 "c1") (assignment H2 "c2") (assignment H2 "unobs")
                             (assignment θ1 "1") (assignment θ1 "0")
                             (assignment θ2 "1") (assignment θ2 "0")
                             (assignment θ3 "1") (assignment θ3 "0")
                             (assignment θ4 "1") (assignment θ4 "0")
                             (assignment θ5 "1") (assignment θ5 "0")
                             (assignment θ6 "1") (assignment θ6 "0")))

    ;; Start a BDD manager
    (setf bm (pike::create-bdd-manager))

    ;; Create a BDD encoder (which connects a state space and BDD manager)
    (setf bde (pike::make-bdd-encoder :ss ss
                                      :bm bm))

    ;; Set up the encoder
    (pike::bdd-encoder-initialize bde
                                  `(,R1 ,H2 ,R2 ,H1 ,θ1 ,θ2 ,θ3 ,θ4 ,θ5 ,θ6)
                                  :use-binaries nil ;; Simpler for testing
                                  :variable-order-heuristic :default ;; Use pTPN order above
                                  :constraints constraints)

    ;; Set sum product / projection variables
    (pike::bdd-set-Σ∏-variables bm bn-variables)
    (pike::bdd-set-projection-variables bm bn-variables)
    (pike::bdd-set-weights bm weights)

    (setf n (pike::bdd-encoder-get-variable-constraints-bdd bde))
    (dolist (c constraints)
      (setf n (pike::bdd-and bm
                             n
                             (pike::constraint->bdd bde c))))

    (format t "Number of BDD nodes in n: ~a~%" (pike::bdd-count-children n))
    (format t "Max Sum Product: ~a~%" (pike::bdd-maxσ∏ bm n))
    (values bde bm)))


(defun test-unobserved-choices-3()
  "See example from paper / lab notebook / phone from 10/11/2018. This works, kind of -- but requires the use of a
counting variable, which I don't know how to efficiently encode. The next option (phantom BNs) seems to work better."
  (let* ((ss (make-state-space-bits))

         ;; pTPN variables
         (H1 (make-instance 'decision-variable
                            :name "H1"
                            :domain (list "c1" "c2")))
         (H2 (make-instance 'decision-variable
                            :name "H2"
                            :domain (list "c1" "c2")))
         (R1 (make-instance 'decision-variable
                            :name "R1"
                            :domain (list "c1" "c2")))
         (R2 (make-instance 'decision-variable
                            :name "R2"
                            :domain (list "c1" "c2")))

         ;; Observation variable
         (obs (make-instance 'decision-variable
                             :name "obs"
                             :domain (list "1" "0")))

         ;; Testing: counter variable??
         (ct (make-instance 'decision-variable
                            :name "ct"
                            :domain (list "1" "0")))

         ;; Bayesian network variables
         (θ1 (make-instance 'decision-variable
                            :name "θ1"
                            :domain (list "1" "0")))
         (θ2 (make-instance 'decision-variable
                            :name "θ2"
                            :domain (list "1" "0")))
         (θ3 (make-instance 'decision-variable
                            :name "θ3"
                            :domain (list "1" "0")))
         (θ4 (make-instance 'decision-variable
                            :name "θ4"
                            :domain (list "1" "0")))
         (θ5 (make-instance 'decision-variable
                            :name "θ5"
                            :domain (list "1" "0")))
         (θ6 (make-instance 'decision-variable
                            :name "θ6"
                            :domain (list "1" "0")))

         bm bde n weights bn-variables
         constraints)

    ;; Set up the ss
    (add-variable! ss H1) (add-variable! ss H2) (add-variable! ss R1) (add-variable! ss R2)
    (add-variable! ss obs) (add-variable! ss ct)
    (add-variable! ss θ1) (add-variable! ss θ2) (add-variable! ss θ3) (add-variable! ss θ4) (add-variable! ss θ5) (add-variable! ss θ6)

    ;; Bayesian network constraints (original)
    (push (make-constraint `(and
                             (<=> (= ,θ1 "1")
                                  (or (and (= ,H2 "c1") (= ,ct "0"))
                                      (and (= ,obs "0") (not (= ,H2 "c1")) (= ,ct "1"))))

                             (<=> (= ,θ2 "1")
                                  (or (and (= ,H2 "c2")  (= ,ct "0"))
                                      (and (= ,obs "0") (not (= ,H2 "c2"))  (= ,ct "1"))))

                             (<=> (= ,θ3 "1")
                                  (or (and (= ,H1 "c1") (= ,H2 "c1")  (= ,ct "0"))
                                      (and (= ,H1 "c1") (not (= ,H2 "c1")) (= ,obs "0")  (= ,ct "1"))))

                             (<=> (= ,θ4 "1")
                                  (or (and (= ,H1 "c1") (= ,H2 "c2")  (= ,ct "0"))
                                      (and (= ,H1 "c1") (not (= ,H2 "c2")) (= ,obs "0")  (= ,ct "1"))))

                             (<=> (= ,θ5 "1")
                                  (or (and (= ,H1 "c2") (= ,H2 "c1")  (= ,ct "0"))
                                      (and (= ,H1 "c2") (not (= ,H2 "c1")) (= ,obs "0")  (= ,ct "1"))))

                             (<=> (= ,θ6 "1")
                                  (or (and (= ,H1 "c2") (= ,H2 "c2")  (= ,ct "0"))
                                      (and (= ,H1 "c2") (not (= ,H2 "c2")) (= ,obs "0")  (= ,ct "1"))))))
          constraints)

    ;; ;; Counter
    (push (make-constraint `(=> (= ,obs "1")
                                (= ,ct "0")))
          constraints)


    ;; Full models with ct variable too -- works!
    ;; (push (make-constraint `(or
    ;;                          (and (= ,θ1 "1" )  (= ,θ2 "0" )  (= ,θ3 "1" )  (= ,θ4 "0" )  (= ,θ5 "0" )  (= ,θ6 "0" )  (= ,H1 "c1" )  (= ,H2 "c1" ) (= ,obs "1") (= ,ct "0"))
    ;;                          (and (= ,θ1 "1" )  (= ,θ2 "0" )  (= ,θ3 "0" )  (= ,θ4 "0" )  (= ,θ5 "1" )  (= ,θ6 "0" )  (= ,H1 "c2" )  (= ,H2 "c1" ) (= ,obs "1") (= ,ct "0"))
    ;;                          (and (= ,θ1 "0" )  (= ,θ2 "1" )  (= ,θ3 "0" )  (= ,θ4 "1" )  (= ,θ5 "0" )  (= ,θ6 "0" )  (= ,H1 "c1" )  (= ,H2 "c2" ) (= ,obs "1") (= ,ct "0"))
    ;;                          (and (= ,θ1 "0" )  (= ,θ2 "1" )  (= ,θ3 "0" )  (= ,θ4 "0" )  (= ,θ5 "0" )  (= ,θ6 "1" )  (= ,H1 "c2" )  (= ,H2 "c2" ) (= ,obs "1") (= ,ct "0"))


    ;;                          (and (= ,θ1 "1" )  (= ,θ2 "0" )  (= ,θ3 "1" )  (= ,θ4 "0" )  (= ,θ5 "0" )  (= ,θ6 "0" )  (= ,H1 "c1" )  (= ,H2 "c1" ) (= ,obs "0") (= ,ct "0"))
    ;;                          (and (= ,θ1 "0" )  (= ,θ2 "1" )  (= ,θ3 "0" )  (= ,θ4 "1" )  (= ,θ5 "0" )  (= ,θ6 "0" )  (= ,H1 "c1" )  (= ,H2 "c1" ) (= ,obs "0") (= ,ct "1"))

    ;;                          (and (= ,θ1 "1" )  (= ,θ2 "0" )  (= ,θ3 "0" )  (= ,θ4 "0" )  (= ,θ5 "1" )  (= ,θ6 "0" )  (= ,H1 "c2" )  (= ,H2 "c1" ) (= ,obs "0") (= ,ct "0"))
    ;;                          (and (= ,θ1 "0" )  (= ,θ2 "1" )  (= ,θ3 "0" )  (= ,θ4 "0" )  (= ,θ5 "0" )  (= ,θ6 "1" )  (= ,H1 "c2" )  (= ,H2 "c1" ) (= ,obs "0") (= ,ct "1"))

    ;;                          (and (= ,θ1 "1" )  (= ,θ2 "0" )  (= ,θ3 "1" )  (= ,θ4 "0" )  (= ,θ5 "0" )  (= ,θ6 "0" )  (= ,H1 "c1" )  (= ,H2 "c2" ) (= ,obs "0") (= ,ct "1"))
    ;;                          (and (= ,θ1 "0" )  (= ,θ2 "1" )  (= ,θ3 "0" )  (= ,θ4 "1" )  (= ,θ5 "0" )  (= ,θ6 "0" )  (= ,H1 "c1" )  (= ,H2 "c2" ) (= ,obs "0") (= ,ct "0"))

    ;;                          (and (= ,θ1 "1" )  (= ,θ2 "0" )  (= ,θ3 "0" )  (= ,θ4 "0" )  (= ,θ5 "1" )  (= ,θ6 "0" )  (= ,H1 "c2" )  (= ,H2 "c2" ) (= ,obs "0") (= ,ct "1"))
    ;;                          (and (= ,θ1 "0" )  (= ,θ2 "1" )  (= ,θ3 "0" )  (= ,θ4 "0" )  (= ,θ5 "0" )  (= ,θ6 "1" )  (= ,H1 "c2" )  (= ,H2 "c2" ) (= ,obs "0") (= ,ct "0"))

    ;;                          ))
    ;;       constraints)



    ;; Manual override on Bayesian network constraints... specifying 12 models... it works!
    ;; (push (make-constraint `(or
    ;;                          (and (= ,θ1 "1" )  (= ,θ2 "0" )  (= ,θ3 "1" )  (= ,θ4 "0" )  (= ,θ5 "0" )  (= ,θ6 "0" )  (= ,H1 "c1" )  (= ,H2 "c1" ) (= ,obs "1"))
    ;;                          (and (= ,θ1 "1" )  (= ,θ2 "0" )  (= ,θ3 "0" )  (= ,θ4 "0" )  (= ,θ5 "1" )  (= ,θ6 "0" )  (= ,H1 "c2" )  (= ,H2 "c1" ) (= ,obs "1"))
    ;;                          (and (= ,θ1 "0" )  (= ,θ2 "1" )  (= ,θ3 "0" )  (= ,θ4 "1" )  (= ,θ5 "0" )  (= ,θ6 "0" )  (= ,H1 "c1" )  (= ,H2 "c2" ) (= ,obs "1"))
    ;;                          (and (= ,θ1 "0" )  (= ,θ2 "1" )  (= ,θ3 "0" )  (= ,θ4 "0" )  (= ,θ5 "0" )  (= ,θ6 "1" )  (= ,H1 "c2" )  (= ,H2 "c2" ) (= ,obs "1"))


    ;;                          (and (= ,θ1 "1" )  (= ,θ2 "0" )  (= ,θ3 "1" )  (= ,θ4 "0" )  (= ,θ5 "0" )  (= ,θ6 "0" )  (= ,H1 "c1" )  (= ,H2 "c1" ) (= ,obs "0"))
    ;;                          (and (= ,θ1 "0" )  (= ,θ2 "1" )  (= ,θ3 "0" )  (= ,θ4 "1" )  (= ,θ5 "0" )  (= ,θ6 "0" )  (= ,H1 "c1" )  (= ,H2 "c1" ) (= ,obs "0"))

    ;;                          (and (= ,θ1 "1" )  (= ,θ2 "0" )  (= ,θ3 "0" )  (= ,θ4 "0" )  (= ,θ5 "1" )  (= ,θ6 "0" )  (= ,H1 "c2" )  (= ,H2 "c1" ) (= ,obs "0"))
    ;;                          (and (= ,θ1 "0" )  (= ,θ2 "1" )  (= ,θ3 "0" )  (= ,θ4 "0" )  (= ,θ5 "0" )  (= ,θ6 "1" )  (= ,H1 "c2" )  (= ,H2 "c1" ) (= ,obs "0"))

    ;;                          (and (= ,θ1 "1" )  (= ,θ2 "0" )  (= ,θ3 "1" )  (= ,θ4 "0" )  (= ,θ5 "0" )  (= ,θ6 "0" )  (= ,H1 "c1" )  (= ,H2 "c2" ) (= ,obs "0"))
    ;;                          (and (= ,θ1 "0" )  (= ,θ2 "1" )  (= ,θ3 "0" )  (= ,θ4 "1" )  (= ,θ5 "0" )  (= ,θ6 "0" )  (= ,H1 "c1" )  (= ,H2 "c2" ) (= ,obs "0"))

    ;;                          (and (= ,θ1 "1" )  (= ,θ2 "0" )  (= ,θ3 "0" )  (= ,θ4 "0" )  (= ,θ5 "1" )  (= ,θ6 "0" )  (= ,H1 "c2" )  (= ,H2 "c2" ) (= ,obs "0"))
    ;;                          (and (= ,θ1 "0" )  (= ,θ2 "1" )  (= ,θ3 "0" )  (= ,θ4 "0" )  (= ,θ5 "0" )  (= ,θ6 "1" )  (= ,H1 "c2" )  (= ,H2 "c2" ) (= ,obs "0"))

    ;;                          ))
    ;;       constraints)


    ;; Causal link constraints
    (push (make-constraint `(and (=> (= ,H1 "c1")
                                     (= ,R2 "c1"))

                                 (=> (= ,H1 "c2")
                                     (= ,R2 "c2"))))
          constraints)

    ;; ;; Observation constraint
    (push (make-constraint `(<=> (not (= ,R1 "c1"))
                                 (= ,obs "0")))
          constraints)

    ;; ;; ;; Optional: even if we choose R1 = c2 (so that H2 shouldn't be observed!),
    ;; ;; ;; same result with this encoding
    (push (make-constraint `(= ,R1 "c2"))
          constraints)

    ;; Testing!!!!!
    ;; (push (make-constraint `(= ,R1 "c2"))
    ;;       constraints)


    ;; Set weights
    (setf weights (make-hash-table :test #'pike::eql-list-2))
    (setf (gethash `(,(assignment θ1 "1") t) weights) (/ 2 3))
    (setf (gethash `(,(assignment θ2 "1") t) weights) (/ 1 3))
    (setf (gethash `(,(assignment θ3 "1") t) weights) 1)
    (setf (gethash `(,(assignment θ4 "1") t) weights) 0)
    (setf (gethash `(,(assignment θ5 "1") t) weights) 0)
    (setf (gethash `(,(assignment θ6 "1") t) weights) 1)
    (setf (gethash `(,(assignment obs "1") nil) weights) (/ 1 (length (pike::decision-variable-values obs))))

    (setf bn-variables (list (assignment H1 "c1") (assignment H1 "c2")
                             (assignment H2 "c1") (assignment H2 "c2")
                             (assignment obs "1") (assignment obs "0")
                             (assignment ct "1") (assignment ct "0")
                             (assignment θ1 "1") (assignment θ1 "0")
                             (assignment θ2 "1") (assignment θ2 "0")
                             (assignment θ3 "1") (assignment θ3 "0")
                             (assignment θ4 "1") (assignment θ4 "0")
                             (assignment θ5 "1") (assignment θ5 "0")
                             (assignment θ6 "1") (assignment θ6 "0")))

    ;; Start a BDD manager
    (setf bm (pike::create-bdd-manager))

    ;; Create a BDD encoder (which connects a state space and BDD manager)
    (setf bde (pike::make-bdd-encoder :ss ss
                                      :bm bm))

    ;; Set up the encoder
    (pike::bdd-encoder-initialize bde
                                  `(,R1 ,obs  ,H2 ,R2 ,H1 ,θ1 ,θ2 ,θ3 ,θ4 ,θ5 ,θ6 ,ct)
                                  :use-binaries nil ;; Simpler for testing
                                  :variable-order-heuristic :default ;; Use pTPN order above
                                  :constraints constraints)

    ;; Set sum product / projection variables
    (pike::bdd-set-Σ∏-variables bm bn-variables)
    (pike::bdd-set-projection-variables bm bn-variables)
    (pike::bdd-set-weights bm weights)

    (setf n (pike::bdd-encoder-get-variable-constraints-bdd bde))
    (dolist (c constraints)
      (setf n (pike::bdd-and bm
                             n
                             (pike::constraint->bdd bde c))))

    (format t "Number of BDD nodes in n: ~a~%" (pike::bdd-count-children n))
    (format t "Max Sum Product: ~a~%" (pike::bdd-maxσ∏ bm n))
    (values bde bm)))



(defun test-unobserved-choices-4()
  "See example from paper / lab notebook / phone from 10/11/2018. This one works!!"
  (let* ((ss (make-state-space-bits))

         ;; pTPN variables
         (H1 (make-instance 'decision-variable
                            :name "H1"
                            :domain (list "c1" "c2")))
         (H2 (make-instance 'decision-variable
                            :name "H2"
                            :domain (list "c1" "c2")))
         (R1 (make-instance 'decision-variable
                            :name "R1"
                            :domain (list "c1" "c2")))
         (R2 (make-instance 'decision-variable
                            :name "R2"
                            :domain (list "c1" "c2")))

         ;; Two spec variables for posterity
         (spec1 (make-instance 'decision-variable
                               :name "spec1"
                               :domain (list "1" "none")))
         (spec2 (make-instance 'decision-variable
                               :name "spec2"
                               :domain (list "1" "none")))

         ;; Observation variable
         (obs (make-instance 'decision-variable
                             :name "obs"
                             :domain (list "1" "0")))


         ;; Bayesian network variables
         (θ1 (make-instance 'decision-variable
                            :name "θ1"
                            :domain (list "1" "0")))
         (θ2 (make-instance 'decision-variable
                            :name "θ2"
                            :domain (list "1" "0")))
         (θ3 (make-instance 'decision-variable
                            :name "θ3"
                            :domain (list "1" "0")))
         (θ4 (make-instance 'decision-variable
                            :name "θ4"
                            :domain (list "1" "0")))
         (θ5 (make-instance 'decision-variable
                            :name "θ5"
                            :domain (list "1" "0")))
         (θ6 (make-instance 'decision-variable
                            :name "θ6"
                            :domain (list "1" "0")))

         ;; "Phantom" Bayesian network variables
         (H2p (make-instance 'decision-variable
                             :name "H2'"
                             :domain (list "c1" "c2")))
         (θ1p (make-instance 'decision-variable
                             :name "θ1'"
                             :domain (list "1" "0")))
         (θ2p (make-instance 'decision-variable
                             :name "θ2'"
                             :domain (list "1" "0")))
         (θ3p (make-instance 'decision-variable
                             :name "θ3'"
                             :domain (list "1" "0")))
         (θ4p (make-instance 'decision-variable
                             :name "θ4'"
                             :domain (list "1" "0")))
         (θ5p (make-instance 'decision-variable
                             :name "θ5'"
                             :domain (list "1" "0")))
         (θ6p (make-instance 'decision-variable
                             :name "θ6'"
                             :domain (list "1" "0")))

         bm bde n weights bn-variables
         constraints)

    ;; Set up the ss
    (add-variable! ss H1) (add-variable! ss H2) (add-variable! ss R1) (add-variable! ss R2) (add-variable! ss spec1) (add-variable! ss spec2)
    (add-variable! ss obs)
    (add-variable! ss θ1) (add-variable! ss θ2) (add-variable! ss θ3) (add-variable! ss θ4) (add-variable! ss θ5) (add-variable! ss θ6)
    (add-variable! ss H2p) (add-variable! ss θ1p) (add-variable! ss θ2p) (add-variable! ss θ3p) (add-variable! ss θ4p) (add-variable! ss θ5p) (add-variable! ss θ6p)

    ;; Bayesian network constraints (original)
    (push (make-constraint `(and
                             ;; Bayesian network constraints
                             (<=> (= ,θ1 "1")
                                  (and (= ,H2 "c1") (= ,obs "1")))

                             (<=> (= ,θ2 "1")
                                  (and (= ,H2 "c2") (= ,obs "1")))

                             (<=> (= ,θ3 "1")
                                  (and (= ,H1 "c1") (= ,H2 "c1") (= ,obs "1")))

                             (<=> (= ,θ4 "1")
                                  (and (= ,H1 "c1") (= ,H2 "c2") (= ,obs "1")))

                             (<=> (= ,θ5 "1")
                                  (and (= ,H1 "c2") (= ,H2 "c1") (= ,obs "1")))

                             (<=> (= ,θ6 "1")
                                  (and (= ,H1 "c2") (= ,H2 "c2") (= ,obs "1")))

                             ;; Phantom Bayesian network constraints
                             ;; Bayesian network constraints
                             (<=> (= ,θ1p "1")
                                  (and (= ,H2p "c1") (= ,obs "0")))

                             (<=> (= ,θ2p "1")
                                  (and (= ,H2p "c2") (= ,obs "0")))

                             (<=> (= ,θ3p "1")
                                  (and (= ,H1 "c1") (= ,H2p "c1") (= ,obs "0")))

                             (<=> (= ,θ4p "1")
                                  (and (= ,H1 "c1") (= ,H2p "c2") (= ,obs "0")))

                             (<=> (= ,θ5p "1")
                                  (and (= ,H1 "c2") (= ,H2p "c1") (= ,obs "0")))

                             (<=> (= ,θ6p "1")
                                  (and (= ,H1 "c2") (= ,H2p "c2") (= ,obs "0")))

                             ;; Lock the phantom vars if not being used, so as to not
                             ;; artificially increase the WMC by having extra "free" variables
                             ;; Can pick arbitary assignment, doesn't matter.
                             (=> (= ,obs "1")
                                 (and (= ,H2p "c1")))
                             ;; If the phantom vars are used, lock the real vars (same reasoning).
                             (=> (= ,obs "0")
                                 (and (= ,H2 "c1")))))

          constraints)



    ;; Causal link constraints
    (push (make-constraint `(and (=> (= ,H1 "c1")
                                     (and (= ,R2 "c1") (= ,spec1 "1")))

                                 (=> (= ,H1 "c2")
                                     (and (= ,R2 "c2") (= ,spec2 "1")))))
          constraints)

    ;; Observation constraint
    (push (make-constraint `(<=> (= ,R1 "c1")
                                 (= ,obs "1")))
          constraints)

    ;; Optional: even if we choose R1 = c2 (so that H2 shouldn't be observed!),
    ;; same result with this encoding
    (push (make-constraint `(= ,R1 "c2"))
          constraints)

    ;; Testing!!!!!
    ;; (push (make-constraint `(and (= ,obs "1")))
    ;;       constraints)


    ;; Set weights
    (setf weights (make-hash-table :test #'pike::eql-list-2))
    (setf (gethash `(,(assignment θ1 "1") t) weights) (/ 2 3))
    (setf (gethash `(,(assignment θ2 "1") t) weights) (/ 1 3))
    (setf (gethash `(,(assignment θ3 "1") t) weights) 1)
    (setf (gethash `(,(assignment θ4 "1") t) weights) 0)
    (setf (gethash `(,(assignment θ5 "1") t) weights) 0)
    (setf (gethash `(,(assignment θ6 "1") t) weights) 1)

    (setf (gethash `(,(assignment θ1p "1") t) weights) (/ 2 3))
    (setf (gethash `(,(assignment θ2p "1") t) weights) (/ 1 3))
    (setf (gethash `(,(assignment θ3p "1") t) weights) 1)
    (setf (gethash `(,(assignment θ4p "1") t) weights) 0)
    (setf (gethash `(,(assignment θ5p "1") t) weights) 0)
    (setf (gethash `(,(assignment θ6p "1") t) weights) 1)

    ;; Below not needed if "locking" the real vars
    ;; (setf (gethash `(,(assignment obs "1") nil) weights) (/ 1 (length (pike::decision-variable-values obs))))

    (setf bn-variables (list (assignment H1 "c1") (assignment H1 "c2")
                             (assignment H2 "c1") (assignment H2 "c2")
                             (assignment obs "1") (assignment obs "0")
                             (assignment θ1 "1") (assignment θ1 "0")
                             (assignment θ2 "1") (assignment θ2 "0")
                             (assignment θ3 "1") (assignment θ3 "0")
                             (assignment θ4 "1") (assignment θ4 "0")
                             (assignment θ5 "1") (assignment θ5 "0")
                             (assignment θ6 "1") (assignment θ6 "0")
                             (assignment H2p "c1") (assignment H2p "c2")
                             (assignment θ1p "1") (assignment θ1p "0")
                             (assignment θ2p "1") (assignment θ2p "0")
                             (assignment θ3p "1") (assignment θ3p "0")
                             (assignment θ4p "1") (assignment θ4p "0")
                             (assignment θ5p "1") (assignment θ5p "0")
                             (assignment θ6p "1") (assignment θ6p "0")))

    ;; Start a BDD manager
    (setf bm (pike::create-bdd-manager))

    ;; Create a BDD encoder (which connects a state space and BDD manager)
    (setf bde (pike::make-bdd-encoder :ss ss
                                      :bm bm))

    ;; Set up the encoder
    (pike::bdd-encoder-initialize bde
                                  ;; `(,R1 ,H2 ,R2 ,H1 ,spec1 ,spec2 ,obs ,H2p ,θ1 ,θ2 ,θ3 ,θ4 ,θ5 ,θ6 ,θ1p ,θ2p ,θ3p ,θ4p ,θ5p ,θ6p) ;; Works!
                                  ;; `(,R1 ,H2 ,θ1 ,θ2 ,θ1p ,θ2p ,R2 ,H1 ,θ3 ,θ4 ,θ5 ,θ6 ,θ3p ,θ4p ,θ5p ,θ6p ,obs ,H2p ,spec1 ,spec2 ) ;; Doesn't work! Correctly reproduces results.
                                  `(,R1 ,H2 ,θ1 ,θ2 ,R2 ,H1 ,θ3 ,θ4 ,θ5 ,θ6 ,obs ,H2p ,θ1p ,θ2p ,θ3p ,θ4p ,θ5p ,θ6p ,spec1 ,spec2 ) ;; Works!
                                  :use-binaries nil ;; Simpler for testing
                                  :variable-order-heuristic :default ;; Use pTPN order above
                                  :constraints constraints)

    ;; Set sum product / projection variables
    (pike::bdd-set-Σ∏-variables bm bn-variables)
    (pike::bdd-set-projection-variables bm bn-variables)
    (pike::bdd-set-weights bm weights)

    (setf n (pike::bdd-encoder-get-variable-constraints-bdd bde))
    (dolist (c constraints)
      (setf n (pike::bdd-and bm
                             n
                             (pike::constraint->bdd bde c))))

    (format t "Number of BDD nodes in n: ~a~%" (pike::bdd-count-children n))
    (format t "Max Sum Product: ~a~%" (pike::bdd-maxσ∏ bm n))
    (values bde bm)))



(defun test-find-phantom-bdd-sets-1 ()
  (let ((ss (make-state-space-bits))
        (H1 (make-instance 'decision-variable
                           :name "H1"
                           :domain (list "y" "n")))
        (H2 (make-instance 'decision-variable
                           :name "H2"
                           :domain (list "y" "n")))
        (HG (make-instance 'decision-variable
                           :name "HG"
                           :domain (list "y" "n")))
        (R1 (make-instance 'decision-variable
                           :name "R1"
                           :domain (list "c1" "c2")))
        (RG (make-instance 'decision-variable
                           :name "RG"
                           :domain (list "c1" "c2")))


        bm bde n bn-variables
        constraints)

    ;; Create constraints
    (push (make-constraint `(and
                             (<=> (= ,H1 "y")
                                  (= ,R1 "c1"))

                             (<=> (= ,H2 "y")
                                  (= ,R1 "c2"))

                             (<=> (= ,HG "y")
                                  true)))
          constraints)


    ;; Set projection variables
    (setf bn-variables (list (assignment H1 "y") (assignment H1 "n")
                             (assignment H2 "y") (assignment H2 "n")
                             (assignment HG "y") (assignment HG "n")))


    ;; Start a BDD manager
    (setf bm (pike::create-bdd-manager))

    ;; Create a BDD encoder (which connects a state space and BDD manager)
    (setf bde (pike::make-bdd-encoder :ss ss
                                      :bm bm))

    ;; Set up the encoder
    (pike::bdd-encoder-initialize bde
                                  `(,H1 ,H2 ,HG ,R1 ,RG)
                                  :use-binaries nil ;; Simpler for testing
                                  :variable-order-heuristic :default ;; Use pTPN order above
                                  :constraints constraints)

    ;; Set sum product / projection variables
    (pike::bdd-set-Σ∏-variables bm bn-variables)
    (pike::bdd-set-projection-variables bm bn-variables)

    (setf n (pike::bdd-encoder-get-variable-constraints-bdd bde))
    (dolist (c constraints)
      (setf n (pike::bdd-and bm
                             n
                             (pike::constraint->bdd bde c))))
    ;; Project away
    (setf n (pike::bdd-project bm n))

    (format t "Number of BDD nodes in n: ~a~%" (pike::bdd-count-children n))
    (format t "Projected model count: ~a~%" (pike::bdd-wmc bm n))

    ;; Get the models as lists of assignments
    (let (models models-raw)
      (setf models-raw (pike::bdd-enumerate-all-projected-models bm n))
      (setf models (loop for m in models-raw
                      collect (loop for (a pos?) in m when (and pos? (equal "y" (pike::assignment-value a))) collect (pike::assignment-variable a))))
      (format t "Projected models: ~a~%" models)
      models)))


(defun test-find-phantom-bdd-sets-2 ()
  (let ((ss (make-state-space-bits))
        (H1 (make-instance 'decision-variable
                           :name "H1"
                           :domain (list "y" "n")))
        (H2 (make-instance 'decision-variable
                           :name "H2"
                           :domain (list "y" "n")))
        (R1 (make-instance 'decision-variable
                           :name "R1"
                           :domain (list "c1" "c2")))
        (R2 (make-instance 'decision-variable
                           :name "R2"
                           :domain (list "c1" "c2")))

        bm bde n bn-variables
        constraints)

    ;; Create constraints
    (push (make-constraint `(and
                             (<=> (= ,H1 "y")
                                  (= ,R1 "c1"))

                             (<=> (= ,H2 "y")
                                  true)))
          constraints)


    ;; Set projection variables
    (setf bn-variables (list (assignment H1 "y") (assignment H1 "n")
                             (assignment H2 "y") (assignment H2 "n")))


    ;; Start a BDD manager
    (setf bm (pike::create-bdd-manager))

    ;; Create a BDD encoder (which connects a state space and BDD manager)
    (setf bde (pike::make-bdd-encoder :ss ss
                                      :bm bm))

    ;; Set up the encoder
    (pike::bdd-encoder-initialize bde
                                  `(,R1 ,H2 ,R2 ,H1)
                                  :use-binaries nil ;; Simpler for testing
                                  :variable-order-heuristic :default ;; Use pTPN order above
                                  :constraints constraints)

    ;; Set sum product / projection variables
    (pike::bdd-set-Σ∏-variables bm bn-variables)
    (pike::bdd-set-projection-variables bm bn-variables)

    (setf n (pike::bdd-encoder-get-variable-constraints-bdd bde))
    (dolist (c constraints)
      (setf n (pike::bdd-and bm
                             n
                             (pike::constraint->bdd bde c))))
    ;; Project away
    (setf n (pike::bdd-project bm n))

    (format t "Number of BDD nodes in n: ~a~%" (pike::bdd-count-children n))
    (format t "Projected model count: ~a~%" (pike::bdd-wmc bm n))

    ;; Get the models as lists of assignments
    (let (models models-raw)
      (setf models-raw (pike::bdd-enumerate-all-projected-models bm n))
      (setf models (loop for m in models-raw
                      collect (loop for (a pos?) in m when (and pos? (equal "y" (pike::assignment-value a))) collect (pike::assignment-variable a))))
      (format t "Projected models: ~a~%" models)
      models)))




(defun test-find-phantom-bdd-sets-3 ()
  (let ((ss (make-state-space-bits))
        (H1 (make-instance 'decision-variable
                           :name "H1"
                           :domain (list "y" "n")))
        (H2 (make-instance 'decision-variable
                           :name "H2"
                           :domain (list "y" "n")))
        (H3 (make-instance 'decision-variable
                           :name "H3"
                           :domain (list "y" "n")))
        (HG (make-instance 'decision-variable
                           :name "HG"
                           :domain (list "y" "n")))
        (R1 (make-instance 'decision-variable
                           :name "R1"
                           :domain (list "c1" "c2")))
        (R2 (make-instance 'decision-variable
                           :name "R2"
                           :domain (list "c1" "c2")))
        (R3 (make-instance 'decision-variable
                           :name "R3"
                           :domain (list "c1" "c2")))
        (RG (make-instance 'decision-variable
                           :name "RG"
                           :domain (list "c1" "c2")))


        bm bde n bn-variables
        constraints)

    ;; Create constraints
    (push (make-constraint `(and
                             (<=> (= ,H1 "y")
                                  (= ,R1 "c1"))

                             (<=> (= ,H2 "y")
                                  (= ,R2 "c1"))

                             (<=> (= ,H3 "y")
                                  (= ,R3 "c1"))

                             (<=> (= ,HG "y")
                                  true)))
          constraints)


    ;; Set projection variables
    (setf bn-variables (list (assignment H1 "y") (assignment H1 "n")
                             (assignment H2 "y") (assignment H2 "n")
                             (assignment H3 "y") (assignment H3 "n")
                             (assignment HG "y") (assignment HG "n")))


    ;; Start a BDD manager
    (setf bm (pike::create-bdd-manager))

    ;; Create a BDD encoder (which connects a state space and BDD manager)
    (setf bde (pike::make-bdd-encoder :ss ss
                                      :bm bm))

    ;; Set up the encoder
    (pike::bdd-encoder-initialize bde
                                  `(,H1 ,H2 ,H3 ,HG ,R1 ,R2 ,R3 ,RG)
                                  :use-binaries nil ;; Simpler for testing
                                  :variable-order-heuristic :default ;; Use pTPN order above
                                  :constraints constraints)

    ;; Set sum product / projection variables
    (pike::bdd-set-Σ∏-variables bm bn-variables)
    (pike::bdd-set-projection-variables bm bn-variables)

    (setf n (pike::bdd-encoder-get-variable-constraints-bdd bde))
    (dolist (c constraints)
      (setf n (pike::bdd-and bm
                             n
                             (pike::constraint->bdd bde c))))
    ;; Project away
    (setf n (pike::bdd-project bm n))

    (format t "Number of BDD nodes in n: ~a~%" (pike::bdd-count-children n))
    (format t "Projected model count: ~a~%" (pike::bdd-wmc bm n))

    ;; Get the models as lists of assignments
    (let (models models-raw)
      (setf models-raw (pike::bdd-enumerate-all-projected-models bm n))
      (setf models (loop for m in models-raw
                      collect (loop for (a pos?) in m when (and pos? (equal "y" (pike::assignment-value a))) collect (pike::assignment-variable a))))
      (format t "Projected models: ~a~%" models)
      models)))


(defun test-find-phantom-bdd-sets-4 ()
  (let ((ss (make-state-space-bits))
        (H1 (make-instance 'decision-variable
                           :name "H1"
                           :domain (list "y" "n")))
        (H2 (make-instance 'decision-variable
                           :name "H2"
                           :domain (list "y" "n")))
        (H3 (make-instance 'decision-variable
                           :name "H3"
                           :domain (list "y" "n")))
        (HG (make-instance 'decision-variable
                           :name "HG"
                           :domain (list "y" "n")))
        (R1 (make-instance 'decision-variable
                           :name "R1"
                           :domain (list "c1" "c2")))
        (RG (make-instance 'decision-variable
                           :name "RG"
                           :domain (list "c1" "c2")))

        bm bde n bn-variables
        constraints)

    ;; Create constraints
    (push (make-constraint `(and
                             (<=> (= ,H1 "y")
                                  (= ,R1 "c1"))

                             (<=> (= ,H2 "y")
                                  (= ,R1 "c1"))

                             (<=> (= ,H3 "y")
                                  (= ,R1 "c1"))

                             (<=> (= ,HG "y")
                                  true)))
          constraints)


    ;; Set projection variables
    (setf bn-variables (list (assignment H1 "y") (assignment H1 "n")
                             (assignment H2 "y") (assignment H2 "n")
                             (assignment H3 "y") (assignment H3 "n")
                             (assignment HG "y") (assignment HG "n")))


    ;; Start a BDD manager
    (setf bm (pike::create-bdd-manager))

    ;; Create a BDD encoder (which connects a state space and BDD manager)
    (setf bde (pike::make-bdd-encoder :ss ss
                                      :bm bm))

    ;; Set up the encoder
    (pike::bdd-encoder-initialize bde
                                  `(,H1 ,H2 ,H3 ,HG ,R1 ,RG)
                                  :use-binaries nil ;; Simpler for testing
                                  :variable-order-heuristic :default ;; Use pTPN order above
                                  :constraints constraints)

    ;; Set sum product / projection variables
    (pike::bdd-set-Σ∏-variables bm bn-variables)
    (pike::bdd-set-projection-variables bm bn-variables)

    (setf n (pike::bdd-encoder-get-variable-constraints-bdd bde))
    (dolist (c constraints)
      (setf n (pike::bdd-and bm
                             n
                             (pike::constraint->bdd bde c))))
    ;; Project away
    (setf n (pike::bdd-project bm n))

    (format t "Number of BDD nodes in n: ~a~%" (pike::bdd-count-children n))
    (format t "Projected model count: ~a~%" (pike::bdd-wmc bm n))

    ;; Get the models as lists of assignments
    (let (models models-raw)
      (setf models-raw (pike::bdd-enumerate-all-projected-models bm n))
      (setf models (loop for m in models-raw
                      collect (loop for (a pos?) in m when (and pos? (equal "y" (pike::assignment-value a))) collect (pike::assignment-variable a))))
      (format t "Projected models: ~a~%" models)
      models)))



(defun test-find-phantom-bdd-sets-5 ()
  (let ((ss (make-state-space-bits))
        (H1 (make-instance 'decision-variable
                           :name "H1"
                           :domain (list "y" "n")))
        (H2 (make-instance 'decision-variable
                           :name "H2"
                           :domain (list "y" "n")))
        (H3 (make-instance 'decision-variable
                           :name "H3"
                           :domain (list "y" "n")))
        (HG (make-instance 'decision-variable
                           :name "HG"
                           :domain (list "y" "n")))
        (R1 (make-instance 'decision-variable
                           :name "R1"
                           :domain (list "c1" "c2")))
        (R2 (make-instance 'decision-variable
                           :name "R1"
                           :domain (list "c1" "c2")))
        (RG (make-instance 'decision-variable
                           :name "RG"
                           :domain (list "c1" "c2")))

        bm bde n bn-variables
        constraints)

    ;; Create constraints
    (push (make-constraint `(and
                             (<=> (= ,H1 "y")
                                  (and (= ,R1 "c1") (= ,R2 "c1")))

                             (<=> (= ,H2 "y")
                                  (= ,R1 "c1"))

                             (<=> (= ,H3 "y")
                                  (= ,R1 "c2"))

                             (<=> (= ,HG "y")
                                  true)))
          constraints)


    ;; Set projection variables
    (setf bn-variables (list (assignment H1 "y") (assignment H1 "n")
                             (assignment H2 "y") (assignment H2 "n")
                             (assignment H3 "y") (assignment H3 "n")
                             (assignment HG "y") (assignment HG "n")))


    ;; Start a BDD manager
    (setf bm (pike::create-bdd-manager))

    ;; Create a BDD encoder (which connects a state space and BDD manager)
    (setf bde (pike::make-bdd-encoder :ss ss
                                      :bm bm))

    ;; Set up the encoder
    (pike::bdd-encoder-initialize bde
                                  `(,H1 ,H2 ,H3 ,HG ,R1 ,R2 ,RG)
                                  :use-binaries nil ;; Simpler for testing
                                  :variable-order-heuristic :default ;; Use pTPN order above
                                  :constraints constraints)

    ;; Set sum product / projection variables
    (pike::bdd-set-Σ∏-variables bm bn-variables)
    (pike::bdd-set-projection-variables bm bn-variables)

    (setf n (pike::bdd-encoder-get-variable-constraints-bdd bde))
    (dolist (c constraints)
      (setf n (pike::bdd-and bm
                             n
                             (pike::constraint->bdd bde c))))
    ;; Project away
    (setf n (pike::bdd-project bm n))

    (format t "Number of BDD nodes in n: ~a~%" (pike::bdd-count-children n))
    (format t "Projected model count: ~a~%" (pike::bdd-wmc bm n))

    ;; Get the models as lists of assignments
    (let (models models-raw)
      (setf models-raw (pike::bdd-enumerate-all-projected-models bm n))
      (setf models (loop for m in models-raw
                      collect (loop for (a pos?) in m when (and pos? (equal "y" (pike::assignment-value a))) collect (pike::assignment-variable a))))
      (format t "Projected models: ~a~%" models)
      models)))
