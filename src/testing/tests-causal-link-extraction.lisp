;;;; Copyright (c) 2015 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)

(in-package #:pike/test)

(def-suite :tests-causal-link-extraction
    :description "Causal link extraction-related tests for Pike"
    :in :tests-pike)

(in-suite :tests-causal-link-extraction)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; extraction-1
;;   Probably the simplest possible example: one consumer and one producer.
;;   PDDL version
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun setup-extraction-1 ()
  (let (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "causal-links/pddl/thing-1.rmpl.tpn.out")
                            :domain-filename (pike-test-file "causal-links/pddl/simple-domain.pddl")
                            :problem-filename (pike-test-file "causal-links/pddl/simple-problem.pddl")
                            :debug-mode T))
    (setf ps (create-pike-session po))
    ps))

(test extraction-1
  ;; Run it once without disturbances
  (let ((ps (setup-extraction-1)))
    ;; Make sure it compiles properly
    (is-true (pike-compile ps))
    ;; 1 causal link set with 1 producer
    (is (= 1 (length (pike-causal-link-sets ps))))
    (is (= 1 (length (producer-events (first (pike-causal-link-sets ps))))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; extraction-2
;;   Probably the simplest possible example: one consumer and one producer.
;;   RMPL version
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun setup-extraction-2 ()
  (let (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "causal-links/rmpl/thing-1.rmpl.tpn")
                            :domain-filename (pike-test-file "causal-links/rmpl/thing-1.rmpl.xml")
                            :debug-mode T))
    (setf ps (create-pike-session po))
    ps))

(test extraction-2
  ;; Run it once without disturbances
  (let ((ps (setup-extraction-2)))
    ;; Make sure it compiles properly
    (is-true (pike-compile ps))
    ;; 1 causal link set with 1 producer
    (is (= 1 (length (pike-causal-link-sets ps))))
    (is (= 1 (length (producer-events (first (pike-causal-link-sets ps))))))))






;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; dominance-1
;;   A producer followed by a choice of a producer. Make sure we don't have
;;   any causal link dominance here.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun setup-dominance-1 ()
  (let (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "causal-links/pddl/thing-2.rmpl.tpn.out")
                            :domain-filename (pike-test-file "causal-links/pddl/simple-domain.pddl")
                            :problem-filename (pike-test-file "causal-links/pddl/simple-problem.pddl")
                            :debug-mode T))
    (setf ps (create-pike-session po))
    ps))

(test dominance-1
  ;; Run it once without disturbances
  (let ((ps (setup-dominance-1)))
    ;; Make sure it compiles properly
    (is-true (pike-compile ps))
    ;; 1 causal link set with 2 producers
    (is (= 1 (length (pike-causal-link-sets ps))))
    (is (= 2 (length (producer-events (first (pike-causal-link-sets ps))))))))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; dominance-2
;;  A choice of a producer followed by a producer. We should have causal
;;  link domination here.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun setup-dominance-2 ()
  (let (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "causal-links/pddl/thing-3.rmpl.tpn.out")
                            :domain-filename (pike-test-file "causal-links/pddl/simple-domain.pddl")
                            :problem-filename (pike-test-file "causal-links/pddl/simple-problem.pddl")
                            :use-causal-link-dominance? t
                            :debug-mode T))
    (setf ps (create-pike-session po))
    ps))

(test dominance-2
  ;; Run it once without disturbances
  (let ((ps (setup-dominance-2)))
    ;; Make sure it compiles properly
    (is-true (pike-compile ps))
    ;; 1 causal link set with 2 producers
    (is (= 1 (length (pike-causal-link-sets ps))))
    (is (= 1 (length (producer-events (first (pike-causal-link-sets ps))))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; dominance-3
;; Same case as above, but here we test explicitly turning off causal link
;; domination. If it works, we should have two causal links instead of one
;; dominating one.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun setup-dominance-3 ()
  (let (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "causal-links/pddl/thing-3.rmpl.tpn.out")
                            :domain-filename (pike-test-file "causal-links/pddl/simple-domain.pddl")
                            :problem-filename (pike-test-file "causal-links/pddl/simple-problem.pddl")
                            :use-causal-link-dominance? nil
                            :debug-mode T))
    (setf ps (create-pike-session po))
    ps))

(test dominance-3
  ;; Run it once without disturbances
  (let ((ps (setup-dominance-3)))
    ;; Make sure it compiles properly
    (is-true (pike-compile ps))
    ;; 1 causal link set with 2 producers
    (is (= 1 (length (pike-causal-link-sets ps))))
    (is (= 2 (length (producer-events (first (pike-causal-link-sets ps))))))))
