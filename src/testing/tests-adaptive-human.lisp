;;;; Copyright (c) 2018 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike/test)

(def-suite :tests-cc-adaptive-human
    :description "Tests for Riker, where we model a human adapting back to the robot in two different ways."
    :in :tests-pike)

(in-suite :tests-cc-adaptive-human)


(defmethod setup-adaptive-human-simple-friendly-1 (&key (constraint-knowledge-base-type :cc-wmc-bdd) (memoize-constraint-calls nil))
  (let (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "adaptive-human/simple/simple.tpn")
                            :domain-filename (pike-test-file "adaptive-human/simple/domain.pddl")
                            :problem-filename (pike-test-file "adaptive-human/simple/problem.pddl")
                            :probability-distribution (pike-test-file "adaptive-human/simple/bn-friendly.xmlbif")
                            :chance-constraint 0.95
                            :debug-mode T
                            :memoize-constraint-calls memoize-constraint-calls ;; nil for testing
                            :constraint-knowledge-base-type constraint-knowledge-base-type))
    (setf ps (create-pike-session po))
    ps))

(defmethod setup-adaptive-human-simple-adverserial-1 (&key (constraint-knowledge-base-type :cc-wmc-bdd) (memoize-constraint-calls nil))
  (let (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "adaptive-human/simple/simple.tpn")
                            :domain-filename (pike-test-file "adaptive-human/simple/domain.pddl")
                            :problem-filename (pike-test-file "adaptive-human/simple/problem.pddl")
                            :probability-distribution (pike-test-file "adaptive-human/simple/bn-adverserial.xmlbif")
                            :chance-constraint 0.95
                            :debug-mode T
                            :memoize-constraint-calls memoize-constraint-calls ;; nil for testing
                            :constraint-knowledge-base-type constraint-knowledge-base-type))
    (setf ps (create-pike-session po))
    ps))


(defmethod setup-adaptive-human-simple-infdiag-friendly-1 (&key (constraint-knowledge-base-type :cc-wmc-bdd) (memoize-constraint-calls nil))
  (let (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "adaptive-human/simple-infdiag/simple.tpn")
                            :domain-filename (pike-test-file "adaptive-human/simple-infdiag/domain.pddl")
                            :problem-filename (pike-test-file "adaptive-human/simple-infdiag/problem.pddl")
                            :probability-distribution (pike-test-file "adaptive-human/simple-infdiag/infdiag-friendly.pgmx")
                            :chance-constraint 0.95
                            :debug-mode T
                            :memoize-constraint-calls memoize-constraint-calls ;; nil for testing
                            :constraint-knowledge-base-type constraint-knowledge-base-type))
    (setf ps (create-pike-session po))
    ps))

(defmethod setup-adaptive-human-simple-infdiag-adverserial-1 (&key (constraint-knowledge-base-type :cc-wmc-bdd) (memoize-constraint-calls nil))
  (let (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "adaptive-human/simple-infdiag/simple.tpn")
                            :domain-filename (pike-test-file "adaptive-human/simple-infdiag/domain.pddl")
                            :problem-filename (pike-test-file "adaptive-human/simple-infdiag/problem.pddl")
                            :probability-distribution (pike-test-file "adaptive-human/simple-infdiag/infdiag-adverserial.pgmx")
                            :chance-constraint 0.95
                            :debug-mode T
                            :memoize-constraint-calls memoize-constraint-calls ;; nil for testing
                            :constraint-knowledge-base-type constraint-knowledge-base-type))
    (setf ps (create-pike-session po))
    ps))


(test riker-adaptive-human-simple-friendly-1
  ;; Possibly examine multiple different methods of computing
  ;; the probabilities
  (dolist (cc-type `(:cc-wmc-bdd :cc-wmc-ab-bdd))
    (let ((ps (setup-adaptive-human-simple-friendly-1 :constraint-knowledge-base-type cc-type)))
      ;; Make sure it compiles properly
      (is-true (pike-compile ps))

      ;; Get some relevant variables
      (let* ((ckb (pike-constraint-knowledge-base ps)))

        ;; Test probability computations!
        (is  (= (/ 99 100)
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list ))))))))


(test riker-adaptive-human-simple-adverserial-1
  ;; Possibly examine multiple different methods of computing
  ;; the probabilities
  (dolist (cc-type `(:cc-wmc-bdd :cc-wmc-ab-bdd))
    (let ((ps (setup-adaptive-human-simple-adverserial-1 :constraint-knowledge-base-type cc-type)))
      ;; Make sure it compiles properly
      (is-true (pike-compile ps))

      ;; Get some relevant variables
      (let* ((ckb (pike-constraint-knowledge-base ps)))

        ;; Test probability computations!
        (is  (= (/ 1 100)
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list ))))))))


(test riker-adaptive-human-simple-infdiag-friendly-1
  ;; Possibly examine multiple different methods of computing
  ;; the probabilities
  (dolist (cc-type `(:cc-wmc-bdd :cc-wmc-ab-bdd))
    (let ((ps (setup-adaptive-human-simple-infdiag-friendly-1 :constraint-knowledge-base-type cc-type)))
      ;; Make sure it compiles properly
      (is-true (pike-compile ps))

      ;; Get some relevant variables
      (let* ((ckb (pike-constraint-knowledge-base ps)))

        ;; Test probability computations!
        (is  (= (/ 99 100)
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list ))))))))


(test riker-adaptive-human-simple-infdiag-adverserial-1
  ;; Possibly examine multiple different methods of computing
  ;; the probabilities
  (dolist (cc-type `(:cc-wmc-bdd :cc-wmc-ab-bdd))
    (let ((ps (setup-adaptive-human-simple-infdiag-adverserial-1 :constraint-knowledge-base-type cc-type)))
      ;; Make sure it compiles properly
      (is-true (pike-compile ps))

      ;; Get some relevant variables
      (let* ((ckb (pike-constraint-knowledge-base ps)))

        ;; Test probability computations!
        (is  (= (/ 1 100)
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list ))))))))
