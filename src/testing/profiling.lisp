;;;; Copyright (c) 2015 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; A simple, light-weight profiler for quickly measuring you long
;; your Lisp code takes!
;;
;; Suppose you have a (non-inlined) method "foo". You can of course call
;; foo like:
;;       (foo args ...)
;;
;;
;; To engage profiling, simply first create a database to store results:
;;       (setf db (make-instance 'profiler-db)
;;
;; Then for each function you want to profile:
;;      (profiler-engage 'foo db)
;;
;; That will re-write the function referred to by the 'foo symbol to a new
;; version that takes the same arguments, returns the same values, but
;; now stores profiling data.
;;
;; There are also methods below to disengage profiling and retrieve the
;; results.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(in-package #:pike/test)

(defclass profiler-db ()
  ((profiled-symbols
    :type hash-table
    :accessor profiled-symbols
    :initform (make-hash-table :test #'eql)
    :documentation "A hash table mapping symbols to their original functions")
   (profiled-data
    :type hash-table
    :accessor profiled-data
    :initform (make-hash-table :test #'eql)
    :documentation "Hash table mapping function symbol to list of times"))
  (:documentation "Records information for a profiler db"))

(defmethod print-object ((db profiler-db) s)
  "Pretty print"
  (with-slots (profiled-symbols) db
    (format s "#<PROFILER-DB profiling ~a function~p>" (hash-table-count profiled-symbols) (hash-table-count profiled-symbols))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Engaging / disengaging profiling
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun profiler-engage (fn-symbol db)
  "Engage profiling for the given symbol. Note that while attempts are made
   to minimize measurement overhead, results could be off for extremely fast
   methods."
  (declare #+:sbcl (sb-ext:muffle-conditions sb-ext:compiler-note))
  (declare (type symbol fn-symbol)
           (type profiler-db db))
  (with-slots (profiled-symbols profiled-data) db
    ;; If it's already profiled just return
    (when (is-profiled? fn-symbol db)
      (error "Function is already profiled!"))
    ;; Set up profiling
    (let ((fn (symbol-function fn-symbol))
          (data (list)))
      ;; Store the original function referred to by this symbol
      (setf (gethash fn-symbol profiled-symbols) fn)
      ;; Store a referece to the data (which will be updated via closure below)
      (setf (gethash fn-symbol profiled-data) data)
      ;; Now, override it to a custom profiling function
      (setf (symbol-function fn-symbol)
            #'(lambda (&rest args)
                (declare (optimize (speed 3)))
                ;; Call the original function and get the results
                (let* (signaled-error
                       (time-start-raw (get-internal-run-time)) ; get-internal-real-time ?
                       (results (handler-case (multiple-value-list (apply fn args))
                                  (error (e)
                                    (setf signaled-error e))))
                       (time-end-raw (get-internal-run-time))
                       (t_delta (float (/ (- time-end-raw time-start-raw) internal-time-units-per-second))))
                  ;; Update profiling data.
                  (push t_delta (gethash fn-symbol profiled-data))
                  ;; Return the same results as the original fn, or throw an error.
                  (if signaled-error
                      (error signaled-error)
                      (apply #'values results))))))
    T))


(defun profiler-disengage (fn-symbol db)
  "Disengage profiling on the given symbol."
  (declare (type symbol fn-symbol)
           (type profiler-db db))
  (with-slots (profiled-symbols profiled-data) db
    ;; Make sure this function is actually profiled
    (unless (is-profiled? fn-symbol db)
      (error "The given function isn't profiled."))
    ;; Disengage profiling by restoring original function to symbol
    (setf (symbol-function fn-symbol) (gethash fn-symbol profiled-symbols))
    ;; Remove associated entry and data
    (remhash fn-symbol profiled-symbols)
    (remhash fn-symbol profiled-data)))


(defun profiler-disengage-all (db)
  "Helper function to disengage all profiling"
  (declare (type profiler-db db))
  (with-slots (profiled-symbols) db
    (maphash #'(lambda (fn-symbol fn)
                 (declare (ignore fn))
                 (profiler-disengage fn-symbol db))
             profiled-symbols)))


(defun is-profiled? (fn-symbol db)
  "Helper function that returns T if this function is profiled, nil otherwise."
  (declare (type symbol fn-symbol)
           (type profiler-db db))
  (with-slots (profiled-symbols) db
    (let (fn found?)
      (multiple-value-setq (fn found?) (gethash fn-symbol profiled-symbols))
      found?)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Accessing / clearing profiled data
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun profiler-get-raw-data (fn-symbol db)
  "Retrieve the raw profiled data. This is a list of execution times for all profiled runs."
  (declare (type symbol fn-symbol)
           (type profiler-db db))
  (with-slots (profiled-data) db
    (let (data found?)
      (multiple-value-setq (data found?) (gethash fn-symbol profiled-data))
      (unless found?
        (error "This function is not profiled!"))
      data)))


(defun profiler-get-count (fn-symbol db)
  "Retrieve the number of times the given function was called."
  (declare (type symbol fn-symbol)
           (type profiler-db db))
  (let ((data (profiler-get-raw-data fn-symbol db)))
    (length data)))


(defun profiler-get-average (fn-symbol db)
  "Retrieve the average time measurement for the given function"
  (declare (type symbol fn-symbol)
           (type profiler-db db))
  (let* ((data (profiler-get-raw-data fn-symbol db))
         (N (length data)))
    ;; Prevent divide-by-zeros
    (when (= N 0)
      (error "No data available - undefined average!"))
    ;; Return the average as a float
    (float (/ (reduce #'+ data) N))))


(defun profiler-get-stats (fn-symbol db)
  "Calculate various statistics and return as a hash table."
  (declare (type symbol fn-symbol)
           (type profiler-db db))
  (compute-stats (profiler-get-raw-data fn-symbol db)))


(defun compute-stats (data)
  "Helper method to compute & return stats"
  (let* ((stats (make-hash-table))
         (N (length data))
         u)
    ;; N
    (setf (gethash :N stats) N)
    (unless (= N 0)
      ;; Mean
      (setf u (float (/ (reduce #'+ data) N)))
      (setf (gethash :mean stats) u)
      ;; Std. dev
      (setf (gethash :stddev stats)
            (float (sqrt (/ (reduce #'+ (mapcar #'(lambda (x)
                                                   (* (- x u) (- x u)))
                                               data))
                            N))))
      ;; Min
      (setf (gethash :min stats)
            (reduce #'min data))
      ;; Max
      (setf (gethash :max stats)
            (reduce #'max data)))
    stats))


(defun profiler-clear-data-for-function (fn-symbol db)
  "Clear data for the given function"
  (declare (type symbol fn-symbol)
           (type profiler-db db))
  (with-slots (profiled-data) db
    ;; Make sure it's profiled
    (unless (is-profiled? fn-symbol db)
      (error "Function isn't profiled!"))
    ;; Reset profiled data
    (setf (gethash fn-symbol profiled-data) (list))))


(defun profiler-clear-all-data (db)
  "Clear data for all functions"
  (declare (type profiler-db db))
  (with-slots (profiled-symbols) db
    (maphash #'(lambda (fn-symbol fn)
                 (declare (ignore fn))
                 (profiler-clear-data-for-function fn-symbol db))
             profiled-symbols)))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; A helpful profiler macro, separate from
;; all of the above
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defmacro measure-time (&rest body)
  "Return a float corresponding to the time to execute body.
   Discards the return value.

   Use as below:
   (let (time)

       (with-measured-time (time)
          ...)

       (print t))
"
  (let ((time-start-raw-var (gensym))
        (time-end-raw-var (gensym)))
    `(let (,time-start-raw-var ,time-end-raw-var)
       (setf ,time-start-raw-var (get-internal-real-time)) ; get-internal-real-time ? get-internal-run-time
       ,@body
       (setf ,time-end-raw-var (get-internal-real-time))
       (float (/ (- ,time-end-raw-var ,time-start-raw-var) internal-time-units-per-second)))))


(defmacro with-measured-time (fields &rest body)
  "Return a float corresponding to the time to execute body.

   Use as below:
   (let (time)

       (with-measured-time (time)
          ...)

       (print t))
"
  (let ((time-var (first fields))
        (time-start-raw-var (gensym))
        (time-end-raw-var (gensym))
        (results (gensym)))
    `(let (,time-start-raw-var ,time-end-raw-var ,results)
       (setf ,time-start-raw-var (get-internal-run-time)) ; get-internal-real-time ?
       (setf ,results (multiple-value-list ,@body))
       (setf ,time-end-raw-var (get-internal-run-time))
       (setf ,time-var (float (/ (- ,time-end-raw-var ,time-start-raw-var) internal-time-units-per-second)))
       (apply #'values ,results))))
