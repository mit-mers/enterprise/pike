;;;; Copyright (c) 2016 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike/test)

(def-suite :tests-simplify-constraints
    :description "Constraint simplification tests for Pike"
    :in :tests-pike)

(in-suite :tests-simplify-constraints)


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Conjunctions
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;
(test constraints-simplify-conjunction
  "Tests basic constraint simplification."
  (let* ((ss (make-state-space-bits))
         (x (make-instance 'decision-variable
                           :name "x"
                           :domain (list 1 2 3)))
         (y (make-instance 'decision-variable
                           :name "y"
                           :domain (list 1 2 3)))
         (z (make-instance 'decision-variable
                           :name "z"
                           :domain (list 1 2 3))))

    (add-variable! ss x)
    (add-variable! ss y)
    (add-variable! ss z)

    ;; Removing True's
    (let* ((c (make-constraint `(and (= ,x 1)
                                     true
                                     (= ,y 1))))
           (c-simpl (simplify-constraint! c)))
      (is (typep c-simpl 'conjunction-constraint))
      (is  (equalp "(x=1 ∧ y=1)" (format nil "~a" c-simpl))))

    ;; Dominating False's
    (let* ((c (make-constraint `(and (= ,x 1)
                                     false
                                     (= ,y 1))))
           (c-simpl (simplify-constraint! c)))
      (is (typep c-simpl 'constant-constraint))
      (is  (equalp "False" (format nil "~a" c-simpl))))

    ;; Merging in subconjuncts
    (let* ((c (make-constraint `(and (= ,x 1)
                                     (and (= ,y 1)
                                          (and (= ,z 1))))))
           (c-simpl (simplify-constraint! c)))
      (is (typep c-simpl 'conjunction-constraint))
      (is (equalp "(z=1 ∧ y=1 ∧ x=1)" (format nil "~a" c-simpl))))

    ;; Single conjunct
    (let* ((c (make-constraint `(and (= ,x 1)
                                     true)))
           (c-simpl (simplify-constraint! c)))
      (is (typep c-simpl 'assignment-constraint))
      (is (equalp "x=1" (format nil "~a" c-simpl))))

    ;; Empty conjuncts
    (let* ((c (make-constraint `(and )))
           (c-simpl (simplify-constraint! c)))
      (is (typep c-simpl 'constant-constraint))
      (is (equalp "True" (format nil "~a" c-simpl))))))


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Disjunctions
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;
 (test constraints-simplify-disjunction
   "Tests basic constraint simplification."
   (let* ((ss (make-state-space-bits))
         (x (make-instance 'decision-variable
                           :name "x"
                           :domain (list 1 2 3)))
         (y (make-instance 'decision-variable
                           :name "y"
                           :domain (list 1 2 3)))
         (z (make-instance 'decision-variable
                           :name "z"
                           :domain (list 1 2 3))))

    (add-variable! ss x)
    (add-variable! ss y)
    (add-variable! ss z)

    ;; Removing False's
    (let* ((c (make-constraint `(or (= ,x 1)
                                    false
                                    (= ,y 1))))
           (c-simpl (simplify-constraint! c)))
      (is (typep c-simpl 'disjunction-constraint))
      (is  (equalp "(x=1 ∨ y=1)" (format nil "~a" c-simpl))))

    ;; Dominating True's
    (let* ((c (make-constraint `(or (= ,x 1)
                                    true
                                    (= ,y 1))))
           (c-simpl (simplify-constraint! c)))
      (is (typep c-simpl 'constant-constraint))
      (is (equalp "True" (format nil "~a" c-simpl))))

    ;; Merging in subdisjuncts
    (let* ((c (make-constraint `(or (= ,x 1)
                                    (or (= ,y 1)
                                        (or (= ,z 1))))))
           (c-simpl (simplify-constraint! c)))
      (is (typep c-simpl 'disjunction-constraint))
      (is (equalp "(z=1 ∨ y=1 ∨ x=1)" (format nil "~a" c-simpl))))

    ;; Single disjunct
    (let* ((c (make-constraint `(or (= ,x 1)
                                    false)))
           (c-simpl (simplify-constraint! c)))
      (is (typep c-simpl 'assignment-constraint))
      (is (equalp "x=1" (format nil "~a" c-simpl))))

    ;; Empty disjuncts
    (let* ((c (make-constraint `(or)))
           (c-simpl (simplify-constraint! c)))
      (is (typep c-simpl 'constant-constraint))
      (is (equalp "False" (format nil "~a" c-simpl))))))


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Implications
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;
 (test constraints-simplify-implication
   "Tests basic constraint simplification."
   (let* ((ss (make-state-space-bits))
         (x (make-instance 'decision-variable
                           :name "x"
                           :domain (list 1 2 3)))
         (y (make-instance 'decision-variable
                           :name "y"
                           :domain (list 1 2 3)))
         (z (make-instance 'decision-variable
                           :name "z"
                           :domain (list 1 2 3))))

     ;; True => X becomes X
     (let* ((c (make-constraint `(=> true
                                     (= ,x 1))))
           (c-simpl (simplify-constraint! c)))
      (is (typep c-simpl 'assignment-constraint))
      (is (equalp "x=1" (format nil "~a" c-simpl))))

    ;; False => X becomes True
     (let* ((c (make-constraint `(=> false
                                     (= ,x 1))))
           (c-simpl (simplify-constraint! c)))
      (is (typep c-simpl 'constant-constraint))
      (is (equalp "True" (format nil "~a" c-simpl))))

    ;; X => True becomes True
     (let* ((c (make-constraint `(=> (= ,x 1)
                                     true)))
           (c-simpl (simplify-constraint! c)))
      (is (typep c-simpl 'constant-constraint))
      (is  (equalp "True" (format nil "~a" c-simpl))))


    ;; X => False becomes not(X)
     (let* ((c (make-constraint `(=> (= ,x 1)
                                     false)))
           (c-simpl (simplify-constraint! c)))
      (is (typep c-simpl 'negation-constraint))
      (is (equalp "¬(x=1)" (format nil "~a" c-simpl))))

    ;; X => Y remains unchanged
     (let* ((c (make-constraint `(=> (= ,x 1)
                                     (= ,y 1))))
           (c-simpl (simplify-constraint! c)))
      (is (typep c-simpl 'implication-constraint))
      (is (equalp "(x=1 ⇒ y=1)" (format nil "~a" c-simpl))))))


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Equivalence
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;
 (test constraints-simplify-equivalence
   "Tests basic constraint simplification."
   (let* ((ss (make-state-space-bits))
         (x (make-instance 'decision-variable
                           :name "x"
                           :domain (list 1 2 3)))
         (y (make-instance 'decision-variable
                           :name "y"
                           :domain (list 1 2 3)))
         (z (make-instance 'decision-variable
                           :name "z"
                           :domain (list 1 2 3))))

     ;; X <=> False becomes (not X)
     (let* ((c (make-constraint `(<=> (= ,x 1)
                                      false)))
            (c-simpl (simplify-constraint! c)))
       (is (typep c-simpl 'negation-constraint))
       (is (equalp "¬(x=1)" (format nil "~a" c-simpl))))

     ;; X <=> True becomes X
     (let* ((c (make-constraint `(<=> (= ,x 1)
                                      true)))
            (c-simpl (simplify-constraint! c)))
       (is (typep c-simpl 'assignment-constraint))
       (is (equalp "x=1" (format nil "~a" c-simpl))))

    ;; X <=> X becomes True
     (let* ((c (make-constraint `(<=> (= ,x 1)
                                      (= ,x 1))))
           (c-simpl (simplify-constraint! c)))
      (is (typep c-simpl 'constant-constraint))
      (is (equalp "True" (format nil "~a" c-simpl))))

    ;; X <=> (not X) becomes True
     (let* ((c (make-constraint `(<=> (= ,x 1)
                                      (¬ (= ,x 1)))))
           (c-simpl (simplify-constraint! c)))
      (is (typep c-simpl 'constant-constraint))
      (is  (equalp "False" (format nil "~a" c-simpl))))))



;; ;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Negations
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;
(test constraints-simplify-negation
  "Tests basic constraint simplification."
  (let* ((ss (make-state-space-bits))
         (x (make-instance 'decision-variable
                           :name "x"
                           :domain (list 1 2 3)))
         (y (make-instance 'decision-variable
                           :name "y"
                           :domain (list 1 2 3)))
         (z (make-instance 'decision-variable
                           :name "z"
                           :domain (list 1 2 3))))

    ;; Negation of a constant
    (let* ((c (make-constraint `(not true)))
           (c-simpl (simplify-constraint! c)))
      (is (typep c-simpl 'constant-constraint))
      (is (equalp "False" (format nil "~a" c-simpl))))


    ;; Negation of a negation
    (let* ((c (make-constraint `(not (not (= ,x 1)))))
           (c-simpl (simplify-constraint! c)))
      (is (typep c-simpl 'assignment-constraint))
      (is (equalp "x=1" (format nil "~a" c-simpl))))


    ;; Unchanged
    (let* ((c (make-constraint `(not (= ,x 1))))
           (c-simpl (simplify-constraint! c)))
      (is (typep c-simpl 'negation-constraint))
      (is (equalp "¬(x=1)" (format nil "~a" c-simpl))))))


(test constraints-sexp-to-constraint ()
      "Tests conversion from s-expressions to contraints"
      (let* ((x (make-instance 'decision-variable
                               :name "x"
                               :domain (list 1 2 3)))
             (y (make-instance 'decision-variable
                               :name "y"
                               :domain (list 1 2 3)))
             (z (make-instance 'decision-variable
                               :name "z"
                               :domain (list 1 2 3)))

             (constraint (make-constraint `(=> (= ,z 1)
                                               (∧ (= ,x 1)
                                                  (= ,y 1))))))

        (is (equalp "(z=1 ⇒ (x=1 ∧ y=1))" (format nil "~a" constraint)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Test constraint hashing and equality checking
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(test constraints-hashing-and-equality ()
      "Tests conversion from s-expressions to contraints"
      (let* ((x (make-instance 'decision-variable
                               :name "x"
                               :domain (list 1 2 3)))
             (y (make-instance 'decision-variable
                               :name "y"
                               :domain (list 1 2 3)))
             (z (make-instance 'decision-variable
                               :name "z"
                               :domain (list 1 2 3)))

             (c1 (make-constraint `(=> (= ,z 1)
                                       (∨ (= ,x 1)
                                          (∧ (= ,y 1)
                                             (= ,x 2))))))

             (c2 (make-constraint `(=> (= ,z 1)
                                       (∨ (= ,x 1)
                                          (∧ (= ,x 2)
                                             (= ,y 1))))))

             (c3 (make-constraint `(=> (= ,z 2)
                                       (∨ (= ,x 1)
                                          (∧ (= ,x 2)
                                             (= ,y 1)))))))


        (is (equal-constraints c1 c2))
        (is (= (hash-constraint c1) (hash-constraint c2)))

        (is (not (equal-constraints c1 c3)))))






;; TODO: Move to separate file
(defun test-prime-implicants ()
  (let* ((ss (make-state-space-bits))
         (x (make-instance 'decision-variable
                           :name "x"
                           :domain (list 1 2)))
         (y (make-instance 'decision-variable
                           :name "y"
                           :domain (list 1 2)))
         (z (make-instance 'decision-variable
                           :name "z"
                           :domain (list 1 2)))
         (sp1 (make-instance 'decision-variable
                             :name "sp1"
                             :domain (list 1 2 "⊥")))
         ;; (sp2 (make-instance 'decision-variable
         ;;                     :name "sp2"
         ;;                     :domain (list 2 "⊥")))

         ;; (sp1 (make-instance 'decision-variable
         ;;                     :name "sp1"
         ;;                     :domain (list 0 1)))
         ;; (sp2 (make-instance 'decision-variable
         ;;                     :name "sp2"
         ;;                     :domain (list 0 1)))
         ;; (sp3 (make-instance 'decision-variable
         ;;                     :name "sp3"
         ;;                     :domain (list 0 1)))

         (ckb (make-instance 'pike::constraint-knowledge-base-atms
                             :ss ss
                             :atms-type :piatms)))

    (add-variable! ss x)
    (add-variable! ss y)
    (add-variable! ss z)
    (add-variable! ss sp1)
    ;; (add-variable! ss sp2)
    ;; (add-variable! ss sp3)

    (pike::setup-variable-constraints ckb)

    ;; (pike::encode-constraint! ckb
    ;;                           (make-constraint `(=> (= ,z 1)
    ;;                                                 (∨ (= ,sp3 1)
    ;;                                                    (∧ (= ,sp1 1)
    ;;                                                       (= ,y 1))))))

       (pike::encode-constraint! ckb
                              (make-constraint `(=> (= ,z 1)
                                                    (∨ (= ,sp1 2)
                                                       (∧ (= ,sp1 1)
                                                          (= ,y 1))))))


    (pike::encode-constraint! ckb
                              (make-constraint `(¬ (= ,sp1 2))))

    ;; (pike::encode-constraint! ckb
    ;;                           (make-constraint `(∨ (= ,x 1) (= ,y 1) (= ,z 1))))

    ;; (pike::encode-constraint! ckb
    ;;                           (make-constraint `(=> (= ,x 1) (= ,sp1 1))))

    ;; (pike::encode-constraint! ckb
    ;;                           (make-constraint `(=> (= ,y 1) (= ,sp1 1))))

    ;; (pike::encode-constraint! ckb
    ;;                           (make-constraint `(=> (= ,z 1) (= ,sp1 1))))



    ;; (pike::encode-constraint! ckb
    ;;                           (make-constraint `(=> (¬ (= ,sp1 1))
    ;;                                                 (¬ (= ,sp3 1)))))

    ;; (pike::encode-constraint! ckb
    ;;                           (make-constraint `(=> (= ,z 2)
    ;;                                                 (∨ (∧ (= ,sp2 1)
    ;;                                                       (= ,y 1))))))

    ;; (pike::encode-constraint! ckb
    ;;                           (make-constraint `(=> (= ,z 1)
    ;;                                                 (∨ (= ,sp1 3)
    ;;                                                    (∧ (= ,sp1 1)
    ;;                                                       (= ,y 1))))))

    ;; (pike::encode-constraint! ckb
    ;;                           (make-constraint `(=> (¬ (= ,sp1 1))
    ;;                                                 (¬ (= ,sp1 3)))))

    ;; (pike::encode-constraint! ckb
    ;;                           (make-constraint `(=> (= ,z 2)
    ;;                                                 (∨ (∧ (= ,sp2 2)
    ;;                                                       (= ,y 1))))))


    (process-constraints ckb)

    ;; Print solutions
    (print-solutions ckb)))
