;;;; Copyright (c) 2015 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)

(in-package #:pike/test)

(def-suite :tests-strong-controllability
    :description "Strong controllability-related tests for Pike"
    :in :tests-pike)

(in-suite :tests-strong-controllability)




(defun setup-strong-controllability-uncontrollable-1 ()
  (let (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "strong-controllability/test2/uncontrollable.tpn.out")
                            :domain-filename (pike-test-file "strong-controllability/test2/simple-domain.pddl")
                            :problem-filename (pike-test-file "strong-controllability/test2/simple-problem.pddl")
                            :debug-mode T))
    (setf ps (create-pike-session po))
    ps))

(test strong-controllability-uncontrollable-1
      "Tests strong controllability for a plan with no uncontrollable durations.
      Note that the dispatch times for this shouldn't really make much sense, since no
      extra constraints are added. Dispatch isn't supposed to make sense here."
      (let ((ps (setup-strong-controllability-uncontrollable-1)))
        ;; Make sure it compiles properly
        (is-true (pike-compile ps))
        ;; Dispatch it, and check various execution times
        (run-pike-with-sim-dt ps)
        ;; Check some execution times
        (is-true (event-executed-in-window? ps "ev1" 0.0 0.1))
        (is-true (event-executed-in-window? ps "ev2" 5.0 5.1))))
