;;;; Copyright (c) 2011-2015 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike/test)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Various methods to aid in testing
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun pike-test-file (filename-relative)
  "Get a test case file relative to Pike's package"
  (asdf:system-relative-pathname :pike (format nil "test-cases/~a" filename-relative)))


(defun test-pike-with-sim (po)
  "Test Pike in a real-time test, both compiling & executing."
  (let* ((ps (create-pike-session po))
         (sim (make-instance 'simulator-realtime :pike-session ps)))
    (unwind-protect
         (progn
           (setf (pike-time-fn po) (sim-get-time-fn sim))
           (pike-register-hook ps :activity-dispatch (sim-get-dispatch-fn sim))

           (pike-compile ps)
           (pike-execute ps))

      ;; Cleanup the simulator.
      (sim-cleanup sim))
    ps))



(defgeneric pike-termination-status (ps))
(defmethod pike-termination-status ((ps pike-session))
  "Helper function to return Pike's termination status"
  (execution-context-status (pike-execution-context ps)))


(defgeneric execution-succeeded? (ps))
(defmethod execution-succeeded? ((ps pike-session))
  "Helper method that returns T if execution completed successfully, nil
  otherwise."
  (eql (pike-termination-status ps) :complete))


(defgeneric execution-failed? (ps))
(defmethod execution-failed? ((ps pike-session))
  "Helper method that returns T if execution completed failed, nil
  otherwise."
  (eql (pike-termination-status ps) :failed))


(defgeneric event-execution-time (ps event-name))
(defmethod event-execution-time ((ps pike-session) event-name)
  "Returns the time at which this event was executed, or nil if it wasn't"
  (with-slots (execution-context) ps
    (with-slots (event->executed-time) execution-context
      ;; Retrieve the event of interest
      (let ((e (find-event ps event-name))
            time found?)
        (multiple-value-setq (time found?) (gethash e event->executed-time))
        ;; If the event wasn't scheduled, return nil
        (unless found?
          (return-from event-execution-time nil))
        ;; Otherwise, return the time
        time))))


(defgeneric event-executed-in-window? (ps event-name lb ub))
(defmethod event-executed-in-window? ((ps pike-session) event-name lb ub)
  "Returns T if the given event was executed and was scheduled
  within the given time window w.r.t. to the start of the plan, nil otherwise."
  (let ((time (event-execution-time ps event-name)))
    ;; Make sure the event was executed
    (unless time
      (return-from event-executed-in-window? nil))
    (and (>= time lb)
         (<= time ub))))


(defgeneric event-executed-after-event? (ps event-last-name event-first-name))
(defmethod event-executed-after-event? ((ps pike-session) event-last-name event-first-name)
  "Returns T iif event-last was scheduled strictly after event-first, otherwise nil"
  (let ((time-first (event-execution-time ps event-first-name))
        (time-last (event-execution-time ps event-last-name)))
    ;; Make sure both were executed
    (when (or (null time-first) (null time-last))
      (return-from event-executed-after-event? nil))
    (> time-last time-first)))


(defgeneric choice-was-made? (ps variable-name value-name))
(defmethod choice-was-made? ((ps pike-session) variable-name value-name)
  "Returns T if at any point during execution, Pike explicitly committed to making
   the given choice, and nil if otherwise."
  (with-slots (execution-context) ps
    (with-slots (assignment->commit-time) execution-context
      (let ((assignment (find-variable-assignment ps variable-name value-name))
            time found?)
        (multiple-value-setq (time found?) (gethash assignment assignment->commit-time))
        found?))))
