;;;; Copyright (c) 2018 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike/test)

(def-suite :tests-cc-unobserved-choices
    :description "Tests for Riker, when not all uncontrollable choices get to be observed."
    :in :tests-pike)

(in-suite :tests-cc-unobserved-choices)


(defmethod setup-unobserved-choices-1 (&key (constraint-knowledge-base-type :cc-wmc-bdd) (memoize-constraint-calls nil))
  (let (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "unobserved-choices/unobserved-choices-2.tpn")
                            :domain-filename (pike-test-file "unobserved-choices/domain.pddl")
                            :problem-filename (pike-test-file "unobserved-choices/problem.pddl")
                            :probability-distribution (pike-test-file "unobserved-choices/bn-2.xmlbif")
                            :chance-constraint 0.95
                            :debug-mode T
                            :memoize-constraint-calls memoize-constraint-calls ;; nil for testing
                            :constraint-knowledge-base-type constraint-knowledge-base-type))
    (setf ps (create-pike-session po))
    ps))

(defmethod setup-unobserved-choices-2 (&key (constraint-knowledge-base-type :cc-wmc-bdd) (memoize-constraint-calls nil))
  (let (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "unobserved-choices/unobserved-choices.tpn")
                            :domain-filename (pike-test-file "unobserved-choices/domain.pddl")
                            :problem-filename (pike-test-file "unobserved-choices/problem.pddl")
                            :probability-distribution (pike-test-file "unobserved-choices/bn.xmlbif")
                            :chance-constraint 0.95
                            :debug-mode T
                            :memoize-constraint-calls memoize-constraint-calls ;; nil for testing
                            :constraint-knowledge-base-type constraint-knowledge-base-type))
    (setf ps (create-pike-session po))
    ps))

(defmethod setup-unobserved-choices-3 (&key (constraint-knowledge-base-type :cc-wmc-bdd) (memoize-constraint-calls nil))
  (let (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "unobserved-choices/unobserved-choices.tpn")
                            :domain-filename (pike-test-file "unobserved-choices/domain.pddl")
                            :problem-filename (pike-test-file "unobserved-choices/problem.pddl")
                            :probability-distribution (pike-test-file "unobserved-choices/bn-3.xmlbif")
                            :chance-constraint 0.95
                            :debug-mode T
                            :memoize-constraint-calls memoize-constraint-calls ;; nil for testing
                            :constraint-knowledge-base-type constraint-knowledge-base-type))
    (setf ps (create-pike-session po))
    ps))

(defmethod setup-unobserved-choices-4 (&key (constraint-knowledge-base-type :cc-wmc-bdd) (memoize-constraint-calls nil))
  (let (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "unobserved-choices/unobserved-choices-3.tpn")
                            :domain-filename (pike-test-file "unobserved-choices/domain.pddl")
                            :problem-filename (pike-test-file "unobserved-choices/problem.pddl")
                            :probability-distribution (pike-test-file "unobserved-choices/bn-4.xmlbif")
                            :chance-constraint 0.95
                            :debug-mode T
                            :memoize-constraint-calls memoize-constraint-calls ;; nil for testing
                            :constraint-knowledge-base-type constraint-knowledge-base-type))
    (setf ps (create-pike-session po))
    ps))

(defmethod setup-apples-simple-1 (&key (constraint-knowledge-base-type :cc-wmc-bdd) (memoize-constraint-calls nil))
  (let (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "unobserved-choices/apples-simple/unobserved-choices.tpn")
                            :domain-filename (pike-test-file "unobserved-choices/apples-simple/domain.pddl")
                            :problem-filename (pike-test-file "unobserved-choices/apples-simple/problem.pddl")
                            :probability-distribution (pike-test-file "unobserved-choices/apples-simple/infdiag.pgmx")
                            :chance-constraint 0.95
                            :debug-mode T
                            :memoize-constraint-calls memoize-constraint-calls ;; nil for testing
                            :constraint-knowledge-base-type constraint-knowledge-base-type))
    (setf ps (create-pike-session po))
    ps))


(defmethod setup-apples-simple-2 (&key (constraint-knowledge-base-type :cc-wmc-bdd) (memoize-constraint-calls nil))
  (let (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "unobserved-choices/apples-simple/unobserved-choices.tpn")
                            :domain-filename (pike-test-file "unobserved-choices/apples-simple/domain.pddl")
                            :problem-filename (pike-test-file "unobserved-choices/apples-simple/problem.pddl")
                            :probability-distribution (pike-test-file "unobserved-choices/apples-simple/bn.xmlbif")
                            :chance-constraint 0.95
                            :debug-mode T
                            :memoize-constraint-calls memoize-constraint-calls ;; nil for testing
                            :constraint-knowledge-base-type constraint-knowledge-base-type))
    (setf ps (create-pike-session po))
    ps))

(test riker-unobserved-choices-1
  ;; Possibly examine multiple different methods of computing
  ;; the probabilities
  (dolist (cc-type `(:cc-wmc-bdd :cc-wmc-ab-bdd))
    (let ((ps (setup-unobserved-choices-1 :constraint-knowledge-base-type cc-type)))
      ;; Make sure it compiles properly
      (is-true (pike-compile ps))

      ;; Get some relevant variables
      (let* ((ckb (pike-constraint-knowledge-base ps))
             (R1 (find-variable ps "R1"))
             (R2 (find-variable ps "R2"))
             (H1 (find-variable ps "H1"))
             (H2 (find-variable ps "H2")))

        ;; Test probability computations!
        (is  (= 1
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list )
                 :conflicts (list ))))

        (is  (= 1
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment R1 "c1"))
                 :conflicts (list ))))

        (is  (= (/ 2 3)
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment R1 "c2"))
                 :conflicts (list ))))

        (is  (= 1
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment R1 "c1") (assignment H2 "c1") (assignment H1 "c1"))
                 :conflicts (list ))))

        (is  (= 0
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment R1 "c2") (assignment H2 "c1") (assignment H1 "c1"))
                 :conflicts (list ))))

        (is  (= 0
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment R1 "c2") (assignment H2 "∘") (assignment H1 "c1"))
                 :conflicts (list ))))

        (is  (= 0
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment R1 "c1") (assignment H2 "c1") (assignment R2 "c2") (assignment H1 "c1"))
                 :conflicts (list ))))))))



(test riker-unobserved-choices-2
  ;; Possibly examine multiple different methods of computing
  ;; the probabilities
  (dolist (cc-type `(:cc-wmc-bdd :cc-wmc-ab-bdd))
    (let ((ps (setup-unobserved-choices-2 :constraint-knowledge-base-type cc-type)))
      ;; Make sure it compiles properly
      (is-true (pike-compile ps))

      ;; Get some relevant variables
      (let* ((ckb (pike-constraint-knowledge-base ps))
             (R1 (find-variable ps "R1"))
             (G  (find-variable ps "G")))

        ;; Test probability computations!
        (is  (= (/ 1 2)
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list )
                 :conflicts (list ))))

        (is  (= (/ 1 2)
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment R1 "c1"))
                 :conflicts (list ))))

        (is  (= (/ 1 2)
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment R1 "c2"))
                 :conflicts (list ))))

        (is  (= 1
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment R1 "c2") (assignment G "A"))
                 :conflicts (list ))))))))


(test riker-unobserved-choices-3
  ;; Possibly examine multiple different methods of computing
  ;; the probabilities
  (dolist (cc-type `(:cc-wmc-bdd :cc-wmc-ab-bdd))
    (let ((ps (setup-unobserved-choices-3 :constraint-knowledge-base-type cc-type)))
      ;; Make sure it compiles properly
      (is-true (pike-compile ps))

      ;; Get some relevant variables
      (let* ((ckb (pike-constraint-knowledge-base ps))
             (R1 (find-variable ps "R1"))
             (G  (find-variable ps "G")))

        ;; Test probability computations!
        (is  (= (/ 1 2)
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list )
                 :conflicts (list ))))

        (is  (= (/ 1 4)
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment R1 "c1"))
                 :conflicts (list ))))

        (is  (= (/ 1 2)
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment R1 "c2"))
                 :conflicts (list ))))

        (is  (= 1
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment R1 "c2") (assignment G "A"))
                 :conflicts (list ))))))))

(test riker-unobserved-choices-4
  ;; Possibly examine multiple different methods of computing
  ;; the probabilities
  (dolist (cc-type `(:cc-wmc-bdd :cc-wmc-ab-bdd))
    (let ((ps (setup-unobserved-choices-4 :constraint-knowledge-base-type cc-type)))
      ;; Make sure it compiles properly
      (is-true (pike-compile ps))

      ;; Get some relevant variables
      (let* ((ckb (pike-constraint-knowledge-base ps))
             (HQ (find-variable ps "HQ"))
             (R2 (find-variable ps "R2"))
             (H1 (find-variable ps "H1"))
             (H2 (find-variable ps "H2")))

        ;; Test probability computations!
        (is  (= (/ 5 6)
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list )
                 :conflicts (list ))))

        (is  (= 1
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment HQ "c1"))
                 :conflicts (list ))))

        (is  (= (/ 2 3)
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment HQ "c2"))
                 :conflicts (list ))))

        (is  (= 1
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment HQ "c1") (assignment H2 "c1") (assignment H1 "c1"))
                 :conflicts (list ))))

        (is  (= 0
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment HQ "c2") (assignment H2 "c1") (assignment H1 "c1"))
                 :conflicts (list ))))

        (is  (= 0
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment HQ "c2") (assignment H2 "∘") (assignment H1 "c1"))
                 :conflicts (list ))))

        (is  (= 0
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment H2 "c1") (assignment R2 "c1") (assignment H1 "c2"))
                 :conflicts (list ))))))))
