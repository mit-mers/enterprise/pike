;;;; Copyright (c) 2015 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package :pike/test)

(def-suite :tests-profiler
    :description "Testing the profiler"
    :in :tests-pike)

(in-suite :tests-profiler)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Tests basic profiling features
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Set up a dummy function
(defun foo-profiler-dummy (n)
  (let ((x 0))
    (dotimes (i n)
      (incf x))
    x))

(test profiler-1
  ;; Set up a profiling db and test it!
  (let ((db (make-instance 'profiler-db))
        (foo-fn-orig #'foo-profiler-dummy))

    (is (= (foo-profiler-dummy 1000) 1000))          ; Original function works
    (is-false (is-profiled? 'foo-profiler-dummy db)) ; Not profiled yet
    ;; Engage profiling
    (profiler-engage 'foo-profiler-dummy db)
    (is-true (is-profiled? 'foo-profiler-dummy db))   ; Now profiled
    (is-false (eql foo-fn-orig #'foo-profiler-dummy)) ; Should point to a different function
    (is (= 0 (profiler-get-count 'foo-profiler-dummy db)))
    ;; Run a few tests, make sure they return the same result
    (foo-profiler-dummy 1000)
    (foo-profiler-dummy 1000)
    (is (= 1000 (foo-profiler-dummy 1000)))                ; Should return the same value(s)
    (is (= 3 (profiler-get-count 'foo-profiler-dummy db))) ; Should have 3 values
    ;; Remove profiling
    (profiler-disengage-all db)
    (is-false (is-profiled? 'foo-profiler-dummy db))  ; No longer profiled
    (is-true (eql foo-fn-orig #'foo-profiler-dummy)))) ; Make sure the function gets put back
