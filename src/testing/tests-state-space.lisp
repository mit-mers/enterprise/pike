;;;; Copyright (c) 2014-2015 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike/test)

(def-suite :tests-state-space
    :description "State-space-related tests for Pike"
    :in :tests-pike)

(in-suite :tests-state-space)



(test state-space-1
  "Tests creation of a state space and adding variables, and unions."
  (let* ((ss (make-state-space-bits))
         (x (make-instance 'decision-variable
                           :name "x"
                           :domain (list 1 2 3 4)))
         (y (make-instance 'decision-variable
                           :name "y"
                           :domain (list 1 2 3 4)))
         (z (make-instance 'decision-variable
                           :name "z"
                           :domain (list 1 2 3 4)))

         e1
         e2)

    (add-variable! ss x)
    (add-variable! ss y)
    (add-variable! ss z)

    (setf e1 (make-environment-from-assignments ss (list (assignment x 1))))
    (setf e2 (make-environment-from-assignments ss (list (assignment y 2))))

    (is (equal "{x=1, y=2}" (print-environment (environment-union e1 e2 ss) ss nil)))))
