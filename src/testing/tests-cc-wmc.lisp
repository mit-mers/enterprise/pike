;;;; Copyright (c) 2018 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike/test)

(def-suite :tests-cc-wmc
    :description "Tests for using weighed model counting for chance-constrained execution"
    :in :tests-pike)

(in-suite :tests-cc-wmc)

(defmethod setup-morning-routine-1 (&key (constraint-knowledge-base-type :cc-wmc-bdd) (memoize-constraint-calls t))
  (let (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "morning-routine/tpn-morning-routine-processed-1.tpn")
                            :domain-filename (pike-test-file "morning-routine/domain.pddl")
                            :problem-filename (pike-test-file "morning-routine/problem.pddl")
                            :probability-distribution (pike-test-file "morning-routine/infdiag.pgmx") ;; (pike-test-file "morning-routine/morning-routine-1.xmlbif")
                            :chance-constraint 0.95
                            :debug-mode T
                            :memoize-constraint-calls memoize-constraint-calls ;; nil for testing
                            :constraint-knowledge-base-type constraint-knowledge-base-type))
    (setf ps (create-pike-session po))
    ps))


(defmethod setup-morning-routine-2 (&key (constraint-knowledge-base-type :cc-wmc-bdd) (memoize-constraint-calls t))
  (let (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "morning-routine/tpn-morning-routine-processed-pretty-ordered.tpn")
                            :domain-filename (pike-test-file "morning-routine/domain.pddl")
                            :problem-filename (pike-test-file "morning-routine/problem.pddl")
                            :probability-distribution (pike-test-file "morning-routine/morning-routine-1.xmlbif")
                            :chance-constraint 0.9
                            :debug-mode T
                            :memoize-constraint-calls memoize-constraint-calls ;; nil for testing
                            :constraint-knowledge-base-type constraint-knowledge-base-type))
    (setf ps (create-pike-session po))
    ps))


(defmethod setup-desserts-1 (&key (constraint-knowledge-base-type :cc-wmc-bdd) (memoize-constraint-calls t))
  (let (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "desserts/desserts.tpn")
                            :domain-filename (pike-test-file "desserts/domain.pddl")
                            :problem-filename (pike-test-file "desserts/problem.pddl")
                            :probability-distribution (pike-test-file "desserts/bn.xmlbif")
                            :chance-constraint 0.9
                            :debug-mode T
                            :memoize-constraint-calls memoize-constraint-calls ;; nil for testing
                            :constraint-knowledge-base-type constraint-knowledge-base-type))
    (setf ps (create-pike-session po))
    ps))


(defmethod setup-k-scalable-1 (&key (constraint-knowledge-base-type :cc-wmc-bdd))
  (let (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename "plan.tpn"
                            :domain-filename "domain.pddl"
                            :problem-filename "problem.pddl"
                            :probability-distribution "bn.xmlbif"
                            :chance-constraint 1.e-6
                            :debug-mode T
                            :memoize-constraint-calls t ;; nil for testing
                            :constraint-knowledge-base-type constraint-knowledge-base-type))
    (setf ps (create-pike-session po))
    ps))




(test morning-routine-1-cc-various
  ;; Possibly examine multiple different methods of computing
  ;; the probabilities
  (dolist (cc-type `()) ;; :cc-wmc-ddnnf ;; don't really need to test this anymore...
    (let ((ps (setup-morning-routine-1 :constraint-knowledge-base-type cc-type)))
      ;; Make sure it compiles properly
      (is-true (pike-compile ps))

      ;; Get some relevant variables
      (let* ((ss (pike::pike-state-space ps))
             (ckb (pike-constraint-knowledge-base ps))
             (x_weather (find-variable ps "x_weather_1"))
             (x_commute (find-variable ps "x_commute"))
             (x_breakfast (find-variable ps "x_breakfast"))
             (x_snooze (find-variable ps "x_snooze"))
             (x_pump_bike (find-variable ps "x_pump_bike"))
             (x_keys (find-variable ps "x_keys"))
             (x_shoes (find-variable ps "x_shoes"))
             (x_umbrella (find-variable ps "x_umbrella"))
             (s_tires_inflated (find-variable ps "s_{(tires-inflated bicycle),Event_0x7f91923c7f50}")))

        ;; Test probability computations!
        (is  (= 1
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list )
                 :conflicts (list ))))


        (is  (= 1
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment x_pump_bike "yes"))
                 :conflicts (list ))))

        (is  (= (/ 27 40)
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment x_pump_bike "no"))
                 :conflicts (list ))))

        ;; Causal link violated!
        (is (= (/ 27 40)
               (pike::compute-conditional-probability-of-correctness
                ckb
                :assignments-given (list )
                :conflicts (list (make-environment-from-assignments ss (list (assignment s_tires_inflated "Event_0x7f91923a37d0")))))))


        ;; Robot can't make choice yet for keys: neither reaches the 95% chance constraint (more on this later!).
        ;; So for now the robot can wait a bit (in general this might cause temporal failure though if we wait too long...
        ;; might be better sometimes to just take a risk and go?)
        ;; Math: For "no", it means we're not robust to driving a car again. Bayesian update though: P(Commute=car) = 0.0972 now, so 1 - that is 0.903.
        ;; For "yes", we note that we can only make 3 of the 4 choices... and two have been made. That means we can hand the
        ;; person shoes, or give him/her an umbrella, but not both. If we give an umbrella, we're robust to driving and biking but not running.
        ;; If we give shoes, we're robust to driving, biking, and running -- but only in the event that it's sunny outside.
        ;; These incur risk. We also need to take into account the updated probability values (updated from observation).
        ;; We may add as follow P(car | obs) + P(bike, rain | obs) + P(bike, sunny | obs) + P(run, sunny | obs).
        ;; So the only case we have no way of handling is if it's running and rainy out.
        ;; So 1 - P(run, rainy | get_up).  P(run, rainy | get_up) = 0.25*0.1*0.9 / (0.36) = 0.0625. So, the total answer is 1 - 0.0625 = 0.9375.
        (is (= (/ 65 72)
               (pike::compute-conditional-probability-of-correctness
                ckb
                :assignments-given (list (assignment x_pump_bike "yes")
                                         (assignment x_snooze "get_up")
                                         (assignment x_keys "no"))
                :conflicts (list ))))

        (is (= (/ 15 16)
               (pike::compute-conditional-probability-of-correctness
                ckb
                :assignments-given (list (assignment x_pump_bike "yes")
                                         (assignment x_snooze "get_up")
                                         (assignment x_keys "yes"))
                :conflicts (list ))))



        ;; Observe: Human makes eggs for breakfast
        ;; Math: We could already handle 100% of cases before, so this doesn't change anything.
        ;; It will effectively trigger another Bayesian update though: our posterior over commuting method will update.
        (is (= 1
               (pike::compute-conditional-probability-of-correctness
                ckb
                :assignments-given (list (assignment x_pump_bike "yes")
                                         (assignment x_snooze "get_up")
                                         (assignment x_breakfast "eggs"))
                :conflicts (list ))))



        ;; Now can we make the x_keys choice now? Yes we can! The posterior has updated a second time. Choose to NOT give keys.
        ;; Math: The posterior is now as noted below. Choosing yes: by same reasoning above, we have 1 - P(run, rainy | get_up, eggs).
        ;; P(run, rainy, get_up, eggs) = 0.02024, P(get_up, eggs) = 0.296. So answer is 1 - 0.0204 / 0.296 = 0.932.
        ;; If, however we assign keys to no, we can guarantee later that we can both pick up the umbrella and give the shoes.
        ;; We are therefore completely robust to bicycling and running, but not at all to driving.
        ;; The result is therefore the sum of those intents, namely 0.8894 + 0.0988 = 0.988.
        (is (= (/ 1103 1184)
               (pike::compute-conditional-probability-of-correctness
                ckb
                :assignments-given (list (assignment x_pump_bike "yes")
                                         (assignment x_snooze "get_up")
                                         (assignment x_breakfast "eggs")
                                         (assignment x_keys "yes"))
                :conflicts (list ))))


        (is (= (/ 585 592)
               (pike::compute-conditional-probability-of-correctness
                ckb
                :assignments-given (list (assignment x_pump_bike "yes")
                                         (assignment x_snooze "get_up")
                                         (assignment x_breakfast "eggs")
                                         (assignment x_keys "no"))
                :conflicts (list ))))


        ;; And yes, we can make the choice for shoes next.
        ;; Math: Yes to picking up shoes is the same as the above case; we can still completely
        ;; handle bicycling and running, but not driving. So same mathematical value (no observations
        ;; have happened since then). For the no case, that would mean we can only be robust to bicycling,
        ;; which is 0.0988 after previous observation updates.
        (is (= (/ 585 592)
               (pike::compute-conditional-probability-of-correctness
                ckb
                :assignments-given (list (assignment x_pump_bike "yes")
                                         (assignment x_snooze "get_up")
                                         (assignment x_breakfast "eggs")
                                         (assignment x_keys "no")
                                         (assignment x_shoes "yes"))
                :conflicts (list ))))

        (is (= (/ 117 1184)
               (pike::compute-conditional-probability-of-correctness
                ckb
                :assignments-given (list (assignment x_pump_bike "yes")
                                         (assignment x_snooze "get_up")
                                         (assignment x_breakfast "eggs")
                                         (assignment x_keys "no")
                                         (assignment x_shoes "no"))
                :conflicts (list ))))


        ;; Similarly, we can make a choice to give the umbrella.
        ;; Math: Yes: same as before. No: This would mean we're robust to running and biking,
        ;; but only in the case of sunny weather. So we get: P(run, sunny | snooze, eggs) + P(bike, sunny | snooze, eggs)
        ;; = (after math) 0.8209 + 0.0912 = 0.912
        (is (= (/ 585 592)
               (pike::compute-conditional-probability-of-correctness
                ckb
                :assignments-given (list (assignment x_pump_bike "yes")
                                         (assignment x_snooze "get_up")
                                         (assignment x_breakfast "eggs")
                                         (assignment x_keys "no")
                                         (assignment x_shoes "yes")
                                         (assignment x_umbrella "yes")))))

        (is (= (/ 135 148)
               (pike::compute-conditional-probability-of-correctness
                ckb
                :assignments-given (list (assignment x_pump_bike "yes")
                                         (assignment x_snooze "get_up")
                                         (assignment x_breakfast "eggs")
                                         (assignment x_keys "no")
                                         (assignment x_shoes "yes")
                                         (assignment x_umbrella "no")))))


        ;; And finally, we get to observe the actual commute choice:
        ;; Math. At this point given the choices that have been made, we're entirely robust
        ;; to biking and running, and not at all to driving. So it will be 1.0 or 0.0 depending
        ;; on which choice gets made.
        (is (= 1
               (pike::compute-conditional-probability-of-correctness
                ckb
                :assignments-given (list (assignment x_pump_bike "yes")
                                         (assignment x_snooze "get_up")
                                         (assignment x_breakfast "eggs")
                                         (assignment x_keys "no")
                                         (assignment x_shoes "yes")
                                         (assignment x_umbrella "yes")
                                         (assignment x_commute "run")))))

        (is (= 0
               (pike::compute-conditional-probability-of-correctness
                ckb
                :assignments-given (list (assignment x_pump_bike "yes")
                                         (assignment x_snooze "get_up")
                                         (assignment x_breakfast "eggs")
                                         (assignment x_keys "no")
                                         (assignment x_shoes "yes")
                                         (assignment x_umbrella "yes")
                                         (assignment x_commute "car")))))

        (is (= 1
               (pike::compute-conditional-probability-of-correctness
                ckb
                :assignments-given (list (assignment x_pump_bike "yes")
                                         (assignment x_snooze "get_up")
                                         (assignment x_breakfast "eggs")
                                         (assignment x_keys "no")
                                         (assignment x_shoes "yes")
                                         (assignment x_umbrella "yes")
                                         (assignment x_commute "bike")))))


        ;; TODO: Add in some more unit-tests with conflicts given.
        ;; See my note in (compute-conditional-probability-of-sucess ) --
        ;; shoudl test more!

        t)))

  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; Updated to take into account choice ordering!
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  (dolist (cc-type `(:cc-wmc-bdd  :cc-wmc-ab-bdd)) ;;  :patms :cc-wmc-bdd
    (let ((ps (setup-morning-routine-1 :constraint-knowledge-base-type cc-type)))
      ;; Make sure it compiles properly
      (is-true (pike-compile ps))

      ;; Get some relevant variables
      (let* ((ss (pike::pike-state-space ps))
             (ckb (pike-constraint-knowledge-base ps))
             (x_weather (find-variable ps "x_weather_1"))
             (x_commute (find-variable ps "x_commute"))
             (x_breakfast (find-variable ps "x_breakfast"))
             (x_snooze (find-variable ps "x_snooze"))
             (x_pump_bike (find-variable ps "x_pump_bike"))
             (x_keys (find-variable ps "x_keys"))
             (x_shoes (find-variable ps "x_shoes"))
             (x_umbrella (find-variable ps "x_umbrella"))
             (s_tires_inflated (find-variable ps "s_{(tires-inflated bicycle),Event_0x7f91923c7f50}")))

        ;; Test probability computations!
        (is  (= (/ 19 20)
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list )
                 :conflicts (list ))))


        (is  (= (/ 19 20)
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment x_pump_bike "yes"))
                 :conflicts (list ))))

        (is  (= (/ 27 40)
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment x_pump_bike "no"))
                 :conflicts (list ))))

        ;; Causal link violated!
        (is (= (/ 27 40)
               (pike::compute-conditional-probability-of-correctness
                ckb
                :assignments-given (list )
                :conflicts (list (make-environment-from-assignments ss (list (assignment s_tires_inflated "Event_0x7f91923a37d0")))))))


        ;; Robot can't make choice yet for keys: neither reaches the 95% chance constraint (more on this later!).
        ;; So for now the robot can wait a bit (in general this might cause temporal failure though if we wait too long...
        ;; might be better sometimes to just take a risk and go?)
        ;; Math: For "no", it means we're not robust to driving a car again. Bayesian update though: P(Commute=car) = 0.0972 now, so 1 - that is 0.903.
        ;; For "yes", we note that we can only make 3 of the 4 choices... and two have been made. That means we can hand the
        ;; person shoes, or give him/her an umbrella, but not both. If we give an umbrella, we're robust to driving and biking but not running.
        ;; If we give shoes, we're robust to driving, biking, and running -- but only in the event that it's sunny outside.
        ;; These incur risk. We also need to take into account the updated probability values (updated from observation).
        ;; We may add as follow P(car | obs) + P(bike, rain | obs) + P(bike, sunny | obs) + P(run, sunny | obs).
        ;; So the only case we have no way of handling is if it's running and rainy out.
        ;; So 1 - P(run, rainy | get_up).  P(run, rainy | get_up) = 0.25*0.1*0.9 / (0.36) = 0.0625. So, the total answer is 1 - 0.0625 = 0.9375.
        (is (= (/ 65 72)
               (pike::compute-conditional-probability-of-correctness
                ckb
                :assignments-given (list (assignment x_pump_bike "yes")
                                         (assignment x_snooze "get_up")
                                         (assignment x_keys "no"))
                :conflicts (list ))))

        (is (= (/ 67 72)
               (pike::compute-conditional-probability-of-correctness
                ckb
                :assignments-given (list (assignment x_pump_bike "yes")
                                         (assignment x_snooze "get_up")
                                         (assignment x_keys "yes"))
                :conflicts (list ))))



        ;; Observe: Human makes eggs for breakfast
        ;; Math: We could already handle 100% of cases before, so this doesn't change anything.
        ;; It will effectively trigger another Bayesian update though: our posterior over commuting method will update.
        (is (= (/ 585 592)
               (pike::compute-conditional-probability-of-correctness
                ckb
                :assignments-given (list (assignment x_pump_bike "yes")
                                         (assignment x_snooze "get_up")
                                         (assignment x_breakfast "eggs"))
                :conflicts (list ))))



        ;; Now can we make the x_keys choice now? Yes we can! The posterior has updated a second time. Choose to NOT give keys.
        ;; Math: The posterior is now as noted below. Choosing yes: by same reasoning above, we have 1 - P(run, rainy | get_up, eggs).
        ;; P(run, rainy, get_up, eggs) = 0.02024, P(get_up, eggs) = 0.296. So answer is 1 - 0.0204 / 0.296 = 0.932.
        ;; If, however we assign keys to no, we can guarantee later that we can both pick up the umbrella and give the shoes.
        ;; We are therefore completely robust to bicycling and running, but not at all to driving.
        ;; The result is therefore the sum of those intents, namely 0.8894 + 0.0988 = 0.988.
        (is (= (/ 547 592)
               (pike::compute-conditional-probability-of-correctness
                ckb
                :assignments-given (list (assignment x_pump_bike "yes")
                                         (assignment x_snooze "get_up")
                                         (assignment x_breakfast "eggs")
                                         (assignment x_keys "yes"))
                :conflicts (list ))))


        (is (= (/ 585 592)
               (pike::compute-conditional-probability-of-correctness
                ckb
                :assignments-given (list (assignment x_pump_bike "yes")
                                         (assignment x_snooze "get_up")
                                         (assignment x_breakfast "eggs")
                                         (assignment x_keys "no"))
                :conflicts (list ))))


        ;; And yes, we can make the choice for shoes next.
        ;; Math: Yes to picking up shoes is the same as the above case; we can still completely
        ;; handle bicycling and running, but not driving. So same mathematical value (no observations
        ;; have happened since then). For the no case, that would mean we can only be robust to bicycling,
        ;; which is 0.0988 after previous observation updates.
        (is (= (/ 585 592)
               (pike::compute-conditional-probability-of-correctness
                ckb
                :assignments-given (list (assignment x_pump_bike "yes")
                                         (assignment x_snooze "get_up")
                                         (assignment x_breakfast "eggs")
                                         (assignment x_keys "no")
                                         (assignment x_shoes "yes"))
                :conflicts (list ))))

        (is (= (/ 117 1184)
               (pike::compute-conditional-probability-of-correctness
                ckb
                :assignments-given (list (assignment x_pump_bike "yes")
                                         (assignment x_snooze "get_up")
                                         (assignment x_breakfast "eggs")
                                         (assignment x_keys "no")
                                         (assignment x_shoes "no"))
                :conflicts (list ))))


        ;; Similarly, we can make a choice to give the umbrella.
        ;; Math: Yes: same as before. No: This would mean we're robust to running and biking,
        ;; but only in the case of sunny weather. So we get: P(run, sunny | snooze, eggs) + P(bike, sunny | snooze, eggs)
        ;; = (after math) 0.8209 + 0.0912 = 0.912
        (is (= (/ 585 592)
               (pike::compute-conditional-probability-of-correctness
                ckb
                :assignments-given (list (assignment x_pump_bike "yes")
                                         (assignment x_snooze "get_up")
                                         (assignment x_breakfast "eggs")
                                         (assignment x_keys "no")
                                         (assignment x_shoes "yes")
                                         (assignment x_umbrella "yes")))))

        (is (= (/ 135 148)
               (pike::compute-conditional-probability-of-correctness
                ckb
                :assignments-given (list (assignment x_pump_bike "yes")
                                         (assignment x_snooze "get_up")
                                         (assignment x_breakfast "eggs")
                                         (assignment x_keys "no")
                                         (assignment x_shoes "yes")
                                         (assignment x_umbrella "no")))))


        ;; And finally, we get to observe the actual commute choice:
        ;; Math. At this point given the choices that have been made, we're entirely robust
        ;; to biking and running, and not at all to driving. So it will be 1.0 or 0.0 depending
        ;; on which choice gets made.
        (is (= 1
               (pike::compute-conditional-probability-of-correctness
                ckb
                :assignments-given (list (assignment x_pump_bike "yes")
                                         (assignment x_snooze "get_up")
                                         (assignment x_breakfast "eggs")
                                         (assignment x_keys "no")
                                         (assignment x_shoes "yes")
                                         (assignment x_umbrella "yes")
                                         (assignment x_commute "run")))))

        (is (= 0
               (pike::compute-conditional-probability-of-correctness
                ckb
                :assignments-given (list (assignment x_pump_bike "yes")
                                         (assignment x_snooze "get_up")
                                         (assignment x_breakfast "eggs")
                                         (assignment x_keys "no")
                                         (assignment x_shoes "yes")
                                         (assignment x_umbrella "yes")
                                         (assignment x_commute "car")))))

        (is (= 1
               (pike::compute-conditional-probability-of-correctness
                ckb
                :assignments-given (list (assignment x_pump_bike "yes")
                                         (assignment x_snooze "get_up")
                                         (assignment x_breakfast "eggs")
                                         (assignment x_keys "no")
                                         (assignment x_shoes "yes")
                                         (assignment x_umbrella "yes")
                                         (assignment x_commute "bike")))))


        ;; TODO: Add in some more unit-tests with conflicts given.
        ;; See my note in (compute-conditional-probability-of-sucess ) --
        ;; should test more!

        t))))




(test morning-routine-1-cc-online-various
  ;; Updated to now take into account choice ordering
  (dolist (cc-type `(:cc-wmc-bdd :cc-wmc-ab-bdd)) ;;   :patms :cc-wmc-bdd
    (let* ((ps (setup-morning-routine-1 :constraint-knowledge-base-type cc-type
                                        :memoize-constraint-calls t))
           (disturbances `((:at-time 0.01 :sample-probability ,#'(lambda (p) (is-true (= (/ 19 20) p))))
                           (:at-time 1.00 :commit (("x_snooze" "get_up")))
                           (:at-time 1.01 :sample-probability ,#'(lambda (p) (is-true (= (/ 67 72) p))))
                           (:at-time 2.00 :commit (("x_breakfast" "eggs")))
                           (:at-time 2.01 :sample-probability ,#'(lambda (p) (is-true (= (/ 585 592) p))))
                           (:at-time 4.01 :sample-probability ,#'(lambda (p) (is-true (= (/ 585 592) p))))
                           (:at-time 10.70 :commit (("x_commute" "run")))
                           (:at-time 10.71 :sample-probability ,#'(lambda (p) (is-true (= 1 p))))
                           ;;(:at-time 14.00 :commit (("x_weather_1" "sunny"))) ;; Breaks phantom lockout constraints!
                           (:at-time 14.00 :commit (("x_weather_2" "sunny"))))))
      ;; Make sure it compiles properly
      (pike-compile ps)

      ;; Dispatch it, and check various execution times
      (run-pike-with-sim-dt ps :disturbances disturbances)

      ;; Was it successful?
      (is-true (not (execution-failed? ps)))

      ;; Were the correct choices made?
      (is-true (choice-was-made? ps "x_pump_bike" "yes"))
      (is-true (choice-was-made? ps "x_keys" "no"))
      (is-true (choice-was-made? ps "x_shoes" "yes"))
      (is-true (choice-was-made? ps "x_umbrella" "yes")))))


;; Test computing marginals conditioned on observations (useful for question asking)
(test morning-routine-1-cc-marginals-various
  (dolist (cc-type `(:cc-wmc-bdd :cc-wmc-ab-bdd :cc-wmc-ddnnf)) ;;    :patms

    (let* ((ps (setup-morning-routine-1 :constraint-knowledge-base-type cc-type
                                        :memoize-constraint-calls t))
           ckb)

      (pike-compile ps)
      (setf ckb (pike-constraint-knowledge-base ps))

      ;; With no conditioned history
      (is-true (= (/ 13 40) (compute-conditional-marginal-probability-of-uncontrollable-choice ckb (find-variable-assignment ps "x_commute" "run"))))
      (is-true (= (/ 13 40) (compute-conditional-marginal-probability-of-uncontrollable-choice ckb (find-variable-assignment ps "x_commute" "bike"))))
      (is-true (= (/ 7 20) (compute-conditional-marginal-probability-of-uncontrollable-choice ckb (find-variable-assignment ps "x_commute" "car"))))

      ;; Condition on an observed choice!
      (update-with-constraint! ckb (make-instance 'assignment-constraint :assignment (find-variable-assignment ps "x_snooze" "get_up")))

      ;; Now with condiitioned history
      (is-true (= (/ 13 16) (compute-conditional-marginal-probability-of-uncontrollable-choice ckb (find-variable-assignment ps "x_commute" "run"))))
      (is-true (= (/ 13 144) (compute-conditional-marginal-probability-of-uncontrollable-choice ckb (find-variable-assignment ps "x_commute" "bike"))))
      (is-true (= (/ 7 72) (compute-conditional-marginal-probability-of-uncontrollable-choice ckb (find-variable-assignment ps "x_commute" "car")))))))


;; Some faulty gripper tests

(test riker-faulty-gripper-1
  "Check with the simple-n domain in my thesis. Checks probability computation, and also makes sure that the
   policy BDD sizes match what is expected (and grow linearly). Note that if we change our constraint encoding,
   those relevant checks below will expectedely break!!"
  (dolist (cc-type `(:cc-wmc-bdd :cc-wmc-ab-bdd))
    ;; N = 1
    (let ((ps (setup-faulty-gripper-3 :constraint-knowledge-base-type cc-type)))
      ;; Make sure it compiles properly
      (is-true (pike-compile ps))

      ;; Get some relevant variables
      (let* ((ckb (pike-constraint-knowledge-base ps))
             (x_e1 (find-variable ps "x_e1"))
             (x_e2 (find-variable ps "x_e2"))
             (x_e3 (find-variable ps "x_e3"))
             (x_e4 (find-variable ps "x_e4"))
             (x_R1 (find-variable ps "x_R1"))
             (x_R2 (find-variable ps "x_R2")))

        ;; Test probability computations!
        (is  (= (/ 40613 50000) ;; 0.81266
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list )
                 :conflicts (list ))))

        (is  (= (/ 976 1075) ;; 0.9079
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment x_e1 "held"))
                 :conflicts (list ))))

        (is  (= (/ 3127 5200) ;; 0.6013
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment x_e1 "dropped"))
                 :conflicts (list ))))

        (is  (= 0 ;; Violated causal link
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment x_e1 "held")
                                          (assignment x_R1 "yes"))
                 :conflicts (list ))))

        (is  (= (/ 976 1075) ;; Same as before
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment x_e1 "held")
                                          (assignment x_R1 "no"))
                 :conflicts (list ))))

        (is  (= (/ 3127 3460) ;; 0.9037
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment x_e1 "dropped")
                                          (assignment x_R1 "yes")
                                          (assignment x_e2 "held"))
                 :conflicts (list ))))

        (is  (= 0 ;; Failed -- dropped twice. Causal link violation to goal.
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment x_e1 "dropped")
                                          (assignment x_R1 "yes")
                                          (assignment x_e2 "dropped"))
                 :conflicts (list ))))

        (is  (= 1 ;; Goal succeeded!
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment x_e1 "dropped")
                                          (assignment x_R1 "yes")
                                          (assignment x_e2 "held")
                                          (assignment x_e3 "held"))
                 :conflicts (list ))))

        (is  (= (/ 241 352) ;; 0.6846. Verifiable -- if we condition the three choices, chance of failure is just marginalized last one (easy with Weka)
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment x_e1 "dropped")
                                          (assignment x_R1 "yes")
                                          (assignment x_e2 "held")
                                          (assignment x_e3 "dropped"))
                 :conflicts (list ))))

        (is  (= 1 ;; Success!
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment x_e1 "dropped")
                                          (assignment x_R1 "yes")
                                          (assignment x_e2 "held")
                                          (assignment x_e3 "dropped")
                                          (assignment x_R1 "yes")
                                          (assignment x_e4 "held"))
                 :conflicts (list ))))))))


;; Desserts tests
(test desserts-1
  ;; Possibly examine multiple different methods of computing
  ;; the probabilities
  (dolist (cc-type `(:cc-wmc-bdd :cc-wmc-ab-bdd)) ;;  :patms :cc-wmc-bdd
    (let ((ps (setup-desserts-1 :constraint-knowledge-base-type cc-type)))
      ;; Make sure it compiles properly
      (is-true (pike-compile ps))

      ;; Get some relevant variables
      (let* ((ss (pike::pike-state-space ps))
             (ckb (pike-constraint-knowledge-base ps))
             (R1 (find-variable ps "R1"))
             (H1 (find-variable ps "H1"))
             (H2 (find-variable ps "H2"))
             (H3 (find-variable ps "H3")))

        ;; Test probability computations!
        (is  (= (/ 73 80)
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list )
                 :conflicts (list ))))

        (is  (= 1
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment R1 "sugar")
                                          (assignment H3 "cookies"))
                 :conflicts (list ))))

        (is  (= 0
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment R1 "fruit")
                                          (assignment H3 "cookies"))
                 :conflicts (list ))))

        (is  (= 0
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment R1 "sugar")
                                          (assignment H3 "fruit"))
                 :conflicts (list ))))

        (is  (= (/ 19 20)
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment R1 "sugar")
                                          (assignment H3 "brownies"))
                 :conflicts (list ))))

        (is  (= 1
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment R1 "sugar")
                                          (assignment H3 "brownies")
                                          (assignment H2 "good"))
                 :conflicts (list ))))

        (is  (= (/ 1 2)
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment R1 "sugar")
                                          (assignment H3 "brownies")
                                          (assignment H2 "burnt"))
                 :conflicts (list ))))

        (is  (= 0
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment R1 "sugar")
                                          (assignment H3 "brownies")
                                          (assignment H2 "burnt")
                                          (assignment H1 "out"))
                 :conflicts (list ))))

        (is  (= 1
                (pike::compute-conditional-probability-of-correctness
                 ckb
                 :assignments-given (list (assignment R1 "sugar")
                                          (assignment H3 "brownies")
                                          (assignment H2 "burnt")
                                          (assignment H1 "buy"))
                 :conflicts (list ))))))))
