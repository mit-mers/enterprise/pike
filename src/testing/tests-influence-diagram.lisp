;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike/test)

(def-suite :tests-influence-diagram
    :description "Tests for influence diagrams"
    :in :tests-pike)

(in-suite :tests-influence-diagram)


(test influence-diagram-verify-1
  ;; Possibly examine multiple different methods of computing
  ;; the probabilities
  (let* ((ss (make-state-space-bits))
         (id (pike::parse-pgmx-file (pike-test-file "influence-diagrams/book-example/infdiag-book.pgmx") :ss ss))
         (A (find-variable ss "A"))
         (B (find-variable ss "B"))
         (C (find-variable ss "C"))
         (D (find-variable ss "D"))
         (E (find-variable ss "E"))
         (F (find-variable ss "F"))
         (G (find-variable ss "G"))
         (H (find-variable ss "H"))
         (I (find-variable ss "I"))
         (J (find-variable ss "J"))
         (K (find-variable ss "K"))
         (L (find-variable ss "L"))
         (D1 (find-variable ss "D1"))
         (D2 (find-variable ss "D2"))
         (D3 (find-variable ss "D3"))
         (D4 (find-variable ss "D4")))

    ;; Make sure no error!
    (is-true (pike::influence-diagram-verify-variable-order id (list B D1 E F D2 D3 G D4 A C D H I J K L)))
    (is-true (pike::influence-diagram-verify-variable-order id (list B D1 F E D2 D3 G D4 A C D H I J K L)))
    (is-true (pike::influence-diagram-verify-variable-order id (list B D1 F E D2 D3 G D4 A C D H I L J K)))

    ;; Signals an error!
    (signals simple-error (pike::influence-diagram-verify-variable-order id (list B D1 E F D2 D3 G A C D H I J K L D4)))
    (signals simple-error (pike::influence-diagram-verify-variable-order id (list B D1 E F D2 D3 G A C D H I J K L)))
    (signals simple-error (pike::influence-diagram-verify-variable-order id (list B D1 D1 E F D3 G A C D H I J K L D4)))))
