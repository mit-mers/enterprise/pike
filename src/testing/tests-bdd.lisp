;;;; Copyright (c) 2018 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike/test)

(def-suite :tests-bdd
    :description "Tests relating to binary decision diagrams (BDDs)"
    :in :tests-pike)

(in-suite :tests-bdd)




(defun test-bdd-1 ()
  (let* ((ss (make-state-space-bits))
         (x (make-instance 'decision-variable
                           :name "x"
                           :domain (list 1 2 3)))
         (y (make-instance 'decision-variable
                           :name "y"
                           :domain (list 1 2 3)))
         (z (make-instance 'decision-variable
                           :name "z"
                           :domain (list 1 2 3)))
         bm bde n

         c1
         constraints)

    ;; Set up the ss
    (add-variable! ss x)
    (add-variable! ss y)
    (add-variable! ss z)

    ;;;; Test making a constraint
    ;; (setf c1 (make-constraint `(=> (= ,y 1)
    ;;                                (= ,x 1))))
    ;; (format t "~a~%" c1)
    ;; (setf n (constraint->bdd bde c1))
    ;; (bdd-to-dot-stream n t)
    ;; (bdd-to-dot-stream (bdd-encoder-get-variable-constraints-bdd bde) t)

    ;;;; Test conditioning
    (setf c1 (make-constraint `(=> (= ,y 1)
                                   (= ,x 1))))
    (push c1 constraints)

    ;; Start a BDD manager
    (setf bm (pike::create-bdd-manager))

    ;; Create a BDD encoder (which connects a state space and BDD manager)
    (setf bde (pike::make-bdd-encoder :ss ss
                                :bm bm))

    ;; Set up the encoder
    (pike::bdd-encoder-initialize bde
                            `(,x ,y, z)
                            :use-binaries t
                            :variable-order-heuristic :force
                            :constraints constraints)


    ;;(setf n (bdd-encoder-get-variable-constraints-bdd bde))
    (setf n (pike::constraint->bdd bde c1))
    (setf n (pike::bdd-and bm (pike::bdd-encoder-get-variable-constraints-bdd bde) n))
    ;; (setf n (bdd-encoder-condition-on-assignment bde n (assignment y 1)))

    (format t "Number of BDD nodes in n: ~a~%" (pike::bdd-count-children n))

    (pike::bdd-to-dot-stream n t)

    (values bde bm x y z)))



(defun test-bdd-2 ()
  (let* ((ps (setup-morning-routine-1 :constraint-knowledge-base-type :sat))
         all-variables
         constraints
         ss
         bm bde n)

    (pike-compile ps)

    (setf all-variables (pike::state-space-bits-variables (pike::pike-state-space ps)))
    (setf constraints (pike::constraint-knowledge-base-constraints (pike-constraint-knowledge-base ps)))
    (setf ss (pike::pike-state-space ps))


    ;; Start a BDD manager
    (setf bm (pike::create-bdd-manager))

    ;; Create a BDD encoder (which connects a state space and BDD manager)
    (setf bde (pike::make-bdd-encoder :ss ss
                                      :bm bm))


    (time (progn
            ;; Set up the encoder
            (pike::bdd-encoder-initialize bde
                                          all-variables
                                          :use-binaries t
                                          :variable-order-heuristic :force
                                          :constraints constraints)

            (setf n (pike::bdd-encoder-get-variable-constraints-bdd bde))
            (dolist (constraint constraints)
              (setf n (pike::bdd-and bm n (pike::constraint->bdd bde constraint))))))


    (pike::bdd-to-dot-stream n t)
    (values ps bde bm)))


(defun test-bdd-3 ()
  (let* ((ss (make-state-space-bits))
         (spec (make-instance 'decision-variable
                           :name "spec"
                           :domain (list "e_P1" "e_P2" "None")))
         (y (make-instance 'decision-variable
                           :name "y"
                           :domain (list 1 2)))
         (z (make-instance 'decision-variable
                           :name "z"
                           :domain (list 1 2)))
         bm bde n

         c1
         constraints)

    ;; Set up the ss
    (add-variable! ss spec)
    (add-variable! ss y)
    (add-variable! ss z)

    ;;;; Test making a constraint
    ;; (setf c1 (make-constraint `(=> (= ,y 1)
    ;;                                (= ,x 1))))
    ;; (format t "~a~%" c1)
    ;; (setf n (constraint->bdd bde c1))
    ;; (bdd-to-dot-stream n t)
    ;; (bdd-to-dot-stream (bdd-encoder-get-variable-constraints-bdd bde) t)

    ;;;; Test conditioning
    (setf c1 (make-constraint `(=> (= ,z 1)
                                   (or (= ,spec "e_P2")
                                       (and (= ,spec "e_P1")
                                            (= ,y 1))))))
    (push c1 constraints)

    ;; Start a BDD manager
    (setf bm (pike::create-bdd-manager))

    ;; Create a BDD encoder (which connects a state space and BDD manager)
    (setf bde (pike::make-bdd-encoder :ss ss
                                :bm bm))

    ;; Set up the encoder
    (pike::bdd-encoder-initialize bde
                            `(,z ,spec ,y)
                            :use-binaries t
                            :variable-order-heuristic :force
                            :constraints constraints)


    ;;(setf n (bdd-encoder-get-variable-constraints-bdd bde))
    (setf n (pike::constraint->bdd bde c1))
    (setf n (pike::bdd-and bm (pike::bdd-encoder-get-variable-constraints-bdd bde) n))
    ;; (setf n (bdd-encoder-condition-on-assignment bde n (assignment y 1)))

    (format t "Number of BDD nodes in n: ~a~%" (pike::bdd-count-children n))

    (pike::bdd-to-dot-stream n t)

    (values bde bm spec y z)))



(defun test-bdd-eric ()
  "An interesting example brought up by Eric Timmons"
  (let* ((ss (make-state-space-bits))
         (x (make-instance 'decision-variable
                           :name "x"
                           :domain (list 1 0)))
         (y1 (make-instance 'decision-variable
                            :name "y1"
                            :domain (list 1 0)))
         (y2 (make-instance 'decision-variable
                            :name "y2"
                            :domain (list 1 0)))
         bm bde n n-proj n-partial n-partial-0 n-partial-1

         c1
         constraints)

    ;; Set up the ss
    (add-variable! ss x)
    (add-variable! ss y1)
    (add-variable! ss y2)


    ;; The Eric BDD -- fully expanded
    (setf c1 (make-constraint `(or (and (= ,x 1) (= ,y1 1) (= ,y2 1))
                                   (and (= ,x 1) (= ,y1 0) (= ,y2 0))
                                   (and (= ,x 0) (= ,y1 1) (= ,y2 0))
                                   (and (= ,x 0) (= ,y1 0) (= ,y2 1)))))

    (push c1 constraints)

    ;; Start a BDD manager
    (setf bm (pike::create-bdd-manager))

    ;; Create a BDD encoder (which connects a state space and BDD manager)
    (setf bde (pike::make-bdd-encoder :ss ss
                                      :bm bm))

    ;; Set up the encoder
    (pike::bdd-encoder-initialize bde
                                  `(,x ,y1, y2)
                                  :use-binaries t
                                  :variable-order-heuristic :default
                                  :constraints constraints)


    ;;(setf n (bdd-encoder-get-variable-constraints-bdd bde))
    (format t "Eric BDD:~%")
    ;;(setf n (pike::constraint->bdd bde c1))
    ;;(setf n (pike::bdd-and bm (pike::bdd-encoder-get-variable-constraints-bdd bde) n))
    (setf n
          (pike::get-bdd-node bm
                              (assignment x 1)

                              (pike::get-bdd-node bm (assignment y1 1)
                                                  ;;(pike::get-bdd-op-or-result bm :and nil)
                                                  (pike::get-bdd-node bm
                                                                      (assignment y2 1)
                                                                      (pike::get-bdd-terminal bm 0)
                                                                      (pike::get-bdd-terminal bm 1))
                                                  (pike::get-bdd-node bm
                                                                      (assignment y2 1)
                                                                      (pike::get-bdd-terminal bm 1)
                                                                      (pike::get-bdd-terminal bm 0)))

                              (pike::get-bdd-node bm (assignment y1 1)
                                                  (pike::get-bdd-node bm
                                                                      (assignment y2 1)
                                                                      (pike::get-bdd-terminal bm 1)
                                                                      (pike::get-bdd-terminal bm 0))
                                                  (pike::get-bdd-node bm
                                                                      (assignment y2 1)
                                                                      (pike::get-bdd-terminal bm 0)
                                                                      (pike::get-bdd-terminal bm 1)))))
    (pike::bdd-to-dot-stream n t)

    ;; Set to project onto y1, y2
    (let (assignments-bn)
      (dolist (v `(,y1 ,y2))
        (dolist (val (pike::decision-variable-values v))
          (push (assignment v val) assignments-bn)))
      (pike::bdd-set-projection-variables bm (remove-duplicates (mapcar #'first
                                                                        (mapcar #'(lambda (a)
                                                                                    (pike::bdd-encoder-get-literal bde a))
                                                                                assignments-bn)))))

    ;; Project away x:
    (format t "Projected version:~%")
    (setf n-proj (pike::bdd-project bm n))
    (pike::bdd-to-dot-stream n-proj t)

    (format t "Partial BDD version of Eric's BDD~%")
    (setf n-partial
          (pike::get-bdd-node bm
                              (assignment x 1)

                              (pike::get-bdd-node bm (assignment y1 1)
                                                  (pike::get-bdd-op-or-result bm :and nil)
                                                  ;; (pike::get-bdd-node bm
                                                  ;;                     (assignment y2 1)
                                                  ;;                     (pike::get-bdd-terminal bm 0)
                                                  ;;                     (pike::get-bdd-terminal bm 1))
                                                  (pike::get-bdd-node bm
                                                                      (assignment y2 1)
                                                                      (pike::get-bdd-terminal bm 1)
                                                                      (pike::get-bdd-terminal bm 0)))

                              (pike::get-bdd-node bm (assignment y1 1)
                                                  (pike::get-bdd-node bm
                                                                      (assignment y2 1)
                                                                      (pike::get-bdd-terminal bm 1)
                                                                      (pike::get-bdd-terminal bm 0))
                                                  (pike::get-bdd-node bm
                                                                      (assignment y2 1)
                                                                      (pike::get-bdd-terminal bm 0)
                                                                      (pike::get-bdd-terminal bm 1)))))
    (pike::bdd-to-dot-stream n-partial t)

    ;; Overapproximate
    (format t "Partial bounded with 1:~%")
    (setf n-partial-1 (pike::bdd-compute-approximation bm n-partial (pike::get-bdd-terminal bm 1)))
    (pike::bdd-to-dot-stream n-partial-1 t)

    ;; Project overapproximation
    (format t "Projected version:~%")
    (setf n-proj (pike::bdd-project bm n-partial-1))
    (pike::bdd-to-dot-stream n-proj t)

    ;; Under approximation
    (format t "Partial bounded with 0:~%")
    (setf n-partial-0 (pike::bdd-compute-approximation bm n-partial (pike::get-bdd-terminal bm 0)))
    (pike::bdd-to-dot-stream n-partial-0 t)

    ;; Project overapproximation
    (format t "Projected version:~%")
    (setf n-proj (pike::bdd-project bm n-partial-0))
    (pike::bdd-to-dot-stream n-proj t)

    (values bde bm x y1 y2)))


(defun test-bdd-quantification-1 ()
  ;; This example taken from slides from
  ;; Marihn J.H. Heule, from University of Texas,
  ;; "Reasoning with Quantified Boolean Formulas".
  (let* ((ss (make-state-space-bits))
         (A (make-instance 'decision-variable
                           :name "A"
                           :domain (list 1 0)))
         (B (make-instance 'decision-variable
                           :name "B"
                           :domain (list 1 0)))
         (C (make-instance 'decision-variable
                           :name "C"
                           :domain (list 1 0)))
         bm bde n n-test

         c1
         constraints)

    ;; Set up the ss
    (add-variable! ss A)
    (add-variable! ss B)
    (add-variable! ss C)


    ;; The Eric BDD -- fully expanded
    (setf c1 (make-constraint `(and (or (= ,A 1)
                                        (= ,B 1))
                                    (or (= ,A 0)
                                        (= ,C 1))
                                    (or (= ,B 0)
                                        (= ,C 0)))))

    (push c1 constraints)

    ;; Start a BDD manager
    (setf bm (pike::create-bdd-manager))

    ;; Create a BDD encoder (which connects a state space and BDD manager)
    (setf bde (pike::make-bdd-encoder :ss ss
                                      :bm bm))

    ;; Set up the encoder
    (pike::bdd-encoder-initialize bde
                                  `(,A ,B, C)
                                  :use-binaries t
                                  :variable-order-heuristic :default
                                  :constraints constraints)

    (format t "BDD:~%")
    ;;(setf n (bdd-encoder-get-variable-constraints-bdd bde))
    (setf n (pike::constraint->bdd bde c1))
    (setf n (pike::bdd-and bm (pike::bdd-encoder-get-variable-constraints-bdd bde) n))
    (pike::bdd-to-dot-stream n t)

    ;; Forall A, There exists B, There Exists C
    (setf n-test n)
    (setf n-test (pike::bdd-existential-quantify bm n-test (assignment C 1)))
    (setf n-test (pike::bdd-existential-quantify bm n-test (assignment B 1)))
    (setf n-test (pike::bdd-universal-quantify bm n-test (assignment A 1)))
    (pike::bdd-to-dot-stream n-test t)

    ;; There exists B, For all A, There exists C
    (setf n-test n)
    (setf n-test (pike::bdd-existential-quantify bm n-test (assignment C 1)))
    (setf n-test (pike::bdd-universal-quantify bm n-test (assignment A 1)))
    (setf n-test (pike::bdd-existential-quantify bm n-test (assignment B 1)))
    (pike::bdd-to-dot-stream n-test t)


    (values bde bm A B C)))

(defun test-bdd-policy-tree-1 ()
  "Two independent variables!"
  (let* ((ss (make-state-space-bits))
         (x (make-instance 'decision-variable
                           :name "x"
                           :domain (list 1 0)))
         (bm (pike::create-bdd-manager))
         (bde (pike::make-bdd-encoder :ss ss
                                      :bm bm))
         y1 y2 θ_y1=c0 θ_y1=c1 θ_y2=c0 θ_y2=c1
         bn weights variables-bn constraints-bn all-variables constraints node-bn node-constraints node-policy)

    (add-variable! ss x)

    ;; Parse the Bayesian network
    (setf bn (pike::parse-xmlbif-file (pike-test-file "bdds/bn-indep-2.xmlbif")
                                      :ss ss))

    (multiple-value-setq (weights variables-bn constraints-bn) (pike::encode-bayesian-network-enc1-for-bdd bn))

    ;; Get access to the variables
    (setf y1 (find-variable ss "y1"))
    (setf y2 (find-variable ss "y2"))
    (setf θ_y1=c0 (find-variable variables-bn "θ_y1=c0|"))
    (setf θ_y1=c1 (find-variable variables-bn "θ_y1=c1|"))
    (setf θ_y2=c0 (find-variable variables-bn "θ_y2=c0|"))
    (setf θ_y2=c1 (find-variable variables-bn "θ_y2=c1|"))


    ;; Select our special variable ordering
    ;; (setf all-variables (list y1 y2 x  θ_y1=c0 θ_y1=c1 θ_y2=c0 θ_y2=c1 ))
    (setf all-variables (list y1  θ_y1=c0 θ_y1=c1  y2 θ_y2=c0 θ_y2=c1 x ))

    ;; Now that we have access to all of the constraints (or rather the offline ones at least),
    ;; select a reasonable BDD variable ordering and prepare to encode constraints.
    (pike::bdd-encoder-initialize bde
                                  all-variables
                                  :use-binaries t
                                  :variable-order-heuristic :default) ;; Use the given ordering! Important for MaxΣ∏

    ;; Also store the weights
    (pike::bdd-set-weights bm weights)

    ;; Set the Σ∏ variables
    (let (assignments-bn)
      (dolist (v variables-bn)
        (dolist (val (pike::decision-variable-values v))
          (push (assignment v val) assignments-bn)))
      (pike::bdd-set-Σ∏-variables bm (remove-duplicates (mapcar #'first
                                                          (mapcar #'(lambda (a)
                                                                      (pike::bdd-encoder-get-literal bde a))
                                                                  assignments-bn)))))


    ;; Encode the Bayesian network as a BDD
    (setf node-bn (pike::bdd-and bm
                                 (pike::bdd-encoder-get-variable-constraints-bdd bde)
                                 (pike::constraint->bdd bde (make-instance 'conjunction-constraint
                                                                           :conjuncts constraints-bn))))

    ;; Create the constraints to encode
    (push (make-constraint `(and (<=> (= ,x 0) (= ,y1 "c0"))
                                 (<=> (= ,x 0) (= ,y2 "c0"))))
          constraints)

    (setf node-constraints (pike::bdd-and bm
                                          (pike::bdd-encoder-get-variable-constraints-bdd bde)
                                          (pike::constraint->bdd bde (make-instance 'conjunction-constraint
                                                                                    :conjuncts constraints))))



    ;; Put them together to create the policy node!
    (setf node-policy (pike::bdd-and bm node-bn node-constraints))

    ;; (pike::bdd-to-dot-stream node-bn t)
    ;; (pike::bdd-to-dot-stream node-constraints t)
    (pike::bdd-to-dot-stream node-policy t)

    (format t "MaxΣ∏ : ~a~%" (pike::bdd-maxΣ∏ bm node-policy))

    (values bm bde)))


(defun test-bdd-policy-tree-2 ()
  "One variable: skewed distribution with domain of size 3"
  (let* ((ss (make-state-space-bits))
         (x (make-instance 'decision-variable
                           :name "x"
                           :domain (list 2 1 0)))
         (bm (pike::create-bdd-manager))
         (bde (pike::make-bdd-encoder :ss ss
                                      :bm bm))
         y1 θ_y1=c0 θ_y1=c1 θ_y1=c2
         bn weights variables-bn constraints-bn all-variables constraints node-bn node-constraints node-policy)

    (add-variable! ss x)

    ;; Parse the Bayesian network
    (setf bn (pike::parse-xmlbif-file (pike-test-file "bdds/bn-1-skewed.xmlbif")
                                      :ss ss))

    (multiple-value-setq (weights variables-bn constraints-bn) (pike::encode-bayesian-network-enc1-for-bdd bn))

    ;; Get access to the variables
    (setf y1 (find-variable ss "y1"))
    (setf θ_y1=c0 (find-variable variables-bn "θ_y1=c0|"))
    (setf θ_y1=c1 (find-variable variables-bn "θ_y1=c1|"))
    (setf θ_y1=c2 (find-variable variables-bn "θ_y1=c2|"))


    ;; Select our special variable ordering
    (setf all-variables (list y1 x θ_y1=c0 θ_y1=c1 θ_y1=c2))

    ;; Now that we have access to all of the constraints (or rather the offline ones at least),
    ;; select a reasonable BDD variable ordering and prepare to encode constraints.
    (pike::bdd-encoder-initialize bde
                                  all-variables
                                  :use-binaries t
                                  :variable-order-heuristic :default) ;; Use the given ordering! Important for MaxΣ∏

    ;; Also store the weights
    (pike::bdd-set-weights bm weights)

    ;; Set the Σ∏ variables
    (let (assignments-bn)
      (dolist (v variables-bn)
        (dolist (val (pike::decision-variable-values v))
          (push (assignment v val) assignments-bn)))
      (pike::bdd-set-Σ∏-variables bm (remove-duplicates (mapcar #'first
                                                                (mapcar #'(lambda (a)
                                                                            (pike::bdd-encoder-get-literal bde a))
                                                                        assignments-bn)))))


    ;; Encode the Bayesian network as a BDD
    (setf node-bn (pike::bdd-and bm
                                 (pike::bdd-encoder-get-variable-constraints-bdd bde)
                                 (pike::constraint->bdd bde (make-instance 'conjunction-constraint
                                                                           :conjuncts constraints-bn))))

    ;; Create the constraints to encode
    (push (make-constraint `(and (<=> (= ,x 0) (= ,y1 "c0"))
                                 (<=> (= ,x 1) (= ,y1 "c1"))
                                 (<=> (= ,x 2) (= ,y1 "c2"))))
          constraints)

    (setf node-constraints (pike::bdd-and bm
                                          (pike::bdd-encoder-get-variable-constraints-bdd bde)
                                          (pike::constraint->bdd bde (make-instance 'conjunction-constraint
                                                                                    :conjuncts constraints))))



    ;; Put them together to create the policy node!
    (setf node-policy (pike::bdd-and bm node-bn node-constraints))

    ;; (pike::bdd-to-dot-stream node-bn t)
    ;; (pike::bdd-to-dot-stream node-constraints t)
    ;; (pike::bdd-to-dot-stream node-policy t)

    ;; Should be 1: the agent can observe the uncontrollable choice first and guarantee success!
    (format t "MaxΣ∏ : ~a~%"(pike::bdd-maxΣ∏ bm node-policy))
    (values bm bde))

  (let* ((ss (make-state-space-bits))
         (x (make-instance 'decision-variable
                           :name "x"
                           :domain (list 2 1 0)))
         (bm (pike::create-bdd-manager))
         (bde (pike::make-bdd-encoder :ss ss
                                      :bm bm))
         y1 θ_y1=c0 θ_y1=c1 θ_y1=c2
         bn weights variables-bn constraints-bn all-variables constraints node-bn node-constraints node-policy)

    (add-variable! ss x)

    ;; Parse the Bayesian network
    (setf bn (pike::parse-xmlbif-file (pike-test-file "bdds/bn-1-skewed.xmlbif")
                                      :ss ss))

    (multiple-value-setq (weights variables-bn constraints-bn) (pike::encode-bayesian-network-enc1-for-bdd bn))

    ;; Get access to the variables
    (setf y1 (find-variable ss "y1"))
    (setf θ_y1=c0 (find-variable variables-bn "θ_y1=c0|"))
    (setf θ_y1=c1 (find-variable variables-bn "θ_y1=c1|"))
    (setf θ_y1=c2 (find-variable variables-bn "θ_y1=c2|"))


    ;; Select our special variable ordering
    (setf all-variables (list x y1 θ_y1=c0 θ_y1=c1 θ_y1=c2))

    ;; Now that we have access to all of the constraints (or rather the offline ones at least),
    ;; select a reasonable BDD variable ordering and prepare to encode constraints.
    (pike::bdd-encoder-initialize bde
                                  all-variables
                                  :use-binaries t
                                  :variable-order-heuristic :default) ;; Use the given ordering! Important for MaxΣ∏

    ;; Also store the weights
    (pike::bdd-set-weights bm weights)

    ;; Set the Σ∏ variables
    (let (assignments-bn)
      (dolist (v variables-bn)
        (dolist (val (pike::decision-variable-values v))
          (push (assignment v val) assignments-bn)))
      (pike::bdd-set-Σ∏-variables bm (remove-duplicates (mapcar #'first
                                                                (mapcar #'(lambda (a)
                                                                            (pike::bdd-encoder-get-literal bde a))
                                                                        assignments-bn)))))


    ;; Encode the Bayesian network as a BDD
    (setf node-bn (pike::bdd-and bm
                                 (pike::bdd-encoder-get-variable-constraints-bdd bde)
                                 (pike::constraint->bdd bde (make-instance 'conjunction-constraint
                                                                           :conjuncts constraints-bn))))

    ;; Create the constraints to encode
    (push (make-constraint `(and (<=> (= ,x 0) (= ,y1 "c0"))
                                 (<=> (= ,x 1) (= ,y1 "c1"))
                                 (<=> (= ,x 2) (= ,y1 "c2"))))
          constraints)

    (setf node-constraints (pike::bdd-and bm
                                          (pike::bdd-encoder-get-variable-constraints-bdd bde)
                                          (pike::constraint->bdd bde (make-instance 'conjunction-constraint
                                                                                    :conjuncts constraints))))



    ;; Put them together to create the policy node!
    (setf node-policy (pike::bdd-and bm node-bn node-constraints))

    ;; (pike::bdd-to-dot-stream node-bn t)
    ;; (pike::bdd-to-dot-stream node-constraints t)
    ;; (pike::bdd-to-dot-stream node-policy t)

    ;; Should be 0.9: the agent must make best guess first
    (format t "MaxΣ∏ : ~a~%"(pike::bdd-maxΣ∏ bm node-policy))
    (values bm bde)))


(defun test-bdd-policy-tree-3 ()
  (let* ((ss (make-state-space-bits))
         (x (make-instance 'decision-variable
                           :name "x"
                           :domain (list 1 0)))
         (bm (pike::create-bdd-manager))
         (bde (pike::make-bdd-encoder :ss ss
                                      :bm bm))
         y1 y2 θ_y1=c0 θ_y1=c1 θ_y2=c0 θ_y2=c1
         bn weights variables-bn constraints-bn all-variables constraints node-bn node-constraints node-policy)

    (add-variable! ss x)

    ;; Parse the Bayesian network
    (setf bn (pike::parse-xmlbif-file (pike-test-file "bdds/bn-indep-2.xmlbif")
                                      :ss ss))

    (multiple-value-setq (weights variables-bn constraints-bn) (pike::encode-bayesian-network-enc1-for-bdd bn))

    ;; Get access to the variables
    (setf y1 (find-variable ss "y1"))
    (setf y2 (find-variable ss "y2"))
    (setf θ_y1=c0 (find-variable variables-bn "θ_y1=c0|"))
    (setf θ_y1=c1 (find-variable variables-bn "θ_y1=c1|"))
    (setf θ_y2=c0 (find-variable variables-bn "θ_y2=c0|"))
    (setf θ_y2=c1 (find-variable variables-bn "θ_y2=c1|"))


    ;; Select our special variable ordering
    (setf all-variables (list y1 x y2   θ_y1=c0 θ_y1=c1 θ_y2=c0 θ_y2=c1 ))
    ;; (setf all-variables (list  y1 θ_y1=c0 θ_y1=c1  x  y2 θ_y2=c0 θ_y2=c1 ))

    ;; Now that we have access to all of the constraints (or rather the offline ones at least),
    ;; select a reasonable BDD variable ordering and prepare to encode constraints.
    (pike::bdd-encoder-initialize bde
                                  all-variables
                                  :use-binaries t
                                  :variable-order-heuristic :default) ;; Use the given ordering! Important for MaxΣ∏

    ;; Also store the weights
    (pike::bdd-set-weights bm weights)

    ;; Set the Σ∏ variables
    (let (assignments-bn)
      (dolist (v variables-bn)
        (dolist (val (pike::decision-variable-values v))
          (push (assignment v val) assignments-bn)))
      (pike::bdd-set-Σ∏-variables bm (remove-duplicates (mapcar #'first
                                                                (mapcar #'(lambda (a)
                                                                            (pike::bdd-encoder-get-literal bde a))
                                                                        assignments-bn)))))


    ;; Encode the Bayesian network as a BDD
    (setf node-bn (pike::bdd-and bm
                                 (pike::bdd-encoder-get-variable-constraints-bdd bde)
                                 (pike::constraint->bdd bde (make-instance 'conjunction-constraint
                                                                           :conjuncts constraints-bn))))

    ;; Create the constraints to encode
    (push (make-constraint `(and (=> (= ,y2 "c0") (= ,x 0))
                                 (=> (= ,y2 "c1") (= ,x 1))))
          constraints)

    (setf node-constraints (pike::bdd-and bm
                                          (pike::bdd-encoder-get-variable-constraints-bdd bde)
                                          (pike::constraint->bdd bde (make-instance 'conjunction-constraint
                                                                                    :conjuncts constraints))))



    ;; Put them together to create the policy node!
    (setf node-policy (pike::bdd-and bm node-bn node-constraints))

    ;; (pike::bdd-to-dot-stream node-bn t)
    ;; (pike::bdd-to-dot-stream node-constraints t)
    (pike::bdd-to-dot-stream node-policy t)

    (format t "MaxΣ∏ : ~a~%" (pike::bdd-maxΣ∏ bm node-policy))

    (values bm bde)))




(defun test-bdd-policy-tree-4 ()
  (let* ((ss (make-state-space-bits))
         (x1 (make-instance 'decision-variable
                            :name "x1"
                            :domain (list "c1" "c2")))
         (x2 (make-instance 'decision-variable
                            :name "x2"
                            :domain (list "c1" "c2")))
         (x3 (make-instance 'decision-variable
                            :name "x3"
                            :domain (list "c1" "c2")))
         (x4 (make-instance 'decision-variable
                            :name "x4"
                            :domain (list "c1" "c2")))
         (bm (pike::create-bdd-manager))
         (bde (pike::make-bdd-encoder :ss ss
                                      :bm bm))
         y1 y2 y3 y4 θ_y1=c1 θ_y1=c2 θ_y2=c1 θ_y2=c2 θ_y3=c1 θ_y3=c2 θ_y4=c1 θ_y4=c2
         bn weights variables-bn constraints-bn all-variables constraints node-bn node-constraints node-policy)

    (add-variable! ss x1)
    (add-variable! ss x2)
    (add-variable! ss x3)
    (add-variable! ss x4)

    ;; Parse the Bayesian network
    (setf bn (pike::parse-xmlbif-file (pike-test-file "choice-order/simple-n/bn-4.xmlbif")
                                      :ss ss))

    (multiple-value-setq (weights variables-bn constraints-bn) (pike::encode-bayesian-network-enc1-for-bdd bn))

    ;; Get access to the variables
    (setf y1 (find-variable ss "y1"))
    (setf y2 (find-variable ss "y2"))
    (setf y3 (find-variable ss "y3"))
    (setf y4 (find-variable ss "y4"))
    (setf θ_y1=c1 (find-variable variables-bn "θ_y1=c1|"))
    (setf θ_y1=c2 (find-variable variables-bn "θ_y1=c2|"))

    (setf θ_y2=c1 (find-variable variables-bn "θ_y2=c1|"))
    (setf θ_y2=c2 (find-variable variables-bn "θ_y2=c2|"))

    (setf θ_y3=c1 (find-variable variables-bn "θ_y3=c1|"))
    (setf θ_y3=c2 (find-variable variables-bn "θ_y3=c2|"))

    (setf θ_y4=c1 (find-variable variables-bn "θ_y4=c1|"))
    (setf θ_y4=c2 (find-variable variables-bn "θ_y4=c2|"))

    ;; Select our special variable ordering
    ;; Good variable ordering:
    ;;(setf all-variables (list  x1 y1 θ_y1=c1 θ_y1=c2  x2 y2 θ_y2=c1 θ_y2=c2  x3 y3 θ_y3=c1 θ_y3=c2  x4 y4 θ_y4=c1 θ_y4=c2 ))

    ;; Regular / policy tree / bigger variable ordering:
    (setf all-variables (list  x1 y1 x2 y2 x3 y3 x4 y4  θ_y1=c1 θ_y1=c2 θ_y2=c1 θ_y2=c2 θ_y3=c1 θ_y3=c2  θ_y4=c1 θ_y4=c2 ))


    ;; Now that we have access to all of the constraints (or rather the offline ones at least),
    ;; select a reasonable BDD variable ordering and prepare to encode constraints.
    (pike::bdd-encoder-initialize bde
                                  all-variables
                                  :use-binaries t
                                  :variable-order-heuristic :default) ;; Use the given ordering! Important for MaxΣ∏

    ;; Also store the weights
    (pike::bdd-set-weights bm weights)

    ;; Set the Σ∏ variables
    (let (assignments-bn)
      (dolist (v variables-bn)
        (dolist (val (pike::decision-variable-values v))
          (push (assignment v val) assignments-bn)))
      (pike::bdd-set-Σ∏-variables bm (remove-duplicates (mapcar #'first
                                                                (mapcar #'(lambda (a)
                                                                            (pike::bdd-encoder-get-literal bde a))
                                                                        assignments-bn)))))


    ;; Encode the Bayesian network as a BDD
    (setf node-bn (pike::bdd-and bm
                                 (pike::bdd-encoder-get-variable-constraints-bdd bde)
                                 (pike::constraint->bdd bde (make-instance 'conjunction-constraint
                                                                           :conjuncts constraints-bn))))

    ;; Create the constraints to encode
    (push (make-constraint `(and (=> (= ,y1 "c1") (= ,x1 "c1"))
                                 (=> (= ,y1 "c2") (= ,x1 "c2"))

                                 (=> (= ,y2 "c1") (= ,x2 "c1"))
                                 (=> (= ,y2 "c2") (= ,x2 "c2"))

                                 (=> (= ,y3 "c1") (= ,x3 "c1"))
                                 (=> (= ,y3 "c2") (= ,x3 "c2"))

                                 (=> (= ,y4 "c1") (= ,x4 "c1"))
                                 (=> (= ,y4 "c2") (= ,x4 "c2"))))
          constraints)

    (setf node-constraints (pike::bdd-and bm
                                          (pike::bdd-encoder-get-variable-constraints-bdd bde)
                                          (pike::constraint->bdd bde (make-instance 'conjunction-constraint
                                                                                    :conjuncts constraints))))



    ;; Put them together to create the policy node!
    (setf node-policy (pike::bdd-and bm node-bn node-constraints))

    ;;(pike::bdd-to-dot-stream node-bn t)
    ;; (pike::bdd-to-dot-stream node-constraints t)
    (pike::bdd-to-dot-stream node-policy t)

    (format t "MaxΣ∏ : ~a~%" (pike::bdd-maxΣ∏ bm node-policy))

    (values bm bde)))


(defun test-bdd-variable-ordering-1 ()
  (let* ((ss (make-state-space-bits))
         (x1 (make-instance 'decision-variable
                            :name "x1"
                            :domain (list 1 2)))
         (y1 (make-instance 'decision-variable
                            :name "y1"
                            :domain (list 1 2)))
         (x2 (make-instance 'decision-variable
                            :name "x2"
                            :domain (list 1 2)))
         (y2 (make-instance 'decision-variable
                            :name "y2"
                            :domain (list 1 2)))
         (x3 (make-instance 'decision-variable
                            :name "x3"
                            :domain (list 1 2)))
         (y3 (make-instance 'decision-variable
                            :name "y3"
                            :domain (list 1 2)))
         bm bde

         (constraints nil))

    ;; Set up the ss
    (add-variable! ss x1)
    (add-variable! ss y1)
    (add-variable! ss x2)
    (add-variable! ss y2)
    (add-variable! ss x3)
    (add-variable! ss y3)


    ;; Make some constraints
    (push (make-constraint `(∨ (∧ (= ,x1 1) (= ,y1 1))
                               (∧ (= ,x2 1) (= ,y2 1))
                               (∧ (= ,x3 1) (= ,y3 1))))
          constraints)


    (pike::bdd-variable-order-heuristic-force `(,x1 ,y1 ,x2 , y2, x3, y3) constraints)))


(defun test-bdd-variable-ordering-2 ()
  (let* ((ps (setup-kitchen-simple :constraint-knowledge-base-type :sat))
         variables constraints
         bm bde)

    (pike-compile ps)

    (setf variables (pike::state-space-bits-variables (pike::pike-state-space ps)))
    (setf constraints (pike::constraint-knowledge-base-constraints (pike-constraint-knowledge-base ps)))


    (time (pike::bdd-variable-order-heuristic-force variables constraints))))


(defun test-bdd-variable-ordering-3 ()
  (let* ((ps (setup-morning-routine-1 :constraint-knowledge-base-type :sat))
         variables constraints
         bm bde)

    (pike-compile ps)

    (setf variables (pike::state-space-bits-variables (pike::pike-state-space ps)))
    (setf constraints (pike::constraint-knowledge-base-constraints (pike-constraint-knowledge-base ps)))


    (time (pike::bdd-variable-order-heuristic-force variables constraints))))


(test test-bdd-projection-1 ()
  (let* ((ss (make-state-space-bits))
         (x (make-instance 'decision-variable
                           :name "x"
                           :domain (list 1 2)))
         (y (make-instance 'decision-variable
                           :name "y"
                           :domain (list 1 2)))
         (z (make-instance 'decision-variable
                           :name "z"
                           :domain (list 1 2)))
         bm bde

         c1 n-c1 n-c1-proj
         c2 n-c2)

    ;; Set up the ss
    (add-variable! ss x)
    (add-variable! ss y)
    (add-variable! ss z)


    ;; Test conditioning
    (setf c1 (make-constraint `(or (and (= ,x 1) (= ,y 1) (= ,z 1))
                                   (and (= ,x 1) (= ,y 2) (= ,z 2))
                                   (and (= ,x 2) (= ,y 2) (= ,z 2)))))

    (setf c2 (make-constraint `(or (and (= ,x 1) (= ,z 1))
                                   (and (= ,x 1) (= ,z 2))
                                   (and (= ,x 2) (= ,z 2)))))

    ;; Start a BDD manager
    (setf bm (pike::create-bdd-manager))

    ;; Create a BDD encoder (which connects a state space and BDD manager)
    (setf bde (pike::make-bdd-encoder
               :ss ss
               :bm bm))

    ;; Set up the encoder
    (pike::bdd-encoder-initialize bde
                                  `(,x ,y ,z)
                                  :use-binaries t
                                  :variable-order-heuristic :force
                                  :constraints `(,c1 ,c2))


    (setf n-c1 (pike::bdd-and bm
                              (pike::bdd-encoder-get-variable-constraints-bdd bde)
                              (pike::constraint->bdd bde c1)))

    (setf n-c2 (pike::bdd-and bm
                              (pike::bdd-encoder-get-variable-constraints-bdd bde)
                              (pike::constraint->bdd bde c2)))

    (pike::bdd-set-projection-variables bm `(,(assignment z 1) ,(assignment x 1)))

    (setf n-c1-proj (pike::bdd-project bm n-c1))

    (is (eql n-c2 n-c1-proj))

    ;;(pike::bdd-to-dot-stream n-c1 t)
    ;;(pike::bdd-to-dot-stream n-c1-proj t)

    (values bde bm x y z)))



(defun test-bdd-wmc-1 ()
  (let* ((ss (make-state-space-bits))
         (x (make-instance 'decision-variable
                           :name "x"
                           :domain (list 1 2)))
         (y (make-instance 'decision-variable
                           :name "y"
                           :domain (list 1 2)))
         (z (make-instance 'decision-variable
                           :name "z"
                           :domain (list 1 2)))
         bm bde

         c1 n-c1 )

    ;; Set up the ss
    (add-variable! ss x)
    (add-variable! ss y)
    (add-variable! ss z)


;;;; Test conditioning
    (setf c1 (make-constraint `(and )))


    ;; Start a BDD manager
    (setf bm (pike::create-bdd-manager))

    ;; Create a BDD encoder (which connects a state space and BDD manager)
    (setf bde (pike::make-bdd-encoder
               :ss ss
               :bm bm))

    ;; Set up the encoder
    (pike::bdd-encoder-initialize bde
                                  `(,x ,y ,z)
                                  :use-binaries t
                                  :variable-order-heuristic :force
                                  :constraints `(,c1))


    (setf n-c1 (pike::bdd-and bm
                              (pike::bdd-encoder-get-variable-constraints-bdd bde)
                              (pike::constraint->bdd bde c1)))

    (format t "Model count: ~a~%" (pike::bdd-wmc bm n-c1))

    (values bde bm x y z)))


(defun test-bdd-animator ()
  (let* ((ss (make-state-space-bits))
         (x (make-instance 'decision-variable
                           :name "x"
                           :domain (list 1 2 3)))
         (y (make-instance 'decision-variable
                           :name "y"
                           :domain (list 1 2 3)))
         (z (make-instance 'decision-variable
                           :name "z"
                           :domain (list 1 2 3)))
         bm bde n

         c1
         constraints

         d)

    ;; Set up the ss
    (add-variable! ss x)
    (add-variable! ss y)
    (add-variable! ss z)

;;;; Test making a constraint
    ;; (setf c1 (make-constraint `(=> (= ,y 1)
    ;;                                (= ,x 1))))
    ;; (format t "~a~%" c1)
    ;; (setf n (constraint->bdd bde c1))
    ;; (bdd-to-dot-stream n t)
    ;; (bdd-to-dot-stream (bdd-encoder-get-variable-constraints-bdd bde) t)

;;;; Test conditioning
    (setf c1 (make-constraint `(=> (= ,y 1)
                                   (= ,x 1))))
    (push c1 constraints)

    ;; Start a BDD manager
    (setf bm (pike::create-bdd-manager))

    ;; Create a BDD encoder (which connects a state space and BDD manager)
    (setf bde (pike::make-bdd-encoder :ss ss
                                      :bm bm))

    ;; Set up the encoder
    (pike::bdd-encoder-initialize bde
                                  `(,x ,y, z)
                                  :use-binaries t
                                  :variable-order-heuristic :force
                                  :constraints constraints)


    ;;(setf n (bdd-encoder-get-variable-constraints-bdd bde))
    (setf n (pike::constraint->bdd bde c1))
    (setf n (pike::bdd-and bm (pike::bdd-encoder-get-variable-constraints-bdd bde) n))
    ;; (setf n (bdd-encoder-condition-on-assignment bde n (assignment y 1)))

    ;; (format t "Number of BDD nodes in n: ~a~%" (pike::bdd-count-children n))

    (pike::bdd-to-dot-stream n nil)

    (setf d (list (with-output-to-string (s) (pike::bdd-to-dot-stream n s))
                  (with-output-to-string (s) (pike::bdd-to-dot-stream (pike::bdd-encoder-get-variable-constraints-bdd bde) s))))
    (format t "~a~%" d)

    (cl-json:encode-json d)

    (values bde bm x y z)))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; BDD Incremental tests!
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun test-bdd-incremental-1 ()
  (let* ((ss (make-state-space-bits))
         (x (make-instance 'decision-variable
                           :name "x"
                           :domain (list 1 2 3)))
         (y (make-instance 'decision-variable
                           :name "y"
                           :domain (list 1 2 3)))
         (z (make-instance 'decision-variable
                           :name "z"
                           :domain (list 1 2 3)))
         bm bde

         constraints

         node-constraints
         key n-check n-result)

    ;; Set up the ss
    (add-variable! ss x)
    (add-variable! ss y)
    (add-variable! ss z)


    ;; Make some constraints
    (push (make-constraint `(=> (= ,x 1)
                                (= ,y 1))) constraints)
    (push (make-constraint `(= ,y 1)) constraints)
    (push (make-constraint `(= ,z 1)) constraints)


    ;; Start a BDD manager
    (setf bm (pike::create-bdd-manager))

    ;; Create a BDD encoder (which connects a state space and BDD manager)
    (setf bde (pike::make-bdd-encoder :ss ss
                                      :bm bm))

    ;; Set up the encoder
    (pike::bdd-encoder-initialize bde
                                  `(,x ,y, z)
                                  :use-binaries t
                                  :variable-order-heuristic :force
                                  :constraints constraints)


    ;; Compile individual constraints to BDDs
    (push  (pike::bdd-encoder-get-variable-constraints-bdd bde) node-constraints)
    (dolist (c constraints)
      (push
       ;; (pike::bdd-and bm
       ;;                (pike::bdd-encoder-get-variable-constraints-bdd bde)
       ;;                (pike::constraint->bdd bde c))
       (pike::constraint->bdd bde c)
       node-constraints))


    ;; Do an incremental conjunction!
    (setf key (pike::bdd-begin-apply bm node-constraints))
    ;; Keep running until completion, and save as a file for analysis.
    (test-bdd-incrementally-generate-until-completion bm key "data-graphs.json")
    (setf n-result (pike::bdd-retrieve-current-approximation bm key))

    ;; Sanity check: does it match the stock DFS case?
    (setf n-check (pike::bdd-encoder-get-variable-constraints-bdd bde))
    (dolist (c constraints)
      (setf n-check (pike::bdd-and bm
                                   n-check
                                   (pike::constraint->bdd bde c))))
    (cond
      ((eql n-result n-check)
       (format t "~%Results match the stock DFS case!~%"))
      (t
       (format t "~%~%WARNING!!!~%WARNING!!!~%WARNING!!!~%Results do NOT match the stock DFS case!!~%")))

    (values bde bm x y z)))



(defun test-bdd-incremental-2 ()
  (let* ((ps (setup-morning-routine-1 :constraint-knowledge-base-type :sat))
         all-variables
         constraints
         ss
         bm bde

         node-constraints
         key
         n-result n-check)

    (pike-compile ps)

    (setf all-variables (pike::state-space-bits-variables (pike::pike-state-space ps)))
    (setf constraints (pike::constraint-knowledge-base-constraints (pike-constraint-knowledge-base ps)))
    (setf ss (pike::pike-state-space ps))

    ;; Start a BDD manager
    (setf bm (pike::create-bdd-manager))

    ;; Create a BDD encoder (which connects a state space and BDD manager)
    (setf bde (pike::make-bdd-encoder :ss ss
                                      :bm bm))

    ;; Set up the encoder
    (pike::bdd-encoder-initialize bde
                                  all-variables
                                  :use-binaries t
                                  :variable-order-heuristic :force
                                  :constraints constraints)


    ;; Compile individual constraints to BDDs
    (push  (pike::bdd-encoder-get-variable-constraints-bdd bde) node-constraints)
    (dolist (c constraints)
      (push
       ;; (pike::bdd-and bm
       ;;                (pike::bdd-encoder-get-variable-constraints-bdd bde)
       ;;                (pike::constraint->bdd bde c))
       (pike::constraint->bdd bde c)
       node-constraints))


    ;; Do an incremental conjunction!
    (setf key (pike::bdd-begin-apply bm node-constraints))
    ;; Keep running until completion, and save as a file for analysis.
    (test-bdd-incrementally-generate-until-completion bm key "data-graphs.json")
    (setf n-result (pike::bdd-retrieve-current-approximation bm key))

    ;; Sanity check: does it match the stock DFS case?
    (setf n-check (pike::bdd-encoder-get-variable-constraints-bdd bde))
    (dolist (c constraints)
      (setf n-check (pike::bdd-and bm
                                   n-check
                                   (pike::constraint->bdd bde c))))
    (cond
      ((eql n-result n-check)
       (format t "~%Results match the stock DFS case!~%"))
      (t
       (format t "~%~%WARNING!!!~%WARNING!!!~%WARNING!!!~%Results do NOT match the stock DFS case!!~%")))

    (values bde bm)))


(defun test-bdd-incremental-3 ()
  (let* ((ps (setup-kitchen-simple :constraint-knowledge-base-type :sat))
         all-variables
         constraints
         ss
         bm bde

         node-constraints
         key
         n-result n-check)

    (pike-compile ps)

    (setf all-variables (pike::state-space-bits-variables (pike::pike-state-space ps)))
    (setf constraints (pike::constraint-knowledge-base-constraints (pike-constraint-knowledge-base ps)))
    (setf ss (pike::pike-state-space ps))

    ;; Start a BDD manager
    (setf bm (pike::create-bdd-manager))

    ;; Create a BDD encoder (which connects a state space and BDD manager)
    (setf bde (pike::make-bdd-encoder :ss ss
                                      :bm bm))

    ;; Set up the encoder
    (pike::bdd-encoder-initialize bde
                                  all-variables
                                  :use-binaries t
                                  :variable-order-heuristic :force
                                  :constraints constraints)


    ;; Compile individual constraints to BDDs
    (push  (pike::bdd-encoder-get-variable-constraints-bdd bde) node-constraints)
    (dolist (c constraints)
      (push
       ;; (pike::bdd-and bm
       ;;                (pike::bdd-encoder-get-variable-constraints-bdd bde)
       ;;                (pike::constraint->bdd bde c))
       (pike::constraint->bdd bde c)
       node-constraints))


    ;; Do an incremental conjunction!
    (setf key (pike::bdd-begin-apply bm node-constraints))
    ;; Keep running until completion, and save as a file for analysis.
    (test-bdd-incrementally-generate-until-completion bm key "data-graphs.json")
    (setf n-result (pike::bdd-retrieve-current-approximation bm key))

    ;; Sanity check: does it match the stock DFS case?
    (setf n-check (pike::bdd-encoder-get-variable-constraints-bdd bde))
    (dolist (c constraints)
      (setf n-check (pike::bdd-and bm
                                   n-check
                                   (pike::constraint->bdd bde c))))
    (cond
      ((eql n-result n-check)
       (format t "~%Results match the stock DFS case!~%"))
      (t
       (format t "~%~%WARNING!!!~%WARNING!!!~%WARNING!!!~%Results do NOT match the stock DFS case!!~%")))

    (values bde bm)))


(defun test-bdd-incremental-4 ()
  (let* ((ps (setup-faulty-gripper-2 :constraint-knowledge-base-type :sat))
         all-variables
         constraints
         ss
         bm bde

         node-constraints
         key
         n-result n-check)

    (pike-compile ps)

    (setf all-variables (pike::state-space-bits-variables (pike::pike-state-space ps)))
    (setf constraints (pike::constraint-knowledge-base-constraints (pike-constraint-knowledge-base ps)))
    (setf ss (pike::pike-state-space ps))

    ;; Start a BDD manager
    (setf bm (pike::create-bdd-manager))

    ;; Create a BDD encoder (which connects a state space and BDD manager)
    (setf bde (pike::make-bdd-encoder :ss ss
                                      :bm bm))

    ;; Set up the encoder
    (pike::bdd-encoder-initialize bde
                                  all-variables
                                  :use-binaries t
                                  :variable-order-heuristic :force
                                  :constraints constraints)


    ;; Compile individual constraints to BDDs
    (push  (pike::bdd-encoder-get-variable-constraints-bdd bde) node-constraints)
    (dolist (c constraints)
      (push
       ;; (pike::bdd-and bm
       ;;                (pike::bdd-encoder-get-variable-constraints-bdd bde)
       ;;                (pike::constraint->bdd bde c))
       (pike::constraint->bdd bde c)
       node-constraints))


    ;; Do an incremental conjunction!
    (setf key (pike::bdd-begin-apply bm node-constraints))
    ;; Keep running until completion, and save as a file for analysis.
    (test-bdd-incrementally-generate-until-completion bm key "data-graphs.json")
    (setf n-result (pike::bdd-retrieve-current-approximation bm key))

    ;; Sanity check: does it match the stock DFS case?
    (setf n-check (pike::bdd-encoder-get-variable-constraints-bdd bde))
    (dolist (c constraints)
      (setf n-check (pike::bdd-and bm
                                   n-check
                                   (pike::constraint->bdd bde c))))
    (cond
      ((eql n-result n-check)
       (format t "~%Results match the stock DFS case!~%"))
      (t
       (format t "~%~%WARNING!!!~%WARNING!!!~%WARNING!!!~%Results do NOT match the stock DFS case!!~%")))

    (values bde bm)))



(defun test-bdd-incremental-5 ()
  (let ((ps (setup-morning-routine-1 :constraint-knowledge-base-type :sat)))
    (test-bdd-incremental-with-probabilities ps)))

(defun test-bdd-incremental-6 ()
  (let ((ps (setup-kitchen-simple :constraint-knowledge-base-type :sat)))
    (test-bdd-incremental-with-probabilities ps)))

(defun test-bdd-incremental-7 ()
  (let ((ps (setup-faulty-gripper-3 :constraint-knowledge-base-type :sat)))
    (test-bdd-incremental-with-probabilities ps)))

(defun test-bdd-incremental-8 ()
  (let ((ps (test-pike-compile-with-file "plan.tpn"
                                         "domain.pddl"
                                         "problem.pddl"
                                         :bn "bn.xmlbif"
                                         :constraint-knowledge-base-type :sat)))
    (test-bdd-incremental-with-probabilities ps)))


(defun test-bdd-incremental-with-probabilities (ps)
  (let* (all-variables
         constraints
         ss
         bn weights variables-bn constraints-bn
         bm bde

         node-constraints node-bn
         key
         n-result n-check)

    (pike-compile ps)

    (setf ss (pike::pike-state-space ps))
    (setf bn (pike::pike-bayesian-network ps))

    ;; Encode the bayesian network as a weighted model counting problem, and get the additional constraints
    ;; and variable weights required to do so.
    (multiple-value-setq (weights variables-bn constraints-bn) (pike::encode-bayesian-network-enc1-for-bdd bn))

    ;; The complete list of all variables
    (setf all-variables (concatenate 'list variables-bn (pike::state-space-bits-variables ss)))
    (remove-duplicates all-variables)

    (setf constraints `(,@(pike::constraint-knowledge-base-constraints (pike-constraint-knowledge-base ps))))
    ;;,@constraints-bn

    ;; Start a BDD manager
    (setf bm (pike::create-bdd-manager))

    ;; Create a BDD encoder (which connects a state space and BDD manager)
    (setf bde (pike::make-bdd-encoder :ss ss
                                      :bm bm))



    ;; Set up the encoder
    (pike::bdd-encoder-initialize bde
                                  all-variables
                                  :use-binaries t
                                  :variable-order-heuristic :force
                                  :constraints `(,@(pike::constraint-knowledge-base-constraints (pike-constraint-knowledge-base ps))
                                                   ,@constraints-bn))

    ;; Also store the weights
    (pike::bdd-set-weights bm weights)

    ;; Set the projection variables
    (let (assignments-bn)
      (dolist (v variables-bn)
        (dolist (val (pike::decision-variable-values v))
          (push (assignment v val) assignments-bn)))
      (pike::bdd-set-projection-variables bm (remove-duplicates (mapcar #'first
                                                                        (mapcar #'(lambda (a)
                                                                                    (pike::bdd-encoder-get-literal bde a))
                                                                                assignments-bn)))))


    ;; Encode the Bayesian network as a BDD
    (setf node-bn (pike::bdd-and bm
                                 (pike::bdd-encoder-get-variable-constraints-bdd bde)
                                 (pike::constraint->bdd bde (make-instance 'conjunction-constraint
                                                                           :conjuncts constraints-bn))))


    ;; Compile individual constraints to BDDs
    ;; (format t "Constraints -> BDD Nodes: ~%")
    ;; (push  (pike::bdd-encoder-get-variable-constraints-bdd bde) node-constraints)
    ;;(push node-bn node-constraints)
    (dolist (c constraints)
      (let (n)
        (setf n (pike::bdd-and bm
                               node-bn ;;(pike::bdd-encoder-get-variable-constraints-bdd bde)
                               (pike::constraint->bdd bde c)))
        ;; (setf n (pike::constraint->bdd bde c))
        ;; (format t "  ~a  :  ~a~%" n c)
        (push n node-constraints)))

    ;; Do an incremental conjunction!
    (setf key (pike::bdd-begin-apply bm
                                     node-constraints))
    ;; :heuristic-fn #'(lambda (n-op)
    ;;                   (pike::bdd-wmc-heuristic bm n-op :node-bn node-bn))



    ;; Keep running until completion, and save as a file for analysis.
    (time (test-bdd-incrementally-generate-until-completion bm key "data-graphs.json" :node-conjoin node-bn))
    (setf n-result (pike::bdd-retrieve-current-approximation bm key))
    (format t "Result has ~a nodes.~%" (pike::bdd-count-children n-result))

    ;; Sanity check: does it match the stock DFS case?
    (time (progn
            (setf n-check (pike::bdd-encoder-get-variable-constraints-bdd bde))
            (dolist (c `(,@(pike::constraint-knowledge-base-constraints (pike-constraint-knowledge-base ps))
                           ,@constraints-bn))
              (setf n-check (pike::bdd-and bm
                                           n-check
                                           (pike::constraint->bdd bde c))))))

    (cond
      ((eql n-result n-check)
       (format t "~%Results match the stock DFS case!~%"))
      (t
       (format t "~%~%WARNING!!!~%WARNING!!!~%WARNING!!!~%Results do NOT match the stock DFS case!!~%")))

    (values bde bm)))


(defun test-bdd-incrementally-generate-until-completion (bm key filename &key (node-conjoin nil) (log-result t) (log-wmc t) (wmc-iteration-mod 1) (account-for-measurement-time-offset t))
  (let ((i 0)
        (done? (pileup:heap-empty-p (pike::bdd-manager-queue-ops bm)))
        (ran-final-iteration? nil)
        (t_0 (get-internal-real-time))
        (t_offset 0)
        d
        n-result)

    (unless node-conjoin
      (setf node-conjoin (pike::get-bdd-terminal bm 1)))


    (format t "Beginning generation!~%")
    (loop while (or (not done?)
                    (not ran-final-iteration?)) do
         (let ((step-data (make-hash-table :test #'equal))
               n-underapprox n-overapprox
               wmc-underapprox wmc-overapprox)
           ;; Store the current result.
           (setf (gethash "iteration" step-data) i)
           (setf (gethash "t" step-data) (coerce (/ (- (get-internal-real-time) t_0 t_offset) internal-time-units-per-second) 'float))
           (format t "Step ~a~%" i)



           (let ((t-measurement-start (get-internal-real-time)))
             (when log-result
               ;; Get the result
               (setf n-result (pike::bdd-retrieve-current-approximation bm key))
               ;; Retrieve the graph (as dot)
               (setf (gethash "graph" step-data)
                     (with-output-to-string (s) (pike::bdd-to-dot-stream n-result s))))

             (when (and log-wmc
                        (= 0 (mod i wmc-iteration-mod)))
               ;; Also, compute an under and over approximation of the solution set.
               (setf n-underapprox (pike::bdd-compute-underapproximation bm key))
               (setf n-overapprox (pike::bdd-compute-overapproximation bm key))
               ;; Compute WMC on under and over approx
               (setf wmc-underapprox (pike::bdd-wmc bm (pike::bdd-project bm (pike::bdd-and bm
                                                                                            n-underapprox
                                                                                            node-conjoin))))
               (setf (gethash "wmc_lowerbound" step-data) wmc-underapprox)
               (setf wmc-overapprox (pike::bdd-wmc bm (pike::bdd-project bm (pike::bdd-and bm
                                                                                           n-overapprox
                                                                                           node-conjoin))))
               (setf (gethash "wmc_upperbound" step-data) wmc-overapprox))

             ;; NOTE: The following offset calibration isn't perfect! Performing these measurements could
             ;; help warm up some of the BDD projection caches etc, and actually speed up its performance
             ;; a bit. This would probably be a small effect though I think.
             (when account-for-measurement-time-offset
               (incf t_offset (- (get-internal-real-time) t-measurement-start))))


           ;; Store this step data
           (push step-data d)

           (when done?
             (setf ran-final-iteration? t))

           ;; If applicable, run again!
           (unless done?
             ;; Apply!
             (pike::bdd-apply-once bm)
             (incf i))

           ;; Are we done yet?
           (setf done? (pileup:heap-empty-p (pike::bdd-manager-queue-ops bm)))))

    ;; Generate the resulting JSON data
    (setf d (reverse d))
    (with-open-file (f filename :direction :output :if-exists :supersede :if-does-not-exist :create)
      (cl-json:encode-json d f))))
