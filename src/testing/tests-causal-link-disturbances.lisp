;;;; Copyright (c) 2015 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)

(in-package #:pike/test)

(def-suite :tests-causal-link-disturbances
    :description "Causal link disturbance-related tests for Pike"
    :in :tests-pike)

(in-suite :tests-causal-link-disturbances)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; disturbance-1
;;   Probably the simplest possible example of a disturbance.
;;   A single plan with two actions: consumer and a producer.
;;   The causal link is violated, which should violated the causal link.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun setup-disturbance-1 (&key (constraint-knowledge-base-type :piatms))
  (let (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "causal-links/pddl/thing-1.rmpl.tpn.out")
                            :domain-filename (pike-test-file "causal-links/pddl/simple-domain.pddl")
                            :problem-filename (pike-test-file "causal-links/pddl/simple-problem.pddl")
                            :constraint-knowledge-base-type constraint-knowledge-base-type
                            :debug-mode T))
    (setf ps (create-pike-session po))
    ps))

(test disturbance-1
  ;; Iterate over different possible compilation backends
  (dolist (constraint-knowledge-base-type `(:atms :piatms :sat :bdd))
    ;; Run it once without disturbances
    (let ((ps (setup-disturbance-1 :constraint-knowledge-base-type constraint-knowledge-base-type)))
      ;; Make sure it compiles properly
      (is-true (pike-compile ps))
      ;; Dispatch it, and check various execution times
      (run-pike-with-sim-dt ps)
      ;; Was it successful?
      (is-true (execution-succeeded? ps))
      ;; Check some execution times
      (is-true (event-executed-in-window? ps "ev4" 3.0 6.0)))

    ;; Now run it again... violating the causal link
    (let ((ps (setup-disturbance-1 :constraint-knowledge-base-type constraint-knowledge-base-type))
          (disturbances `((:on-event "ev2" :delete ("(p thing)")))))
      ;; Make sure it compiles properly
      (pike-compile ps)
      ;; Dispatch it, and check various execution times
      (run-pike-with-sim-dt ps :disturbances disturbances)
      ;; Was it successful?
      (is-true (execution-failed? ps)))))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; disturbance-2
;;   A producer action, followed by a choice of another producer or a no-op,
;;   followed by a consumer. This test makes sure that if p is violated after
;;   the first producer but before the choice, that Pike correctly chooses
;;   to run the second producer to recover and complete successfully.
;;
;;   If, however, both causal links are violated, then we should expect a failure.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun setup-disturbance-2 (&key (constraint-knowledge-base-type :piatms))
  (let (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "causal-links/pddl/thing-2.rmpl.tpn.out")
                            :domain-filename (pike-test-file "causal-links/pddl/simple-domain.pddl")
                            :problem-filename (pike-test-file "causal-links/pddl/simple-problem.pddl")
                            :constraint-knowledge-base-type constraint-knowledge-base-type
                            :debug-mode T))
    (setf ps (create-pike-session po))
    ps))

(test disturbance-2
  ;; Iterate over different possible compilation backends
  (dolist (constraint-knowledge-base-type `(:atms :piatms :sat :bdd))
    ;; Run the plan, violating only the first causal link
    (let ((ps (setup-disturbance-2 :constraint-knowledge-base-type constraint-knowledge-base-type))
          (disturbances `((:on-event "ev2" :delete ("(p thing)")))))
      ;; Make sure it compiles properly
      (is-true (pike-compile ps))
      ;; Dispatch it, and check various execution times
      (run-pike-with-sim-dt ps :disturbances disturbances)
      ;; Was it successful?
      (is-true (execution-succeeded? ps))
      (is-true (choice-was-made? ps "dv1" "c1")))

    ;; Run the plan again, violating both causal link
    (let ((ps (setup-disturbance-2 :constraint-knowledge-base-type constraint-knowledge-base-type))
          (disturbances `((:on-event "ev2" :delete ("(p thing)"))
                          (:on-event "ev8" :delete ("(p thing)")))))
      ;; Make sure it compiles properly
      (pike-compile ps)
      ;; Dispatch it, and check various execution times
      (run-pike-with-sim-dt ps :disturbances disturbances)
      ;; Was it successful?
      (is-true (execution-failed? ps))
      (is-true (choice-was-made? ps "dv1" "c1")))))







;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; disturbance-3
;;   A producer action, followed by a choice with a threat, followed by a
;;   choice with another producer, followed by a choice with the consumer.
;;
;;   We make the following checks:
;;      1.) Plan works
;;      2.) Plan works if consumer
;;      3.) Plan works if consumer & threat (second causal link chosen)
;;      4.) Plan works if consumer, no second causal link (first causal link chosen, no threat)
;;      5.) Plan fails if consumer, threat, and no second producer
;;      6.) Disturbances: If both (make-p) activities run, and both causal links are violated
;;          at same time (like after ev10), plan can still succeed (by not running choice)
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun setup-disturbance-3 (&key (constraint-knowledge-base-type :piatms))
  (let (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "causal-links/pddl/thing-4.rmpl.tpn.out")
                            :domain-filename (pike-test-file "causal-links/pddl/simple-domain.pddl")
                            :problem-filename (pike-test-file "causal-links/pddl/simple-problem.pddl")
                            :constraint-knowledge-base-type constraint-knowledge-base-type
                            :debug-mode T))
    (setf ps (create-pike-session po))
    ps))

(test disturbance-3
  ;; Iterate over different possible compilation backends
  (dolist (constraint-knowledge-base-type `(:atms :piatms :sat :bdd))
    ;; 1.) Above
    (let ((ps (setup-disturbance-3 :constraint-knowledge-base-type constraint-knowledge-base-type))
          (disturbances `()))

      ;; Make sure it compiles properly
      (is-true (pike-compile ps))
      ;; Dispatch it, and check various execution times
      (run-pike-with-sim-dt ps :disturbances disturbances)
      ;; Was it successful?
      (is-true (execution-succeeded? ps)))

    ;; 2.) Above
    (let ((ps (setup-disturbance-3 :constraint-knowledge-base-type constraint-knowledge-base-type))
          (disturbances `((:at-time 0.0 :commit (("dv3" "c0"))))))
      ;; Make sure it compiles properly
      (pike-compile ps)
      ;; Dispatch it, and check various execution times
      (run-pike-with-sim-dt ps :disturbances disturbances)
      ;; Was it successful?
      (is-true (execution-succeeded? ps)))

    ;; 3.) Above
    (let ((ps (setup-disturbance-3 :constraint-knowledge-base-type constraint-knowledge-base-type))
          (disturbances `((:at-time 0.0 :commit (("dv3" "c0")
                                                 ("dv1" "c1"))))))
      ;; Make sure it compiles properly
      (pike-compile ps)
      ;; Dispatch it, and check various execution times
      (run-pike-with-sim-dt ps :disturbances disturbances)
      ;; Was it successful?
      (is-true (execution-succeeded? ps))
      ;; Make sure second causal link is enforced
      (is-true (choice-was-made? ps "dv2" "c0")))

    ;; 4.) Above
    (let ((ps (setup-disturbance-3 :constraint-knowledge-base-type constraint-knowledge-base-type))
          (disturbances `((:at-time 0.0 :commit (("dv3" "c0")
                                                 ("dv2" "c1"))))))
      ;; Make sure it compiles properly
      (pike-compile ps)
      ;; Dispatch it, and check various execution times
      (run-pike-with-sim-dt ps :disturbances disturbances)
      ;; Was it successful?
      (is-true (execution-succeeded? ps))
      ;; Make sure the the threat wasn't chosen
      (is-true (choice-was-made? ps "dv1" "c0")))

    ;; 5.) Above
    (let ((ps (setup-disturbance-3 :constraint-knowledge-base-type constraint-knowledge-base-type))
          (disturbances `((:at-time 0.0 :commit (("dv3" "c0")
                                                 ("dv1" "c1")
                                                 ("dv2" "c1"))))))
      ;; Make sure it compiles properly
      (pike-compile ps)
      ;; Dispatch it, and check various execution times
      (run-pike-with-sim-dt ps :disturbances disturbances)
      ;; Was it successful?
      (is-true (execution-failed? ps)))

    ;; 6.) Above
    (let ((ps (setup-disturbance-3 :constraint-knowledge-base-type constraint-knowledge-base-type))
          (disturbances `((:at-time 0.0 :commit (("dv1" "c0")
                                                 ("dv2" "c0")))
                          (:on-event "ev10" :delete ("(p thing)")))))
      ;; Make sure it compiles properly
      (pike-compile ps)
      ;; Dispatch it, and check various execution times
      (run-pike-with-sim-dt ps :disturbances disturbances)
      ;; Was it successful?
      (is-true (execution-succeeded? ps))
      ;; Did the executive choose dv3 = c1, since the
      ;; causal link can't be satisifed?
      (is-true (choice-was-made? ps "dv3" "c1")))))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; disturbance-journal-ex
;;  A scenario with a producer, a choice producer, and a choice consumer, and
;;  a potentially-ordered threat.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun setup-disturbance-journal-ex (&key (constraint-knowledge-base-type :piatms))
  (let (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "causal-links/pddl/tpn-journal-example.mod.tpn")
                            :domain-filename (pike-test-file "causal-links/pddl/simple-domain.pddl")
                            :problem-filename (pike-test-file "causal-links/pddl/simple-problem.pddl")
                            :constraint-knowledge-base-type constraint-knowledge-base-type
                            :debug-mode T))
    (setf ps (create-pike-session po))
    ps))

(defun setup-disturbance-journal-with-locking-ex (&key (constraint-knowledge-base-type :piatms))
  (let (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "causal-links/pddl/tpn-journal-example.mod.tpn")
                            :domain-filename (pike-test-file "causal-links/pddl/simple-plus-locking-domain.pddl")
                            :problem-filename (pike-test-file "causal-links/pddl/simple-plus-locking-problem.pddl")
                            :constraint-knowledge-base-type constraint-knowledge-base-type
                            :debug-mode T))
    (setf ps (create-pike-session po))
    ps))

(test disturbance-journal-ex ()
      ;; Iterate over different possible compilation backends
      (dolist (constraint-knowledge-base-type `(:atms :piatms :sat :bdd))
        (let ((ps (setup-disturbance-journal-ex :constraint-knowledge-base-type constraint-knowledge-base-type))
              (disturbances `()))
          ;; Compile it
          (pike-compile ps)
          ;; Dispatch it
          (run-pike-with-sim-dt ps :disturbances disturbances)
          ;; Was it successful?
          (is-true (execution-succeeded? ps)))

        (let ((ps (setup-disturbance-journal-ex :constraint-knowledge-base-type constraint-knowledge-base-type))
              (disturbances `((:at-time 0.0 :commit (("z" "1"))))))
          ;; Compile it
          (pike-compile ps)
          ;; Dispatch it
          (run-pike-with-sim-dt ps :disturbances disturbances)
          ;; Was it successful?
          (is-true (execution-succeeded? ps)))

        (let ((ps (setup-disturbance-journal-ex :constraint-knowledge-base-type constraint-knowledge-base-type))
              (disturbances `((:at-time 0.0 :commit (("z" "1")
                                                     ("x" "1"))))))
          ;; Compile it
          (pike-compile ps)
          ;; Dispatch it
          (run-pike-with-sim-dt ps :disturbances disturbances)
          ;; Was it successful?
          (is-true (execution-succeeded? ps)))

        (let ((ps (setup-disturbance-journal-ex :constraint-knowledge-base-type constraint-knowledge-base-type))
              (disturbances `((:at-time 0.0 :commit (("z" "1")
                                                     ;;("y" "1")
                                                     ;;("s_{(p thing),e_c}" "e_P1")
                                                     ;;("o_{(p thing),e_c,e_P1,ev19}" "+1")
                                                     ("x" "1"))))))

          ;; Compile it
          (pike-compile ps)
          ;; Dispatch it
          (run-pike-with-sim-dt ps :disturbances disturbances)
          ;; Was it successful?
          (is-true (execution-succeeded? ps)))))





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; disturbance-producer-nondominance-2
;;
;;  TODO: This also tests causal link extraction, so there should be some
;;  corresponding tests there.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun setup-disturbance-producer-nondominance-2 (&key (constraint-knowledge-base-type :piatms))
  (let (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "causal-links/pddl/tpn-producer-nondominance-2.mod.tpn")
                            :domain-filename (pike-test-file "causal-links/pddl/simple-domain.pddl")
                            :problem-filename (pike-test-file "causal-links/pddl/simple-problem.pddl")
                            :constraint-knowledge-base-type constraint-knowledge-base-type
                            :debug-mode T))
    (setf ps (create-pike-session po))
    ps))


;; (test disturbance-producer-nondominance-2 ()
;;   (let ((ps (setup-disturbance-journal-ex))
;;         (disturbances `()))
;;     ;; Compile it
;;     (pike-compile ps)
;;     ;; Dispatch it
;;     (run-pike-with-sim-dt ps :disturbances disturbances)
;;     ;; Was it successful?
;;     (is-true (execution-succeeded? ps)))

;;   (let ((ps (setup-disturbance-journal-ex))
;;         (disturbances `((:at-time 0.0 :commit (("z" "1"))))))
;;     ;; Compile it
;;     (pike-compile ps)
;;     ;; Dispatch it
;;     (run-pike-with-sim-dt ps :disturbances disturbances)
;;     ;; Was it successful?
;;     (is-true (execution-succeeded? ps)))

;;   (let ((ps (setup-disturbance-journal-ex))
;;         (disturbances `((:at-time 0.0 :commit (("z" "1")
;;                                                ("x" "1"))))))
;;     ;; Compile it
;;     (pike-compile ps)
;;     ;; Dispatch it
;;     (run-pike-with-sim-dt ps :disturbances disturbances)
;;     ;; Was it successful?
;;     (is-true (execution-succeeded? ps)))

;;   (let ((ps (setup-disturbance-producer-nondominance-2))
;;         (disturbances `((:at-time 0.0 :commit (("z" "1")
;;                                                ("x" "1")
;;                                                ("y" "1")
;;                                                ;;("s_{(p thing),e_c}" "e_P1")
;;                                                ;; ("o_{(p thing),e_c,e_P1,e_T1}" "+1")
;;                                                ;; ("o_{(p thing),e_c,e_P1,ev19}" "+1")
;;                                                )))))
;;     ;; Compile it
;;     (pike-compile ps)
;;     ;; Dispatch it
;;     (run-pike-with-sim-dt ps :disturbances disturbances)
;;     ;; Was it successful?
;;     (is-true (execution-succeeded? ps))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; disturbance-producer-nondominance-2
;;
;;  TODO: This also tests causal link extraction, so there should be some
;;  corresponding tests there.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun setup-disturbance-lock-1 (&key (constraint-knowledge-base-type :piatms))
  (let (po ps)
    (setf po (make-instance 'pike-options
                            :plan-filename (pike-test-file "causal-links/pddl/locking-simple-1.mod.tpn")
                            :domain-filename (pike-test-file "causal-links/pddl/locking-domain.pddl")
                            :problem-filename (pike-test-file "causal-links/pddl/locking-problem.pddl")
                            :constraint-knowledge-base-type constraint-knowledge-base-type
                            :debug-mode T))
    (setf ps (create-pike-session po))
    ps))
