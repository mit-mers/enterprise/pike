;;;; Copyright (c) 2016 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)


(declaim (inline slice-label))
(defun slice-label (L assignment ss)
  "Given a label L (a list of environments), take a slice with the
   given assignment, and then remove it."
  (declare (optimize (speed 3)))
  (declare (type list L)
           (type state-space-bits ss))
  (let ((L-sat nil)
        (ea (make-environment-from-assignments ss (list assignment))))
    (declare (type list L-sat)
             (type environment ea))
    (dolist (ei L)
      (when (subsumes? ea ei ss)
        (setf L-sat (add-minimal L-sat (remove-assignments-from-environment ei ea) ss))))
    L-sat))


(declaim (inline remove-subsumed-environments))
(defun remove-subsumed-environments (L-to-remove L ss)
  "Remove any environments from L-to-remove that are
  subsumed by any environment in L."
  (declare (optimize (speed 3)))
  (declare (type list L-to-remove)
           (type list L)
           (type state-space-bits ss))
  ;; Return a reference to the new list; caller must assign to it
  ;; (in case head of list changes)
  (delete-if #'(lambda (ei)
                 (declare (optimize (speed 3)))
                 (declare (type environment ei))
                 (some #'(lambda (el)
                           (declare (optimize (speed 3)))
                           (declare (type environment el))
                           (subsumes? el ei ss))
                       L))
             L-to-remove))


(declaim (inline cross-product))
(defun cross-product (Ls ss)
  "Take the cross product of each of the labels L in Ls."
  (declare (optimize (speed 3))
           (type list Ls)
           (type state-space-bits ss))

  ;; Optional Optimization: sort Ls by label size. That way
  ;; if there's an empty label first, it'll be found.
  ;; NOTE: this will destructively change Ls!
  (setf Ls (sort Ls #'< :key #'length))

  ;; Take the cross product incrementally.
  (let ((L-cross (list (make-environment ss))))
    (dolist (L Ls)
      (let ((L-cross-new nil))
        ;; Take the cross product of L-cross and and L.
        ;; This will later become the next L-cross.
        (dolist (ec L-cross)
          (declare (type environment ec))
          (dolist (el L)
            (declare (type environment el))
            (let ((em (environment-union ec el ss)))
              (declare (type environment em))
              (when (env-valid? ss em)
                (setf L-cross-new (add-minimal L-cross-new em ss))))))
        ;; Update L-cross
        (setf L-cross L-cross-new)))
    L-cross))


(declaim (inline manifests-conflict?))
(defun manifest-conflict? (ei ec ss)
  "Returns true iif the environment ei manifests the
   conflict represented by ec."
  (declare (optimize (speed 3)))
  (declare (type environment ei)
           (type environment ec)
           (type state-space-bits ss))
  (subsumes? ec ei ss))


(declaim (inline resolves-conflict))
(defun resolves-conflict? (ei ec ss)
  "Returns true iif the environment ei manifests the
   conflict represented by ec."
  (declare (optimize (speed 3)))
  (declare (type environment ei)
           (type environment ec)
           (type state-space-bits ss))
  (not (env-valid? ss (environment-union ei ec ss))))


;; TODO: wonder if this is worth putting inline?
(defun generate-constituent-kernels (ei ss)
  "Returns a label consisting of a number of environments,
  namely all of the constituent kernels of ei. Returns a list."
  (declare #+:sbcl (sb-ext:muffle-conditions sb-ext:compiler-note))
  (declare (optimize (speed 3)))
  (declare (type environment ei)
           (type state-space-bits ss))
  (let ((L-kernels nil)
        (assignments (get-assignments-from-environment ss ei)))
    (declare (type list L-kernels)
             (type list assignments))
    (dolist (a assignments)
      (let ((var (assignment-variable a))
            (val (assignment-value a)))
        (dolist (v (decision-variable-values var))
          (when (not (equal val v))
            (push (make-environment-from-assignments ss (list (assignment var v))) L-kernels)))))

    L-kernels))



(defun split-label-on-conflict (L ec ss)
  "Split the contents of this label on the conflict. This
  essentially makes it so that all scenarios represented by this label
  provably resolve the conflict.

  Returns a new reference to the label (a list)."
  (declare (optimize (speed 3)))
  (declare (type list L)
           (type environment ec)
           (type state-space-bits ss))
  (let ((L-new nil)
        (constituent-kernels (generate-constituent-kernels ec ss))) ;; TODO: compute only if needed? A bit of an expensive operation to compute these.
    (declare (type list L-new)
             (type list constituent-kernels))
    (dolist (ei L)
      (cond
        ((manifest-conflict? ei ec ss) T) ;; If it manifests it, don't add it to L-new
        ((resolves-conflict? ei ec ss) (setf L-new (add-minimal L-new ei ss))) ;; Resolves: pass through. ;; TODO: add minimally??
        (T ;; Ambiguous; split on conflict.
         (dolist (eck constituent-kernels)
           (declare (type environment eck))
           (let ((em (environment-union ei eck ss)))

             (declare (type environment em))

             (when (env-valid? ss em)
               (push em L-new)))))))
    L-new))


(declaim (inline compute-label-completions))
(defun compute-label-completions (L var ss)
  "Given a label L, compute completions for it and
  return them as a new label (a list of environments)."
  (declare (optimize (speed 3)))
  (declare (type list L)
           (type decision-variable var)
           (type state-space-bits ss))
  (let ((L-slices nil))
    (declare (type list L-slices))
    ;; Slice the label every which-way based on the domain of the variable
    (dolist (val (decision-variable-values var))
      (let* ((assignment (assignment var val))
             (L-slice (slice-label L assignment ss)))
        (declare (type list L-slice))
        ;; Optimization! Remove any subsumed elements of any L-slice. This
        ;; could potentially save a lot of time, since they're all going to be
        ;; cross producted together.

        ;; Below is no longer required
        ;;(setf L-slice (remove-subsumed-environments L-slice L ss))

        (push L-slice L-slices)))
    ;; Take the cross product of all these slices, and return them!
    (cross-product L-slices ss)))


(declaim (inline get-variables-referenced-in-environment))
(defun get-variables-referenced-in-environment (e ss)
  "Returns a list of variables referenced in this assignment.
   Unfortunately, this isn't very efficient with bit vectors; it
   involves iterating over every bit in the vector. Bit vectors
   are great for somethings, but haven't figured out a great way
   out of this!"
  (declare #+:sbcl (sb-ext:muffle-conditions sb-ext:compiler-note))
  (declare (optimize (speed 3)))
  (declare (type state-space-bits ss)
           (type environment e))
  (let ((assignments (get-assignments-from-environment ss e))
        (vars nil))
    (declare (type list assignments)
             (type list vars))
    (dolist (a assignments)
      (push (assignment-variable a) vars))
    vars))


(defun add-and-compactify (L L-to-add ss)
  "Add the environments in L-to-add minimally to L,
  also compacting and possibly adding those additional
  compactions to L.

  Returns L, the reference to the new label, as well as
  L-added, a list of all labels actually added to L."
  (declare #+:sbcl (sb-ext:muffle-conditions sb-ext:compiler-note))
  (declare (optimize (speed 3)))
  (declare (type list L)
           (type list L-to-add)
           (type state-space-bits ss))
  (let ((L-added nil)
        (Q nil)
        (added? nil))
    (declare (type list L-added)
             (type list Q)
             (type boolean added?))
    ;; First, add each of L-to-add to L, keeping track of which
    ;; ones were actually added.
    (dolist (ei L-to-add)
      (declare (type environment ei))
      (multiple-value-setq (L added?) (add-minimal L ei ss))
      (when added?
        (setf L-added (add-minimal L-added ei ss)))) ;; Should definitely be ad minimal, and not push
    ;; For any variables referenced in any ei just added,
    ;; push those variables back onto the Q.
    (dolist (ei L-added)
      (declare (type environment ei))
      (dolist (var (get-variables-referenced-in-environment ei ss))
        (declare (type decision-variable var))
        (unless (member var Q)
          (push var Q))))

    ;; Now, find completions (and keep repeating) until we reach a fixed point.
    (loop while (> (length Q) 0) do
         (let ((L-just-added nil))
           (declare (type list L-just-added))
           ;; Find the set of completions over this variable
           (setf L-to-add (compute-label-completions L (pop Q) ss))
           ;; Add each env in L-to-add to L, keeping track of which
           ;; ones were actually added
           (dolist (ei L-to-add)
             (declare (type environment ei))
             (multiple-value-setq (L added?) (add-minimal L ei ss))
             (when added?
               (setf L-added (add-minimal L-added ei ss))
               (push ei L-just-added)))
           ;; For any variables referenced in any ei just added,
           ;; push those variables back onto the Q.
           (dolist (ei L-just-added)
             (declare (type environment ei))
             (dolist (var (get-variables-referenced-in-environment ei ss))
               (declare (type decision-variable var))
               (unless (member var Q)
                 (push var Q))))))

    ;; Return both the new list L, as well as the environments
    ;; actually added.
    (values L L-added)))




(defun fully-compactify (L ss)
  "Fully compactify the label L, by finding completions
  over all variables. Keeps finding completions until we reach
  a fixed point, meaning we're maximally compact.

  Returns a reference to the new label L (who's head could have changed).
  "
  (declare #+:sbcl (sb-ext:muffle-conditions sb-ext:compiler-note))
  (declare (optimize (speed 3)))
  (declare (type list L)
           (type state-space-bits ss))
  ;; Initialize our Q to all of the variables.
  (let ((Q (copy-seq (state-space-bits-variables ss))))
    (declare (type list Q))
    (loop while (> (length Q) 0) do
         (let ((L-to-add nil)
               (L-added nil)
               (added? nil))
           (declare (type list L-to-add)
                    (type list L-added)
                    (type boolean added?))
           ;; Find the set of completions over this variable
           (setf L-to-add (compute-label-completions L (pop Q) ss))
           ;; Add each env in L-to-add to L, keeping track of which
           ;; ones were actually added
           (dolist (ei L-to-add)
             (declare (type environment ei))
             (multiple-value-setq (L added?) (add-minimal L ei ss))
             (when added?
               (push ei L-added)))
           ;; For any variables referenced in any ei just added,
           ;; push those variables back onto the Q.
           (dolist (ei L-added)
             (declare (type environment ei))
             (dolist (var (get-variables-referenced-in-environment ei ss))
               (declare (type decision-variable var))
               (unless (member var Q)
                 (push var Q))))))

    L))
