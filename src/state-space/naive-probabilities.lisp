;;;; Copyright (c) 2015 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)

;;; Simple implementation of independent variable / marginal distributions!

;; NOTE THIS FILE IS OBSOLETE - USE BAYESIAN NETWORKS NOW!

(defstruct (probability-tables)
  (p-table (make-hash-table :test #'eql)))


(declaim (inline add-marginal))
(defun add-marginal (assignment prob p-tables)
  "Add an entry into the probability tables, like P(x=2) = 0.3"
  (declare (optimize (speed 3)))
  (declare (type decision-variable-assignment assignment)
           (type probability-tables p-tables))
  (setf (gethash assignment (probability-tables-p-table p-tables)) prob))


(declaim (inline get-marginal))
(defun get-marginal (assignment p-tables)
  "Given an assignment, return it's marginal probability."
  (declare (optimize (speed 3)))
  (declare (type decision-variable-assignment assignment)
           (type probability-tables p-tables))
  (let (p found?)

    (multiple-value-setq (p found?) (gethash assignment (probability-tables-p-table p-tables)))
    (unless found?
      (error "No probability associated with ~a!" assignment))
    p))


(defun set-uniform-independent-probabilities (p-tables ss &key (allow-fractions nil))
  (with-slots (variables) ss
    (dolist (var variables)
      (let* ((domain (decision-variable-values var))
             (N (length domain)))
        (dolist (d domain)
          (if allow-fractions
              (add-marginal (assignment var d) (/ 1 N) p-tables)
              (add-marginal (assignment var d) (float (/ 1 N)) p-tables)))))))


(defun compute-environment-probability-naive (p-tables ss env)
  "Makes a strong independence assumption."
  (declare #+:sbcl (sb-ext:muffle-conditions sb-ext:compiler-note))
  (declare (optimize (speed 3))
           (type environment env))
  (let ((p 1)) ;; 1.d0
    ;;(declare (type double-float p))
    (dolist (assignment (get-assignments-from-environment ss env))
      ;; (setf p (* p  (coerce (get-marginal assignment p-tables) 'double-float)))
      (setf p (* p (get-marginal assignment p-tables))))
    p))
