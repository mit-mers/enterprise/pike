;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)

;; TODO - cleanup accessor names

(defclass state-space ()
  ((variables
    :type list
    :initform nil
    :accessor state-space-variables)))

(defclass decision-variable ()
  ((name
    :initarg :name
    :accessor decision-variable-name)
   (values
    :type list
    :initarg :domain
    :initform nil
    :accessor decision-variable-values)
   (controllable?
    :type (or nil T)
    :initarg :controllable?
    :initform T
    :accessor controllable?)
   (assignments
    :type hash-table
    :initform (make-hash-table :test #'equal)
    :accessor decision-variable-assignments)
   (utilities-map
    :type hash-table
    :initform (make-hash-table :test #'eql))))

(defgeneric uncontrollable? (obj))
(defmethod uncontrollable? ((dv decision-variable))
  "Convenience method for querying uncontrollability"
  (not (controllable? dv)))


(defclass decision-variable-assignment ()
  ((variable
    :type decision-variable
    :initarg :variable
    :accessor assignment-variable)
   (value
    :initarg :value
    :accessor assignment-value)))


(defmethod print-object ((dv decision-variable) s)
  (with-slots (name) dv
    (format s "#<DECISION-VARIABLE \"~a\">" name)))

(defmethod print-object ((a decision-variable-assignment) s)
  (with-slots (variable value) a
    (format s "~a=~a" (decision-variable-name variable) value)))

(defgeneric assignment (variable value))
(defmethod assignment ((dv decision-variable) value)
  (with-slots (assignments) dv
    (let (assignment found)
      (multiple-value-setq (assignment found) (gethash value assignments))
      (unless found
        (error "Couldn't find value \"~a\" in variable's domain!" value))
      assignment)))

(defgeneric assignment-utility (obj))
(defmethod assignment-utility ((a decision-variable-assignment))
  (with-slots (utilities-map) (assignment-variable a)
    (gethash a utilities-map)))


;; TODO check assignment valid value?


(defmethod initialize-instance :after ((dv decision-variable) &key (utilities nil))
  (with-slots (values assignments utilities-map) dv
    ;; Set up assignments
    (dolist (v values)
      (setf (gethash v assignments) (make-instance 'decision-variable-assignment :variable dv :value v)))
    ;; Set up utilities
    (when utilities
      (assert (= (length values) (length utilities)))
      (mapcar #'(lambda (v u)
                  (setf (gethash (assignment dv v) utilities-map) u))
              values utilities))))


;; Add a domain value.
;; NOTE -- this doesn't work in cases where the variable has already been added to the
;; state space -- won't adjust the bit layout (but wouldn't be hard to fix).
(defmethod add-new-domain-value ((dv decision-variable) value)
  (with-slots (values assignments) dv
    (push value values)
    (setf (gethash value assignments) (make-instance 'decision-variable-assignment :variable dv :value value))))



(defgeneric add-variable! (ss variable))


(defgeneric make-environment-from-assignments (ss assignments))

;; (declaim (inline subsumes?))
;; (defgeneric subsumes? (e1 e2 ss)

;;   )


;; (declaim (inline environment-union))
;; (defgeneric environment-union (e1 e2 ss)

;;   )



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Bit vector implementations
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(deftype environment () 'simple-bit-vector)


(defstruct (state-space-bits)
  (variables nil
           :type list)

  (n-bits 0
          :type fixnum)

  (assignment-to-bit-index (make-hash-table)
                           :type hash-table)

  (bit-index-to-assignment (make-array 0 :adjustable T))

  (zero-vector (make-array 0 :element-type 'bit :adjustable nil)
               :type environment)

  ;; These ones below are useful for LVS operations
  (variable-conflict-environments nil
                             :type list)
  (bit-index-to-all-variable-assignment-environments (make-array 0 :adjustable T))

  ;; Scratch space for more efficient computations (less memory allocation)
  (temp-scratch-space-1 (make-array 0 :element-type 'bit :adjustable nil)
             :type environment)
  (temp-scratch-space-2 (make-array 0 :element-type 'bit :adjustable nil)
                      :type environment))



(declaim (inline make-environment))
(defun make-environment (ssb)
  (declare (optimize (speed 3)))
  (declare (type state-space-bits ssb))
  (with-slots (n-bits) ssb
    (make-array n-bits :element-type 'bit :initial-element 0 :adjustable nil)))




(defmethod add-variable! ((ss state-space-bits) (variable decision-variable))
  (with-slots (n-bits variables zero-vector assignment-to-bit-index
                      bit-index-to-assignment temp-scratch-space-1 temp-scratch-space-2
                      variable-conflict-environments bit-index-to-all-variable-assignment-environments) ss

    (push variable variables)

    (adjust-array bit-index-to-assignment (+ n-bits (hash-table-count (decision-variable-assignments variable))))

    (loop for assignment being the hash-values of (decision-variable-assignments variable) do
         (setf (gethash assignment assignment-to-bit-index) n-bits)
         (setf (aref bit-index-to-assignment n-bits) assignment)
         (incf n-bits))

    ;; Re-allocate the zero vector and scratch space (need new sizes)
    (setf zero-vector (make-environment ss))
    (setf temp-scratch-space-1 (make-environment ss))
    (setf temp-scratch-space-2 (make-environment ss))


    ;; Make an env like {x=1, x=2, x=3} and add it as a
    ;; variable conflict nogood.
    ;; But first, make all the previous environments bigger.
    (setf variable-conflict-environments nil)
    (dolist (v variables)
      (push (make-environment-from-assignments
             ss
             (loop for assignment being the hash-values of (decision-variable-assignments v)
                collecting assignment))
            variable-conflict-environments))


    ;; Make a list of environments like ({x=1}, {x=2}, {x=3}) and
    ;; associate it with each one, so that we can map each assignment
    ;; to all other assignments for that variable.
    (setf bit-index-to-all-variable-assignment-environments (make-array n-bits :adjustable T))
    (dolist (v variables)
      (let (all-variable-assignment-environments)
        ;; First, construct this list
        (loop for assignment being the hash-values of (decision-variable-assignments v) do
           ;; NOTE: Added on 12/6/2018: a somewhat HACK that doesn't add the \circ
           ;; domain value that represents unassignment in activity. Adding this here because
           ;; it messes up finding completions in APSP type computations.
             (unless (equal (assignment-value assignment) "∘") ;; Here
               (push (make-environment-from-assignments
                      ss
                      (list assignment))
                     all-variable-assignment-environments)))
        ;; Now, associate it with variable using the array mapping.
        (adjust-array bit-index-to-all-variable-assignment-environments n-bits)
        (loop for assignment being the hash-values of (decision-variable-assignments v) do
             (setf (aref bit-index-to-all-variable-assignment-environments
                         (gethash assignment assignment-to-bit-index))
                   all-variable-assignment-environments))))))


(defun print-environment (env ss s)
  "Pretty-print the given environment to the given stream (T for standard out, nil to return a string)."
  (declare (type environment env)
           (type state-space-bits ss))
  (with-slots (bit-index-to-assignment n-bits) ss
    (format s "{~{~a~^, ~}}"
            (loop for i from 0 to (- n-bits 1)
                 when (= (aref env i) 1)
                 collect (aref bit-index-to-assignment i)))))


(defun print-label (L ss s)
  "Pretty print a label to the given stream (a label is a set of environments)."
  (declare (type state-space-bits ss))
  (format s "[ ~{~a~^,~%  ~} ]"
          (loop for ei in L collect (print-environment ei ss nil))))


(defmethod make-environment-from-assignments ((ss state-space-bits) (assignments list))
  "Given a list of decision variable assignments, convert it to the
   corresponding environment representation."
  (declare (optimize (speed 3)))
  (with-slots (n-bits assignment-to-bit-index) ss
    (let ((env (make-environment ss))
          idx found)
      (declare (type environment env))
      (dolist (assignment assignments)
        (multiple-value-setq (idx found) (gethash assignment assignment-to-bit-index))
        (unless found
          (error "Couldn't find assignment ~a in variable's assignment domain!" assignment))
        (setf (bit env idx) 1))
      env)))


(declaim (inline get-assignments-from-environment))
(defun get-assignments-from-environment (ss environment)
  "Does the opposite of make-environment-from-assignments. Given an environment,
   return the resulting list of assignments that it efficiently represents."
  (declare #+:sbcl (sb-ext:muffle-conditions sb-ext:compiler-note))
  (declare (optimize (speed 3)))
  (declare (type environment environment)
           (type state-space-bits ss))
  (with-slots (n-bits bit-index-to-assignment) ss
    (let (assignments)
      (declare (type list assignments))
      ;; For each "1" bit inside the environment bit vector,
      ;; add the corresponding assignment to the list.
      (dotimes (i n-bits)
        (when (= (sbit environment i) 1)
          (push (aref bit-index-to-assignment i)
                assignments)))
      assignments)))


;; TODO: document better.
;; Must be called whenever the size of an environment changes -
;; i.e., by adding variables, etc. Otherwise, the bit-ior
;; operations won't match up due to different file sizes.
(declaim (inline recast-environment-bit))
(defun recast-environment-bit (e ssb)
  (declare (optimize (speed 3)))
  (declare (type environment e)
           (type state-space-bits ssb))
  (with-slots (n-bits) ssb
    ;; Create an adjustable copy of e, then resize it, then or-it until a new environment.
    (let ((e-tmp (make-array (first (array-dimensions e)) :element-type 'bit :initial-element 0 :adjustable T))
          (e-new (make-environment ssb)))
      (bit-ior e-tmp e e-tmp)     ;; e-tmp is now e, but adjustable
      (adjust-array e-tmp n-bits) ;; Make it bigger
      (bit-ior e-tmp e-new e-new) ;; bit it e-new and return it
      e-new)))


(declaim (inline recast-environment))
(defun recast-environment (e ss)
  (declare (optimize (speed 3)))
  ;; TODO this is maybe more "generic"?
  (recast-environment-bit e ss))





(declaim (inline subsumes?-bit))
(defun subsumes?-bit (e1 e2 ssb)
  "High speed inline for performing subsumption tests
   with bit vectors representing environments.

   Note that this seems to be SIGNIFICANTLY slower
   when using object oriented defmethods."
  (declare (optimize (speed 3)))
  (declare (type environment e1 e2)
           (type state-space-bits ssb))
  (with-slots (zero-vector temp-scratch-space-1) ssb
    (bit-andc2 e1 e2 temp-scratch-space-1)
    (equal zero-vector temp-scratch-space-1)))

(declaim (inline subsumes?))
(defun subsumes? (e1 e2 ss)
  ;; In the future, if we add different types: manually
  ;; specialize on type without using CLOS's heavy duty
  ;; (and very slow) defmethod type inference.
  (declare (optimize (speed 3)))
  (subsumes?-bit e1 e2 ss))



(declaim (inline environment-union-bit))
(defun environment-union-bit (e1 e2 ssb)
  (declare (optimize (speed 3)))
  (declare (ignore ssb))
  (declare (type environment e1 e2))
  (bit-ior e1 e2))

(declaim (inline environment-union))
(defun environment-union (e1 e2 ssb)
  (declare (optimize (speed 3)))
  (environment-union-bit e1 e2 ssb))




(declaim (inline environment-union-multiple))
(defun environment-union-multiple (ssb &rest environments)
  (declare (optimize (speed 3)))
  (declare (type state-space-bits ssb)
           (type list environments))
  (let ((e-out (make-environment ssb)))
   (dolist (e environments)
     (declare (type environment e))
     (bit-ior e-out e e-out))
    e-out))


(declaim (inline environment-union-in-place-bit))
(defun environment-union-in-place-bit (e1 e2 eresult ssb)
  (declare (optimize (speed 3)))
  (declare (ignore ssb))
  (declare (type environment e1 e2 eresult))
  (bit-ior e1 e2 eresult))


(declaim (inline environment-union-in-place))
(defun environment-union-in-place (e1 e2 eresult ssb)
  (declare (optimize (speed 3)))
  (environment-union-in-place-bit e1 e2 eresult ssb))



(declaim (inline env-valid?-bit))
(defun env-valid?-bit (ss e-b)
  (declare (optimize (speed 3)))
  (declare (type state-space-bits ss)
           (type environment e-b))
  (with-slots (variable-conflict-environments temp-scratch-space-2) ss
    (dolist (conflict variable-conflict-environments)
      (declare (type environment conflict))
      (bit-and conflict e-b temp-scratch-space-2)
      (unless (< (count 1 temp-scratch-space-2) 2)
        (return-from env-valid?-bit nil)))
    T))

(declaim (inline env-valid?))
(defun env-valid? (ss e-b)
  "Helper method to check and see if an environment is valid.
   I.e., doesn't do anything silly like assign both x=1 and x=2."
  (declare (optimize (speed 3)))
  (env-valid?-bit ss e-b))




(declaim (inline environment-equal?-bit))
(defun environment-equal?-bit (e1 e2 ssb)
  (declare (optimize (speed 3)))
  "Determines (quickly!) if two environments
   are identical -- i.e., they contain the same
   assignments."
  (declare (ignore ssb))
  (declare (type environment e1 e2)
           (type state-space-bits ssb))
  (equal e1 e2))

(declaim (inline environment-equal?))
(defun environment-equal? (e1 e2 ss)
  (declare (optimize (speed 3)))
  ;; In the future, if we add different types: manually
  ;; specialize on type without using CLOS's heavy duty
  ;; (and very slow) defmethod type inference.
  (environment-equal?-bit e1 e2 ss))



(declaim (inline is-empty-environment?-bit))
(defun is-empty-environment?-bit (e ssb)
  "Determines (quickly!) if this is the univeral or
   empty environment, {}."
  (declare (optimize (speed 3)))
  (declare (type environment e)
           (type state-space-bits ssb))
  (with-slots (zero-vector) ssb
    (equal e zero-vector)))


(declaim (inline is-empty-environment?))
(defun is-empty-environment? (e ss)
  (declare (optimize (speed 3)))
  ;; In the future, if we add different types: manually
  ;; specialize on type without using CLOS's heavy duty
  ;; (and very slow) defmethod type inference.
  (is-empty-environment?-bit e ss))




(declaim (inline remove-assignments-from-environment-bit))
(defun remove-assignments-from-environment!-bit (e e-a)
  "Determines (quickly!) if this is the univeral or
   empty environment, {}."
  (declare (optimize (speed 3)))
  (declare (type environment e)
           (type environment e-a))
  (bit-andc2 e e-a T))

(declaim (inline remove-assignments-from-environment))
(defun remove-assignments-from-environment! (e e-a)
  "Removes the assignments, represented as an environment e-a, from
  the environment e. Modifies e."
  (declare (optimize (speed 3)))
  (remove-assignments-from-environment!-bit e e-a))


(declaim (inline remove-assignments-from-environment-bit))
(defun remove-assignments-from-environment-bit (e e-a)
  "Determines (quickly!) if this is the univeral or
   empty environment, {}."
  (declare (optimize (speed 3)))
  (declare (type environment e)
           (type environment e-a))
  (bit-andc2 e e-a))


(declaim (inline remove-assignments-from-environment))
(defun remove-assignments-from-environment (e e-a)
  "Removes the assignments, represented as an environment e-a, from
  the environment e. Returns a copy."
  (declare (optimize (speed 3)))
  (remove-assignments-from-environment-bit e e-a))


(declaim (inline add-minimal-bit))
(defun add-minimal-bit (L e ss)
  (declare (optimize (speed 3)))
  (declare (type state-space-bits ss))
  (declare (type list L))
  (declare (type environment e))
  ;; Don't do anything if any e-i in L subsumes e.
  (dolist (e-i L)
    (declare (type environment e-i))
    (when (subsumes? e-i e ss)
      (return-from add-minimal-bit (values L nil))))
  ;; Remove from L anything that e subsumes
  (setf L (delete-if #'(lambda (e-i)
                         (declare (optimize (speed 3)))
                         (declare (type environment e-i))
                         (subsumes? e e-i ss))
                     L))
  (push e L)
  (values L t))


(declaim (inline add-minimal))
(defun add-minimal (L e ss)
  (declare (optimize (speed 3)))
  (add-minimal-bit L e ss))





(declaim (inline add-maximal-bit))
(defun add-maximal-bit (L e ss)
  (declare (optimize (speed 3)))
  (declare (type state-space-bits ss))
  (declare (type list L))
  (declare (type environment e))
  ;; Don't do anything if any e-i in L is subsumed by e.
  (dolist (e-i L)
    (declare (type environment e-i))
    (when (subsumes? e e-i ss)
      (return-from add-maximal-bit (values L nil))))
  ;; Remove from L anything that is subsumed by e
  (setf L (delete-if #'(lambda (e-i)
                         (declare (optimize (speed 3)))
                         (declare (type environment e-i))
                         (subsumes? e-i e ss))
                     L))
  (push e L)
  (values L t))


(declaim (inline add-maximal))
(defun add-maximal (L e ss)
  (declare (optimize (speed 3)))
  (add-maximal-bit L e ss))






(declaim (inline add-minimal-return-delta))
(defun add-minimal-return-delta (L e ss)
  "Returns new reference to L, list of added, list of removed"
  (declare (optimize (speed 3)))
  (declare (type state-space-bits ss))
  (declare (type list L))
  (declare (type environment e))
  ;; Don't do anything if any e-i in L subsumes e.
  (dolist (e-i L)
    (declare (type environment e-i))
    (when (subsumes? e-i e ss)
      (return-from add-minimal-return-delta (values L nil nil))))
  ;; Remove from L anything that e subsumes
  (let (L-removed)
    (flet ((subsumed-by-e (e-i)
             (declare (optimize (speed 3)))
             (declare (type environment e-i))
             (subsumes? e e-i ss)))

      (setf L-removed (remove-if-not #'subsumed-by-e L))
      (setf L (delete-if #'subsumed-by-e L))
      (push e L)
      (values L (list e) L-removed))))


(declaim (inline project-environment))
(defun project-environment (e e-mask)
  "Passes 1's in the mask through, stops 0's"
  (declare (optimize (speed 3)))
  (declare (type environment e)
           (type environment e-mask))
  (bit-and e e-mask))


(defun make-controllable-projection-mask (ss &key (controllable? t))
  (with-slots (variables) ss
    (let (assignments)
      (dolist (var variables)
        (when (eql controllable? (controllable? var))
          (dolist (val (decision-variable-values var))
            (push (assignment var val) assignments))))
      (make-environment-from-assignments ss assignments))))
