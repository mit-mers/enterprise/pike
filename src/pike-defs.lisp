;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)
(in-package #:pike)


(defclass pike-session ()
  ((tpn
    :type tpn:temporal-plan-network
    :accessor pike-tpn
    :documentation "The original TPN object specifying
the plan to be executed.")
   (plant-model
    :type plant-model
    :accessor pike-plant-model
    :documentation "A plant model, which intuitively
specifies how activities should be interpreted in terms
of preconditions / effects.")
   (ss
    :type state-space-bits
    :initform (make-instance 'state-space-bits)
    :accessor pike-state-space
    :documentation "The state space keeps track of
decision variables and their domains, as well as
environments and other useful things.")
   (apsp
    :type apsp-table
    :accessor pike-apsp
    :documentation "A table containing a labeled
APSP. Maps pairs of events to LVS's representing
the shortest temporal distances between them.")
   (ldg
    :type labeled-distance-graph
    :accessor pike-ldg
    :documentation "A labeled distance graph representation
of the input problem.")
   (activities
    :type list
    :initform nil
    :accessor pike-activities
    :documentation "A list of activity objects for execution.")
   (initial-state
    :type state
    :accessor pike-initial-state
    :documentation "The initial state (w.r.t to the plant model)")
   (goal-state
    :type state
    :accessor pike-goal-state
    :documentation "The goal state (w.r.t. to the plant model)")
   (temporal-conflict-environments
    :type list
    :initform nil
    :accessor pike-temporal-conflict-environments
    :documentation "A minimal list of environments that
yield temporal infeasability")
   (constraint-knowledge-base
    :type constraint-knowledge-base
    :accessor pike-constraint-knowledge-base
    :documentation "A knowledge base of constraints.")
   (causal-link-sets
    :type list
    :initform nil
    :accessor pike-causal-link-sets
    :documentation "A list of causal link sets")
   (causal-link-temporal-constraints
    :type list
    :initform nil
    :accessor pike-causal-link-temporal-constraints
    :documentation "A list of temporal constraints that
were added to the plan as a result of causal link extraction.")
   (additional-constraints
    :type list
    :initform nil
    :accessor pike-additional-constraints
    :documentation "A list of additional propositional constraints
that should be processed.")
   (bayesian-network
    :initform nil
    :accessor pike-bayesian-network
    :documentation "A probabilty distribution (represented as a Bayesian network)
over the uncontrollable choices in Pike.")
   (variable-total-ordering
    :initform nil
    :accessor pike-variable-total-ordering
    :documentation "A total ordering over the decision variables of the problem,
represented as a list.")
   (execution-context
    :type execution-context
    :accessor pike-execution-context
    :documentation "The online execution context")
   (pike-options
    :type pike-options
    :initarg :options
    :accessor pike-options
    :documentation "The options object contains many useful
parameters and options specified by the user. These control
many different options about how Pike operates.")
   (hooks
    :type hash-table
    :initform (make-hash-table)
    :accessor pike-hooks-table
    :documentation "A hashtable mapping hook / callback symbols (like :event-executed)
to functions that will be called."))
  (:documentation "A Pike session. Contains useful information
for Pike during compilation, dispatch, and afterwards."))



(defclass event ()
  ((id
    :initarg :id
    :initform (error "Event ID must be supplied!")
    :type symbol
    :reader id)
   (name
    :type string
    :initform ""
    :initarg :name
    :reader name)
   (guard
    :type environment
    :initarg :guard
    :accessor guard)
   (activity-start
    :type activity
    :initarg :activity-start
    :accessor event-activity-start)
   (activity-end
    :type activity
    :initarg :activity-end
    :accessor event-activity-end)
   (preconditions
    :type list
    :initarg :preconditions
    :initform nil
    :accessor preconditions)
   (effects
    :type list
    :initarg :effects
    :initform nil
    :accessor effects))
  (:documentation "Represents a temporal event. Contains additional
fields useful for Pike during compilation and dispatch."))


(defclass activity ()
  ((id
    :type symbol
    :initarg :id
    :accessor id)
   (start-event
    :type event
    :initarg :start-event
    :accessor activity-start-event)
   (end-event
    :type event
    :initarg :end-event
    :accessor activity-end-event)
   (action
    :type action
    :initarg :action
    :accessor activity-action))
  (:documentation "Represents an activity that can be dispatched."))


(defmethod print-object ((e event) s)
  (with-slots (id) e
    (format s "<EVENT ~a>" id)))

(defmethod print-object ((activity activity) s)
  (with-slots (start-event end-event action) activity
      (format s "<ACTIVITY ~a: ~a -> ~a>" action start-event end-event)))

(defgeneric mention-event (ps event))
(defmethod mention-event ((ps pike-session) (e event))
  "Returns a human-readable string briefly describing an event."
  (with-output-to-string (s)
    (cond
      ((eql e (event-initial (pike-ldg ps)))
       (format s "initial conditions"))

      ((eql e (event-final (pike-ldg ps)))
       (format s "goal conditions"))

      (T ;; Normal event
       (format s "event ~a" (colorize :yellow (id e)))
       (when (slot-boundp e 'activity-end)
         (format s " (end of ~a)"
                 (colorize :purple (action-dispatch (activity-action (event-activity-end e))))))
       (when (slot-boundp e 'activity-start)
         (format s " (start of ~a)"
                 (colorize :purple (action-dispatch (activity-action (event-activity-start e))))))))))





(defclass pike-options ()
  ((plan-filename
    :initarg :plan-filename
    :accessor pike-options-plan-filename
    :initform (error "Must specify a plan file!")
    :documentation "Filename for a the plan file.")
   (domain-filename
    :initarg :domain-filename
    :accessor pike-options-domain-filename
    :initform nil
    :documentation "Filename for the domain file, if any.")
   (problem-filename
    :initarg :problem-filename
    :accessor pike-options-problem-filename
    :initform nil
    :documentation "Filename for the problem file, if any.")
   (probability-distribution
    :initarg :probability-distribution
    :accessor pike-options-probability-distribution
    :initform nil
    :documentation "Filename for the probability distribution, if any.")
   (chance-constraint
    :initarg :chance-constraint
    :accessor pike-options-chance-constraint
    :initform 1.e-6
    :documentation "The chance constraint value; specifies the minimum
acceptable probability of success.")
   (time-fn
    :type function
    :initarg :time-fn
    :accessor pike-time-fn
    :initform #'default-get-time
    :documentation "Any function that returns the current time in seconds.
The function must not require any arguments, and return a numerical time.")
   (use-execution-monitoring?
    :type (or T nil)
    :initform T
    :initarg :use-execution-monitoring?
    :accessor pike-options-use-execution-monitoring?
    :documentation "Flag for whether or not to use causal link-based
execution monitoring.")
   (initial-choice-assignments
    :type list
    :initform nil
    :initarg :initial-choice-assignments
    :accessor pike-options-initial-choice-assignments
    :documentation "A list of pairs representing
assignments to decision variables. These assignments
will be committed to immediately at the start of online
execution. Pairs contain strings representing the name
of decision variables, and their corresponding values.
Ex., (list (list \"dv1\" \"c1\"))")
   (make-uncontrollable-choices
    :type (or T nil)
    :initform nil
    :initarg :make-uncontrollable-choices?
    :accessor pike-options-make-uncontrollable-choices?
    :documentation "Flag to set whether or not Pike
will not make choices for decision variables that are
marked as uncontrollable. If nil, Pike will respect
the uncontrollability and not make those choices. If
set to T, Pike will ignore this and go ahead and make
these uncontrollable chocies anyway.")
   (use-strong-controllability
    :type (or T nil)
    :initform T
    :initarg :use-strong-controllability?
    :accessor pike-options-use-strong-controllability?
    :documentation "Flag to set whether or not
Pike will automatically add in strong controllability
constraints to the plan.")
   (constraint-knowledge-base-type
    :type symbol
    :initform :bdd ;; :atms :piatms :patms :sat :bdd :cc-wmc-ddnnf :cc-wmc-bdd :cc-wmc-ab-bdd
    :initarg :constraint-knowledge-base-type
    :accessor pike-options-constraint-knowledge-base-type
    :documentation "Sets what kind of
constraint solver backend is used - the normal ATMS, the
prime-implicant generating piATMS, BDDs, a SAT solver,
chance-constrained WMC with d-DNNF, chance-constrained WMC with BDDs,
and chance-constrained anytime BDD construction..")
   (epsilon
    :type float
    :initform 0.0
    :initarg :epsilon
    :accessor pike-options-epsilon
    :documentation "A small forgiveness factor. If an event
needed to be executed at time t = 2, and the current time
t_now = 2.00001, maybe we should call that OK instead of
aborting due to a temporal inconsistency. This constant
allows the strictness of Pike to be set - a value of 0.0
means that NO temporal inconsistency will be tolerated.")
   (use-causal-link-dominance
    :type (or t nil)
    :initform t ;; Default: t, use dominance
    :initarg :use-causal-link-dominance?
    :accessor pike-options-use-causal-link-dominance?
    :documentation "A flag to set whether causal link
dominance should be used when extracting causal links.
If enabled (which is the default), during offline causal
link extraction, Pike will assume that the effects of
every event necessarily hold. Therefore, it will be possible
to remove certain potential producer events or certain potential
threat conditions from consideration, and from becoming causal links,
if those causal links would have been dominated by a later-occuring
and more general causal link. This assumption is not necessarily valid
however, specifically in the case of online execution in which arbitrary
online disturbances may occur. If this flag is set to true and certain
disturbances occur online, Pike will prematurely signal failure even
if it would have been possible to succeed, because it will not be aware
of other potential causal links that should have been dominated but
weren't actually online. Setting ot nil will preserve all of these
potential causal links, at the expense of a more complex constraint encoding.")
   (encode-causal-support-variables
    :initform t ;; nil or t. Safer: t.
    :initarg :encode-causal-support-variables
    :accessor pike-options-encode-causal-support-variables
    :documentation "A flag setting whether the mutex / disjunction
variable value constraints are encoded into ATMS-like encoding,
for the causal link support choice variables s_{p, e_c}.")
   (memoize-constraint-calls
    :initform t
    :initarg :memoize-constraint-calls
    :accessor pike-options-memoize-constraint-calls
    :documentation "A flag setting whether or not to memoize
in an intelligent way (using environments), the calls to constraint
checking given additional constraints to commit to, etc. This can make
online execution latency considerably faster; there's really not a good
reason IMHO (other than possibly testing?) not to use this.")
   (hook-list
    :type list
    :initarg :hooks
    :initform nil
    :documentation "A property list with hooks / callbacks!")
   (generate-debug-files
    :type (or nil T)
    :initform nil
    :initarg :generate-debug-files
    :accessor pike-generate-debug-files?
    :documentation "A flag that, when T, causes Pike
to output various debugging files that can be used to
help understand it's behavior.")
   (debug-mode
    :type (or nil T)
    :initform nil
    :initarg :debug-mode
    :accessor pike-debug-mode
    :documentation "A flag that, when T, causes
Pike to operate in debug mode. This means it will
be more verbose and could possibly do more checks."))
  (:documentation "A helper class to manage the possible
options when executing Pike."))
