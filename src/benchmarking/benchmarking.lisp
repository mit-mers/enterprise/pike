;;;; Copyright (c) 2015 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)

(in-package #:pike/benchmarking)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Benchmarking functions for Pike!
;;
;; Note that before your running any of these, to really max out performance (at the
;; expensive of safety / debug errors), you should consider running:
;;
;;   (declaim (optimize (speed 3) (safety 0)))
;;
;; and then recompiling everything.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defparameter *profiled-fns-offline* `(;; Compilation
                               pike-compile
                               compute-labeled-apsp
                               extract-causal-links
                               process-constraints
                               check-if-solutions-exist)
  "A list of offline function symbols that will be measured during benchmarking")

(defparameter *profiled-fns-online* `(
                               ;; Execution
                               can-execution-succeed?
                               execute-cycle
                               could-add-conflict-environments-and-commit-to-environment?
                               get-conflict-environments-needed-to-execute-event
                               execute-event
                               commit-to-environment
                               add-conflict-environment
                               prune-events-that-wont-be-executed
                               prune-execution-windows
                               propagate-execution-time)
  "A list of online function symbols that will be measured during benchmarking")


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Run benchmarking on sets of ready-made TPN's.
;;
;; Accepts a path to a root directory with a set of benchmarks, ex. "~/bench/"
;; (note the trailing slash is important!)
;;
;; Optionally accepts a max timeout (in seconds) for each problem.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defmethod benchmark-pike (path &key (timeout 60.0) (skip-existing T))
  ;; Read in the test meta data, controlling number of tests to run
  (let* ((meta (read-file-parsing-parenthesis (format nil "~a~a" path "bench.lisp")))
         (N (getf meta :N)))
    ;; Benchmark all N problems
    (dotimes (i N)
      (format t "~%=====================================~%")
      (format t "Trial ~a / ~a~%" (+ i 1) N)
      (format t "Path: ~aprob-~a/~%~%" path i)
      (if (or (not (probe-file (format nil "~aprob-~a/results.json" path i)))
              (not skip-existing))
        (benchmark-pike-problem (format nil "~aprob-~a/" path i) :timeout timeout)
        (format t "Skipping: results.json already exists.")))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Run benchmarking for a specific problem instance. Called in loop by
;; (benchmark-pike ...).
;;
;; Accepts a path to a problem instance, ex. "~/bench/prob-0/" (note the trailing
;; slash is important!
;;
;; Also accepts a maximum time (in seconds) allowed for compilation.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defmethod benchmark-pike-problem (path &key (timeout 60.0))
  (let* ((po (make-instance 'pike-options
                                   :plan-filename (format nil "~aplan.tpn" path)
                                   :domain-filename (format nil "~adomain.pddl" path)
                                   :problem-filename (format nil "~aproblem.pddl" path)
                                   :debug-mode t
                                   :generate-debug-files t)))
    (benchmark-pike-with-options po :timeout timeout :results-filename (format nil "~aresults.json" path))))


(defmethod benchmark-pike-with-options ((po pike-options) &key (mode :regular) (timeout 60.0) (results-filename "results.json"))
  (let ((db (make-instance 'profiler-db)))
    (unwind-protect
         (let* ((ps (create-pike-session po))
                (apsp-finished-count 0)
                (compiled? T)
                (compiled-result "t")
                (timed-out? nil)
                params)

           ;; Engage profiling for all offline functions
           (dolist (fn-symbol *profiled-fns-offline*)
             (profiler-engage fn-symbol db))

           ;; Set any useful hooks here
           (pike-register-hook ps :apsp-computed
                               #'(lambda ()
                                   (incf apsp-finished-count)))

           (bordeaux-threads:with-timeout (timeout)
             ;; Compile plan.
             (handler-case (pike-compile ps)
               (bordeaux-threads:timeout (e)
                 (declare (ignore e))
                 (format t "Soft Timed out!~%")
                 (setf timed-out? T)
                 (setf compiled? nil)
                 (setf compiled-result "soft timeout!"))
               (condition (e)
                 (format t "Compile error: ~a~%" e)
                 (setf compiled-result (format nil "~a" e))
                 (setf compiled? nil))))

           ;; Set up some extra parameters
           (push `(:compiled ,(json-bool compiled?)) params)
           (push `(:compiled-result ,(json-string compiled-result)) params)
           (push `(:time-limit-soft ,(json-float timeout)) params)
           (push `(:timed-out-soft ,(json-bool timed-out?)) params)
           (push `(:apsp-finished-count ,(json-float apsp-finished-count)) params)
           (when compiled?
             (push `(:solution-size ,(get-solution-size (pike-constraint-knowledge-base ps))) params))

           ;; Depending on what constraint knowledge base was used, also
           ;; output some additional information.
           (when (slot-boundp ps 'pike::constraint-knowledge-base)
             (let ((ckb-type (pike::pike-options-constraint-knowledge-base-type po))
                   (ckb (pike-constraint-knowledge-base ps)))
               ;; Possibly unpack the ckb
               (when (typep ckb 'pike::constraint-knowledge-base-memoizer)
                 (setf ckb (pike::ckb-internal ckb)))
               (cond
                 ((or (eql :cc-wmc-ddnnf ckb-type)
                      (eql :cc-wmc-bdd ckb-type))
                  ;; Add the probability of success!
                  (push `(:p-success ,(json-float (float (pike::compute-conditional-probability-of-correctness ckb :conflicts nil :assignments-given nil)))) params))

                 ((eql :patms ckb-type)
                  ;; Add the probability of success!
                  (push `(:p-success ,(json-float (float (pike::compute-conditional-probability-of-correctness ckb :conflicts nil :assignments-given nil)))) params)
                  ;; Generate a PATMS benchmarking file
                  (pike::output-patms-benchmarking-to-file "patms-log.csv")))))

           ;; Analyze & write results so far (in case we timeout during execution!)
           (write-benchmark-data-to-file results-filename db :params params)

           ;; Execute it (if it compiled)
           (when compiled?
             ;; Engage profiling for all offline functions
             (dolist (fn-symbol *profiled-fns-online*)
               (profiler-engage fn-symbol db))
             ;; Now execute
             (format t "~%Compilation succeeded, now simulating execution~%")


             (cond
               ;; Regular / original mode
               ((eql :regular mode)
                (run-pike-with-sim-dt ps :dt 0.01))

               ;; A special mode for probabilistic testing with Riker. Given the Bayesian network,
               ;; sample from it, and simulate execution.
               ((eql :probabilistic-sample-intent mode)
                (let (intent-sample
                      activities-dispatched)

                  ;; Sample the true intent from the Bayesian network
                  (setf intent-sample (pike::sample-from-bayesian-network (pike::pike-bayesian-network ps)))

                  ;; Add appropriate hooks
                  (pike-register-hook ps :updated-choices-being-waited-on
                                      #'(lambda (choices time)
                                          (declare (ignore time))
                                          (dolist (v choices)
                                            (let ((a (gethash v intent-sample)))
                                              (pike::pike-notify-choice-made ps
                                                                             (pike::decision-variable-name v)
                                                                             (pike::assignment-value a))))))

                  (pike-register-hook ps :activity-dispatch
                                      #'(lambda (activity lb ub time)
                                          (declare (ignore lb ub time))
                                          (push (pike::action-dispatch (pike::activity-action activity))
                                                activities-dispatched)))

                  ;; If Pike asks a question (it's in probablisitic deadlock),
                  ;; the following ends execution. (We could alternatively provide the answer...)
                  (pike-register-hook ps :ask-question
                                      #'(lambda (variable-question time)
                                          (declare (ignore variable-question time))
                                          (pike::preempt-execution ps)))

                  ;; Execute
                  (run-pike-with-sim-dt ps :dt 0.01)
                  (setf activities-dispatched (reverse activities-dispatched))

                  ;; Push the activities and sampled intent to params
                  (push `(:activities-dispatched ,(json-obj activities-dispatched)) params)
                  (let ((intent-d (make-hash-table :test #'equal)))
                    (maphash #'(lambda (var assignment)
                                 (setf (gethash (pike::decision-variable-name var) intent-d)
                                       (pike::assignment-value assignment)))
                             intent-sample)
                    (push `(:intent-sampled ,(json-obj intent-d)) params))
                  t)))


             (push `(:execution-result ,(json-string (execution-context-status (pike-execution-context ps)))) params))

           ;; Analyze & write results
           (write-benchmark-data-to-file results-filename db :params params))

      ;; Cleanup: Disengage all profiling
      (profiler-disengage-all db))))


(defmethod write-benchmark-data-to-file (file (db profiler-db) &key (params nil))
  (with-open-file (s file :if-exists :supersede :direction :output)
    (format s "{~%")
    (let (fields)
      (loop for (key value) in params do
           (push (format nil "\"~a\": ~a" key value) fields))
      (dolist (fn (append *profiled-fns-offline* *profiled-fns-online*))
        (when (is-profiled? fn db)
          (push (format nil "\"~a\": ~a" fn (stats-to-json (profiler-get-stats fn db))) fields)))
      (format s "~{  ~a~^,~%~}~%" fields))
    (format s "}")))
