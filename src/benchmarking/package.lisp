;;;; Copyright (c) 2016 Massachusetts Institute of Technology
;;;;
;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:
;;;;
;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.
;;;;
;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.
;;;;
;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.
;;;;
;;;; The Pike CLI sub-package deals with various Command Line Interface
;;;; functionality, such as parsing arguments, etc.
;;;; Think of it as the Lisp equivalent of the Python argparse package
;;;; (and simpler than many other packages out there).
;;;;
;;;; Authors:
;;;;   Steve Levine

(in-package #:cl-user)

(uiop:define-package #:pike/benchmarking
    (:use :cl :cl-user :pike :pike/cli :pike/test)
  (:export :build-benchmark-standalone-executable))
