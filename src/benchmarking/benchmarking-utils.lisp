;;;; Copyright (c) 2015 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Steve Levine (sjlevine@mit.edu)

(in-package #:pike/benchmarking)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Various useful tidbits for benchmarking
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmacro json-bool (bool)
  "Helper macro to convert T / nil to true and false"
  `(if ,bool
       "true"
       "false"))

(defmacro json-string (str)
  "Helper macro to convert to a string"
  `(format nil "\"~a\"" ,str))

(defmacro json-float (v)
  "Helper macro to convert to a float that can be parsed by other languages"
  `(format nil "~f" ,v))

(defun json-obj (o)
  "Helper function to convert an object to a JSON string representation.
  NOTE: Weird / buggy edge case for nil -- is it a boolean or a list?"
  (cond
    ;; Hash tables --> { }
    ((typep o 'hash-table)
     (format nil "{~{~a~^, ~}}" (loop for key being the hash-keys of o
                                   using (hash-value value)
                                   collect (format nil "~a: ~a"
                                                   (json-obj key)
                                                   (json-obj value)))))
    ;; List --> [ ]
    ((typep o 'list)
     (format nil "[~{~a~^, ~}]" (mapcar #'json-obj o)))

    ;; String --> ""
    ((typep o 'string)
     (json-string o))

    ;; Float
    ((typep o 'number)
     (json-float o))

    ;; Boolean
    ((typep o 'boolean)
     (json-bool o))

    (t
     (error "Unknown type, can't convert to json!"))))

(defmethod stats-to-json (stats)
  "Helper function that converts the given stats to valid JSON"
  (format nil "{~{~a~^, ~}}"
          (loop for key being the hash-keys of stats
             using (hash-value value)
             collect (format nil "\"~a\": ~a" key (json-float value)))))

(defmethod pike-session-from-path (path)
  "Helper method to set up a pike session from a path problem"
  (let* ((po (make-instance 'pike-options
                            :plan-filename (format nil "~aplan.tpn" path)
                            :domain-filename (format nil "~adomain.pddl" path)
                            :problem-filename (format nil "~aproblem.pddl" path)
                            :debug-mode T))
         (ps (create-pike-session po)))
    ps))
