;;;; Copyright (c) 2016 Massachusetts Institute of Technology
;;;;
;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:
;;;;
;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.
;;;;
;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.
;;;;
;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.
;;;;
;;;; The Pike CLI sub-package deals with various Command Line Interface
;;;; functionality, such as parsing arguments, etc.
;;;; Think of it as the Lisp equivalent of the Python argparse package
;;;; (and simpler than many other packages out there).
;;;;
;;;; Authors:
;;;;   Steve Levine
(in-package :pike/benchmarking)

(defmethod build-benchmark-standalone-executable (&key (executable-filename "pike-benchmark"))
  "Call this function to build an executable. It will save it as the given filename, and then
   quit this lisp."
  #+sbcl
  (sb-ext:save-lisp-and-die "pike-benchmark" ; Executable filename to produce
                            :toplevel #'benchmark-standalone-main
                            :executable t
                            :save-runtime-options t)
  #-sbcl
  (error "Sorry, building benchmark executable is only currently supported for Pike with SBCL!"))


(defmethod benchmark-standalone-main ()
  "The main entry point for an executable
  that performs benchmarking."

  ;; Disable the LDB if we're in SBCL. If things crash, we want to exit
  ;; immediately instead of sitting in an ldb shell indefinitely.
  #+sbcl
  (sb-alien:alien-funcall
    (sb-alien:extern-alien "disable_lossage_handler" (function sb-alien:void)))
  ;; Detect interrupts, like from Ctrl-C
  (handler-case (benchmark-go)
    #+sbcl
    (sb-sys:interactive-interrupt ()
      (format t "Caught Ctrl-C, quitting.~%")) ))
    ;; (T (e)
    ;;   (format t "~a ~a~%" (colorize :red "Caught Error:") e))


(defmethod benchmark-go ()
  (colorize-set-terminal)
  (format t (colorize :green "==== Pike Benchmarker! ====~%"))
  (let* ((opts (make-instance 'parse-options
                              :program-name "pike-benchmark"
                              :description "Benchmark plans boldly."
                              :banner (make-enterprise-banner)
                              :quit-on-help t
                              :arguments (list
                                          (make-instance 'argument
                                                         :key :plan-filename
                                                         :flags nil
                                                         :name "plan"
                                                         :has-parameter? t
                                                         :description "The plan filename (ends in .tpn)"
                                                         :default-value nil)
                                          (make-instance 'argument
                                                         :key :domain-filename
                                                         :flags (list "--domain" "-d")
                                                         :name "domain"
                                                         :has-parameter? t
                                                         :description "The domain filename (ends in .pddl or .xml)"
                                                         :default-value nil)
                                          (make-instance 'argument
                                                         :key :problem-filename
                                                         :flags (list "--problem" "-p")
                                                         :name "problem"
                                                         :has-parameter? t
                                                         :description "The problem filename (ends in .pddl)"
                                                         :default-value nil)
                                          (make-instance 'argument
                                                         :key :bayesian-network
                                                         :flags (list "--bayesian-network" "-b")
                                                         :name "Bayesian network"
                                                         :has-parameter? t
                                                         :description "The Bayesian network (if applicable. ends in .xmlbif)"
                                                         :default-value nil)
                                          (make-instance 'argument
                                                         :key :output-filename
                                                         :flags (list "--output" "-o")
                                                         :name "output"
                                                         :has-parameter? t
                                                         :description "The output results filename (ends in .pddl)"
                                                         :default-value "results.json")
                                          (make-instance 'argument
                                                         :key :constraint-knowledge-base-type
                                                         :flags (list "--constraint-knowledge-base-type")
                                                         :name "constraint knowledge base type"
                                                         :has-parameter? t
                                                         :description "The type of constraint solver to use (either atms, piatms, sat, bdd, patms, cc-wmc-ddnnf, cc-wmc-bdd, or cc-wmc-anytime-bdd)."
                                                         :default-value "bdd")
                                          (make-instance 'argument
                                                         :key :chance-constraint
                                                         :flags (list "-cc" "--chance-constraint")
                                                         :name "chance constraint"
                                                         :has-parameter? t
                                                         :description "The chance constraint value, if applicable. Must be a float in [0, 1]. Default: 0.95."
                                                         :default-value "0.95")
                                          (make-instance 'argument
                                                         :key :timeout
                                                         :flags (list "--timeout")
                                                         :name "timeout"
                                                         :has-parameter? t
                                                         :description "The timeout before execution is benchmarking is terminated."
                                                         :default-value "60.0")

                                          (make-instance 'argument
                                                         :key :debug-mode
                                                         :flags (list "-v" "--verbose" "--debug")
                                                         :name "debug"
                                                         :has-parameter? nil
                                                         :description "Reports extra information useful for debugging"
                                                         :default-value nil)

                                          (make-instance 'argument
                                                         :key :disable-constraint-knowledge-base-memoization
                                                         :flags (list "--disable-constraint-knowledge-base-memoization")
                                                         :name "disable memoization"
                                                         :has-parameter? nil
                                                         :description "Turns off constraint knowledge base memoization / cacheing. Recommended only for testing; this feature should only ever improve performance."
                                                         :default-value nil)

                                          (make-instance 'argument
                                                         :key :disable-encode-causal-support-variables
                                                         :flags (list "--disable-encode-causal-support-variables")
                                                         :name "disable encoding causal support variables"
                                                         :has-parameter? nil
                                                         :description "Turns off encoding causal support variables for the ATMS, piTMS, and PATMS."
                                                         :default-value nil)

                                          (make-instance 'argument
                                                         :key :generate-debug-files
                                                         :flags (list "-f" "--debug-files")
                                                         :name "debug files"
                                                         :has-parameter? nil
                                                         :description "Generates various debugging files"
                                                         :default-value nil)
                                          (make-instance 'argument
                                                         :key :mode
                                                         :flags (list "-m" "--mode")
                                                         :name "mode"
                                                         :has-parameter? t
                                                         :description "The benchmarking mode: regular, or probabilistic-sample-intent"
                                                         :default-value "regular"))))
         args po constraint-knowledge-base-type mode)
    ;; Parse arguments, or exit otherwise.
    (handler-case (setf args (parse-command-line-arguments opts))
      (condition ()
        (print-help-text opts)
        (quit)))

    (cond
      ((equalp "atms" (gethash :constraint-knowledge-base-type args))
       (setf constraint-knowledge-base-type :atms))
      ((equalp "piatms" (gethash :constraint-knowledge-base-type args))
       (setf constraint-knowledge-base-type :piatms))
      ((equalp "sat" (gethash :constraint-knowledge-base-type args))
       (setf constraint-knowledge-base-type :sat))
      ((equalp "bdd" (gethash :constraint-knowledge-base-type args))
       (setf constraint-knowledge-base-type :bdd))
      ((equalp "patms" (gethash :constraint-knowledge-base-type args))
       (setf constraint-knowledge-base-type :patms))
      ((equalp "cc-wmc-ddnnf" (gethash :constraint-knowledge-base-type args))
       (setf constraint-knowledge-base-type :cc-wmc-ddnnf))
      ((equalp "cc-wmc-bdd" (gethash :constraint-knowledge-base-type args))
       (setf constraint-knowledge-base-type :cc-wmc-bdd))
      ((equalp "cc-wmc-anytime-bdd" (gethash :constraint-knowledge-base-type args))
       (setf constraint-knowledge-base-type :cc-wmc-anytime-bdd))
      (T
       (error "Invalid ATMS type specified in command line arguments!")))

    (cond
      ((equalp "regular" (gethash :mode args))
       (setf mode :regular))
      ((equalp "probabilistic-sample-intent" (gethash :mode args))
       (setf mode :probabilistic-sample-intent))
      (T
       (error "Invalid mode specified in command line arguments!")))

    (setf po (make-instance 'pike-options
                            :plan-filename (gethash :plan-filename args)
                            :domain-filename (gethash :domain-filename args)
                            :problem-filename (gethash :problem-filename args)
                            :probability-distribution (gethash :bayesian-network args)
                            :chance-constraint (float (read-from-string (gethash :chance-constraint args)))
                            :constraint-knowledge-base-type constraint-knowledge-base-type
                            :encode-causal-support-variables (not (gethash :disable-encode-causal-support-variables args))
                            :memoize-constraint-calls (not (gethash :disable-constraint-knowledge-base-memoization args))
                            :debug-mode (gethash :debug-mode args)
                            :generate-debug-files (gethash :generate-debug-files args)))

    (benchmark-pike-with-options po
                                 :results-filename (gethash :output-filename args)
                                 :timeout (parse-number:parse-number (gethash :timeout args))
                                 :mode mode)))


(defmethod make-enterprise-banner ()
  "We've got to have style, no? :-)"
  (concatenate 'string "                 _____.-----._____
    ___----~~~~~~. ... ..... ... .~~~~~~----___
 =================================================
    ~~~-----......._____________.......-----~~~
     "
               (colorize :red "(____)")
               "          \\   |   /          "
               (colorize :red "(____)") "
       ||           _/   |   \\_           ||
        \\\\_______--~  "
               (colorize :blue "//~~~\\\\") "  ~--_______//
         `~~~~---__   "
               (colorize :blue "\\\\___//") "   __---~~~~'
                   ~~-_______-~~
"))
