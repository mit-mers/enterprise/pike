;;; Run this file with: sbcl --load build_script.lisp

;; Adjust optimization settings as appropriate
(declaim (optimize (speed 3)))
;; (load "~/.sbclrc") ;; Necessary only if SBCL is run in "script" mode
(ql:quickload :pike/benchmarking)
(pike/benchmarking:build-benchmark-standalone-executable :executable-filename "pike-benchmark")
