# Pike changelog #

## Released

## v0.1.0 - Released

### Added
+ This changelog.
+ Dependency on `safe-queue` for mailboxes and threading.

### Removed
+ Dependency on `mtk-utils` for `infinity` and threading processes.

