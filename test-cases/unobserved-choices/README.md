# Unobserved Choices Tests for Riker

These tests exercise an interesting feature of Riker due to its pTPN input: sometimes not observing certain choices. Since choices in a pTPN are nested, it is possible Riker to make choices that preclude certain uncontrollable choices from being observed. When Riker observes uncontrollable choices, it can ``do better'' by tailoring its policy on it. However, Riker must reason that it may not to get to observe all uncontrollable choices based on its own decisions.

Ths is the motivation for my ``Phantom Bayesian Network'' work.


## Test cases

Here is a summary of the test cases in this folder, and how to run them.

### Unobserved Choices

In this example, Riker has a choice  (R1) about whether or not to observe the outcome of an uncontrollable choice (H2). Afterwards, Riker must decide (R2) which choice to make to satisfy a causal link with a human choice (H1). Yes I know, the choice names are backwards kind of. There is a causal link from actions of R2 to H1. Additionally, H2 is extremely strongly correlated with H1 -- deterministic relationship from probability distribution. So observing H2 tells Riker what H1 will be, so it can guarantee success with that observation by virtue of R2. The prior on H1 is 2/3 and 1/3.

Files:
- unobserved-choices-2.py -- generates pTPN
- unobserved-choices-2.tpn -- pTPN
- bn-2.xmlbif -- Bayesian network
- domain.pddl and problem.pddl -- PDDL files for causal link extraction

What you should see when run: 
- Overall P(success) = 1
- P(success | R1 = c1): 1, because that's when Riker observes H2
- P(success | R1 = c2): 2/3, because that's when Riker doesn't observe H2 and essentially has to make the best guess (2/3)


### Unobserved Choices 2

This is a similar albeit slightly more complicated example of the above. Riker gets to choose between one of two choices that give information about a goal G (A, B, C, or D).

An english explanation / scenario: suppose there are 4 apples: (a) big red, (b) big green, (c) little red, (d) little green. Riker needs to give the correct apple to the person, but doesn't know which one he / she wants (belief is uniform 1/4). Riker can choose to ask one of two questions: H1 asks "is it big?". H2 asks "is it red?". Riker can't ask both questions, but must choose. Either question alone doesn't narrow it down. Previous versions of Riker incorrectly assumed it would have access to both responses, which would uniquely define the apple, and hence Riker would give a 100% chance of success erroneously. Now it should say 50%. This version corresponds to bn.xmlbif.

There's also an alternate interesting version with bn-3.xmlbif: now H1 is useless w.r.t. the task, and asks "Is the weather cold today?" Not correlated at all.

Files:
- unobserved-choices.py -- generates pTPN
- unobserved-choices.tpn -- pTPN
- bn.xmlbif OR bn-3.xmlbif -- Bayesian network
- domain.pddl and problem.pddl -- PDDL files for causal link extraction

What you should see when run using bn.xmlbif: 
- Overall P(success) = 1/2
- P(success | R1 = c1): 1/2, because it could be either big red or big green etc.
- P(success | R1 = c2): 1/2, because it could be either big red or little red etc.

What you should see when run using bn-3.xmlbif: 
- Overall P(success) = 1/2
- P(success | R1 = c1): 1/4, because that choice gives no information! Conditionally independent.
- P(success | R1 = c2): 1/2, because that choices narrows it down to 2 possibilities


### Unobserved Choices 3

This example is extremely similar to the first example above, except that now that initial choice (R1) has been replaced with an uncontrollable choice (HQ). HQ is probabilistically indepenedent of all other variables, and has a 50/50 probability. 

An intuitive English explanation is as follows. The human decides with 50 / 50% chance if he / she will allow the robot to ask a question. If the robot does get to ask a question (H2), it tells the answer determinsitically for later desire of human (H1). This would allow the robot (R2) to pick correctly. So for the 50% of the time when the robot gets to ask a question, it will have a 100% chance of success. If the robot doesn't get to ask a question, it's forced to take it's best guess for R2 because it doesn't get to observe the outcome to the question. In this case, there is a 2/3 chance of success. Therefore overall, we have a 5/6 chance of success.


Files:
- unobserved-choices-3.py -- generates pTPN
- unobserved-choices-3.tpn -- pTPN
- bn-4.xmlbif -- Bayesian network
- domain.pddl and problem.pddl -- PDDL files for causal link extraction

What you should see when run: 
- Overall P(success) = 5/6, which is a 50/50 mix of the following two outcomes:
- P(success | HQ = c1): 1, because that's when Riker observes H2
- P(success | HQ = c2): 2/3, because that's when Riker doesn't observe H2 and essentially has to make the best guess (2/3)

