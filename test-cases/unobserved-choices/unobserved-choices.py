#!/usr/bin/env python
"""
This file demonstrates how to use the pytpn API to build a TPN from scratch!
"""

from pytpn.tpn import TPN, Event, DecisionVariable, Episode, TemporalConstraint, Expression, And, Or, Not, Constant, Assignment, TPNSegment

if __name__ == '__main__':
    # Create a TPN
    tpn = TPN()
    # Create 3 activities (episodes with dispatch fields)
    a1 = tpn.create_episode(dispatch='(noop t1)', duration=[2, 4])
    a2 = tpn.create_episode(dispatch='(noop t1)', duration=[2, 4])
    a3 = tpn.create_episode(dispatch='(noop t1)', duration=[2, 4])
    a4 = tpn.create_episode(dispatch='(noop t1)', duration=[2, 4])

    a5 = tpn.create_episode(dispatch='(make-p t1)', duration=[2, 4])
    a6 = tpn.create_episode(dispatch='(make-p t2)', duration=[2, 4])
    a7 = tpn.create_episode(dispatch='(make-p t3)', duration=[2, 4])
    a8 = tpn.create_episode(dispatch='(make-p t4)', duration=[2, 4])

    a9  = tpn.create_episode(dispatch='(require-p t1)', duration=[2, 4])
    a10 = tpn.create_episode(dispatch='(require-p t2)', duration=[2, 4])
    a11 = tpn.create_episode(dispatch='(require-p t3)', duration=[2, 4])
    a12 = tpn.create_episode(dispatch='(require-p t4)', duration=[2, 4])


    # Make a choice!
    choice_1 = tpn.choice([a1, a2], domain=['c1', 'c2'], choice_id='H1', type='uncontrollable')
    choice_2 = tpn.choice([a3, a4], domain=['c1', 'c2'], choice_id='H2', type='uncontrollable')

    choice_3 = tpn.choice([choice_1, choice_2], domain=['c1', 'c2'], choice_id='R1', type='controllable')

    choice_4 = tpn.choice([a5, a6, a7, a8], domain=['A', 'B', 'C', 'D'], choice_id='R2', type='controllable')

    choice_5 = tpn.choice([a9, a10, a11, a12], domain=['A', 'B', 'C', 'D'], choice_id='G', type='uncontrollable')

    seq = tpn.sequence([choice_3, choice_4, choice_5])

    # Set the TPN to this segment and write out!
    tpn.start_event = seq.from_event
    tpn.end_event = seq.to_event
    # Write a file!
    tpn.to_xml_file('unobserved-choices.tpn')
