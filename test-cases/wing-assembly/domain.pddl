(define (domain Airbus)
(:requirements 
	:typing
	:durative-actions
	:equality
	:negative-preconditions
	:disjunctive-preconditions
	:conditional-effects
)

(:types

  locatable - object

  agent - locatable
    mobileagent - agent
      mobilitywam - mobileagent      
    immobileagent - agent
      baxterarm - immobileagent
      human - immobileagent
      wam - immobileagent
  
  part - locatable
    frame - part
    rib - part
    clecogun - part
    clecobox - part

  jig - locatable

  location - object

  ribslot - object
  clecohole - object
)

(:predicates
  (at ?a - locatable ?l - location)
  (carrying-frame ?a - agent)
  (empty-gripper ?a - agent)
  (can-pickup ?a - agent ?p - part)
  (can-pickup-cleco ?a - agent)


  (frame-held-left ?f - frame ?a - agent)
  (frame-held-right ?f - frame ?a - agent)
  (frame-held ?f - frame ?a1 ?a2 - agent)
  (braced ?f - frame) (not-braced ?f - frame)
  (frame-on-jig ?f - frame ?j - jig)

  (holding-rib ?a - agent ?r - rib)
  (slot-on-frame ?s - ribslot  ?f - frame)
  (fits ?r - rib ?s - ribslot)
  (rib-not-in-slot ?r - rib)
  (rib-in-slot ?r - rib ?s - ribslot)
  (rib-secure-in-slot ?r - rib ?s - ribslot)

  (hole-in-rib ?h - clecohole ?r - rib)
  (holding-cleco-gun ?a - agent ?c - clecogun)
  (holding-cleco ?a - agent)
  (loaded-cleco-gun ?c - clecogun)
  (cleco-in-hole ?h - clecohole)
)


(:durative-action grab-frame-left
 :parameters ( ?f - frame ?a - wam ?l - location)
 :duration (= ?duration 1)
 :condition (and
		(over all (at ?a ?l))
		(over all (at ?f ?l)) 
		(over all (not-braced ?f))
		(at start (empty-gripper ?a)) 
	)
 :effect (and 
		(at end (frame-held-left ?f ?a)) 
		(at start (not (empty-gripper ?a))) 
		(at start (carrying-frame ?a))
	)
)

(:durative-action grab-frame-right
 :parameters ( ?f - frame ?a - wam ?l - location)
 :duration (= ?duration 1)
 :condition (and 
		(over all (at ?a ?l))
		(over all (at ?f ?l)) 
		(over all (not-braced ?f))
		(at start (empty-gripper ?a)) 
	)
 :effect (and 
		(at end (frame-held-right  ?f ?a)) 
		(at start (not (empty-gripper ?a))) 
		(at start (carrying-frame ?a))
	)
)

(:durative-action pick-up-frame
 :parameters ( ?f - frame ?a1 ?a2 - wam ?l - location)
 :duration (= ?duration 1)
 :condition (and 
		(over all (frame-held-left ?f ?a1))
		(over all (frame-held-right ?f ?a2))
		(over all (not-braced ?f))
	)
 :effect (and 
		(at end (frame-held  ?f ?a1 ?a2)) 
	)
)

(:durative-action put-down-frame
 :parameters ( ?f - frame ?a1 ?a2 - wam ?l - location)
 :duration (= ?duration 1)
 :condition (and 
		(over all (at ?a1 ?l)) 
		(over all (at ?a2 ?l)) 
		(over all (not-braced ?f))
		(at start (frame-held  ?f ?a1 ?a2))  
	    )
 :effect (and 
		(at end (not (frame-held  ?f ?a1 ?a2))) 
		(at end (empty-gripper ?a1)) 
		(at end (empty-gripper ?a2))
                (at end (not (carrying-frame ?a1)))
                (at end (not (carrying-frame ?a2)))
	    )
)

(:durative-action carry-frame
 :parameters ( ?f - frame ?a1 ?a2 - mobileagent ?l1 ?l2 - location)
 :duration (= ?duration 1)
 :condition (and 
		(over all (frame-held  ?f ?a1 ?a2)) 
		(at start (at ?a1 ?l1)) 
		(at start (at ?a2 ?l1))
		(at start (at ?f ?l1))  
		(over all (not-braced ?f))
	    )
 :effect (and 
		(at start (not (at ?a1 ?l1))) 
		(at start (not (at ?a2 ?l1)))
		(at start (not (at ?f ?l1)))  

		(at end (at ?a1 ?l2)) 
		(at end (at ?a2 ?l2))
		(at end (at ?f ?l2))  
	    )
)

(:durative-action lift-and-brace-frame
 :parameters ( ?f - frame ?a1 ?a2 - wam ?l - location)
 :duration (= ?duration 1)
 :condition (and 
		(over all (frame-held  ?f ?a1 ?a2))
		(over all (at ?a1 ?l))
		(over all (at ?a2 ?l))
	    )
 :effect (and 
		(at end (braced ?f))
		(at end (not (not-braced ?f)))
	)
)

(:durative-action unbrace-frame
 :parameters ( ?f - frame ?a1 ?a2 - wam ?l - location)
 :duration (= ?duration 1)
 :condition (and 
		(over all (frame-held  ?f ?a1 ?a2))
		(over all (at ?a1 ?l))
		(over all (at ?a2 ?l))
		(at start (braced ?f))
	    )
 :effect (and 
		(at end (not (braced ?f)))
		(at end (not-braced ?f))
	)
)


(:durative-action pick-up-rib
 :parameters ( ?r - rib ?a - agent ?l - location)
 :duration (= ?duration 1)
 :condition (and 
		(over all (at ?a ?l)) 
		(at start (at ?r ?l)) 
		(at start (empty-gripper ?a)) 
		(over all (can-pickup ?a ?r))
	)
 :effect (and 
		(at end (holding-rib  ?a ?r)) 
		(at start (not (at ?r ?l))) 
		(at start (not (empty-gripper ?a)))
	)
)

(:durative-action put-down-rib
 :parameters ( ?r - rib ?a - agent ?l - location)
 :duration (= ?duration 1)
 :condition (and 
		(over all (at ?a ?l))  
		(at start (holding-rib  ?a ?r)) 
		(over all (rib-not-in-slot ?r))
	)
 :effect (and  
		(at end (at ?r ?l)) 
		(at start (not (holding-rib  ?a ?r))) 
		(at end (empty-gripper ?a))
	)
)

(:durative-action put-rib-in-slot
 :parameters ( ?r - rib ?a - agent ?l - location ?f - frame ?s - ribslot)
 :duration (= ?duration 1)
 :condition (and 
		(over all (braced ?f)) 
		(over all (at ?a ?l)) 
		(over all (at ?f ?l)) 
		(over all (holding-rib ?a ?r))  
		(over all (slot-on-frame ?s ?f))
		(over all (fits ?r ?s))
		(at start (rib-not-in-slot ?r))
	    )
 :effect (and 
		(at end (rib-in-slot ?r ?s))   
		(at end (not (rib-not-in-slot ?r)))
	)
)

(:durative-action put-rib-in-slot-with-human
 :parameters ( ?r - rib ?a - agent ?h - human ?l - location ?f - frame ?s - ribslot)
 :duration (= ?duration 1)
 :condition (and 
		(over all (braced ?f)) 
		(over all (at ?a ?l)) 
		(over all (at ?h ?l)) 
		(over all (at ?f ?l)) 
		(over all (holding-rib ?a ?r))  
		(over all (slot-on-frame ?s ?f))
		(over all (fits ?r ?s))
		(at start (rib-not-in-slot ?r))
	    )
 :effect (and 
		(at end (rib-in-slot ?r ?s))   
		(at end (not (rib-not-in-slot ?r)))
	)
)

(:durative-action leave-rib-in-slot
 :parameters ( ?r - rib ?a - agent ?l - location ?f - frame ?s - ribslot)
 :duration (= ?duration 1)
 :condition (and   
		(over all (rib-secure-in-slot ?r ?s)) 
		(over all (braced ?f)) 
		(over all (at ?a ?l)) 
		(over all (at ?f ?l))   
		(over all (slot-on-frame ?s ?f))
	)
 :effect (and 
		(at end (not (holding-rib ?a ?r))) 
		(at end (empty-gripper ?a)) 
	)
)


(:durative-action pick-up-cleco
 :parameters ( ?b - clecobox ?a - agent ?l - location)
 :duration (= ?duration 1)
 :condition (and 
		(over all (at ?a ?l)) 
		(over all (at ?b ?l)) 
		(at start (empty-gripper ?a)) 
		(over all (can-pickup-cleco ?a))
	)
 :effect (and 
		(at end (holding-cleco ?a))  
		(at start (not (empty-gripper ?a)))
	)
)

(:durative-action drop-cleco
 :parameters (?a - agent)
 :duration (= ?duration 1)
 :condition (and 
		(at start (holding-cleco ?a)) 
	)
 :effect (and  
		(at start (not (holding-cleco ?a))) 
		(at end (empty-gripper ?a)) 
	)
)

(:durative-action hand-cleco
 :parameters (?a1 - agent ?a2 - human ?l - location)
 :duration (= ?duration 1)
 :condition (and 
		(at start (holding-cleco ?a1)) 
		(at start (empty-gripper ?a2))
		(over all (at ?a1 ?l))
		(over all (at ?a2 ?l))
	)
 :effect (and  
		(at start (not (holding-cleco ?a1))) 
		(at end (empty-gripper ?a1))
		(at start (not (empty-gripper ?a2)))
		(at end (holding-cleco ?a2))
	)
)



(:durative-action pick-up-cleco-gun
 :parameters ( ?c - clecogun ?a - agent ?l - location)
 :duration (= ?duration 1)
 :condition (and 
		(over all (at ?a ?l)) 
		(at start (at ?c ?l)) 
		(at start (empty-gripper ?a)) 
		(over all (can-pickup ?a ?c))
	)
 :effect (and 
		(at end (holding-cleco-gun  ?a ?c)) 
		(at start (not (at ?c ?l))) 
		(at start (not (empty-gripper ?a)))
	)
)

(:durative-action put-down-cleco-gun
 :parameters ( ?c - clecogun ?a - agent ?l - location)
 :duration (= ?duration 1)
 :condition (and 
		(over all (at ?a ?l)) 
		(at start (holding-cleco-gun  ?a ?c)) 		
	)
 :effect (and 
		(at start (not (holding-cleco-gun  ?a ?c))) 
		(at end (at ?c ?l)) 
		(at end (empty-gripper ?a))
	)
)

(:durative-action hand-cleco-gun
 :parameters ( ?c - clecogun ?a1 - agent ?a2 - human ?l - location)
 :duration (= ?duration 1)
 :condition (and 
		(over all (at ?a1 ?l)) (over all (at ?a2 ?l))  
		(at start (holding-cleco-gun ?a1 ?c)) 
		(at start (empty-gripper ?a2)) 
	)
 :effect (and 
		(at start (not (holding-cleco-gun ?a1 ?c)))
		(at end   (empty-gripper ?a1))

		(at end   (holding-cleco-gun ?a2 ?c)) 
		(at start (not (empty-gripper ?a2)))
	)
)



(:durative-action load-cleco-gun
 :parameters ( ?c - clecogun ?a1 ?a2 - human ?l - location)
 :duration (= ?duration 1)
 :condition (and 
		(over all (at ?a1 ?l)) 
		(over all (at ?a2 ?l)) 
		(over all (holding-cleco-gun  ?a1 ?c)) 
		(at start (holding-cleco ?a2)) 
	)
 :effect (and 
		(at end (loaded-cleco-gun ?c)) 
		(at start (not (holding-cleco ?a2))) 
		(at end (empty-gripper ?a2)) 
	)
)

(:durative-action fire-cleco-into-hole
 :parameters ( ?c - clecogun ?h - clecohole ?r - rib ?s - ribslot ?f - frame ?a - human ?l - location)
 :duration (= ?duration 1)
 :condition (and 
		(over all (at ?a ?l)) 
		(over all (at ?f ?l)) 
		(over all (braced ?f)) 
		(over all (slot-on-frame ?s ?f)) 
		(over all (rib-in-slot ?r ?s)) 
		(over all (holding-cleco-gun  ?a ?c)) 
		(over all (hole-in-rib ?h ?r))
		(at start (loaded-cleco-gun ?c))
	)
 :effect (and 
		(at end (cleco-in-hole ?h))
		(at start (not (loaded-cleco-gun ?c)))	
	)
)


(:durative-action check-rib-secure-in-slot
 :parameters ( ?r - rib ?s - ribslot ?h1 ?h2 - clecohole)
 :duration (= ?duration 1)
 :condition (and 
		(over all (rib-in-slot ?r ?s)) 
		(over all (hole-in-rib ?h1 ?r))
		(over all (cleco-in-hole ?h1))
		(over all (hole-in-rib ?h2 ?r))
		(over all (cleco-in-hole ?h2))
		
	)
 :effect (and 
		(at end (rib-secure-in-slot ?r ?s)) 
	)
)

(:durative-action pick-up-cleco-and-cleco-gun
 :parameters (?c - clecogun ?b - clecobox ?h1 - human ?h2 - human ?l - location)
 :duration (= ?duration 1)
 :condition (and 
		(over all (at ?h1 ?l)) 
		(over all (at ?h2 ?l))
		(at start (at ?c ?l)) 
		(over all (at ?b ?l)) 
		(at start (empty-gripper ?h1)) 
		(at start (empty-gripper ?h2)) 

		(over all (can-pickup-cleco ?h1))
		(over all (can-pickup ?h2 ?c))
    )
 :effect (and 
		(at end (holding-cleco ?h1))  
		(at start (not (empty-gripper ?h1)))

		(at end (holding-cleco-gun  ?h2 ?c)) 
		(at start (not (at ?c ?l))) 
		(at start (not (empty-gripper ?h2)))
))

)

