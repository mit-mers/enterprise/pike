(define (problem problem1)
(:domain Boeing)
(:objects
  loc - location
  wam_loki wam_thor - wam
  baxter_right baxter_left - baxterarm
  human_left human_right - human
  frame1 - frame
  rib1 - rib
  ribslot1 - ribslot
  clecohole1 clecohole2 - clecohole
  clecogun1 - clecogun
  clecobox1 - clecobox
)

(:init

  (can-pickup baxter_right rib1)

  (can-pickup baxter_left clecogun1)
  (can-pickup human_right clecogun1)

  (can-pickup-cleco human_left)
  (can-pickup-cleco baxter_left)

  (at wam_loki loc)      (empty-gripper wam_loki)
  (at wam_thor loc)      (empty-gripper wam_thor)
  (at human_left loc)      (empty-gripper human_left)
  (at human_right loc)      (empty-gripper human_right) 
  (at baxter_right loc)   (empty-gripper baxter_right)
  (at baxter_left loc)  (empty-gripper baxter_left)
  (at clecogun1 loc)
  (at clecobox1 loc)

  (at rib1 loc) (rib-not-in-slot rib1)


  (at frame1 loc)  (not-braced frame1)
 
  
  (slot-on-frame ribslot1 frame1) (fits rib1 ribslot1) 
  (hole-in-rib clecohole1 rib1) (hole-in-rib clecohole2 rib1) 
 
)

(:goal
  (and
 (rib-secure-in-slot rib1 ribslot1)
  )
)
)
