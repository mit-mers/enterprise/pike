#!/usr/bin/env python

"""
RMPyL program modeling a collaborative pick-and-place task between a human and a robot.
"""
from __future__ import division
from rmpyl.rmpyl import RMPyL, Episode

DURATION_DIVISION_FACTOR = 10.0 # 1.0



def commute(prog, commute_action, lb=1.0, ub=2.0, dryoff=False, dryoff_variable_name='x_dryoff'):
    ep_commute = Episode(action=commute_action, duration={'ctype': 'controllable', 'lb': lb, 'ub': ub})
    if not dryoff:
        return ep_commute
    else:
        return prog.sequence(ep_commute,
                             prog.observe({
                                'name': dryoff_variable_name,
                                'id': dryoff_variable_name,
                                'ctype': 'uncontrollable',
                                'domain': ['rainy', 'sunny']
                             },
                             *[Episode(action='(dry-off person)', duration={'ctype': 'controllable', 'lb': 15 / DURATION_DIVISION_FACTOR, 'ub': 20 / DURATION_DIVISION_FACTOR}),
                               Episode()]))

def main():
    prog = RMPyL(name='run()')
    prog *= prog.sequence(
                prog.parallel(
                    prog.sequence(

                        prog.observe({
                            'name': 'x_snooze',
                            'id': 'x_snooze',
                            'ctype': 'uncontrollable',
                            'domain': ['snooze', 'get_up']
                        },
                        *[Episode(action='(snooze person)', duration={'ctype': 'controllable', 'lb': 60 / DURATION_DIVISION_FACTOR, 'ub': 75 / DURATION_DIVISION_FACTOR}),
                        Episode()]),

                        prog.observe({
                            'id': 'x_breakfast',
                            'name': 'x_breakfast',
                            'ctype': 'uncontrollable',
                            'domain': ['eggs', 'cereal']
                        },
                        *[Episode(action='(eat-breakfast eggs)', duration={'ctype': 'controllable', 'lb': 30 / DURATION_DIVISION_FACTOR, 'ub': 35 / DURATION_DIVISION_FACTOR}),
                          Episode(action='(eat-breakfast cereal)', duration={'ctype': 'controllable', 'lb': 5 / DURATION_DIVISION_FACTOR, 'ub': 10 / DURATION_DIVISION_FACTOR})]),
                    ),

                    prog.sequence(
                        prog.decide({
                            'name': 'x_pump_bike',
                            'id': 'x_pump_bike',
                            'domain': ['yes', 'no'],
                            'utility': [0.0, 1.0]
                        },
                        *[Episode(action='(inflate-tires robot bicycle)', duration={'ctype': 'controllable', 'lb': 35 / DURATION_DIVISION_FACTOR, 'ub': 45 / DURATION_DIVISION_FACTOR}),
                        Episode()]),

                    prog.decide({
                        'name': 'x_keys',
                        'id': 'x_keys',
                        'domain': ['yes', 'no'],
                        'utility': [0.0, 1.0]
                    },
                    *[Episode(action='(give robot person car-keys)', duration={'ctype': 'controllable', 'lb': 35 / DURATION_DIVISION_FACTOR, 'ub': 45 / DURATION_DIVISION_FACTOR}),
                    Episode()]),

                    prog.decide({
                        'name': 'x_shoes',
                        'id': 'x_shoes',
                        'domain': ['yes', 'no'],
                        'utility': [0.0, 1.0]
                    },
                    *[Episode(action='(give robot person running-shoes)', duration={'ctype': 'controllable', 'lb': 35 / DURATION_DIVISION_FACTOR, 'ub': 45 / DURATION_DIVISION_FACTOR}),
                    Episode()]),

                    prog.decide({
                        'name': 'x_umbrella',
                        'id': 'x_umbrella',
                        'domain': ['yes', 'no'],
                        'utility': [0.0, 1.0]
                    },
                    *[Episode(action='(give robot person umbrella)', duration={'ctype': 'controllable', 'lb': 35 / DURATION_DIVISION_FACTOR, 'ub': 45 / DURATION_DIVISION_FACTOR}),
                    Episode()]),

                    )
                , duration={'ctype': 'controllable', 'lb': 0 / DURATION_DIVISION_FACTOR, 'ub': 120 / DURATION_DIVISION_FACTOR}),
                prog.observe({
                    'id': 'x_commute',
                    'name': 'x_commute',
                    'ctype': 'uncontrollable',
                    'domain': ['car', 'bike', 'run']
                },
                *[commute(prog, '(drive person home work)', lb=15.0 / DURATION_DIVISION_FACTOR, ub=20.0 / DURATION_DIVISION_FACTOR, dryoff=False),
                  commute(prog, '(bike person home work)', lb=30.0 / DURATION_DIVISION_FACTOR, ub=40.0 / DURATION_DIVISION_FACTOR, dryoff=True, dryoff_variable_name='x_weather_1'),
                  commute(prog, '(run person home work)', lb=45.0 / DURATION_DIVISION_FACTOR, ub=55.0 / DURATION_DIVISION_FACTOR, dryoff=True, dryoff_variable_name='x_weather_2'),
                 ])
            )
    # Must get to work in 2.5 hours of first wake up
    # prog.add_overall_temporal_constraint(ctype='controllable', lb=0.0, ub=150.0)
    return prog


if __name__=='__main__':
    prog = main()
    prog.to_ptpn(filename="tpn-morning-routine-raw-1.tpn")
