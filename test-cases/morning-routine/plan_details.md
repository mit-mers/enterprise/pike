# Morning Routine

This plan represents the morning routine of a healthy individual. It involves the person waking up in the morning, preparing for day, and commuting to work via different options.

The person may either way up early, or snooze to sleep in for an extra hour or so. To get to the work later, the person will commute either via driving a car, bicycling, or running. The robot assistant doesn't know which commuting method will be taken, but it does have an a priori estimate, can observe choices the human makes, and also has an a-priori estimate of the weather (if it will rain or not).

There is a temporal deadline as to when the person must get to work. Driving is the fastest option, and is the person is more likely to choose this if it's raining out. Biking and running are still options, but if the person gets caught in the rain then he/she will need to take an extra "dry off" action before going into work, which will require an umbrella to have been given to him/her. Furthermore, if the person will be biking or running, then a hearty breakfast of eggs will be needed. Otherwise for driving just cereal will do.

The robot has a few different options of things it can do. It can give the person car keys, can pump the bike tires, can give the person running shoes, and can give the person an umbrella. Due to the temporal deadlines though, the robot won't have time to do all of these things -- it will only have time to do a few of them (those tasks that the robot chooses to do must all be complete before the person leaves for work that day).

Note that in this domain, the human acts "independently" of the robot. That is to say, he/she does not adapt intentions based on what the robot does. The robot treats the person as completely uncontrollable, and tries to make choices that maximize the likelihood of success given the uncontrollable human.

## Post processing Changes:

- Change - x_weather_1 and x_weather_2 to the same variable, x_weather (remove one variable definition, and find and replace)
- Removed extraneous temporal constraints
- Add a temporal constraint making the last choice of the robot come before the choice where the person makes the decision

How to make some of these changes:

In the variable declaration:
- Change <variable>x_weather_2</variable> --> <variable>x_weather</variable>
- Remove <at event>...
- Make the guard true
<guard>
  <boolean-constant>true</boolean-constant>
</guard>
