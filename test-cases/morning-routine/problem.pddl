(define (problem morning-routine-1)
  (:domain boeing-domain)
  (:objects
   person - agent
   robot - agent
   )

  (:init
   (at person home)

   )

  (:goal
   (and
    (at person work)
    )))
