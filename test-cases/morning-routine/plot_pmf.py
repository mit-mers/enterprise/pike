#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt

if __name__ == '__main__':

    # Inferencing numbers:
    # At first (no observations):
    # Car: 0.35
    # Bike: 0.325
    # Run: 0.325
    #
    # After observing x_snooze = get_up:
    # Car: 0.0972
    # Bike: 0.0903
    # Run: 0.8125
    #
    # After additionally observing x_breakfats = eggs:
    # Car: 0.0118
    # Bike: 0.0988
    # Run: 0.8894

    objs = ['Car', 'Bike', 'Run']
    # p = [0.35, 0.325, 0.325]
    # p = [0.0972, 0.0903,0.8125]
    p = [0.0118, 0.0988, 0.8894]

    x = np.arange(len(objs))
    plt.bar(x, p, align='center', color='green')
    ax = plt.gca()
    ax.set_ylim([0.0, 1.0])
    plt.xticks(x, objs)
    plt.ylabel('Probability')
    plt.xlabel('Outcome')
    plt.title('P(Commute)')
    # plt.grid(True)
    plt.show()
