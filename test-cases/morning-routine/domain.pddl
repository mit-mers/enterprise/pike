(define (domain boeing-domain)
  (:requirements :strips :typing :durative-actions :duration-inequalities)
  (:types agent thing bicycle location)

  (:predicates
   (has ?a - agent ?t - thing)
   (at ?a - agent ?l - location)
   (tires-inflated ?b - bicycle)
   (is-dry ?a - agent)
   )


  (:constants
   car-keys - thing
   running-shoes - thing
   umbrella - thing
   bicycle - bicycle
   home - location
   work - location
   )

  (:durative-action snooze
                    :parameters (?a - agent)
                    :duration (and (>= ?duration 1) (<= ?duration 2))
                    :condition (and )
                    :effect (and ))

  (:durative-action eat-breakfast
                    :parameters (?a - agent)
                    :duration (and (>= ?duration 1) (<= ?duration 2))
                    :condition (and )
                    :effect (and ))

  (:durative-action drive
                    :parameters (?a - agent ?ls ?lf - location)
                    :duration (and (>= ?duration 1) (<= ?duration 2))
                    :condition (and (at start (has ?a car-keys)))
                    :effect (and (at start (not (at ?a ?ls)))
                                 (at end (at ?a ?lf))))

  (:durative-action bike
                    :parameters (?a - agent ?ls ?lf - location)
                    :duration (and (>= ?duration 1) (<= ?duration 2))
                    :condition (and (at start (tires-inflated bicycle)))
                    :effect (and (at start (not (at ?a ?ls)))
                                 (at end (at ?a ?lf))))

  (:durative-action run
                    :parameters (?a - agent ?ls ?lf - location)
                    :duration (and (>= ?duration 1) (<= ?duration 2))
                    :condition (and (at start (has ?a running-shoes)))
                    :effect (and (at start (not (at ?a ?ls)))
                                 (at end (at ?a ?lf))))

  (:durative-action inflate-tires
                    :parameters (?a - agent ?b - bicycle)
                    :duration (and (>= ?duration 1) (<= ?duration 2))
                    :condition (and )
                    :effect (and (at end (tires-inflated ?b))))

  (:durative-action give
                    :parameters (?ag ?ar - agent ?t - thing)
                    :duration (and (>= ?duration 1) (<= ?duration 2))
                    :condition (and )
                    :effect (and (at end (has ?ar ?t))))

  (:durative-action dry-off
                    :parameters (?a - agent)
                    :duration (and (>= ?duration 1) (<= ?duration 2))
                    :condition (and (at start (has ?a umbrella)))
                    :effect (and (at end (is-dry ?a))))



  )
