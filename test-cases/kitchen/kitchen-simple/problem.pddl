(define (problem kitchen-1)
    (:domain kitchen)
    (:objects
        robot - agent
        human - agent
        mug - object
        glass - object
        juice - object
        grounds - object
        bagel - object
        cereal - object
        milk - object
        creamcheese - object)

    (:init
        (on-shelf bagel)
        (on-shelf creamcheese)
        (on-shelf cereal)
        (on-shelf milk)
        (on-shelf juice)
        (on-shelf grounds)
        (on-shelf mug)
        (on-shelf glass))

    (:goal
        (and (food-ready)
             (drink-ready))))
