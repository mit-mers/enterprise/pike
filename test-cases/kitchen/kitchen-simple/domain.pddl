(define (domain kitchen)
    (:requirements :strips :typing :durative-actions :duration-inequalities)

    (:types agent object)

    (:predicates

        (on-shelf ?o - object)
        (on-table ?o - object)

        (coffee-made)
        (toasted ?o - object)

        (drink-ready)
        (food-ready)

        )


    (:durative-action get
        :parameters (?a - agent ?o - object)
        :duration   (and (>= ?duration 0.5) (<= ?duration 1.0))
        :condition  (and (at start (on-shelf ?o)))

        :effect     (and (at end (not (on-shelf ?o)))
                         (at end (on-table ?o))))


    (:durative-action make-coffee
        :parameters (?a - agent ?b - object)
        :duration   (and (>= ?duration 3.0) (<= ?duration 5.0))
        :condition  (and (at start (on-table ?b)))

        :effect     (and (at end (not (on-table ?b)))
                         (at end   (coffee-made))))


    (:durative-action pour-coffee
        :parameters (?a - agent ?m - object)
        :duration   (and (>= ?duration 0.5) (<= ?duration 1.0))
        :condition  (and (at start (coffee-made))
                         (at start (on-table ?m)))

        :effect     (and (at end (drink-ready))))


    (:durative-action pour-juice
        :parameters (?a - agent ?j ?g - object)
        :duration   (and (>= ?duration 0.5) (<= ?duration 1.0))
        :condition  (and (at start (on-table ?j))
                         (at start (on-table ?g)))

        :effect     (and (at end (drink-ready))))


    (:durative-action toast
        :parameters (?a - agent ?b - object)
        :duration   (and (>= ?duration 3.0) (<= ?duration 5.0))
        :condition  (and (at start (on-table ?b)))

        :effect     (and (at end (toasted ?b))))


    (:durative-action add-spread
        :parameters (?a - agent ?b ?c - object)
        :duration   (and (>= ?duration 1.0) (<= ?duration 2.0))
        :condition  (and (at start (on-table ?b))
                         (at start (on-table ?c))
                         (at start (toasted ?b)))

        :effect     (and (at end (food-ready))))

    (:durative-action mix
        :parameters (?a - agent ?c ?m - object)
        :duration   (and (>= ?duration 1.0) (<= ?duration 2.0))
        :condition  (and (at start (on-table ?c))
                         (at start (on-table ?m)))

        :effect     (and (at end (food-ready))))

)
