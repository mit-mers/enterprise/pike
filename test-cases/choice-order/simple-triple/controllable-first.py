#!/usr/bin/env python
"""
This file demonstrates how to use the pytpn API to build a TPN from scratch!
"""

from pytpn.tpn import TPN, Event, DecisionVariable, Episode, TemporalConstraint, Expression, And, Or, Not, Constant, Assignment, TPNSegment

if __name__ == '__main__':
    # Create a TPN
    tpn = TPN()
    # Create 3 activities (episodes with dispatch fields)
    a1 = tpn.create_episode(dispatch='(make-p t1)', duration=[2, 4])
    a2 = tpn.create_episode(dispatch='(make-p t2)', duration=[2, 4])

    a3 = tpn.create_episode(dispatch='(require-p t1)', duration=[2, 4])
    a4 = tpn.create_episode(dispatch='(require-p t2)', duration=[2, 4])

    a5 = tpn.create_episode(dispatch='(make-p t3)', duration=[2, 4])
    a6 = tpn.create_episode(dispatch='(make-p t4)', duration=[2, 4])

    a7 = tpn.create_episode(dispatch='(require-p t3)', duration=[2, 4])
    a8 = tpn.create_episode(dispatch='(require-p t4)', duration=[2, 4])


    # Make a choice!
    choice_1 = tpn.choice([a1, a2], domain=['c1', 'c2'], choice_id='x', type='controllable')
    choice_2 = tpn.choice([tpn.sequence([a3, a5]), tpn.sequence([a4, a6])], domain=['c1', 'c2'], choice_id='y1', type='uncontrollable')
    choice_3 = tpn.choice([a7, a8], domain=['c1', 'c2'], choice_id='y2', type='uncontrollable')

    seq = tpn.sequence([choice_1, choice_2, choice_3])

    # Set the TPN to this segment and write out!
    tpn.start_event = seq.from_event
    tpn.end_event = seq.to_event
    # Write a file!
    tpn.to_xml_file('controllable-first.tpn')
