#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt

import matplotlib.style
import matplotlib as mpl
import matplotlib.colors as colors
from matplotlib.colors import LinearSegmentedColormap
# See https://matplotlib.org/users/dflt_style_changes.html
mpl.rcParams['mathtext.fontset'] = 'cm'
mpl.rcParams['mathtext.rm'] = 'serif'

mpl.rcParams['legend.fancybox'] = False
# mpl.rcParams['legend.loc'] = 'upper right'
# mpl.rcParams['legend.numpoints'] = 2
# mpl.rcParams['legend.fontsize'] = 'large'
mpl.rcParams['legend.framealpha'] = None
mpl.rcParams['legend.scatterpoints'] = 3
mpl.rcParams['legend.edgecolor'] = 'inherit'

scale = 0.75
fontsize_default = 11 * 1 / scale

# Smaller file size: Use CMR10 (LaTeX) font
# plt.rcParams["font.family"] = "serif"
# plt.rcParams["font.serif"] = "cmr10"
# plt.rcParams["font.size"] = fontsize_default

# Use true LaTeX rendering
plt.rcParams["text.usetex"] = True
plt.rcParams["font.family"] = "serif"
plt.rcParams["font.serif"] = "Computer Modern Sans serif"
plt.rcParams["font.size"] = fontsize_default


def dot_plot_replans(ks, rs_separate, rs_concurrent, ts_separate, ts_concurrent):
    plt.plot([2, 10000], [0, 0], '--', color=(204.0/255, 51.0/255, 51.0/255), linewidth=1, label='Concurrent IR\&A')

    plt.semilogx(ks, rs_separate, 'o', fillstyle='full', color=(0, 0, 0.45), markeredgewidth=0.0, alpha=0.1, markersize=5, label='Separate IR\&A')

    #plt.semilogx(ks, rs_concurrent, 'o', fillstyle='full', color=(0.45, 0, 0), markeredgewidth=0.0, alpha=0.1, markersize=5, label='Concurrent IR\&A')

    plt.xlabel('Number of Intents in Plan ($k$)')
    plt.ylabel('Number of Replans')
    plt.title('Number of Replans vs Number of Plan Intents $k$', fontsize=fontsize_default)
    plt.legend()

    # Grid
    # plt.grid(True, linestyle='-', color=(0.95, 0.95, 0.95), which='both')
    # plt.gca().set_axisbelow(True)



def dot_plot_timed(ks, rs_separate, rs_concurrent, ts_separate, ts_concurrent):
    plt.semilogx(ks, ts_separate, 'o', fillstyle='full', color=(0, 0, 0.45), markeredgewidth=0.0, alpha=0.1, markersize=5, label='Separate IR&A')

    plt.semilogx(ks, ts_concurrent, 'o', fillstyle='full', color=(0.45, 0, 0), markeredgewidth=0.0, alpha=0.1, markersize=5, label='Concurrent IR&A')

    plt.xlabel('Number of Plan Intents')
    plt.ylabel('Total Execution Time (s)')
    plt.title('Total Online Execution Time vs Number of Plan Intents')
    plt.legend()


def plot_lines_timed(ks, rs_separate, rs_concurrent, ts_separate, ts_concurrent):
    def data_by_ks(ks, data):
        "Return a dictionary mapping k values to lists of data."
        d = {}
        for i in range(len(ks)):
            k = ks[i]
            m = data[i]
            if not k in d:
                d[k] = [m]
            else:
                d[k].append(m)
        return d

    d_ts_separate = data_by_ks(ks, ts_separate)
    # print(d_ts_separate)
    # Mean
    d_ts_mean = {k: np.mean(vals) for (k, vals) in d_ts_separate.items()}
    ks_sorted, ts_mean_sorted = list(zip(*sorted(d_ts_mean.items(), key=lambda p: p[0])))
    # Min
    d_ts_min = {k: min(vals) for (k, vals) in d_ts_separate.items()}
    ks_sorted, ts_min_sorted = list(zip(*sorted(d_ts_min.items(), key=lambda p: p[0])))
    # Max
    d_ts_max = {k: max(vals) for (k, vals) in d_ts_separate.items()}
    ks_sorted, ts_max_sorted = list(zip(*sorted(d_ts_max.items(), key=lambda p: p[0])))
    plt.semilogx(ks_sorted, ts_mean_sorted, color=(0, 0, 0.45), linewidth=0)
    #plt.semilogx(ks_sorted, ts_min_sorted, color=(0, 0, 0.45), linewidth=1, alpha=0.25)
    #plt.semilogx(ks_sorted, ts_max_sorted, color=(0, 0, 0.45), linewidth=1, alpha=0.25)
    plt.fill_between(ks_sorted, ts_min_sorted, ts_max_sorted, color=(0, 0, 0.45), alpha=0.5)

    d_ts_concurrent = data_by_ks(ks, ts_concurrent)
    # print(d_ts_separate)
    # Mean
    d_ts_mean = {k: np.mean(vals) for (k, vals) in d_ts_concurrent.items()}
    ks_sorted, ts_mean_sorted = list(zip(*sorted(d_ts_mean.items(), key=lambda p: p[0])))
    # Min
    d_ts_min = {k: min(vals) for (k, vals) in d_ts_concurrent.items()}
    ks_sorted, ts_min_sorted = list(zip(*sorted(d_ts_min.items(), key=lambda p: p[0])))
    # Max
    d_ts_max = {k: max(vals) for (k, vals) in d_ts_concurrent.items()}
    ks_sorted, ts_max_sorted = list(zip(*sorted(d_ts_max.items(), key=lambda p: p[0])))
    plt.semilogx(ks_sorted, ts_mean_sorted, color=(0, 0, 0.45), linewidth=0)
    #plt.semilogx(ks_sorted, ts_min_sorted, color=(0, 0, 0.45), linewidth=1, alpha=0.25)
    #plt.semilogx(ks_sorted, ts_max_sorted, color=(0, 0, 0.45), linewidth=1, alpha=0.25)
    plt.fill_between(ks_sorted, ts_min_sorted, ts_max_sorted, color=(0.45, 0, 0), alpha=0.5)



def ratio_plot_timed(ks, rs_separate, rs_concurrent, ts_separate, ts_concurrent, ts_separate_penalties, ts_concurrent_penalties, temporal_structures):
    # Select a colormap

    cmap = matplotlib.cm.get_cmap('viridis_r')
    #cmap = matplotlib.cm.get_cmap('plasma')
    #cmap = matplotlib.cm.get_cmap('cool')
    #cmap = LinearSegmentedColormap.from_list('Steve', [(0.45, 0, 0), (0, 0, 0.45)], N=256)

    plt.scatter(ts_separate, ts_concurrent,
                s=5**2, marker='o', linewidths=0,
                c=ks, cmap=cmap, alpha=1, norm=colors.LogNorm(vmin=min(ks), vmax=max(ks)),
                label='Point IR\&A')

    plt.plot([0, 42.5], [0, 42.5], color='k', linewidth=mpl.rcParams['axes.linewidth'])


    # plt.axis('equal')

    plt.axis([0, 42.5, 0, 42.5])
    plt.xticks([0, 5, 10, 15, 20, 25, 30, 35, 40])
    plt.yticks([0, 5, 10, 15, 20, 25, 30, 35, 40])
    plt.grid(True, linestyle='--', color=(0.8, 0.8, 0.8))
    plt.gca().set_axisbelow(True)
    plt.gca().set_aspect('equal', adjustable='box')

    plt.xlabel('Separate IR\&A Execution Time (s)')
    plt.ylabel('Concurrent IR\&A Execution Time (s)')
    plt.title('Total Time to Reach Goal', fontsize=fontsize_default)

    color_bar = plt.colorbar(label='Number of Intents ($k$)')
    # color_bar.set_alpha(1)
    # color_bar.draw_all()
    #plt.legend()


def ratio_plot_timed_2(ks, rs_separate, rs_concurrent, ts_separate, ts_concurrent):

    plt.semilogx(ks, np.array(ts_concurrent) / np.array(ts_separate), 'o', fillstyle='full', color=(0.45, 0, 0.45), markeredgewidth=0.0, alpha=0.1, markersize=5, label='Point IR&A')

    plt.plot([1, 10000], [1, 1], color='k', linewidth=1)

    plt.axis('equal')

    plt.ylim((0, 2))
    # plt.xlabel('Number of Intents in Plan')
    # plt.ylabel('Number of Replans')
    # plt.title('Number of Replans vs Number of Plan Intents')
    plt.legend()



if __name__ == '__main__':

    N = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16])
    bdd_size_original_ordering = np.array([13, 35, 83, 187, 411, 891, 1915, 4091, 8699, 18427, 38907, 81915, 172027, 360443, 753659, 1572859])
    bdd_size_better_ordering = np.array([13, 22, 31, 40, 49, 58, 67, 76, 85, 94, 103, 112, 121, 130, 139, 148])

    plt.semilogy(N, bdd_size_better_ordering, 'o-', fillstyle='full', color=(0., 0, 0.65), markeredgewidth=0.0, alpha=1, markersize=5, label='Better Variable Ordering')
    plt.semilogy(N, bdd_size_original_ordering, 'o-', fillstyle='full', color=(0.65, 0, 0), markeredgewidth=0.0, alpha=1, markersize=5, label='Original Variable Ordering')

    # ax = plt.gca()
    # locmaj = matplotlib.ticker.LinearLocator(numticks=8)
    # ax.xaxis.set_major_locator(locmaj)
    plt.xticks(range(2, 16 + 1, 2))
    # plt.xticks([2, 4, 6, 8, 10, 12, 14, 16])

    plt.xlabel('Problem Size ($N$)')
    plt.ylabel('Size of Policy BDD')
    plt.title('Policy BDD Size with Different Variable Orderings')
    plt.legend()
    plt.show()
