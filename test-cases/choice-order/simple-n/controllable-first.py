#!/usr/bin/env python
"""
This file demonstrates how to use the pytpn API to build a TPN from scratch!
"""

from pytpn.tpn import TPN, Event, DecisionVariable, Episode, TemporalConstraint, Expression, And, Or, Not, Constant, Assignment, TPNSegment
from bayesiannetwork.bayesian_network_generator import *

class SimpleNGenerator:
    def __init__(self, N=1):
        self.N = N

    def go(self):
        self.generate_ptpn()
        self.generate_bn()
        self.generate_pddl()


    def generate_bn(self):
        # Create a new "gripper status" decision variable

        variables = ['y{}'.format(i) for i in range(1, self.N + 1)]
        variable_description = {var: ['c1', 'c2'] for var in variables}

        # Generate the dependency structure
        G = nx.DiGraph()
        G.add_nodes_from(variables)

        # Now create CPT tables
        # Initial table
        cpts = []
        # Next tables
        for v in variables:
            cpts.append(CPT(v, [], np.array([0.9, 0.1])))

        bng = BayesianNetworkGenerator()
        bng.write_xmlbif_file('bn-{}.xmlbif'.format(self.N), variable_description, cpts)


    def generate_ptpn(self):
        # Create a TPN
        tpn = TPN()
        chunks = []
        for i in range(self.N):
            # Create 3 activities (episodes with dispatch fields)
            a1 = tpn.create_episode(dispatch='(make-p t{})'.format(2*i + 1), duration=[2, 4])
            a2 = tpn.create_episode(dispatch='(make-p t{})'.format(2*i + 2), duration=[2, 4])

            a3 = tpn.create_episode(dispatch='(require-p t{})'.format(2*i + 1), duration=[2, 4])
            a4 = tpn.create_episode(dispatch='(require-p t{})'.format(2*i + 2), duration=[2, 4])

            choice_1 = tpn.choice([a1, a2], domain=['c1', 'c2'], choice_id=tpn.create_unique_id(prefix='x', ids_taken={dv.id for dv in tpn.decision_variables}), type='controllable')
            choice_2 = tpn.choice([a3, a4], domain=['c1', 'c2'], choice_id=tpn.create_unique_id(prefix='y', ids_taken={dv.id for dv in tpn.decision_variables}), type='uncontrollable')

            chunks.extend([choice_1, choice_2])

        seq = tpn.sequence(chunks)

        # Set the TPN to this segment and write out!
        tpn.start_event = seq.from_event
        tpn.end_event = seq.to_event
        # Write a file!
        tpn.to_xml_file('controllable-first-{}.tpn'.format(self.N))


    def generate_pddl(self):
        with open('problem-template.pddl', 'r') as f:
            template = f.read()
        with open('problem-{}.pddl'.format(self.N), 'w') as f:
            objects = '\n'.join('t{} - thing'.format(i) for i in range(1, 2*self.N + 1))
            f.write(template.format(objects=objects))

if __name__ == '__main__':
    for N in range(1, 20 + 1):
        sng = SimpleNGenerator(N=N)
        sng.go()
