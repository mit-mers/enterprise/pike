See example in my August 2018 lab notebook.

In this example, we have a corelated Bayesian network with two variables. We swap which variable in the BN controls the first/second choice in the pTPN, and show that the order does indeed affect the resulting risk estimates.
