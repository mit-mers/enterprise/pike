#!/usr/bin/env python
"""
This file demonstrates how to use the pytpn API to build a TPN from scratch!
"""

from pytpn.tpn import TPN, Event, DecisionVariable, Episode, TemporalConstraint, Expression, And, Or, Not, Constant, Assignment, TPNSegment

if __name__ == '__main__':
    # Create a TPN
    tpn = TPN()
    # Create 3 activities (episodes with dispatch fields)
    a1 = tpn.create_episode(dispatch='(make-p t1)', duration=[2, 4])
    a2 = tpn.create_episode(dispatch='(make-p t2)', duration=[2, 4])
    #a3 = tpn.create_episode(dispatch='(make-p t3)', duration=[2, 4])

    a4 = tpn.create_episode(dispatch='(require-p t1)', duration=[2, 4])
    a5 = tpn.create_episode(dispatch='(require-p t2)', duration=[2, 4])
    #a6 = tpn.create_episode(dispatch='(require-p t3)', duration=[2, 4])

    # Make a choice!
    choice_1 = tpn.choice([a1, a2], domain=['c1', 'c2'], choice_id='y', type='uncontrollable')
    choice_2 = tpn.choice([a4, a5], domain=['c1', 'c2'], choice_id='x', type='controllable')

    seq = tpn.sequence([choice_1, choice_2])

    # Set the TPN to this segment and write out!
    tpn.start_event = seq.from_event
    tpn.end_event = seq.to_event
    # Write a file!
    tpn.to_xml_file('uncontrollable-first.tpn')
