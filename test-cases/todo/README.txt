Test cases that don't quite work
=================================

todo-1:
==========
This case demonstrates a limitation in the dispatcher API. Note quite sure yet what the best way to fix it is. When Pike dispatches an activity, it outputs a [lb, ub] for the activity dispatcher. Example: (launch-quadcopter q1) [2, 5]. This tells the activity dispatcher that actually implements the launch opereation that it can take *anywhere* between 2 and 5 seconds to launch the quadcopter.

It is, however, possible to construct TPN's will disjunctive, non-overlapping durations that make such ranges likely to be invalid. todo-1.rmpl (and todo-1.rmpl.tpn) demonstrate this. 

In this example, there is a choice between a1 and a2, which tke either [1, 2] or [4, 5] respectively. These two actions are in parallel with foo(). So no matter what, foo will take around [1, 2] seconds or [4, 5] seconds, depending on the choice. It cannot take, however, 3 seconds to complete - that would make the plan inconsistent. 

In this example, Pike should somehow tell the activity dispatcher about the disjunctive, non-overlapping duration for foo. Right now it doesn't, and it will choose something like [1, 5] (which again, may lead to error if the activity dispatcher chooses a duration of 3).

Pike probably should, at minimum, warn about such non-overlapping durations.

