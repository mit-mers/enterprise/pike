#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt

if __name__ == '__main__':
    # Load data
    with open('data-1.dat', 'r') as f:
        data = f.read()
    # Parse the data. First field represents k (number of 3 choice human/robot segments), and
    # second field represents replan count
    ks = []
    rs = []
    for line in data.strip().split('\n'):
        k, r = line.split(', ')
        k = int(k)
        r = int(r)
        ks.append(k)
        rs.append(r)

    plt.plot(ks, rs, 'o', alpha=0.1, color='k')
    plt.xlabel('k')
    plt.ylabel('Replans')
    plt.title('Replans vs Discrete k')
    plt.show()
