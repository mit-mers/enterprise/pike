#!/usr/bin/env python

import itertools
import numpy as np
import matplotlib.pyplot as plt

class IntentSampler(object):
    def __init__(self, bases, ranges):
        self.bases = bases
        self.ranges = ranges
        # Compute a dictionary to ease in mapping
        exponents = itertools.product(*([ranges]*len(bases)))
        self.prods = []
        for exponent in exponents:
            self.prods.append((np.prod(bases**exponent), np.array(exponent)))


    def get_closest_factorization(self, k):
        dists = (np.array([prod[0] for prod in self.prods]) - k)**2
        i_min = np.argmin(dists)
        return self.prods[i_min][1]


    def logarathmic_sample(self, k_min, k_max):
        lk_min = np.log(k_min)
        lk_max = np.log(k_max)
        lk = lk_min + np.random.random()*(lk_max - lk_min)
        return np.exp(lk)


if __name__ == '__main__':
    bases = np.array([2, 3, 5])
    ranges = list(range(0, 6))
    iss = IntentSampler(bases, ranges)

    samples = []
    for i in range(50):
        k_target = iss.logarathmic_sample(1, 10000)
        exponent_closest = iss.get_closest_factorization(k_target)
        k = np.prod(bases ** exponent_closest)
        print(k)
        samples.append(k)

    plt.semilogx(samples, np.zeros(len(samples)), 'o')
    plt.xlim((0, 10000))
    plt.show()
