#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt

import matplotlib.style
import matplotlib as mpl
import matplotlib.colors as colors
from matplotlib.colors import LinearSegmentedColormap
# See https://matplotlib.org/users/dflt_style_changes.html
mpl.rcParams['mathtext.fontset'] = 'cm'
mpl.rcParams['mathtext.rm'] = 'serif'

mpl.rcParams['legend.fancybox'] = False
# mpl.rcParams['legend.loc'] = 'upper right'
# mpl.rcParams['legend.numpoints'] = 2
# mpl.rcParams['legend.fontsize'] = 'large'
mpl.rcParams['legend.framealpha'] = None
mpl.rcParams['legend.scatterpoints'] = 3
mpl.rcParams['legend.edgecolor'] = 'inherit'

scale = 0.75
fontsize_default = 11 * 1 / scale

# Smaller file size: Use CMR10 (LaTeX) font
# plt.rcParams["font.family"] = "serif"
# plt.rcParams["font.serif"] = "cmr10"
# plt.rcParams["font.size"] = fontsize_default

# Use true LaTeX rendering
plt.rcParams["text.usetex"] = True
plt.rcParams["font.family"] = "serif"
plt.rcParams["font.serif"] = "Computer Modern Sans serif"
plt.rcParams["font.size"] = fontsize_default

def load_data(filename):
    # Load data
    with open(filename, 'r') as f:
        data = f.read()
    # Parse the data. First field represents k (number of 3 choice human/robot segments), and
    # second field represents replan count
    ks = []
    rs_separate = []
    rs_concurrent = []
    ts_separate = []
    ts_concurrent = []
    ts_separate_penalties = []
    ts_concurrent_penalties = []
    temporal_structures = []
    line_counter = 1
    for line in data.strip().split('\n'):
        structure, d = line.split(': ')
        d = d.split(' ')
        r_separate, t_separate, t_penalty_separate, r_concurrent, t_concurrent, t_penalty_concurrent, temporal_structure = int(d[0]), float(d[1]), float(d[2]), int(d[3]), float(d[4]), float(d[5]), d[6]

        # Sanity check!!
        if r_concurrent != 0:
            print("Warning: Concurrent one replanned (Line {}). Skipping.".format(line_counter))
            # continue
            raise Exception("Error! Concurrent one replanned?!")


        structure = [int(x) for x in structure.split(',')]
        k = np.prod(structure)
        ks.append(k)
        rs_separate.append(r_separate)
        rs_concurrent.append(r_concurrent)
        ts_separate.append(t_separate)
        ts_concurrent.append(t_concurrent)
        ts_separate_penalties.append(t_penalty_separate)
        ts_concurrent_penalties.append(t_penalty_concurrent)
        temporal_structures.append(temporal_structure)
        line_counter += 1

    return (np.array(ks), np.array(rs_separate), np.array(rs_concurrent), np.array(ts_separate), np.array(ts_concurrent), np.array(ts_separate_penalties), np.array(ts_concurrent_penalties), np.array(temporal_structures))



def dot_plot_replans(ks, rs_separate, rs_concurrent, ts_separate, ts_concurrent):
    plt.plot([2, 10000], [0, 0], '--', color=(204.0/255, 51.0/255, 51.0/255), linewidth=1, label='Concurrent IR\&A')

    plt.semilogx(ks, rs_separate, 'o', fillstyle='full', color=(0, 0, 0.45), markeredgewidth=0.0, alpha=0.1, markersize=5, label='Separate IR\&A')

    #plt.semilogx(ks, rs_concurrent, 'o', fillstyle='full', color=(0.45, 0, 0), markeredgewidth=0.0, alpha=0.1, markersize=5, label='Concurrent IR\&A')

    plt.xlabel('Number of Intents in Plan ($k$)')
    plt.ylabel('Number of Replans')
    plt.title('Number of Replans vs Number of Plan Intents $k$', fontsize=fontsize_default)
    plt.legend()

    # Grid
    # plt.grid(True, linestyle='-', color=(0.95, 0.95, 0.95), which='both')
    # plt.gca().set_axisbelow(True)



def dot_plot_timed(ks, rs_separate, rs_concurrent, ts_separate, ts_concurrent):
    plt.semilogx(ks, ts_separate, 'o', fillstyle='full', color=(0, 0, 0.45), markeredgewidth=0.0, alpha=0.1, markersize=5, label='Separate IR&A')

    plt.semilogx(ks, ts_concurrent, 'o', fillstyle='full', color=(0.45, 0, 0), markeredgewidth=0.0, alpha=0.1, markersize=5, label='Concurrent IR&A')

    plt.xlabel('Number of Plan Intents')
    plt.ylabel('Total Execution Time (s)')
    plt.title('Total Online Execution Time vs Number of Plan Intents')
    plt.legend()


def plot_lines_timed(ks, rs_separate, rs_concurrent, ts_separate, ts_concurrent):
    def data_by_ks(ks, data):
        "Return a dictionary mapping k values to lists of data."
        d = {}
        for i in range(len(ks)):
            k = ks[i]
            m = data[i]
            if not k in d:
                d[k] = [m]
            else:
                d[k].append(m)
        return d

    d_ts_separate = data_by_ks(ks, ts_separate)
    # print(d_ts_separate)
    # Mean
    d_ts_mean = {k: np.mean(vals) for (k, vals) in d_ts_separate.items()}
    ks_sorted, ts_mean_sorted = list(zip(*sorted(d_ts_mean.items(), key=lambda p: p[0])))
    # Min
    d_ts_min = {k: min(vals) for (k, vals) in d_ts_separate.items()}
    ks_sorted, ts_min_sorted = list(zip(*sorted(d_ts_min.items(), key=lambda p: p[0])))
    # Max
    d_ts_max = {k: max(vals) for (k, vals) in d_ts_separate.items()}
    ks_sorted, ts_max_sorted = list(zip(*sorted(d_ts_max.items(), key=lambda p: p[0])))
    plt.semilogx(ks_sorted, ts_mean_sorted, color=(0, 0, 0.45), linewidth=0)
    #plt.semilogx(ks_sorted, ts_min_sorted, color=(0, 0, 0.45), linewidth=1, alpha=0.25)
    #plt.semilogx(ks_sorted, ts_max_sorted, color=(0, 0, 0.45), linewidth=1, alpha=0.25)
    plt.fill_between(ks_sorted, ts_min_sorted, ts_max_sorted, color=(0, 0, 0.45), alpha=0.5)

    d_ts_concurrent = data_by_ks(ks, ts_concurrent)
    # print(d_ts_separate)
    # Mean
    d_ts_mean = {k: np.mean(vals) for (k, vals) in d_ts_concurrent.items()}
    ks_sorted, ts_mean_sorted = list(zip(*sorted(d_ts_mean.items(), key=lambda p: p[0])))
    # Min
    d_ts_min = {k: min(vals) for (k, vals) in d_ts_concurrent.items()}
    ks_sorted, ts_min_sorted = list(zip(*sorted(d_ts_min.items(), key=lambda p: p[0])))
    # Max
    d_ts_max = {k: max(vals) for (k, vals) in d_ts_concurrent.items()}
    ks_sorted, ts_max_sorted = list(zip(*sorted(d_ts_max.items(), key=lambda p: p[0])))
    plt.semilogx(ks_sorted, ts_mean_sorted, color=(0, 0, 0.45), linewidth=0)
    #plt.semilogx(ks_sorted, ts_min_sorted, color=(0, 0, 0.45), linewidth=1, alpha=0.25)
    #plt.semilogx(ks_sorted, ts_max_sorted, color=(0, 0, 0.45), linewidth=1, alpha=0.25)
    plt.fill_between(ks_sorted, ts_min_sorted, ts_max_sorted, color=(0.45, 0, 0), alpha=0.5)



def ratio_plot_timed(ks, rs_separate, rs_concurrent, ts_separate, ts_concurrent, ts_separate_penalties, ts_concurrent_penalties, temporal_structures):
    # Select a colormap

    cmap = matplotlib.cm.get_cmap('viridis_r')
    #cmap = matplotlib.cm.get_cmap('plasma')
    #cmap = matplotlib.cm.get_cmap('cool')
    #cmap = LinearSegmentedColormap.from_list('Steve', [(0.45, 0, 0), (0, 0, 0.45)], N=256)

    plt.scatter(ts_separate, ts_concurrent,
                s=5**2, marker='o', linewidths=0,
                c=ks, cmap=cmap, alpha=1, norm=colors.LogNorm(vmin=min(ks), vmax=max(ks)),
                label='Point IR\&A')

    plt.plot([0, 42.5], [0, 42.5], color='k', linewidth=mpl.rcParams['axes.linewidth'])


    # plt.axis('equal')

    plt.axis([0, 42.5, 0, 42.5])
    plt.xticks([0, 5, 10, 15, 20, 25, 30, 35, 40])
    plt.yticks([0, 5, 10, 15, 20, 25, 30, 35, 40])
    plt.grid(True, linestyle='--', color=(0.8, 0.8, 0.8))
    plt.gca().set_axisbelow(True)
    plt.gca().set_aspect('equal', adjustable='box')

    plt.xlabel('Separate IR\&A Execution Time (s)')
    plt.ylabel('Concurrent IR\&A Execution Time (s)')
    plt.title('Total Time to Reach Goal', fontsize=fontsize_default)

    color_bar = plt.colorbar(label='Number of Intents ($k$)')
    # color_bar.set_alpha(1)
    # color_bar.draw_all()
    #plt.legend()


def ratio_plot_timed_2(ks, rs_separate, rs_concurrent, ts_separate, ts_concurrent):

    plt.semilogx(ks, np.array(ts_concurrent) / np.array(ts_separate), 'o', fillstyle='full', color=(0.45, 0, 0.45), markeredgewidth=0.0, alpha=0.1, markersize=5, label='Point IR&A')

    plt.plot([1, 10000], [1, 1], color='k', linewidth=1)

    plt.axis('equal')

    plt.ylim((0, 2))
    # plt.xlabel('Number of Intents in Plan')
    # plt.ylabel('Number of Replans')
    # plt.title('Number of Replans vs Number of Plan Intents')
    plt.legend()



if __name__ == '__main__':

    # ks, rs = load_data('data-concurrent-ira-final.dat')
    # print("{} data points in concurrent IR&A".format(len(ks)))
    # plt.semilogx(ks, rs, 'o', fillstyle='full', color=(0.45, 0, 0), markeredgewidth=0.0, alpha=0.1, markersize=5, label='Concurrent IR&A')



    (ks, rs_separate, rs_concurrent, ts_separate, ts_concurrent, ts_separate_penalties, ts_concurrent_penalties, temporal_structures) = load_data('data.dat')
    print("{} data points in IR&A data".format(len(ks)))


    # penalty_add_factor = -1 # offsets the recorded penalty (as if it weren't applied in data)
    # penalty_add_factor = 0 # doesn't do anything (includes recorded penalty)
    # penalty_add_factor = 1 # doubles it (because it's already added in data)
    penalty_add_factor = 0

    ts_separate   = ts_separate   + penalty_add_factor * ts_separate_penalties
    ts_concurrent = ts_concurrent + penalty_add_factor * ts_concurrent_penalties

    print("Applied penalty factor: {}".format(penalty_add_factor))

    print("Concurrent took longer than separate in {} instance(s)".format(np.sum(ts_concurrent > ts_separate)))

    print("Average ratio: {}".format(np.mean(np.array(ts_concurrent) / np.array(ts_separate))))


    #dot_plot_timed(ks, rs_separate, rs_concurrent, ts_separate, ts_concurrent)

    #plt.figure()
    dot_plot_replans(ks, rs_separate, rs_concurrent, ts_separate, ts_concurrent)

    #plt.figure()
    #plot_lines_timed(ks, rs_separate, rs_concurrent, ts_separate, ts_concurrent)

    plt.figure()
    ratio_plot_timed(ks, rs_separate, rs_concurrent, ts_separate, ts_concurrent, ts_separate_penalties, ts_concurrent_penalties, temporal_structures)

    # plt.figure()
    # ratio_plot_timed_2(ks, rs_separate, rs_concurrent, ts_separate, ts_concurrent)

    plt.show()
