#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt

import matplotlib.style
import matplotlib as mpl
# See https://matplotlib.org/users/dflt_style_changes.html
mpl.rcParams['mathtext.fontset'] = 'cm'
mpl.rcParams['mathtext.rm'] = 'serif'

mpl.rcParams['legend.fancybox'] = False
# mpl.rcParams['legend.loc'] = 'upper right'
# mpl.rcParams['legend.numpoints'] = 2
# mpl.rcParams['legend.fontsize'] = 'large'
mpl.rcParams['legend.framealpha'] = None
mpl.rcParams['legend.scatterpoints'] = 3
mpl.rcParams['legend.edgecolor'] = 'inherit'

def load_data(filename):
    # Load data
    with open(filename, 'r') as f:
        data = f.read()
    # Parse the data. First field represents k (number of 3 choice human/robot segments), and
    # second field represents replan count
    ks = []
    rs = []
    for line in data.strip().split('\n'):
        structure, r = line.split(': ')
        r = int(r)
        structure = [int(x) for x in structure.split(',')]
        k = np.prod(structure)
        ks.append(k)
        rs.append(r)
    return (ks, rs)



if __name__ == '__main__':

    # ks, rs = load_data('data-concurrent-ira-final.dat')
    # print("{} data points in concurrent IR&A".format(len(ks)))
    # plt.semilogx(ks, rs, 'o', fillstyle='full', color=(0.45, 0, 0), markeredgewidth=0.0, alpha=0.1, markersize=5, label='Concurrent IR&A')

    plt.plot([2, 10000], [0, 0], '--', color=(204.0/255, 51.0/255, 51.0/255), linewidth=1, label='Concurrent IR&A')

    # ks, rs = load_data('data-separate-ira-final.dat')
    ks, rs = load_data('data.dat')
    print("{} data points in separate IR&A".format(len(ks)))
    plt.semilogx(ks, rs, 'o', fillstyle='full', color=(0, 0, 0.45), markeredgewidth=0.0, alpha=0.1, markersize=5, label='Separate IR&A')




    plt.xlabel('Number of Intents in Plan')
    plt.ylabel('Number of Replans')
    plt.title('Replan Count vs Number of Plan Intents')
    plt.legend()
    plt.show()
