#!/usr/bin/env python

"""
Represents a k-scalable human robot interaction scenario
useful for testing.

"""
from __future__ import division
from rmpyl.rmpyl import RMPyL, Episode
import argparse

class KScalableProblemGenerator(object):
    def __init__(self, N_domain=3, N_segments=2):
        # Parameters!
        self.N_domain = N_domain
        self.N_segments = N_segments

        self.prog = None


    def generate_segment(self, i):
        return self.prog.sequence(self.generate_human_choice(i), self.generate_robot_choice(i))


    def generate_human_choice(self, i):
        return self.prog.observe({
            'name': 'H{}'.format(i),
            'id': 'H{}'.format(i),
            'ctype': 'uncontrollable',
            'domain': ['h{}'.format(j) for j in range(1, self.N_domain + 1)]
            },
            *[Episode(action='(h_{}_{})'.format(i, j), duration={'ctype': 'controllable', 'lb': 1.0, 'ub': 2.0}) for j in range(1, self.N_domain + 1)])

    def generate_robot_choice(self, i):
        return self.prog.decide({
            'name': 'R{}'.format(i),
            'id': 'R{}'.format(i),
            'domain': ['r{}'.format(j) for j in range(1, self.N_domain + 1)],
            'utility': [0]*self.N_domain,
            },
            *[Episode(action='(r_{}_{})'.format(i, j), duration={'ctype': 'controllable', 'lb': 1.0, 'ub': 2.0}) for j in range(1, self.N_domain + 1)])


    def generate(self, start_index=1):
        # Generate it!
        self.prog = RMPyL(name='run()')
        segments = [self.generate_segment(i) for i in range(start_index, self.N_segments + 1)]
        self.prog *= self.prog.sequence(*segments)
        return self.prog


    def generate_pddl(self, domain_filename="domain.pddl", problem_filename="problem.pddl"):
        # Generate a PDDL domain file
        with open(domain_filename, 'w') as f:
            predicates = ['(p_{}_{})'.format(i, j) for i in range(1, self.N_segments + 1) for j in range(1, self.N_domain + 1)]
            f.write("""(define (domain k-scalable)
    (:requirements :strips :typing :durative-actions :duration-inequalities)

    (:types {})

""".format(' '.join(predicates)))

            # Generate  actions
            for i in range(1, self.N_segments + 1):
                for j in range(1, self.N_domain + 1):
                    action_human = self.generate_pddl_action("h_{}_{}".format(i, j),
                                                             effects=["(p_{}_{})".format(i, j)])
                    action_robot = self.generate_pddl_action("r_{}_{}".format(i, j),
                                                             preconditions=["(p_{}_{})".format(i, j)])
                    f.write(action_human)
                    f.write(action_robot)

            # Close the PDDL file
            f.write(")")

        # Generate a PDDL problem file
        with open(problem_filename, 'w') as f:
            f.write("""(define (problem kitchen-1)
    (:domain k-scalable)
    (:objects )

    (:init )

    (:goal
        (and )))
""")
        return True


    def generate_pddl_action(self, name, preconditions=[], effects=[]):
        return """    (:durative-action {}
        :parameters ()
        :duration   (and (>= ?duration 1) (<= ?duration 2))
        :condition  (and {})
        :effect     (and {}))

""".format(name, ' '.join("(at start {})".format(p) for p in preconditions), ' '.join("(at end {})".format(p) for p in effects))



if __name__=='__main__':
    parser = argparse.ArgumentParser(description="k-scalable problem generator")
    parser.add_argument("--N_segments", help="Number of segments", type=int, default=2)
    parser.add_argument("--N_domain", help="Size of each domain", type=int, default=3)
    parser.add_argument("--i_start", help="Start index of generation", type=int, default=1)
    args = parser.parse_args()


    print("N_segments = {}".format(args.N_segments))
    print("N_domain = {}".format(args.N_domain))
    print("i_start = {}".format(args.i_start))

    ksg = KScalableProblemGenerator(N_domain=args.N_domain, N_segments=args.N_segments)
    prog = ksg.generate(start_index=args.i_start)
    prog.to_ptpn(filename="plan.tpn")
    ksg.generate_pddl()
