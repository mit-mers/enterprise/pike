# Intro

These tests represent a k-scalable domain: a domain where we can turn a knob, and see how Pike changes. Specifically, these tests aim to show how concurrent IR&A is better than separate IR&A.

To do separate IR&A, we implement this on a simple domian with replanning. See some of the attached TPN's -- there are pairs of choices, a human first then a robot, and causal link between pairs of actions. We can scale the length of this plan to have plans with more intents (k) or less. The separate IR&A runs "intent inference" to try and guess the intent -- but it could be wrong, causing replans to happen, in which we start from wherever that happened but use the observation. In essence, this causes replans to happen because the separate IR&A only uses a single hypothesis, and is very likely to "guess" wrong. Pike, using concurrent IR&A, should never need to replan.

Make sure there's `/home/steve/Desktop/random_tpn/` testing infrastructure available!

# Instructions

To run these tests:

```
(ql:quickload :pike/test)
(test-k-scalable-domain)
```

This will start outputting data to the data.dat file. Each line contains the structure "structure:recounts", where structure is something like "2,3,2,2" etc. This means a pair of 2 choices, followed by a pair of 3 choices, ... etc.

This code samples logarthmically to determine how many intents should be in the plan, calls the k-scalable.py script (from /home/steve/Desktop/random_tpn -- because it uses my testing infrastructure there to make the TPN), and generates also PDDL domain and problem files.

A replan loop is started, and a line is written each time.

## Running the plotting code

From this directory, run:

```
./scripts/k-scalable-plotter.py
```
