(define (domain k-scalable)
    (:requirements :strips :typing :durative-actions :duration-inequalities)

    (:types (p_1_1) (p_1_2) (p_2_1) (p_2_2) (p_3_1) (p_3_2))

    (:durative-action h_1_1
        :parameters ()
        :duration   (and (>= ?duration 1) (<= ?duration 2))
        :condition  (and )
        :effect     (and (at end (p_1_1))))

    (:durative-action r_1_1
        :parameters ()
        :duration   (and (>= ?duration 1) (<= ?duration 2))
        :condition  (and (at start (p_1_1)))
        :effect     (and ))

    (:durative-action h_1_2
        :parameters ()
        :duration   (and (>= ?duration 1) (<= ?duration 2))
        :condition  (and )
        :effect     (and (at end (p_1_2))))

    (:durative-action r_1_2
        :parameters ()
        :duration   (and (>= ?duration 1) (<= ?duration 2))
        :condition  (and (at start (p_1_2)))
        :effect     (and ))

    (:durative-action h_2_1
        :parameters ()
        :duration   (and (>= ?duration 1) (<= ?duration 2))
        :condition  (and )
        :effect     (and (at end (p_2_1))))

    (:durative-action r_2_1
        :parameters ()
        :duration   (and (>= ?duration 1) (<= ?duration 2))
        :condition  (and (at start (p_2_1)))
        :effect     (and ))

    (:durative-action h_2_2
        :parameters ()
        :duration   (and (>= ?duration 1) (<= ?duration 2))
        :condition  (and )
        :effect     (and (at end (p_2_2))))

    (:durative-action r_2_2
        :parameters ()
        :duration   (and (>= ?duration 1) (<= ?duration 2))
        :condition  (and (at start (p_2_2)))
        :effect     (and ))

    (:durative-action h_3_1
        :parameters ()
        :duration   (and (>= ?duration 1) (<= ?duration 2))
        :condition  (and )
        :effect     (and (at end (p_3_1))))

    (:durative-action r_3_1
        :parameters ()
        :duration   (and (>= ?duration 1) (<= ?duration 2))
        :condition  (and (at start (p_3_1)))
        :effect     (and ))

    (:durative-action h_3_2
        :parameters ()
        :duration   (and (>= ?duration 1) (<= ?duration 2))
        :condition  (and )
        :effect     (and (at end (p_3_2))))

    (:durative-action r_3_2
        :parameters ()
        :duration   (and (>= ?duration 1) (<= ?duration 2))
        :condition  (and (at start (p_3_2)))
        :effect     (and ))

)