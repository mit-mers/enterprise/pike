(define (domain pike-np-complete)
	(:requirements :strips :typing :durative-actions :duration-inequalities)
	(:types clause)

	(:predicates
    (clause-holds ?c - clause)
	)


	(:durative-action activity-x
		:parameters ()
		:duration (and (>= ?duration 1) (<= ?duration 2))
		:condition (and	)

		:effect 	(and 	(at end (clause-holds c1))
                    (at end (clause-holds c3)))
	)

  (:durative-action activity-not-x
    :parameters ()
    :duration (and (>= ?duration 1) (<= ?duration 2))
    :condition (and	)

    :effect 	(and 	(at end (clause-holds c2)))
  )

  (:durative-action activity-y
    :parameters ()
    :duration (and (>= ?duration 1) (<= ?duration 2))
    :condition (and	)

    :effect 	(and 	(at end (clause-holds c2)))
  )

  (:durative-action activity-not-y
    :parameters ()
    :duration (and (>= ?duration 1) (<= ?duration 2))
    :condition (and	)

    :effect 	(and 	(at end (clause-holds c1)))
  )

  (:durative-action activity-z
    :parameters ()
    :duration (and (>= ?duration 1) (<= ?duration 2))
    :condition (and	)

    :effect 	(and 	(at end (clause-holds c2))
                    (at end (clause-holds c3)))
  )

  (:durative-action activity-not-z
    :parameters ()
    :duration (and (>= ?duration 1) (<= ?duration 2))
    :condition (and	)

    :effect 	(and 	)
  )




  (:durative-action activity-clause-c1
    :parameters ()
    :duration (and (>= ?duration 1) (<= ?duration 2))
    :condition (and	(at start (clause-holds c1)))
    :effect 	(and 	)
  )

  (:durative-action activity-clause-c2
    :parameters ()
    :duration (and (>= ?duration 1) (<= ?duration 2))
    :condition (and	(at start (clause-holds c2)))
    :effect 	(and 	)
  )

  (:durative-action activity-clause-c3
    :parameters ()
    :duration (and (>= ?duration 1) (<= ?duration 2))
    :condition (and	(at start (clause-holds c3)))
    :effect 	(and 	)
  )

)
