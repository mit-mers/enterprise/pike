#!/usr/bin/env python

"""
RMPyL program modeling a collaborative pick-and-place task between a human and a robot.
"""
from rmpyl.rmpyl import RMPyL, Episode


def variable_activity(prog, variable):
    return prog.decide({'name': variable,
                        'domain': ['true', 'false']},
                       Episode(action='(activity-{})'.format(variable), duration={'ctype': 'controllable', 'lb': 1, 'ub': 2}),
                       Episode(action='(activity-not-{})'.format(variable), duration={'ctype': 'controllable', 'lb': 1, 'ub': 2}))

def clause_activity(prog, clause):
    return Episode(action='(activity-clause-{})'.format(clause), duration={'ctype': 'controllable', 'lb': 1, 'ub': 2})


def main_plan():
    prog = RMPyL(name='run()')
    prog *= prog.sequence(prog.parallel(variable_activity(prog, 'x'),
                                        variable_activity(prog, 'y'),
                                        variable_activity(prog, 'z')),
                          prog.parallel(clause_activity(prog, 'c1'),
                                        clause_activity(prog, 'c2'),
                                        clause_activity(prog, 'c3')))
    return prog


if __name__=='__main__':
    prog = main_plan()
    prog.to_ptpn(filename="pike_np_hard_mapping_example.tpn")
