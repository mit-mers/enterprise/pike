(define (domain simple-domain)
    (:requirements :strips :typing :durative-actions :duration-inequalities)
  (:types thing)

  (:predicates
   (has-sugar)
   (has-fruit)
   (has-dessert)
   )

  (:durative-action get-sugar
                    :parameters ()
                    :duration (and (>= ?duration 2) (<= ?duration 4))
                    :condition (and	)
                    :effect (and  (at end (has-sugar) )))

  (:durative-action get-fruit
                    :parameters ()
                    :duration (and (>= ?duration 2) (<= ?duration 4))
                    :condition (and	)
                    :effect (and  (at end (has-fruit) )))

  (:durative-action make-cookies
                    :parameters ()
                    :duration (and (>= ?duration 2) (<= ?duration 4))
                    :condition (and	(at start (has-sugar)))
                    :effect (and (at end (has-dessert))))

  (:durative-action make-brownies
                    :parameters ()
                    :duration (and (>= ?duration 2) (<= ?duration 4))
                    :condition (and	(at start (has-sugar)))
                    :effect (and  ))

  (:durative-action bake-brownies
                    :parameters ()
                    :duration (and (>= ?duration 2) (<= ?duration 4))
                    :condition (and	)
                    :effect (and (at end (has-dessert)) ))

  (:durative-action make-fruit-salad
                    :parameters ()
                    :duration (and (>= ?duration 2) (<= ?duration 4))
                    :condition (and	(at start (has-fruit)))
                    :effect (and (at end (has-dessert))))

  (:durative-action throw-away-burnt-brownies
                    :parameters ()
                    :duration (and (>= ?duration 2) (<= ?duration 4))
                    :condition (and	)
                    :effect (and (at end (not (has-dessert))) (at end (not (has-sugar))) ))

  (:durative-action noop
                    :parameters ()
                    :duration (and (>= ?duration 2) (<= ?duration 4))
                    :condition (and )
                    :effect (and ))

  (:durative-action buy-more-sugar-at-store
                    :parameters ()
                    :duration (and (>= ?duration 2) (<= ?duration 4))
                    :condition (and	)
                    :effect (and  (at end (has-sugar) )))

  (:durative-action store-out-of-sugar
                    :parameters ()
                    :duration (and (>= ?duration 2) (<= ?duration 4))
                    :condition (and	)
                    :effect (and  )))


  )
