#!/usr/bin/env python
"""
This file demonstrates how to use the pytpn API to build a TPN from scratch!
"""

from pytpn.tpn import TPN, Event, DecisionVariable, Episode, TemporalConstraint, Expression, And, Or, Not, Constant, Assignment, TPNSegment

if __name__ == '__main__':
    # Create a TPN
    tpn = TPN()

    # Activities
    a_sugar = tpn.create_episode(dispatch='(get-sugar)', duration=[2, 4])
    a_fruit = tpn.create_episode(dispatch='(get-fruit)', duration=[2, 4])

    a_brownies_1 = tpn.create_episode(dispatch='(make-brownies)', duration=[2, 4])
    a_cookies = tpn.create_episode(dispatch='(make-cookies)', duration=[2, 4])
    a_fruit_salad = tpn.create_episode(dispatch='(make-fruit-salad)', duration=[2, 4])

    a_bake = tpn.create_episode(dispatch='(bake-brownies)', duration=[2, 4])
    a_throw_away = tpn.create_episode(dispatch='(throw-away-burnt-brownies)', duration=[2, 4])
    a_buy_more_sugar = tpn.create_episode(dispatch='(buy-more-sugar-at-store)', duration=[2, 4])
    a_no_sugar = tpn.create_episode(dispatch='(store-out-of-sugar)', duration=[2, 4])
    a_brownies_2 = tpn.create_episode(dispatch='(make-brownies)', duration=[2, 4])
    a_bake_2 = tpn.create_episode(dispatch='(bake-brownies)', duration=[2, 4])
    a_noop = tpn.create_episode(dispatch='(noop)', duration=[2, 4])

    # Choices
    c_R1 = tpn.choice([a_sugar, a_fruit], domain=['sugar', 'fruit'], choice_id='R1', type='controllable')
    c_store = tpn.choice([a_buy_more_sugar, a_no_sugar], domain=['buy', 'out'], choice_id='H1', type='uncontrollable')
    c_burnt = tpn.choice([tpn.sequence([a_throw_away, c_store, a_brownies_2, a_bake_2]), a_noop], domain=['burnt', 'good'], choice_id='H2', type='uncontrollable')

    seg_brownies = tpn.sequence([a_brownies_1, a_bake, c_burnt])

    c_dessert = tpn.choice([seg_brownies, a_cookies, a_fruit_salad], domain=['brownies', 'cookies', 'fruit'], choice_id='H3', type='uncontrollable')

    seq = tpn.sequence([c_R1, c_dessert])

    # Set the TPN to this segment and write out!
    tpn.start_event = seq.from_event
    tpn.end_event = seq.to_event
    # Write a file!
    tpn.to_xml_file('desserts.tpn')
