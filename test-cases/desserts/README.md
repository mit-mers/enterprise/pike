# Desert Example

This is a simple example put together to illustrate Riker / Uhura integration for the Toyota review. Domain comes from brainstorm between Shawn, Nikhil, and Steve. Example encoded by Steve.

A human and a robot our jointly making a dessert together: brownies, cookies, or fruit salad. Robot should pick sugar as first choice, because consistent with first two deserts. If brownies, they could burn in oven -- if that happens, person will need to go to store to get more supplies (50/50 if they're out of stock).

Overall probability of success: 91.25%
If brownies chosen: 95% (if other: 100%)
If brownies and burnt: 50% (violates risk bound -- call Uhura)
If brownies and not burnt: 100%
