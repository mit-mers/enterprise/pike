#!/usr/bin/env python

"""
RMPyL program modeling a collaborative pick-and-place task between a human and a robot.
"""
from rmpyl.rmpyl import RMPyL, Episode


def pickup(prog, obj, from_loc):
    return Episode(action='(pick-up robot {} {})'.format(obj, from_loc), duration={'ctype': 'controllable', 'lb': 0.1, 'ub': 120})

def stack(prog, obj_top, obj_bottom):
    return Episode(action='(stack robot {} {})'.format(obj_top, obj_bottom), duration={'ctype': 'controllable', 'lb': 0.1, 'ub': 120})

def drop(prog, obj, drop_loc):
    return Episode(action='(drop robot {} {})'.format(obj, drop_loc), duration={'ctype': 'controllable', 'lb': 0.1, 'ub': 120})


def noisy_pickup(prog, obj, from_loc, drop_loc, id=0):
    possibly_drop = prog.observe({'name': 'drop{}'.format(id),
                  'ctype': 'uncontrollable',
                  'domain': ['dropped', 'held']},
                  *[drop(prog, obj, drop_loc), Episode()])

    return prog.sequence(pickup(prog, obj, from_loc), possibly_drop)

def pickup_and_possibly_retry(prog, obj, from_loc, drop_loc, id=0):
    return prog.sequence(noisy_pickup(prog, obj, from_loc, drop_loc, id=id),
                         prog.decide({'name': 'retry{}'.format(id),
                                      'domain': ['yes', 'no']},
                                      *[noisy_pickup(prog, obj, from_loc, drop_loc, id=id+1), Episode()]))

def main_plan_2():
    prog = RMPyL(name='run()')
    prog *= pickup_and_possibly_retry(prog, 'redblock', 'tabletop1', 'tabletop1', id=0)
    prog *= stack(prog, 'redblock', 'blueblock')
    return prog


def main_plan_4():
    prog = RMPyL(name='run()')
    prog *= pickup_and_possibly_retry(prog, 'redblock', 'tabletop1', 'tabletop1', id=0)
    prog *= stack(prog, 'redblock', 'blueblock')
    prog *= pickup_and_possibly_retry(prog, 'greenblock', 'tabletop3', 'tabletop3', id=2)
    prog *= stack(prog, 'greenblock', 'redblock')

    return prog


if __name__=='__main__':
    prog = main_plan_2()
    prog.to_ptpn(filename="faulty_gripper_2.tpn")

    prog = main_plan_4()
    prog.to_ptpn(filename="faulty_gripper_4.tpn")
