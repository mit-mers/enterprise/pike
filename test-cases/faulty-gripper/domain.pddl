(define (domain boeing-domain)
	(:requirements :strips :typing :durative-actions :duration-inequalities)
	(:types robot block)

	(:predicates
		(holding ?r - robot ?t - block)
		(empty-gripper ?r - robot)
		(clear-above ?t - block)
		(can-pickup ?r - robot ?t - block)
		(on ?t ?b - block)
	)


	(:durative-action pick-up
		:parameters (?r - robot ?t - block ?b - block)
		:duration (and (>= ?duration 10) (<= ?duration 30))
		:condition (and	(at start (clear-above ?t))
      							(at start (empty-gripper ?r))
      							(over all (can-pickup ?r ?t))
      							(at start (on ?t ?b)))

		:effect 	(and 	(at end (not (empty-gripper ?r)))
      							(at end (not (on ?t ?b)))
      							(at end (holding ?r ?t))
      							(at end (not (clear-above ?t)))
                    (at end (clear-above ?b)))
	)

  (:durative-action stack
    :parameters (?r - robot ?t - block ?b - block)
    :duration (and (>= ?duration 10) (<= ?duration 30))
    :condition (and	(at start (holding ?r ?t))
                    (at start (clear-above ?b)))

    :effect 	(and 	(at end (empty-gripper ?r))
                    (at end (on ?t ?b))
                    (at end (not (holding ?r ?t)))
                    (at end (not (clear-above ?b)))
                    (at end (clear-above ?t)))
  )

  (:durative-action drop
    :parameters (?r - robot ?t - block ?b - block)
    :duration (and (>= ?duration 10) (<= ?duration 30))
    :condition (and	(at start (holding ?r ?t))
                    (at start (clear-above ?b)))

    :effect 	(and 	(at end (empty-gripper ?r))
                    (at end (on ?t ?b))
                    (at end (not (holding ?r ?t)))
                    (at end (not (clear-above ?b)))
                    (at end (clear-above ?t)))
  )



)
