(define (problem boeing-problem)
	(:domain boeing-domain)
	(:objects
		robot - robot
		redblock - block
		blueblock - block
    greenblock - block
		tabletop1 - block
    tabletop2 - block
    tabletop3 - block
	)

	(:init  (empty-gripper robot)
      		(can-pickup robot redblock)
      		(can-pickup robot blueblock)
          (can-pickup robot greenblock)

      		(on redblock tabletop1)
          (on blueblock tabletop2)
          (on greenblock tabletop3)

      		(clear-above redblock)
      		(clear-above blueblock)
          (clear-above greenblock)
          )

	(:goal
		(and ;; (on redblock blueblock)
         ;; (on greenblock redblock)
    )
)

)
