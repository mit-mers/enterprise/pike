#!/usr/bin/env python

"""
RMPyL program modeling a collaborative pick-and-place task between a human and a robot.
"""
from __future__ import division
from rmpyl.rmpyl import RMPyL, Episode



def main():
    prog = RMPyL(name='run()')
    prog *= prog.sequence(

                prog.decide({
                    'name': 'x_R',
                    'id': 'x_R',
                    'domain': ['a', 'b', 'c'],
                },
                *[Episode(action='(make a)', duration={'ctype': 'controllable', 'lb': 1, 'ub': 2}),
                  Episode(action='(make b)', duration={'ctype': 'controllable', 'lb': 1, 'ub': 2}),
                  Episode(action='(make c)', duration={'ctype': 'controllable', 'lb': 1, 'ub': 2})]),


                prog.observe({
                    'name': 'x_H',
                    'id': 'x_H',
                    'ctype': 'uncontrollable',
                    'domain': ['a', 'b', 'c']
                },
                *[Episode(action='(require a)', duration={'ctype': 'controllable', 'lb': 1, 'ub': 2}),
                  Episode(action='(require b)', duration={'ctype': 'controllable', 'lb': 1, 'ub': 2}),
                  Episode(action='(require c)', duration={'ctype': 'controllable', 'lb': 1, 'ub': 2})])

            )
    # Must get to work in 2.5 hours of first wake up
    # prog.add_overall_temporal_constraint(ctype='controllable', lb=0.0, ub=150.0)
    return prog


if __name__=='__main__':
    prog = main()
    prog.to_ptpn(filename="prob-test-1.tpn")
