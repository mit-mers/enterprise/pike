(define (domain probability-tests)
  (:requirements :strips :typing :durative-actions :duration-inequalities)
  (:types thing)

  (:predicates
   (has ?t - thing)
   )


  (:durative-action require
                    :parameters (?t - thing)
                    :duration (and (>= ?duration 1) (<= ?duration 2))
                    :condition (and (at start (has ?t)))
                    :effect (and ))

  (:durative-action make
                    :parameters (?t - thing)
                    :duration (and (>= ?duration 1) (<= ?duration 2))
                    :condition (and
                    :effect (and (at end (has ?t))))

  )
