(define (domain simple-plus-locking-domain)
    (:requirements :strips :typing :durative-actions :duration-inequalities)
  (:types thing)

  (:predicates
   (p ?t - thing)
   (lock-available ?t))

  (:durative-action make-p
                    :parameters (?t - thing)
                    :duration (and (>= ?duration 2) (<= ?duration 4))
                    :condition (and	(at start (lock-available ?t)))
                    :effect (and  (at end (p ?t))
                                  (at start (not (lock-available ?t)))
                                  (at end (lock-available ?t))))

  (:durative-action not-p
                    :parameters (?t - thing)
                    :duration (and (>= ?duration 2) (<= ?duration 4))
                    :condition (and	(at start (lock-available ?t)))
                    :effect (and (at end (not (p ?t)))
                                 (at start (not (lock-available ?t)))
                                 (at end (lock-available ?t))))

  (:durative-action require-p
                    :parameters (?t - thing)
                    :duration (and (>= ?duration 2) (<= ?duration 4))
                    :condition (and (at start (p ?t))
                                    (at start (lock-available ?t)))
                    :effect (and (at start (not (lock-available ?t)))
                                 (at end (lock-available ?t))))

  (:durative-action noop
                    :parameters (?t - thing)
                    :duration (and (>= ?duration 2) (<= ?duration 4))
                    :condition (and )
                    :effect (and ))

  )

