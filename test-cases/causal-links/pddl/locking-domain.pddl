(define (domain locking-domain)
    (:requirements :strips :typing :durative-actions :duration-inequalities)
  (:types thing)

  (:predicates
   (lock-available ?t - thing))

  (:durative-action go
                    :parameters (?t - thing)
                    :duration (and (>= ?duration 2) (<= ?duration 4))
                    :condition (and	(at start (lock-available ?t)))
                    :effect (and  (at start (not (lock-available ?t)))
                                  (at end        (lock-available ?t))))

  )

