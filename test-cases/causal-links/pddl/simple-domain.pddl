(define (domain simple-domain)
    (:requirements :strips :typing :durative-actions :duration-inequalities)
  (:types thing)

  (:predicates
   (p ?t - thing))

  (:durative-action make-p
                    :parameters (?t - thing)
                    :duration (and (>= ?duration 2) (<= ?duration 4))
                    :condition (and	)
                    :effect (and  (at end (p ?t))))

  (:durative-action not-p
                    :parameters (?t - thing)
                    :duration (and (>= ?duration 2) (<= ?duration 4))
                    :condition (and	)
                    :effect (and (at start (not (p ?t)))))

  (:durative-action require-p
                    :parameters (?t - thing)
                    :duration (and (>= ?duration 2) (<= ?duration 4))
                    :condition (and (at start (p ?t)))
                    :effect (and ))

  (:durative-action noop
                    :parameters (?t - thing)
                    :duration (and (>= ?duration 2) (<= ?duration 4))
                    :condition (and )
                    :effect (and ))

  )

