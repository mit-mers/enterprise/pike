#!/usr/bin/env python
"""
This file demonstrates how to use the pytpn API to build a TPN from scratch!
"""

from pytpn.tpn import TPN, Event, DecisionVariable, Episode, TemporalConstraint, Expression, And, Or, Not, Constant, Assignment, TPNSegment

if __name__ == '__main__':
    # Create a TPN
    tpn = TPN()
    # Create 3 activities (episodes with dispatch fields)
    a1 = tpn.create_episode(dispatch='(make-p t1)', duration=[2, 4])
    a2 = tpn.create_episode(dispatch='(make-p t2)', duration=[2, 4])



    a3 = tpn.create_episode(dispatch='(require-p t1)', duration=[2, 4])
    a4 = tpn.create_episode(dispatch='(require-p t2)', duration=[2, 4])
    choice_y1 = tpn.choice([a3, a4], domain=['c1', 'c2'], choice_id='y1', type='uncontrollable')

    a5 = tpn.create_episode(dispatch='(require-p t1)', duration=[2, 4])
    a6 = tpn.create_episode(dispatch='(require-p t2)', duration=[2, 4])
    choice_y2 = tpn.choice([a5, a6], domain=['c1', 'c2'], choice_id='y2', type='uncontrollable')



    # Make a choice!
    choice_x1 = tpn.choice([tpn.sequence([a1, choice_y1]), tpn.sequence([a2, choice_y2])], domain=['c1', 'c2'], choice_id='x1', type='controllable')

    # Set the TPN to this segment and write out!
    tpn.start_event = choice_x1.from_event
    tpn.end_event = choice_x1.to_event
    # Write a file!
    tpn.to_xml_file('simple.tpn')
