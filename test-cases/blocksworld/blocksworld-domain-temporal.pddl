(define (domain boeing-domain)
	(:requirements :strips :typing :durative-actions :duration-inequalities)
	(:types robot block cart)

	(:predicates
		(holding ?r - robot ?t - block)
		(empty-gripper ?r - robot)
		(clear-above ?t - block)
		(clear-cart ?c - cart)
		(can-reach ?r - robot ?t - block)
		(can-reach-cart ?r - robot ?c - cart)
		(on ?t ?b - block)
		(on-ground ?t - block)
		(on-cart ?t - block ?c - cart)
	)
	

	(:durative-action pick-up-block-from-ground
		:parameters (?r - robot ?t - block)
		:duration (and (>= ?duration 10) (<= ?duration 30))
		:condition (and		(at start (clear-above ?t))
							(at start (empty-gripper ?r))
							(over all (can-reach ?r ?t))
							(at start (on-ground ?t)))

		:effect 	(and 	(at end 	(not (empty-gripper ?r)))
							(at end (not (on-ground ?t)))
							(at end (holding ?r ?t))
							(at end (not (clear-above ?t))))
	)


	(:durative-action pick-up-block
		:parameters (?r - robot ?t ?b - block)
		:duration (and (>= ?duration 10) (<= ?duration 30))
		:condition (and		(at start (clear-above ?t))
							(at start (empty-gripper ?r))
							(over all (can-reach ?r ?t))
							(at start (on ?t ?b)))

		:effect 	(and 	(at end (not (empty-gripper ?r)))
							(at end (not (on ?t ?b)))
							(at end (holding ?r ?t))
							(at end (clear-above ?b))
							(at end (not (clear-above ?t))))
	)

	(:durative-action pick-up-block-from-cart
		:parameters (?r - robot ?t - block ?c - cart)
		:duration (and (>= ?duration 10) (<= ?duration 30))
		:condition (and		(at start (clear-above ?t))
							(at start (empty-gripper ?r))
							(over all (can-reach ?r ?t))
							(at start (on-cart ?t ?c)))

		:effect 	(and 	(at end (not (empty-gripper ?r)))
							(at end (not (on-cart ?t ?c)))
							(at end (holding ?r ?t))
							(at end (clear-cart ?c))
							(at end (not (clear-above ?t))))
	)

	(:durative-action stack-block
		:parameters (?r - robot ?t ?b - block)
		:duration (and (>= ?duration 10) (<= ?duration 30))
		:condition (and		(at start (holding ?r ?t))
							(at start (clear-above ?b)))

		:effect		(and	(at end (empty-gripper ?r))
							(at end (on ?t ?b))
							(at end (not (clear-above ?b)))
							(at end (not (holding ?r ?t)))
							(at end (clear-above ?t)))
	)

	(:durative-action put-block-on-ground
		:parameters (?r - robot ?t - block)
		:duration (and (>= ?duration 20) (<= ?duration 40))
		:condition (and 	(at start (holding ?r ?t)))

		:effect 	(and 	(at end (empty-gripper ?r))
							(at end (not (holding ?r ?t)))					
							(at end (on-ground ?t))
							(at end (clear-above ?t)))
	)

	(:durative-action put-block-on-cart
		:parameters (?r - robot ?t - block ?c - cart)
		:duration (and (>= ?duration 10) (<= ?duration 30))
		:condition (and		(at start (holding ?r ?t))
							(at start (clear-cart ?c))
							(over all (can-reach-cart ?r ?c)))

		:effect		(and	(at end (empty-gripper ?r))
							(at end (on-cart ?t ?c))
							(at end (not (clear-cart ?c)))
							(at end (not (holding ?r ?t)))
							(at end (clear-above ?t)))
	)
)

