(define (problem blocksword-problem)
	(:domain blocksworld-domain)
	(:objects
		WAM - robot
		BlueBlock - block
		RedBlock - block
		PinkBlock - block
		LimeBlock - block
		Cart - cart
	)

	(:init (empty-gripper WAM)
		(on-ground BlueBlock)
		(can-reach WAM RedBlock)
		(can-reach WAM PinkBlock)
		(can-reach WAM LimeBlock)
		(can-reach WAM BlueBlock)
		(can-reach-cart WAM Cart)
		(on PinkBlock BlueBlock)
		(on-ground LimeBlock)
		(on RedBlock LimeBlock)
		(clear-above RedBlock)
		(clear-above PinkBlock)
		(clear-cart Cart))

	(:goal
		(and (on-cart RedBlock Cart) (on PinkBlock RedBlock))
)

)
